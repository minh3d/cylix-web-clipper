"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var sinon = require("sinon");
var mockPdfDocument_1 = require("../contentCapture/mockPdfDocument");
var frontEndGlobals_1 = require("../../scripts/clipperUI/frontEndGlobals");
var saveToOneNote_1 = require("../../scripts/saveToOneNote/saveToOneNote");
var oneNoteSaveablePage_1 = require("../../scripts/saveToOneNote/oneNoteSaveablePage");
var oneNoteSaveablePdf_1 = require("../../scripts/saveToOneNote/oneNoteSaveablePdf");
var oneNoteSaveablePdfSynchronousBatched_1 = require("../../scripts/saveToOneNote/oneNoteSaveablePdfSynchronousBatched");
var clipperStorageKeys_1 = require("../../scripts/storage/clipperStorageKeys");
var asyncUtils_1 = require("../asyncUtils");
var testModule_1 = require("../testModule");
var SaveToOneNoteTests = (function (_super) {
    __extends(SaveToOneNoteTests, _super);
    function SaveToOneNoteTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SaveToOneNoteTests.prototype.module = function () {
        return "saveToOneNote";
    };
    SaveToOneNoteTests.prototype.beforeEach = function () {
        asyncUtils_1.AsyncUtils.mockSetTimeout();
        this.server = sinon.fakeServer.create();
        this.server.respondImmediately = true;
        this.saveToOneNote = new saveToOneNote_1.SaveToOneNote("userToken");
    };
    SaveToOneNoteTests.prototype.afterEach = function () {
        asyncUtils_1.AsyncUtils.restoreSetTimeout();
        this.server.restore();
    };
    SaveToOneNoteTests.prototype.tests = function () {
        var _this = this;
        test("When saving a 'just a page', save() should resolve with the parsed response and request in a responsePackage if the API call succeeds", function (assert) {
            var done = assert.async();
            var saveLocation = "sectionId";
            var responseJson = {
                key: "value"
            };
            _this.server.respondWith("POST", "https://www.onenote.com/api/v1.0/me/notes/sections/" + saveLocation + "/pages", [200, { "Content-Type": "application/json" },
                JSON.stringify(responseJson)
            ]);
            _this.saveToOneNote.save({ page: _this.getMockSaveablePage(), saveLocation: saveLocation }).then(function (responsePackage) {
                deepEqual(responsePackage.parsedResponse, responseJson, "The parsedResponse field should be the response in json form");
                ok(responsePackage.request, "The request field should be defined");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("When saving a 'just a page', save() should resolve with the parsed response and request in a responsePackage if the API call succeeds and no saveLocation is specified", function (assert) {
            var done = assert.async();
            var responseJson = {
                key: "value"
            };
            _this.server.respondWith("POST", "https://www.onenote.com/api/v1.0/me/notes/pages", [200, { "Content-Type": "application/json" },
                JSON.stringify(responseJson)
            ]);
            _this.saveToOneNote.save({ page: _this.getMockSaveablePage() }).then(function (responsePackage) {
                deepEqual(responsePackage.parsedResponse, responseJson, "The parsedResponse field should be the response in json form");
                ok(responsePackage.request, "The request field should be defined");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("When saving a 'just a page', but the this.server returns an unexpected response code, reject should be called with the error object", function (assert) {
            var done = assert.async();
            var responseJson = {
                key: "value"
            };
            _this.server.respondWith("POST", "https://www.onenote.com/api/v1.0/me/notes/pages", [404, { "Content-Type": "application/json" },
                JSON.stringify(responseJson)
            ]);
            _this.saveToOneNote.save({ page: _this.getMockSaveablePage() }).then(function (responsePackage) {
                ok(false, "resolve should not be called");
            }, function (error) {
                deepEqual(error, { error: "Unexpected response status", statusCode: 404, responseHeaders: { "Content-Type": "application/json" }, response: JSON.stringify(responseJson), timeout: 30000 }, "The error object should be returned in the reject");
            }).then(function () {
                done();
            });
        });
        // From this point, we now test cases where we hit multiple endpoints. For this to work, we need to declare the fakeServer
        // responses before the this.saveToOneNote call otherwise the fakeServer will respond with 404
        test("When saving a pdf, save() should resolve with the parsed response and request in a responsePackage assuming the patch permissions call succeeds", function (assert) {
            var done = assert.async();
            var saveLocation = "mySection";
            var pageId = "abc";
            var createPageJson = {
                id: pageId
            };
            // Request patch permissions
            _this.server.respondWith("GET", "https://www.onenote.com/api/v1.0/me/notes/sections/" + saveLocation + "/pages?top=1", [200, { "Content-Type": "application/json" },
                JSON.stringify({ getPages: "getPages" })
            ]);
            // Create initial page
            _this.server.respondWith("POST", "https://www.onenote.com/api/v1.0/me/notes/sections/" + saveLocation + "/pages", [200, { "Content-Type": "application/json" },
                JSON.stringify(createPageJson)
            ]);
            // Check that page exists before patching
            _this.server.respondWith("GET", "https://www.onenote.com/api/v1.0/me/notes/pages/" + pageId + "/content", [200, { "Content-Type": "application/json" },
                JSON.stringify({ getPage: "getPage" })
            ]);
            // Patch to the page
            _this.server.respondWith("PATCH", "https://www.onenote.com/api/v1.0/me/notes/pages/" + pageId + "/content", [200, { "Content-Type": "application/json" },
                JSON.stringify({ updatePage: "updatePage" })
            ]);
            _this.saveToOneNote.save({ page: _this.getMockSaveablePdf([0, 1]), saveLocation: saveLocation }).then(function (responsePackage) {
                deepEqual(responsePackage.parsedResponse, createPageJson, "The parsedResponse field should be the create page response in json form");
                ok(responsePackage.request, "The request field should be defined");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("When saving a pdf to the default location, save() should resolve with the parsed response and request in a responsePackage assuming the patch permissions call succeeds", function (assert) {
            var done = assert.async();
            var pageId = "abc";
            var createPageJson = {
                id: pageId
            };
            // Request patch permissions
            _this.server.respondWith("GET", "https://www.onenote.com/api/v1.0/me/notes/pages?top=1", [200, { "Content-Type": "application/json" },
                JSON.stringify({ getPages: "getPages" })
            ]);
            // Create initial page
            _this.server.respondWith("POST", "https://www.onenote.com/api/v1.0/me/notes/pages", [200, { "Content-Type": "application/json" },
                JSON.stringify(createPageJson)
            ]);
            // Check that page exists before patching
            _this.server.respondWith("GET", "https://www.onenote.com/api/v1.0/me/notes/pages/" + pageId + "/content", [200, { "Content-Type": "application/json" },
                JSON.stringify({ getPage: "getPage" })
            ]);
            // Patch to the page
            _this.server.respondWith("PATCH", "https://www.onenote.com/api/v1.0/me/notes/pages/" + pageId + "/content", [200, { "Content-Type": "application/json" },
                JSON.stringify({ updatePage: "updatePage" })
            ]);
            _this.saveToOneNote.save({ page: _this.getMockSaveablePdf([0, 1]) }).then(function (responsePackage) {
                deepEqual(responsePackage.parsedResponse, createPageJson, "The parsedResponse field should be the create page response in json form");
                ok(responsePackage.request, "The request field should be defined");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("When saving a pdf, save() should resolve with the parsed response and request in a responsePackage assuming the patch permission validity has been cached in localStorage", function (assert) {
            var done = assert.async();
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.hasPatchPermissions, "true");
            var pageId = "abc";
            var createPageJson = {
                id: pageId
            };
            // Create initial page
            _this.server.respondWith("POST", "https://www.onenote.com/api/v1.0/me/notes/pages", [200, { "Content-Type": "application/json" },
                JSON.stringify(createPageJson)
            ]);
            // Check that page exists before patching
            _this.server.respondWith("GET", "https://www.onenote.com/api/v1.0/me/notes/pages/" + pageId + "/content", [200, { "Content-Type": "application/json" },
                JSON.stringify({ getPage: "getPage" })
            ]);
            // Update page with pdf pages
            _this.server.respondWith("PATCH", "https://www.onenote.com/api/v1.0/me/notes/pages/" + pageId + "/content", [200, { "Content-Type": "application/json" },
                JSON.stringify({ updatePage: "updatePage" })
            ]);
            _this.saveToOneNote.save({ page: _this.getMockSaveablePdf([0, 1]) }).then(function (responsePackage) {
                deepEqual(responsePackage.parsedResponse, createPageJson, "The parsedResponse field should be the create page response in json form");
                ok(responsePackage.request, "The request field should be defined");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("When saving a single page pdf in BATCH mode, save() should resolve with the first createPage response and request in a responsePackage, assuming all createPages succeeded", function (assert) {
            // Create initialPage
            var done = assert.async();
            var saveLocation = "mySection";
            var pageId = "abc";
            var createPageJson = {
                id: pageId
            };
            // Create initial page
            _this.server.respondWith("POST", "https://www.onenote.com/api/v1.0/me/notes/sections/" + saveLocation + "/pages", [200, { "Content-Type": "application/json" },
                JSON.stringify(createPageJson)
            ]);
            _this.saveToOneNote.save({ page: _this.getMockSaveablePdfSynchronousBatch([]), saveLocation: saveLocation }).then(function (responsePackage) {
                deepEqual(responsePackage.parsedResponse, createPageJson, "The parsedResponse field should be the create page response in json form");
                ok(responsePackage.request, "The request field should be defined");
                strictEqual(_this.server.requests.length, 1, "A 1-page PDF that is distributing pages should make exactly 1 call to the API");
            }, function (error) {
                ok(false, "reject should not be called: " + error);
            }).then(function () {
                done();
            });
        });
        test("When saving a single page pdf in BATCH mode, save() should resolve with the first createPage response and request in a responsePackage, assuming all createPages succeeded and no saveLocation is specified", function (assert) {
            var done = assert.async();
            var pageId = "abc";
            var createPageJsonOne = {
                id: pageId
            };
            // Create initial page
            _this.server.respondWith("POST", "https://www.onenote.com/api/v1.0/me/notes/pages", [200, { "Content-Type": "application/json" },
                JSON.stringify(createPageJsonOne)
            ]);
            _this.saveToOneNote.save({ page: _this.getMockSaveablePdfSynchronousBatch([]) }).then(function (responsePackage) {
                deepEqual(responsePackage.parsedResponse, createPageJsonOne, "The parsedResponse field should be the create page response in json form of the first createPage request");
                ok(responsePackage.request, "The request field should be defined");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("When saving a multi-page pdf in BATCH mode, save() should resolve with the first createPage response and request in a responsePackage, assuming all createPages succeeded", function (assert) {
            var done = assert.async();
            var saveLocation = "mySection";
            var pageId = "abc";
            var createPageJsonOne = {
                id: pageId
            };
            var pageIdTwo = "def";
            var createPageJsonTwo = {
                id: pageIdTwo
            };
            var responses = [createPageJsonOne, createPageJsonTwo];
            var responseClass = new (function () {
                function class_1() {
                    var _this = this;
                    this.responses = responses;
                    this.count = 0;
                    this.respond = function (request) {
                        var response = _this.responses[_this.count];
                        _this.count++;
                        request.respond(201, { "Content-Type": "application/json" }, JSON.stringify(response));
                    };
                }
                return class_1;
            }());
            // Create initial page
            _this.server.respondWith("POST", "https://www.onenote.com/api/v1.0/me/notes/sections/" + saveLocation + "/pages", responseClass.respond);
            _this.saveToOneNote.save({ page: _this.getMockSaveablePdfSynchronousBatch([1]), saveLocation: saveLocation }).then(function (responsePackage) {
                deepEqual(responsePackage.parsedResponse, createPageJsonOne, "The parsedResponse field should be the create page response in json form of the first createPage request");
                ok(responsePackage.request, "The request field should be defined");
                strictEqual(_this.server.requests.length, 2, "A 2-page PDF that is distributing pages should make exactly 2 calls to the API");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("When saving a multi-page pdf in BATCH mode, save() should resolve with the first createPage response and request in a responsePackage, assuming all createPages succeeded and no saveLocation is specified", function (assert) {
            var done = assert.async();
            var pageId = "abc";
            var createPageJsonOne = {
                id: pageId
            };
            var pageIdTwo = "def";
            var createPageJsonTwo = {
                id: pageIdTwo
            };
            var responses = [createPageJsonOne, createPageJsonTwo];
            var responseClass = new (function () {
                function class_2() {
                    var _this = this;
                    this.responses = responses;
                    this.count = 0;
                    this.respond = function (request) {
                        var response = _this.responses[_this.count];
                        _this.count++;
                        request.respond(201, { "Content-Type": "application/json" }, JSON.stringify(response));
                    };
                }
                return class_2;
            }());
            // Create initial page
            _this.server.respondWith("POST", "https://www.onenote.com/api/v1.0/me/notes/pages", responseClass.respond);
            _this.saveToOneNote.save({ page: _this.getMockSaveablePdfSynchronousBatch([1]) }).then(function (responsePackage) {
                deepEqual(responsePackage.parsedResponse, createPageJsonOne, "The parsedResponse field should be the create page response in json form of the first createPage request");
                ok(responsePackage.request, "The request field should be defined");
                strictEqual(_this.server.requests.length, 2, "A 2-page PDF that is distributing pages should make exactly 2 calls to the API");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("When saving a pdf that is distribued over many pages, if the first page creation fails, reject should be called with the error object", function (assert) {
            var done = assert.async();
            var responseJson = {
                key: "value"
            };
            _this.server.respondWith("POST", "https://www.onenote.com/api/v1.0/me/notes/pages", [404, { "Content-Type": "application/json" }, JSON.stringify(responseJson)]);
            _this.saveToOneNote.save({ page: _this.getMockSaveablePdfSynchronousBatch([1]) }).then(function (responsePackage) {
                ok(false, "resolve should not be called");
            }, function (error) {
                deepEqual(error, { error: "Unexpected response status", statusCode: 404, responseHeaders: { "Content-Type": "application/json" }, response: JSON.stringify(responseJson), timeout: 30000 }, "The error object should be returned in the reject");
            }).then(function () {
                done();
            });
        });
        test("When saving a pdf that is distributed over many pages, if any of the createPage calls fail, the call should still resolve", function (assert) {
            var done = assert.async();
            var pageId1 = "abc";
            var createPageJson1 = {
                id: pageId1
            };
            var pageId2 = "def";
            var createPageJson2 = {
                id: pageId2
            };
            var errorResponse = {
                key: "value"
            };
            var pageId3 = "ghi";
            var createPageJson3 = {
                id: pageId3
            };
            var headers = { "Content-Type": "application/json" };
            var responses = [
                {
                    statusCode: 201,
                    headers: headers,
                    body: JSON.stringify(createPageJson1)
                },
                {
                    statusCode: 201,
                    headers: headers,
                    body: JSON.stringify(createPageJson2)
                },
                {
                    statusCode: 404,
                    headers: headers,
                    body: JSON.stringify(errorResponse)
                },
                {
                    statusCode: 201,
                    headers: headers,
                    body: JSON.stringify(createPageJson3)
                }
            ];
            var responseClass = new (function () {
                function class_3() {
                    var _this = this;
                    this.responses = responses;
                    this.count = 0;
                    this.respond = function (request) {
                        var response = _this.responses[_this.count];
                        _this.count++;
                        request.respond(response.statusCode, response.headers, response.body);
                    };
                }
                return class_3;
            }());
            // Initial create page
            _this.server.respondWith("POST", "https://www.onenote.com/api/v1.0/me/notes/pages", responseClass.respond);
            _this.saveToOneNote.save({ page: _this.getMockSaveablePdfSynchronousBatch([1]) }).then(function (responsePackage) {
                deepEqual(responsePackage.parsedResponse, createPageJson1, "The parsedResponse field should be the create page response in json form of the first createPage request");
                ok(responsePackage.request, "The request field should be defined");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("When saving a pdf and a PATCH permission check is needed, but that check returns an unexpected response code, reject should be called with the error object", function (assert) {
            var done = assert.async();
            var responseJson = {
                getPages: "getPages"
            };
            // Request patch permissions
            _this.server.respondWith("GET", "https://www.onenote.com/api/v1.0/me/notes/pages?top=1", [404, { "Content-Type": "application/json" },
                JSON.stringify(responseJson)
            ]);
            _this.saveToOneNote.save({ page: _this.getMockSaveablePdf([0, 1]) }).then(function (responsePackage) {
                ok(false, "resolve should not be called");
            }, function (error) {
                deepEqual(error, { error: "Unexpected response status", statusCode: 404, responseHeaders: { "Content-Type": "application/json" }, response: JSON.stringify(responseJson), timeout: 30000 }, "The error object should be returned in the reject");
            }).then(function () {
                done();
            });
        });
        test("When saving a pdf, if the page creation fails, reject should be called with the error object", function (assert) {
            var done = assert.async();
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.hasPatchPermissions, "true");
            var responseJson = {
                getPages: "getPages"
            };
            // Create initial page
            _this.server.respondWith("POST", "https://www.onenote.com/api/v1.0/me/notes/pages", [404, { "Content-Type": "application/json" },
                JSON.stringify(responseJson)
            ]);
            _this.saveToOneNote.save({ page: _this.getMockSaveablePdf([0, 1]) }).then(function (responsePackage) {
                ok(false, "resolve should not be called");
            }, function (error) {
                deepEqual(error, { error: "Unexpected response status", statusCode: 404, responseHeaders: { "Content-Type": "application/json" }, response: JSON.stringify(responseJson), timeout: 30000 }, "The error object should be returned in the reject");
            }).then(function () {
                done();
            });
        });
        test("When saving a pdf, if the check for page existence fails, reject should be called with the error object", function (assert) {
            var done = assert.async();
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.hasPatchPermissions, "true");
            var pageId = "abc";
            var createPageJson = {
                id: pageId
            };
            var responseJson = {
                getPages: "getPages"
            };
            // Create initial page
            _this.server.respondWith("POST", "https://www.onenote.com/api/v1.0/me/notes/pages", [200, { "Content-Type": "application/json" },
                JSON.stringify(createPageJson)
            ]);
            // Check that page exists before patching
            _this.server.respondWith("GET", "https://www.onenote.com/api/v1.0/me/notes/pages/" + pageId + "/content", [404, { "Content-Type": "application/json" },
                JSON.stringify(responseJson)
            ]);
            _this.saveToOneNote.save({ page: _this.getMockSaveablePdf([0, 1]) }).then(function (responsePackage) {
                ok(false, "resolve should not be called");
            }, function (error) {
                deepEqual(error, { error: "Unexpected response status", statusCode: 404, responseHeaders: { "Content-Type": "application/json" }, response: JSON.stringify(responseJson), timeout: 30000 }, "The error object should be returned in the reject");
            }).then(function () {
                done();
            });
        });
        test("When saving a pdf, if the PATCH call fails, reject should be called with the error object", function (assert) {
            var done = assert.async();
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.hasPatchPermissions, "true");
            var pageId = "abc";
            var createPageJson = {
                id: pageId
            };
            var responseJson = {
                getPages: "getPages"
            };
            // Create initial page
            _this.server.respondWith("POST", "https://www.onenote.com/api/v1.0/me/notes/pages", [200, { "Content-Type": "application/json" },
                JSON.stringify(createPageJson)
            ]);
            // Check that page exists before patching
            _this.server.respondWith("GET", "https://www.onenote.com/api/v1.0/me/notes/pages/" + pageId + "/content", [200, { "Content-Type": "application/json" },
                JSON.stringify({ getPage: "getPage" })
            ]);
            // Update page with pdf pages
            _this.server.respondWith("PATCH", "https://www.onenote.com/api/v1.0/me/notes/pages/" + pageId + "/content", [404, { "Content-Type": "application/json" },
                JSON.stringify(responseJson)
            ]);
            _this.saveToOneNote.save({ page: _this.getMockSaveablePdf([0, 1]) }).then(function (responsePackage) {
                ok(false, "resolve should not be called");
            }, function (error) {
                deepEqual(error, { error: "Unexpected response status", statusCode: 404, responseHeaders: { "Content-Type": "application/json" }, response: JSON.stringify(responseJson), timeout: 30000 }, "The error object should be returned in the reject");
            }).then(function () {
                done();
            });
        });
        test("When saving a pdf and a save location is specified, if the PATCH call fails, reject should be called with the error object", function (assert) {
            var done = assert.async();
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.hasPatchPermissions, "true");
            var saveLocation = "sectionId";
            var pageId = "abc";
            var createPageJson = {
                id: pageId
            };
            var responseJson = {
                getPages: "getPages"
            };
            // Create initial page
            _this.server.respondWith("POST", "https://www.onenote.com/api/v1.0/me/notes/sections/" + saveLocation + "/pages", [200, { "Content-Type": "application/json" },
                JSON.stringify(createPageJson)
            ]);
            // Check that page exists before patching
            _this.server.respondWith("GET", "https://www.onenote.com/api/v1.0/me/notes/pages/" + pageId + "/content", [200, { "Content-Type": "application/json" },
                JSON.stringify({ getPage: "getPage" })
            ]);
            // Update page with pdf pages
            _this.server.respondWith("PATCH", "https://www.onenote.com/api/v1.0/me/notes/pages/" + pageId + "/content", [404, { "Content-Type": "application/json" },
                JSON.stringify(responseJson)
            ]);
            _this.saveToOneNote.save({ page: _this.getMockSaveablePdf([0, 1]), saveLocation: saveLocation }).then(function (responsePackage) {
                ok(false, "resolve should not be called");
            }, function (error) {
                deepEqual(error, { error: "Unexpected response status", statusCode: 404, responseHeaders: { "Content-Type": "application/json" }, response: JSON.stringify(responseJson), timeout: 30000 }, "The error object should be returned in the reject");
            }).then(function () {
                done();
            });
        });
    };
    SaveToOneNoteTests.prototype.getMockSaveablePage = function () {
        var page = new OneNoteApi.OneNotePage();
        return new oneNoteSaveablePage_1.OneNoteSaveablePage(page);
    };
    SaveToOneNoteTests.prototype.getMockSaveablePdf = function (pageIndexes) {
        var page = new OneNoteApi.OneNotePage();
        var mockPdf = new mockPdfDocument_1.MockPdfDocument();
        return new oneNoteSaveablePdf_1.OneNoteSaveablePdf(page, mockPdf, pageIndexes);
    };
    SaveToOneNoteTests.prototype.getMockSaveablePdfSynchronousBatch = function (pageIndexes) {
        var page = new OneNoteApi.OneNotePage();
        var mockPdf = new mockPdfDocument_1.MockPdfDocument();
        return new oneNoteSaveablePdfSynchronousBatched_1.OneNoteSaveablePdfSynchronousBatched(page, mockPdf, pageIndexes, "en-US", "sample.pdf");
    };
    return SaveToOneNoteTests;
}(testModule_1.TestModule));
exports.SaveToOneNoteTests = SaveToOneNoteTests;
(new SaveToOneNoteTests()).runTests();
