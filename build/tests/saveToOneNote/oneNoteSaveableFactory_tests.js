"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var mockPdfDocument_1 = require("../contentCapture/mockPdfDocument");
var clipMode_1 = require("../../scripts/clipperUI/clipMode");
var status_1 = require("../../scripts/clipperUI/status");
var oneNoteSaveableFactory_1 = require("../../scripts/saveToOneNote/oneNoteSaveableFactory");
var mockProps_1 = require("../mockProps");
var testModule_1 = require("../testModule");
/**
 * For now, we have tests that simulate passing in valid clipperStates into the factory. We don't validate
 * the output, but we at least make sure we don't throw any exceptions or any funny business.
 */
var OneNoteSaveableFactoryTests = (function (_super) {
    __extends(OneNoteSaveableFactoryTests, _super);
    function OneNoteSaveableFactoryTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    OneNoteSaveableFactoryTests.prototype.module = function () {
        return "oneNoteSaveableFactoryTests";
    };
    OneNoteSaveableFactoryTests.prototype.tests = function () {
        var _this = this;
        test("Creating a saveable full page from clipperState should resolve", function (assert) {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.currentMode.set(clipMode_1.ClipMode.FullPage);
            clipperState.fullPageResult.data.Images = ["data:image/png;base64,iVBORw0KGgo"];
            clipperState.fullPageResult.status = status_1.Status.Succeeded;
            var factory = new oneNoteSaveableFactory_1.OneNoteSaveableFactory(clipperState);
            _this.assertFactoryDoesNotReject(factory, assert);
        });
        test("Creating a saveable augmented content from clipperState should resolve", function (assert) {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.currentMode.set(clipMode_1.ClipMode.Augmentation);
            clipperState.augmentationResult.data.ContentModel = 1;
            clipperState.augmentationResult.data.ContentInHtml = "<p>hello world</p>";
            clipperState.augmentationResult.status = status_1.Status.Succeeded;
            var factory = new oneNoteSaveableFactory_1.OneNoteSaveableFactory(clipperState);
            _this.assertFactoryDoesNotReject(factory, assert);
        });
        test("Creating a saveable pdf from clipperState should resolve if the user selected all pages", function (assert) {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.currentMode.set(clipMode_1.ClipMode.Pdf);
            clipperState.pdfResult.data.set({
                pdf: new mockPdfDocument_1.MockPdfDocument(),
                viewportDimensions: mockPdfDocument_1.MockPdfValues.dimensions,
                byteLength: mockPdfDocument_1.MockPdfValues.byteLength
            });
            clipperState.pdfResult.status = status_1.Status.Succeeded;
            clipperState.pdfPreviewInfo.allPages = true;
            clipperState.pdfPreviewInfo.isLocalFileAndNotAllowed = false;
            clipperState.pdfPreviewInfo.selectedPageRange = "";
            clipperState.pdfPreviewInfo.shouldAttachPdf = false;
            clipperState.pdfPreviewInfo.shouldDistributePages = false;
            var factory = new oneNoteSaveableFactory_1.OneNoteSaveableFactory(clipperState);
            _this.assertFactoryDoesNotReject(factory, assert);
        });
        test("Creating a saveable pdf from clipperState should resolve if the user selected a page range", function (assert) {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.currentMode.set(clipMode_1.ClipMode.Pdf);
            clipperState.pdfResult.data.set({
                pdf: new mockPdfDocument_1.MockPdfDocument(),
                viewportDimensions: mockPdfDocument_1.MockPdfValues.dimensions,
                byteLength: mockPdfDocument_1.MockPdfValues.byteLength
            });
            clipperState.pdfResult.status = status_1.Status.Succeeded;
            clipperState.pdfPreviewInfo.allPages = false;
            clipperState.pdfPreviewInfo.isLocalFileAndNotAllowed = false;
            clipperState.pdfPreviewInfo.selectedPageRange = "1-3";
            clipperState.pdfPreviewInfo.shouldAttachPdf = false;
            clipperState.pdfPreviewInfo.shouldDistributePages = false;
            var factory = new oneNoteSaveableFactory_1.OneNoteSaveableFactory(clipperState);
            _this.assertFactoryDoesNotReject(factory, assert);
        });
        test("Creating a saveable pdf from clipperState should resolve if the user attaches the pdf", function (assert) {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.currentMode.set(clipMode_1.ClipMode.Pdf);
            clipperState.pdfResult.data.set({
                pdf: new mockPdfDocument_1.MockPdfDocument(),
                viewportDimensions: mockPdfDocument_1.MockPdfValues.dimensions,
                byteLength: mockPdfDocument_1.MockPdfValues.byteLength
            });
            clipperState.pdfResult.status = status_1.Status.Succeeded;
            clipperState.pdfPreviewInfo.allPages = true;
            clipperState.pdfPreviewInfo.isLocalFileAndNotAllowed = false;
            clipperState.pdfPreviewInfo.selectedPageRange = "";
            clipperState.pdfPreviewInfo.shouldAttachPdf = true;
            clipperState.pdfPreviewInfo.shouldDistributePages = false;
            var factory = new oneNoteSaveableFactory_1.OneNoteSaveableFactory(clipperState);
            _this.assertFactoryDoesNotReject(factory, assert);
        });
        test("Creating a saveable pdf from clipperState should resolve if the user attaches one note per page", function (assert) {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.currentMode.set(clipMode_1.ClipMode.Pdf);
            clipperState.pdfResult.data.set({
                pdf: new mockPdfDocument_1.MockPdfDocument(),
                viewportDimensions: mockPdfDocument_1.MockPdfValues.dimensions,
                byteLength: mockPdfDocument_1.MockPdfValues.byteLength
            });
            clipperState.pdfResult.status = status_1.Status.Succeeded;
            clipperState.pdfPreviewInfo.allPages = true;
            clipperState.pdfPreviewInfo.isLocalFileAndNotAllowed = false;
            clipperState.pdfPreviewInfo.selectedPageRange = "";
            clipperState.pdfPreviewInfo.shouldAttachPdf = true;
            clipperState.pdfPreviewInfo.shouldDistributePages = true;
            var factory = new oneNoteSaveableFactory_1.OneNoteSaveableFactory(clipperState);
            _this.assertFactoryDoesNotReject(factory, assert);
        });
        test("Creating a saveable region clip from clipperState should resolve", function (assert) {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.currentMode.set(clipMode_1.ClipMode.Region);
            clipperState.regionResult.data = ["data:image/png;base64,iVBORw0KGgo"];
            clipperState.regionResult.status = status_1.Status.Succeeded;
            var factory = new oneNoteSaveableFactory_1.OneNoteSaveableFactory(clipperState);
            _this.assertFactoryDoesNotReject(factory, assert);
        });
        test("Creating a saveable region clip from clipperState should resolve if there's more than 1 region", function (assert) {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.currentMode.set(clipMode_1.ClipMode.Region);
            clipperState.regionResult.data = ["data:image/png;base64,iVBORw0KGgo", "data:image/png;base64,iVBORw0KGgo"];
            clipperState.regionResult.status = status_1.Status.Succeeded;
            var factory = new oneNoteSaveableFactory_1.OneNoteSaveableFactory(clipperState);
            _this.assertFactoryDoesNotReject(factory, assert);
        });
        test("Creating a saveable bookmark from clipperState should resolve", function (assert) {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.currentMode.set(clipMode_1.ClipMode.Bookmark);
            clipperState.bookmarkResult.data = {
                description: "desc",
                thumbnailSrc: "data:image/png;base64,iVBORw0KGgo",
                title: "title",
                url: "url"
            };
            clipperState.bookmarkPreviewInfo = { previewBodyHtml: "<p>stuff</p>" };
            clipperState.bookmarkResult.status = status_1.Status.Succeeded;
            var factory = new oneNoteSaveableFactory_1.OneNoteSaveableFactory(clipperState);
            _this.assertFactoryDoesNotReject(factory, assert);
        });
        test("Creating a saveable selection clip from clipperState should resolve", function (assert) {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.currentMode.set(clipMode_1.ClipMode.Selection);
            clipperState.selectionPreviewInfo.previewBodyHtml = "<h1>header</h1>";
            var factory = new oneNoteSaveableFactory_1.OneNoteSaveableFactory(clipperState);
            _this.assertFactoryDoesNotReject(factory, assert);
        });
    };
    OneNoteSaveableFactoryTests.prototype.assertFactoryDoesNotReject = function (factory, assert) {
        var done = assert.async();
        factory.getSaveable().then(function (saveable) {
            ok(!!saveable, "getSaveable should not resolve with an undefined object");
        })["catch"](function () {
            ok(false, "getSaveable should not throw an exception or reject");
        }).then(function () {
            done();
        });
    };
    return OneNoteSaveableFactoryTests;
}(testModule_1.TestModule));
exports.OneNoteSaveableFactoryTests = OneNoteSaveableFactoryTests;
(new OneNoteSaveableFactoryTests()).runTests();
