"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var oneNoteSaveablePage_1 = require("../../scripts/saveToOneNote/oneNoteSaveablePage");
var testModule_1 = require("../testModule");
var OneNoteSaveablePageTests = (function (_super) {
    __extends(OneNoteSaveablePageTests, _super);
    function OneNoteSaveablePageTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    OneNoteSaveablePageTests.prototype.module = function () {
        return "oneNoteSaveablePage";
    };
    OneNoteSaveablePageTests.prototype.tests = function () {
        test("When getting the page object, it should be the same object as the page passed into the ctor", function (assert) {
            var done = assert.async();
            var expectedPage = new OneNoteApi.OneNotePage();
            var saveable = new oneNoteSaveablePage_1.OneNoteSaveablePage(expectedPage);
            saveable.getPage().then(function (page) {
                strictEqual(expectedPage, page);
                done();
            });
        });
        test("getNumPatches should always return 0", function () {
            var saveable = new oneNoteSaveablePage_1.OneNoteSaveablePage(new OneNoteApi.OneNotePage());
            strictEqual(saveable.getNumPatches(), 0, "There are 0 patches to apply");
        });
        test("getPatch should always return undefined", function (assert) {
            var done = assert.async();
            var saveable = new oneNoteSaveablePage_1.OneNoteSaveablePage(new OneNoteApi.OneNotePage());
            saveable.getPatch(0).then(function (page) {
                strictEqual(page, undefined);
                done();
            });
        });
    };
    return OneNoteSaveablePageTests;
}(testModule_1.TestModule));
exports.OneNoteSaveablePageTests = OneNoteSaveablePageTests;
(new OneNoteSaveablePageTests()).runTests();
