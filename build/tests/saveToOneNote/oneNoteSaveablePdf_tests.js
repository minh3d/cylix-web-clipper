"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var mockPdfDocument_1 = require("../contentCapture/mockPdfDocument");
var oneNoteSaveablePdf_1 = require("../../scripts/saveToOneNote/oneNoteSaveablePdf");
var testModule_1 = require("../testModule");
var OneNoteSaveablePdfTests = (function (_super) {
    __extends(OneNoteSaveablePdfTests, _super);
    function OneNoteSaveablePdfTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    OneNoteSaveablePdfTests.prototype.module = function () {
        return "oneNoteSaveablePdf";
    };
    OneNoteSaveablePdfTests.prototype.tests = function () {
        test("When getting the page object, it should be the same object as the page passed into the ctor", function (assert) {
            var done = assert.async();
            var expectedPage = new OneNoteApi.OneNotePage();
            var saveable = new oneNoteSaveablePdf_1.OneNoteSaveablePdf(expectedPage, new mockPdfDocument_1.MockPdfDocument(), [0]);
            saveable.getPage().then(function (page) {
                strictEqual(expectedPage, page);
                done();
            });
        });
    };
    return OneNoteSaveablePdfTests;
}(testModule_1.TestModule));
exports.OneNoteSaveablePdfTests = OneNoteSaveablePdfTests;
// TODO: extend MockPdfDocument to allow the dev to create pdf documents of arbitrary page length
(new OneNoteSaveablePdfTests()).runTests();
