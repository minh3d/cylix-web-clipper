"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var mockPdfDocument_1 = require("../contentCapture/mockPdfDocument");
var oneNoteSaveablePdfSynchronousBatched_1 = require("../../scripts/saveToOneNote/oneNoteSaveablePdfSynchronousBatched");
var testModule_1 = require("../testModule");
var OneNoteSaveablePdfSynchronousBatchedTests = (function (_super) {
    __extends(OneNoteSaveablePdfSynchronousBatchedTests, _super);
    function OneNoteSaveablePdfSynchronousBatchedTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    OneNoteSaveablePdfSynchronousBatchedTests.prototype.module = function () {
        return "oneNoteSaveablePdfSynchronousBatched";
    };
    OneNoteSaveablePdfSynchronousBatchedTests.prototype.tests = function () {
        test("When getting the page object, it should be the same object as the page passed into the ctor", function (assert) {
            var done = assert.async();
            var expectedPage = new OneNoteApi.OneNotePage();
            var saveable = new oneNoteSaveablePdfSynchronousBatched_1.OneNoteSaveablePdfSynchronousBatched(expectedPage, new mockPdfDocument_1.MockPdfDocument(), [1], "en-US", "sample.pdf");
            saveable.getPage().then(function (page) {
                strictEqual(expectedPage, page);
                done();
            });
        });
        test("When constructed, getNumPages should return the length of pageIndices + 1 to account for the initial page passed in", function () {
            var expectedPage = new OneNoteApi.OneNotePage();
            var saveable = new oneNoteSaveablePdfSynchronousBatched_1.OneNoteSaveablePdfSynchronousBatched(expectedPage, new mockPdfDocument_1.MockPdfDocument(), [], "en-US", "sample.pdf");
            strictEqual(saveable.getNumPages(), 1);
            saveable = new oneNoteSaveablePdfSynchronousBatched_1.OneNoteSaveablePdfSynchronousBatched(expectedPage, new mockPdfDocument_1.MockPdfDocument(), [1], "en-US", "sample.pdf");
            strictEqual(saveable.getNumPages(), 2);
            saveable = new oneNoteSaveablePdfSynchronousBatched_1.OneNoteSaveablePdfSynchronousBatched(expectedPage, new mockPdfDocument_1.MockPdfDocument(), [3, 4, 5, 6], "en-US", "sample.pdf");
            strictEqual(saveable.getNumPages(), 5);
        });
    };
    return OneNoteSaveablePdfSynchronousBatchedTests;
}(testModule_1.TestModule));
exports.OneNoteSaveablePdfSynchronousBatchedTests = OneNoteSaveablePdfSynchronousBatchedTests;
// TODO: extend MockPdfDocument to allow the dev to create pdf documents of arbitrary page length
(new OneNoteSaveablePdfSynchronousBatchedTests()).runTests();
