"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var sinon = require("sinon");
var constants_1 = require("../../scripts/constants");
var frontEndGlobals_1 = require("../../scripts/clipperUI/frontEndGlobals");
var augmentationHelper_1 = require("../../scripts/contentCapture/augmentationHelper");
var asyncUtils_1 = require("../asyncUtils");
var mithrilUtils_1 = require("../mithrilUtils");
var mockProps_1 = require("../mockProps");
var testModule_1 = require("../testModule");
var AugmentationHelperTests = (function (_super) {
    __extends(AugmentationHelperTests, _super);
    function AugmentationHelperTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AugmentationHelperTests.prototype.module = function () {
        return "augmentationHelper-sinon";
    };
    AugmentationHelperTests.prototype.beforeEach = function () {
        this.server = sinon.fakeServer.create();
        this.server.respondImmediately = true;
        asyncUtils_1.AsyncUtils.mockSetTimeout();
        // The augmentation call waits on the session id, so we need to set this
        frontEndGlobals_1.Clipper.sessionId.set("abcde");
    };
    AugmentationHelperTests.prototype.afterEach = function () {
        this.server.restore();
        asyncUtils_1.AsyncUtils.restoreSetTimeout();
        frontEndGlobals_1.Clipper.sessionId.set(undefined);
    };
    AugmentationHelperTests.prototype.tests = function () {
        var _this = this;
        test("makeAugmentationRequest should return the parsed response and the original xhr in the resolved promise", function (assert) {
            var done = assert.async();
            var state = mockProps_1.MockProps.getMockClipperState();
            var pageInfo = state.pageInfo;
            var responseJson = [{
                    ContentModel: 1,
                    ContentInHtml: "Hello world",
                    ContentObjects: []
                }];
            _this.server.respondWith("POST", constants_1.Constants.Urls.augmentationApiUrl + "?renderMethod=extractAggressive&url=" + pageInfo.canonicalUrl + "&lang=" + pageInfo.contentLocale, [200, { "Content-Type": "application/json" },
                JSON.stringify(responseJson)
            ]);
            augmentationHelper_1.AugmentationHelper.makeAugmentationRequest(pageInfo.canonicalUrl, pageInfo.contentLocale, pageInfo.contentData, "abc123").then(function (responsePackage) {
                deepEqual(responsePackage.parsedResponse, responseJson, "The parsedResponse field should be the response in json form");
                ok(responsePackage.request, "The request field should be defined");
            })["catch"](function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("makeAugmentationRequest should return the error object in the rejected promise if the status code is not 200", function (assert) {
            var done = assert.async();
            var state = mockProps_1.MockProps.getMockClipperState();
            var pageInfo = state.pageInfo;
            var responseJson = "A *spooky* 404 message!";
            _this.server.respondWith("POST", constants_1.Constants.Urls.augmentationApiUrl + "?renderMethod=extractAggressive&url=" + pageInfo.canonicalUrl + "&lang=" + pageInfo.contentLocale, [404, { "Content-Type": "application/json" },
                JSON.stringify(responseJson)
            ]);
            augmentationHelper_1.AugmentationHelper.makeAugmentationRequest(pageInfo.canonicalUrl, pageInfo.contentLocale, pageInfo.contentData, "abc123").then(function (responsePackage) {
                ok(false, "resolve should not be called");
            })["catch"](function (error) {
                deepEqual(error, { error: "Unexpected response status", statusCode: 404, responseHeaders: { "Content-Type": "application/json" }, response: JSON.stringify(responseJson), timeout: 30000 }, "The error object should be returned in the reject");
            }).then(function () {
                done();
            });
        });
        test("makeAugmentationRequest should return the error object in the rejected promise if the status code is 200, but the response cannot be parsed as json", function (assert) {
            var done = assert.async();
            var state = mockProps_1.MockProps.getMockClipperState();
            var pageInfo = state.pageInfo;
            var obj = [{
                    ContentModel: 1,
                    ContentInHtml: "Hello world",
                    ContentObjects: []
                }];
            var unJsonifiableString = "{" + JSON.stringify(obj);
            _this.server.respondWith("POST", constants_1.Constants.Urls.augmentationApiUrl + "?renderMethod=extractAggressive&url=" + pageInfo.canonicalUrl + "&lang=" + pageInfo.contentLocale, [200, { "Content-Type": "application/json" },
                unJsonifiableString
            ]);
            augmentationHelper_1.AugmentationHelper.makeAugmentationRequest(pageInfo.canonicalUrl, pageInfo.contentLocale, pageInfo.contentData, "abc123").then(function (responsePackage) {
                ok(false, "resolve should not be called");
            })["catch"](function (error) {
                deepEqual(error, { error: "Unable to parse response", statusCode: 200, responseHeaders: { "Content-Type": "application/json" }, response: unJsonifiableString }, "The error object should be returned in the reject");
            }).then(function () {
                done();
            });
        });
    };
    return AugmentationHelperTests;
}(testModule_1.TestModule));
exports.AugmentationHelperTests = AugmentationHelperTests;
var AugmentationHelperSinonTests = (function (_super) {
    __extends(AugmentationHelperSinonTests, _super);
    function AugmentationHelperSinonTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AugmentationHelperSinonTests.prototype.module = function () {
        return "augmentationHelper";
    };
    AugmentationHelperSinonTests.prototype.beforeEach = function () {
        this.fixture = mithrilUtils_1.MithrilUtils.getFixture();
    };
    AugmentationHelperSinonTests.prototype.tests = function () {
        var _this = this;
        test("getAugmentationType returns the augmentation type as a string for types that we support", function () {
            var supported = [augmentationHelper_1.AugmentationModel.Article, augmentationHelper_1.AugmentationModel.Recipe, augmentationHelper_1.AugmentationModel.Product];
            var state = mockProps_1.MockProps.getMockClipperState();
            for (var i = 0; i < supported.length; i++) {
                state.augmentationResult.data = {
                    ContentInHtml: "",
                    ContentModel: supported[i],
                    ContentObjects: undefined,
                    PageMetadata: {}
                };
                strictEqual(augmentationHelper_1.AugmentationHelper.getAugmentationType(state), augmentationHelper_1.AugmentationModel[supported[i]]);
            }
        });
        test("getAugmentationType returns 'Article' if the augmentation result is unavailable", function () {
            strictEqual(augmentationHelper_1.AugmentationHelper.getAugmentationType(undefined), augmentationHelper_1.AugmentationModel[augmentationHelper_1.AugmentationModel.Article]);
            var state = mockProps_1.MockProps.getMockClipperState();
            state.augmentationResult.data = undefined;
            strictEqual(augmentationHelper_1.AugmentationHelper.getAugmentationType(state), augmentationHelper_1.AugmentationModel[augmentationHelper_1.AugmentationModel.Article]);
            state.augmentationResult = undefined;
            strictEqual(augmentationHelper_1.AugmentationHelper.getAugmentationType(state), augmentationHelper_1.AugmentationModel[augmentationHelper_1.AugmentationModel.Article]);
        });
        test("getAugmentationType returns 'Article' for types that we don't support", function () {
            var state = mockProps_1.MockProps.getMockClipperState();
            state.augmentationResult.data = {
                ContentInHtml: "",
                ContentModel: augmentationHelper_1.AugmentationModel.BizCard,
                ContentObjects: undefined,
                PageMetadata: {}
            };
            strictEqual(augmentationHelper_1.AugmentationHelper.getAugmentationType(state), augmentationHelper_1.AugmentationModel[augmentationHelper_1.AugmentationModel.Article]);
        });
        test("getArticlePreviewElement should return the MainArticleContainer class element if it exists", function () {
            var mainArticleContainer = document.createElement("div");
            mainArticleContainer.className = "MainArticleContainer";
            _this.fixture.appendChild(mainArticleContainer);
            var actual = augmentationHelper_1.AugmentationHelper.getArticlePreviewElement(document);
            strictEqual(actual, mainArticleContainer, "The MainArticleContainer should be retrieved");
        });
        test("getArticlePreviewElement should return the document's body if the MainArticleContainer class element does not exist", function () {
            var actual = augmentationHelper_1.AugmentationHelper.getArticlePreviewElement(document);
            strictEqual(actual, document.body, "The body should be retrieved");
        });
    };
    return AugmentationHelperSinonTests;
}(testModule_1.TestModule));
exports.AugmentationHelperSinonTests = AugmentationHelperSinonTests;
(new AugmentationHelperTests()).runTests();
(new AugmentationHelperSinonTests()).runTests();
