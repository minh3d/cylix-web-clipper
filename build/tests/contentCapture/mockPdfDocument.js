"use strict";
var pdfDataUrls_1 = require("../clipperUI/components/previewViewer/pdfDataUrls");
var MockPdfValues;
(function (MockPdfValues) {
    MockPdfValues.pageDataUrls = pdfDataUrls_1.pdfDataUrls;
    MockPdfValues.pageDataUrlsMap = {
        "0": pdfDataUrls_1.pdfDataUrls[0],
        "1": pdfDataUrls_1.pdfDataUrls[1],
        "2": pdfDataUrls_1.pdfDataUrls[2]
    };
    MockPdfValues.dimensions = [{
            width: 1, height: 1
        }, {
            width: 2, height: 2
        }, {
            width: 3, height: 3
        }];
    MockPdfValues.byteLength = 5;
    MockPdfValues.data = new Uint8Array([10, 20, 30]);
})(MockPdfValues = exports.MockPdfValues || (exports.MockPdfValues = {}));
var MockPdfDocument = (function () {
    function MockPdfDocument() {
        // There are 3 urls
        this.pageDataUrls = MockPdfValues.pageDataUrls;
        this.pageViewportDimensions = MockPdfValues.dimensions;
    }
    MockPdfDocument.prototype.numPages = function () {
        return MockPdfValues.pageDataUrls.length;
    };
    MockPdfDocument.prototype.getByteLength = function () {
        return Promise.resolve(MockPdfValues.byteLength);
    };
    MockPdfDocument.prototype.getData = function () {
        return Promise.resolve(MockPdfValues.data);
    };
    MockPdfDocument.prototype.getPageListAsDataUrls = function (pageIndexes) {
        var dataUrls = [];
        for (var i = 0; i < pageIndexes.length; i++) {
            dataUrls.push(this.pageDataUrls[pageIndexes[i]]);
        }
        return Promise.resolve(dataUrls);
    };
    MockPdfDocument.prototype.getPageAsDataUrl = function (pageIndex) {
        return Promise.resolve(this.pageDataUrls[pageIndex]);
    };
    MockPdfDocument.prototype.getAllPageViewportDimensions = function () {
        return Promise.resolve(this.pageViewportDimensions);
    };
    MockPdfDocument.prototype.getPageListViewportDimensions = function (pageIndexes) {
        var dimensions = [];
        for (var i = 0; i < pageIndexes.length; i++) {
            dimensions.push(this.pageViewportDimensions[pageIndexes[i]]);
        }
        return Promise.resolve(dimensions);
    };
    MockPdfDocument.prototype.getPageViewportDimensions = function (pageIndex) {
        return Promise.resolve(this.pageViewportDimensions[pageIndex]);
    };
    return MockPdfDocument;
}());
exports.MockPdfDocument = MockPdfDocument;
