"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var frontEndGlobals_1 = require("../../scripts/clipperUI/frontEndGlobals");
var bookmarkHelper_1 = require("../../scripts/contentCapture/bookmarkHelper");
var objectUtils_1 = require("../../scripts/objectUtils");
var stubSessionLogger_1 = require("../../scripts/logging/stubSessionLogger");
var testModule_1 = require("../testModule");
var BookmarkHelperTests = (function (_super) {
    __extends(BookmarkHelperTests, _super);
    function BookmarkHelperTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    BookmarkHelperTests.prototype.module = function () {
        return "bookmarkHelper";
    };
    BookmarkHelperTests.prototype.beforeEach = function () {
        frontEndGlobals_1.Clipper.logger = new stubSessionLogger_1.StubSessionLogger();
    };
    BookmarkHelperTests.prototype.tests = function () {
        test("bookmarkPage rejects when url is undefined or empty", function (assert) {
            var done = assert.async();
            var metadata = TestHelper.createListOfMetaTags([TestHelper.StandardMetadata.PrimaryDescription, TestHelper.StandardMetadata.PrimaryThumbnail]);
            bookmarkHelper_1.BookmarkHelper.bookmarkPage(undefined, TestHelper.Content.testPageTitle, metadata).then(function () {
                ok(false, "undefined url should not resolve");
                done();
            }, function () {
                bookmarkHelper_1.BookmarkHelper.bookmarkPage("", TestHelper.Content.testPageTitle, metadata).then(function () {
                    ok(false, "empty url should not resolve");
                })["catch"](function () {
                    ok(true, "empty url should reject");
                }).then(function () {
                    done();
                });
            });
        });
        test("bookmarkPage returns complete BookmarkResult when fully-formed list of metadata exists", function (assert) {
            var done = assert.async();
            var expectedResult = {
                url: TestHelper.Content.testBookmarkUrl,
                title: TestHelper.Content.testPageTitle,
                description: TestHelper.Content.testDescriptionValue
            };
            var metadata = TestHelper.createListOfMetaTags([TestHelper.StandardMetadata.PrimaryDescription]);
            bookmarkHelper_1.BookmarkHelper.bookmarkPage(expectedResult.url, TestHelper.Content.testPageTitle, metadata).then(function (result) {
                strictEqual(result.url, expectedResult.url, "bookmarked url is incorrect");
                strictEqual(result.description, expectedResult.description, "bookmarked description is incorrect");
                strictEqual(result.thumbnailSrc, expectedResult.thumbnailSrc, "bookmarked thumbnail source is incorrect");
            })["catch"](function (error) {
                ok(false, "fully-formed list of metadata should not reject. error: " + error);
            }).then(function () {
                done();
            });
        });
        test("bookmarkPage returns BookmarkResult with just a url when list of metadata does not exist", function (assert) {
            var done = assert.async();
            var expectedResult = {
                url: TestHelper.Content.testBookmarkUrl,
                title: TestHelper.Content.testPageTitle
            };
            bookmarkHelper_1.BookmarkHelper.bookmarkPage(expectedResult.url, TestHelper.Content.testPageTitle, undefined).then(function (result) {
                strictEqual(result.url, expectedResult.url, "bookmarked url is incorrect");
                strictEqual(result.description, undefined, "bookmarked description should be undefined. actual: '" + result.description + "'");
                strictEqual(result.thumbnailSrc, undefined, "bookmarked thumbnail source should be undefined. actual: '" + result.thumbnailSrc + "'");
            })["catch"](function (error) {
                ok(false, "undefined list of metadata should not reject. error: " + error);
            }).then(function () {
                done();
            });
        });
        test("bookmarkPage returns BookmarkResult with just a url when list of metadata is empty", function (assert) {
            var done = assert.async();
            var expectedResult = {
                url: TestHelper.Content.testBookmarkUrl,
                title: TestHelper.Content.testPageTitle
            };
            bookmarkHelper_1.BookmarkHelper.bookmarkPage(expectedResult.url, TestHelper.Content.testPageTitle, new Array()).then(function (result) {
                strictEqual(result.url, expectedResult.url, "bookmarked url is incorrect");
                strictEqual(result.description, undefined, "bookmarked description should be undefined. actual: '" + result.description + "'");
                strictEqual(result.thumbnailSrc, undefined, "bookmarked thumbnail source should be undefined. actual: '" + result.thumbnailSrc + "'");
            })["catch"](function (error) {
                ok(false, "empty list of metadata should not reject. error: " + error);
            }).then(function () {
                done();
            });
        });
        test("bookmarkPage returns BookmarkResult with just a url when list of metadata contains no useful information", function (assert) {
            var done = assert.async();
            var expectedResult = {
                url: TestHelper.Content.testBookmarkUrl,
                title: TestHelper.Content.testPageTitle
            };
            var metadata = TestHelper.createListOfMetaTags([TestHelper.StandardMetadata.Fake]);
            bookmarkHelper_1.BookmarkHelper.bookmarkPage(expectedResult.url, TestHelper.Content.testPageTitle, metadata).then(function (result) {
                strictEqual(result.url, expectedResult.url, "bookmarked url is incorrect");
                strictEqual(result.description, undefined, "bookmarked description should be undefined. actual: '" + result.description + "'");
                strictEqual(result.thumbnailSrc, undefined, "bookmarked thumbnail source should be undefined. actual: '" + result.thumbnailSrc + "'");
            })["catch"](function (error) {
                ok(false, "useless list of metadata should not reject. error: " + error);
            }).then(function () {
                done();
            });
        });
        test("getPrimaryDescription returns undefined if og:description meta tag does not exist", function () {
            strictEqual(bookmarkHelper_1.BookmarkHelper.getPrimaryDescription(undefined), undefined, "undefined list of meta tags should return undefined. actual: '" + bookmarkHelper_1.BookmarkHelper.getPrimaryDescription(undefined));
            var metaTags = new Array();
            strictEqual(bookmarkHelper_1.BookmarkHelper.getPrimaryDescription(metaTags), undefined, "empty list of meta tags should return undefined");
            metaTags = TestHelper.createListOfMetaTags([
                TestHelper.StandardMetadata.FallbackDescription,
                TestHelper.StandardMetadata.FallbackDescription,
                TestHelper.StandardMetadata.FallbackThumbnail,
                TestHelper.StandardMetadata.FallbackThumbnail,
                TestHelper.StandardMetadata.PrimaryThumbnail
            ]);
            var result = bookmarkHelper_1.BookmarkHelper.getPrimaryDescription(metaTags);
            strictEqual(result, undefined, "missing og:description should return undefined. actual: '" + result + "'");
        });
        test("getPrimaryDescription returns content of og:description meta tag", function () {
            var metaTags = TestHelper.createListOfMetaTags([
                TestHelper.StandardMetadata.PrimaryDescription
            ]);
            strictEqual(bookmarkHelper_1.BookmarkHelper.getPrimaryDescription(metaTags).description, TestHelper.Content.testDescriptionValue, "description is incorrect");
        });
        test("getFallbackDescription returns undefined if fallback description meta tags do not exist", function () {
            var metaTags = TestHelper.createListOfMetaTags([
                TestHelper.StandardMetadata.PrimaryDescription,
                TestHelper.StandardMetadata.PrimaryThumbnail,
                TestHelper.StandardMetadata.Fake,
                TestHelper.StandardMetadata.FallbackThumbnail
            ]);
            var result = bookmarkHelper_1.BookmarkHelper.getFallbackDescription(metaTags);
            strictEqual(bookmarkHelper_1.BookmarkHelper.getFallbackDescription(metaTags), undefined, "missing fallback description should return undefined. actual: '" + result + "'");
        });
        test("getFallbackDescription returns content if fallback description meta tags with content exist", function () {
            var metaTags = new Array();
            // add all fallback description attributes to tag list
            for (var iFallback = 0; iFallback < bookmarkHelper_1.BookmarkHelper.fallbackDescriptionKeyValuePairs.length; iFallback++) {
                metaTags.push(TestHelper.createListOfMetaTags([
                    TestHelper.StandardMetadata.FallbackDescription
                ], iFallback)[0]);
            }
            var assertCounter = 0;
            while (metaTags.length > 0) {
                var descResult = bookmarkHelper_1.BookmarkHelper.getFallbackDescription(metaTags);
                var expectedDesc = TestHelper.Content.fallbackDescContentPrefix + assertCounter;
                strictEqual(descResult.description, expectedDesc, "content is incorrect - is fallback order incorrect?");
                assertCounter++;
                if (metaTags.length > 1) {
                    // remove content and make sure it falls back to next in line
                    metaTags[0].removeAttribute("content");
                    descResult = bookmarkHelper_1.BookmarkHelper.getFallbackDescription(metaTags);
                    expectedDesc = TestHelper.Content.fallbackDescContentPrefix + assertCounter;
                    strictEqual(descResult.description, expectedDesc, "content is incorrect - are we incorrectly falling back to undefined content?");
                }
                metaTags.shift();
            }
        });
        test("getPrimaryThumbnailSrc returns undefined if og:image meta tag does not exist", function () {
            strictEqual(bookmarkHelper_1.BookmarkHelper.getPrimaryThumbnailSrc(undefined), undefined, "undefined list of meta tags should return undefined");
            var metaTags = new Array();
            strictEqual(bookmarkHelper_1.BookmarkHelper.getPrimaryThumbnailSrc(metaTags), undefined, "empty list of meta tags should return undefined");
            metaTags = TestHelper.createListOfMetaTags([
                TestHelper.StandardMetadata.FallbackDescription,
                TestHelper.StandardMetadata.FallbackDescription,
                TestHelper.StandardMetadata.FallbackThumbnail,
                TestHelper.StandardMetadata.FallbackThumbnail,
                TestHelper.StandardMetadata.PrimaryDescription
            ]);
            var result = bookmarkHelper_1.BookmarkHelper.getPrimaryThumbnailSrc(metaTags);
            strictEqual(result, undefined, "missing og:image should return undefined. actual: '" + result + "'");
        });
        test("getPrimaryThumbnailSrc returns content of og:image meta tag", function () {
            var metaTags = TestHelper.createListOfMetaTags([
                TestHelper.StandardMetadata.PrimaryThumbnail
            ]);
            strictEqual(bookmarkHelper_1.BookmarkHelper.getPrimaryThumbnailSrc(metaTags).thumbnailSrc, TestHelper.Content.testThumbnailSrcValue, "thumbnail source is incorrect");
        });
        test("getFallbackThumbnailSrc returns undefined if fallback thumbnail src meta tags do not exist", function () {
            var metaTags = TestHelper.createListOfMetaTags([
                TestHelper.StandardMetadata.PrimaryDescription,
                TestHelper.StandardMetadata.PrimaryThumbnail,
                TestHelper.StandardMetadata.Fake,
                TestHelper.StandardMetadata.FallbackDescription
            ]);
            var result = bookmarkHelper_1.BookmarkHelper.getFallbackThumbnailSrc(metaTags);
            strictEqual(result, undefined, "missing fallback thumbnail src should return undefined. actual: '" + result + "'");
        });
        test("getFallbackThumbnailSrc returns content if fallback thumbnail src meta tags with content exist", function () {
            var metaTags = new Array();
            // add all fallback thumbnail src attributes to tag list
            for (var iFallback = 0; iFallback < bookmarkHelper_1.BookmarkHelper.fallbackThumbnailKeyValuePairs.length; iFallback++) {
                metaTags.push(TestHelper.createListOfMetaTags([
                    TestHelper.StandardMetadata.FallbackThumbnail
                ], iFallback)[0]);
            }
            var assertCounter = 0;
            while (metaTags.length > 0) {
                var thumbnailSrcResult = bookmarkHelper_1.BookmarkHelper.getFallbackThumbnailSrc(metaTags);
                var expectedThumbnailSrc = TestHelper.Content.fallbackThumbnailSrcContentBase + assertCounter;
                strictEqual(thumbnailSrcResult.thumbnailSrc, expectedThumbnailSrc, "content is incorrect - is fallback order incorrect?");
                assertCounter++;
                if (metaTags.length > 1) {
                    // remove content and make sure it falls back to next in line
                    metaTags[0].removeAttribute("content");
                    thumbnailSrcResult = bookmarkHelper_1.BookmarkHelper.getFallbackThumbnailSrc(metaTags);
                    expectedThumbnailSrc = TestHelper.Content.fallbackThumbnailSrcContentBase + assertCounter;
                    strictEqual(thumbnailSrcResult.thumbnailSrc, expectedThumbnailSrc, "content is incorrect - are we incorrectly falling back to undefined content?");
                }
                metaTags.shift();
            }
        });
        test("getFirstImageOnPage returns undefined if image tags do not exist", function () {
            var thumbnailSrcResult = bookmarkHelper_1.BookmarkHelper.getFirstImageOnPage(undefined);
            strictEqual(thumbnailSrcResult, undefined, "undefined list of image elements should return undefined");
            var images = new Array();
            thumbnailSrcResult = bookmarkHelper_1.BookmarkHelper.getFirstImageOnPage(images);
            strictEqual(thumbnailSrcResult, undefined, "empty list of image elements should return undefined");
        });
        test("getFirstImageOnPage returns undefined if image tag without src values exist", function () {
            var images = new Array();
            images.push(TestHelper.createHTMLImageElement(undefined));
            images.push(TestHelper.createHTMLImageElement(""));
            var thumbnailSrcResult = bookmarkHelper_1.BookmarkHelper.getFirstImageOnPage(images);
            strictEqual(thumbnailSrcResult, undefined, "list of image elements without src values should return undefined");
        });
        test("getFirstImageOnPage returns content if image tag with content exist", function () {
            var images = new Array();
            images.push(TestHelper.createHTMLImageElement(TestHelper.Content.fallbackThumbnailSrcContentBase + "0"));
            images.push(TestHelper.createHTMLImageElement(TestHelper.Content.fallbackThumbnailSrcContentBase + "1"));
            var thumbnailSrcResult = bookmarkHelper_1.BookmarkHelper.getFirstImageOnPage(images);
            strictEqual(thumbnailSrcResult.thumbnailSrc, TestHelper.Content.fallbackThumbnailSrcContentBase + "0");
        });
        test("getMetaContent returns undefined if metatags are undefined", function () {
            var result = bookmarkHelper_1.BookmarkHelper.getMetaContent(undefined, bookmarkHelper_1.BookmarkHelper.primaryThumbnailKeyValuePair);
            strictEqual(result, undefined, "actual: " + result);
        });
        test("getMetaContent returns undefined if metatags are empty", function () {
            var result = bookmarkHelper_1.BookmarkHelper.getMetaContent(new Array(), bookmarkHelper_1.BookmarkHelper.primaryThumbnailKeyValuePair);
            strictEqual(result, undefined, "actual: " + result);
        });
        test("getMetaContent returns undefined if metadata is undefined", function () {
            var metaTags = TestHelper.createListOfMetaTags([
                TestHelper.StandardMetadata.Fake
            ]);
            var result = bookmarkHelper_1.BookmarkHelper.getMetaContent(metaTags, undefined);
            strictEqual(result, undefined, "actual: " + result);
        });
        test("getMetaContent returns undefined if metadata is empty", function () {
            var metaTags = TestHelper.createListOfMetaTags([
                TestHelper.StandardMetadata.Fake
            ]);
            var emptyMetadata = { key: bookmarkHelper_1.BookmarkHelper.propertyAttrName, value: "" };
            var result = bookmarkHelper_1.BookmarkHelper.getMetaContent(metaTags, emptyMetadata);
            strictEqual(result, undefined, "actual: " + result);
        });
        test("getMetaContent returns undefined if the metadata exists but the content is undefined", function () {
            // non-standard metadata, so not using TestHelper.createListOfMetaTags
            var metaTags = new Array();
            metaTags.push(TestHelper.createHTMLMetaElement(bookmarkHelper_1.BookmarkHelper.primaryThumbnailKeyValuePair, undefined));
            var result = bookmarkHelper_1.BookmarkHelper.getMetaContent(metaTags, bookmarkHelper_1.BookmarkHelper.primaryThumbnailKeyValuePair);
            strictEqual(result, undefined, "actual: " + result);
        });
        test("getMetaContent returns undefined if the metadata exists but the content is empty", function () {
            // non-standard metadata, so not using TestHelper.createListOfMetaTags
            var metaTags = new Array();
            metaTags.push(TestHelper.createHTMLMetaElement(bookmarkHelper_1.BookmarkHelper.primaryThumbnailKeyValuePair, ""));
            var result = bookmarkHelper_1.BookmarkHelper.getMetaContent(metaTags, bookmarkHelper_1.BookmarkHelper.primaryThumbnailKeyValuePair);
            strictEqual(result, undefined, "actual: " + result);
        });
        test("getMetaContent returns content if it exists", function () {
            var metaTags = TestHelper.createListOfMetaTags([
                TestHelper.StandardMetadata.PrimaryThumbnail
            ]);
            var result = bookmarkHelper_1.BookmarkHelper.getMetaContent(metaTags, bookmarkHelper_1.BookmarkHelper.primaryThumbnailKeyValuePair);
            strictEqual(result, TestHelper.Content.testThumbnailSrcValue, "actual: " + result);
        });
    };
    return BookmarkHelperTests;
}(testModule_1.TestModule));
exports.BookmarkHelperTests = BookmarkHelperTests;
var TestHelper;
(function (TestHelper) {
    var Content;
    (function (Content) {
        Content.testDescriptionValue = "test description";
        Content.testThumbnailSrcValue = "http://www.abc.com/thumbnail.jpg";
        Content.testBookmarkUrl = "https://www.onenote.com";
        Content.fallbackDescContentPrefix = "test fallback description ";
        Content.fallbackThumbnailSrcContentBase = "http://www.abc.com/fallback.jpg?src=";
        Content.testPageTitle = "";
    })(Content = TestHelper.Content || (TestHelper.Content = {}));
    function createHTMLMetaElement(attribute, content) {
        var metaElement = document.createElement("meta");
        metaElement.setAttribute(attribute.key, attribute.value);
        if (content) {
            metaElement.content = content;
        }
        return metaElement;
    }
    TestHelper.createHTMLMetaElement = createHTMLMetaElement;
    function createHTMLImageElement(srcUrl) {
        var imgElement = document.createElement("img");
        if (srcUrl) {
            imgElement.setAttribute(bookmarkHelper_1.BookmarkHelper.srcAttrName, srcUrl);
        }
        return imgElement;
    }
    TestHelper.createHTMLImageElement = createHTMLImageElement;
    var StandardMetadata;
    (function (StandardMetadata) {
        StandardMetadata[StandardMetadata["Fake"] = 0] = "Fake";
        StandardMetadata[StandardMetadata["FallbackDescription"] = 1] = "FallbackDescription";
        StandardMetadata[StandardMetadata["FallbackThumbnail"] = 2] = "FallbackThumbnail";
        StandardMetadata[StandardMetadata["PrimaryDescription"] = 3] = "PrimaryDescription";
        StandardMetadata[StandardMetadata["PrimaryThumbnail"] = 4] = "PrimaryThumbnail";
    })(StandardMetadata = TestHelper.StandardMetadata || (TestHelper.StandardMetadata = {}));
    function createListOfMetaTags(metadataTypes, fallbackIndexer) {
        var metaTags = new Array();
        for (var _i = 0, metadataTypes_1 = metadataTypes; _i < metadataTypes_1.length; _i++) {
            var type = metadataTypes_1[_i];
            switch (type) {
                case StandardMetadata.PrimaryDescription:
                    metaTags.push(TestHelper.createHTMLMetaElement(bookmarkHelper_1.BookmarkHelper.primaryDescriptionKeyValuePair, TestHelper.Content.testDescriptionValue));
                    break;
                case StandardMetadata.PrimaryThumbnail:
                    metaTags.push(TestHelper.createHTMLMetaElement(bookmarkHelper_1.BookmarkHelper.primaryThumbnailKeyValuePair, TestHelper.Content.testThumbnailSrcValue));
                    break;
                case StandardMetadata.Fake:
                    var uselessMetadata = {
                        key: bookmarkHelper_1.BookmarkHelper.propertyAttrName,
                        value: "attributeFake"
                    };
                    metaTags.push(TestHelper.createHTMLMetaElement(uselessMetadata, "content fake"));
                    break;
                case StandardMetadata.FallbackDescription:
                    var descIndexer = void 0;
                    if (objectUtils_1.ObjectUtils.isNullOrUndefined(fallbackIndexer)) {
                        // if not provided, get a random fallback description
                        descIndexer = TestHelper.getRandomNumber(bookmarkHelper_1.BookmarkHelper.fallbackDescriptionKeyValuePairs.length - 1);
                    }
                    else {
                        descIndexer = fallbackIndexer;
                    }
                    var descMetadata = bookmarkHelper_1.BookmarkHelper.fallbackDescriptionKeyValuePairs[descIndexer];
                    metaTags.push(TestHelper.createHTMLMetaElement(descMetadata, TestHelper.Content.fallbackDescContentPrefix + descIndexer));
                    break;
                case StandardMetadata.FallbackThumbnail:
                    var thumbnailIndexer = void 0;
                    if (objectUtils_1.ObjectUtils.isNullOrUndefined(fallbackIndexer)) {
                        // if not provided, get a random fallback thumbnail src
                        thumbnailIndexer = TestHelper.getRandomNumber(bookmarkHelper_1.BookmarkHelper.fallbackThumbnailKeyValuePairs.length - 1);
                    }
                    else {
                        thumbnailIndexer = fallbackIndexer;
                    }
                    var thumbnailMetadata = bookmarkHelper_1.BookmarkHelper.fallbackThumbnailKeyValuePairs[thumbnailIndexer];
                    metaTags.push(TestHelper.createHTMLMetaElement(thumbnailMetadata, TestHelper.Content.fallbackThumbnailSrcContentBase + thumbnailIndexer));
                    break;
                default:
                    break;
            }
        }
        return metaTags;
    }
    TestHelper.createListOfMetaTags = createListOfMetaTags;
    function getRandomNumber(maxInclusive) {
        return Math.floor(Math.random() * (maxInclusive + 1));
    }
    TestHelper.getRandomNumber = getRandomNumber;
})(TestHelper || (TestHelper = {}));
(new BookmarkHelperTests()).runTests();
