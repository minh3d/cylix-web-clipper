"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../scripts/constants");
var urlUtils_1 = require("../scripts/urlUtils");
var tooltipType_1 = require("../scripts/clipperUI/tooltipType");
var testModule_1 = require("./testModule");
var UrlUtilsTests = (function (_super) {
    __extends(UrlUtilsTests, _super);
    function UrlUtilsTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    UrlUtilsTests.prototype.module = function () {
        return "urlUtils";
    };
    UrlUtilsTests.prototype.tests = function () {
        test("addUrlQueryValue should add a name/value pair to a url that contains existing (and different) name/value pairs", function () {
            var originalUrl = "https://www.onenote.com/strings?ids=WebClipper.&pizza=cheesy&rice=fried";
            var name = "name";
            var value = "matthew";
            var newUrl = urlUtils_1.UrlUtils.addUrlQueryValue(originalUrl, name, value);
            strictEqual(newUrl, "https://www.onenote.com/strings?ids=WebClipper.&pizza=cheesy&rice=fried&name=matthew");
        });
        test("addUrlQueryValue should add a name/value pair to a url that does not already contain a name/value pair", function () {
            var originalUrl = "http://www.onenote.com/strings";
            var name = "ids";
            var value = "Whatever";
            var newUrl = urlUtils_1.UrlUtils.addUrlQueryValue(originalUrl, name, value);
            strictEqual(newUrl, "http://www.onenote.com/strings?ids=Whatever");
        });
        test("addUrlQueryValue should add a name/value pair correctly if the url contains a fragment and a name/value pair", function () {
            var originalUrl = "https://www.onenote.com/strings?ids=WebClipper.#frag";
            var name = "k";
            var value = "p";
            var newUrl = urlUtils_1.UrlUtils.addUrlQueryValue(originalUrl, name, value);
            strictEqual(newUrl, "https://www.onenote.com/strings?ids=WebClipper.&k=p#frag");
        });
        test("addUrlQueryValue should add a name/value pair correctly if the url contains a fragment but not a name/value pair", function () {
            var originalUrl = "https://www.onenote.com/strings#frag";
            var name = "k";
            var value = "p";
            var newUrl = urlUtils_1.UrlUtils.addUrlQueryValue(originalUrl, name, value);
            strictEqual(newUrl, "https://www.onenote.com/strings?k=p#frag");
        });
        test("addUrlQueryValue should replace the value of an existing name in the query string", function () {
            var originalUrl = "http://www.onenote.com/strings?ids=old";
            var name = "ids";
            var value = "new";
            var newUrl = urlUtils_1.UrlUtils.addUrlQueryValue(originalUrl, name, value);
            strictEqual(newUrl, "http://www.onenote.com/strings?ids=new");
        });
        test("addUrlQueryValue should return the originalUrl parameter when passed an empty url", function () {
            var url = "";
            strictEqual(urlUtils_1.UrlUtils.addUrlQueryValue(url, "name", "value"), url, "The originalUrl should be returned");
        });
        test("addUrlQueryValue should return the originalUrl parameter when passed a null url", function () {
            /* tslint:disable:no-null-keyword */
            var url = null;
            strictEqual(urlUtils_1.UrlUtils.addUrlQueryValue(url, "name", "value"), url, "The originalUrl should be returned");
            /* tslint:enable:no-null-keyword */
        });
        test("addUrlQueryValue should return the originalUrl parameter when passed an undefined url", function () {
            var url = undefined;
            strictEqual(urlUtils_1.UrlUtils.addUrlQueryValue(url, "name", "value"), url, "The originalUrl should be returned");
        });
        test("addUrlQueryValue should return the originalUrl parameter when passed an empty name", function () {
            var url = "http://www.thiswebsite.rules/strings?";
            strictEqual(urlUtils_1.UrlUtils.addUrlQueryValue(url, "", "value"), url, "The originalUrl should be returned");
        });
        test("addUrlQueryValue should return the originalUrl parameter when passed a null name", function () {
            /* tslint:disable:no-null-keyword */
            var url = "http://www.thiswebsite.rules/strings?";
            strictEqual(urlUtils_1.UrlUtils.addUrlQueryValue(url, null, "value"), url, "The originalUrl should be returned");
            /* tslint:enable:no-null-keyword */
        });
        test("addUrlQueryValue should return the originalUrl parameter when passed an undefined name", function () {
            var url = "http://www.thiswebsite.rules/strings?";
            strictEqual(urlUtils_1.UrlUtils.addUrlQueryValue(url, undefined, "value"), url, "The originalUrl should be returned");
        });
        test("addUrlQueryValue should return the originalUrl parameter when passed an empty value", function () {
            var url = "http://www.thiswebsite.rules/strings?";
            strictEqual(urlUtils_1.UrlUtils.addUrlQueryValue(url, "name", ""), url, "The originalUrl should be returned");
        });
        test("addUrlQueryValue should return the originalUrl parameter when passed a null value", function () {
            /* tslint:disable:no-null-keyword */
            var url = "http://www.thiswebsite.rules/strings?";
            strictEqual(urlUtils_1.UrlUtils.addUrlQueryValue(url, "name", null), url, "The originalUrl should be returned");
            /* tslint:enable:no-null-keyword */
        });
        test("addUrlQueryValue should return the originalUrl parameter when passed an undefined value", function () {
            var url = "http://www.thiswebsite.rules/strings?";
            strictEqual(urlUtils_1.UrlUtils.addUrlQueryValue(url, "name", undefined), url, "The originalUrl should be returned");
        });
        test("addUrlQueryValue should camel case the query param key added when told to do so", function () {
            var url = "http://www.thiswebsite.rules?name1=value1";
            var key = "name2";
            strictEqual(urlUtils_1.UrlUtils.addUrlQueryValue(url, key, "value2", true), url + "&Name2=value2", "'" + key + "' should be camel cased in the url");
        });
        test("getFileNameFromUrl should return the file name when the url has a pdf file name", function () {
            var fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("www.website.com/my-file.pdf");
            strictEqual(fileName, "my-file.pdf", "File name should be retrieved from url");
        });
        test("getFileNameFromUrl should return the file name when the url has a doc file name", function () {
            var fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("www.website.com/my-file.doc");
            strictEqual(fileName, "my-file.doc", "File name should be retrieved from url");
        });
        test("getFileNameFromUrl should return the file name when the url has a doc file name and a number in the file name", function () {
            var fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("www.website.com/1my-file5.doc");
            strictEqual(fileName, "1my-file5.doc", "File name should be retrieved from url");
        });
        test("getFileNameFromUrl should return the file name when the url has a doc file name and a number in the extension", function () {
            var fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("www.website.com/my-file.7zip");
            strictEqual(fileName, "my-file.7zip", "File name should be retrieved from url");
        });
        test("getFileNameFromUrl should return the file name when the url has a doc file name while preserving casing", function () {
            var fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("www.website.com/FILE.json");
            strictEqual(fileName, "FILE.json", "File name should be retrieved from url while preserving casing");
        });
        test("getFileNameFromUrl should return the file name when the url has a doc file name and the url is valid but unusual/unexpected", function () {
            var fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("www.website.website/file.json");
            strictEqual(fileName, "file.json", "File name should be retrieved from url");
            fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("http://www.website.com/file.json");
            strictEqual(fileName, "file.json", "File name should be retrieved from url");
            fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("https://www.website.com/file.json");
            strictEqual(fileName, "file.json", "File name should be retrieved from url");
            fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("website.com/file.json");
            strictEqual(fileName, "file.json", "File name should be retrieved from url");
            fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("www.website.com/really/long/url/5/file.json");
            strictEqual(fileName, "file.json", "File name should be retrieved from url");
            fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("www.0800-website.reviews/file.json");
            strictEqual(fileName, "file.json", "File name should be retrieved from url");
        });
        test("getFileNameFromUrl should return undefined when the url has no file name by default", function () {
            var fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("www.website.com/");
            strictEqual(fileName, undefined, "File name should be retrieved from url while preserving casing");
            fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("www.website.com");
            strictEqual(fileName, undefined, "File name should be retrieved from url while preserving casing");
            fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("website.com");
            strictEqual(fileName, undefined, "File name should be retrieved from url while preserving casing");
            fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("wus.www.website.com");
            strictEqual(fileName, undefined, "File name should be retrieved from url while preserving casing");
            fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("wus.www.website.com/pages/5");
            strictEqual(fileName, undefined, "File name should be retrieved from url while preserving casing");
        });
        test("getFileNameFromUrl should return the fallback when the url has no file name when the fallback is specified", function () {
            var fallback = "Fallback.xyz";
            var fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("www.website.com/", fallback);
            strictEqual(fileName, fallback, "File name should be retrieved from url while preserving casing");
            fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("www.website.com", fallback);
            strictEqual(fileName, fallback, "File name should be retrieved from url while preserving casing");
            fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("website.com", fallback);
            strictEqual(fileName, fallback, "File name should be retrieved from url while preserving casing");
            fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("wus.www.website.com", fallback);
            strictEqual(fileName, fallback, "File name should be retrieved from url while preserving casing");
            fileName = urlUtils_1.UrlUtils.getFileNameFromUrl("wus.www.website.com/pages/5", fallback);
            strictEqual(fileName, fallback, "File name should be retrieved from url while preserving casing");
        });
        test("getFileNameFromUrl should return the fallback when passed an empty url", function () {
            var fallback = "Fallback.xyz";
            strictEqual(urlUtils_1.UrlUtils.getFileNameFromUrl("", fallback), fallback, "The fallback should be returned");
        });
        test("getFileNameFromUrl should return the fallback when passed a null url", function () {
            /* tslint:disable:no-null-keyword */
            var fallback = "Fallback.xyz";
            strictEqual(urlUtils_1.UrlUtils.getFileNameFromUrl(null, fallback), fallback, "The fallback should be returned");
            /* tslint:enable:no-null-keyword */
        });
        test("getFileNameFromUrl should return the fallback when passed an undefined url", function () {
            var fallback = "Fallback.xyz";
            strictEqual(urlUtils_1.UrlUtils.getFileNameFromUrl(undefined, fallback), fallback, "The fallback should be returned");
        });
        test("getQueryValue should return the query value in the general case", function () {
            var url = "www.website.com/stuff?q=v";
            strictEqual(urlUtils_1.UrlUtils.getQueryValue(url, "q"), "v");
        });
        test("getQueryValue should return the query value when there is more than one pair", function () {
            var url = "www.website.com/stuff?q=v&q1=v1&q2=v2";
            strictEqual(urlUtils_1.UrlUtils.getQueryValue(url, "q"), "v");
            strictEqual(urlUtils_1.UrlUtils.getQueryValue(url, "q1"), "v1");
            strictEqual(urlUtils_1.UrlUtils.getQueryValue(url, "q2"), "v2");
        });
        test("getQueryValue should return undefined if the key doesn't exist", function () {
            var url = "www.website.com/stuff?q=v";
            strictEqual(urlUtils_1.UrlUtils.getQueryValue(url, "notexist"), undefined);
        });
        test("getQueryValue should return undefined if no keys exist", function () {
            var url = "www.website.com/stuff?";
            strictEqual(urlUtils_1.UrlUtils.getQueryValue(url, "notexist"), undefined);
            url = "www.website.com/stuff";
            strictEqual(urlUtils_1.UrlUtils.getQueryValue(url, "notexist"), undefined);
        });
        test("getQueryValue should return empty string if the key exists but the value doesn't", function () {
            var url = "www.website.com/stuff?q=";
            strictEqual(urlUtils_1.UrlUtils.getQueryValue(url, "q"), "");
        });
        test("getQueryValue should return undefined if any of the parameters are undefined or empty string", function () {
            strictEqual(urlUtils_1.UrlUtils.getQueryValue("www.website.com/stuff?q=v", undefined), undefined);
            strictEqual(urlUtils_1.UrlUtils.getQueryValue(undefined, "q"), undefined);
            strictEqual(urlUtils_1.UrlUtils.getQueryValue("www.website.com/stuff?q=v", ""), undefined);
            strictEqual(urlUtils_1.UrlUtils.getQueryValue("", "q"), undefined);
        });
        test("getQueryValue should return a valid error if an error is specified.", function () {
            var url = "https://www.onenote.com/webclipper/auth?error=access_denied&error_description=AADSTS50000:+There+was+an+error+issuing+a+token.&state=abcdef12-6ac8-41bd-0445-f7ef7a4182e7";
            strictEqual(urlUtils_1.UrlUtils.getQueryValue(url, constants_1.Constants.Urls.QueryParams.error), "access_denied");
        });
        test("getQueryValue should return a valid error description if an O365 error_description is specified.", function () {
            var url = "https://www.onenote.com/webclipper/auth?error=access_denied&error_description=AADSTS50000:+There+was+an+error+issuing+a+token.&state=abcdef12-6ac8-41bd-0445-f7ef7a4182e7";
            strictEqual(urlUtils_1.UrlUtils.getQueryValue(url, constants_1.Constants.Urls.QueryParams.errorDescription), "AADSTS50000: There was an error issuing a token.");
        });
        test("getQueryValue should return a valid error description if an MSA errorDescription is specified.", function () {
            var url = "https://www.onenote.com/webclipper/auth?error=access_denied&errorDescription=AADSTS50000:+There+was+an+error+issuing+a+token.&state=abcdef12-6ac8-41bd-0445-f7ef7a4182e7";
            strictEqual(urlUtils_1.UrlUtils.getQueryValue(url, constants_1.Constants.Urls.QueryParams.errorDescription), "AADSTS50000: There was an error issuing a token.");
        });
        test("onWhiteListedDomain should return true for urls with /yyyy/mm/dd/", function () {
            ok(urlUtils_1.UrlUtils.onWhitelistedDomain("http://www.sdfdsfsdasdsa.com/2014/09/25/garden/when-blogging-becomes-a-slog.html?_r=1"));
        });
        test("onWhiteListedDomain should return true for urls with /yyyy/mm/", function () {
            ok(urlUtils_1.UrlUtils.onWhitelistedDomain("http://www.sdfdsfsdasdsa.com/2014/09/garden/when-blogging-becomes-a-slog.html?_r=1"));
        });
        test("onWhiteListedDomain should return true for urls with /yy/mm/", function () {
            ok(urlUtils_1.UrlUtils.onWhitelistedDomain("http://www.sdfdsfsdasdsa.com/16/09/garden/when-blogging-becomes-a-slog.html?_r=1"));
        });
        test("onWhiteListedDomain should return true for urls with /yyyy/m/", function () {
            ok(urlUtils_1.UrlUtils.onWhitelistedDomain("http://www.sdfdsfsdasdsa.com/2014/1/garden/when-blogging-becomes-a-slog.html?_r=1"));
        });
        test("onWhiteListedDomain should return true for urls dash-seperated dates assuming they include the year, month, and day", function () {
            ok(urlUtils_1.UrlUtils.onWhitelistedDomain("http://www.sdfdsfsdasdsa.com/2014-09-25/garden/when-blogging-becomes-a-slog.html?_r=1"));
        });
        test("onWhiteListedDomain should return false for urls dash-seperated dates assuming they do not include the year, month, and day", function () {
            ok(!urlUtils_1.UrlUtils.onWhitelistedDomain("http://www.sdfdsfsdasdsa.com/2014-09/garden/when-blogging-becomes-a-slog.html?_r=1"));
        });
        test("onWhiteListedDomain should return false for urls with just years", function () {
            ok(!urlUtils_1.UrlUtils.onWhitelistedDomain("http://www.sdfdsfsdasdsa.com/2014/garden/when-blogging-becomes-a-slog.html?_r=1"));
        });
        test("onWhiteListedDomain should return false for live sign-in URL", function () {
            ok(!urlUtils_1.UrlUtils.onWhitelistedDomain("https://login.live.com/oauth20_authorize.srf?client_id=123456-789a-bcde-1234-123456789abc&scope=wl.signin%20wl.basic%20wl.emails%20wl.offline_access%20office.onenote_update&redirect_uri=https://www.onenote.com/webclipper/auth&response_type=code"));
        });
        test("onWhiteListedDomain should return false given undefined or empty string", function () {
            ok(!urlUtils_1.UrlUtils.onWhitelistedDomain(""));
            ok(!urlUtils_1.UrlUtils.onWhitelistedDomain(undefined));
        });
        test("getPathName should return a valid path given a valid URL", function () {
            ok(urlUtils_1.UrlUtils.getPathname("https://www.foo.com/bar/blah"), "/bar/blah");
            ok(urlUtils_1.UrlUtils.getPathname("https://www.youtube.com/watch?v=dQw4w9WgXcQ"), "/watch");
            ok(urlUtils_1.UrlUtils.getPathname("https://www.youtube.com"), "/");
        });
        var tooltipsToTest = [tooltipType_1.TooltipType.Pdf, tooltipType_1.TooltipType.Product, tooltipType_1.TooltipType.Recipe];
        test("checkIfUrlMatchesAContentType should return UNDEFINED for an undefined, null, or empty URL", function () {
            ok(!urlUtils_1.UrlUtils.checkIfUrlMatchesAContentType(undefined, tooltipsToTest));
            /* tslint:disable:no-null-keyword */
            ok(!urlUtils_1.UrlUtils.checkIfUrlMatchesAContentType(null, tooltipsToTest));
            /* tslint:enable:no-null-keyword */
            ok(!urlUtils_1.UrlUtils.checkIfUrlMatchesAContentType("", tooltipsToTest));
        });
        test("checkIfUrlMatchesAContentType for PDF should return UNDEFINED for a valid URL without .pdf at the end", function () {
            ok(!urlUtils_1.UrlUtils.checkIfUrlMatchesAContentType("https://www.fistbump.reviews", tooltipsToTest));
            ok(!urlUtils_1.UrlUtils.checkIfUrlMatchesAContentType("https://fistbumppdfs.reviews", tooltipsToTest));
        });
        test("checkIfUrlMatchesAContentType for PDF should return PDF for a valid URL with .pdf at the end, case insensitive", function () {
            strictEqual(urlUtils_1.UrlUtils.checkIfUrlMatchesAContentType("https://wwww.wen.jen/shipItFresh.pdf", tooltipsToTest), tooltipType_1.TooltipType.Pdf);
            strictEqual(urlUtils_1.UrlUtils.checkIfUrlMatchesAContentType("http://www.orimi.com/pdf-test.PDF", tooltipsToTest), tooltipType_1.TooltipType.Pdf);
        });
        test("checkIfUrlMatchesAContentType for Product should return UNDEFINED for an invalid url", function () {
            ok(!urlUtils_1.UrlUtils.checkIfUrlMatchesAContentType("https://www.onenote.com/clipper", tooltipsToTest));
        });
        test("checkIfUrlMatchesAContentType for Product should return Product for a valid Wal-mart URL", function () {
            strictEqual(urlUtils_1.UrlUtils.checkIfUrlMatchesAContentType("http://www.walmart.com/ip/49424374", tooltipsToTest), tooltipType_1.TooltipType.Product);
        });
        test("checkIfUrlMatchesAContentType for Recipe should return UNDEFINED for a valid url that isnt in the regex match", function () {
            ok(!urlUtils_1.UrlUtils.checkIfUrlMatchesAContentType("https://www.onenote.com/clipper", tooltipsToTest));
        });
        test("checkIfUrlMatchesAContentType for Recipe should return RECIPE for a valid URL", function () {
            strictEqual(urlUtils_1.UrlUtils.checkIfUrlMatchesAContentType("http://www.chowhound.com/recipes/easy-grilled-cheese-31834", tooltipsToTest), tooltipType_1.TooltipType.Recipe);
        });
    };
    return UrlUtilsTests;
}(testModule_1.TestModule));
exports.UrlUtilsTests = UrlUtilsTests;
(new UrlUtilsTests()).runTests();
