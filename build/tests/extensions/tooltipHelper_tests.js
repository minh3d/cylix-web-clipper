"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../scripts/constants");
var tooltipType_1 = require("../../scripts/clipperUI/tooltipType");
var tooltipHelper_1 = require("../../scripts/extensions/tooltipHelper");
var clipperStorageKeys_1 = require("../../scripts/storage/clipperStorageKeys");
var mockStorage_1 = require("../storage/mockStorage");
var testModule_1 = require("../testModule");
var TooltipHelperTests = (function (_super) {
    __extends(TooltipHelperTests, _super);
    function TooltipHelperTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.testType = tooltipType_1.TooltipType.Pdf;
        _this.validTypes = [tooltipType_1.TooltipType.Pdf, tooltipType_1.TooltipType.Product, tooltipType_1.TooltipType.Recipe, tooltipType_1.TooltipType.Video];
        _this.baseTime = new Date("09/27/2016 00:00:00 PM").getTime();
        return _this;
    }
    TooltipHelperTests.prototype.module = function () {
        return "tooltipHelper";
    };
    TooltipHelperTests.prototype.beforeEach = function () {
        this.mockStorage = new mockStorage_1.MockStorage();
        this.tooltipHelper = new tooltipHelper_1.TooltipHelper(this.mockStorage);
    };
    TooltipHelperTests.prototype.tests = function () {
        var _this = this;
        // undefined clipped time, undefined lastSeenTime should return 0
        // null clipped time, null lastSeenTime
        test("Null or undefined passed to getTooltipInformation should throw an Error", function () {
            /* tslint:disable:no-null-keyword */
            throws(function () {
                _this.tooltipHelper.getTooltipInformation(undefined, undefined);
            });
            throws(function () {
                _this.tooltipHelper.getTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.lastSeenTooltipTimeBase, undefined);
            });
            throws(function () {
                _this.tooltipHelper.getTooltipInformation(undefined, _this.testType);
            });
            throws(function () {
                _this.tooltipHelper.getTooltipInformation(null, null);
            });
            throws(function () {
                _this.tooltipHelper.getTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.lastSeenTooltipTimeBase, null);
            });
            throws(function () {
                _this.tooltipHelper.getTooltipInformation(null, _this.testType);
            });
            /* tslint:enable:no-null-keyword */
        });
        test("getTooltipInformation should return 0 for a value that is not in storage", function () {
            var value = _this.tooltipHelper.getTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.lastSeenTooltipTimeBase, _this.testType);
            strictEqual(value, 0);
        });
        test("getTooltipInformation should return 0 for an invalid value", function () {
            var storageKey = tooltipHelper_1.TooltipHelper.getStorageKeyForTooltip(clipperStorageKeys_1.ClipperStorageKeys.lastSeenTooltipTimeBase, _this.testType);
            var expected = "blah";
            _this.mockStorage.setValue(storageKey, expected);
            var value = _this.tooltipHelper.getTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.lastSeenTooltipTimeBase, _this.testType);
            strictEqual(value, 0);
        });
        test("getTooltipInformation should return correct information for a value that is in storage", function () {
            var storageKey = tooltipHelper_1.TooltipHelper.getStorageKeyForTooltip(clipperStorageKeys_1.ClipperStorageKeys.lastSeenTooltipTimeBase, _this.testType);
            var expected = 1234;
            _this.mockStorage.setValue(storageKey, expected.toString());
            var value = _this.tooltipHelper.getTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.lastSeenTooltipTimeBase, _this.testType);
            strictEqual(value, expected);
        });
        test("Null or undefined passed to setTooltipInformation should throw an Error", function () {
            /* tslint:disable:no-null-keyword */
            throws(function () {
                _this.tooltipHelper.setTooltipInformation(undefined, undefined, "");
            });
            throws(function () {
                _this.tooltipHelper.setTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.lastSeenTooltipTimeBase, undefined, "");
            });
            throws(function () {
                _this.tooltipHelper.setTooltipInformation(undefined, _this.testType, "");
            });
            throws(function () {
                _this.tooltipHelper.setTooltipInformation(null, null, "");
            });
            throws(function () {
                _this.tooltipHelper.setTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.lastSeenTooltipTimeBase, null, "");
            });
            throws(function () {
                _this.tooltipHelper.setTooltipInformation(null, _this.testType, "");
            });
            /* tslint:enable:no-null-keyword */
        });
        test("setTooltipInformation should correctly set the key and value when given valid arguments", function () {
            var val = 4134134;
            _this.tooltipHelper.setTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.lastSeenTooltipTimeBase, _this.testType, val.toString());
            var actual = _this.tooltipHelper.getTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.lastSeenTooltipTimeBase, _this.testType);
            strictEqual(actual, val);
        });
        test("Null or undefined passed to tooltipDelayIsOver should throw an Error", function () {
            /* tslint:disable:no-null-keyword */
            throws(function () {
                _this.tooltipHelper.tooltipDelayIsOver(undefined, null);
            });
            throws(function () {
                _this.tooltipHelper.tooltipDelayIsOver(null, null);
            });
            /* tslint:enable:no-null-keyword */
        });
        test("tooltipHasBeenSeenInLastTimePeriod should return FALSE when nothing is in storage", function () {
            ok(!_this.tooltipHelper.tooltipHasBeenSeenInLastTimePeriod(_this.baseTime, _this.testType, constants_1.Constants.Settings.timeBetweenSameTooltip));
        });
        test("tooltipHasBeenSeenInLastTimePeriod should return FALSE when a value is in storage but it is outside the time period", function () {
            _this.setSeenTimeOutsideOfRange(_this.testType, constants_1.Constants.Settings.timeBetweenSameTooltip);
            ok(!_this.tooltipHelper.tooltipHasBeenSeenInLastTimePeriod(_this.baseTime, _this.testType, constants_1.Constants.Settings.timeBetweenSameTooltip));
        });
        test("tooltipHasBeenSeenInLastTimePeriod should return TRUE when a value is in storage and is within the time period", function () {
            _this.setSeenTimeWithinRange(_this.testType, constants_1.Constants.Settings.timeBetweenSameTooltip);
            ok(_this.tooltipHelper.tooltipHasBeenSeenInLastTimePeriod(_this.testType, _this.baseTime, constants_1.Constants.Settings.timeBetweenSameTooltip));
        });
        test("hasAnyTooltipBeenSeenInLastTimePeriod should return FALSE when nothing is in storage", function () {
            ok(!_this.tooltipHelper.hasAnyTooltipBeenSeenInLastTimePeriod(_this.baseTime, _this.validTypes, constants_1.Constants.Settings.timeBetweenDifferentTooltips));
        });
        test("hasAnyTooltipBeenSeenInLastTimePeriod should return TRUE if at least one of the tooltips has a lastSeenTooltipTime in Storage within the time period", function () {
            _this.setSeenTimeWithinRange(_this.testType, constants_1.Constants.Settings.timeBetweenDifferentTooltips);
            ok(_this.tooltipHelper.hasAnyTooltipBeenSeenInLastTimePeriod(_this.baseTime, _this.validTypes, constants_1.Constants.Settings.timeBetweenDifferentTooltips));
        });
        test("tooltipDelayIsOver should return TRUE when nothing in in storage", function () {
            ok(_this.tooltipHelper.tooltipDelayIsOver(_this.testType, _this.baseTime));
        });
        // Have they clipped this content? If so, return FALSE
        test("tooltipDelayIsOver should return FALSE when the user has clipped this content type regardless of when they Clipped it and the rest of the values in storage", function () {
            _this.setClipTimeWithinRange();
            ok(!_this.tooltipHelper.tooltipDelayIsOver(_this.testType, _this.baseTime));
            _this.setClipTimeOutsideOfRange();
            ok(!_this.tooltipHelper.tooltipDelayIsOver(_this.testType, _this.baseTime));
        });
        // Have they seen ANY content? If so, return FALSE
        test("tooltipDelayIsOver should return FALSE when they have seen a tooltip in the last Constants.Settings.timeBetweenSameTooltip period", function () {
            _this.setSeenTimeWithinRange(_this.testType, constants_1.Constants.Settings.timeBetweenSameTooltip);
            ok(!_this.tooltipHelper.tooltipDelayIsOver(_this.testType, _this.baseTime));
        });
        test("tooltipDelayIsOver should return FALSE when they have seen a tooltip (not the same one) in the last Constants.Settings.timeBetweenDifferentTooltipsPeriod", function () {
            _this.setSeenTimeWithinRange(tooltipType_1.TooltipType.Pdf, constants_1.Constants.Settings.timeBetweenDifferentTooltips);
            ok(!_this.tooltipHelper.tooltipDelayIsOver(tooltipType_1.TooltipType.Product, _this.baseTime));
        });
        test("tooltipDelayIsOver should return TRUE if they have NOT seen a different tooltip in the last Constants.Settings.timeBetweenDifferentTooltipsPeriod", function () {
            _this.setSeenTimeOutsideOfRange(tooltipType_1.TooltipType.Product, constants_1.Constants.Settings.timeBetweenDifferentTooltips);
            ok(_this.tooltipHelper.tooltipDelayIsOver(_this.testType, _this.baseTime));
        });
        // Has the user has seen the tooltip more than maxTooltipsShown times? If so, return FALSE
        test("tooltipDelayIsOVer should return FALSE when the user has seen this tooltip more than maxTooltipsShown times", function () {
            _this.setNumTimesTooltipHasBeenSeen(constants_1.Constants.Settings.maximumNumberOfTimesToShowTooltips);
            ok(!_this.tooltipHelper.tooltipDelayIsOver(_this.testType, _this.baseTime));
        });
        test("tooltipDelayIsOver should return TRUE when the user hasn't seen a tooltip in a while, they've never clipped this content, and they haven't gone over the max number of times to see the tooltip", function () {
            _this.setSeenTimeOutsideOfRange(_this.testType, constants_1.Constants.Settings.timeBetweenSameTooltip);
            ok(_this.tooltipHelper.tooltipDelayIsOver(_this.testType, _this.baseTime));
        });
        test("tooltipDelayIsOver should return FALSE when the user has seen a tooltip in the last Constants.Settings.timeBetweenSameTooltip period, no matter the value of lastClippedTime", function () {
            _this.setSeenTimeWithinRange(_this.testType, constants_1.Constants.Settings.timeBetweenSameTooltip);
            ok(!_this.tooltipHelper.tooltipDelayIsOver(_this.testType, _this.baseTime));
            _this.setClipTimeOutsideOfRange();
            ok(!_this.tooltipHelper.tooltipDelayIsOver(_this.testType, _this.baseTime));
            _this.setClipTimeWithinRange();
            ok(!_this.tooltipHelper.tooltipDelayIsOver(_this.testType, _this.baseTime));
        });
        test("tooltipDelayIsOver should return FALSE when the user has clipped a tooltip, no matter the value of lastSeenTime", function () {
            _this.setClipTimeWithinRange();
            ok(!_this.tooltipHelper.tooltipDelayIsOver(_this.testType, _this.baseTime));
            _this.setSeenTimeOutsideOfRange(_this.testType, constants_1.Constants.Settings.timeBetweenSameTooltip);
            ok(!_this.tooltipHelper.tooltipDelayIsOver(_this.testType, _this.baseTime));
            _this.setSeenTimeWithinRange(_this.testType, constants_1.Constants.Settings.timeBetweenSameTooltip);
            ok(!_this.tooltipHelper.tooltipDelayIsOver(_this.testType, _this.baseTime));
        });
    };
    TooltipHelperTests.prototype.setClipTimeWithinRange = function () {
        var timeToSet = this.baseTime - constants_1.Constants.Settings.timeBetweenSameTooltip + 5000;
        this.tooltipHelper.setTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.lastClippedTooltipTimeBase, this.testType, timeToSet.toString());
    };
    TooltipHelperTests.prototype.setClipTimeOutsideOfRange = function () {
        var timeToSet = this.baseTime - constants_1.Constants.Settings.timeBetweenSameTooltip - 5000;
        this.tooltipHelper.setTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.lastClippedTooltipTimeBase, this.testType, timeToSet.toString());
    };
    TooltipHelperTests.prototype.setSeenTimeWithinRange = function (tooltipType, timePeriod) {
        var timeToSet = this.baseTime - timePeriod + 5000;
        this.tooltipHelper.setTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.lastSeenTooltipTimeBase, tooltipType, timeToSet.toString());
    };
    TooltipHelperTests.prototype.setSeenTimeOutsideOfRange = function (tooltipType, timePeriod) {
        var timeToSet = this.baseTime - timePeriod - 5000;
        this.tooltipHelper.setTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.lastSeenTooltipTimeBase, tooltipType, timeToSet.toString());
    };
    TooltipHelperTests.prototype.setNumTimesTooltipHasBeenSeen = function (times) {
        this.tooltipHelper.setTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.numTimesTooltipHasBeenSeenBase, this.testType, times.toString());
    };
    return TooltipHelperTests;
}(testModule_1.TestModule));
exports.TooltipHelperTests = TooltipHelperTests;
(new TooltipHelperTests()).runTests();
