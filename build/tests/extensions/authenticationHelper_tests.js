"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var sinon = require("sinon");
var authenticationHelper_1 = require("../../scripts/extensions/authenticationHelper");
var clipperData_1 = require("../../scripts/storage/clipperData");
var logger_1 = require("../../scripts/logging/logger");
var asyncUtils_1 = require("../asyncUtils");
var testModule_1 = require("../testModule");
var AuthenticationHelperTests = (function (_super) {
    __extends(AuthenticationHelperTests, _super);
    function AuthenticationHelperTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AuthenticationHelperTests.prototype.module = function () {
        return "authenticationHelper";
    };
    AuthenticationHelperTests.prototype.beforeEach = function () {
        this.mockClipperData = sinon.createStubInstance(clipperData_1.ClipperData);
        this.mockLogger = sinon.createStubInstance(logger_1.Logger);
        this.authentationHelper = new authenticationHelper_1.AuthenticationHelper(this.mockClipperData, this.mockLogger);
    };
    AuthenticationHelperTests.prototype.tests = function () {
        /* tslint:disable:no-null-keyword */
        var _this = this;
        test("A valid userInfo should be validated by isValidUserInformation", function () {
            var userInfo = getValidUserInformationJson();
            ok(_this.authentationHelper.isValidUserInformation(userInfo), "Valid userInfo should be validated");
        });
        test("A null userInfo should be invalidated by isValidUserInformation", function () {
            ok(!_this.authentationHelper.isValidUserInformation(null), "Null userInfo should be invalidated");
        });
        test("An undefined userInfo should be invalidated by isValidUserInformation", function () {
            ok(!_this.authentationHelper.isValidUserInformation(undefined), "Undefined userInfo should be invalidated");
        });
        test("A non-json-string userInfo should be invalidated by isValidUserInformation", function () {
            ok(!_this.authentationHelper.isValidUserInformation("{}}"), "Non-json-string userInfo should be invalidated");
        });
        test("Invalid accessToken should be detected by isValidUserInformation", function () {
            var userInfo = getValidUserInformationJson();
            userInfo.accessToken = null;
            ok(!_this.authentationHelper.isValidUserInformation(userInfo), "Null accessToken should be seen as invalid");
            userInfo.accessToken = undefined;
            ok(!_this.authentationHelper.isValidUserInformation(userInfo), "Undefined accessToken should be seen as invalid");
            userInfo.accessToken = "";
            ok(!_this.authentationHelper.isValidUserInformation(userInfo), "Empty accessToken should be seen as invalid");
        });
        test("Invalid accessTokenExpiration should be detected by isValidUserInformation", function () {
            var userInfo = getValidUserInformationJson();
            userInfo.accessTokenExpiration = 0;
            ok(!_this.authentationHelper.isValidUserInformation(userInfo), "0 accessTokenExpiration should be seen as invalid");
            userInfo.accessTokenExpiration = -1;
            ok(!_this.authentationHelper.isValidUserInformation(userInfo), "<0 accessTokenExpiration should be seen as invalid");
        });
        test("Invalid authType should be detected by isValidUserInformation", function () {
            var userInfo = getValidUserInformationJson();
            userInfo.authType = null;
            ok(!_this.authentationHelper.isValidUserInformation(userInfo), "Null authType should be seen as invalid");
            userInfo.authType = undefined;
            ok(!_this.authentationHelper.isValidUserInformation(userInfo), "Undefined authType should be seen as invalid");
            userInfo.authType = "";
            ok(!_this.authentationHelper.isValidUserInformation(userInfo), "Empty authType should be seen as invalid");
        });
        test("Valid cid should be detected by isValidUserInformation", function () {
            var userInfo = getValidUserInformationJson();
            userInfo.cid = null;
            ok(_this.authentationHelper.isValidUserInformation(userInfo), "Null cid should be seen as valid");
            userInfo.cid = undefined;
            ok(_this.authentationHelper.isValidUserInformation(userInfo), "Undefined cid should be seen as valid");
            userInfo.cid = "";
            ok(_this.authentationHelper.isValidUserInformation(userInfo), "Empty cid should be seen as valid");
        });
        test("Valid emailAddress should be detected by isValidUserInformation", function () {
            var userInfo = getValidUserInformationJson();
            userInfo.emailAddress = null;
            ok(_this.authentationHelper.isValidUserInformation(userInfo), "Null emailAddress should be seen as valid");
            userInfo.emailAddress = undefined;
            ok(_this.authentationHelper.isValidUserInformation(userInfo), "Undefined emailAddress should be seen as valid");
            userInfo.emailAddress = "";
            ok(_this.authentationHelper.isValidUserInformation(userInfo), "Empty emailAddress should be seen as valid");
        });
        test("Valid fullName should be detected by isValidUserInformation", function () {
            var userInfo = getValidUserInformationJson();
            userInfo.fullName = null;
            ok(_this.authentationHelper.isValidUserInformation(userInfo), "Null fullName should be seen as valid");
            userInfo.fullName = undefined;
            ok(_this.authentationHelper.isValidUserInformation(userInfo), "Undefined fullName should be seen as valid");
            userInfo.fullName = "";
            ok(_this.authentationHelper.isValidUserInformation(userInfo), "Empty fullName should be seen as valid");
        });
        /* tslint:enable:no-null-keyword */
    };
    return AuthenticationHelperTests;
}(testModule_1.TestModule));
exports.AuthenticationHelperTests = AuthenticationHelperTests;
var AuthenticationHelperSinonTests = (function (_super) {
    __extends(AuthenticationHelperSinonTests, _super);
    function AuthenticationHelperSinonTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.clipperId = "XX-1a23456b-a1b2-12ab-1a2b-12a34b567c8d";
        _this.authUrl = "https://www.onenote.com/webclipper/userinfo?clipperId=" + _this.clipperId;
        return _this;
    }
    AuthenticationHelperSinonTests.prototype.module = function () {
        return "authenticationHelper-sinon";
    };
    AuthenticationHelperSinonTests.prototype.beforeEach = function () {
        this.server = sinon.fakeServer.create();
        this.server.respondImmediately = true;
        asyncUtils_1.AsyncUtils.mockSetTimeout();
        this.mockClipperData = sinon.createStubInstance(clipperData_1.ClipperData);
        this.mockLogger = sinon.createStubInstance(logger_1.Logger);
        this.authentationHelper = new authenticationHelper_1.AuthenticationHelper(this.mockClipperData, this.mockLogger);
    };
    AuthenticationHelperSinonTests.prototype.afterEach = function () {
        this.server.restore();
        asyncUtils_1.AsyncUtils.restoreSetTimeout();
    };
    AuthenticationHelperSinonTests.prototype.tests = function () {
        var _this = this;
        test("retrieveUserInformation resolves the response as a json string if it represents valid user information", function (assert) {
            var done = assert.async();
            _this.server.respondWith("POST", _this.authUrl, [200, { "Content-Type": "application/json" },
                JSON.stringify(getValidUserInformationJson())
            ]);
            _this.authentationHelper.retrieveUserInformation(_this.clipperId).then(function (responsePackage) {
                strictEqual(responsePackage.parsedResponse, JSON.stringify(getValidUserInformationJson()), "The user information should be resolved as a json string");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("retrieveUserInformation resolves the response with no parameter if it returns an empty object", function (assert) {
            var done = assert.async();
            _this.server.respondWith("POST", _this.authUrl, [200, { "Content-Type": "application/json" },
                JSON.stringify({})
            ]);
            _this.authentationHelper.retrieveUserInformation(_this.clipperId).then(function (responsePackage) {
                strictEqual(responsePackage.parsedResponse, JSON.stringify({}), "The response should be an empty JSON Response.");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("retrieveUserInformation rejects the response with error object if the status code is 4XX", function (assert) {
            var done = assert.async();
            var responseMessage = "Something went wrong";
            _this.server.respondWith("POST", _this.authUrl, [404, { "Content-Type": "application/json" },
                responseMessage
            ]);
            _this.authentationHelper.retrieveUserInformation(_this.clipperId).then(function (responsePackage) {
                ok(false, "resolve should not be called");
            }, function (error) {
                var expected = {
                    error: "Unexpected response status",
                    statusCode: 404,
                    responseHeaders: { "Content-Type": "application/json" },
                    response: responseMessage,
                    timeout: 30000
                };
                deepEqual(error, expected, "The error object should be returned in the reject");
            }).then(function () {
                done();
            });
        });
        test("retrieveUserInformation rejects the response with error object if the status code is 5XX", function (assert) {
            var done = assert.async();
            var responseMessage = "Something went wrong on our end";
            _this.server.respondWith("POST", _this.authUrl, [503, { "Content-Type": "application/json" },
                responseMessage
            ]);
            _this.authentationHelper.retrieveUserInformation(_this.clipperId).then(function (responsePackage) {
                ok(false, "resolve should not be called");
            }, function (error) {
                var expected = {
                    error: "Unexpected response status",
                    statusCode: 503,
                    responseHeaders: { "Content-Type": "application/json" },
                    response: responseMessage,
                    timeout: 30000
                };
                deepEqual(error, expected, "The error object should be returned in the reject");
            }).then(function () {
                done();
            });
        });
    };
    return AuthenticationHelperSinonTests;
}(testModule_1.TestModule));
exports.AuthenticationHelperSinonTests = AuthenticationHelperSinonTests;
function getValidUserInformationJson() {
    return {
        accessToken: "abcd",
        accessTokenExpiration: 2000,
        authType: "MSA",
        cid: "cid",
        cookieInRequest: true,
        emailAddress: "me@myemail.xyz",
        fullName: "George"
    };
}
(new AuthenticationHelperTests()).runTests();
(new AuthenticationHelperSinonTests()).runTests();
