"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var extensionBase_1 = require("../../scripts/extensions/extensionBase");
var version_1 = require("../../scripts/versioning/version");
var testModule_1 = require("../testModule");
var ExtensionBaseTests = (function (_super) {
    __extends(ExtensionBaseTests, _super);
    function ExtensionBaseTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ExtensionBaseTests.prototype.module = function () {
        return "extensionBase";
    };
    ExtensionBaseTests.prototype.tests = function () {
        test("shouldCheckForMajorUpdates should return true if the last seen version is less than the current version", function () {
            ok(extensionBase_1.ExtensionBase.shouldCheckForMajorUpdates(new version_1.Version("3.0.9"), new version_1.Version("3.1.0")));
        });
        test("shouldCheckForMajorUpdates should return false if the last seen version is greater than the current version", function () {
            ok(!extensionBase_1.ExtensionBase.shouldCheckForMajorUpdates(new version_1.Version("3.1.1"), new version_1.Version("3.1.0")));
        });
        test("shouldCheckForMajorUpdates should return false if the last seen version is equal to the current version", function () {
            ok(!extensionBase_1.ExtensionBase.shouldCheckForMajorUpdates(new version_1.Version("4.1.10"), new version_1.Version("4.1.10")));
        });
        test("shouldCheckForMajorUpdates should return false if the current version is undefined", function () {
            ok(!extensionBase_1.ExtensionBase.shouldCheckForMajorUpdates(new version_1.Version("4.1.10"), undefined));
        });
        test("shouldCheckForMajorUpdates should return true if the last seen version is undefined", function () {
            ok(extensionBase_1.ExtensionBase.shouldCheckForMajorUpdates(undefined, new version_1.Version("4.1.10")));
        });
        test("shouldCheckForMajorUpdates should return false if both parameters are undefined", function () {
            ok(!extensionBase_1.ExtensionBase.shouldCheckForMajorUpdates(undefined, undefined));
        });
    };
    return ExtensionBaseTests;
}(testModule_1.TestModule));
exports.ExtensionBaseTests = ExtensionBaseTests;
(new ExtensionBaseTests()).runTests();
