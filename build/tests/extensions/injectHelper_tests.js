"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var injectHelper_1 = require("../../scripts/extensions/injectHelper");
var testModule_1 = require("../testModule");
var InjectHelperTests = (function (_super) {
    __extends(InjectHelperTests, _super);
    function InjectHelperTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    InjectHelperTests.prototype.module = function () {
        return "injectHelper";
    };
    InjectHelperTests.prototype.tests = function () {
        test("isKnownUninjectablePage should return true on about:tabs", function () {
            ok(injectHelper_1.InjectHelper.isKnownUninjectablePage("about:tabs"));
        });
        test("isKnownUninjectablePage should return false on a 'regular' site", function () {
            ok(!injectHelper_1.InjectHelper.isKnownUninjectablePage("www.bing.com"));
            ok(!injectHelper_1.InjectHelper.isKnownUninjectablePage("https://www.bing.com"));
            ok(!injectHelper_1.InjectHelper.isKnownUninjectablePage("http://www.bing.com"));
        });
        test("isKnownUninjectablePage should return false on a blank or undefined input", function () {
            ok(!injectHelper_1.InjectHelper.isKnownUninjectablePage(""));
            ok(!injectHelper_1.InjectHelper.isKnownUninjectablePage(undefined));
        });
    };
    return InjectHelperTests;
}(testModule_1.TestModule));
exports.InjectHelperTests = InjectHelperTests;
(new InjectHelperTests()).runTests();
