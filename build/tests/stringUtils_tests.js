"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var operationResult_1 = require("../scripts/operationResult");
var stringUtils_1 = require("../scripts/stringUtils");
var testModule_1 = require("./testModule");
var StringUtilsTests = (function (_super) {
    __extends(StringUtilsTests, _super);
    function StringUtilsTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    StringUtilsTests.prototype.module = function () {
        return "stringUtils";
    };
    StringUtilsTests.prototype.tests = function () {
        test("A blank or undefined page range should return undefined", function () {
            /* tslint:disable */
            strictEqual(stringUtils_1.StringUtils.parsePageRange(undefined).status, operationResult_1.OperationResult.Failed, "parsePageRange(undefined) did not return Status.Failed");
            strictEqual(stringUtils_1.StringUtils.parsePageRange(undefined).result, "", "parsePageRange(undefined) was not undefined");
            strictEqual(stringUtils_1.StringUtils.parsePageRange(null).status, operationResult_1.OperationResult.Failed, "parsePageRange(null) did not return Status.Failed");
            strictEqual(stringUtils_1.StringUtils.parsePageRange(null).result, "", "parsePageRange(null) was not undefined");
            /* tslint:enable */
        });
        test("An empty string should return an empty string", function () {
            var ret = stringUtils_1.StringUtils.parsePageRange("");
            ok(ret.status === operationResult_1.OperationResult.Failed);
            deepEqual(ret.result, "");
            var retTwo = stringUtils_1.StringUtils.parsePageRange("           	 	");
            ok(retTwo.status === operationResult_1.OperationResult.Failed);
            deepEqual(retTwo.result, "");
        });
        test("A string of commas should return an empty string", function () {
            var ret = stringUtils_1.StringUtils.parsePageRange(",,,,,,");
            ok(ret.status === operationResult_1.OperationResult.Failed);
            deepEqual(ret.result, ",,,,,,");
            var retTwo = stringUtils_1.StringUtils.parsePageRange(",, , ");
            ok(retTwo.status === operationResult_1.OperationResult.Failed);
            // The result should be trimmed
            deepEqual(retTwo.result, ",, ,");
        });
        test("A single digit should return a single digit array", function () {
            var ret = stringUtils_1.StringUtils.parsePageRange("1");
            ok(ret.status === operationResult_1.OperationResult.Succeeded);
            deepEqual(ret.result, [1], "A page range of '1' should return [1]");
        });
        test("A comma separated list of digits should return those digits as an array", function () {
            var ret = stringUtils_1.StringUtils.parsePageRange("1,3,5,7");
            ok(ret.status === operationResult_1.OperationResult.Succeeded);
            deepEqual(ret.result, [1, 3, 5, 7], "A page range of '1,3,5,7' should return [1,3,5,7]");
        });
        test("A range ending in a comma should still be legal", function () {
            var ret = stringUtils_1.StringUtils.parsePageRange("1,3,5,7,");
            ok(ret.status === operationResult_1.OperationResult.Succeeded);
            deepEqual(ret.result, [1, 3, 5, 7], "A page range of '1,3,5,7,' should return [1,3,5,7]");
        });
        test("A range ending in multiple commas should still be legal", function () {
            var ret = stringUtils_1.StringUtils.parsePageRange("1,3,5,7,,,");
            ok(ret.status === operationResult_1.OperationResult.Succeeded);
            deepEqual(ret.result, [1, 3, 5, 7], "A page range of '1,3,5,7,,,' should return [1,3,5,7]");
        });
        test("A range that has whitespace between commas should still be legal", function () {
            var ret = stringUtils_1.StringUtils.parsePageRange("1,, , 3");
            ok(ret.status === operationResult_1.OperationResult.Succeeded);
            deepEqual(ret.result, [1, 3], "A page range of '1,, , 3' should return [1, 3]");
        });
        test("A range with a hyphen in it should return a range including that interval and both endpoints", function () {
            var ret = stringUtils_1.StringUtils.parsePageRange("1-5");
            ok(ret.status === operationResult_1.OperationResult.Succeeded);
            deepEqual(ret.result, [1, 2, 3, 4, 5], "A page range of '1-5' should return [1,2,3,4,5]");
        });
        test("A range with overlapping numbers should return an array with only one of each digit", function () {
            var ret = stringUtils_1.StringUtils.parsePageRange("1-3,2-4,1,2,3");
            ok(ret.status === operationResult_1.OperationResult.Succeeded);
            deepEqual(ret.result, [1, 2, 3, 4], "A range of '1-3,2-4,1,2,3' should return [1,2,3,4]");
        });
        test("parsePageRange should ignore whitespace on the ends of the string and whitespace in between the digits/ranges", function () {
            var ret = stringUtils_1.StringUtils.parsePageRange("   1 - 5   ,  5  , 8 ");
            ok(ret.status === operationResult_1.OperationResult.Succeeded);
            deepEqual(ret.result, [1, 2, 3, 4, 5, 8]);
        });
        test("A range with negative inputs should return the part with the invalid string", function () {
            var ret = stringUtils_1.StringUtils.parsePageRange("-5--2,-1,0,2");
            ok(ret.status === operationResult_1.OperationResult.Failed);
            strictEqual(ret.result, "-5--2");
        });
        test("A range with non-numeric inputs should return the part with the invalid string", function () {
            var ret = stringUtils_1.StringUtils.parsePageRange("1,a-b,7,d,e,f");
            ok(ret.status === operationResult_1.OperationResult.Failed);
            strictEqual(ret.result, "a-b");
        });
        test("A range that has numbers out of order, such as 1,5-3 should return the part with the invalid string", function () {
            var ret = stringUtils_1.StringUtils.parsePageRange("5-3");
            ok(ret.status === operationResult_1.OperationResult.Failed);
            strictEqual(ret.result, "5-3");
        });
        test("A range that exceeds a range of 2^32 should return the part with the invalid string", function () {
            var ret = stringUtils_1.StringUtils.parsePageRange("1-4294967295");
            ok(ret.status === operationResult_1.OperationResult.Failed);
            strictEqual(ret.result, "1-4294967295");
        });
        test("A number that far exceeds a range of 2^32 should return the number as a string", function () {
            var ret = stringUtils_1.StringUtils.parsePageRange("999999999999999999999999999999", 5);
            ok(ret.status === operationResult_1.OperationResult.Failed);
            strictEqual(ret.result, "999999999999999999999999999999");
        });
        test("A range that far exceeds a range of 2^32 should return the part with the invalid string", function () {
            var ret = stringUtils_1.StringUtils.parsePageRange("1-999999999999999999999999999999");
            ok(ret.status === operationResult_1.OperationResult.Failed);
            strictEqual(ret.result, "1-999999999999999999999999999999");
        });
        test("Ranges that have 0 anywhere in them should be invalid", function () {
            var one = stringUtils_1.StringUtils.parsePageRange("0");
            ok(one.status === operationResult_1.OperationResult.Failed);
            strictEqual(one.result, "0");
            var two = stringUtils_1.StringUtils.parsePageRange("0-5");
            ok(two.status === operationResult_1.OperationResult.Failed);
            strictEqual(two.result, "0-5");
            var three = stringUtils_1.StringUtils.parsePageRange("0-5");
            ok(three.status === operationResult_1.OperationResult.Failed);
            strictEqual(three.result, "0-5");
            var four = stringUtils_1.StringUtils.parsePageRange("1,3,4,0");
            ok(four.status === operationResult_1.OperationResult.Failed);
            strictEqual(four.result, "0");
        });
        test("Validate the range when max range is provided", function () {
            deepEqual(stringUtils_1.StringUtils.parsePageRange("1-5", 10).result, [1, 2, 3, 4, 5], "Given range is within the max bounds of 10.");
            deepEqual(stringUtils_1.StringUtils.parsePageRange("1,3,5,6,8", 9).result, [1, 3, 5, 6, 8], "Given range is within the max range of 9");
            var one = stringUtils_1.StringUtils.parsePageRange("1-13", 10);
            strictEqual(one.result, "1-13", "Given range is outside of the max bounds of 10.");
            var two = stringUtils_1.StringUtils.parsePageRange("1,3,5,6,8", 2);
            strictEqual(two.result, "3", "Given range is outside the max bounds of 2");
        });
        test("getBatchedPageTitle should return a title of the form [nameOfDocument]: Page [i + 1]", function () {
            strictEqual(stringUtils_1.StringUtils.getBatchedPageTitle("", -3), ": Page -2");
            strictEqual(stringUtils_1.StringUtils.getBatchedPageTitle("document", 0), "document: Page 1");
        });
    };
    return StringUtilsTests;
}(testModule_1.TestModule));
exports.StringUtilsTests = StringUtilsTests;
(new StringUtilsTests()).runTests();
