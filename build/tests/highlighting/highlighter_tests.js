"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var highlighter_1 = require("../../scripts/highlighting/highlighter");
var mithrilUtils_1 = require("../mithrilUtils");
var testModule_1 = require("../testModule");
var HighlighterTests = (function (_super) {
    __extends(HighlighterTests, _super);
    function HighlighterTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    HighlighterTests.prototype.module = function () {
        return "highlighter";
    };
    HighlighterTests.prototype.tests = function () {
        test("The previous highlighter should be disabled when a new one is reconstructed", function () {
            var textHighlighter = highlighter_1.Highlighter.reconstructInstance(mithrilUtils_1.MithrilUtils.getFixture(), {
                enabled: true
            });
            ok(textHighlighter.isEnabled(), "The first textHighlighter instance should be enabled");
            highlighter_1.Highlighter.reconstructInstance(mithrilUtils_1.MithrilUtils.getFixture(), {});
            ok(!textHighlighter.isEnabled(), "The second textHighlighter instance should be enabled");
        });
    };
    return HighlighterTests;
}(testModule_1.TestModule));
exports.HighlighterTests = HighlighterTests;
(new HighlighterTests()).runTests();
