"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var logger_1 = require("../../scripts/logging/logger");
var MockLogger = (function (_super) {
    __extends(MockLogger, _super);
    function MockLogger() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MockLogger.prototype.logEvent = function (event) { };
    MockLogger.prototype.logFailure = function (label, failureType, failureInfo, id) { };
    MockLogger.prototype.logUserFunnel = function (label) { };
    MockLogger.prototype.logSessionStart = function () { };
    MockLogger.prototype.logSessionEnd = function (endTrigger) { };
    MockLogger.prototype.logTrace = function (label, level, message) { };
    MockLogger.prototype.pushToStream = function (label, value) { };
    MockLogger.prototype.logClickEvent = function (clickId) { };
    MockLogger.prototype.setContextProperty = function (key, value) { };
    return MockLogger;
}(logger_1.Logger));
exports.MockLogger = MockLogger;
