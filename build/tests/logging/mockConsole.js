"use strict";
var Log = require("../../scripts/logging/log");
;
var MockConsole = (function () {
    function MockConsole() {
        this.backlog = [];
    }
    MockConsole.prototype.warn = function (message, object) {
        this.backlog.push({
            level: Log.Trace.Level.Warning,
            message: message,
            object: object
        });
    };
    MockConsole.prototype.error = function (message, object) {
        this.backlog.push({
            level: Log.Trace.Level.Error,
            message: message,
            object: object
        });
    };
    MockConsole.prototype.info = function (message, object) {
        this.backlog.push({
            level: Log.Trace.Level.Information,
            message: message,
            object: object
        });
    };
    MockConsole.prototype.log = function (message, object) {
        this.backlog.push({
            level: Log.Trace.Level.None,
            message: message,
            object: object
        });
    };
    return MockConsole;
}());
exports.MockConsole = MockConsole;
