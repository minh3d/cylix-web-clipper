"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var sinon = require("sinon");
var Log = require("../../scripts/logging/log");
var sessionLogger_1 = require("../../scripts/logging/sessionLogger");
var testModule_1 = require("../testModule");
var MockAlwaysTrueContext = (function () {
    function MockAlwaysTrueContext() {
    }
    MockAlwaysTrueContext.prototype.requirementsAreMet = function (requirements) { return true; };
    return MockAlwaysTrueContext;
}());
var MockAlwaysFalseContext = (function () {
    function MockAlwaysFalseContext() {
    }
    MockAlwaysFalseContext.prototype.requirementsAreMet = function (requirements) { return false; };
    return MockAlwaysFalseContext;
}());
var requiredContext = Log.Context.Custom.AppInfoId;
var MockOneRequirementContext = (function () {
    function MockOneRequirementContext() {
    }
    MockOneRequirementContext.prototype.requirementsAreMet = function (requirements) {
        return !!requirements[Log.Context.toString(requiredContext)];
    };
    return MockOneRequirementContext;
}());
var MockSessionLogger = (function (_super) {
    __extends(MockSessionLogger, _super);
    function MockSessionLogger(options) {
        return _super.call(this, options) || this;
    }
    MockSessionLogger.prototype.getUserSessionId = function () { return "abc"; };
    MockSessionLogger.prototype.handleClickEvent = function (clickId) { };
    MockSessionLogger.prototype.handleEvent = function (event) { };
    MockSessionLogger.prototype.handleEventPure = function (event) { };
    MockSessionLogger.prototype.handleSessionStart = function () { };
    MockSessionLogger.prototype.handleSessionEnd = function (endTrigger) { };
    MockSessionLogger.prototype.handleFailure = function (label, failureType, failureInfo, id) { };
    MockSessionLogger.prototype.handleUserFunnel = function (label) { };
    MockSessionLogger.prototype.handleTrace = function (label, level, message) { };
    MockSessionLogger.prototype.handleSetUserSessionId = function (sessionId) { return "usid"; };
    MockSessionLogger.prototype.handleSetContext = function (key, value) { };
    return MockSessionLogger;
}(sessionLogger_1.SessionLogger));
var SessionLoggerTests = (function (_super) {
    __extends(SessionLoggerTests, _super);
    function SessionLoggerTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SessionLoggerTests.prototype.module = function () {
        return "sessionLogger";
    };
    SessionLoggerTests.prototype.beforeEach = function () {
        this.mockAlwaysTrueContext = new MockAlwaysTrueContext();
        this.mockAlwaysFalseContext = new MockAlwaysFalseContext();
        this.mockOneReqContext = new MockOneRequirementContext();
        this.alwaysTrueReqCheckSpy = sinon.spy(this.mockAlwaysTrueContext.requirementsAreMet);
        this.mockAlwaysTrueContext.requirementsAreMet = this.alwaysTrueReqCheckSpy;
        this.alwaysFalseReqCheckSpy = sinon.spy(this.mockAlwaysFalseContext.requirementsAreMet);
        this.mockAlwaysFalseContext.requirementsAreMet = this.alwaysFalseReqCheckSpy;
        this.oneReqCheckSpy = sinon.spy(this.mockOneReqContext.requirementsAreMet);
        this.mockOneReqContext.requirementsAreMet = this.oneReqCheckSpy;
    };
    SessionLoggerTests.prototype.tests = function () {
        var _this = this;
        test("logEvent should call handleEvent if the context requirements are met", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var handleEventSpy = sinon.spy(mockSessionLogger.handleEvent);
            mockSessionLogger.handleEvent = handleEventSpy;
            var baseEvent = new Log.Event.BaseEvent(0);
            mockSessionLogger.logEvent(baseEvent);
            ok(_this.alwaysTrueReqCheckSpy.calledOnce, "The context requirements should be checked");
            ok(handleEventSpy.calledOnce, "handleEvent should be called once");
            ok(handleEventSpy.calledWith(baseEvent), "handleEvent should be passed the same event object");
        });
        test("logEvent should not call handleEvent if the event parameter is undefined", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var handleEventSpy = sinon.spy(mockSessionLogger.handleEvent);
            mockSessionLogger.handleEvent = handleEventSpy;
            var logFailureSpy = sinon.spy(mockSessionLogger.logFailure);
            mockSessionLogger.logFailure = logFailureSpy;
            mockSessionLogger.logEvent(undefined);
            ok(handleEventSpy.notCalled, "handleEvent should not be called");
            ok(logFailureSpy.calledOnce, "logFailure should be called once");
            ok(logFailureSpy.calledWith(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected), "logFailure should be called with InvalidArgument and Unexpected parameters");
        });
        test("logEvent should not call handleEvent if context requirements are not met", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysFalseContext
            });
            var handleEventSpy = sinon.spy(mockSessionLogger.handleEvent);
            mockSessionLogger.handleEvent = handleEventSpy;
            var baseEvent = new Log.Event.BaseEvent(0);
            mockSessionLogger.logEvent(baseEvent);
            ok(_this.alwaysFalseReqCheckSpy.calledOnce, "The context requirements should be checked");
            ok(handleEventSpy.notCalled, "handleEvent should not be called");
        });
        test("logFailure should call handleFailure if the context requirements are met", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var handleFailureSpy = sinon.spy(mockSessionLogger.handleFailure);
            mockSessionLogger.logFailure = handleFailureSpy;
            var failureInfo = { error: "err" };
            var id = "xyz";
            mockSessionLogger.logFailure(0, 0, failureInfo, id);
            ok(handleFailureSpy.calledOnce, "logFailure should be called once");
            ok(handleFailureSpy.calledWith(0, 0, failureInfo, id), "logFailure should be called with the piped parameters");
        });
        test("logFailure should call logFailure again as an invalid method use if label is undefined", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var logFailureSpy = sinon.spy(mockSessionLogger.logFailure);
            mockSessionLogger.logFailure = logFailureSpy;
            mockSessionLogger.logFailure(undefined, 0, { error: "err" }, "xyz");
            ok(logFailureSpy.calledTwice, "logFailure should be called twice (the second time is for the args failure)");
            ok(logFailureSpy.calledWith(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected), "logFailure should be called with InvalidArgument and Unexpected parameters");
        });
        test("logFailure should call logFailure again as an invalid method use if failureType is undefined", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var logFailureSpy = sinon.spy(mockSessionLogger.logFailure);
            mockSessionLogger.logFailure = logFailureSpy;
            mockSessionLogger.logFailure(0, undefined, { error: "err" }, "xyz");
            ok(logFailureSpy.calledTwice, "logFailure should be called once (the second time is for the args failure)");
            ok(logFailureSpy.calledWith(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected), "logFailure should be called with InvalidArgument and Unexpected parameters");
        });
        test("logFailure should not call handleFailure if context requirements are not met", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysFalseContext
            });
            var handleFailureSpy = sinon.spy(mockSessionLogger.handleFailure);
            mockSessionLogger.handleFailure = handleFailureSpy;
            mockSessionLogger.logFailure(0, 0, { error: "err" }, "xyz");
            ok(_this.alwaysFalseReqCheckSpy.calledOnce, "The context requirements should be checked");
            ok(handleFailureSpy.notCalled, "handleFailure should not be called");
        });
        test("logUserFunnel should call handleUserFunnel if the context requirements are met", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var handleUserFunnelSpy = sinon.spy(mockSessionLogger.handleUserFunnel);
            mockSessionLogger.handleUserFunnel = handleUserFunnelSpy;
            mockSessionLogger.logUserFunnel(0);
            ok(_this.alwaysTrueReqCheckSpy.calledOnce, "The context requirements should be checked");
            ok(handleUserFunnelSpy.calledOnce, "handleUserFunnel should be called once");
            ok(handleUserFunnelSpy.calledWith(0), "handleUserFunnel should be passed the same label");
        });
        test("logUserFunnel should not call handleUserFunnel if the label parameter is undefined", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var handleUserFunnelSpy = sinon.spy(mockSessionLogger.handleUserFunnel);
            mockSessionLogger.handleUserFunnel = handleUserFunnelSpy;
            var logFailureSpy = sinon.spy(mockSessionLogger.logFailure);
            mockSessionLogger.logFailure = logFailureSpy;
            mockSessionLogger.logUserFunnel(undefined);
            ok(handleUserFunnelSpy.notCalled, "handleUserFunnel should not be called");
            ok(logFailureSpy.calledOnce, "logFailure should be called once");
            ok(logFailureSpy.calledWith(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected), "logFailure should be called with InvalidArgument and Unexpected parameters");
        });
        test("logUserFunnel should not call handleUserFunnel if context requirements are not met", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysFalseContext
            });
            var handleUserFunnelSpy = sinon.spy(mockSessionLogger.handleUserFunnel);
            mockSessionLogger.handleUserFunnel = handleUserFunnelSpy;
            mockSessionLogger.logUserFunnel(0);
            ok(_this.alwaysFalseReqCheckSpy.calledOnce, "The context requirements should be checked");
            ok(handleUserFunnelSpy.notCalled, "handleUserFunnel should not be called");
        });
        test("logSession should call executeSessionStart and handleSetUserSessionId if the context requirements are met", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var executeSessionStartSpy = sinon.spy(mockSessionLogger.executeSessionStart);
            mockSessionLogger.executeSessionStart = executeSessionStartSpy;
            var handleSetUserSessionIdSpy = sinon.spy(mockSessionLogger.handleSetUserSessionId);
            mockSessionLogger.handleSetUserSessionId = handleSetUserSessionIdSpy;
            mockSessionLogger.logSessionStart();
            ok(_this.alwaysTrueReqCheckSpy.calledOnce, "The context requirements should be checked");
            ok(executeSessionStartSpy.calledOnce, "executeSessionStart should be called once");
            ok(handleSetUserSessionIdSpy.calledOnce, "handleSetUserSessionId should be called once");
        });
        test("logSession should called logFailure if the same session state is set twice in a row", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var logFailureSpy = sinon.spy(mockSessionLogger.logFailure);
            mockSessionLogger.logFailure = logFailureSpy;
            mockSessionLogger.logSessionStart();
            mockSessionLogger.logSessionStart();
            ok(logFailureSpy.calledOnce, "logFailure should be called once");
            ok(logFailureSpy.calledWith(Log.Failure.Label.SessionAlreadySet, Log.Failure.Type.Unexpected), "logFailure should be called with session already set information");
        });
        test("logSession should called logFailure if the first it's called, it's called with Ended (as we assume it's Ended to begin with)", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var handleSessionSpy = sinon.spy(mockSessionLogger.handleSession);
            mockSessionLogger.handleSession = handleSessionSpy;
            var logFailureSpy = sinon.spy(mockSessionLogger.logFailure);
            mockSessionLogger.logFailure = logFailureSpy;
            mockSessionLogger.logSessionEnd(0);
            ok(logFailureSpy.calledOnce, "logFailure should be called once");
            ok(logFailureSpy.calledWith(Log.Failure.Label.SessionAlreadySet, Log.Failure.Type.Unexpected), "logFailure should be called with session already set information");
            ok(handleSessionSpy.notCalled, "handleSession should not be called");
        });
        test("logSession End should log each stream as an event without checking all the usual logEvent requirements (i.e., call the pure version of the function)", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var handleEventPureSpy = sinon.spy(mockSessionLogger.handleEventPure);
            mockSessionLogger.handleEventPure = handleEventPureSpy;
            mockSessionLogger.logSessionStart();
            mockSessionLogger.pushToStream(0, "a");
            mockSessionLogger.pushToStream(1, "b");
            mockSessionLogger.logSessionEnd(0);
            ok(handleEventPureSpy.calledTwice, "handleEventPure should be called once for every stream");
        });
        test("logSession Start should clear the streams", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var handleEventPureSpy = sinon.spy(mockSessionLogger.handleEventPure);
            mockSessionLogger.handleEventPure = handleEventPureSpy;
            mockSessionLogger.logSessionStart();
            mockSessionLogger.pushToStream(0, "a");
            mockSessionLogger.pushToStream(1, "b");
            mockSessionLogger.logSessionEnd(0);
            ok(handleEventPureSpy.calledTwice, "handleEventPure should be called once for every stream");
            mockSessionLogger.logSessionStart();
            mockSessionLogger.logSessionEnd(0);
            ok(handleEventPureSpy.calledTwice, "handleEventPure should not be called additional times");
        });
        test("logSession should not call handleSession if context requirements are not met", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysFalseContext
            });
            var handleSessionSpy = sinon.spy(mockSessionLogger.handleSession);
            mockSessionLogger.handleSession = handleSessionSpy;
            mockSessionLogger.logSessionStart();
            ok(_this.alwaysFalseReqCheckSpy.calledOnce, "The context requirements should be checked");
            ok(handleSessionSpy.notCalled, "handleSession should not be called");
        });
        test("logTrace should call handleTrace if the context requirements are met", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var handleTraceSpy = sinon.spy(mockSessionLogger.handleTrace);
            mockSessionLogger.handleTrace = handleTraceSpy;
            mockSessionLogger.logTrace(0, 0, "hi");
            ok(_this.alwaysTrueReqCheckSpy.calledOnce, "The context requirements should be checked");
            ok(handleTraceSpy.calledOnce, "handleTrace should be called once");
            ok(handleTraceSpy.calledWith(0, 0, "hi"), "handleTrace should be passed the same event object");
        });
        test("logTrace should not call handleTrace if the label parameter is undefined", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var handleTraceSpy = sinon.spy(mockSessionLogger.handleTrace);
            mockSessionLogger.handleTrace = handleTraceSpy;
            var logFailureSpy = sinon.spy(mockSessionLogger.logFailure);
            mockSessionLogger.logFailure = logFailureSpy;
            mockSessionLogger.logTrace(undefined, 0, "hi");
            ok(handleTraceSpy.notCalled, "handleTrace should not be called");
            ok(logFailureSpy.calledOnce, "logFailure should be called once");
            ok(logFailureSpy.calledWith(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected), "logFailure should be called with InvalidArgument and Unexpected parameters");
        });
        test("logTrace should not call handleTrace if the level parameter is undefined", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var handleTraceSpy = sinon.spy(mockSessionLogger.handleTrace);
            mockSessionLogger.handleTrace = handleTraceSpy;
            var logFailureSpy = sinon.spy(mockSessionLogger.logFailure);
            mockSessionLogger.logFailure = logFailureSpy;
            mockSessionLogger.logTrace(0, undefined, "hi");
            ok(handleTraceSpy.notCalled, "handleTrace should not be called");
            ok(logFailureSpy.calledOnce, "logFailure should be called once");
            ok(logFailureSpy.calledWith(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected), "logFailure should be called with InvalidArgument and Unexpected parameters");
        });
        test("logTrace should not call handleTrace if context requirements are not met", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysFalseContext
            });
            var handleTraceSpy = sinon.spy(mockSessionLogger.handleTrace);
            mockSessionLogger.handleTrace = handleTraceSpy;
            mockSessionLogger.logTrace(0, 0);
            ok(_this.alwaysFalseReqCheckSpy.calledOnce, "The context requirements should be checked");
            ok(handleTraceSpy.notCalled, "handleTrace should not be called");
        });
        test("pushToStream should call logFailure if the label is undefined", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var logFailureSpy = sinon.spy(mockSessionLogger.logFailure);
            mockSessionLogger.logFailure = logFailureSpy;
            mockSessionLogger.pushToStream(undefined, "item");
            ok(logFailureSpy.calledOnce, "logFailure should be called once");
            ok(logFailureSpy.calledWith(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected), "logFailure should be called with InvalidArgument and Unexpected parameters");
        });
        test("pushToStream should not call logFailure if the value is undefined", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var logFailureSpy = sinon.spy(mockSessionLogger.logFailure);
            mockSessionLogger.logFailure = logFailureSpy;
            mockSessionLogger.pushToStream(0, undefined);
            ok(logFailureSpy.notCalled, "logFailure should be called once");
        });
        test("logClickEvent should call handleClickEvent and pushToStream if the context requirements are met", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var handleClickEventSpy = sinon.spy(mockSessionLogger.handleClickEvent);
            mockSessionLogger.handleClickEvent = handleClickEventSpy;
            var pushToStreamSpy = sinon.spy(mockSessionLogger.pushToStream);
            mockSessionLogger.pushToStream = pushToStreamSpy;
            mockSessionLogger.logClickEvent("buttonA");
            ok(handleClickEventSpy.calledOnce, "handleClickEvent should be called once");
            ok(handleClickEventSpy.calledWith("buttonA"), "handleClickEvent should be called with the clickId parameter");
            ok(pushToStreamSpy.calledOnce, "pushToStreamSpy should be called once");
            ok(pushToStreamSpy.calledWith(Log.Event.Label.Click, "buttonA"), "pushToStreamSpy should be called with the click event and the clickId parameter");
        });
        test("logClickEvent should call logFailure if the clickId is undefined", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            var logFailureSpy = sinon.spy(mockSessionLogger.logFailure);
            mockSessionLogger.logFailure = logFailureSpy;
            var handleClickEventSpy = sinon.spy(mockSessionLogger.handleClickEvent);
            mockSessionLogger.handleClickEvent = handleClickEventSpy;
            var pushToStreamSpy = sinon.spy(mockSessionLogger.pushToStream);
            mockSessionLogger.pushToStream = pushToStreamSpy;
            mockSessionLogger.logClickEvent(undefined);
            ok(handleClickEventSpy.notCalled, "handleClickEvent should not be called");
            ok(pushToStreamSpy.notCalled, "pushToStream should not be called");
            ok(logFailureSpy.calledOnce, "logFailure should be called once");
            ok(logFailureSpy.calledWith(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected), "logFailure should be called with InvalidArgument and Unexpected parameters");
        });
        test("logClickEvent should not call handleClickEvent and pushToStream if the context requirements are not met", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysFalseContext
            });
            var handleClickEventSpy = sinon.spy(mockSessionLogger.handleClickEvent);
            mockSessionLogger.handleClickEvent = handleClickEventSpy;
            var pushToStreamSpy = sinon.spy(mockSessionLogger.pushToStream);
            mockSessionLogger.pushToStream = pushToStreamSpy;
            mockSessionLogger.logClickEvent("buttonA");
            ok(_this.alwaysFalseReqCheckSpy.calledOnce, "The context requirements should be checked");
            ok(handleClickEventSpy.notCalled, "handleClickEvent should not be called");
            ok(pushToStreamSpy.notCalled, "pushToStream should not be called");
        });
        test("setContextProperty ensures that previous queued events are finally logged after the required context properties are met", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockOneReqContext
            });
            var eventA = new Log.Event.BaseEvent(0);
            var eventB = new Log.Event.BaseEvent(1);
            mockSessionLogger.logEvent(eventA);
            mockSessionLogger.logEvent(eventB);
            var handleEventSpy = sinon.spy(mockSessionLogger.handleEvent);
            mockSessionLogger.handleEvent = handleEventSpy;
            ok(handleEventSpy.notCalled, "handleEvent should not be called yet");
            mockSessionLogger.setContextProperty(requiredContext, "value");
            var expectedContextCheckReqs = {};
            expectedContextCheckReqs[Log.Context.toString(requiredContext)] = "value";
            ok(_this.oneReqCheckSpy.calledWith(expectedContextCheckReqs));
            ok(handleEventSpy.calledTwice, "The two events should be dequeued");
            ok(handleEventSpy.calledWith(eventA), "The first event should be used as a parameter");
            ok(handleEventSpy.calledWith(eventB), "The second event should be used as a parameter");
        });
        test("hasUserInteracted should return false if no click events were logged and true otherwise", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            ok(!mockSessionLogger.hasUserInteracted(), "hasUserInteracted should be false");
            mockSessionLogger.logClickEvent("buttonA");
            ok(mockSessionLogger.hasUserInteracted(), "hasUserInteracted should be true");
            mockSessionLogger.logClickEvent("buttonB");
            ok(mockSessionLogger.hasUserInteracted(), "hasUserInteracted should still be true");
        });
        test("hasUserInteracted should return false after a session ended has been logged", function () {
            var mockSessionLogger = new MockSessionLogger({
                contextStrategy: _this.mockAlwaysTrueContext
            });
            ok(!mockSessionLogger.hasUserInteracted(), "hasUserInteracted should be false");
            mockSessionLogger.logClickEvent("buttonA");
            ok(mockSessionLogger.hasUserInteracted(), "hasUserInteracted should be true");
            // We call Started here as we can't end a session that's already ended
            mockSessionLogger.logSessionStart();
            mockSessionLogger.logSessionEnd(0);
            ok(!mockSessionLogger.hasUserInteracted(), "hasUserInteracted should be false after the session end");
            mockSessionLogger.logClickEvent("buttonA");
            ok(mockSessionLogger.hasUserInteracted(), "hasUserInteracted should be true");
        });
    };
    return SessionLoggerTests;
}(testModule_1.TestModule));
exports.SessionLoggerTests = SessionLoggerTests;
(new SessionLoggerTests()).runTests();
