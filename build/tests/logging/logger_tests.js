"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var sinon = require("sinon");
var Log = require("../../scripts/logging/log");
var testModule_1 = require("../testModule");
var mockLogger_1 = require("./mockLogger");
var LoggerTests = (function (_super) {
    __extends(LoggerTests, _super);
    function LoggerTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LoggerTests.prototype.module = function () {
        return "logger";
    };
    LoggerTests.prototype.tests = function () {
        test("logFailure should be called as a Json parse unexpected failure when logJsonParseUnexpected is called", function () {
            var mockLogger = new mockLogger_1.MockLogger();
            var logFailureSpy = sinon.spy(mockLogger.logFailure);
            mockLogger.logFailure = logFailureSpy;
            mockLogger.logJsonParseUnexpected("{{}");
            ok(logFailureSpy.calledOnce, "logFailure should be called once");
            ok(logFailureSpy.calledWith(Log.Failure.Label.JsonParse, Log.Failure.Type.Unexpected, undefined, "{{}"), "logFailure should be called as a Json parse unexpected failure");
        });
        test("logJsonParseUnexpected should pipe an undefined id to logFailure if called with undefined", function () {
            var mockLogger = new mockLogger_1.MockLogger();
            var logFailureSpy = sinon.spy(mockLogger.logFailure);
            mockLogger.logFailure = logFailureSpy;
            mockLogger.logJsonParseUnexpected(undefined);
            ok(logFailureSpy.calledOnce, "logFailure should be called once");
            ok(logFailureSpy.calledWith(Log.Failure.Label.JsonParse, Log.Failure.Type.Unexpected, undefined, undefined), "logFailure should be called with an undefined id");
        });
        test("logJsonParseUnexpected should pipe an empty string id to logFailure if called with empty string", function () {
            var mockLogger = new mockLogger_1.MockLogger();
            var logFailureSpy = sinon.spy(mockLogger.logFailure);
            mockLogger.logFailure = logFailureSpy;
            mockLogger.logJsonParseUnexpected("");
            ok(logFailureSpy.calledOnce, "logFailure should be called once");
            ok(logFailureSpy.calledWith(Log.Failure.Label.JsonParse, Log.Failure.Type.Unexpected, undefined, ""), "logFailure should be called with an empty string id");
        });
    };
    return LoggerTests;
}(testModule_1.TestModule));
exports.LoggerTests = LoggerTests;
(new LoggerTests()).runTests();
