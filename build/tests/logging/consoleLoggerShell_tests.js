"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var sinon = require("sinon");
var consoleLoggerShell_1 = require("../../scripts/logging/consoleLoggerShell");
var mockConsole_1 = require("./mockConsole");
var testModule_1 = require("../testModule");
var ConsoleLoggerShellTests = (function (_super) {
    __extends(ConsoleLoggerShellTests, _super);
    function ConsoleLoggerShellTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ConsoleLoggerShellTests.prototype.module = function () {
        return "consoleLoggerShell";
    };
    ConsoleLoggerShellTests.prototype.beforeEach = function () {
        this.mockConsole = sinon.createStubInstance(mockConsole_1.MockConsole);
        this.consoleLoggerShell = new consoleLoggerShell_1.ConsoleLoggerShell(this.mockConsole);
    };
    ConsoleLoggerShellTests.prototype.tests = function () {
        var _this = this;
        test("logToConsole should throw an error if no event is passed in", function () {
            throws(function () {
                _this.consoleLoggerShell.logToConsole(undefined);
            }, Error("'event' argument to logToConsole was: undefined"));
        });
        test("logToConsole should always contain the EventName of the event in the message logged to the console", function () {
            var event = {
                EventName: "MyEvent"
            };
            _this.consoleLoggerShell.logToConsole(event);
            ok(_this.mockConsole.log.calledOnce, "The log function should be called once");
            ok(_this.mockConsole.log.calledWith("[MyEvent]", sinon.match(event)), "The log function should be called with the message and object");
        });
        test("If a Level property is present on the event, the message should contain that string", function () {
            var event = {
                EventName: "MyEvent",
                Level: "None"
            };
            _this.consoleLoggerShell.logToConsole(event);
            ok(_this.mockConsole.log.calledOnce, "The log function should be called once");
            ok(_this.mockConsole.log.calledWith("[MyEvent] [None]", sinon.match(event)), "The log message should contain the EventName and Level tag");
        });
        test("If an Event has a Failed Status, the message should be logged as an error", function () {
            var event = {
                EventName: "MyEvent",
                Status: "Failed"
            };
            _this.consoleLoggerShell.logToConsole(event);
            ok(_this.mockConsole.error.calledOnce, "The error function should be called once");
            ok(_this.mockConsole.error.calledWith("[MyEvent] [Error]", sinon.match(event)), "The error message should contain the EventName and Level Error tag");
        });
        test("If an Event has a Level and Message property, that message should be contained in the message logged to console", function () {
            var event = {
                EventName: "MyEvent",
                Level: "None",
                Message: "Hello world"
            };
            _this.consoleLoggerShell.logToConsole(event);
            ok(_this.mockConsole.log.calledOnce, "The log function should be called once");
            ok(_this.mockConsole.log.calledWith("[MyEvent] [None] Hello world", sinon.match(event)), "The log message should contain the EventName, Level tag, and message");
        });
        test("If an Event has a Message property but no Level, the message should simply not include the Level", function () {
            var event = {
                EventName: "MyEvent",
                Message: "Hello world"
            };
            _this.consoleLoggerShell.logToConsole(event);
            ok(_this.mockConsole.log.calledOnce, "The log function should be called once");
            ok(_this.mockConsole.log.calledWith("[MyEvent] Hello world", sinon.match(event)), "The log message should contain the EventName and message");
        });
        test("If the Level property is Warning, the warn function should be called on the console object", function () {
            var event = {
                EventName: "MyEvent",
                Level: "Warning",
                Message: "Hello world"
            };
            _this.consoleLoggerShell.logToConsole(event);
            ok(_this.mockConsole.warn.calledOnce, "The warn function should be called once");
            ok(_this.mockConsole.warn.calledWith("[MyEvent] [Warning] Hello world", sinon.match(event)), "The warn message should contain the EventName and message");
        });
        test("If the Level property is Error, the error function should be called on the console object", function () {
            var event = {
                EventName: "MyEvent",
                Level: "Error",
                Message: "Hello world"
            };
            _this.consoleLoggerShell.logToConsole(event);
            ok(_this.mockConsole.error.calledOnce, "The error function should be called once");
            ok(_this.mockConsole.error.calledWith("[MyEvent] [Error] Hello world", sinon.match(event)), "The error message should contain the EventName and message");
        });
        test("If the Level property is Verbose, the info function should be called on the console object", function () {
            var event = {
                EventName: "MyEvent",
                Level: "Verbose",
                Message: "Hello world"
            };
            _this.consoleLoggerShell.logToConsole(event);
            ok(_this.mockConsole.info.calledOnce, "The info function should be called once");
            ok(_this.mockConsole.info.calledWith("[MyEvent] [Verbose] Hello world", sinon.match(event)), "The info message should contain the EventName and message");
        });
        test("If the Level property is Information, the info function should be called on the console object", function () {
            var event = {
                EventName: "MyEvent",
                Level: "Information",
                Message: "Hello world"
            };
            _this.consoleLoggerShell.logToConsole(event);
            ok(_this.mockConsole.info.calledOnce, "The info function should be called once");
            ok(_this.mockConsole.info.calledWith("[MyEvent] [Information] Hello world", sinon.match(event)), "The info message should contain the EventName and message");
        });
        test("If the Level property is some arbitrary value, the log function should be called on the console object", function () {
            var event = {
                EventName: "MyEvent",
                Level: "asdf",
                Message: "Hello world"
            };
            _this.consoleLoggerShell.logToConsole(event);
            ok(_this.mockConsole.log.calledOnce, "The log function should be called once");
            ok(_this.mockConsole.log.calledWith("[MyEvent] [asdf] Hello world", sinon.match(event)), "The log message should contain the EventName and message");
        });
    };
    return ConsoleLoggerShellTests;
}(testModule_1.TestModule));
exports.ConsoleLoggerShellTests = ConsoleLoggerShellTests;
(new ConsoleLoggerShellTests()).runTests();
