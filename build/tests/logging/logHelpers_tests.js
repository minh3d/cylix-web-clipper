"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Log = require("../../scripts/logging/log");
var logHelpers_1 = require("../../scripts/logging/logHelpers");
var testModule_1 = require("../testModule");
var LogHelpersTests = (function (_super) {
    __extends(LogHelpersTests, _super);
    function LogHelpersTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LogHelpersTests.prototype.module = function () {
        return "logHelpers";
    };
    LogHelpersTests.prototype.tests = function () {
        test("createBaseEvent creates a k-v pair with EventType, Label, Category, and EventName", function () {
            var result = logHelpers_1.LogHelpers.createBaseEventAsJson("subCat", "lab");
            deepEqual(result, {
                EventType: "ReportData",
                Label: "lab",
                Category: "WebClipper.subCat",
                EventName: "WebClipper.subCat.lab"
            });
        });
        test("createClickEvent creates a base k-v pair with subcategory as Click and eventname as the id", function () {
            var result = logHelpers_1.LogHelpers.createClickEventAsJson("buttonA");
            deepEqual(result, {
                EventType: "ReportData",
                Label: "buttonA",
                Category: "WebClipper.Click",
                EventName: "WebClipper.Click.buttonA"
            });
        });
        test("createLogEvent creates a base k-v pair with additional Duration key if a BaseEvent", function () {
            var label = 0;
            var labelAsString = Log.Event.Label[label];
            var result = logHelpers_1.LogHelpers.createLogEventAsJson(new Log.Event.BaseEvent({
                Label: label,
                Duration: 10
            }));
            deepEqual(result, {
                EventType: "ReportData",
                Label: labelAsString,
                Category: "WebClipper.BaseEvent",
                EventName: "WebClipper.BaseEvent." + labelAsString,
                Duration: 10
            });
        });
        test("createLogEvent creates a base k-v pair with additional Duration, Status key if a non-failed PromiseEvent", function () {
            var label = 0;
            var labelAsString = Log.Event.Label[label];
            var result = logHelpers_1.LogHelpers.createLogEventAsJson(new Log.Event.PromiseEvent({
                Label: label,
                Duration: 20,
                LogStatus: Log.Status.Succeeded
            }));
            deepEqual(result, {
                EventType: "ReportData",
                Label: labelAsString,
                Category: "WebClipper.PromiseEvent",
                EventName: "WebClipper.PromiseEvent." + labelAsString,
                Duration: 20,
                Status: "Succeeded"
            });
        });
        test("createLogEvent creates a base k-v pair with additional Duration, Status, FailureInfo, FailureType key if a failed PromiseEvent", function () {
            var label = 0;
            var labelAsString = Log.Event.Label[label];
            var promiseEvent = new Log.Event.PromiseEvent({
                Label: label,
                Duration: 30,
                LogStatus: Log.Status.Failed
            });
            var failureInfo = { error: "err info" };
            promiseEvent.setFailureInfo(failureInfo);
            var failureType = 0;
            var failureTypeAsString = Log.Failure.Type[failureType];
            promiseEvent.setFailureType(0);
            var result = logHelpers_1.LogHelpers.createLogEventAsJson(promiseEvent);
            deepEqual(JSON.stringify(result), JSON.stringify({
                EventType: "ReportData",
                Label: labelAsString,
                Category: "WebClipper.PromiseEvent",
                EventName: "WebClipper.PromiseEvent." + labelAsString,
                Duration: 30,
                Status: "Failed",
                FailureInfo: JSON.stringify(failureInfo),
                FailureType: failureTypeAsString
            }));
        });
        test("createLogEvent creates a base k-v pair with additional Duration, Stream key if a StreamEvent", function () {
            var label = 0;
            var labelAsString = Log.Event.Label[label];
            var stream = ["a", "b"];
            var streamEvent = new Log.Event.StreamEvent({
                Label: label,
                Duration: 30,
                Stream: stream
            });
            var result = logHelpers_1.LogHelpers.createLogEventAsJson(streamEvent);
            deepEqual(JSON.stringify(result), JSON.stringify({
                EventType: "ReportData",
                Label: labelAsString,
                Category: "WebClipper.StreamEvent",
                EventName: "WebClipper.StreamEvent." + labelAsString,
                Duration: 30,
                Stream: JSON.stringify(stream)
            }));
        });
        test("createSetContextEvent creates a base k-v pair with addditional Key, Value properties", function () {
            var key = 0;
            var keyAsString = Log.Context.toString(key);
            var result = logHelpers_1.LogHelpers.createSetContextEventAsJson(key, "val");
            deepEqual(JSON.stringify(result), JSON.stringify({
                EventType: "ReportData",
                Label: "SetContextProperty",
                Category: "WebClipper.BaseEvent",
                EventName: "WebClipper.BaseEvent.SetContextProperty",
                Key: keyAsString,
                Value: "val"
            }));
        });
        test("createFailureEvent creates a base k-v pair with additional FailureType, FailureInfo, Id, StackTrace", function () {
            var label = 0;
            var labelAsString = Log.Failure.Label[label];
            var failureType = 0;
            var failureTypeAsString = Log.Failure.Type[failureType];
            var failureInfo = { error: "err" };
            var id = "id";
            var result = logHelpers_1.LogHelpers.createFailureEventAsJson(label, failureType, failureInfo, id);
            deepEqual(JSON.stringify(result), JSON.stringify({
                EventType: "ReportData",
                Label: labelAsString,
                Category: "WebClipper.Failure",
                EventName: "WebClipper.Failure." + labelAsString,
                FailureType: failureTypeAsString,
                FailureInfo: JSON.stringify(failureInfo),
                Id: id
            }));
        });
        test("createFunnelEvent creates a base k-v pair", function () {
            var label = 0;
            var labelAsString = Log.Funnel.Label[label];
            var result = logHelpers_1.LogHelpers.createFunnelEventAsJson(label);
            deepEqual(JSON.stringify(result), JSON.stringify({
                EventType: "ReportData",
                Label: labelAsString,
                Category: "WebClipper.Funnel",
                EventName: "WebClipper.Funnel." + labelAsString
            }));
        });
        test("createSessionEvent Started creates a base k-v pair", function () {
            var result = logHelpers_1.LogHelpers.createSessionStartEventAsJson();
            deepEqual(JSON.stringify(result), JSON.stringify({
                EventType: "ReportData",
                Label: "Started",
                Category: "WebClipper.Session",
                EventName: "WebClipper.Session.Started"
            }));
        });
        test("createSessionEvent Ended creates a base k-v pair with additional Trigger property", function () {
            var trigger = 0;
            var triggerAsString = Log.Session.EndTrigger[trigger];
            var result = logHelpers_1.LogHelpers.createSessionEndEventAsJson(trigger);
            deepEqual(JSON.stringify(result), JSON.stringify({
                EventType: "ReportData",
                Label: "Ended",
                Category: "WebClipper.Session",
                EventName: "WebClipper.Session.Ended",
                Trigger: triggerAsString
            }));
        });
        test("createTraceEvent creates a base k-v pair with additional Message, Level property if not a Warning", function () {
            var label = 0;
            var labelAsString = Log.Trace.Label[label];
            var level = Log.Trace.Level.Information;
            var levelAsString = Log.Trace.Level[level];
            var message = "msg";
            var result = logHelpers_1.LogHelpers.createTraceEventAsJson(label, level, message);
            deepEqual(JSON.stringify(result), JSON.stringify({
                EventType: "ReportData",
                Label: labelAsString,
                Category: "WebClipper.Trace",
                EventName: "WebClipper.Trace." + labelAsString,
                Message: message,
                Level: levelAsString
            }));
        });
    };
    return LogHelpersTests;
}(testModule_1.TestModule));
exports.LogHelpersTests = LogHelpersTests;
(new LogHelpersTests()).runTests();
