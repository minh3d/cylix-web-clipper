"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var sinon = require("sinon");
var Log = require("../../scripts/logging/log");
var loggerDecorator_1 = require("../../scripts/logging/loggerDecorator");
var testModule_1 = require("../testModule");
var MockLoggerDecorator = (function (_super) {
    __extends(MockLoggerDecorator, _super);
    function MockLoggerDecorator() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MockLoggerDecorator.prototype.outputClickEvent = function (clickId) { };
    MockLoggerDecorator.prototype.outputEvent = function (event) { };
    MockLoggerDecorator.prototype.outputSessionStart = function () { };
    MockLoggerDecorator.prototype.outputSessionEnd = function (endTrigger) { };
    MockLoggerDecorator.prototype.outputFailure = function (label, failureType, failureInfo, id) { };
    MockLoggerDecorator.prototype.outputUserFunnel = function (label) { };
    MockLoggerDecorator.prototype.outputTrace = function (label, level, message) { };
    MockLoggerDecorator.prototype.outputSetContext = function (key, value) { };
    return MockLoggerDecorator;
}(loggerDecorator_1.LoggerDecorator));
var LoggerDecoratorTests = (function (_super) {
    __extends(LoggerDecoratorTests, _super);
    function LoggerDecoratorTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LoggerDecoratorTests.prototype.module = function () {
        return "loggerDecorator";
    };
    LoggerDecoratorTests.prototype.tests = function () {
        test("handleClickEvent should call the child's implementation while logging the click event on the parent", function () {
            var inner = new MockLoggerDecorator();
            var outer = new MockLoggerDecorator({ component: inner });
            var executeClickEventSpy = sinon.spy(inner.executeClickEvent);
            inner.executeClickEvent = executeClickEventSpy;
            outer.logClickEvent("abc");
            ok(executeClickEventSpy.calledOnce, "inner's logClickEvent should be called once");
            ok(executeClickEventSpy.calledWith("abc"), "inner's logClickEvent should be called with the same parameter(s)");
        });
        test("handleEvent should call the child's implementation while logging the event on the parent", function () {
            var inner = new MockLoggerDecorator();
            var outer = new MockLoggerDecorator({ component: inner });
            var logEventSpy = sinon.spy(inner.logEvent);
            inner.logEvent = logEventSpy;
            var event = new Log.Event.BaseEvent(0);
            outer.logEvent(event);
            ok(logEventSpy.calledOnce, "inner's logEvent should be called once");
            ok(logEventSpy.calledWith(event), "inner's logEvent should be called with the same parameter(s)");
        });
        test("handleSession should call the child's executeSessionStart implementation while logging the session on the parent", function () {
            var inner = new MockLoggerDecorator();
            var outer = new MockLoggerDecorator({ component: inner });
            var executeSessionStartSpy = sinon.spy(inner.executeSessionStart);
            inner.executeSessionStart = executeSessionStartSpy;
            outer.logSessionStart();
            ok(executeSessionStartSpy.calledOnce, "inner's executeSessionStartSpy should be called once");
        });
        test("handleFailure should call the child's implementation while logging the session on the parent", function () {
            var inner = new MockLoggerDecorator();
            var outer = new MockLoggerDecorator({ component: inner });
            var logFailureSpy = sinon.spy(inner.logFailure);
            inner.logFailure = logFailureSpy;
            outer.logFailure(0, 0);
            ok(logFailureSpy.calledOnce, "inner's logFailure should be called once");
            ok(logFailureSpy.calledWith(0, 0), "inner's logFailure should be called with the same parameter(s)");
        });
        test("handleUserFunnel should call the child's implementation while logging the session on the parent", function () {
            var inner = new MockLoggerDecorator();
            var outer = new MockLoggerDecorator({ component: inner });
            var logUserFunnelSpy = sinon.spy(inner.logUserFunnel);
            inner.logUserFunnel = logUserFunnelSpy;
            outer.logUserFunnel(0);
            ok(logUserFunnelSpy.calledOnce, "inner's logUserFunnel should be called once");
            ok(logUserFunnelSpy.calledWith(0), "inner's logUserFunnel should be called with the same parameter(s)");
        });
        test("handleTrace should call the child's implementation while logging the session on the parent", function () {
            var inner = new MockLoggerDecorator();
            var outer = new MockLoggerDecorator({ component: inner });
            var logTraceSpy = sinon.spy(inner.logTrace);
            inner.logTrace = logTraceSpy;
            outer.logTrace(0, 0);
            ok(logTraceSpy.calledOnce, "inner's logTrace should be called once");
            ok(logTraceSpy.calledWith(0, 0), "inner's logTrace should be called with the same parameter(s)");
        });
        test("handleSetContext should call the child's implementation while logging the session on the parent", function () {
            var inner = new MockLoggerDecorator();
            var outer = new MockLoggerDecorator({ component: inner });
            var setContextPropertySpy = sinon.spy(inner.setContextProperty);
            inner.setContextProperty = setContextPropertySpy;
            outer.setContextProperty(0, "x");
            ok(setContextPropertySpy.calledOnce, "inner's setContextProperty should be called once");
            ok(setContextPropertySpy.calledWith(0, "x"), "inner's setContextProperty should be called with the same parameter(s)");
        });
    };
    return LoggerDecoratorTests;
}(testModule_1.TestModule));
exports.LoggerDecoratorTests = LoggerDecoratorTests;
(new LoggerDecoratorTests()).runTests();
