"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var sinon = require("sinon");
var constants_1 = require("../../scripts/constants");
var communicator_1 = require("../../scripts/communicator/communicator");
var Log = require("../../scripts/logging/log");
var communicatorLoggerPure_1 = require("../../scripts/logging/communicatorLoggerPure");
var testModule_1 = require("../testModule");
var CommunicatorLoggerPureTests = (function (_super) {
    __extends(CommunicatorLoggerPureTests, _super);
    function CommunicatorLoggerPureTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CommunicatorLoggerPureTests.prototype.module = function () {
        return "communicatorLoggerPure";
    };
    CommunicatorLoggerPureTests.prototype.beforeEach = function () {
        this.stubCommunicator = sinon.createStubInstance(communicator_1.Communicator);
        this.communicatorLoggerPure = new communicatorLoggerPure_1.CommunicatorLoggerPure(this.stubCommunicator);
    };
    CommunicatorLoggerPureTests.prototype.tests = function () {
        var _this = this;
        test("logEvent should call callRemoteFunction on the communicator", function () {
            var event = new Log.Event.BaseEvent(0);
            _this.communicatorLoggerPure.logEvent(event);
            var callRemoteFunctionSpy = _this.stubCommunicator.callRemoteFunction;
            ok(callRemoteFunctionSpy.calledOnce, "callRemoteFunction was called once");
            ok(callRemoteFunctionSpy.calledWith(constants_1.Constants.FunctionKeys.telemetry), "callRemoteFunction should be called with the telemetry function key");
        });
        test("logFailure should call callRemoteFunction on the communicator", function () {
            _this.communicatorLoggerPure.logFailure(0, 0);
            var callRemoteFunctionSpy = _this.stubCommunicator.callRemoteFunction;
            ok(callRemoteFunctionSpy.calledOnce, "callRemoteFunction was called once");
            ok(callRemoteFunctionSpy.calledWith(constants_1.Constants.FunctionKeys.telemetry), "callRemoteFunction should be called with the telemetry function key");
        });
        test("logUserFunnel should call callRemoteFunction on the communicator", function () {
            _this.communicatorLoggerPure.logUserFunnel(0);
            var callRemoteFunctionSpy = _this.stubCommunicator.callRemoteFunction;
            ok(callRemoteFunctionSpy.calledOnce, "callRemoteFunction was called once");
            ok(callRemoteFunctionSpy.calledWith(constants_1.Constants.FunctionKeys.telemetry), "callRemoteFunction should be called with the telemetry function key");
        });
        test("logSession should call callRemoteFunction on the communicator", function () {
            _this.communicatorLoggerPure.logSessionStart();
            var callRemoteFunctionSpy = _this.stubCommunicator.callRemoteFunction;
            ok(callRemoteFunctionSpy.calledOnce, "callRemoteFunction was called once");
            ok(callRemoteFunctionSpy.calledWith(constants_1.Constants.FunctionKeys.telemetry), "callRemoteFunction should be called with the telemetry function key");
        });
        test("logTrace should call callRemoteFunction on the communicator", function () {
            _this.communicatorLoggerPure.logTrace(0, 0);
            var callRemoteFunctionSpy = _this.stubCommunicator.callRemoteFunction;
            ok(callRemoteFunctionSpy.calledOnce, "callRemoteFunction was called once");
            ok(callRemoteFunctionSpy.calledWith(constants_1.Constants.FunctionKeys.telemetry), "callRemoteFunction should be called with the telemetry function key");
        });
        test("pushToStream should call callRemoteFunction on the communicator", function () {
            _this.communicatorLoggerPure.pushToStream(0, "x");
            var callRemoteFunctionSpy = _this.stubCommunicator.callRemoteFunction;
            ok(callRemoteFunctionSpy.calledOnce, "callRemoteFunction was called once");
            ok(callRemoteFunctionSpy.calledWith(constants_1.Constants.FunctionKeys.telemetry), "callRemoteFunction should be called with the telemetry function key");
        });
        test("logClickEvent should call callRemoteFunction on the communicator", function () {
            _this.communicatorLoggerPure.logClickEvent("abc");
            var callRemoteFunctionSpy = _this.stubCommunicator.callRemoteFunction;
            ok(callRemoteFunctionSpy.calledOnce, "callRemoteFunction was called once");
            ok(callRemoteFunctionSpy.calledWith(constants_1.Constants.FunctionKeys.telemetry), "callRemoteFunction should be called with the telemetry function key");
        });
        test("setContextProperty should call callRemoteFunction on the communicator", function () {
            _this.communicatorLoggerPure.setContextProperty("xyz", "abc");
            var callRemoteFunctionSpy = _this.stubCommunicator.callRemoteFunction;
            ok(callRemoteFunctionSpy.calledOnce, "callRemoteFunction was called once");
            ok(callRemoteFunctionSpy.calledWith(constants_1.Constants.FunctionKeys.telemetry), "callRemoteFunction should be called with the telemetry function key");
        });
    };
    return CommunicatorLoggerPureTests;
}(testModule_1.TestModule));
exports.CommunicatorLoggerPureTests = CommunicatorLoggerPureTests;
(new CommunicatorLoggerPureTests()).runTests();
