"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var context_1 = require("../../scripts/logging/context");
var testModule_1 = require("../testModule");
var ContextTests = (function (_super) {
    __extends(ContextTests, _super);
    function ContextTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.requiredProps = ["a", "b", "c"];
        return _this;
    }
    ContextTests.prototype.module = function () {
        return "context";
    };
    ContextTests.prototype.beforeEach = function () {
        this.prod = new context_1.ProductionRequirements(this.requiredProps);
        this.noReq = new context_1.NoRequirements();
    };
    ContextTests.prototype.tests = function () {
        var _this = this;
        test("NoRequirements should always return true regardless of what is put in", function () {
            ok(_this.noReq.requirementsAreMet(undefined), "No requirements returned false, which should never happen");
        });
        test("ProductionRequirements should return false for an empty or undefined argument", function () {
            strictEqual(_this.prod.requirementsAreMet(undefined), false, "ProdRequirements incorrectly returned true for an undefined object");
            strictEqual(_this.prod.requirementsAreMet({}), false, "ProdRequirements incorrectly returned true for an empty object");
        });
        test("ProductionRequirements should return false for a subset of properties that don't meet the full property set", function () {
            var partiallyFilledProps = {
                a: "bar",
                b: "baz"
            };
            strictEqual(_this.prod.requirementsAreMet(partiallyFilledProps), false, "requirementsAreMet incorrectly returned true when the contextProps passed in did not match those injected at construction");
        });
        test("ProductionRequirements should return true for properties that meet exactly the requirements", function () {
            var fullyFilledProps = {
                a: "bar",
                b: "baz",
                c: "foo"
            };
            strictEqual(_this.prod.requirementsAreMet(fullyFilledProps), true, "requirementsAreMet incorrectly returned false when the contextProps passed in matched the requirements injected at construction");
        });
        test("ProductionRequirements should return true for props that exceed the props injected at constructin", function () {
            var excessProps = {
                a: "a",
                b: "b",
                c: "c",
                d: "d"
            };
            strictEqual(_this.prod.requirementsAreMet(excessProps), true, "requirementsAreMet incorrectly returned false when the contextProps passed in exceeded the requirements injected at construction");
        });
    };
    return ContextTests;
}(testModule_1.TestModule));
exports.ContextTests = ContextTests;
(new ContextTests()).runTests();
