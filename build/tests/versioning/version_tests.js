"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var version_1 = require("../../scripts/versioning/version");
var testModule_1 = require("../testModule");
var VersionTests = (function (_super) {
    __extends(VersionTests, _super);
    function VersionTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    VersionTests.prototype.module = function () {
        return "version";
    };
    VersionTests.prototype.tests = function () {
        test("The constructor should throw an error if the parameter does not follow 'int.int.int'", function () {
            throws(function () {
                var version = new version_1.Version("abcd");
            }, Error("version must match 'int.int.int' pattern, but was: abcd"));
            throws(function () {
                var version = new version_1.Version("3123");
            }, Error("version must match 'int.int.int' pattern, but was: 3123"));
            throws(function () {
                var version = new version_1.Version("3.1.0.");
            }, Error("version must match 'int.int.int' pattern, but was: 3.1.0."));
            throws(function () {
                var version = new version_1.Version(".3.1.0");
            }, Error("version must match 'int.int.int' pattern, but was: .3.1.0"));
            throws(function () {
                var version = new version_1.Version("3..0");
            }, Error("version must match 'int.int.int' pattern, but was: 3..0"));
            throws(function () {
                var version = new version_1.Version("3.0");
            }, Error("version must match 'int.int.int' pattern, but was: 3.0"));
            throws(function () {
                var version = new version_1.Version("3.0.a");
            }, Error("version must match 'int.int.int' pattern, but was: 3.0.a"));
            throws(function () {
                var version = new version_1.Version("3.1.0 ");
            }, Error("version must match 'int.int.int' pattern, but was: 3.1.0 "));
            throws(function () {
                var version = new version_1.Version(" 3.1.0");
            }, Error("version must match 'int.int.int' pattern, but was:  3.1.0"));
            throws(function () {
                var version = new version_1.Version("");
            }, Error("version must match 'int.int.int' pattern, but was: "));
            throws(function () {
                var version = new version_1.Version(undefined);
            }, Error("version must match 'int.int.int' pattern, but was: undefined"));
            /* tslint:disable:no-null-keyword */
            throws(function () {
                var version = new version_1.Version(null);
            }, Error("version must match 'int.int.int' pattern, but was: null"));
            /* tslint:enable:no-null-keyword */
        });
        test("Test comparison methods where both versions are instantiated with the same parameters", function () {
            var v1 = new version_1.Version("1.2.3");
            var v2 = new version_1.Version("1.2.3");
            ok(v1.isEqualTo(v2));
            ok(!v1.isGreaterThan(v2));
            ok(v1.isGreaterThanOrEqualTo(v2));
            ok(!v1.isLesserThan(v2));
            ok(v1.isLesserThanOrEqualTo(v2));
            ok(v2.isEqualTo(v1));
            ok(!v2.isGreaterThan(v1));
            ok(v2.isGreaterThanOrEqualTo(v1));
            ok(!v2.isLesserThan(v1));
            ok(v2.isLesserThanOrEqualTo(v1));
        });
        test("Test comparison methods where both the versions are same but had different parameters in the ctor", function () {
            var v1 = new version_1.Version("01.2.003");
            var v2 = new version_1.Version("1.02.3");
            ok(v1.isEqualTo(v2));
            ok(!v1.isGreaterThan(v2));
            ok(v1.isGreaterThanOrEqualTo(v2));
            ok(!v1.isLesserThan(v2));
            ok(v1.isLesserThanOrEqualTo(v2));
            ok(v2.isEqualTo(v1));
            ok(!v2.isGreaterThan(v1));
            ok(v2.isGreaterThanOrEqualTo(v1));
            ok(!v2.isLesserThan(v1));
            ok(v2.isLesserThanOrEqualTo(v1));
        });
        test("Test one version's major number being greater than the other", function () {
            var greater = new version_1.Version("50.10.5");
            var lesser = new version_1.Version("40.99.99");
            ok(!greater.isEqualTo(lesser));
            ok(greater.isGreaterThan(lesser));
            ok(greater.isGreaterThanOrEqualTo(lesser));
            ok(!greater.isLesserThan(lesser));
            ok(!greater.isLesserThanOrEqualTo(lesser));
            ok(!lesser.isEqualTo(greater));
            ok(!lesser.isGreaterThan(greater));
            ok(!lesser.isGreaterThanOrEqualTo(greater));
            ok(lesser.isLesserThan(greater));
            ok(lesser.isLesserThanOrEqualTo(greater));
        });
        test("Test major numbers being the same, and the minor numbers are not", function () {
            var greater = new version_1.Version("50.30.5");
            var lesser = new version_1.Version("50.10.99");
            ok(!greater.isEqualTo(lesser));
            ok(greater.isGreaterThan(lesser));
            ok(greater.isGreaterThanOrEqualTo(lesser));
            ok(!greater.isLesserThan(lesser));
            ok(!greater.isLesserThanOrEqualTo(lesser));
            ok(!lesser.isEqualTo(greater));
            ok(!lesser.isGreaterThan(greater));
            ok(!lesser.isGreaterThanOrEqualTo(greater));
            ok(lesser.isLesserThan(greater));
            ok(lesser.isLesserThanOrEqualTo(greater));
        });
        test("Test major and minor numbers being the same, and the patch numbers are not", function () {
            var greater = new version_1.Version("50.10.5");
            var lesser = new version_1.Version("50.10.1");
            ok(!greater.isEqualTo(lesser));
            ok(greater.isGreaterThan(lesser));
            ok(greater.isGreaterThanOrEqualTo(lesser));
            ok(!greater.isLesserThan(lesser));
            ok(!greater.isLesserThanOrEqualTo(lesser));
            ok(!lesser.isEqualTo(greater));
            ok(!lesser.isGreaterThan(greater));
            ok(!lesser.isGreaterThanOrEqualTo(greater));
            ok(lesser.isLesserThan(greater));
            ok(lesser.isLesserThanOrEqualTo(greater));
        });
        test("Test that we aren't just only comparing the patch", function () {
            var greater = new version_1.Version("50.11.1");
            var lesser = new version_1.Version("50.10.99");
            ok(!greater.isEqualTo(lesser));
            ok(greater.isGreaterThan(lesser));
            ok(greater.isGreaterThanOrEqualTo(lesser));
            ok(!greater.isLesserThan(lesser));
            ok(!greater.isLesserThanOrEqualTo(lesser));
            ok(!lesser.isEqualTo(greater));
            ok(!lesser.isGreaterThan(greater));
            ok(!lesser.isGreaterThanOrEqualTo(greater));
            ok(lesser.isLesserThan(greater));
            ok(lesser.isLesserThanOrEqualTo(greater));
        });
        test("Test that we ignore patch update when that is requested", function () {
            var current = new version_1.Version("3.2.0");
            var nonSignificantUpdate = new version_1.Version("3.2.999");
            ok(current.isEqualTo(nonSignificantUpdate, true /* ignorePatchUpdate */));
            ok(!current.isGreaterThan(nonSignificantUpdate, true /* ignorePatchUpdate */));
            ok(current.isGreaterThanOrEqualTo(nonSignificantUpdate, true /* ignorePatchUpdate */));
            ok(!current.isLesserThan(nonSignificantUpdate, true /* ignorePatchUpdate */));
            ok(current.isLesserThanOrEqualTo(nonSignificantUpdate, true /* ignorePatchUpdate */));
        });
        test("toString should return the major, minor, and patch numbers delimited with periods", function () {
            var version = new version_1.Version("3.1.0");
            strictEqual(version.toString(), "3.1.0");
        });
        test("toString should remove 0s at the start of each number", function () {
            var version = new version_1.Version("03.1.0");
            strictEqual(version.toString(), "3.1.0");
            version = new version_1.Version("3.01.0");
            strictEqual(version.toString(), "3.1.0");
            version = new version_1.Version("3.1.00");
            strictEqual(version.toString(), "3.1.0");
        });
        test("toString should correctly allow more than one digit per number", function () {
            var version = new version_1.Version("31.10.9");
            strictEqual(version.toString(), "31.10.9");
        });
    };
    return VersionTests;
}(testModule_1.TestModule));
exports.VersionTests = VersionTests;
(new VersionTests()).runTests();
