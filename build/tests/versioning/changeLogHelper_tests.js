"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var changeLogHelper_1 = require("../../scripts/versioning/changeLogHelper");
var version_1 = require("../../scripts/versioning/version");
var testModule_1 = require("../testModule");
var ChangeLogHelperTests = (function (_super) {
    __extends(ChangeLogHelperTests, _super);
    function ChangeLogHelperTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.generalUpdates = [{
                version: "3.3.1",
                date: "06/13/2016",
                changes: [{
                        title: "3.3.1:title",
                        description: "3.3.1:description",
                        supportedBrowsers: ["Edge", "Chrome", "Firefox", "Safari", "Bookmarklet"]
                    }]
            }];
        return _this;
    }
    ChangeLogHelperTests.prototype.module = function () {
        return "changeLogHelper";
    };
    ChangeLogHelperTests.prototype.tests = function () {
        var _this = this;
        test("For the case where there is only one update, getUpdatesSinceVersion returns that update if the version parameter is older", function () {
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(_this.generalUpdates, new version_1.Version("2.3.0")), _this.generalUpdates);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(_this.generalUpdates, new version_1.Version("3.2.0")), _this.generalUpdates);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(_this.generalUpdates, new version_1.Version("3.3.0")), _this.generalUpdates);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(_this.generalUpdates, new version_1.Version("3.2.99")), _this.generalUpdates);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(_this.generalUpdates, new version_1.Version("0.0.0")), _this.generalUpdates);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(_this.generalUpdates, new version_1.Version("2.99.99")), _this.generalUpdates);
        });
        test("For the case where there is only one update. getUpdatesSinceVersion returns an empty list if the version parameter is newer", function () {
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(_this.generalUpdates, new version_1.Version("3.3.2")), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(_this.generalUpdates, new version_1.Version("99.99.99")), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(_this.generalUpdates, new version_1.Version("3.99.0")), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(_this.generalUpdates, new version_1.Version("3.4.0")), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(_this.generalUpdates, new version_1.Version("4.0.0")), []);
        });
        test("For the case where there is only one update, getUpdatesSinceVersion returns the update if passed an undefined version", function () {
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(_this.generalUpdates, undefined), _this.generalUpdates);
        });
        test("For the case where there is only one update, getUpdatesSinceVersion returns an empty list if the version parameter is the same", function () {
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(_this.generalUpdates, new version_1.Version(_this.generalUpdates[0].version)), []);
        });
        test("For the case where there is only one update, getUpdatesBetweenVersions returns the update if it is between the lower (exclusive) and upper (inclusive) versions", function () {
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(_this.generalUpdates, new version_1.Version("3.3.0"), new version_1.Version("3.3.2")), _this.generalUpdates);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(_this.generalUpdates, new version_1.Version("3.3.0"), new version_1.Version("3.3.1")), _this.generalUpdates);
        });
        test("For the case where there is only one update, getUpdatesBetweenVersions returns an empty list if the lower version parameter is the same", function () {
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(_this.generalUpdates, new version_1.Version("3.3.1"), new version_1.Version("3.3.2")), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(_this.generalUpdates, new version_1.Version("3.3.1"), new version_1.Version("3.3.1")), []);
        });
        test("For the case where there is only one update, getUpdatesBetweenVersions behaves like there is no lower bound if it is undefined", function () {
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(_this.generalUpdates, undefined, new version_1.Version("3.3.2")), _this.generalUpdates);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(_this.generalUpdates, undefined, new version_1.Version("3.3.1")), _this.generalUpdates);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(_this.generalUpdates, undefined, new version_1.Version("3.3.0")), []);
        });
        var multipleUpdates = [{
                version: "5.3.1",
                date: "06/28/2016",
                changes: [{
                        title: "5.3.1:title",
                        description: "5.3.1:description",
                        supportedBrowsers: ["Edge", "Chrome", "Firefox", "Safari"]
                    }]
            }, {
                version: "4.3.1",
                date: "06/15/2016",
                changes: [{
                        title: "4.3.1:title",
                        description: "4.3.1:description",
                        supportedBrowsers: ["Edge", "Chrome", "Firefox", "Safari"]
                    }]
            }, {
                version: "3.3.1",
                date: "06/13/2016",
                changes: [{
                        title: "3.3.0:title",
                        description: "3.3.0:description",
                        supportedBrowsers: ["Edge", "Chrome", "Firefox", "Safari", "Bookmarklet"]
                    }]
            }];
        test("For the multiple updates case, getUpdatesSinceVersion returns all updates if the version parameter is older than all of them", function () {
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(multipleUpdates, new version_1.Version("2.3.0")), multipleUpdates);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(multipleUpdates, new version_1.Version("3.2.0")), multipleUpdates);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(multipleUpdates, new version_1.Version("3.3.0")), multipleUpdates);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(multipleUpdates, new version_1.Version("3.2.99")), multipleUpdates);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(multipleUpdates, new version_1.Version("0.0.0")), multipleUpdates);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(multipleUpdates, new version_1.Version("2.99.99")), multipleUpdates);
        });
        test("For the multiple updates case, getUpdatesSinceVersion returns an empty list if the version parameter is newer than all of them", function () {
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(multipleUpdates, new version_1.Version("5.3.2")), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(multipleUpdates, new version_1.Version("99.99.99")), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(multipleUpdates, new version_1.Version("5.99.0")), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(multipleUpdates, new version_1.Version("5.4.0")), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(multipleUpdates, new version_1.Version("6.0.0")), []);
        });
        test("For the multiple updates case, getUpdatesSinceVersion returns the update if passed an undefined version", function () {
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(multipleUpdates, undefined), multipleUpdates);
        });
        test("For the multiple updates case, getUpdatesSinceVersion returns the newer versions, non-inclusive of the version parameter", function () {
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(multipleUpdates, new version_1.Version("3.3.1")), multipleUpdates.slice(0, 2));
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(multipleUpdates, new version_1.Version("3.3.2")), multipleUpdates.slice(0, 2));
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(multipleUpdates, new version_1.Version("4.3.0")), multipleUpdates.slice(0, 2));
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(multipleUpdates, new version_1.Version("4.3.1")), multipleUpdates.slice(0, 1));
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(multipleUpdates, new version_1.Version("5.3.0")), multipleUpdates.slice(0, 1));
        });
        test("For the multiple updates case, getUpdatesBetweenVersions returns the update if it is between the lower (exclusive) and upper (inclusive) versions", function () {
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(multipleUpdates, new version_1.Version("3.2.0"), new version_1.Version("3.3.0")), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(multipleUpdates, new version_1.Version("3.3.0"), new version_1.Version("3.3.2")), multipleUpdates.slice(2));
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(multipleUpdates, new version_1.Version("4.3.0"), new version_1.Version("4.3.1")), multipleUpdates.slice(1, 2));
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(multipleUpdates, new version_1.Version("3.3.0"), new version_1.Version("5.3.2")), multipleUpdates);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(multipleUpdates, new version_1.Version("3.3.0"), new version_1.Version("5.3.1")), multipleUpdates);
        });
        test("For the multiple updates case, getUpdatesBetweenVersions behaves like there is no lower bound if it is undefined", function () {
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(multipleUpdates, undefined, new version_1.Version("3.3.0")), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(multipleUpdates, undefined, new version_1.Version("3.3.2")), multipleUpdates.slice(2));
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(multipleUpdates, undefined, new version_1.Version("4.3.1")), multipleUpdates.slice(1));
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(multipleUpdates, undefined, new version_1.Version("5.3.2")), multipleUpdates);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(multipleUpdates, undefined, new version_1.Version("5.3.1")), multipleUpdates);
        });
        // Empty updates cases
        test("If updates is empty, getUpdatesSinceVersion should return an empty list", function () {
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion([], new version_1.Version("3.3.0")), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion([], undefined), []);
        });
        test("If updates is empty, getUpdatesBetweenVersions should return an empty list", function () {
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions([], new version_1.Version("3.3.0"), new version_1.Version("4.0.0")), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions([], undefined, new version_1.Version("4.0.0")), []);
        });
        // Undefined updates cases
        test("If updates is undefined, getUpdatesSinceVersion should return an empty list", function () {
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(undefined, new version_1.Version("3.3.0")), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesSinceVersion(undefined, undefined), []);
        });
        test("If updates is undefined, getUpdatesBetweenVersions should return an empty list", function () {
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(undefined, new version_1.Version("3.3.0"), new version_1.Version("4.0.0")), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(undefined, undefined, new version_1.Version("4.0.0")), []);
        });
        // Test filterUpdatesThatDontApplyToBrowser
        test("Given that there is a single major update with all changes that apply to the browser, it should be returned", function () {
            var browser = "X";
            var updates = [{
                    version: "3.3.1",
                    date: "06/13/2016",
                    changes: [{
                            title: "3.3.1:a:title",
                            description: "3.3.1:a:description",
                            supportedBrowsers: [browser]
                        }, {
                            title: "3.3.1:b:title",
                            description: "3.3.1:b:description",
                            supportedBrowsers: [browser]
                        }]
                }];
            deepEqual(changeLogHelper_1.ChangeLogHelper.filterUpdatesThatDontApplyToBrowser(updates, browser), updates);
        });
        test("Given that there is a single major update with all changes that don't apply to the browser, filterUpdatesThatDontApplyToBrowser should return an empty list", function () {
            var browser = "X";
            var updates = [{
                    version: "3.3.1",
                    date: "06/13/2016",
                    changes: [{
                            title: "3.3.1:a:title",
                            description: "3.3.1:a:description",
                            supportedBrowsers: [browser]
                        }, {
                            title: "3.3.1:b:title",
                            description: "3.3.1:b:description",
                            supportedBrowsers: [browser]
                        }]
                }];
            deepEqual(changeLogHelper_1.ChangeLogHelper.filterUpdatesThatDontApplyToBrowser(updates, "Y"), []);
        });
        test("Given that there is a single major update with some changes that don't apply to the browser, filterUpdatesThatDontApplyToBrowser should the update with only the changes that match the browser", function () {
            var getUpdates = function () {
                return [{
                        version: "3.3.1",
                        date: "06/13/2016",
                        changes: [{
                                title: "3.3.1:a:title",
                                description: "3.3.1:a:description",
                                supportedBrowsers: ["X"]
                            }]
                    }];
            };
            var updatesWithX = getUpdates();
            var updatesWithXY = getUpdates();
            updatesWithXY[0].changes.push({
                title: "3.3.1:b:title",
                description: "3.3.1:b:description",
                supportedBrowsers: ["Y"]
            });
            deepEqual(changeLogHelper_1.ChangeLogHelper.filterUpdatesThatDontApplyToBrowser(updatesWithXY, "X"), updatesWithX);
        });
        test("Given that there is two major updates with some changes that don't apply to the browser, filterUpdatesThatDontApplyToBrowser should the updates with only the changes that match the browser", function () {
            var getUpdates = function () {
                return [{
                        version: "3.3.1",
                        date: "06/13/2016",
                        changes: [{
                                title: "3.3.1:a:title",
                                description: "3.3.1:a:description",
                                supportedBrowsers: ["X"]
                            }]
                    }, {
                        version: "3.3.2",
                        date: "06/14/2016",
                        changes: [{
                                title: "3.3.2:a:title",
                                description: "3.3.2:a:description",
                                supportedBrowsers: ["X"]
                            }]
                    }];
            };
            var updatesWithX = getUpdates();
            var updatesWithXY = getUpdates();
            updatesWithXY[0].changes.push({
                title: "3.3.1:b:title",
                description: "3.3.1:b:description",
                supportedBrowsers: ["Y"]
            });
            deepEqual(changeLogHelper_1.ChangeLogHelper.filterUpdatesThatDontApplyToBrowser(updatesWithXY, "X"), updatesWithX);
        });
        test("If any of the parameters are empty or undefined, filterUpdatesThatDontApplyToBrowser should return the empty list", function () {
            deepEqual(changeLogHelper_1.ChangeLogHelper.filterUpdatesThatDontApplyToBrowser([], "X"), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.filterUpdatesThatDontApplyToBrowser(undefined, "X"), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.filterUpdatesThatDontApplyToBrowser([], undefined), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.filterUpdatesThatDontApplyToBrowser(undefined, undefined), []);
            deepEqual(changeLogHelper_1.ChangeLogHelper.filterUpdatesThatDontApplyToBrowser(undefined, ""), []);
        });
    };
    return ChangeLogHelperTests;
}(testModule_1.TestModule));
exports.ChangeLogHelperTests = ChangeLogHelperTests;
(new ChangeLogHelperTests()).runTests();
