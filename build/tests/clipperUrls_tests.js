"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var clientType_1 = require("../scripts/clientType");
var clipperUrls_1 = require("../scripts/clipperUrls");
var constants_1 = require("../scripts/constants");
var stringUtils_1 = require("../scripts/stringUtils");
var mockProps_1 = require("./mockProps");
var testModule_1 = require("./testModule");
var TestConstants;
(function (TestConstants) {
    var LogCategories;
    (function (LogCategories) {
        LogCategories.oneNoteClipperUsage = "OneNoteClipperUsage";
    })(LogCategories = TestConstants.LogCategories || (TestConstants.LogCategories = {}));
    var Urls;
    (function (Urls) {
        Urls.clipperFeedbackUrl = "https://www.onenote.com/feedback";
    })(Urls = TestConstants.Urls || (TestConstants.Urls = {}));
})(TestConstants || (TestConstants = {}));
var ClipperUrlsTests = (function (_super) {
    __extends(ClipperUrlsTests, _super);
    function ClipperUrlsTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ClipperUrlsTests.prototype.module = function () {
        return "clipperUrls";
    };
    ClipperUrlsTests.prototype.tests = function () {
        test("The generated feedback url should be correct with url query values set appropriately", function () {
            var startingState = mockProps_1.MockProps.getMockClipperState();
            var usid = stringUtils_1.StringUtils.generateGuid();
            var url = clipperUrls_1.ClipperUrls.generateFeedbackUrl(startingState, usid, TestConstants.LogCategories.oneNoteClipperUsage);
            strictEqual(url.indexOf("#"), -1, "There should be no fragment in the feedback url");
            var splitUrl = url.split("?");
            var hostAndPath = splitUrl[0];
            var queryParams = splitUrl[1].split("&");
            strictEqual(hostAndPath, TestConstants.Urls.clipperFeedbackUrl, "The feedback host and path should be correct");
            var expectedQueryParams = {
                LogCategory: TestConstants.LogCategories.oneNoteClipperUsage,
                originalUrl: startingState.pageInfo.rawUrl,
                clipperId: startingState.clientInfo.clipperId,
                usid: usid,
                type: clientType_1.ClientType[startingState.clientInfo.clipperType],
                version: startingState.clientInfo.clipperVersion
            };
            strictEqual(queryParams.length, 6, "There must be exactly 6 query params");
            for (var i = 0; i < queryParams.length; i++) {
                var keyValuePair = queryParams[i].split("=");
                var key = keyValuePair[0];
                var value = keyValuePair[1];
                ok(expectedQueryParams.hasOwnProperty(key), "The key " + key + " must exist in the query params");
                strictEqual(value, expectedQueryParams[key], "The correct value must be assigned to the key " + key);
            }
        });
        test("generateSignInUrl should generate the correct URL given a valid clipperId, sessionId and MSA authType", function () {
            var newUrl = clipperUrls_1.ClipperUrls.generateSignInUrl("ON-1234-abcd", "session1234", "Msa");
            strictEqual(newUrl, constants_1.Constants.Urls.Authentication.signInUrl + "?authType=Msa&clipperId=ON-1234-abcd&userSessionId=session1234");
        });
        test("generateSignInUrl should generate the correct URL given a valid clipperId, sessionId, and O365 authType", function () {
            var newUrl = clipperUrls_1.ClipperUrls.generateSignInUrl("ON-1234-abcd", "session4567", "OrgId");
            strictEqual(newUrl, constants_1.Constants.Urls.Authentication.signInUrl + "?authType=OrgId&clipperId=ON-1234-abcd&userSessionId=session4567");
        });
        test("generateSignOutUrl should generate the correct URL given a valid clipperId, sessionId and MSA authType", function () {
            var newUrl = clipperUrls_1.ClipperUrls.generateSignOutUrl("ON-1234-abcd", "session1234", "Msa");
            strictEqual(newUrl, constants_1.Constants.Urls.Authentication.signOutUrl + "?authType=Msa&clipperId=ON-1234-abcd&userSessionId=session1234");
        });
        test("generateSignOutUrl should generate the correct URL given a valid clipperId, sessionId, and O365 authType", function () {
            var newUrl = clipperUrls_1.ClipperUrls.generateSignOutUrl("ON-1234-abcd", "session4567", "OrgId");
            strictEqual(newUrl, constants_1.Constants.Urls.Authentication.signOutUrl + "?authType=OrgId&clipperId=ON-1234-abcd&userSessionId=session4567");
        });
    };
    return ClipperUrlsTests;
}(testModule_1.TestModule));
exports.ClipperUrlsTests = ClipperUrlsTests;
(new ClipperUrlsTests()).runTests();
