"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var promiseUtils_1 = require("../scripts/promiseUtils");
var asyncUtils_1 = require("./asyncUtils");
var testModule_1 = require("./testModule");
var PromiseUtilsTests = (function (_super) {
    __extends(PromiseUtilsTests, _super);
    function PromiseUtilsTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.expectedResolve = "succeeded";
        _this.expectedReject = "failed";
        return _this;
    }
    PromiseUtilsTests.prototype.module = function () {
        return "promiseUtils";
    };
    PromiseUtilsTests.prototype.beforeEach = function () {
        this.callCount = 0;
        asyncUtils_1.AsyncUtils.mockSetTimeout();
    };
    PromiseUtilsTests.prototype.afterEach = function () {
        asyncUtils_1.AsyncUtils.restoreSetTimeout();
    };
    PromiseUtilsTests.prototype.tests = function () {
        var _this = this;
        test("execWithRetry should call the function once if the first attempt succeeds, and there are more than enough retries", function (assert) {
            var done = assert.async();
            promiseUtils_1.PromiseUtils.execWithRetry(_this.fnGenerator(1)).then(function (retVal) {
                strictEqual(retVal, _this.expectedResolve, "execWithRetry should resolve with the same value the function's promise resolves with");
                strictEqual(_this.callCount, 1, "execWithRetry should call the function once");
            })["catch"](function () {
                ok(false, "execWithRetry should not reject");
            }).then(function () {
                done();
            });
        });
        test("execWithRetry should call the function twice if the first attempt fails, but the second succeeds, and there are more than enough retries", function (assert) {
            var done = assert.async();
            promiseUtils_1.PromiseUtils.execWithRetry(_this.fnGenerator(2)).then(function (retVal) {
                strictEqual(retVal, _this.expectedResolve, "execWithRetry should resolve with the same value the function's promise resolves with");
                strictEqual(_this.callCount, 2, "execWithRetry should call the function twice");
            })["catch"](function () {
                ok(false, "execWithRetry should not reject");
            }).then(function () {
                done();
            });
        });
        test("With the default options, execWithRetry should reject if all attempts fail and there are not enough retries, and the callback should be called 4 times in total", function (assert) {
            var done = assert.async();
            promiseUtils_1.PromiseUtils.execWithRetry(_this.fnGenerator(Infinity)).then(function (retVal) {
                ok(false, "execWithRetry should not resolve");
            })["catch"](function (retVal) {
                strictEqual(retVal, _this.expectedReject, "execWithRetry should reject with the same value the function's promise rejects with");
                strictEqual(_this.callCount, 4, "execWithRetry should call the function 4 times (1 first attempt, then 3 retries)");
            }).then(function () {
                done();
            });
        });
        test("execWithRetry should call the function once if the first attempt succeeds, and there are 0 retries", function (assert) {
            var done = assert.async();
            var retryOptions = { retryCount: 0, retryWaitTimeInMs: 0 };
            promiseUtils_1.PromiseUtils.execWithRetry(_this.fnGenerator(1), retryOptions).then(function (retVal) {
                strictEqual(retVal, _this.expectedResolve, "execWithRetry should resolve with the same value the function's promise resolves with");
                strictEqual(_this.callCount, 1, "execWithRetry should call the function once");
            })["catch"](function () {
                ok(false, "execWithRetry should not reject");
            }).then(function () {
                done();
            });
        });
        test("execWithRetry should call the function once then reject if the first attempt fails, and there are 0 retries", function (assert) {
            var done = assert.async();
            var retryOptions = { retryCount: 0, retryWaitTimeInMs: 0 };
            promiseUtils_1.PromiseUtils.execWithRetry(_this.fnGenerator(Infinity), retryOptions).then(function (retVal) {
                ok(false, "execWithRetry should not resolve");
            })["catch"](function (retVal) {
                strictEqual(retVal, _this.expectedReject, "execWithRetry should reject with the same value the function's promise rejects with");
                strictEqual(_this.callCount, 1, "execWithRetry should call the function once");
            }).then(function () {
                done();
            });
        });
        test("execWithRetry should call the function n times if only the nth attempt succeeds, and there are n-1 retries", function (assert) {
            var done = assert.async();
            var retryOptions = { retryCount: 49, retryWaitTimeInMs: 0 };
            promiseUtils_1.PromiseUtils.execWithRetry(_this.fnGenerator(50), retryOptions).then(function (retVal) {
                strictEqual(retVal, _this.expectedResolve, "execWithRetry should resolve with the same value the function's promise resolves with");
                strictEqual(_this.callCount, 50, "execWithRetry should call the function 50 times");
            })["catch"](function () {
                ok(false, "execWithRetry should not reject");
            }).then(function () {
                done();
            });
        });
    };
    PromiseUtilsTests.prototype.fnGenerator = function (callsUntilSucceed) {
        var _this = this;
        return (function () {
            _this.callCount++;
            if (_this.callCount === callsUntilSucceed) {
                return Promise.resolve(_this.expectedResolve);
            }
            return Promise.reject(_this.expectedReject);
        }).bind(this);
    };
    return PromiseUtilsTests;
}(testModule_1.TestModule));
exports.PromiseUtilsTests = PromiseUtilsTests;
(new PromiseUtilsTests()).runTests();
