"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var rtl_1 = require("../../scripts/localization/rtl");
var testModule_1 = require("../testModule");
var RtlTests = (function (_super) {
    __extends(RtlTests, _super);
    function RtlTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RtlTests.prototype.module = function () {
        return "rtl";
    };
    RtlTests.prototype.tests = function () {
        test("isRtl should return true for all ISO 691-1 codes that are RTL that we support", function () {
            var codes = ["ar", "fa", "he", "sd", "ug", "ur"];
            for (var i = 0; i < codes.length; i++) {
                var code = codes[i];
                ok(rtl_1.Rtl.isRtl(code), code + " detected as RTL");
            }
        });
        test("isRtl should return true for RTL ISO 691-1 codes that have a ISO 3166-1 code appended", function () {
            var code = "ar-LY";
            ok(rtl_1.Rtl.isRtl(code), code + " detected as RTL");
        });
        test("isRtl should return true for RTL ISO 691-1 codes that are capitalized", function () {
            var code = "SD";
            ok(rtl_1.Rtl.isRtl(code), code + " detected as RTL");
        });
        test("isRtl should return true for RTL ISO 691-1 codes that are capitalized and have a ISO 3166-1 code appended", function () {
            var code = "AR-LY";
            ok(rtl_1.Rtl.isRtl(code), code + " detected as RTL");
        });
        test("isRtl should return true for RTL ISO 691-1 codes that have an invalid ISO 3166-1 code appended", function () {
            var code = "ar-NOTEXIST";
            ok(rtl_1.Rtl.isRtl(code), code + " detected as RTL");
        });
        test("isRtl should return false when passed an ISO 639-1 code that corresponds to a LTR language", function () {
            var code = "ja";
            ok(!rtl_1.Rtl.isRtl(code), code + "detected as non RTL");
        });
        test("isRtl should return false when passed an ISO 639-1 code that does not correspond to an existing language", function () {
            var code = "jz";
            ok(!rtl_1.Rtl.isRtl(code), code + "detected as non RTL");
        });
        test("isRtl should return false when passed a capitalized ISO 639-1 code that does not correspond to an existing language", function () {
            var code = "JZ";
            ok(!rtl_1.Rtl.isRtl(code), code + "detected as non RTL");
        });
        test("isRtl should return false when passed the empty string", function () {
            ok(!rtl_1.Rtl.isRtl(""), "Empty string detected as non RTL");
        });
        test("isRtl should return false when passed undefined", function () {
            ok(!rtl_1.Rtl.isRtl(undefined), "Undefined detected as non RTL");
        });
        test("isRtl should return false when passed the null", function () {
            /* tslint:disable:no-null-keyword */
            ok(!rtl_1.Rtl.isRtl(null), "Null string detected as non RTL");
            /* tslint:enable:no-null-keyword */
        });
        test("getIso639P1LocaleCode should return the ISO 639-1 code in the general case", function () {
            var code = "en-US";
            strictEqual(rtl_1.Rtl.getIso639P1LocaleCode(code), "en");
        });
        test("getIso639P1LocaleCode should return the ISO 639-1 code even if underscores are used", function () {
            var code = "en_US";
            strictEqual(rtl_1.Rtl.getIso639P1LocaleCode(code), "en");
        });
        test("getIso639P1LocaleCode should return the ISO 639-1 code when it is already passed in", function () {
            var code = "en";
            strictEqual(rtl_1.Rtl.getIso639P1LocaleCode(code), "en");
        });
        test("getIso639P1LocaleCode should return the ISO 639-1 code for codes with more than one dash", function () {
            var code = "en-en-US";
            strictEqual(rtl_1.Rtl.getIso639P1LocaleCode(code), "en");
        });
        test("getIso639P1LocaleCode should return the ISO 639-1 code when there's a mix of dashes and underscores", function () {
            strictEqual(rtl_1.Rtl.getIso639P1LocaleCode("ar-US_US"), "ar");
            strictEqual(rtl_1.Rtl.getIso639P1LocaleCode("ar_US-US"), "ar");
            strictEqual(rtl_1.Rtl.getIso639P1LocaleCode("ph_BLAH-BLAH_BLAH-"), "ph");
        });
        test("getIso639P1LocaleCode should return empty string when passed empty string", function () {
            strictEqual(rtl_1.Rtl.getIso639P1LocaleCode(""), "");
        });
        test("getIso639P1LocaleCode should return empty string when passed undefined", function () {
            strictEqual(rtl_1.Rtl.getIso639P1LocaleCode(undefined), "");
        });
        test("getIso639P1LocaleCode should return empty string when passed null", function () {
            /* tslint:disable:no-null-keyword */
            strictEqual(rtl_1.Rtl.getIso639P1LocaleCode(null), "");
            /* tslint:enable:no-null-keyword */
        });
    };
    return RtlTests;
}(testModule_1.TestModule));
exports.RtlTests = RtlTests;
(new RtlTests()).runTests();
