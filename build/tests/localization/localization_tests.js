"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var localization_1 = require("../../scripts/localization/localization");
var testModule_1 = require("../testModule");
var LocalizationTests = (function (_super) {
    __extends(LocalizationTests, _super);
    function LocalizationTests() {
        var _this = _super.call(this) || this;
        _this.backupStrings = require("../../strings.json");
        _this.localizedStrings = {};
        _this.getMockLocalizedStrings = function () {
            return JSON.parse(JSON.stringify(_this.localizedStrings));
        };
        // Set up localized strings as backup + postfix for testing
        for (var property in _this.backupStrings) {
            if (_this.backupStrings.hasOwnProperty(property)) {
                _this.localizedStrings[property] = _this.backupStrings[property] + "-LOCALIZED";
                // Used for testing keys that can be found in the backup strings
                if (!_this.existKey) {
                    _this.existKey = property;
                }
            }
        }
        return _this;
    }
    LocalizationTests.prototype.module = function () {
        return "localization";
    };
    LocalizationTests.prototype.tests = function () {
        var _this = this;
        test("getLocalizedString returns the localized string if there is a matching key", function () {
            var mockLocalizedStrings = _this.getMockLocalizedStrings();
            localization_1.Localization.setLocalizedStrings(mockLocalizedStrings);
            strictEqual(localization_1.Localization.getLocalizedString(_this.existKey), mockLocalizedStrings[_this.existKey], "If the key exists, the localized string should be fetched");
        });
        test("getLocalizedString returns the backup string if there is no matching key", function () {
            var mockLocalizedStrings = _this.getMockLocalizedStrings();
            delete mockLocalizedStrings[_this.existKey];
            localization_1.Localization.setLocalizedStrings(mockLocalizedStrings);
            strictEqual(localization_1.Localization.getLocalizedString(_this.existKey), _this.backupStrings[_this.existKey], "If the key does not exist, the backup string should be fetched");
        });
        test("getLocalizedString returns the backup string if there is no localized strings populated", function () {
            strictEqual(localization_1.Localization.getLocalizedString(_this.existKey), _this.backupStrings[_this.existKey], "If no localized strings exist, the backup string should be fetched");
        });
        test("getLocalizedString should throw an error if there is no matching key in the localized or backup strings", function () {
            throws(function () {
                var mockLocalizedStrings = _this.getMockLocalizedStrings();
                localization_1.Localization.setLocalizedStrings(mockLocalizedStrings);
                localization_1.Localization.getLocalizedString("Non.Existing.Key");
            }, Error("getLocalizedString could not find a localized or fallback string: Non.Existing.Key"));
        });
        test("getLocalizedString should throw an error if there is no matching key in the backup strings and no localized strings were set", function () {
            throws(function () {
                localization_1.Localization.getLocalizedString("Non.Existing.Key");
            }, Error("getLocalizedString could not find a localized or fallback string: Non.Existing.Key"));
        });
        test("getLocalizedString should throw an error if the parameter is the empty string", function () {
            throws(function () {
                localization_1.Localization.getLocalizedString("");
            }, Error("stringId must be a non-empty string, but was: "));
        });
        test("getLocalizedString should throw an error if the parameter is null", function () {
            /* tslint:disable:no-null-keyword */
            throws(function () {
                localization_1.Localization.getLocalizedString(null);
            }, Error("stringId must be a non-empty string, but was: null"));
            /* tslint:enable:no-null-keyword */
        });
        test("getLocalizedString should throw an error if the parameter is undefined", function () {
            throws(function () {
                localization_1.Localization.getLocalizedString(undefined);
            }, Error("stringId must be a non-empty string, but was: undefined"));
        });
        test("formatFontFamily should ensure that font family names that contain inner-spaces should be wrapped in single quotes", function () {
            var fontFamily = "Segoe UI Bold,Segoe UI,Segoe,Segoe WP,Helvetica Neue,Roboto,Helvetica,Arial,Tahoma,Verdana,sans-serif";
            var formattedFontFamily = localization_1.Localization.formatFontFamily(fontFamily);
            strictEqual(formattedFontFamily, "'Segoe UI Bold','Segoe UI',Segoe,'Segoe WP','Helvetica Neue',Roboto,Helvetica,Arial,Tahoma,Verdana,sans-serif", "Values containing inner spaces are wrapped in single quotes");
        });
        test("formatFontFamily should not format an already-formatted input, but should simply return the input", function () {
            var fontFamily = "'Segoe UI Bold','Segoe UI',Segoe,'Segoe WP','Helvetica Neue',Roboto,Helvetica,Arial,Tahoma,Verdana,sans-serif";
            var formattedFontFamily = localization_1.Localization.formatFontFamily(fontFamily);
            strictEqual(formattedFontFamily, fontFamily, "Already-formatted values are left as-is");
        });
        test("formatFontFamily should correctly format an inner-space containing name if it is the only one in the input", function () {
            var fontFamily = "My Family";
            var formattedFontFamily = localization_1.Localization.formatFontFamily(fontFamily);
            strictEqual(formattedFontFamily, "'My Family'", "Unformatted name is formatted if it is the only one in the input");
        });
        test("formatFontFamily should leave an already-formatted name as-is if it is the only one in the input", function () {
            var fontFamily = "'Already Formatted'";
            var formattedFontFamily = localization_1.Localization.formatFontFamily(fontFamily);
            strictEqual(formattedFontFamily, fontFamily, "Already-formatted name is left as-is if it is the only one in the input");
        });
        test("formatFontFamily should trim names with unecessary whitespace", function () {
            var fontFamily = "Segoe UI Bold,Segoe UI,   Segoe    ,Segoe WP,   Helvetica Neue,Roboto   ,Helvetica,\tArial,Tahoma\n,Verdana,sans-serif";
            var formattedFontFamily = localization_1.Localization.formatFontFamily(fontFamily);
            strictEqual(formattedFontFamily, "'Segoe UI Bold','Segoe UI',Segoe,'Segoe WP','Helvetica Neue',Roboto,Helvetica,Arial,Tahoma,Verdana,sans-serif", "Values are all trimmed of outer whitespace");
        });
        test("formatFontFamily should return empty string when passed an empty string", function () {
            var formattedFontFamily = localization_1.Localization.formatFontFamily("");
            strictEqual(formattedFontFamily, "", "Input of empty string results in an output of empty string");
        });
        test("formatFontFamily should return empty string when passed null", function () {
            /* tslint:disable:no-null-keyword */
            var formattedFontFamily = localization_1.Localization.formatFontFamily(null);
            strictEqual(formattedFontFamily, "", "Input of null results in an output of empty string");
            /* tslint:enable:no-null-keyword */
        });
        test("formatFontFamily should return empty string when passed undefined", function () {
            var formattedFontFamily = localization_1.Localization.formatFontFamily(undefined);
            strictEqual(formattedFontFamily, "", "Input of undefined results in an output of empty string");
        });
    };
    return LocalizationTests;
}(testModule_1.TestModule));
exports.LocalizationTests = LocalizationTests;
(new LocalizationTests()).runTests();
