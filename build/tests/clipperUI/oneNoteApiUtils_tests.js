"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var frontEndGlobals_1 = require("../../scripts/clipperUI/frontEndGlobals");
var oneNoteApiUtils_1 = require("../../scripts/clipperUI/oneNoteApiUtils");
var stubSessionLogger_1 = require("../../scripts/logging/stubSessionLogger");
var testModule_1 = require("../testModule");
var OneNoteApiUtilsTests = (function (_super) {
    __extends(OneNoteApiUtilsTests, _super);
    function OneNoteApiUtilsTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    OneNoteApiUtilsTests.prototype.module = function () {
        return "oneNoteApiUtils";
    };
    OneNoteApiUtilsTests.prototype.beforeEach = function () {
        frontEndGlobals_1.Clipper.logger = new stubSessionLogger_1.StubSessionLogger();
    };
    OneNoteApiUtilsTests.prototype.tests = function () {
        test("getApiResponseCode should correctly parse out the http response code from the error object", function () {
            var expectedCode = "12345";
            var requestError = {
                error: "err",
                statusCode: 404,
                response: JSON.stringify({
                    error: {
                        code: expectedCode,
                        message: "Unable to create a page in this section because the hamster in the server room died.",
                        "@api.url": "http://aka.ms/onenote-errors#C12345"
                    }
                }),
                responseHeaders: {}
            };
            strictEqual(oneNoteApiUtils_1.OneNoteApiUtils.getApiResponseCode(requestError), expectedCode, "The http response code should be correctly parsed out of the request error");
        });
        test("getApiResponseCode should return undefined if the error object is null", function () {
            /* tslint:disable:no-null-keyword */
            strictEqual(oneNoteApiUtils_1.OneNoteApiUtils.getApiResponseCode(null), undefined, "Undefined should be returned");
            /* tslint:enable:no-null-keyword */
        });
        test("getApiResponseCode should return undefined if the error object is undefined", function () {
            strictEqual(oneNoteApiUtils_1.OneNoteApiUtils.getApiResponseCode(undefined), undefined, "Undefined should be returned");
        });
        test("getApiResponseCode should return undefined if the response in the request error is undefined", function () {
            var requestError = {
                error: "err",
                statusCode: 404,
                response: undefined,
                responseHeaders: {}
            };
            strictEqual(oneNoteApiUtils_1.OneNoteApiUtils.getApiResponseCode(requestError), undefined, "Undefined should be returned");
        });
        test("getApiResponseCode should return undefined if the response's error in the request error is undefined", function () {
            var requestError = {
                error: "err",
                statusCode: 404,
                response: JSON.stringify({
                    error: undefined
                }),
                responseHeaders: {}
            };
            strictEqual(oneNoteApiUtils_1.OneNoteApiUtils.getApiResponseCode(requestError), undefined, "Undefined should be returned");
        });
        test("getApiResponseCode should return undefined if the response's error's code in the request error is empty string", function () {
            var requestError = {
                error: "err",
                statusCode: 404,
                response: JSON.stringify({
                    error: {
                        code: "",
                        message: "Unable to create a page in this section because the hamster in the server room died.",
                        "@api.url": "http://aka.ms/onenote-errors#C12345"
                    }
                }),
                responseHeaders: {}
            };
            strictEqual(oneNoteApiUtils_1.OneNoteApiUtils.getApiResponseCode(requestError), undefined, "Undefined should be returned");
        });
        test("getApiResponseCode should return undefined if the response's error's code in the request error is null", function () {
            /* tslint:disable:no-null-keyword */
            var requestError = {
                error: "err",
                statusCode: 404,
                response: JSON.stringify({
                    error: {
                        code: null,
                        message: "Unable to create a page in this section because the hamster in the server room died.",
                        "@api.url": "http://aka.ms/onenote-errors#C12345"
                    }
                }),
                responseHeaders: {}
            };
            strictEqual(oneNoteApiUtils_1.OneNoteApiUtils.getApiResponseCode(requestError), undefined, "Undefined should be returned");
            /* tslint:enable:no-null-keyword */
        });
        test("getApiResponseCode should return undefined if the response's error's code in the request error is undefined", function () {
            var requestError = {
                error: "err",
                statusCode: 404,
                response: JSON.stringify({
                    error: {
                        code: undefined,
                        message: "Unable to create a page in this section because the hamster in the server room died.",
                        "@api.url": "http://aka.ms/onenote-errors#C12345"
                    }
                }),
                responseHeaders: {}
            };
            strictEqual(oneNoteApiUtils_1.OneNoteApiUtils.getApiResponseCode(requestError), undefined, "Undefined should be returned");
        });
        test("For matching error codes, isRetryable should return true for error codes that should be retryable", function () {
            var retryableCodes = ["10001", "10002", "10007", "19999", "30103"];
            for (var i = 0; i < retryableCodes.length; i++) {
                ok(oneNoteApiUtils_1.OneNoteApiUtils.isRetryable(retryableCodes[i]), "Code " + retryableCodes[i] + " should be retryable");
            }
        });
        test("For matching error codes, isRetryable should return false for error codes that should not be retryable", function () {
            var retryableCodes = ["10003", "10004", "10006", "30101", "30102", "30104", "30105"];
            for (var i = 0; i < retryableCodes.length; i++) {
                ok(!oneNoteApiUtils_1.OneNoteApiUtils.isRetryable(retryableCodes[i]), "Code " + retryableCodes[i] + " should not be retryable");
            }
        });
        test("For an unknown error code, isRetryable should return false", function () {
            ok(!oneNoteApiUtils_1.OneNoteApiUtils.isRetryable("-1"), "Code -1 should not be retryable");
            ok(!oneNoteApiUtils_1.OneNoteApiUtils.isRetryable("3456"), "Code 3456 should not be retryable");
            ok(!oneNoteApiUtils_1.OneNoteApiUtils.isRetryable(undefined));
        });
        test("For matching error codes, isExpected should return true for error codes that are expected", function () {
            var expectedCodes = ["10001", "10002", "10004", "10006", "10007", "30101", "30102", "30103", "30104", "30105"];
            for (var i = 0; i < expectedCodes.length; i++) {
                ok(oneNoteApiUtils_1.OneNoteApiUtils.isExpected(expectedCodes[i]), "Code " + expectedCodes[i] + " should be expected");
            }
        });
        test("For matching error codes, isExpected should return false for error codes that are not expected", function () {
            var nonExpectedCodes = ["19999"];
            for (var i = 0; i < nonExpectedCodes.length; i++) {
                ok(!oneNoteApiUtils_1.OneNoteApiUtils.isExpected(nonExpectedCodes[i]), "Code " + nonExpectedCodes[i] + " should not be expected");
            }
        });
        test("For an unknown error code, isExpected should return false", function () {
            ok(!oneNoteApiUtils_1.OneNoteApiUtils.isExpected("-1"), "Code -1 should not be expected");
            ok(!oneNoteApiUtils_1.OneNoteApiUtils.isExpected("123980123"), "Code 123980123 should not be expected");
        });
        test("For known error codes, the appropriate failure message should be returned from getLocalizedErrorMessage", function () {
            var stringsJson = require("../../strings.json");
            var codeMessagePairs = [
                { code: "10001", message: stringsJson["WebClipper.Error.GenericError"] },
                { code: "10002", message: stringsJson["WebClipper.Error.GenericError"] },
                { code: "10003", message: stringsJson["WebClipper.Error.GenericError"] },
                { code: "10004", message: stringsJson["WebClipper.Error.PasswordProtected"] },
                { code: "10006", message: stringsJson["WebClipper.Error.CorruptedSection"] },
                { code: "10007", message: stringsJson["WebClipper.Error.GenericError"] },
                { code: "19999", message: stringsJson["WebClipper.Error.GenericError"] },
                { code: "30101", message: stringsJson["WebClipper.Error.QuotaExceeded"] },
                { code: "30102", message: stringsJson["WebClipper.Error.SectionTooLarge"] },
                { code: "30103", message: stringsJson["WebClipper.Error.GenericError"] },
                { code: "30104", message: stringsJson["WebClipper.Error.UserAccountSuspended"] },
                { code: "30105", message: stringsJson["WebClipper.Error.NotProvisioned"] }
            ];
            for (var i = 0; i < codeMessagePairs.length; i++) {
                strictEqual(oneNoteApiUtils_1.OneNoteApiUtils.getLocalizedErrorMessage(codeMessagePairs[i].code), codeMessagePairs[i].message, "Code " + codeMessagePairs[i].code + " should be associated with the matching error message");
            }
        });
        test("For unknown error codes, the generic failure message should be returned from getLocalizedErrorMessage", function () {
            var stringsJson = require("../../strings.json");
            strictEqual(oneNoteApiUtils_1.OneNoteApiUtils.getLocalizedErrorMessage("-1"), stringsJson["WebClipper.Error.GenericError"], "Unknown code -1 should be associated with the generic error message");
            strictEqual(oneNoteApiUtils_1.OneNoteApiUtils.getLocalizedErrorMessage("47561234"), stringsJson["WebClipper.Error.GenericError"], "Unknown code 47561234 should be associated with the generic error message");
        });
        test("For known get notebooks error codes, the appropriate failure message should be returned from getLocalizedErrorMessageForGetNotebooks", function () {
            var stringsJson = require("../../strings.json");
            var message = "We couldn't load your notebooks because a list limit was exceeded in OneDrive.\n<a href=\"https://aka.ms/onapi-too-many-items-actionable\">Learn more</a>";
            var codeMessagePairs = [
                { code: "10008", message: message },
                { code: "10013", message: message }
            ];
            for (var i = 0; i < codeMessagePairs.length; i++) {
                strictEqual(oneNoteApiUtils_1.OneNoteApiUtils.getLocalizedErrorMessageForGetNotebooks(codeMessagePairs[i].code), codeMessagePairs[i].message, "Code " + codeMessagePairs[i].code + " should be associated with the matching error message");
            }
        });
        test("For unknown error codes in the get notebooks scenario, the generic get notebooks failure message should be returned from getLocalizedErrorMessageForGetNotebooks", function () {
            var stringsJson = require("../../strings.json");
            strictEqual(oneNoteApiUtils_1.OneNoteApiUtils.getLocalizedErrorMessageForGetNotebooks("-1"), stringsJson["WebClipper.SectionPicker.NotebookLoadUnretryableFailureMessage"], "Unknown code -1 should be associated with the generic error message");
            strictEqual(oneNoteApiUtils_1.OneNoteApiUtils.getLocalizedErrorMessageForGetNotebooks("99999"), stringsJson["WebClipper.SectionPicker.NotebookLoadUnretryableFailureMessage"], "Unknown code 99999 should be associated with the generic error message");
        });
    };
    return OneNoteApiUtilsTests;
}(testModule_1.TestModule));
exports.OneNoteApiUtilsTests = OneNoteApiUtilsTests;
(new OneNoteApiUtilsTests()).runTests();
