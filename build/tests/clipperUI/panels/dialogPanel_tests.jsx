"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../scripts/constants");
var frontEndGlobals_1 = require("../../../scripts/clipperUI/frontEndGlobals");
var dialogPanel_1 = require("../../../scripts/clipperUI/panels/dialogPanel");
var stubSessionLogger_1 = require("../../../scripts/logging/stubSessionLogger");
var assert_1 = require("../../assert");
var mithrilUtils_1 = require("../../mithrilUtils");
var testModule_1 = require("../../testModule");
var DialogPanelTests = (function (_super) {
    __extends(DialogPanelTests, _super);
    function DialogPanelTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DialogPanelTests.prototype.module = function () {
        return "dialogPanel";
    };
    DialogPanelTests.prototype.beforeEach = function () {
        frontEndGlobals_1.Clipper.logger = new stubSessionLogger_1.StubSessionLogger();
    };
    DialogPanelTests.prototype.tests = function () {
        test("Given a message and a button, the dialog panel should render them both correctly", function () {
            var expectedMessage = "hello world";
            var count = 0;
            var buttons = [{
                    id: "a",
                    label: "My button",
                    handler: function () {
                        count++;
                    }
                }];
            mithrilUtils_1.MithrilUtils.mountToFixture(<dialogPanel_1.DialogPanel message={expectedMessage} buttons={buttons}/>);
            strictEqual(document.getElementById(constants_1.Constants.Ids.dialogMessage).innerText, expectedMessage, "The message should be rendered on the dialog panel");
            var dialogButtonContainer = document.getElementById(constants_1.Constants.Ids.dialogButtonContainer);
            var renderedButtons = dialogButtonContainer.getElementsByTagName("a");
            strictEqual(renderedButtons.length, 1, "Only one button should render");
            strictEqual(renderedButtons[0].getElementsByTagName("span")[0].innerText, buttons[0].label, "The button label should be the same as the one passed in");
            strictEqual(count, 0, "The button callback should have not been called yet");
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                renderedButtons[0].click();
            });
            strictEqual(count, 1, "The button callback should be called once");
        });
        test("Given two buttons, they should render correctly and respond to clicks using their own callbacks", function () {
            var expectedMessage = "hello world";
            var countA = 0;
            var countB = 0;
            var buttons = [{
                    id: "a",
                    label: "A Button",
                    handler: function () {
                        countA++;
                    }
                }, {
                    id: "b",
                    label: "B Button",
                    handler: function () {
                        countB++;
                    }
                }];
            mithrilUtils_1.MithrilUtils.mountToFixture(<dialogPanel_1.DialogPanel message={expectedMessage} buttons={buttons}/>);
            strictEqual(document.getElementById(constants_1.Constants.Ids.dialogMessage).innerText, expectedMessage, "The message should be rendered on the dialog panel");
            var dialogButtonContainer = document.getElementById(constants_1.Constants.Ids.dialogButtonContainer);
            var renderedButtons = dialogButtonContainer.getElementsByTagName("a");
            strictEqual(renderedButtons.length, 2, "Two buttons should render");
            for (var i = 0; i < buttons.length; i++) {
                strictEqual(renderedButtons[i].getElementsByTagName("span")[0].innerText, buttons[i].label, "The button label should be the same as the one passed in");
            }
            strictEqual(countA, 0, "The A button callback should have not been called yet");
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                renderedButtons[0].click();
            });
            strictEqual(countA, 1, "The A button callback should be called once");
            strictEqual(countB, 0, "The B button callback should have not been called yet");
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                renderedButtons[1].click();
            });
            strictEqual(countB, 1, "The B button callback should be called once");
        });
        test("Given no buttons, no buttons should be rendered", function () {
            var expectedMessage = "hello world";
            var buttons = [];
            mithrilUtils_1.MithrilUtils.mountToFixture(<dialogPanel_1.DialogPanel message={expectedMessage} buttons={buttons}/>);
            strictEqual(document.getElementById(constants_1.Constants.Ids.dialogMessage).innerText, expectedMessage, "The message should be rendered on the dialog panel");
            var dialogButtonContainer = document.getElementById(constants_1.Constants.Ids.dialogButtonContainer);
            var renderedButtons = dialogButtonContainer.getElementsByTagName("a");
            strictEqual(renderedButtons.length, 0, "No buttons should render");
        });
        test("Given some buttons, they should have equal tab indexes", function () {
            var expectedMessage = "hello world";
            var buttons = [
                { id: "a", label: "a", handler: undefined },
                { id: "b", label: "b", handler: undefined },
                { id: "c", label: "c", handler: undefined }
            ];
            mithrilUtils_1.MithrilUtils.mountToFixture(<dialogPanel_1.DialogPanel message={expectedMessage} buttons={buttons}/>);
            var dialogButtonContainer = document.getElementById(constants_1.Constants.Ids.dialogButtonContainer);
            var renderedButtons = dialogButtonContainer.getElementsByTagName("a");
            assert_1.Assert.equalTabIndexes(renderedButtons);
        });
    };
    return DialogPanelTests;
}(testModule_1.TestModule));
exports.DialogPanelTests = DialogPanelTests;
(new DialogPanelTests()).runTests();
