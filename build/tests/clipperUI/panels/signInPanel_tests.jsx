"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../scripts/constants");
var userInfo_1 = require("../../../scripts/userInfo");
var status_1 = require("../../../scripts/clipperUI/status");
var signInPanel_1 = require("../../../scripts/clipperUI/panels/signInPanel");
var assert_1 = require("../../assert");
var mithrilUtils_1 = require("../../mithrilUtils");
var mockProps_1 = require("../../mockProps");
var testModule_1 = require("../../testModule");
var SignInPanelTests = (function (_super) {
    __extends(SignInPanelTests, _super);
    function SignInPanelTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.stringsJson = require("../../../strings.json");
        _this.mockSignInPanelProps = {
            clipperState: mockProps_1.MockProps.getMockClipperState(),
            onSignInInvoked: function () { }
        };
        return _this;
    }
    SignInPanelTests.prototype.module = function () {
        return "signInPanel";
    };
    SignInPanelTests.prototype.beforeEach = function () {
        this.defaultComponent = <signInPanel_1.SignInPanel {...this.mockSignInPanelProps}/>;
    };
    SignInPanelTests.prototype.tests = function () {
        var _this = this;
        test("The sign in panel should display the correct default message when the override is not specified", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            strictEqual(document.getElementById(constants_1.Constants.Ids.signInText).innerText, _this.stringsJson["WebClipper.Label.SignInDescription"], "The displayed message should be the default");
        });
        test("The sign in panel should display the token refresh error message when we're unable to refresh it", function () {
            var state = mockProps_1.MockProps.getMockClipperState();
            state.userResult = { status: status_1.Status.Failed, data: { lastUpdated: 10000000, updateReason: userInfo_1.UpdateReason.TokenRefreshForPendingClip } };
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(<signInPanel_1.SignInPanel clipperState={state} onSignInInvoked={_this.mockSignInPanelProps.onSignInInvoked}/>);
            strictEqual(document.getElementById(constants_1.Constants.Ids.signInText).innerText, _this.stringsJson["WebClipper.Error.GenericExpiredTokenRefreshError"], "The displayed message should be the correct error message");
        });
        test("The sign in panel should display the sign in failed message when sign in failed", function () {
            var state = mockProps_1.MockProps.getMockClipperState();
            state.userResult = { status: status_1.Status.Failed, data: { lastUpdated: 10000000, updateReason: userInfo_1.UpdateReason.SignInAttempt } };
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(<signInPanel_1.SignInPanel clipperState={state} onSignInInvoked={_this.mockSignInPanelProps.onSignInInvoked}/>);
            strictEqual(document.getElementById(constants_1.Constants.Ids.signInText).innerText, _this.stringsJson["WebClipper.Error.SignInUnsuccessful"], "The displayed message should be the correct error message");
        });
        test("The sign in panel should display the sign in description when the sign in attempt was cancelled", function () {
            var state = mockProps_1.MockProps.getMockClipperState();
            state.userResult = { status: status_1.Status.Failed, data: { lastUpdated: 10000000, updateReason: userInfo_1.UpdateReason.SignInCancel } };
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(<signInPanel_1.SignInPanel clipperState={state} onSignInInvoked={_this.mockSignInPanelProps.onSignInInvoked}/>);
            strictEqual(document.getElementById(constants_1.Constants.Ids.signInText).innerText, _this.stringsJson["WebClipper.Label.SignInDescription"], "The displayed message should be the sign in description message");
        });
        test("The callback should be passed the MSA auth type if the user clicks on the MSA button", function () {
            var type;
            var onSignInInvoked = function (chosenType) {
                type = chosenType;
            };
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(<signInPanel_1.SignInPanel clipperState={mockProps_1.MockProps.getMockClipperState()} onSignInInvoked={onSignInInvoked}/>);
            var msaButton = document.getElementById(constants_1.Constants.Ids.signInButtonMsa);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                msaButton.click();
            });
            strictEqual(type, userInfo_1.AuthType.Msa, "The MSA auth type should be recorded");
        });
        test("The callback should be passed the OrgId auth type if the user clicks on the OrgId button", function () {
            var type;
            var onSignInInvoked = function (chosenType) {
                type = chosenType;
            };
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(<signInPanel_1.SignInPanel clipperState={mockProps_1.MockProps.getMockClipperState()} onSignInInvoked={onSignInInvoked}/>);
            var orgIdButton = document.getElementById(constants_1.Constants.Ids.signInButtonOrgId);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                orgIdButton.click();
            });
            strictEqual(type, userInfo_1.AuthType.OrgId, "The OrgId auth type should be recorded");
        });
        test("The 'more' button is enabled when a sign-in failure is detected (OrgID)", function () {
            var state = mockProps_1.MockProps.getMockClipperState();
            state.userResult = { status: status_1.Status.Failed, data: { lastUpdated: 10000000, updateReason: userInfo_1.UpdateReason.SignInAttempt, errorDescription: "OrgId: An error has occured." } };
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(<signInPanel_1.SignInPanel clipperState={state} onSignInInvoked={_this.mockSignInPanelProps.onSignInInvoked}/>);
            strictEqual(document.getElementById(constants_1.Constants.Ids.signInToggleErrorInformationText).innerText, _this.stringsJson["WebClipper.Label.SignInUnsuccessfulMoreInformation"], "The displayed message should be the 'More' message");
            ok(document.getElementById(constants_1.Constants.Ids.signInErrorDescriptionContainer), "The error description should be showing");
        });
        test("The 'less' button is enabled when a sign-in failure is detected and the more button was clicked (OrgID)", function () {
            var state = mockProps_1.MockProps.getMockClipperState();
            state.userResult = { status: status_1.Status.Failed, data: { lastUpdated: 10000000, updateReason: userInfo_1.UpdateReason.SignInAttempt, errorDescription: "OrgId: An error has occured." } };
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(<signInPanel_1.SignInPanel clipperState={state} onSignInInvoked={_this.mockSignInPanelProps.onSignInInvoked}/>);
            var moreButton = document.getElementById(constants_1.Constants.Ids.signInErrorMoreInformation);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                moreButton.click();
            });
            strictEqual(document.getElementById(constants_1.Constants.Ids.signInToggleErrorInformationText).innerText, _this.stringsJson["WebClipper.Label.SignInUnsuccessfulLessInformation"], "The displayed message should be the 'Less' message");
        });
        test("The error description is showing when a sign-in failure is detected and the more button was clicked (OrgID)", function () {
            var state = mockProps_1.MockProps.getMockClipperState();
            state.userResult = { status: status_1.Status.Failed, data: { lastUpdated: 10000000, updateReason: userInfo_1.UpdateReason.SignInAttempt, errorDescription: "OrgId: An error has occured." } };
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(<signInPanel_1.SignInPanel clipperState={state} onSignInInvoked={_this.mockSignInPanelProps.onSignInInvoked}/>);
            var moreButton = document.getElementById(constants_1.Constants.Ids.signInErrorMoreInformation);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                moreButton.click();
            });
            ok(!!document.getElementById(constants_1.Constants.Ids.signInErrorDescriptionContainer), "The error description is showing");
        });
        test("The 'more' button is not there when a sign-in failure is detected (MSA)", function () {
            var state = mockProps_1.MockProps.getMockClipperState();
            state.userResult = { status: status_1.Status.Failed, data: { lastUpdated: 10000000, updateReason: userInfo_1.UpdateReason.SignInAttempt, writeableCookies: true, errorDescription: "MSA: An error has occured." } };
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(<signInPanel_1.SignInPanel clipperState={state} onSignInInvoked={_this.mockSignInPanelProps.onSignInInvoked}/>);
            ok(!document.getElementById(constants_1.Constants.Ids.signInToggleErrorInformationText), "The error information toggle should not be there in case of an MSA error.");
            ok(!document.getElementById(constants_1.Constants.Ids.signInErrorDescriptionContainer), "The error description should not be showing");
        });
        test("The 'more' button is there when a sign-in failure is detected (MSA) because of unwriteable cookies", function () {
            var state = mockProps_1.MockProps.getMockClipperState();
            state.userResult = { status: status_1.Status.Failed, data: { lastUpdated: 10000000, updateReason: userInfo_1.UpdateReason.SignInAttempt, writeableCookies: false } };
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(<signInPanel_1.SignInPanel clipperState={state} onSignInInvoked={_this.mockSignInPanelProps.onSignInInvoked}/>);
            ok(document.getElementById(constants_1.Constants.Ids.signInToggleErrorInformationText), "The error information toggle should be there in case of an MSA error with no writeable cookies.");
            ok(document.getElementById(constants_1.Constants.Ids.signInErrorDescriptionContainer), "The error description should be showing");
        });
        test("Test the tab order when the 'more' button is not there", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            assert_1.Assert.tabOrderIsIncremental([constants_1.Constants.Ids.signInButtonMsa, constants_1.Constants.Ids.signInButtonOrgId]);
        });
        test("Test the tab order when the 'more' button is enabled", function () {
            var state = mockProps_1.MockProps.getMockClipperState();
            state.userResult = { status: status_1.Status.Failed, data: { lastUpdated: 10000000, updateReason: userInfo_1.UpdateReason.SignInAttempt, errorDescription: "OrgId: An error has occured." } };
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(<signInPanel_1.SignInPanel clipperState={state} onSignInInvoked={_this.mockSignInPanelProps.onSignInInvoked}/>);
            assert_1.Assert.tabOrderIsIncremental([constants_1.Constants.Ids.signInErrorMoreInformation, constants_1.Constants.Ids.signInButtonMsa, constants_1.Constants.Ids.signInButtonOrgId]);
        });
    };
    return SignInPanelTests;
}(testModule_1.TestModule));
exports.SignInPanelTests = SignInPanelTests;
(new SignInPanelTests()).runTests();
