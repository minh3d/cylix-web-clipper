"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../scripts/constants");
var clipMode_1 = require("../../../scripts/clipperUI/clipMode");
var status_1 = require("../../../scripts/clipperUI/status");
var optionsPanel_1 = require("../../../scripts/clipperUI/panels/optionsPanel");
var augmentationHelper_1 = require("../../../scripts/contentCapture/augmentationHelper");
var mithrilUtils_1 = require("../../mithrilUtils");
var mockProps_1 = require("../../mockProps");
var testModule_1 = require("../../testModule");
var mockPdfDocument_1 = require("../../contentCapture/mockPdfDocument");
var _ = require("lodash");
var OptionsPanelTests = (function (_super) {
    __extends(OptionsPanelTests, _super);
    function OptionsPanelTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    OptionsPanelTests.prototype.module = function () {
        return "optionsPanel";
    };
    OptionsPanelTests.prototype.beforeEach = function () {
        var _this = this;
        var mockState = mockProps_1.MockProps.getMockClipperState();
        this.mockOptionsPanelProps = {
            onStartClip: function () { _this.onStartClipCalled = true; },
            onPopupToggle: function (shouldNowBeOpen) { },
            clipperState: mockState
        };
        this.onStartClipCalled = false;
    };
    OptionsPanelTests.prototype.tests = function () {
        var _this = this;
        test("If the current mode is full page and its call to the API has succeeded, the clip button should be active and clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.FullPage);
            _this.mockOptionsPanelProps.clipperState.fullPageResult.status = status_1.Status.Succeeded;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(true, true);
        });
        test("If the current mode is full page and its call to the API is in progress, the clip button should not be active or clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.FullPage);
            _this.mockOptionsPanelProps.clipperState.fullPageResult.status = status_1.Status.InProgress;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(false, false);
        });
        test("If the current mode is full page and its call to the API failed, the clip button should not be active or clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.FullPage);
            _this.mockOptionsPanelProps.clipperState.fullPageResult.status = status_1.Status.Failed;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(false, false);
        });
        test("If the current mode is region and there are regions selected, the clip button should be active and clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Region);
            _this.mockOptionsPanelProps.clipperState.regionResult.status = status_1.Status.Succeeded;
            _this.mockOptionsPanelProps.clipperState.regionResult.data = ["src1", "src2"];
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(true, true);
        });
        test("If the current mode is region and there are no regions selected, the clip button should not be active or clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Region);
            _this.mockOptionsPanelProps.clipperState.regionResult.status = status_1.Status.Succeeded;
            _this.mockOptionsPanelProps.clipperState.regionResult.data = [];
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(false, false);
        });
        test("If the current mode is region and the status is NotStarted, the clip button should not be active or clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Region);
            _this.mockOptionsPanelProps.clipperState.regionResult.status = status_1.Status.NotStarted;
            _this.mockOptionsPanelProps.clipperState.regionResult.data = [];
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(false, false);
        });
        test("If the current mode is region and the status is InProgress, the clip button should not be active or clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Region);
            _this.mockOptionsPanelProps.clipperState.regionResult.status = status_1.Status.InProgress;
            _this.mockOptionsPanelProps.clipperState.regionResult.data = [];
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(false, false);
        });
        test("If the current mode is region and the status is InProgress and has images already stored, the clip button should not be active or clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Region);
            _this.mockOptionsPanelProps.clipperState.regionResult.status = status_1.Status.InProgress;
            _this.mockOptionsPanelProps.clipperState.regionResult.data = ["src1", "src2"];
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(false, false);
        });
        test("If the current mode is selection and the status is succeeded and has a text stored in clipperState, the clip button should be active and clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Selection);
            _this.mockOptionsPanelProps.clipperState.selectionPreviewInfo.previewBodyHtml = "hello world";
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(true, true);
        });
        test("If the current mode is augmentation and the status is succeeded and has content stored in clipperState, the clip button should be active and clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Augmentation);
            _this.mockOptionsPanelProps.clipperState.augmentationResult.status = status_1.Status.Succeeded;
            _this.mockOptionsPanelProps.clipperState.augmentationResult.data = {
                ContentInHtml: "hello article",
                ContentModel: augmentationHelper_1.AugmentationModel.Article
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(true, true);
        });
        test("If the current mode is augmentation and the status is succeeded and has content stored in clipperState as a recipe, the clip button should be active and clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Augmentation);
            _this.mockOptionsPanelProps.clipperState.augmentationResult.status = status_1.Status.Succeeded;
            _this.mockOptionsPanelProps.clipperState.augmentationResult.data = {
                ContentInHtml: "hello article",
                ContentModel: augmentationHelper_1.AugmentationModel.Recipe
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(true, true);
        });
        test("If the current mode is augmentation and the status is succeeded but no content was found, the clip button should not be active or clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Augmentation);
            _this.mockOptionsPanelProps.clipperState.augmentationResult.status = status_1.Status.Succeeded;
            _this.mockOptionsPanelProps.clipperState.augmentationResult.data = {
                ContentInHtml: "",
                ContentModel: augmentationHelper_1.AugmentationModel.None
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(false, false);
        });
        test("If the current mode is augmentation and the status is succeeded but no content was found, and ContentInHtml is undefined, the clip button should not be active or clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Augmentation);
            _this.mockOptionsPanelProps.clipperState.augmentationResult.status = status_1.Status.Succeeded;
            _this.mockOptionsPanelProps.clipperState.augmentationResult.data = {
                ContentInHtml: undefined,
                ContentModel: augmentationHelper_1.AugmentationModel.None
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(false, false);
        });
        test("If the current mode is augmentation and the status is in progress, the clip button should not be active or clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Augmentation);
            _this.mockOptionsPanelProps.clipperState.augmentationResult.status = status_1.Status.InProgress;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(false, false);
        });
        test("If the current mode is augmentation and the status indicates failure, the clip button should not be active or clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Augmentation);
            _this.mockOptionsPanelProps.clipperState.augmentationResult.status = status_1.Status.Failed;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(false, false);
        });
        test("If the current mode is bookmark and the status indicates success, the clip button should should be active and clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Bookmark);
            _this.mockOptionsPanelProps.clipperState.bookmarkResult.status = status_1.Status.Succeeded;
            _this.mockOptionsPanelProps.clipperState.bookmarkResult.data = {
                title: "title",
                url: "mywebsite.website.com.website"
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(true, true);
        });
        test("If the current mode is bookmark and the status indicates failure, the clip button should not be active or clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Bookmark);
            _this.mockOptionsPanelProps.clipperState.bookmarkResult.status = status_1.Status.Failed;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(false, false);
        });
        test("If the current mode is bookmark and the status indicates InProgress, the clip button should not be active or clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Bookmark);
            _this.mockOptionsPanelProps.clipperState.bookmarkResult.status = status_1.Status.InProgress;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(false, false);
        });
        test("If the current mode is bookmark and the status indicates NotStarted, the clip button should not be active or clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Bookmark);
            _this.mockOptionsPanelProps.clipperState.bookmarkResult.status = status_1.Status.NotStarted;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(false, false);
        });
        test("If the current mode is PDF and the status indicates Succeeded, and AllPages is selected, the clip button should be active and clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Pdf);
            _this.mockOptionsPanelProps.clipperState.pdfResult.status = status_1.Status.Succeeded;
            _this.mockOptionsPanelProps.clipperState.pdfResult.data.set({
                pdf: new mockPdfDocument_1.MockPdfDocument(),
                viewportDimensions: mockPdfDocument_1.MockPdfValues.dimensions,
                byteLength: mockPdfDocument_1.MockPdfValues.byteLength
            });
            _this.mockOptionsPanelProps.clipperState.pdfPreviewInfo = {
                allPages: true,
                isLocalFileAndNotAllowed: true,
                selectedPageRange: ""
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(true, true);
        });
        test("If the current mode is PDF and the status indicates Succeeded, and AllPages is selected, even though a custom selection was made as well, the clip button should be active and clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Pdf);
            _this.mockOptionsPanelProps.clipperState.pdfResult.status = status_1.Status.Succeeded;
            _this.mockOptionsPanelProps.clipperState.pdfResult.data.set({
                pdf: new mockPdfDocument_1.MockPdfDocument(),
                viewportDimensions: mockPdfDocument_1.MockPdfValues.dimensions,
                byteLength: mockPdfDocument_1.MockPdfValues.byteLength
            });
            _this.mockOptionsPanelProps.clipperState.pdfPreviewInfo = {
                allPages: true,
                isLocalFileAndNotAllowed: true,
                selectedPageRange: "1--3invalid"
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(true, true);
        });
        test("If the current mode is PDF and the status indicates Succeeded, and a valid page selection is selected, the clip button should be active and clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Pdf);
            _this.mockOptionsPanelProps.clipperState.pdfResult.status = status_1.Status.Succeeded;
            _this.mockOptionsPanelProps.clipperState.pdfResult.data.set({
                pdf: new mockPdfDocument_1.MockPdfDocument(),
                viewportDimensions: mockPdfDocument_1.MockPdfValues.dimensions,
                byteLength: mockPdfDocument_1.MockPdfValues.byteLength
            });
            _.assign(_this.mockOptionsPanelProps.clipperState.pdfPreviewInfo, {
                allPages: false,
                isLocalFileAndNotAllowed: true,
                selectedPageRange: "1-3"
            });
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(true, true);
        });
        // Popover tests begin
        test("If the current mode is PDF and the status indicates Succeeded, and an invalid page selection is selected, the clip button should be active and clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Pdf);
            _this.mockOptionsPanelProps.clipperState.pdfResult.status = status_1.Status.Succeeded;
            _this.mockOptionsPanelProps.clipperState.pdfResult.data.set({
                pdf: new mockPdfDocument_1.MockPdfDocument(),
                viewportDimensions: mockPdfDocument_1.MockPdfValues.dimensions,
                byteLength: mockPdfDocument_1.MockPdfValues.byteLength
            });
            _.assign(_this.mockOptionsPanelProps.clipperState.pdfPreviewInfo, {
                allPages: false,
                isLocalFileAndNotAllowed: true,
                selectedPageRange: "1-3,5,6-"
            });
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(true, false);
        });
        test("If the current mode is PDF and the status indicates Succeeded, and a syntactically correct but out-of-bounds selection is selected, the clip button should be active and clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Pdf);
            _this.mockOptionsPanelProps.clipperState.pdfResult.status = status_1.Status.Succeeded;
            _this.mockOptionsPanelProps.clipperState.pdfResult.data.set({
                pdf: new mockPdfDocument_1.MockPdfDocument(),
                viewportDimensions: mockPdfDocument_1.MockPdfValues.dimensions,
                byteLength: mockPdfDocument_1.MockPdfValues.byteLength
            });
            _this.mockOptionsPanelProps.clipperState.pdfPreviewInfo = {
                allPages: false,
                isLocalFileAndNotAllowed: true,
                selectedPageRange: "4"
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(true, false);
        });
        test("If the current mode is PDF and the status indicates Succeeded, and an empty page selection is selected, the clip button should be active and clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Pdf);
            _this.mockOptionsPanelProps.clipperState.pdfResult.status = status_1.Status.Succeeded;
            _this.mockOptionsPanelProps.clipperState.pdfResult.data.set({
                pdf: new mockPdfDocument_1.MockPdfDocument(),
                viewportDimensions: mockPdfDocument_1.MockPdfValues.dimensions,
                byteLength: mockPdfDocument_1.MockPdfValues.byteLength
            });
            _this.mockOptionsPanelProps.clipperState.pdfPreviewInfo = {
                allPages: false,
                isLocalFileAndNotAllowed: true,
                selectedPageRange: ""
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(true, false);
        });
        test("If the current mode is PDF and the status indicates Succeeded, and an undefined page selection is selected, the clip button should not be active or clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Pdf);
            _this.mockOptionsPanelProps.clipperState.pdfResult.status = status_1.Status.Succeeded;
            _this.mockOptionsPanelProps.clipperState.pdfResult.data.set({
                pdf: new mockPdfDocument_1.MockPdfDocument(),
                viewportDimensions: mockPdfDocument_1.MockPdfValues.dimensions,
                byteLength: mockPdfDocument_1.MockPdfValues.byteLength
            });
            _this.mockOptionsPanelProps.clipperState.pdfPreviewInfo = {
                allPages: false,
                isLocalFileAndNotAllowed: true
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(false, false);
        });
        test("If the current mode is PDF and the status indicates Succeeded, but the file is local and it was not allowed, the clip button should not be active or clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Pdf);
            _this.mockOptionsPanelProps.clipperState.pdfResult.status = status_1.Status.Succeeded;
            _this.mockOptionsPanelProps.clipperState.pdfResult.data.set({
                pdf: new mockPdfDocument_1.MockPdfDocument(),
                viewportDimensions: mockPdfDocument_1.MockPdfValues.dimensions,
                byteLength: mockPdfDocument_1.MockPdfValues.byteLength
            });
            _this.mockOptionsPanelProps.clipperState.pdfPreviewInfo = {
                allPages: true,
                isLocalFileAndNotAllowed: false,
                selectedPageRange: ""
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(false, false);
        });
        test("If the current mode is PDF and the status indicates InProgress, the clip button should not be active or clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Pdf);
            _this.mockOptionsPanelProps.clipperState.pdfResult.status = status_1.Status.InProgress;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(false, false);
        });
        test("If the current mode is PDF and the status indicates NotStarted, the clip button should not be active or clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Pdf);
            _this.mockOptionsPanelProps.clipperState.pdfResult.status = status_1.Status.InProgress;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(false, false);
        });
        test("If the current mode is PDF and the status indicates failure, the clip button should not be active or clickable", function () {
            _this.mockOptionsPanelProps.clipperState.currentMode.set(clipMode_1.ClipMode.Pdf);
            _this.mockOptionsPanelProps.clipperState.pdfResult.status = status_1.Status.Failed;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(optionsPanel_1.OptionsPanel, {onStartClip:_this.mockOptionsPanelProps.onStartClip, onPopupToggle:_this.mockOptionsPanelProps.onPopupToggle, clipperState:_this.mockOptionsPanelProps.clipperState}));
            _this.assertClipButtonAvailability(false, false);
        });
    };
    OptionsPanelTests.prototype.assertClipButtonAvailability = function (isAvailable, onStartClippedCalled) {
        // First test clicking the clip button
        var clipButton = document.getElementById(constants_1.Constants.Ids.clipButton);
        ok(clipButton, "The clip button should be present");
        var clipButtonContainer = document.getElementById(constants_1.Constants.Ids.clipButtonContainer);
        ok(clipButtonContainer, "The clip button container should always be present");
        strictEqual(!clipButtonContainer.classList.contains("disabled"), isAvailable, "The clip button container should indicate that the button is active if it is available");
        mithrilUtils_1.MithrilUtils.simulateAction(function () {
            clipButton.click();
        });
        strictEqual(this.onStartClipCalled, onStartClippedCalled, "The onStartClip callback should be called only if it's available");
        // Then test the hotkey
        mithrilUtils_1.MithrilUtils.simulateAction(function () {
            var clipHotKeyEvent;
            // TODO: if initKeyboardEvent gets removed in phantomjs, or KeyboardEvent constructor
            // 	gets support, then uncomment this line
            // if (KeyboardEvent) {
            // 	clipHotKeyEvent = new KeyboardEvent("keydown", {
            // 		altKey: true,
            // 		key: Constants.StringKeyCodes.c
            // 	});
            // }
            if (document.createEvent) {
                clipHotKeyEvent = document.createEvent("KeyboardEvent");
                clipHotKeyEvent.initKeyboardEvent("keydown", // typeArg,
                true, // canBubbleArg,
                true, // cancelableArg,
                /* tslint:disable:no-null-keyword */
                null, // viewArg,  Specifies UIEvent.view. This value may be null.
                /* tslint:enable:no-null-keyword */
                false, // ctrlKeyArg,
                true, // altKeyArg,
                false, // shiftKeyArg,
                false, // metaKeyArg,
                9, // keyCodeArg,
                0); // charCodeArg);
            }
            document.dispatchEvent(clipHotKeyEvent);
        });
        strictEqual(this.onStartClipCalled, onStartClippedCalled, "The onStartClip callback should be called only if it's available");
    };
    return OptionsPanelTests;
}(testModule_1.TestModule));
exports.OptionsPanelTests = OptionsPanelTests;
(new OptionsPanelTests()).runTests();
