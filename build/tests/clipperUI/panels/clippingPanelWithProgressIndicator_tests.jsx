"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../scripts/constants");
var clippingPanelWithProgressIndicator_1 = require("../../../scripts/clipperUI/panels/clippingPanelWithProgressIndicator");
var mithrilUtils_1 = require("../../mithrilUtils");
var mockProps_1 = require("../../mockProps");
var testModule_1 = require("../../testModule");
var ClippingPanelWithProgressIndicatorTests = (function (_super) {
    __extends(ClippingPanelWithProgressIndicatorTests, _super);
    function ClippingPanelWithProgressIndicatorTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ClippingPanelWithProgressIndicatorTests.prototype.module = function () {
        return "clippingPanelWithProgressIndicator";
    };
    ClippingPanelWithProgressIndicatorTests.prototype.beforeEach = function () {
        this.mockClipperState = mockProps_1.MockProps.getMockClipperState();
    };
    ClippingPanelWithProgressIndicatorTests.prototype.tests = function () {
        var _this = this;
        test("Given that numItemsCompleted or numItemsTotal is undefined, the panel should not render any loading message", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(<clippingPanelWithProgressIndicator_1.ClippingPanelWithProgressIndicator clipperState={_this.mockClipperState}/>);
            ok(!document.getElementById(constants_1.Constants.Ids.clipProgressIndicatorMessage), "The clipping progress indication message should not be rendered");
        });
        test("If numItemsCompleted is greater than numItemsTotal, the progress message should not be rendered", function () {
            _this.mockClipperState.clipSaveStatus.numItemsCompleted = 5;
            _this.mockClipperState.clipSaveStatus.numItemsTotal = 1;
            ok(!document.getElementById(constants_1.Constants.Ids.clipProgressIndicatorMessage), "The clipping progress indication message should not be rendered");
        });
        test("If numItemsCompleted is negative, the progress message should not be rendered", function () {
            _this.mockClipperState.clipSaveStatus.numItemsCompleted = -1;
            ok(!document.getElementById(constants_1.Constants.Ids.clipProgressIndicatorMessage), "The clipping progress indication message should not be rendered");
        });
        test("If numItemsTotal is negative, the progress message should not be rendered", function () {
            _this.mockClipperState.clipSaveStatus.numItemsTotal = -1;
            ok(!document.getElementById(constants_1.Constants.Ids.clipProgressIndicatorMessage), "The clipping progress indication message should not be rendered");
        });
        test("Given that state has a valid clipSaveStatus, the panel should render the progress message with the correct values subsittuted", function () {
            _this.mockClipperState.clipSaveStatus.numItemsCompleted = 1;
            _this.mockClipperState.clipSaveStatus.numItemsTotal = 5;
            mithrilUtils_1.MithrilUtils.mountToFixture(<clippingPanelWithProgressIndicator_1.ClippingPanelWithProgressIndicator clipperState={_this.mockClipperState}/>);
            var messageElement = document.getElementById(constants_1.Constants.Ids.clipProgressIndicatorMessage);
            ok(messageElement, "The clipping progress indication message should be rendered");
            strictEqual(messageElement.innerText, "Clipping page 2 of 5...");
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                _this.mockClipperState.clipSaveStatus.numItemsCompleted = 2;
                _this.mockClipperState.clipSaveStatus.numItemsTotal = 6;
            });
            messageElement = document.getElementById(constants_1.Constants.Ids.clipProgressIndicatorMessage);
            ok(messageElement, "The clipping progress indication message should be rendered");
            strictEqual(messageElement.innerText, "Clipping page 3 of 6...");
        });
    };
    return ClippingPanelWithProgressIndicatorTests;
}(testModule_1.TestModule));
exports.ClippingPanelWithProgressIndicatorTests = ClippingPanelWithProgressIndicatorTests;
(new ClippingPanelWithProgressIndicatorTests()).runTests();
