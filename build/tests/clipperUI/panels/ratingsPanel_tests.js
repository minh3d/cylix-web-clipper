"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ratingsPanel_1 = require("../../../scripts/clipperUI/panels/ratingsPanel");
var frontEndGlobals_1 = require("../../../scripts/clipperUI/frontEndGlobals");
var ratingsHelper_1 = require("../../../scripts/clipperUI/ratingsHelper");
var clipperStorageKeys_1 = require("../../../scripts/storage/clipperStorageKeys");
var constants_1 = require("../../../scripts/constants");
var objectUtils_1 = require("../../../scripts/objectUtils");
var settings_1 = require("../../../scripts/settings");
var mithrilUtils_1 = require("../../mithrilUtils");
var mockProps_1 = require("../../mockProps");
var testModule_1 = require("../../testModule");
var TestConstants;
(function (TestConstants) {
    var LogCategories;
    (function (LogCategories) {
        LogCategories.oneNoteClipperUsage = "OneNoteClipperUsage";
    })(LogCategories = TestConstants.LogCategories || (TestConstants.LogCategories = {}));
    var Urls;
    (function (Urls) {
        Urls.clipperFeedbackUrl = "https://www.onenote.com/feedback";
    })(Urls = TestConstants.Urls || (TestConstants.Urls = {}));
})(TestConstants || (TestConstants = {}));
var RatingsPanelTests = (function (_super) {
    __extends(RatingsPanelTests, _super);
    function RatingsPanelTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RatingsPanelTests.prototype.module = function () {
        return "ratingsPanel";
    };
    RatingsPanelTests.prototype.beforeEach = function () {
        settings_1.Settings.setSettingsJsonForTesting({});
        ratingsHelper_1.RatingsHelper.preCacheNeededValues();
    };
    RatingsPanelTests.prototype.tests = function () {
        test("'Positive' click at RatingsPromptStage.Init goes to RatingsPromptStage.Rate when rate url exists", function (assert) {
            var done = assert.async();
            settings_1.Settings.setSettingsJsonForTesting({
                "ChromeExtension_RatingUrl": {
                    "Value": "https://chrome.google.com/webstore/detail/onenote-web-clipper/reviews"
                }
            });
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.showRatingsPrompt = true;
            var ratingsPanel = m.component(ratingsPanel_1.RatingsPanel, {clipperState:clipperState});
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(ratingsPanel);
            var initPositive = document.getElementById(constants_1.Constants.Ids.ratingsButtonInitYes);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                initPositive.click();
            });
            strictEqual(ratingsHelper_1.RatingsPromptStage[controllerInstance.state.userSelectedRatingsPromptStage], ratingsHelper_1.RatingsPromptStage[ratingsHelper_1.RatingsPromptStage.Rate]);
            frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.doNotPromptRatings, function (doNotPromptRatingsAsStr) {
                strictEqual(doNotPromptRatingsAsStr, "true");
                done();
            });
        });
        test("'Positive' click at RatingsPromptStage.Init goes to RatingsPromptStage.End when rate url does not exist", function (assert) {
            var done = assert.async();
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.showRatingsPrompt = true;
            var ratingsPanel = m.component(ratingsPanel_1.RatingsPanel, {clipperState:clipperState});
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(ratingsPanel);
            var initPositive = document.getElementById(constants_1.Constants.Ids.ratingsButtonInitYes);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                initPositive.click();
            });
            strictEqual(ratingsHelper_1.RatingsPromptStage[controllerInstance.state.userSelectedRatingsPromptStage], ratingsHelper_1.RatingsPromptStage[ratingsHelper_1.RatingsPromptStage.End]);
            frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.doNotPromptRatings, function (doNotPromptRatingsAsStr) {
                strictEqual(doNotPromptRatingsAsStr, "true");
                done();
            });
        });
        test("'Negative' click at RatingsPromptStage.Init without a prior bad rating goes to RatingsPromptStage.Feedback when feedback url exists (and doNotPromptRatings === undefined)", function (assert) {
            var done = assert.async();
            settings_1.Settings.setSettingsJsonForTesting({
                "LogCategory_RatingsPrompt": {
                    "Value": TestConstants.LogCategories.oneNoteClipperUsage
                }
            });
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastSeenVersion, "3.1.0");
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.showRatingsPrompt = true;
            var ratingsPanel = m.component(ratingsPanel_1.RatingsPanel, {clipperState:clipperState});
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(ratingsPanel);
            var initNegative = document.getElementById(constants_1.Constants.Ids.ratingsButtonInitNo);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                initNegative.click();
            });
            strictEqual(ratingsHelper_1.RatingsPromptStage[controllerInstance.state.userSelectedRatingsPromptStage], ratingsHelper_1.RatingsPromptStage[ratingsHelper_1.RatingsPromptStage.Feedback]);
            frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.doNotPromptRatings, function (doNotPromptRatingsAsStr) {
                strictEqual(doNotPromptRatingsAsStr, undefined, "doNotPromptRatings should be undefined");
                done();
            });
        });
        test("'Negative' click at RatingsPromptStage.Init without a prior bad rating goes to RatingsPromptStage.End when feedback url does not exist (and doNotPromptRatings === undefined)", function (assert) {
            var done = assert.async();
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastSeenVersion, "3.1.0");
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.showRatingsPrompt = true;
            var ratingsPanel = m.component(ratingsPanel_1.RatingsPanel, {clipperState:clipperState});
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(ratingsPanel);
            var initNegative = document.getElementById(constants_1.Constants.Ids.ratingsButtonInitNo);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                initNegative.click();
            });
            strictEqual(ratingsHelper_1.RatingsPromptStage[controllerInstance.state.userSelectedRatingsPromptStage], ratingsHelper_1.RatingsPromptStage[ratingsHelper_1.RatingsPromptStage.End]);
            frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.doNotPromptRatings, function (doNotPromptRatingsAsStr) {
                strictEqual(doNotPromptRatingsAsStr, undefined, "doNotPromptRatings should be undefined");
                done();
            });
        });
        test("'Negative' click at RatingsPromptStage.Init with a prior bad rating sets doNotPromptRatings to 'true' (feedback url exists)", function (assert) {
            var done = assert.async();
            settings_1.Settings.setSettingsJsonForTesting({
                "LogCategory_RatingsPrompt": {
                    "Value": TestConstants.LogCategories.oneNoteClipperUsage
                }
            });
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingDate, (Date.now() - constants_1.Constants.Settings.minTimeBetweenBadRatings).toString());
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastSeenVersion, "3.1.0");
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.showRatingsPrompt = true;
            var ratingsPanel = m.component(ratingsPanel_1.RatingsPanel, {clipperState:clipperState});
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(ratingsPanel);
            var initNegative = document.getElementById(constants_1.Constants.Ids.ratingsButtonInitNo);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                initNegative.click();
            });
            strictEqual(ratingsHelper_1.RatingsPromptStage[controllerInstance.state.userSelectedRatingsPromptStage], ratingsHelper_1.RatingsPromptStage[ratingsHelper_1.RatingsPromptStage.Feedback]);
            frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.doNotPromptRatings, function (doNotPromptRatingsAsStr) {
                strictEqual(doNotPromptRatingsAsStr, "true");
                done();
            });
        });
        test("'Negative' click at RatingsPromptStage.Init with a prior bad rating sets doNotPromptRatings to 'true' (feedback url does not exist)", function (assert) {
            var done = assert.async();
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingDate, (Date.now() - constants_1.Constants.Settings.minTimeBetweenBadRatings).toString());
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastSeenVersion, "3.1.0");
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.showRatingsPrompt = true;
            var ratingsPanel = m.component(ratingsPanel_1.RatingsPanel, {clipperState:clipperState});
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(ratingsPanel);
            var initNegative = document.getElementById(constants_1.Constants.Ids.ratingsButtonInitNo);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                initNegative.click();
            });
            strictEqual(ratingsHelper_1.RatingsPromptStage[controllerInstance.state.userSelectedRatingsPromptStage], ratingsHelper_1.RatingsPromptStage[ratingsHelper_1.RatingsPromptStage.End]);
            frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.doNotPromptRatings, function (doNotPromptRatingsAsStr) {
                strictEqual(doNotPromptRatingsAsStr, "true");
                done();
            });
        });
        test("'Rate' click at RatingsPromptStage.Rate goes to RatingsPromptStage.End when rate url exists", function () {
            settings_1.Settings.setSettingsJsonForTesting({
                "ChromeExtension_RatingUrl": {
                    "Value": "https://chrome.google.com/webstore/detail/onenote-web-clipper/reviews"
                }
            });
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.showRatingsPrompt = true;
            var ratingsPanel = m.component(ratingsPanel_1.RatingsPanel, {clipperState:clipperState});
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(ratingsPanel);
            // skip to RATE panel
            controllerInstance.setState({ currentRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.Rate });
            m.redraw(true);
            var ratePositive = document.getElementById(constants_1.Constants.Ids.ratingsButtonRateYes);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                ratePositive.click();
            });
            strictEqual(ratingsHelper_1.RatingsPromptStage[controllerInstance.state.userSelectedRatingsPromptStage], ratingsHelper_1.RatingsPromptStage[ratingsHelper_1.RatingsPromptStage.End]);
        });
        test("'Rate' click at RatingsPromptStage.Rate not available when rate url does not exist (unexpected scenario)", function () {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.showRatingsPrompt = true;
            var ratingsPanel = m.component(ratingsPanel_1.RatingsPanel, {clipperState:clipperState});
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(ratingsPanel);
            // skip to RATE panel
            controllerInstance.setState({ currentRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.Rate });
            m.redraw(true);
            var ratePositive = document.getElementById(constants_1.Constants.Ids.ratingsButtonRateYes);
            ok(objectUtils_1.ObjectUtils.isNullOrUndefined(ratePositive), "'Rate' button should not exist");
            strictEqual(ratingsHelper_1.RatingsPromptStage[controllerInstance.state.userSelectedRatingsPromptStage], ratingsHelper_1.RatingsPromptStage[ratingsHelper_1.RatingsPromptStage.None]);
        });
        test("'No Thanks' click at RatingsPromptStage.Rate goes to RatingsPromptStage.None when rate url exists", function () {
            settings_1.Settings.setSettingsJsonForTesting({
                "ChromeExtension_RatingUrl": {
                    "Value": "https://chrome.google.com/webstore/detail/onenote-web-clipper/reviews"
                }
            });
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.showRatingsPrompt = true;
            var ratingsPanel = m.component(ratingsPanel_1.RatingsPanel, {clipperState:clipperState});
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(ratingsPanel);
            // skip to RATE panel
            controllerInstance.setState({ currentRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.Rate });
            m.redraw(true);
            var rateNegative = document.getElementById(constants_1.Constants.Ids.ratingsButtonRateNo);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                rateNegative.click();
            });
            strictEqual(ratingsHelper_1.RatingsPromptStage[controllerInstance.state.userSelectedRatingsPromptStage], ratingsHelper_1.RatingsPromptStage[ratingsHelper_1.RatingsPromptStage.None]);
        });
        test("'No Thanks' click at RatingsPromptStage.Rate not available when rate url does not exist (unexpected scenario)", function () {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.showRatingsPrompt = true;
            var ratingsPanel = m.component(ratingsPanel_1.RatingsPanel, {clipperState:clipperState});
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(ratingsPanel);
            // skip to RATE panel
            controllerInstance.setState({ currentRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.Rate });
            m.redraw(true);
            var rateNegative = document.getElementById(constants_1.Constants.Ids.ratingsButtonRateNo);
            ok(objectUtils_1.ObjectUtils.isNullOrUndefined(rateNegative), "'No Thanks' button should not exist");
            strictEqual(ratingsHelper_1.RatingsPromptStage[controllerInstance.state.userSelectedRatingsPromptStage], ratingsHelper_1.RatingsPromptStage[ratingsHelper_1.RatingsPromptStage.None]);
        });
        test("'Feedback' click at RatingsPromptStage.Feedback goes to RatingsPromptStage.End when feedback url exists", function () {
            settings_1.Settings.setSettingsJsonForTesting({
                "LogCategory_RatingsPrompt": {
                    "Value": TestConstants.LogCategories.oneNoteClipperUsage
                }
            });
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastSeenVersion, "3.1.0");
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.showRatingsPrompt = true;
            var ratingsPanel = m.component(ratingsPanel_1.RatingsPanel, {clipperState:clipperState});
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(ratingsPanel);
            // skip to FEEDBACK panel
            controllerInstance.setState({ currentRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.Feedback });
            m.redraw(true);
            var feedbackPositive = document.getElementById(constants_1.Constants.Ids.ratingsButtonFeedbackYes);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                feedbackPositive.click();
            });
            strictEqual(ratingsHelper_1.RatingsPromptStage[controllerInstance.state.userSelectedRatingsPromptStage], ratingsHelper_1.RatingsPromptStage[ratingsHelper_1.RatingsPromptStage.End]);
        });
        test("'Feedback' click at RatingsPromptStage.Feedback not available when feedback url does not exist (unexpected scenario)", function () {
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastSeenVersion, "3.1.0");
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.showRatingsPrompt = true;
            var ratingsPanel = m.component(ratingsPanel_1.RatingsPanel, {clipperState:clipperState});
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(ratingsPanel);
            // skip to FEEDBACK panel
            controllerInstance.setState({ currentRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.Feedback });
            m.redraw(true);
            var feedbackPositive = document.getElementById(constants_1.Constants.Ids.ratingsButtonFeedbackYes);
            ok(objectUtils_1.ObjectUtils.isNullOrUndefined(feedbackPositive), "'Feedback' button should not exist");
            strictEqual(ratingsHelper_1.RatingsPromptStage[controllerInstance.state.userSelectedRatingsPromptStage], ratingsHelper_1.RatingsPromptStage[ratingsHelper_1.RatingsPromptStage.None]);
        });
        test("'No Thanks' click at RatingsPromptStage.Feedback goes to RatingsPromptStage.None when feedback url exists", function () {
            settings_1.Settings.setSettingsJsonForTesting({
                "LogCategory_RatingsPrompt": {
                    "Value": TestConstants.LogCategories.oneNoteClipperUsage
                }
            });
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastSeenVersion, "3.1.0");
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.showRatingsPrompt = true;
            var ratingsPanel = m.component(ratingsPanel_1.RatingsPanel, {clipperState:clipperState});
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(ratingsPanel);
            // skip to FEEDBACK panel
            controllerInstance.setState({ currentRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.Feedback });
            m.redraw(true);
            var feedbackNegative = document.getElementById(constants_1.Constants.Ids.ratingsButtonFeedbackNo);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                feedbackNegative.click();
            });
            strictEqual(ratingsHelper_1.RatingsPromptStage[controllerInstance.state.userSelectedRatingsPromptStage], ratingsHelper_1.RatingsPromptStage[ratingsHelper_1.RatingsPromptStage.None]);
        });
        test("'No Thanks' click at RatingsPromptStage.Feedback not available when feedback url does not exist (unexpected scenario)", function () {
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastSeenVersion, "3.1.0");
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.showRatingsPrompt = true;
            var ratingsPanel = m.component(ratingsPanel_1.RatingsPanel, {clipperState:clipperState});
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(ratingsPanel);
            // skip to FEEDBACK panel
            controllerInstance.setState({ currentRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.Feedback });
            m.redraw(true);
            var feedbackNegative = document.getElementById(constants_1.Constants.Ids.ratingsButtonFeedbackNo);
            ok(objectUtils_1.ObjectUtils.isNullOrUndefined(feedbackNegative), "'No Thanks' button should not exist");
            strictEqual(ratingsHelper_1.RatingsPromptStage[controllerInstance.state.userSelectedRatingsPromptStage], ratingsHelper_1.RatingsPromptStage[ratingsHelper_1.RatingsPromptStage.None]);
        });
    };
    return RatingsPanelTests;
}(testModule_1.TestModule));
exports.RatingsPanelTests = RatingsPanelTests;
(new RatingsPanelTests()).runTests();
