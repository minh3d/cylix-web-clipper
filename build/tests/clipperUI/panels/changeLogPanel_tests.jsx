"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../scripts/constants");
var changeLogPanel_1 = require("../../../scripts/clipperUI/panels/changeLogPanel");
var mithrilUtils_1 = require("../../mithrilUtils");
var mockUpdates_1 = require("../../mockUpdates");
var testModule_1 = require("../../testModule");
var ChangeLogPanelTests = (function (_super) {
    __extends(ChangeLogPanelTests, _super);
    function ChangeLogPanelTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ChangeLogPanelTests.prototype.module = function () {
        return "changeLogPanel";
    };
    ChangeLogPanelTests.prototype.tests = function () {
        test("For a single update containing multiple changes, those changes should be displayed", function () {
            var updates = mockUpdates_1.MockUpdates.getMockUpdates();
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(<changeLogPanel_1.ChangeLogPanel updates={updates}/>);
            var changesElements = document.getElementsByClassName(constants_1.Constants.Classes.changes);
            strictEqual(changesElements.length, 1);
            // Get all changes from the mock updates
            var mockChanges = updates.map(function (update) { return update.changes; });
            var flattenedMockChanges = mockChanges.reduce(function (prev, cur) { return prev.concat(cur); });
            var changes = changesElements[0];
            var changeElements = changes.getElementsByClassName(constants_1.Constants.Classes.change);
            strictEqual(changeElements.length, flattenedMockChanges.length);
        });
        test("For multiple updates containing multiple changes each, all changes should be displayed", function () {
            var updates = mockUpdates_1.MockUpdates.getMockMultipleUpdates();
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(<changeLogPanel_1.ChangeLogPanel updates={updates}/>);
            var changesElements = document.getElementsByClassName(constants_1.Constants.Classes.changes);
            strictEqual(changesElements.length, 1);
            // Get all changes from the mock updates
            var mockChanges = updates.map(function (update) { return update.changes; });
            var flattenedMockChanges = mockChanges.reduce(function (prev, cur) { return prev.concat(cur); });
            var changes = changesElements[0];
            var changeElements = changes.getElementsByClassName(constants_1.Constants.Classes.change);
            strictEqual(changeElements.length, flattenedMockChanges.length);
        });
        test("For a single update containing multiple changes, check that their titles and descriptions are being displayed in order", function () {
            var updates = mockUpdates_1.MockUpdates.getMockUpdates();
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(<changeLogPanel_1.ChangeLogPanel updates={updates}/>);
            var changesElements = document.getElementsByClassName(constants_1.Constants.Classes.changes);
            // Get all changes from the mock updates
            var mockChanges = updates.map(function (update) { return update.changes; });
            var flattenedMockChanges = mockChanges.reduce(function (prev, cur) { return prev.concat(cur); });
            var changes = changesElements[0];
            var changeElements = changes.getElementsByClassName(constants_1.Constants.Classes.change);
            for (var i = 0; i < changeElements.length; i++) {
                // Check title
                var changeTitleElements = changeElements[i].getElementsByClassName(constants_1.Constants.Classes.changeTitle);
                strictEqual(changeTitleElements.length, 1);
                strictEqual(changeTitleElements[0].innerText, flattenedMockChanges[i].title);
                // Check description
                var changeDescriptionElements = changeElements[i].getElementsByClassName(constants_1.Constants.Classes.changeDescription);
                strictEqual(changeDescriptionElements.length, 1);
                strictEqual(changeDescriptionElements[0].innerText, flattenedMockChanges[i].description);
            }
        });
        test("For an update containing some changes with image urls, check that they get rendered if they have an image, and not rendered if they don't", function () {
            var updates = mockUpdates_1.MockUpdates.getMockUpdatesWithSomeImages();
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(<changeLogPanel_1.ChangeLogPanel updates={updates}/>);
            var changesElements = document.getElementsByClassName(constants_1.Constants.Classes.changes);
            // Get all changes from the mock updates
            var mockChanges = updates.map(function (update) { return update.changes; });
            var flattenedMockChanges = mockChanges.reduce(function (prev, cur) { return prev.concat(cur); });
            var changes = changesElements[0];
            var changeElements = changes.getElementsByClassName(constants_1.Constants.Classes.change);
            for (var i = 0; i < changeElements.length; i++) {
                var changeImageElements = changeElements[i].getElementsByClassName(constants_1.Constants.Classes.changeImage);
                strictEqual(changeImageElements.length, flattenedMockChanges[i].imageUrl ? 1 : 0);
                if (flattenedMockChanges[i].imageUrl) {
                    var imgElements = changeImageElements[0].getElementsByTagName("IMG");
                    strictEqual(imgElements.length, 1);
                    strictEqual(imgElements[0].src, flattenedMockChanges[i].imageUrl);
                }
            }
        });
        test("For multiple updates containing multiple changes each, check that their titles and descriptions are being displayed in order", function () {
            var updates = mockUpdates_1.MockUpdates.getMockMultipleUpdates();
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(<changeLogPanel_1.ChangeLogPanel updates={updates}/>);
            var changesElements = document.getElementsByClassName(constants_1.Constants.Classes.changes);
            // Get all changes from the mock updates
            var mockChanges = updates.map(function (update) { return update.changes; });
            var flattenedMockChanges = mockChanges.reduce(function (prev, cur) { return prev.concat(cur); });
            var changes = changesElements[0];
            var changeElements = changes.getElementsByClassName(constants_1.Constants.Classes.change);
            for (var i = 0; i < changeElements.length; i++) {
                // Check title
                var changeTitleElements = changeElements[i].getElementsByClassName(constants_1.Constants.Classes.changeTitle);
                strictEqual(changeTitleElements.length, 1);
                strictEqual(changeTitleElements[0].innerText, flattenedMockChanges[i].title);
                // Check description
                var changeDescriptionElements = changeElements[i].getElementsByClassName(constants_1.Constants.Classes.changeDescription);
                strictEqual(changeDescriptionElements.length, 1);
                strictEqual(changeDescriptionElements[0].innerText, flattenedMockChanges[i].description);
            }
        });
    };
    return ChangeLogPanelTests;
}(testModule_1.TestModule));
exports.ChangeLogPanelTests = ChangeLogPanelTests;
(new ChangeLogPanelTests()).runTests();
