"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../scripts/constants");
var clipMode_1 = require("../../../scripts/clipperUI/clipMode");
var status_1 = require("../../../scripts/clipperUI/status");
var clippingPanel_1 = require("../../../scripts/clipperUI/panels/clippingPanel");
var augmentationHelper_1 = require("../../../scripts/contentCapture/augmentationHelper");
var mithrilUtils_1 = require("../../mithrilUtils");
var mockProps_1 = require("../../mockProps");
var testModule_1 = require("../../testModule");
var ClippingPanelTests = (function (_super) {
    __extends(ClippingPanelTests, _super);
    function ClippingPanelTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.stringsJson = require("../../../strings.json");
        return _this;
    }
    ClippingPanelTests.prototype.module = function () {
        return "clippingPanel";
    };
    ClippingPanelTests.prototype.beforeEach = function () {
        this.mockClipperState = mockProps_1.MockProps.getMockClipperState();
        this.mockClipperState.augmentationResult = {
            data: {},
            status: status_1.Status.Succeeded
        };
    };
    ClippingPanelTests.prototype.tests = function () {
        var _this = this;
        test("If the clipmode is full page, the clipping panel should indicate so", function () {
            _this.mockClipperState.currentMode.set(clipMode_1.ClipMode.FullPage);
            mithrilUtils_1.MithrilUtils.mountToFixture(<clippingPanel_1.ClippingPanel clipperState={_this.mockClipperState}/>);
            strictEqual(document.getElementById(constants_1.Constants.Ids.clipperApiProgressContainer).innerText, _this.stringsJson["WebClipper.ClipType.ScreenShot.ProgressLabel"]);
        });
        test("If the clipmode is region, the clipping panel should indicate so", function () {
            _this.mockClipperState.currentMode.set(clipMode_1.ClipMode.Region);
            mithrilUtils_1.MithrilUtils.mountToFixture(<clippingPanel_1.ClippingPanel clipperState={_this.mockClipperState}/>);
            strictEqual(document.getElementById(constants_1.Constants.Ids.clipperApiProgressContainer).innerText, _this.stringsJson["WebClipper.ClipType.Region.ProgressLabel"]);
        });
        test("If the clipmode is article, the clipping panel should indicate so", function () {
            _this.mockClipperState.currentMode.set(clipMode_1.ClipMode.Augmentation);
            _this.mockClipperState.augmentationResult.data.ContentModel = augmentationHelper_1.AugmentationModel.Article;
            mithrilUtils_1.MithrilUtils.mountToFixture(<clippingPanel_1.ClippingPanel clipperState={_this.mockClipperState}/>);
            strictEqual(document.getElementById(constants_1.Constants.Ids.clipperApiProgressContainer).innerText, _this.stringsJson["WebClipper.ClipType.Article.ProgressLabel"]);
        });
        test("If the clipmode is recipe, the clipping panel should indicate so", function () {
            _this.mockClipperState.currentMode.set(clipMode_1.ClipMode.Augmentation);
            _this.mockClipperState.augmentationResult.data.ContentModel = augmentationHelper_1.AugmentationModel.Recipe;
            mithrilUtils_1.MithrilUtils.mountToFixture(<clippingPanel_1.ClippingPanel clipperState={_this.mockClipperState}/>);
            strictEqual(document.getElementById(constants_1.Constants.Ids.clipperApiProgressContainer).innerText, _this.stringsJson["WebClipper.ClipType.Recipe.ProgressLabel"]);
        });
        test("If the clipmode is product, the clipping panel should indicate so", function () {
            _this.mockClipperState.currentMode.set(clipMode_1.ClipMode.Augmentation);
            _this.mockClipperState.augmentationResult.data.ContentModel = augmentationHelper_1.AugmentationModel.Product;
            mithrilUtils_1.MithrilUtils.mountToFixture(<clippingPanel_1.ClippingPanel clipperState={_this.mockClipperState}/>);
            strictEqual(document.getElementById(constants_1.Constants.Ids.clipperApiProgressContainer).innerText, _this.stringsJson["WebClipper.ClipType.Product.ProgressLabel"]);
        });
        test("If the clipmode is full page and there were augmentation results, the clipping panel should indicate full page", function () {
            _this.mockClipperState.currentMode.set(clipMode_1.ClipMode.FullPage);
            _this.mockClipperState.augmentationResult.data.ContentModel = augmentationHelper_1.AugmentationModel.Product;
            mithrilUtils_1.MithrilUtils.mountToFixture(<clippingPanel_1.ClippingPanel clipperState={_this.mockClipperState}/>);
            strictEqual(document.getElementById(constants_1.Constants.Ids.clipperApiProgressContainer).innerText, _this.stringsJson["WebClipper.ClipType.ScreenShot.ProgressLabel"]);
        });
        test("If the clipmode is some 'new' augmentation mode but we don't have a string to support it, the clipping panel should fall back to full page text", function () {
            _this.mockClipperState.currentMode.set(clipMode_1.ClipMode.Augmentation);
            _this.mockClipperState.augmentationResult.data.ContentModel = augmentationHelper_1.AugmentationModel.BizCard;
            mithrilUtils_1.MithrilUtils.mountToFixture(<clippingPanel_1.ClippingPanel clipperState={_this.mockClipperState}/>);
            strictEqual(document.getElementById(constants_1.Constants.Ids.clipperApiProgressContainer).innerText, _this.stringsJson["WebClipper.ClipType.ScreenShot.ProgressLabel"]);
        });
    };
    return ClippingPanelTests;
}(testModule_1.TestModule));
exports.ClippingPanelTests = ClippingPanelTests;
(new ClippingPanelTests()).runTests();
