"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var sinon = require("sinon");
var constants_1 = require("../../../scripts/constants");
var clippingPanelWithDelayedMessage_1 = require("../../../scripts/clipperUI/panels/clippingPanelWithDelayedMessage");
var mithrilUtils_1 = require("../../mithrilUtils");
var mockProps_1 = require("../../mockProps");
var testModule_1 = require("../../testModule");
var ClippingPanelWithDelayedMessageTests = (function (_super) {
    __extends(ClippingPanelWithDelayedMessageTests, _super);
    function ClippingPanelWithDelayedMessageTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ClippingPanelWithDelayedMessageTests.prototype.module = function () {
        return "clippingPanelWithDelayedMessage";
    };
    ClippingPanelWithDelayedMessageTests.prototype.beforeEach = function () {
        this.mockClipperState = mockProps_1.MockProps.getMockClipperState();
        this.clock = sinon.useFakeTimers();
    };
    ClippingPanelWithDelayedMessageTests.prototype.afterEach = function () {
        this.clock.restore();
    };
    ClippingPanelWithDelayedMessageTests.prototype.tests = function () {
        var _this = this;
        test("If passed a delay of 0, the panel should display the delayed message as soon as it is instantiated", function () {
            var expectedMessage = "hello world";
            mithrilUtils_1.MithrilUtils.mountToFixture(<clippingPanelWithDelayedMessage_1.ClippingPanelWithDelayedMessage clipperState={_this.mockClipperState} delay={0} message={expectedMessage}/>);
            mithrilUtils_1.MithrilUtils.tick(_this.clock, 1);
            var clipProgressDelayedMessage = document.getElementById(constants_1.Constants.Ids.clipProgressDelayedMessage);
            ok(clipProgressDelayedMessage, "The clip progress delayed message should render immediately");
            strictEqual(clipProgressDelayedMessage.innerText, expectedMessage, "The message should be rendered in the clip progress delayed message");
        });
        test("If passed a non-zero positive delay, the panel should not display the delayed message until the delay has been passed", function () {
            var expectedMessage = "hello world";
            var delay = 10000;
            mithrilUtils_1.MithrilUtils.mountToFixture(<clippingPanelWithDelayedMessage_1.ClippingPanelWithDelayedMessage clipperState={_this.mockClipperState} delay={delay} message={expectedMessage}/>);
            var clipProgressDelayedMessage = document.getElementById(constants_1.Constants.Ids.clipProgressDelayedMessage);
            ok(!clipProgressDelayedMessage, "The clip progress delayed message should not render immediately");
            mithrilUtils_1.MithrilUtils.tick(_this.clock, delay - 1);
            clipProgressDelayedMessage = document.getElementById(constants_1.Constants.Ids.clipProgressDelayedMessage);
            ok(!clipProgressDelayedMessage, "The clip progress delayed message should not render just before the delay");
            mithrilUtils_1.MithrilUtils.tick(_this.clock, 1);
            clipProgressDelayedMessage = document.getElementById(constants_1.Constants.Ids.clipProgressDelayedMessage);
            ok(clipProgressDelayedMessage, "The clip progress delayed message should render after the delay");
            strictEqual(clipProgressDelayedMessage.innerText, expectedMessage, "The message should be rendered in the clip progress delayed message");
        });
        test("If passed a delay < 0, the panel should display the delayed message as soon as it is instantiated", function () {
            var expectedMessage = "hello world";
            mithrilUtils_1.MithrilUtils.mountToFixture(<clippingPanelWithDelayedMessage_1.ClippingPanelWithDelayedMessage clipperState={_this.mockClipperState} delay={-10000} message={expectedMessage}/>);
            mithrilUtils_1.MithrilUtils.tick(_this.clock, 1);
            var clipProgressDelayedMessage = document.getElementById(constants_1.Constants.Ids.clipProgressDelayedMessage);
            ok(clipProgressDelayedMessage, "The clip progress delayed message should render immediately");
            strictEqual(clipProgressDelayedMessage.innerText, expectedMessage, "The message should be rendered in the clip progress delayed message");
        });
    };
    return ClippingPanelWithDelayedMessageTests;
}(testModule_1.TestModule));
exports.ClippingPanelWithDelayedMessageTests = ClippingPanelWithDelayedMessageTests;
(new ClippingPanelWithDelayedMessageTests()).runTests();
