"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var frontEndGlobals_1 = require("../../scripts/clipperUI/frontEndGlobals");
var ratingsHelper_1 = require("../../scripts/clipperUI/ratingsHelper");
var clipperStorageKeys_1 = require("../../scripts/storage/clipperStorageKeys");
var constants_1 = require("../../scripts/constants");
var clientType_1 = require("../../scripts/clientType");
var objectUtils_1 = require("../../scripts/objectUtils");
var settings_1 = require("../../scripts/settings");
var stringUtils_1 = require("../../scripts/stringUtils");
var mockProps_1 = require("../mockProps");
var testModule_1 = require("../testModule");
var TestConstants;
(function (TestConstants) {
    var LogCategories;
    (function (LogCategories) {
        LogCategories.oneNoteClipperUsage = "OneNoteClipperUsage";
    })(LogCategories = TestConstants.LogCategories || (TestConstants.LogCategories = {}));
    var Urls;
    (function (Urls) {
        Urls.clipperFeedbackUrl = "https://www.onenote.com/feedback";
    })(Urls = TestConstants.Urls || (TestConstants.Urls = {}));
})(TestConstants || (TestConstants = {}));
var RatingsHelperTests = (function (_super) {
    __extends(RatingsHelperTests, _super);
    function RatingsHelperTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RatingsHelperTests.prototype.module = function () {
        return "ratingsHelper";
    };
    RatingsHelperTests.prototype.beforeEach = function () {
        settings_1.Settings.setSettingsJsonForTesting({});
        ratingsHelper_1.RatingsHelper.preCacheNeededValues();
    };
    RatingsHelperTests.prototype.tests = function () {
        test("setNumSuccessfulClipsRatingsEnablement does not set the value when ClipperStorageKeys.doNotPromptRatings is set", function (assert) {
            var done = assert.async();
            var numClips = 12;
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClips, numClips.toString());
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.doNotPromptRatings, "true");
            ratingsHelper_1.RatingsHelper.setNumSuccessfulClipsRatingsEnablement();
            frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClipsRatingsEnablement, function (numClipsAnchorAsStr) {
                ok(objectUtils_1.ObjectUtils.isNullOrUndefined(numClipsAnchorAsStr));
                done();
            });
        });
        test("setNumSuccessfulClipsRatingsEnablement does not overwrite the value when it already exists", function (assert) {
            var done = assert.async();
            var numClips = 12;
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClips, numClips.toString());
            var expectedStorageValue = 999;
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClipsRatingsEnablement, expectedStorageValue.toString());
            ratingsHelper_1.RatingsHelper.setNumSuccessfulClipsRatingsEnablement();
            frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClipsRatingsEnablement, function (numClipsAnchorAsStr) {
                strictEqual(parseInt(numClipsAnchorAsStr, 10), expectedStorageValue);
                done();
            });
        });
        test("setNumSuccessfulClipsRatingsEnablement sets the value to (ClipperStorageKeys.numSuccessfulClips - 1) when applicable", function (assert) {
            var done = assert.async();
            var numClips = 12;
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClips, numClips.toString());
            ratingsHelper_1.RatingsHelper.setNumSuccessfulClipsRatingsEnablement();
            frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClipsRatingsEnablement, function (numClipsAnchorAsStr) {
                strictEqual(parseInt(numClipsAnchorAsStr, 10), numClips - 1);
                done();
            });
        });
        test("clipSuccessDelayIsOver returns false when numClips is invalid", function () {
            var invalidParams = [undefined, NaN, -1];
            for (var _i = 0, invalidParams_1 = invalidParams; _i < invalidParams_1.length; _i++) {
                var numClips = invalidParams_1[_i];
                var over = ratingsHelper_1.RatingsHelper.clipSuccessDelayIsOver(numClips);
                strictEqual(over, false, "numClips is invalid with value " + numClips);
            }
        });
        test("clipSuccessDelayIsOver returns false when numClips is out of range", function () {
            var outOfRangeParams = [0, constants_1.Constants.Settings.minClipSuccessForRatingsPrompt - 1, constants_1.Constants.Settings.maxClipSuccessForRatingsPrompt + 1];
            for (var _i = 0, outOfRangeParams_1 = outOfRangeParams; _i < outOfRangeParams_1.length; _i++) {
                var numClips = outOfRangeParams_1[_i];
                var over = ratingsHelper_1.RatingsHelper.clipSuccessDelayIsOver(numClips);
                strictEqual(over, false, "numClips is out of range with value " + numClips);
            }
        });
        test("clipSuccessDelayIsOver returns true when numClips is in range", function () {
            var validParams = [constants_1.Constants.Settings.minClipSuccessForRatingsPrompt, constants_1.Constants.Settings.minClipSuccessForRatingsPrompt + 1, constants_1.Constants.Settings.maxClipSuccessForRatingsPrompt - 1, constants_1.Constants.Settings.maxClipSuccessForRatingsPrompt];
            for (var _i = 0, validParams_1 = validParams; _i < validParams_1.length; _i++) {
                var numClips = validParams_1[_i];
                var over = ratingsHelper_1.RatingsHelper.clipSuccessDelayIsOver(numClips);
                strictEqual(over, true, "numClips is valid with value " + numClips);
            }
        });
        test("clipSuccessDelayIsOver returns false when anchorClipValue is greater than numClips", function () {
            var numClips = constants_1.Constants.Settings.minClipSuccessForRatingsPrompt;
            var anchorClipValue = numClips + 1;
            var over = ratingsHelper_1.RatingsHelper.clipSuccessDelayIsOver(numClips, anchorClipValue);
            strictEqual(over, false, "anchorClipValue should never be greater than numClips");
        });
        test("clipSuccessDelayIsOver returns false when anchorClipValue is equal to numClips", function () {
            var numClips = constants_1.Constants.Settings.minClipSuccessForRatingsPrompt;
            var anchorClipValue = numClips;
            var over = ratingsHelper_1.RatingsHelper.clipSuccessDelayIsOver(numClips, anchorClipValue);
            strictEqual(over, false, "anchorClipValue should never be equal to numClips");
        });
        test("clipSuccessDelayIsOver returns false when (numClips - anchorClipValue) is out of range", function () {
            var anchorClipValue = 1;
            var outOfRangeParams = [0, constants_1.Constants.Settings.minClipSuccessForRatingsPrompt - 1, constants_1.Constants.Settings.maxClipSuccessForRatingsPrompt + 1];
            for (var _i = 0, outOfRangeParams_2 = outOfRangeParams; _i < outOfRangeParams_2.length; _i++) {
                var outOfRange = outOfRangeParams_2[_i];
                var numClips = outOfRange + anchorClipValue;
                var over = ratingsHelper_1.RatingsHelper.clipSuccessDelayIsOver(numClips, anchorClipValue);
                strictEqual(over, false, "(numClips - anchorClipValue) should be out of range. numClips: " + numClips + ". anchorClipValue: " + anchorClipValue);
            }
        });
        test("clipSuccessDelayIsOver returns true when anchorClipValue is invalid (and defaults to 0) but numClips is in range", function () {
            var invalidAnchors = [undefined, NaN, -1];
            for (var _i = 0, invalidAnchors_1 = invalidAnchors; _i < invalidAnchors_1.length; _i++) {
                var anchorClipValue = invalidAnchors_1[_i];
                var over = ratingsHelper_1.RatingsHelper.clipSuccessDelayIsOver(constants_1.Constants.Settings.minClipSuccessForRatingsPrompt, anchorClipValue);
                strictEqual(over, true, "anchorClipValue is invalid with value " + anchorClipValue + ", but should be ignored since numClips is valid with value " + constants_1.Constants.Settings.minClipSuccessForRatingsPrompt);
            }
        });
        test("clipSuccessDelayIsOver returns true when (numClips - anchorClipValue) is in range", function () {
            var anchorClipValue = 100;
            var validParams = [constants_1.Constants.Settings.minClipSuccessForRatingsPrompt, constants_1.Constants.Settings.minClipSuccessForRatingsPrompt + 1, constants_1.Constants.Settings.maxClipSuccessForRatingsPrompt - 1, constants_1.Constants.Settings.maxClipSuccessForRatingsPrompt];
            for (var _i = 0, validParams_2 = validParams; _i < validParams_2.length; _i++) {
                var outOfRange = validParams_2[_i];
                var numClips = outOfRange + anchorClipValue;
                var over = ratingsHelper_1.RatingsHelper.clipSuccessDelayIsOver(numClips, anchorClipValue);
                strictEqual(over, true, "(numClips - anchorClipValue) should be valid. numClips: " + numClips + ". anchorClipValue: " + anchorClipValue);
            }
        });
        test("badRatingVersionDelayIsOver returns false when version string params are invalid", function () {
            var invalidParams = [undefined, "", "version", "12345", "a.b.c", "a.33.0", "33.b.0"];
            for (var _i = 0, invalidParams_2 = invalidParams; _i < invalidParams_2.length; _i++) {
                var badRatingVersion = invalidParams_2[_i];
                if (badRatingVersion === undefined) {
                    continue; // this is a valid state for badRatingVersion
                }
                for (var _a = 0, invalidParams_3 = invalidParams; _a < invalidParams_3.length; _a++) {
                    var lastSeenVersion = invalidParams_3[_a];
                    var over = ratingsHelper_1.RatingsHelper.badRatingVersionDelayIsOver(badRatingVersion, lastSeenVersion);
                    strictEqual(over, false, "one or both version string params is/are invalid. badRatingVersion: " + badRatingVersion + ". lastSeenVersion: " + lastSeenVersion);
                }
            }
        });
        test("badRatingVersionDelayIsOver returns false when there has not been a version update since the bad rating", function () {
            var invalidParams = ["-999.-999.-999", "2.999.999", "3.-999.999", "3.1.999", "3.2.0", "3.2.999"];
            var badRatingVersion = "3.2.0";
            for (var _i = 0, invalidParams_4 = invalidParams; _i < invalidParams_4.length; _i++) {
                var lastSeenVersion = invalidParams_4[_i];
                var over = ratingsHelper_1.RatingsHelper.badRatingVersionDelayIsOver(badRatingVersion, lastSeenVersion);
                strictEqual(over, false, "there has not been a version update since " + badRatingVersion + ". lastSeenVersion: " + lastSeenVersion);
            }
        });
        test("badRatingVersionDelayIsOver returns true when there has been a version update since the bad rating", function () {
            var invalidParams = ["3.3.0", "4.0.0"];
            var badRatingVersion = "3.2.0";
            for (var _i = 0, invalidParams_5 = invalidParams; _i < invalidParams_5.length; _i++) {
                var lastSeenVersion = invalidParams_5[_i];
                var over = ratingsHelper_1.RatingsHelper.badRatingVersionDelayIsOver(badRatingVersion, lastSeenVersion);
                strictEqual(over, true, "there has been a version update since " + badRatingVersion + ". lastSeenVersion: " + lastSeenVersion);
            }
        });
        test("badRatingTimingDelayIsOver returns false when date string params are invalid", function () {
            var invalidParams = [undefined, NaN, constants_1.Constants.Settings.maximumJSTimeValue + 1, (constants_1.Constants.Settings.maximumJSTimeValue * -1) - 1];
            for (var _i = 0, invalidParams_6 = invalidParams; _i < invalidParams_6.length; _i++) {
                var badRatingDate_1 = invalidParams_6[_i];
                if (badRatingDate_1 === undefined || isNaN(badRatingDate_1)) {
                    continue; // these are valid states for badRatingDate
                }
                for (var _a = 0, invalidParams_7 = invalidParams; _a < invalidParams_7.length; _a++) {
                    var currentDate_1 = invalidParams_7[_a];
                    var over_1 = ratingsHelper_1.RatingsHelper.badRatingTimingDelayIsOver(badRatingDate_1, currentDate_1);
                    strictEqual(over_1, false, "one or both date number params is/are invalid. badRatingDate: " + badRatingDate_1 + ". currentDate: " + currentDate_1);
                }
            }
            var currentDate = Date.now();
            var badRatingDate = currentDate + 1;
            var over = ratingsHelper_1.RatingsHelper.badRatingTimingDelayIsOver(badRatingDate, currentDate);
            strictEqual(over, false, "bad rating somehow occurred after the current date. badRatingDate: " + badRatingDate + ". currentDate: " + currentDate);
        });
        test("badRatingTimingDelayIsOver returns false when the time between bad rating and the current date is less than the minimum allowed span", function () {
            var minTime = constants_1.Constants.Settings.minTimeBetweenBadRatings;
            var timeBetweenRatings = minTime - 1;
            var currentDate = Date.now();
            var badRatingDate = currentDate - timeBetweenRatings;
            var over = ratingsHelper_1.RatingsHelper.badRatingTimingDelayIsOver(badRatingDate, currentDate);
            strictEqual(over, false, "Timespan between bad rating and current date means the delay is not over. badRatingDate: " + new Date(badRatingDate) + ". currentDate: " + new Date(currentDate));
        });
        test("badRatingTimingDelayIsOver returns true when the time between bad rating and the current date is greater than or equal to the minimum allowed span", function () {
            var minTime = constants_1.Constants.Settings.minTimeBetweenBadRatings;
            var timeBetweenRatings = [minTime, minTime + 1];
            var currentDate = Date.now();
            for (var _i = 0, timeBetweenRatings_1 = timeBetweenRatings; _i < timeBetweenRatings_1.length; _i++) {
                var time = timeBetweenRatings_1[_i];
                var badRatingDate = currentDate - time;
                var over = ratingsHelper_1.RatingsHelper.badRatingTimingDelayIsOver(badRatingDate, currentDate);
                strictEqual(over, true, "Timespan between bad rating and current date means the delay is over. badRatingDate: " + new Date(badRatingDate) + ". currentDate: " + new Date(currentDate));
            }
        });
        test("getFeedbackUrlIfExists returns undefined if log category for ratings prompt does not exist", function () {
            var url = ratingsHelper_1.RatingsHelper.getFeedbackUrlIfExists({});
            strictEqual(url, undefined, "setting for log category for ratings prompt does not exist");
            settings_1.Settings.setSettingsJsonForTesting({
                "LogCategory_RatingsPrompt": {
                    "Value": undefined
                }
            });
            url = ratingsHelper_1.RatingsHelper.getFeedbackUrlIfExists({});
            strictEqual(url, undefined, "value for log category for ratings prompt is undefined");
            settings_1.Settings.setSettingsJsonForTesting({
                "LogCategory_RatingsPrompt": {
                    "Value": ""
                }
            });
            url = ratingsHelper_1.RatingsHelper.getFeedbackUrlIfExists({});
            strictEqual(url, undefined, "value for log category for ratings prompt is empty");
        });
        test("getFeedbackUrlIfExists returns a feedback url if log category for ratings prompt exists", function () {
            settings_1.Settings.setSettingsJsonForTesting({
                "LogCategory_RatingsPrompt": {
                    "Value": TestConstants.LogCategories.oneNoteClipperUsage
                }
            });
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            frontEndGlobals_1.Clipper.sessionId.set(stringUtils_1.StringUtils.generateGuid());
            var expectedFeedbackUrl = "https://www.onenote.com/feedback"
                + "?LogCategory=" + settings_1.Settings.getSetting("LogCategory_RatingsPrompt")
                + "&originalUrl=" + clipperState.pageInfo.rawUrl
                + "&clipperId=" + clipperState.clientInfo.clipperId
                + "&usid=" + frontEndGlobals_1.Clipper.getUserSessionId()
                + "&type=" + clientType_1.ClientType[clipperState.clientInfo.clipperType]
                + "&version=" + clipperState.clientInfo.clipperVersion;
            var url = ratingsHelper_1.RatingsHelper.getFeedbackUrlIfExists(clipperState);
            strictEqual(url, expectedFeedbackUrl);
        });
        test("getRateUrlIfExists returns undefined if ClientType/ClipperType is invalid", function () {
            var url = ratingsHelper_1.RatingsHelper.getRateUrlIfExists(undefined);
            strictEqual(url, undefined, "Ratings url should be undefined on an undefined ClientType");
            var invalidClientType = 999;
            url = ratingsHelper_1.RatingsHelper.getRateUrlIfExists(invalidClientType);
            strictEqual(url, undefined, "Ratings url should be undefined on an invalid ClientType");
        });
        test("getRateUrlIfExists returns undefined if a client's rate url does not exist", function () {
            var clientType = clientType_1.ClientType.ChromeExtension;
            var settingName = clientType_1.ClientType[clientType] + ratingsHelper_1.RatingsHelper.rateUrlSettingNameSuffix;
            var url = ratingsHelper_1.RatingsHelper.getRateUrlIfExists(clientType);
            strictEqual(url, undefined, "Ratings url should be undefined if " + settingName + " is not in settings");
            settings_1.Settings.setSettingsJsonForTesting({
                "ChromeExtension_RatingUrl": {
                    "Value": undefined
                }
            });
            url = ratingsHelper_1.RatingsHelper.getRateUrlIfExists(clientType);
            strictEqual(url, undefined, "Ratings url should be undefined if the value of " + settingName + " is undefined");
            settings_1.Settings.setSettingsJsonForTesting({
                "ChromeExtension_RatingUrl": {
                    "Value": ""
                }
            });
            url = ratingsHelper_1.RatingsHelper.getRateUrlIfExists(clientType);
            strictEqual(url, undefined, "Ratings url should be undefined if the value of " + settingName + " is empty");
        });
        test("getRateUrlIfExists returns a rating url if it exists", function () {
            var clientType = clientType_1.ClientType.ChromeExtension;
            var expectedRatingUrl = "https://chrome.google.com/webstore/detail/onenote-web-clipper/reviews";
            settings_1.Settings.setSettingsJsonForTesting({
                "ChromeExtension_RatingUrl": {
                    "Value": expectedRatingUrl
                }
            });
            var url = ratingsHelper_1.RatingsHelper.getRateUrlIfExists(clientType);
            strictEqual(url, expectedRatingUrl);
        });
        test("ratingsPromptEnabledForClient returns false if ClientType/ClipperType is invalid", function () {
            var isEnabled = ratingsHelper_1.RatingsHelper.ratingsPromptEnabledForClient(undefined);
            strictEqual(isEnabled, false, "Ratings should be disabled on an undefined ClientType");
            var invalidClientType = 999;
            isEnabled = ratingsHelper_1.RatingsHelper.ratingsPromptEnabledForClient(invalidClientType);
            strictEqual(isEnabled, false, "Ratings should be disabled on an invalid ClientType");
        });
        test("ratingsPromptEnabledForClient returns false if a client's enable value does not exist", function () {
            var clientType = clientType_1.ClientType.ChromeExtension;
            var settingName = clientType_1.ClientType[clientType] + ratingsHelper_1.RatingsHelper.ratingsPromptEnabledSettingNameSuffix;
            var isEnabled = ratingsHelper_1.RatingsHelper.ratingsPromptEnabledForClient(clientType);
            strictEqual(isEnabled, false, "Ratings should be disabled if " + settingName + " is not in settings");
            settings_1.Settings.setSettingsJsonForTesting({
                "ChromeExtension_RatingsEnabled": {
                    "Value": undefined
                }
            });
            isEnabled = ratingsHelper_1.RatingsHelper.ratingsPromptEnabledForClient(clientType);
            strictEqual(isEnabled, false, "Ratings should be disabled if the value of " + settingName + " is undefined");
            settings_1.Settings.setSettingsJsonForTesting({
                "ChromeExtension_RatingsEnabled": {
                    "Value": ""
                }
            });
            isEnabled = ratingsHelper_1.RatingsHelper.ratingsPromptEnabledForClient(clientType);
            strictEqual(isEnabled, false, "Ratings should be disabled if the value of " + settingName + " is empty");
        });
        test("ratingsPromptEnabledForClient returns false if a client's enable value is not 'true' (case-insensitive)", function () {
            var clientType = clientType_1.ClientType.ChromeExtension;
            settings_1.Settings.setSettingsJsonForTesting({
                "ChromeExtension_RatingsEnabled": {
                    "Value": "false"
                }
            });
            var isEnabled = ratingsHelper_1.RatingsHelper.ratingsPromptEnabledForClient(clientType);
            strictEqual(isEnabled, false);
            settings_1.Settings.setSettingsJsonForTesting({
                "ChromeExtension_RatingsEnabled": {
                    "Value": "garbage"
                }
            });
            isEnabled = ratingsHelper_1.RatingsHelper.ratingsPromptEnabledForClient(clientType);
            strictEqual(isEnabled, false);
        });
        test("ratingsPromptEnabledForClient returns true if a client's enable value is 'true' (case-insensitive)", function () {
            var clientType = clientType_1.ClientType.ChromeExtension;
            settings_1.Settings.setSettingsJsonForTesting({
                "ChromeExtension_RatingsEnabled": {
                    "Value": "true"
                }
            });
            var isEnabled = ratingsHelper_1.RatingsHelper.ratingsPromptEnabledForClient(clientType);
            strictEqual(isEnabled, true);
            settings_1.Settings.setSettingsJsonForTesting({
                "ChromeExtension_RatingsEnabled": {
                    "Value": "TrUe"
                }
            });
            isEnabled = ratingsHelper_1.RatingsHelper.ratingsPromptEnabledForClient(clientType);
            strictEqual(isEnabled, true);
        });
        test("badRatingAlreadyOccurred returns false when lastBadRatingDate does not exist in storage", function () {
            var alreadyRatedBad = ratingsHelper_1.RatingsHelper.badRatingAlreadyOccurred();
            strictEqual(alreadyRatedBad, false);
        });
        test("badRatingAlreadyOccurred returns false when lastBadRatingDate is not a number", function () {
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingDate, "not a number");
            var alreadyRatedBad = ratingsHelper_1.RatingsHelper.badRatingAlreadyOccurred();
            strictEqual(alreadyRatedBad, false);
        });
        test("badRatingAlreadyOccurred returns false when lastBadRatingDate is a number out of date range", function () {
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingDate, (constants_1.Constants.Settings.maximumJSTimeValue + 1).toString());
            var alreadyRatedBad = ratingsHelper_1.RatingsHelper.badRatingAlreadyOccurred();
            strictEqual(alreadyRatedBad, false);
        });
        test("badRatingAlreadyOccurred returns true when lastBadRatingDate is a valid number", function () {
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingDate, Date.now().toString());
            var alreadyRatedBad = ratingsHelper_1.RatingsHelper.badRatingAlreadyOccurred();
            strictEqual(alreadyRatedBad, true);
        });
        test("shouldShowRatingsPrompt returns hardcoded false when clipperState is undefined", function () {
            var shouldShowRatingsPrompt = ratingsHelper_1.RatingsHelper.shouldShowRatingsPrompt(undefined);
            strictEqual(shouldShowRatingsPrompt, false);
        });
        test("shouldShowRatingsPrompt returns cached false when shouldShowRatingsPrompt is already set to false", function () {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.showRatingsPrompt = false;
            var shouldShowRatingsPrompt = ratingsHelper_1.RatingsHelper.shouldShowRatingsPrompt(clipperState);
            strictEqual(shouldShowRatingsPrompt, false);
        });
        test("shouldShowRatingsPrompt returns cached true when shouldShowRatingsPrompt is already set to true", function () {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.showRatingsPrompt = true;
            var shouldShowRatingsPrompt = ratingsHelper_1.RatingsHelper.shouldShowRatingsPrompt(clipperState);
            strictEqual(shouldShowRatingsPrompt, true);
        });
        test("shouldShowRatingsPrompt returns false when ratings prompt is disabled for the client", function () {
            settings_1.Settings.setSettingsJsonForTesting({
                "ChromeExtension_RatingsEnabled": {
                    "Value": "false"
                }
            });
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            var shouldShowRatingsPrompt = ratingsHelper_1.RatingsHelper.shouldShowRatingsPrompt(clipperState);
            strictEqual(shouldShowRatingsPrompt, false);
        });
        test("shouldShowRatingsPrompt returns false when do not prompt ratings is set in storage to 'true' (case-insensitive)", function () {
            settings_1.Settings.setSettingsJsonForTesting({
                "ChromeExtension_RatingsEnabled": {
                    "Value": "true"
                }
            });
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.doNotPromptRatings, "tRuE");
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            var shouldShowRatingsPrompt = ratingsHelper_1.RatingsHelper.shouldShowRatingsPrompt(clipperState);
            strictEqual(shouldShowRatingsPrompt, false);
        });
        test("shouldShowRatingsPrompt returns true when do not prompt ratings is set in storage but to an invalid value", function () {
            settings_1.Settings.setSettingsJsonForTesting({
                "ChromeExtension_RatingsEnabled": {
                    "Value": "true"
                }
            });
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.doNotPromptRatings, "invalid");
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClips, constants_1.Constants.Settings.minClipSuccessForRatingsPrompt.toString());
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClipsRatingsEnablement, "0");
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            var shouldShowRatingsPrompt = ratingsHelper_1.RatingsHelper.shouldShowRatingsPrompt(clipperState);
            strictEqual(shouldShowRatingsPrompt, true);
        });
        test("shouldShowRatingsPrompt returns true when a valid configuration is provided", function () {
            settings_1.Settings.setSettingsJsonForTesting({
                "FirefoxExtension_RatingsEnabled": {
                    "Value": "true"
                }
            });
            var anchorClipValue = constants_1.Constants.Settings.maxClipSuccessForRatingsPrompt + 1;
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClips, (anchorClipValue + constants_1.Constants.Settings.maxClipSuccessForRatingsPrompt).toString());
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClipsRatingsEnablement, anchorClipValue.toString());
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingDate, (Date.now() - constants_1.Constants.Settings.minTimeBetweenBadRatings).toString());
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingVersion, "3.0.9");
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastSeenVersion, "3.1.0");
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.clientInfo.clipperType = clientType_1.ClientType.FirefoxExtension;
            var shouldShowRatingsPrompt = ratingsHelper_1.RatingsHelper.shouldShowRatingsPrompt(clipperState);
            strictEqual(shouldShowRatingsPrompt, true);
        });
        test("shouldShowRatingsPrompt returns false when number of successful clips is below the min", function () {
            settings_1.Settings.setSettingsJsonForTesting({
                "ChromeExtension_RatingsEnabled": {
                    "Value": "true"
                }
            });
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClips, (constants_1.Constants.Settings.minClipSuccessForRatingsPrompt - 1).toString());
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClipsRatingsEnablement, "0");
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingDate, (Date.now() - constants_1.Constants.Settings.minTimeBetweenBadRatings).toString());
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingVersion, "3.0.9");
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastSeenVersion, "3.1.0");
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            var shouldShowRatingsPrompt = ratingsHelper_1.RatingsHelper.shouldShowRatingsPrompt(clipperState);
            strictEqual(shouldShowRatingsPrompt, false);
        });
        test("shouldShowRatingsPrompt returns false when last bad rating date is too recent", function () {
            settings_1.Settings.setSettingsJsonForTesting({
                "ChromeExtension_RatingsEnabled": {
                    "Value": "true"
                }
            });
            var timeDiffInMs = 1000 * 60 * 60 * 24; // to make last bad rating date 1 day sooner than the min time delay
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClips, constants_1.Constants.Settings.minClipSuccessForRatingsPrompt.toString());
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClipsRatingsEnablement, "0");
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingDate, (Date.now() - constants_1.Constants.Settings.minTimeBetweenBadRatings + timeDiffInMs).toString());
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingVersion, "3.0.9");
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastSeenVersion, "3.1.0");
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            var shouldShowRatingsPrompt = ratingsHelper_1.RatingsHelper.shouldShowRatingsPrompt(clipperState);
            strictEqual(shouldShowRatingsPrompt, false);
        });
        test("shouldShowRatingsPrompt returns false when there has not been a significant version update since the last bad rating", function () {
            settings_1.Settings.setSettingsJsonForTesting({
                "ChromeExtension_RatingsEnabled": {
                    "Value": "true"
                }
            });
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClips, constants_1.Constants.Settings.minClipSuccessForRatingsPrompt.toString());
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClipsRatingsEnablement, "0");
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingDate, (Date.now() - constants_1.Constants.Settings.minTimeBetweenBadRatings).toString());
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingVersion, "3.0.9");
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastSeenVersion, "3.0.999");
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            var shouldShowRatingsPrompt = ratingsHelper_1.RatingsHelper.shouldShowRatingsPrompt(clipperState);
            strictEqual(shouldShowRatingsPrompt, false);
        });
        test("shouldShowRatingsPrompt returns false when (numSuccessfulClips - numSuccessfulClipsRatingsEnablement) is out of range", function () {
            settings_1.Settings.setSettingsJsonForTesting({
                "FirefoxExtension_RatingsEnabled": {
                    "Value": "true"
                }
            });
            var anchorClipValue = constants_1.Constants.Settings.maxClipSuccessForRatingsPrompt + 1;
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClips, (anchorClipValue + constants_1.Constants.Settings.maxClipSuccessForRatingsPrompt + 1).toString());
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClipsRatingsEnablement, anchorClipValue.toString());
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.clientInfo.clipperType = clientType_1.ClientType.FirefoxExtension;
            var shouldShowRatingsPrompt = ratingsHelper_1.RatingsHelper.shouldShowRatingsPrompt(clipperState);
            strictEqual(shouldShowRatingsPrompt, false);
        });
    };
    return RatingsHelperTests;
}(testModule_1.TestModule));
exports.RatingsHelperTests = RatingsHelperTests;
(new RatingsHelperTests()).runTests();
