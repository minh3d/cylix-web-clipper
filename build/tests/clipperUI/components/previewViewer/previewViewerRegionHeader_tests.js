"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var previewViewerRegionHeader_1 = require("../../../../scripts/clipperUI/components/previewViewer/previewViewerRegionHeader");
var status_1 = require("../../../../scripts/clipperUI/status");
var constants_1 = require("../../../../scripts/constants");
var mithrilUtils_1 = require("../../../mithrilUtils");
var mockProps_1 = require("../../../mockProps");
var testModule_1 = require("../../../testModule");
var PreviewViewerAugmentationHeaderTests = (function (_super) {
    __extends(PreviewViewerAugmentationHeaderTests, _super);
    function PreviewViewerAugmentationHeaderTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PreviewViewerAugmentationHeaderTests.prototype.module = function () {
        return "previewViewerAugmentationHeader";
    };
    PreviewViewerAugmentationHeaderTests.prototype.beforeEach = function () {
        this.mockClipperState = mockProps_1.MockProps.getMockClipperState();
        this.defaultComponent =
            m.component(previewViewerRegionHeader_1.PreviewViewerRegionHeader, {clipperState:this.mockClipperState});
    };
    PreviewViewerAugmentationHeaderTests.prototype.tests = function () {
        var _this = this;
        test("The addRegionControl should be visible", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            ok(!!document.getElementById(constants_1.Constants.Ids.addRegionControl));
        });
        test("The addRegionControl's buttons should be visible", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            ok(!!document.getElementById(constants_1.Constants.Ids.addAnotherRegionButton));
        });
        test("When clicking on the add region button, the regionResult prop should be set accordingly", function () {
            var previewViewerRegionHeader = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var previousRegionResultData = _this.mockClipperState.regionResult.data;
            var addAnotherRegionButton = document.getElementById(constants_1.Constants.Ids.addAnotherRegionButton);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                addAnotherRegionButton.click();
            });
            deepEqual(previewViewerRegionHeader.props.clipperState.regionResult, { status: status_1.Status.InProgress, data: previousRegionResultData }, "The status of the region result should be in progress, and the data untouched");
        });
    };
    return PreviewViewerAugmentationHeaderTests;
}(testModule_1.TestModule));
exports.PreviewViewerAugmentationHeaderTests = PreviewViewerAugmentationHeaderTests;
(new PreviewViewerAugmentationHeaderTests()).runTests();
