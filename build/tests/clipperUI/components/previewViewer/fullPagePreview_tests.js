"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../../scripts/constants");
var clipMode_1 = require("../../../../scripts/clipperUI/clipMode");
var status_1 = require("../../../../scripts/clipperUI/status");
var fullPagePreview_1 = require("../../../../scripts/clipperUI/components/previewViewer/fullPagePreview");
var mithrilUtils_1 = require("../../../mithrilUtils");
var mockProps_1 = require("../../../mockProps");
var testModule_1 = require("../../../testModule");
var FullPagePreviewTests = (function (_super) {
    __extends(FullPagePreviewTests, _super);
    function FullPagePreviewTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.stringsJson = require("../../../../strings.json");
        return _this;
    }
    FullPagePreviewTests.prototype.module = function () {
        return "fullPagePreview";
    };
    FullPagePreviewTests.prototype.tests = function () {
        var _this = this;
        test("The full page header should be displayed in Full Page mode", function () {
            var mockClipperState = _this.getMockFullPageModeState();
            var defaultComponent = m.component(fullPagePreview_1.FullPagePreview, {clipperState:mockClipperState});
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            ok(!document.getElementById(constants_1.Constants.Ids.addRegionControl), "The region control should not exist");
            ok(!document.getElementById(constants_1.Constants.Ids.highlightControl), "The highlight control should not exist");
            ok(!document.getElementById(constants_1.Constants.Ids.serifControl), "The font family control should not exist");
            ok(!document.getElementById(constants_1.Constants.Ids.decrementFontSize), "The decrement font size button should not exist");
            ok(!document.getElementById(constants_1.Constants.Ids.incrementFontSize), "The increment font size button should not exist");
        });
        test("The editable title of the page should be displayed in the preview title in Full Page mode", function () {
            var mockClipperState = _this.getMockFullPageModeState();
            var defaultComponent = m.component(fullPagePreview_1.FullPagePreview, {clipperState:mockClipperState});
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            strictEqual(previewHeaderInput.value, mockClipperState.previewGlobalInfo.previewTitleText, "The title of the page should be displayed in the preview title");
            ok(!previewHeaderInput.readOnly);
        });
        test("When the call to the full page screenshot fetch has not started, the preview should indicate that it is loading in Full Page mode", function () {
            var clipperState = _this.getMockFullPageModeState();
            clipperState.currentMode.set(clipMode_1.ClipMode.FullPage);
            clipperState.fullPageResult = {
                data: undefined,
                status: status_1.Status.NotStarted
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(fullPagePreview_1.FullPagePreview, {clipperState:clipperState}));
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            strictEqual(previewHeaderInput.value, _this.stringsJson["WebClipper.Preview.LoadingMessage"], "The preview title should display a loading message");
            ok(previewHeaderInput.readOnly);
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            strictEqual(previewBody.getElementsByClassName(constants_1.Constants.Classes.spinner).length, 1, "The spinner should be present in the preview body");
        });
        test("When the call to the full page screenshot fetch is in progress, the preview should indicate that it is loading in Full Page mode", function () {
            var clipperState = _this.getMockFullPageModeState();
            clipperState.currentMode.set(clipMode_1.ClipMode.FullPage);
            clipperState.fullPageResult = {
                data: undefined,
                status: status_1.Status.InProgress
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(fullPagePreview_1.FullPagePreview, {clipperState:clipperState}));
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            strictEqual(previewHeaderInput.value, _this.stringsJson["WebClipper.Preview.LoadingMessage"], "The preview title should display a loading message");
            ok(previewHeaderInput.readOnly);
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            strictEqual(previewBody.getElementsByClassName(constants_1.Constants.Classes.spinner).length, 1, "The spinner should be present in the preview body");
        });
        test("When the call to the full patch screenshot fetch successfully completes, but no data is returned, the preview should indicate no content was found in Full Page mode", function () {
            var clipperState = _this.getMockFullPageModeState();
            clipperState.currentMode.set(clipMode_1.ClipMode.FullPage);
            clipperState.fullPageResult = {
                data: undefined,
                status: status_1.Status.Succeeded
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(fullPagePreview_1.FullPagePreview, {clipperState:clipperState}));
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            strictEqual(previewHeaderInput.value, _this.stringsJson["WebClipper.Preview.NoContentFound"], "The preview title should display a message indicating no content was found");
            ok(previewHeaderInput.readOnly);
            strictEqual(document.getElementById(constants_1.Constants.Ids.previewBody).innerText, "", "The preview body should be empty");
        });
        test("There should be one image rendered for every data url in state in Full Page mode", function () {
            var mockClipperState = _this.getMockFullPageModeState();
            var defaultComponent = m.component(fullPagePreview_1.FullPagePreview, {clipperState:mockClipperState});
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            var images = previewBody.getElementsByTagName("IMG");
            var imageDataInState = mockClipperState.fullPageResult.data;
            strictEqual(images.length, imageDataInState.Images.length);
            for (var i = 0; i < images.length; i++) {
                var image = images[i];
                strictEqual(image.src, "data:image/" + imageDataInState.ImageFormat + ";" + imageDataInState.ImageEncoding + "," + imageDataInState.Images[i]);
            }
        });
        test("When the full page screenshot response is a failure, the preview should display an error message in Full Page mode", function () {
            var expectedMessage = "An error message.";
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.currentMode.set(clipMode_1.ClipMode.FullPage);
            clipperState.fullPageResult = {
                data: {
                    failureMessage: expectedMessage
                },
                status: status_1.Status.Failed
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(fullPagePreview_1.FullPagePreview, {clipperState:clipperState}));
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            strictEqual(previewHeaderInput.value, expectedMessage, "The title of the page should be displayed in the preview title");
            ok(previewHeaderInput.readOnly);
        });
    };
    FullPagePreviewTests.prototype.getMockFullPageModeState = function () {
        var state = mockProps_1.MockProps.getMockClipperState();
        state.currentMode.set(clipMode_1.ClipMode.FullPage);
        state.fullPageResult = {
            data: {
                ImageEncoding: "jpeg",
                ImageFormat: "base64",
                Images: ["abc", "def"]
            },
            status: status_1.Status.Succeeded
        };
        return state;
    };
    return FullPagePreviewTests;
}(testModule_1.TestModule));
exports.FullPagePreviewTests = FullPagePreviewTests;
(new FullPagePreviewTests()).runTests();
