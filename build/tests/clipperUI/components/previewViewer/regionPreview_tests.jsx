"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../../scripts/constants");
var clipMode_1 = require("../../../../scripts/clipperUI/clipMode");
var status_1 = require("../../../../scripts/clipperUI/status");
var regionPreview_1 = require("../../../../scripts/clipperUI/components/previewViewer/regionPreview");
var assert_1 = require("../../../assert");
var mithrilUtils_1 = require("../../../mithrilUtils");
var mockProps_1 = require("../../../mockProps");
var testModule_1 = require("../../../testModule");
var RegionPreviewTests = (function (_super) {
    __extends(RegionPreviewTests, _super);
    function RegionPreviewTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.stringsJson = require("../../../../strings.json");
        return _this;
    }
    RegionPreviewTests.prototype.module = function () {
        return "regionPreview";
    };
    RegionPreviewTests.prototype.tests = function () {
        var _this = this;
        test("The tab order flow from the header to the preview title is correct in Region mode", function () {
            var mockClipperState = _this.getMockRegionModeState();
            var defaultComponent = <regionPreview_1.RegionPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            assert_1.Assert.tabOrderIsIncremental([constants_1.Constants.Ids.addAnotherRegionButton, constants_1.Constants.Ids.previewHeaderInput]);
        });
        test("The tab order flow from the preview title through the region delete buttons is correct in Region mode", function () {
            var mockClipperState = _this.getMockRegionModeState();
            var defaultComponent = <regionPreview_1.RegionPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var elementsInExpectedTabOrder = [
                { name: constants_1.Constants.Ids.previewHeaderInput, elem: document.getElementById(constants_1.Constants.Ids.previewHeaderInput) }
            ];
            var removeButtons = document.getElementsByClassName(constants_1.Constants.Classes.regionSelectionRemoveButton);
            for (var i = 0; i < removeButtons.length; i++) {
                elementsInExpectedTabOrder.push({ name: constants_1.Constants.Classes.regionSelectionRemoveButton + i, elem: removeButtons[i] });
            }
            // Check the flow from the title to the first button
            assert_1.Assert.tabOrderIsIncrementalForElements(elementsInExpectedTabOrder.slice(0, 2));
            for (var i = 2 /* Check buttons */; i < elementsInExpectedTabOrder.length; i++) {
                // Note the '>='
                ok(elementsInExpectedTabOrder[i].elem.tabIndex >= elementsInExpectedTabOrder[i - 1].elem.tabIndex, "Element " + elementsInExpectedTabOrder[i].name + " should have a greater or equal tabIndex than element " + elementsInExpectedTabOrder[i - 1].name);
            }
            for (var i = 0; i < elementsInExpectedTabOrder.length; i++) {
                ok(elementsInExpectedTabOrder[i].elem.tabIndex >= 0);
            }
        });
        test("The region header and all related controls should be displayed in Region mode", function () {
            var mockClipperState = _this.getMockRegionModeState();
            var defaultComponent = <regionPreview_1.RegionPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            ok(document.getElementById(constants_1.Constants.Ids.addRegionControl), "The region control should exist");
            ok(!document.getElementById(constants_1.Constants.Ids.highlightControl), "The highlight control should not exist");
            ok(!document.getElementById(constants_1.Constants.Ids.serifControl), "The font family control should not exist");
            ok(!document.getElementById(constants_1.Constants.Ids.decrementFontSize), "The decrement font size button should not exist");
            ok(!document.getElementById(constants_1.Constants.Ids.incrementFontSize), "The increment font size button should not exist");
        });
        test("The editable title of the page should be displayed in the preview title in Region mode", function () {
            var mockClipperState = _this.getMockRegionModeState();
            var defaultComponent = <regionPreview_1.RegionPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            strictEqual(previewHeaderInput.value, mockClipperState.previewGlobalInfo.previewTitleText, "The title of the page should be displayed in the preview title");
            ok(!previewHeaderInput.readOnly);
        });
        test("There should be one image rendered for every data url in state in Region mode", function () {
            var mockClipperState = _this.getMockRegionModeState();
            var defaultComponent = <regionPreview_1.RegionPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            var selections = previewBody.getElementsByClassName(constants_1.Constants.Classes.regionSelection);
            strictEqual(selections.length, mockClipperState.regionResult.data.length);
            for (var i = 0; i < selections.length; i++) {
                var selection = selections[i];
                var imagesRenderedInSelection = selection.getElementsByClassName(constants_1.Constants.Classes.regionSelectionImage);
                strictEqual(imagesRenderedInSelection.length, 1);
                strictEqual(imagesRenderedInSelection[0].src, mockClipperState.regionResult.data[i], "The image should render the data url in state");
            }
        });
        test("When multiple images are rendered, clicking a middle image's remove button should remove it and only it in Region mode", function () {
            var mockClipperState = _this.getMockRegionModeState();
            var defaultComponent = <regionPreview_1.RegionPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            var selections = previewBody.getElementsByClassName(constants_1.Constants.Classes.regionSelection);
            var indexToRemove = 1;
            // This does not represent a user action, but is done to make testing easier
            mockClipperState.regionResult.data.splice(indexToRemove, 1);
            var removeButton = selections[indexToRemove].getElementsByClassName(constants_1.Constants.Classes.regionSelectionRemoveButton)[0];
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                removeButton.click();
            });
            strictEqual(selections.length, mockClipperState.regionResult.data.length);
            for (var i = 0; i < selections.length; i++) {
                var selection = selections[i];
                var imagesRenderedInSelection = selection.getElementsByClassName(constants_1.Constants.Classes.regionSelectionImage);
                strictEqual(imagesRenderedInSelection.length, 1);
                strictEqual(imagesRenderedInSelection[0].src, mockClipperState.regionResult.data[i], "The image should render the data url in state");
            }
        });
        test("When multiple images are rendered, clicking the first image's remove button should remove it and only it in Region mode", function () {
            var mockClipperState = _this.getMockRegionModeState();
            var defaultComponent = <regionPreview_1.RegionPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            var selections = previewBody.getElementsByClassName(constants_1.Constants.Classes.regionSelection);
            var indexToRemove = 0;
            // This does not represent a user action, but is done to make testing easier
            mockClipperState.regionResult.data.splice(indexToRemove, 1);
            var removeButton = selections[indexToRemove].getElementsByClassName(constants_1.Constants.Classes.regionSelectionRemoveButton)[0];
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                removeButton.click();
            });
            strictEqual(selections.length, mockClipperState.regionResult.data.length);
            for (var i = 0; i < selections.length; i++) {
                var selection = selections[i];
                var imagesRenderedInSelection = selection.getElementsByClassName(constants_1.Constants.Classes.regionSelectionImage);
                strictEqual(imagesRenderedInSelection.length, 1);
                strictEqual(imagesRenderedInSelection[0].src, mockClipperState.regionResult.data[i], "The image should render the data url in state");
            }
        });
        test("When multiple images are rendered, clicking the last image's remove button should remove it and only it in Region mode", function () {
            var mockClipperState = _this.getMockRegionModeState();
            var defaultComponent = <regionPreview_1.RegionPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            var selections = previewBody.getElementsByClassName(constants_1.Constants.Classes.regionSelection);
            var indexToRemove = mockClipperState.regionResult.data.length - 1;
            // This does not represent a user action, but is done to make testing easier
            mockClipperState.regionResult.data.splice(indexToRemove, 1);
            var removeButton = selections[indexToRemove].getElementsByClassName(constants_1.Constants.Classes.regionSelectionRemoveButton)[0];
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                removeButton.click();
            });
            strictEqual(selections.length, mockClipperState.regionResult.data.length);
            for (var i = 0; i < selections.length; i++) {
                var selection = selections[i];
                var imagesRenderedInSelection = selection.getElementsByClassName(constants_1.Constants.Classes.regionSelectionImage);
                strictEqual(imagesRenderedInSelection.length, 1);
                strictEqual(imagesRenderedInSelection[0].src, mockClipperState.regionResult.data[i], "The image should render the data url in state");
            }
        });
        test("When region clipping is disabled, remove image buttons are not rendered. The mode itself is still available because of context image selection mode.", function () {
            var mockClipperState = _this.getMockRegionModeState();
            mockClipperState.injectOptions.enableRegionClipping = false;
            var defaultComponent = <regionPreview_1.RegionPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var regionButtons = document.getElementsByClassName(constants_1.Constants.Classes.regionSelectionRemoveButton);
            strictEqual(regionButtons.length, 0, "No remove image buttons should be rendered");
        });
        test("When region clipping is disabled, the add another region button should not be rendered.", function () {
            var mockClipperState = _this.getMockRegionModeState();
            mockClipperState.injectOptions.enableRegionClipping = false;
            var defaultComponent = <regionPreview_1.RegionPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var addAnotherRegionButton = document.getElementById(constants_1.Constants.Ids.addAnotherRegionButton);
            ok(!addAnotherRegionButton, "The add another region button should not be rendered");
        });
    };
    RegionPreviewTests.prototype.getMockRegionModeState = function () {
        var state = mockProps_1.MockProps.getMockClipperState();
        state.currentMode.set(clipMode_1.ClipMode.Region);
        state.regionResult = {
            data: ["data:image/png;base64,123", "data:image/png;base64,456", "data:image/png;base64,789", "data:image/png;base64,0"],
            status: status_1.Status.Succeeded
        };
        return state;
    };
    return RegionPreviewTests;
}(testModule_1.TestModule));
exports.RegionPreviewTests = RegionPreviewTests;
(new RegionPreviewTests()).runTests();
