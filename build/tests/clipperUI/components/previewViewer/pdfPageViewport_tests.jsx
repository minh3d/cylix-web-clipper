"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../../scripts/constants");
var pdfPageViewport_1 = require("../../../../scripts/clipperUI/components/previewViewer/pdfPageViewport");
var mithrilUtils_1 = require("../../../mithrilUtils");
var testModule_1 = require("../../../testModule");
var pdfDataUrls_1 = require("./pdfDataUrls");
var PdfPageViewportTests = (function (_super) {
    __extends(PdfPageViewportTests, _super);
    function PdfPageViewportTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PdfPageViewportTests.prototype.module = function () {
        return "pdfPageViewport";
    };
    PdfPageViewportTests.prototype.tests = function () {
        test("Given both the imgUrl and dimensions, the imgUrl is rendered in the component in a container div under those dimensions", function () {
            var expectedWidth = pdfDataUrls_1.pdfDataUrlDimensions[0].width;
            var expectedHeight = pdfDataUrls_1.pdfDataUrlDimensions[0].height;
            var pdfPageViewport = mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPageViewport_1.PdfPageViewport viewportDimensions={{ width: parseInt(expectedWidth, 10), height: parseInt(expectedHeight, 10) }} imgUrl={pdfDataUrls_1.pdfDataUrls[0]} index={0}/>);
            var container = mithrilUtils_1.MithrilUtils.getFixture().firstChild;
            var containerComputedStyle = window.getComputedStyle(container);
            strictEqual(containerComputedStyle.maxWidth, expectedWidth, "The container's max width should be the specified viewport width");
            var images = container.getElementsByTagName("img");
            strictEqual(images.length, 1, "There should be one image in the viewport");
            var spinners = container.getElementsByClassName(constants_1.Constants.Classes.spinner);
            strictEqual(spinners.length, 0, "There should be no spinners in the viewport");
            var imageComputedStyle = window.getComputedStyle(images[0]);
            strictEqual(imageComputedStyle.maxWidth, expectedWidth, "The image's max width should be the specified viewport width");
            strictEqual(imageComputedStyle.maxHeight, expectedHeight, "The image's max height should be the specified viewport height");
        });
        test("When not given the imgUrl, the component should render a blank viewport with the specified dimensions", function () {
            var expectedWidth = "300px";
            var expectedHeight = "400px";
            var pdfPageViewport = mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPageViewport_1.PdfPageViewport viewportDimensions={{ width: parseInt(expectedWidth, 10), height: parseInt(expectedHeight, 10) }} index={0}/>);
            var container = mithrilUtils_1.MithrilUtils.getFixture().firstChild;
            var containerComputedStyle = window.getComputedStyle(container);
            strictEqual(containerComputedStyle.width, expectedWidth, "The container's width should be the specified viewport width");
            strictEqual(containerComputedStyle.height, expectedHeight, "The container's height should be the specified viewport width");
            var images = container.getElementsByTagName("img");
            strictEqual(images.length, 0, "There should be no images in the viewport");
            var spinners = container.getElementsByClassName(constants_1.Constants.Classes.spinner);
            strictEqual(spinners.length, 1, "There should be a spinner in the viewport");
            var spinner = spinners[0];
            var spinnerComputerStyle = window.getComputedStyle(spinner);
            strictEqual(spinnerComputerStyle.width, "45px", "The container's width should be 45px");
            strictEqual(spinnerComputerStyle.height, "65px", "The container's height should be 65px");
        });
        test("When not given the imgUrl, the component should render a loading spinner with the specified dimensions given if the viewport is tiny, and the spinner should not exceed the viewport dimensions", function () {
            var expectedWidth = "2px";
            var expectedHeight = "1px";
            var pdfPageViewport = mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPageViewport_1.PdfPageViewport viewportDimensions={{ width: parseInt(expectedWidth, 10), height: parseInt(expectedHeight, 10) }} index={0}/>);
            var container = mithrilUtils_1.MithrilUtils.getFixture().firstChild;
            var containerComputedStyle = window.getComputedStyle(container);
            strictEqual(containerComputedStyle.width, expectedWidth, "The container's width should be the specified viewport width");
            strictEqual(containerComputedStyle.height, expectedHeight, "The container's height should be the specified viewport height + the height of the spinner");
            var images = container.getElementsByTagName("img");
            strictEqual(images.length, 0, "There should be no images in the viewport");
            var spinners = container.getElementsByClassName(constants_1.Constants.Classes.spinner);
            strictEqual(spinners.length, 1, "There should be a spinner in the viewport");
            var spinner = spinners[0];
            var spinnerComputerStyle = window.getComputedStyle(spinner);
            strictEqual(spinnerComputerStyle.width, expectedWidth, "The container's width should not exceed the viewport width");
            strictEqual(spinnerComputerStyle.height, expectedHeight, "The container's height should not exceed the viewport height");
        });
        test("Given the index, the component should render the container with the index stored in the attribute 'data-pageindex'", function () {
            var expectedIndex = 11;
            var pdfPageViewport = mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPageViewport_1.PdfPageViewport viewportDimensions={{ width: 20, height: 15 }} imgUrl={pdfDataUrls_1.pdfDataUrls[0]} index={expectedIndex}/>);
            var container = mithrilUtils_1.MithrilUtils.getFixture().firstChild;
            strictEqual(container.dataset.pageindex, "" + expectedIndex, "The index should be stored in the data-pageindex attribute");
        });
        test("Given the index, but not an imgUrl, the component should still render the container with the index stored in the attribute 'data-pageindex'", function () {
            var expectedIndex = 999;
            var pdfPageViewport = mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPageViewport_1.PdfPageViewport viewportDimensions={{ width: 20, height: 15 }} index={expectedIndex}/>);
            var container = mithrilUtils_1.MithrilUtils.getFixture().firstChild;
            strictEqual(container.dataset.pageindex, "" + expectedIndex, "The index should be stored in the data-pageindex attribute");
        });
    };
    return PdfPageViewportTests;
}(testModule_1.TestModule));
exports.PdfPageViewportTests = PdfPageViewportTests;
(new PdfPageViewportTests()).runTests();
