"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../../scripts/constants");
var clipMode_1 = require("../../../../scripts/clipperUI/clipMode");
var status_1 = require("../../../../scripts/clipperUI/status");
var smartValue_1 = require("../../../../scripts/communicator/smartValue");
var pdfPreview_1 = require("../../../../scripts/clipperUI/components/previewViewer/pdfPreview");
var mithrilUtils_1 = require("../../../mithrilUtils");
var mockProps_1 = require("../../../mockProps");
var testModule_1 = require("../../../testModule");
var mockPdfDocument_1 = require("../../../contentCapture/mockPdfDocument");
var PdfPreviewTests = (function (_super) {
    __extends(PdfPreviewTests, _super);
    function PdfPreviewTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.stringsJson = require("../../../../strings.json");
        return _this;
    }
    PdfPreviewTests.prototype.module = function () {
        return "pdfPreview";
    };
    PdfPreviewTests.prototype.tests = function () {
        var _this = this;
        test("The PDF page header should be displayed in PDF mode", function () {
            var mockClipperState = _this.getMockPdfModeState();
            var defaultComponent = <pdfPreview_1.PdfPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var idsThatShouldNotExist = [constants_1.Constants.Ids.addRegionControl, constants_1.Constants.Ids.highlightControl,
                constants_1.Constants.Ids.serifControl, constants_1.Constants.Ids.decrementFontSize, constants_1.Constants.Ids.incrementFontSize];
            idsThatShouldNotExist.forEach(function (id) {
                ok(!document.getElementById(id), id + " should not exist in the PdfPreviewHeader, but it does");
            });
        });
        test("The editable title of the page should be displayed in the preview title in PDF mode", function () {
            var mockClipperState = _this.getMockPdfModeState();
            var defaultComponent = <pdfPreview_1.PdfPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            strictEqual(previewHeaderInput.value, mockClipperState.previewGlobalInfo.previewTitleText, "The title of the page should be displayed in the preview title");
            ok(!previewHeaderInput.readOnly);
        });
        test("When the call to get the PDF has not started, the preview should indicate that it is loading in PDF Mode", function () {
            var clipperState = _this.getMockPdfModeState();
            clipperState.pdfResult = {
                data: new smartValue_1.SmartValue(),
                status: status_1.Status.NotStarted
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPreview_1.PdfPreview clipperState={clipperState}/>);
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            strictEqual(previewHeaderInput.value, _this.stringsJson["WebClipper.Preview.LoadingMessage"], "The preview title should display a loading message");
            ok(previewHeaderInput.readOnly);
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            strictEqual(previewBody.getElementsByClassName(constants_1.Constants.Classes.spinner).length, 1, "The spinner should be present in the preview body");
        });
        test("When the call to the PDF is in progress, the preview should indicate that it is loading in PDF mode", function () {
            var clipperState = _this.getMockPdfModeState();
            clipperState.pdfResult = {
                data: new smartValue_1.SmartValue(),
                status: status_1.Status.InProgress
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPreview_1.PdfPreview clipperState={clipperState}/>);
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            strictEqual(previewHeaderInput.value, _this.stringsJson["WebClipper.Preview.LoadingMessage"], "The preview title should display a loading message");
            ok(previewHeaderInput.readOnly);
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            strictEqual(previewBody.getElementsByClassName(constants_1.Constants.Classes.spinner).length, 1, "The spinner should be present in the preview body");
        });
        test("When no data urls are present in state, a canvas should be rendered for every page, but the image should not be rendered", function () {
            var clipperState = _this.getMockPdfModeState();
            var pdfPreview = mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPreview_1.PdfPreview clipperState={clipperState}/>);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                pdfPreview.state.renderedPageIndexes = {};
            });
            var imagesCanvases = document.getElementsByClassName(constants_1.Constants.Classes.pdfPreviewImageCanvas);
            strictEqual(imagesCanvases.length, mockPdfDocument_1.MockPdfValues.pageDataUrls.length, "There should be a canvas for every page in the pdf");
            var images = document.getElementsByClassName(constants_1.Constants.Classes.pdfPreviewImage);
            strictEqual(images.length, 0, "There should no pdf images rendered");
        });
        test("When data urls are present in state, a canvas should be rendered for every page, as well as images", function () {
            var clipperState = _this.getMockPdfModeState();
            var pdfPreview = mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPreview_1.PdfPreview clipperState={clipperState}/>);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                pdfPreview.state.renderedPageIndexes = mockPdfDocument_1.MockPdfValues.pageDataUrlsMap;
            });
            var imagesCanvases = document.getElementsByClassName(constants_1.Constants.Classes.pdfPreviewImageCanvas);
            strictEqual(imagesCanvases.length, mockPdfDocument_1.MockPdfValues.pageDataUrls.length, "There should be a canvas for every page in the pdf");
            var images = document.getElementsByClassName(constants_1.Constants.Classes.pdfPreviewImage);
            strictEqual(images.length, mockPdfDocument_1.MockPdfValues.pageDataUrls.length, "There should be an img for every page in the pdf");
            for (var i = 0; i < images.length; i++) {
                var image = images[i];
                strictEqual(image.src, mockPdfDocument_1.MockPdfValues.pageDataUrls[i], "The image src at index " + i + " should match the one in the mock data urls");
            }
        });
        test("When a subset of data urls are present in state, a canvas should be rendered for every page, and every image for each page in the subset", function () {
            var clipperState = _this.getMockPdfModeState();
            var pdfPreview = mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPreview_1.PdfPreview clipperState={clipperState}/>);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                pdfPreview.state.renderedPageIndexes = { "0": mockPdfDocument_1.MockPdfValues.pageDataUrls[0] };
            });
            var imagesCanvases = document.getElementsByClassName(constants_1.Constants.Classes.pdfPreviewImageCanvas);
            strictEqual(imagesCanvases.length, mockPdfDocument_1.MockPdfValues.pageDataUrls.length, "There should be a canvas for every page in the pdf");
            var images = document.getElementsByClassName(constants_1.Constants.Classes.pdfPreviewImage);
            strictEqual(images.length, 1, "There should be an img for every data url present, and in this case, one");
            var image = images[0];
            strictEqual(image.src, mockPdfDocument_1.MockPdfValues.pageDataUrls[0], "The only image shown should be the first image in the mock data urls");
        });
        test("When 'All Pages' is checked in the page range control, all pages should not be grayed out", function () {
            var clipperState = _this.getMockPdfModeState();
            clipperState.pdfPreviewInfo = {
                allPages: true,
                selectedPageRange: "1,3",
                shouldAttachPdf: false
            };
            var pdfPreview = mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPreview_1.PdfPreview clipperState={clipperState}/>);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                pdfPreview.state.renderedPageIndexes = mockPdfDocument_1.MockPdfValues.pageDataUrlsMap;
            });
            var unselectedImages = document.getElementsByClassName(constants_1.Constants.Classes.unselected);
            strictEqual(unselectedImages.length, 0, "There should be no unselected images");
        });
        test("When 'Page Range' is checked in the page range control, but the range is empty, all pages should be grayed out", function () {
            var clipperState = _this.getMockPdfModeState();
            clipperState.pdfPreviewInfo = {
                allPages: false,
                selectedPageRange: "",
                shouldAttachPdf: false
            };
            var pdfPreview = mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPreview_1.PdfPreview clipperState={clipperState}/>);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                pdfPreview.state.renderedPageIndexes = mockPdfDocument_1.MockPdfValues.pageDataUrlsMap;
            });
            var unselectedImages = document.getElementsByClassName(constants_1.Constants.Classes.unselected);
            strictEqual(unselectedImages.length, mockPdfDocument_1.MockPdfValues.pageDataUrls.length, "The number of unselected images should be the length of MockPdfValues.pageDataUrls, if the page range specified is empty");
        });
        test("When 'Page Range' is checked in the page range control, pages outside that range should be grayed out", function () {
            var clipperState = _this.getMockPdfModeState();
            clipperState.pdfPreviewInfo = {
                allPages: false,
                selectedPageRange: "1,3",
                shouldAttachPdf: false
            };
            var pdfPreview = mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPreview_1.PdfPreview clipperState={clipperState}/>);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                pdfPreview.state.renderedPageIndexes = mockPdfDocument_1.MockPdfValues.pageDataUrlsMap;
            });
            var imagesCanvases = document.getElementsByClassName(constants_1.Constants.Classes.pdfPreviewImageCanvas);
            var expectedSelectedIndexes = [0, 2];
            for (var i = 0; i < imagesCanvases.length; ++i) {
                var imageCanvas = imagesCanvases[i];
                if (expectedSelectedIndexes.indexOf(i) === -1) {
                    ok(imageCanvas.classList.contains(constants_1.Constants.Classes.unselected), "Images in the range should be grayed out");
                }
                else {
                    ok(!imageCanvas.classList.contains(constants_1.Constants.Classes.unselected), "Images not in the range should not be grayed out");
                }
            }
        });
        test("When 'Page Range' is checked in the page control, but there is a negative page range specified, we should treat this as an invalid selection and unselect all the pages", function () {
            var clipperState = _this.getMockPdfModeState();
            clipperState.pdfPreviewInfo = {
                allPages: false,
                selectedPageRange: "-1,2",
                shouldAttachPdf: false
            };
            var pdfPreview = mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPreview_1.PdfPreview clipperState={clipperState}/>);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                pdfPreview.state.renderedPageIndexes = mockPdfDocument_1.MockPdfValues.pageDataUrlsMap;
            });
            var selectionQueryForNotGrayedOutImages = "." + constants_1.Constants.Classes.pdfPreviewImageCanvas + ":not(." + constants_1.Constants.Classes.unselected + ")";
            var selectedImageCanvases = document.querySelectorAll(selectionQueryForNotGrayedOutImages);
            strictEqual(selectedImageCanvases.length, 0, "All pages should be grayed out");
        });
        test("When the attachment checkbox is checked, the preview body should show an attachment", function () {
            var clipperState = _this.getMockPdfModeState();
            clipperState.pdfPreviewInfo = {
                allPages: true,
                selectedPageRange: "",
                shouldAttachPdf: true
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPreview_1.PdfPreview clipperState={clipperState}/>);
            strictEqual(document.getElementsByClassName(constants_1.Constants.Classes.attachmentOverlay).length, 1, "The attachment overlay should be present in the content body");
        });
        test("When the attachment checkbox isn't checked, there should be no attachment", function () {
            var clipperState = _this.getMockPdfModeState();
            clipperState.pdfPreviewInfo = {
                allPages: true,
                selectedPageRange: "",
                shouldAttachPdf: false
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPreview_1.PdfPreview clipperState={clipperState}/>);
            strictEqual(document.getElementsByClassName(constants_1.Constants.Classes.attachmentOverlay).length, 0, "The attachment overlay should NOT be present in the content body");
        });
        test("When the pdf screenshot response is a failure, the preview should display an error message in PDF mode", function () {
            var expectedMessage = "An error message.";
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.pdfResult = {
                data: new smartValue_1.SmartValue({ failureMessage: expectedMessage }),
                status: status_1.Status.Failed
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPreview_1.PdfPreview clipperState={clipperState}/>);
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            strictEqual(previewHeaderInput.value, expectedMessage, "The title of the page should be displayed in the preview title");
            ok(previewHeaderInput.readOnly);
        });
        test("When the showPageNumbers state is true, the number overlays should be visible on all canvases when all images are loaded", function () {
            var clipperState = _this.getMockPdfModeState();
            var pdfPreview = mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPreview_1.PdfPreview clipperState={clipperState}/>);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                pdfPreview.state.showPageNumbers = true;
                pdfPreview.state.renderedPageIndexes = mockPdfDocument_1.MockPdfValues.pageDataUrlsMap;
            });
            var overlays = document.getElementsByClassName(constants_1.Constants.Classes.overlay);
            strictEqual(overlays.length, mockPdfDocument_1.MockPdfValues.pageDataUrls.length, "There should be an overlay for every canvas");
            var overlayHiddens = document.getElementsByClassName(constants_1.Constants.Classes.overlayHidden);
            strictEqual(overlayHiddens.length, 0, "There should be no hidden overlays");
            for (var i = 0; i < overlays.length; i++) {
                var overlay = overlays[i];
                strictEqual(overlay.innerText, i + 1 + "", "The overlay number should be the index + 1");
            }
        });
        test("When the showPageNumbers state is true, the number overlays should be visible on all canvases when no images are loaded", function () {
            var clipperState = _this.getMockPdfModeState();
            var pdfPreview = mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPreview_1.PdfPreview clipperState={clipperState}/>);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                pdfPreview.state.showPageNumbers = true;
                pdfPreview.state.renderedPageIndexes = {};
            });
            var overlays = document.getElementsByClassName(constants_1.Constants.Classes.overlay);
            strictEqual(overlays.length, mockPdfDocument_1.MockPdfValues.pageDataUrls.length, "There should be an overlay for every canvas");
            var overlayHiddens = document.getElementsByClassName(constants_1.Constants.Classes.overlayHidden);
            strictEqual(overlayHiddens.length, 0, "There should be no hidden overlays");
            for (var i = 0; i < overlays.length; i++) {
                var overlay = overlays[i];
                strictEqual(overlay.innerText, i + 1 + "", "The overlay number should be the index + 1");
            }
        });
        test("When the showPageNumbers state is false, the number overlays should not be visible on all canvases when all images are loaded", function () {
            var clipperState = _this.getMockPdfModeState();
            var pdfPreview = mithrilUtils_1.MithrilUtils.mountToFixture(<pdfPreview_1.PdfPreview clipperState={clipperState}/>);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                pdfPreview.state.showPageNumbers = false;
                pdfPreview.state.renderedPageIndexes = mockPdfDocument_1.MockPdfValues.pageDataUrlsMap;
            });
            var overlayHiddens = document.getElementsByClassName(constants_1.Constants.Classes.overlayHidden);
            strictEqual(overlayHiddens.length, mockPdfDocument_1.MockPdfValues.pageDataUrls.length, "There should be a hidden overlay for every canvas");
        });
    };
    PdfPreviewTests.prototype.getMockPdfModeState = function () {
        var state = mockProps_1.MockProps.getMockClipperState();
        state.pageInfo.contentType = OneNoteApi.ContentType.EnhancedUrl;
        state.currentMode.set(clipMode_1.ClipMode.Pdf);
        state.pdfResult.data.set({
            pdf: new mockPdfDocument_1.MockPdfDocument(),
            viewportDimensions: mockPdfDocument_1.MockPdfValues.dimensions,
            byteLength: mockPdfDocument_1.MockPdfValues.byteLength
        });
        state.pdfResult.status = status_1.Status.Succeeded;
        state.pdfPreviewInfo = {
            allPages: true,
            selectedPageRange: "",
            shouldAttachPdf: false
        };
        return state;
    };
    return PdfPreviewTests;
}(testModule_1.TestModule));
exports.PdfPreviewTests = PdfPreviewTests;
(new PdfPreviewTests()).runTests();
