"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../../scripts/constants");
var pdfPreviewPage_1 = require("../../../../scripts/clipperUI/components/previewViewer/pdfPreviewPage");
var mithrilUtils_1 = require("../../../mithrilUtils");
var testModule_1 = require("../../../testModule");
var pdfDataUrls_1 = require("./pdfDataUrls");
var PdfPreviewPageTests = (function (_super) {
    __extends(PdfPreviewPageTests, _super);
    function PdfPreviewPageTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PdfPreviewPageTests.prototype.module = function () {
        return "pdfPreviewPage";
    };
    PdfPreviewPageTests.prototype.tests = function () {
        test("Given that the page is selected, the image container's opacity should be set to 1", function () {
            var pdfPreviewPage = mithrilUtils_1.MithrilUtils.mountToFixture(m.component(pdfPreviewPage_1.PdfPreviewPage, {viewportDimensions:{ width: 50, height: 50}, imgUrl:pdfDataUrls_1.pdfDataUrls[0], index:0, showPageNumber:false, isSelected:true}));
            var container = mithrilUtils_1.MithrilUtils.getFixture().getElementsByClassName(constants_1.Constants.Classes.pdfPreviewImageCanvas)[0];
            ok(!container.classList.contains(constants_1.Constants.Classes.unselected), "The unselected class should not be applied to the container");
            strictEqual(mithrilUtils_1.MithrilUtils.getFixture().getElementsByClassName(constants_1.Constants.Classes.unselected).length, 0, "The unselected class should not be applied anywhere");
        });
        test("Given that the page is not selected, the image container's opacity should be set to 0.3", function () {
            var pdfPreviewPage = mithrilUtils_1.MithrilUtils.mountToFixture(m.component(pdfPreviewPage_1.PdfPreviewPage, {viewportDimensions:{ width: 50, height: 50}, imgUrl:pdfDataUrls_1.pdfDataUrls[0], index:0, showPageNumber:false, isSelected:false}));
            var container = mithrilUtils_1.MithrilUtils.getFixture().getElementsByClassName(constants_1.Constants.Classes.pdfPreviewImageCanvas)[0];
            ok(container.classList.contains(constants_1.Constants.Classes.unselected), "The unselected class should be applied to the container");
            strictEqual(mithrilUtils_1.MithrilUtils.getFixture().getElementsByClassName(constants_1.Constants.Classes.unselected).length, 1, "The unselected class should only be applied once in the component");
        });
        // Note: Because we fade out the page numbers using a css class, we check for opacity to determine if the number is in view
        test("Given that showPageNumber is true, the page number should be shown", function () {
            var index = 19;
            var expectedPageNumber = "" + (index + 1);
            var pdfPreviewPage = mithrilUtils_1.MithrilUtils.mountToFixture(m.component(pdfPreviewPage_1.PdfPreviewPage, {viewportDimensions:{ width: 50, height: 50}, imgUrl:pdfDataUrls_1.pdfDataUrls[0], index:index, showPageNumber:true, isSelected:true}));
            var overlay = mithrilUtils_1.MithrilUtils.getFixture().getElementsByClassName(constants_1.Constants.Classes.overlay)[0];
            strictEqual(overlay.innerText, expectedPageNumber, "The page number shown should be the index + 1");
            ok(!overlay.classList.contains(constants_1.Constants.Classes.overlayHidden), "The overlay should not be hidden");
        });
        test("Given that showPageNumber is true, but the page is not selected, the page number should still be shown", function () {
            var index = 19;
            var expectedPageNumber = "" + (index + 1);
            var pdfPreviewPage = mithrilUtils_1.MithrilUtils.mountToFixture(m.component(pdfPreviewPage_1.PdfPreviewPage, {viewportDimensions:{ width: 50, height: 50}, imgUrl:pdfDataUrls_1.pdfDataUrls[0], index:index, showPageNumber:true, isSelected:false}));
            var overlay = mithrilUtils_1.MithrilUtils.getFixture().getElementsByClassName(constants_1.Constants.Classes.overlay)[0];
            strictEqual(overlay.innerText, expectedPageNumber, "The page number shown should be the index + 1");
            ok(!overlay.classList.contains(constants_1.Constants.Classes.overlayHidden), "The overlay should not be hidden");
        });
        test("Given that showPageNumber is true, but the imgUrl is undefined, the page number should still be shown", function () {
            var index = 19;
            var expectedPageNumber = "" + (index + 1);
            var pdfPreviewPage = mithrilUtils_1.MithrilUtils.mountToFixture(m.component(pdfPreviewPage_1.PdfPreviewPage, {viewportDimensions:{ width: 50, height: 50}, index:index, showPageNumber:true, isSelected:true}));
            var overlay = mithrilUtils_1.MithrilUtils.getFixture().getElementsByClassName(constants_1.Constants.Classes.overlay)[0];
            strictEqual(overlay.innerText, expectedPageNumber, "The page number shown should be the index + 1");
            ok(!overlay.classList.contains(constants_1.Constants.Classes.overlayHidden), "The overlay should not be hidden");
        });
        test("Given that showPageNumber is true and the index is 0, the page number should be shown", function () {
            var index = 0;
            var expectedPageNumber = "" + (index + 1);
            var pdfPreviewPage = mithrilUtils_1.MithrilUtils.mountToFixture(m.component(pdfPreviewPage_1.PdfPreviewPage, {viewportDimensions:{ width: 50, height: 50}, imgUrl:pdfDataUrls_1.pdfDataUrls[0], index:index, showPageNumber:true, isSelected:true}));
            var overlay = mithrilUtils_1.MithrilUtils.getFixture().getElementsByClassName(constants_1.Constants.Classes.overlay)[0];
            strictEqual(overlay.innerText, expectedPageNumber, "The page number shown should be the index + 1");
            ok(!overlay.classList.contains(constants_1.Constants.Classes.overlayHidden), "The overlay should not be hidden");
        });
        test("Given that showPageNumber is false, the page number should not be shown", function () {
            var pdfPreviewPage = mithrilUtils_1.MithrilUtils.mountToFixture(m.component(pdfPreviewPage_1.PdfPreviewPage, {viewportDimensions:{ width: 50, height: 50}, imgUrl:pdfDataUrls_1.pdfDataUrls[0], index:10, showPageNumber:false, isSelected:true}));
            var overlay = mithrilUtils_1.MithrilUtils.getFixture().getElementsByClassName(constants_1.Constants.Classes.overlay)[0];
            ok(overlay.classList.contains(constants_1.Constants.Classes.overlayHidden), "The overlay should be hidden");
        });
    };
    return PdfPreviewPageTests;
}(testModule_1.TestModule));
exports.PdfPreviewPageTests = PdfPreviewPageTests;
(new PdfPreviewPageTests()).runTests();
