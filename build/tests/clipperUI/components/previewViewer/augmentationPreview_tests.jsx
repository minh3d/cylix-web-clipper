"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../../scripts/constants");
var clipMode_1 = require("../../../../scripts/clipperUI/clipMode");
var status_1 = require("../../../../scripts/clipperUI/status");
var augmentationHelper_1 = require("../../../../scripts/contentCapture/augmentationHelper");
var augmentationPreview_1 = require("../../../../scripts/clipperUI/components/previewViewer/augmentationPreview");
var assert_1 = require("../../../assert");
var mithrilUtils_1 = require("../../../mithrilUtils");
var mockProps_1 = require("../../../mockProps");
var testModule_1 = require("../../../testModule");
var AugmentationPreviewTests = (function (_super) {
    __extends(AugmentationPreviewTests, _super);
    function AugmentationPreviewTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.stringsJson = require("../../../../strings.json");
        _this.sansSerifFontFamily = _this.stringsJson["WebClipper.FontFamily.Preview.SansSerifDefault"];
        _this.sansSerifDefaultFontSize = _this.stringsJson["WebClipper.FontSize.Preview.SansSerifDefault"];
        _this.serifFontFamily = _this.stringsJson["WebClipper.FontFamily.Preview.SerifDefault"];
        _this.fontSizeStep = constants_1.Constants.Settings.fontSizeStep;
        return _this;
    }
    AugmentationPreviewTests.prototype.module = function () {
        return "augmentationPreview";
    };
    AugmentationPreviewTests.prototype.tests = function () {
        var _this = this;
        test("The tab order flow from the header to the preview title is correct in Augmentation mode", function () {
            var mockClipperState = _this.getMockAugmentationModeState();
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.serif).click();
            });
            assert_1.Assert.tabOrderIsIncremental([constants_1.Constants.Ids.highlightButton, constants_1.Constants.Ids.serif, constants_1.Constants.Ids.decrementFontSize,
                constants_1.Constants.Ids.incrementFontSize, constants_1.Constants.Ids.previewHeaderInput]);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.sansSerif).click();
            });
            assert_1.Assert.tabOrderIsIncremental([constants_1.Constants.Ids.highlightButton, constants_1.Constants.Ids.sansSerif, constants_1.Constants.Ids.decrementFontSize,
                constants_1.Constants.Ids.incrementFontSize, constants_1.Constants.Ids.previewHeaderInput]);
        });
        test("When a pair of codependent buttons have conditional attributes, they both respond with the same behavior when clicked and have different values from each other", function () {
            var mockClipperState = _this.getMockAugmentationModeState();
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.serif).click();
            });
            var selectedTabIndices = [document.getElementById(constants_1.Constants.Ids.serif).tabIndex];
            var unselectedTabIndices = [document.getElementById(constants_1.Constants.Ids.sansSerif).tabIndex];
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.sansSerif).click();
            });
            selectedTabIndices.push(document.getElementById(constants_1.Constants.Ids.sansSerif).tabIndex);
            unselectedTabIndices.push(document.getElementById(constants_1.Constants.Ids.serif).tabIndex);
            strictEqual(selectedTabIndices[0], selectedTabIndices[1], "When the serif button is clicked the tabIndex should be the same as the tabIndex of the sansSerif button when clicked");
            ok(selectedTabIndices[0] > 0, "Selected tabIndex should be greater than 0");
            strictEqual(unselectedTabIndices[0], unselectedTabIndices[1], "When the serif button is not clicked the tabIndex should be the same as the tabIndex of the sansSerif button when not clicked");
            strictEqual(unselectedTabIndices[0], -1, "The unselected tabsIndices should be -1");
        });
        test("The aria-posinset attribute should flow in order, assuming they are all available", function () {
            var mockClipperState = _this.getMockAugmentationModeState();
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            assert_1.Assert.checkAriaSetAttributes([constants_1.Constants.Ids.sansSerif, constants_1.Constants.Ids.serif]);
        });
        test("The augmentation header and all related controls should be displayed in Augmentation mode", function () {
            var mockClipperState = _this.getMockAugmentationModeState();
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            ok(!document.getElementById(constants_1.Constants.Ids.addRegionControl), "The region control should not exist");
            ok(document.getElementById(constants_1.Constants.Ids.highlightControl), "The highlight control should exist");
            ok(document.getElementById(constants_1.Constants.Ids.serifControl), "The font family control should exist");
            ok(document.getElementById(constants_1.Constants.Ids.decrementFontSize), "The decrement font size control should exist");
            ok(document.getElementById(constants_1.Constants.Ids.incrementFontSize), "The increment font size control should exist");
        });
        test("The editable title of the page should be displayed in the preview title in Augmentation mode", function () {
            var mockClipperState = _this.getMockAugmentationModeState();
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            strictEqual(previewHeaderInput.value, mockClipperState.previewGlobalInfo.previewTitleText, "The title of the page should be displayed in the preview title");
            ok(!previewHeaderInput.readOnly);
        });
        test("The augmented content of the page should be displayed in the highlightable preview body in Augmentation Mode", function () {
            var mockClipperState = _this.getMockAugmentationModeState();
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            strictEqual(document.getElementById(constants_1.Constants.Ids.highlightablePreviewBody).innerHTML, mockClipperState.augmentationPreviewInfo.previewBodyHtml, "The editable augmentation result content is displayed in the preview body's highlightable portion");
        });
        test("When the augmentation successfully completes, but no data is returned, the preview should indicate no content was found in Augmentation mode", function () {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.currentMode.set(clipMode_1.ClipMode.Augmentation);
            clipperState.augmentationResult = {
                data: undefined,
                status: status_1.Status.Succeeded
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(<augmentationPreview_1.AugmentationPreview clipperState={clipperState}/>);
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            strictEqual(previewHeaderInput.value, _this.stringsJson["WebClipper.Preview.NoContentFound"], "The preview title should display a message indicating no content was found");
            ok(previewHeaderInput.readOnly);
            strictEqual(document.getElementById(constants_1.Constants.Ids.previewBody).innerText, "", "The preview body should be empty");
        });
        test("When the call to augmentation has not started, the preview should indicate that it is loading in Augmentation mode", function () {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.currentMode.set(clipMode_1.ClipMode.Augmentation);
            clipperState.augmentationResult = {
                data: undefined,
                status: status_1.Status.NotStarted
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(<augmentationPreview_1.AugmentationPreview clipperState={clipperState}/>);
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            strictEqual(previewHeaderInput.value, _this.stringsJson["WebClipper.Preview.LoadingMessage"], "The preview title should display a loading message");
            ok(previewHeaderInput.readOnly);
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            strictEqual(previewBody.getElementsByClassName(constants_1.Constants.Classes.spinner).length, 1, "The spinner should be present in the preview body");
        });
        test("When augmentation is in progress, the preview should indicate that it is loading in Augmentation mode", function () {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.currentMode.set(clipMode_1.ClipMode.Augmentation);
            clipperState.augmentationResult = {
                data: undefined,
                status: status_1.Status.InProgress
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(<augmentationPreview_1.AugmentationPreview clipperState={clipperState}/>);
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            strictEqual(previewHeaderInput.value, _this.stringsJson["WebClipper.Preview.LoadingMessage"], "The preview title should display a loading message");
            ok(previewHeaderInput.readOnly);
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            strictEqual(previewBody.getElementsByClassName(constants_1.Constants.Classes.spinner).length, 1, "The spinner should be present in the preview body");
        });
        test("When the call to augmentation has not started, the preview should indicate that it is loading, even when data is defined in Augmentation mode", function () {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.currentMode.set(clipMode_1.ClipMode.Augmentation);
            clipperState.augmentationResult = {
                data: undefined,
                status: status_1.Status.NotStarted
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(<augmentationPreview_1.AugmentationPreview clipperState={clipperState}/>);
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            strictEqual(previewHeaderInput.value, _this.stringsJson["WebClipper.Preview.LoadingMessage"], "The preview title should display a loading message");
            ok(previewHeaderInput.readOnly);
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            strictEqual(previewBody.getElementsByClassName(constants_1.Constants.Classes.spinner).length, 1, "The spinner should be present in the preview body");
        });
        test("When augmentation is in progress, the preview should indicate that it is loading, even when data is defined in Augmentation mode", function () {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.currentMode.set(clipMode_1.ClipMode.Augmentation);
            clipperState.augmentationResult = {
                data: undefined,
                status: status_1.Status.InProgress
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(<augmentationPreview_1.AugmentationPreview clipperState={clipperState}/>);
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            strictEqual(previewHeaderInput.value, _this.stringsJson["WebClipper.Preview.LoadingMessage"], "The preview title should display a loading message");
            ok(previewHeaderInput.readOnly);
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            strictEqual(previewBody.getElementsByClassName(constants_1.Constants.Classes.spinner).length, 1, "The spinner should be present in the preview body");
        });
        test("When the augmentation response is a failure, the preview should display an error message in Augmentation mode", function () {
            var expectedMessage = "An error message.";
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            clipperState.currentMode.set(clipMode_1.ClipMode.Augmentation);
            clipperState.augmentationResult = {
                data: {
                    failureMessage: expectedMessage
                },
                status: status_1.Status.Failed
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(<augmentationPreview_1.AugmentationPreview clipperState={clipperState}/>);
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            strictEqual(previewHeaderInput.value, expectedMessage, "The title of the page should be displayed in the preview title");
            ok(previewHeaderInput.readOnly);
        });
        test("The default font-size and font-family should be the same as those specified in strings.json in Augmentation mode", function () {
            var mockClipperState = _this.getMockAugmentationModeState();
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            strictEqual(previewBody.style.fontSize, _this.sansSerifDefaultFontSize);
            strictEqual(previewBody.style.fontFamily, _this.sansSerifFontFamily);
        });
        test("On clicking Serif, the fontFamily should be changed to the fontFamily specified in strings.json, and then back upon clicking Sans-Serif in Augmentation mode", function () {
            var mockClipperState = _this.getMockAugmentationModeState();
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var serifButton = document.getElementById(constants_1.Constants.Ids.serif);
            var sansSerifButton = document.getElementById(constants_1.Constants.Ids.sansSerif);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                serifButton.click();
            });
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            strictEqual(_this.serifFontFamily, previewBody.style.fontFamily);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                sansSerifButton.click();
            });
            strictEqual(_this.sansSerifFontFamily, previewBody.style.fontFamily);
        });
        test("Clicking on the same fontFamily button multiple times should only set that fontFamily once in Augmentation mode", function () {
            var mockClipperState = _this.getMockAugmentationModeState();
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var serifButton = document.getElementById(constants_1.Constants.Ids.serif);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                serifButton.click();
            });
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            strictEqual(_this.serifFontFamily, previewBody.style.fontFamily);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                serifButton.click();
            });
            strictEqual(_this.serifFontFamily, previewBody.style.fontFamily);
        });
        test("The default fontSize should be the sansSerif default fontSize, and should increment by two and decrement by two for each click on up and down respectively in Augmentation mode", function () {
            var mockClipperState = _this.getMockAugmentationModeState();
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var increaseFontSizeButton = document.getElementById(constants_1.Constants.Ids.incrementFontSize);
            var decreaseFontSizeButton = document.getElementById(constants_1.Constants.Ids.decrementFontSize);
            var defaultFontSize = parseInt(_this.sansSerifDefaultFontSize, 10);
            var largerFontSize = defaultFontSize + 2 * _this.fontSizeStep;
            var smallerFontSize = defaultFontSize - _this.fontSizeStep;
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            strictEqual(parseInt(previewBody.style.fontSize, 10), parseInt(_this.sansSerifDefaultFontSize, 10));
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                decreaseFontSizeButton.click();
            });
            strictEqual(parseInt(previewBody.style.fontSize, 10), smallerFontSize);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                increaseFontSizeButton.click();
                increaseFontSizeButton.click();
                increaseFontSizeButton.click();
            });
            strictEqual(parseInt(previewBody.style.fontSize, 10), largerFontSize);
        });
        test("If the user tries to increase beyond the maximum fontSize for either Serif or SansSerif, it should cap at the max font size defined in strings.json in Augmentation mode", function () {
            var maximumFontSize = constants_1.Constants.Settings.maximumFontSize;
            var numClicks = (maximumFontSize - parseInt(_this.sansSerifDefaultFontSize, 10)) / _this.fontSizeStep;
            var mockClipperState = _this.getMockAugmentationModeState();
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var increaseFontSizeButton = document.getElementById(constants_1.Constants.Ids.incrementFontSize);
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                for (var i = 0; i < (numClicks + 5); i++) {
                    increaseFontSizeButton.click();
                }
            });
            strictEqual(parseInt(previewBody.style.fontSize, 10), maximumFontSize);
        });
        test("If the user tries to decrease beyond the minimum fontSize for either Serif or SansSerif, it should reset at the minimum font size in Augmentation mode", function () {
            var minimumFontSize = constants_1.Constants.Settings.minimumFontSize;
            var numClicks = (parseInt(_this.sansSerifDefaultFontSize, 10) - minimumFontSize) / _this.fontSizeStep;
            var mockClipperState = _this.getMockAugmentationModeState();
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var decreaseFontSizeButton = document.getElementById(constants_1.Constants.Ids.decrementFontSize);
            var previewBody = document.getElementById(constants_1.Constants.Ids.previewBody);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                for (var i = 0; i < (numClicks + 5); i++) {
                    decreaseFontSizeButton.click();
                }
            });
            strictEqual(parseInt(previewBody.style.fontSize, 10), minimumFontSize);
        });
        test("The default internal state should show that 'Highlighting' is disabled in Augmentation mode", function () {
            var mockClipperState = _this.getMockAugmentationModeState();
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            var augmentationPreview = mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var highlightButton = document.getElementById(constants_1.Constants.Ids.highlightButton);
            ok(!augmentationPreview.props.clipperState.previewGlobalInfo.highlighterEnabled);
            strictEqual(highlightButton.className.indexOf("active"), -1, "The active class should not be applied to the highlight button");
        });
        test("If the user clicks the highlight button, the internal state should show that 'Highlighting' is enabled in Augmentation mode", function () {
            var mockClipperState = _this.getMockAugmentationModeState();
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            var augmentationPreview = mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var highlightButton = document.getElementById(constants_1.Constants.Ids.highlightButton);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                highlightButton.click();
            });
            ok(augmentationPreview.props.clipperState.previewGlobalInfo.highlighterEnabled);
            notStrictEqual(highlightButton.className.indexOf("active"), -1, "The active class should be applied to the highlight button");
        });
        test("If the user selects something in the highlightable preview body, then clicks the highlight button, the internal state should show that 'Highlighting' is disabled in Augmentation mode, and the item should be highlighted", function () {
            var mockClipperState = _this.getMockAugmentationModeState();
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            var augmentationPreview = mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            // This id is necessary for the highlighting to be enabled on this element's children
            var paragraph = document.createElement("p");
            var paragraphId = "para";
            paragraph.id = paragraphId;
            var innerText = "Test";
            paragraph.innerHTML = innerText;
            document.getElementById(constants_1.Constants.Ids.highlightablePreviewBody).appendChild(paragraph);
            var highlightButton = document.getElementById(constants_1.Constants.Ids.highlightButton);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                // Simulate a selection (http://stackoverflow.com/questions/6139107/programatically-select-text-in-a-contenteditable-html-element)
                var range = document.createRange();
                range.selectNodeContents(paragraph);
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
                highlightButton.click();
            });
            ok(!augmentationPreview.props.clipperState.previewGlobalInfo.highlighterEnabled);
            strictEqual(highlightButton.className.indexOf("active"), -1, "The active class should not be applied to the highlight button");
            var childSpans = paragraph.getElementsByClassName(constants_1.Constants.Classes.highlighted);
            strictEqual(childSpans.length, 1, "There should only be one highlighted element");
            strictEqual(childSpans[0].innerText, innerText, "The highlighted element should be the inner text of the paragraph");
        });
        test("If the user selects something outside the highlightable preview body, then clicks the highlight button, the internal state should show that 'Highlighting' is enabled in Augmentation mode, and nothing should be highlighted", function () {
            var mockClipperState = _this.getMockAugmentationModeState();
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            var augmentationPreview = mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            // This id is necessary for the highlighting to be enabled on this element's children
            var paragraph = document.createElement("p");
            var paragraphId = "para";
            paragraph.id = paragraphId;
            var innerText = "Test";
            paragraph.innerHTML = innerText;
            // Note this is outside the preview body
            mithrilUtils_1.MithrilUtils.getFixture().appendChild(paragraph);
            var highlightButton = document.getElementById(constants_1.Constants.Ids.highlightButton);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                var range = document.createRange();
                range.selectNodeContents(paragraph);
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
                highlightButton.click();
            });
            ok(augmentationPreview.props.clipperState.previewGlobalInfo.highlighterEnabled);
            notStrictEqual(highlightButton.className.indexOf("active"), -1, "The active class should be applied to the highlight button");
            var childSpans = paragraph.getElementsByClassName(constants_1.Constants.Classes.highlighted);
            strictEqual(childSpans.length, 0, "There should only be one highlighted element");
        });
        test("If the user is highlighting (i.e., highlighting mode is active), then the 'active' class should be applied to the highlightButton in Augmentation mode", function () {
            var mockClipperState = _this.getMockAugmentationModeState();
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var highlightButton = document.getElementById(constants_1.Constants.Ids.highlightButton);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                highlightButton.click();
            });
            // We do this to manually trigger a config call
            mithrilUtils_1.MithrilUtils.simulateAction(function () { });
            notStrictEqual(highlightButton.className.indexOf("active"), -1, "The active class should be applied to the highlight button");
        });
        test("If the user is not highlighting (i.e., highlighting mode is active), then the 'active' class should not be applied to the highlightButton in Augmentation mode", function () {
            var mockClipperState = _this.getMockAugmentationModeState();
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var highlightButton = document.getElementById(constants_1.Constants.Ids.highlightButton);
            strictEqual(highlightButton.className.indexOf("active"), -1, "The active class should not be applied to the highlight button");
        });
        test("If the editable title feature is disabled, it should be readonly", function () {
            var mockClipperState = _this.getMockAugmentationModeState();
            mockClipperState.injectOptions.enableEditableTitle = false;
            var defaultComponent = <augmentationPreview_1.AugmentationPreview clipperState={mockClipperState}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(defaultComponent);
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            strictEqual(previewHeaderInput.value, mockClipperState.previewGlobalInfo.previewTitleText, "The title of the page should be displayed in the preview title");
            ok(previewHeaderInput.readOnly);
        });
    };
    AugmentationPreviewTests.prototype.getMockAugmentationModeState = function () {
        var state = mockProps_1.MockProps.getMockClipperState();
        state.currentMode.set(clipMode_1.ClipMode.Augmentation);
        state.augmentationResult = {
            data: {
                ContentInHtml: "Jello World content in all its jelly goodness.",
                ContentModel: augmentationHelper_1.AugmentationModel.Product,
                ContentObjects: [],
                PageMetadata: {
                    AutoPageTags: "Product",
                    AutoPageTagsCodes: "Product"
                }
            },
            status: status_1.Status.Succeeded
        };
        return state;
    };
    return AugmentationPreviewTests;
}(testModule_1.TestModule));
exports.AugmentationPreviewTests = AugmentationPreviewTests;
(new AugmentationPreviewTests()).runTests();
