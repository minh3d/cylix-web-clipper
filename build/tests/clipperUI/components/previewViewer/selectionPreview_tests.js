"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../../scripts/constants");
var clipMode_1 = require("../../../../scripts/clipperUI/clipMode");
var selectionPreview_1 = require("../../../../scripts/clipperUI/components/previewViewer/selectionPreview");
var mithrilUtils_1 = require("../../../mithrilUtils");
var mockProps_1 = require("../../../mockProps");
var testModule_1 = require("../../../testModule");
var SelectionPreviewTests = (function (_super) {
    __extends(SelectionPreviewTests, _super);
    function SelectionPreviewTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SelectionPreviewTests.prototype.module = function () {
        return "selectionPreview";
    };
    SelectionPreviewTests.prototype.beforeEach = function () {
        this.mockClipperState = this.getMockSelectionModeState();
        this.defaultComponent = m.component(selectionPreview_1.SelectionPreview, {clipperState:this.mockClipperState});
    };
    SelectionPreviewTests.prototype.tests = function () {
        var _this = this;
        test("The selection's highlightable preview body should render the content", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var highlightablePreviewBody = document.getElementById(constants_1.Constants.Ids.highlightablePreviewBody);
            strictEqual(highlightablePreviewBody.innerText, _this.mockClipperState.selectionPreviewInfo.previewBodyHtml, "The editable selection result content is displayed in the preview body");
            strictEqual(highlightablePreviewBody.innerHTML, _this.mockClipperState.selectionPreviewInfo.previewBodyHtml, "Only the editable selection result content is displayed in the preview body");
        });
        test("The selection preview's highlightable preview body should render the content as HTML, not purely text", function () {
            _this.mockClipperState.selectionPreviewInfo.previewBodyHtml = "<div>The selection</div>";
            _this.defaultComponent = m.component(selectionPreview_1.SelectionPreview, {clipperState:_this.mockClipperState});
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var highlightablePreviewBody = document.getElementById(constants_1.Constants.Ids.highlightablePreviewBody);
            strictEqual(highlightablePreviewBody.innerHTML, _this.mockClipperState.selectionPreviewInfo.previewBodyHtml, "Only the editable selection result content is displayed in the preview body");
            strictEqual(highlightablePreviewBody.childElementCount, 1, "There should be one child");
            var child = highlightablePreviewBody.firstElementChild;
            strictEqual(child.outerHTML, _this.mockClipperState.selectionPreviewInfo.previewBodyHtml, "The child's outer HTML should be the preview body html");
            strictEqual(child.innerHTML, "The selection", "The child's outer HTML should be the selection text");
        });
    };
    SelectionPreviewTests.prototype.getMockSelectionModeState = function () {
        var state = mockProps_1.MockProps.getMockClipperState();
        state.currentMode.set(clipMode_1.ClipMode.Selection);
        state.selectionPreviewInfo = {
            previewBodyHtml: "The selection"
        };
        return state;
    };
    return SelectionPreviewTests;
}(testModule_1.TestModule));
exports.SelectionPreviewTests = SelectionPreviewTests;
(new SelectionPreviewTests()).runTests();
