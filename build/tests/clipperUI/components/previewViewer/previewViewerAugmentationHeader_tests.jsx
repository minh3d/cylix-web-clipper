"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var sinon = require("sinon");
var frontEndGlobals_1 = require("../../../../scripts/clipperUI/frontEndGlobals");
var previewViewerAugmentationHeader_1 = require("../../../../scripts/clipperUI/components/previewViewer/previewViewerAugmentationHeader");
var constants_1 = require("../../../../scripts/constants");
var assert_1 = require("../../../assert");
var mithrilUtils_1 = require("../../../mithrilUtils");
var testModule_1 = require("../../../testModule");
var stubSessionLogger_1 = require("../../../../scripts/logging/stubSessionLogger");
var PreviewViewerAugmentationHeaderTests = (function (_super) {
    __extends(PreviewViewerAugmentationHeaderTests, _super);
    function PreviewViewerAugmentationHeaderTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PreviewViewerAugmentationHeaderTests.prototype.module = function () {
        return "previewViewerAugmentationHeader";
    };
    PreviewViewerAugmentationHeaderTests.prototype.beforeEach = function () {
        this.mockProp = {
            toggleHighlight: sinon.spy(function () { }),
            changeFontFamily: sinon.spy(function (serif) { }),
            changeFontSize: sinon.spy(function (increase) { }),
            serif: false,
            textHighlighterEnabled: false
        };
        this.defaultComponent =
            <previewViewerAugmentationHeader_1.PreviewViewerAugmentationHeader toggleHighlight={this.mockProp.toggleHighlight} changeFontFamily={this.mockProp.changeFontFamily} changeFontSize={this.mockProp.changeFontSize} serif={this.mockProp.serif} textHighlighterEnabled={this.mockProp.textHighlighterEnabled}/>;
        frontEndGlobals_1.Clipper.logger = new stubSessionLogger_1.StubSessionLogger();
    };
    PreviewViewerAugmentationHeaderTests.prototype.tests = function () {
        var _this = this;
        test("The highlightControl should be visible", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            ok(!!document.getElementById(constants_1.Constants.Ids.highlightControl));
        });
        test("The highlightControl's buttons should be visible", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            ok(!!document.getElementById(constants_1.Constants.Ids.highlightButton));
        });
        test("The serifControl should be visible", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            ok(!!document.getElementById(constants_1.Constants.Ids.serifControl));
        });
        test("The serifControl's buttons should be visible", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            ok(!!document.getElementById(constants_1.Constants.Ids.sansSerif));
            ok(!!document.getElementById(constants_1.Constants.Ids.serif));
        });
        test("The fontSizeControl's buttons should be visible", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            ok(!!document.getElementById(constants_1.Constants.Ids.decrementFontSize));
            ok(!!document.getElementById(constants_1.Constants.Ids.incrementFontSize));
        });
        test("The tabbing should flow from highlight to font family selectors to font size selectors", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            assert_1.Assert.tabOrderIsIncremental([constants_1.Constants.Ids.highlightButton, constants_1.Constants.Ids.sansSerif, constants_1.Constants.Ids.decrementFontSize, constants_1.Constants.Ids.incrementFontSize]);
        });
        test("The togglehighlight callback prop should be called exactly once whenever the highlight button is clicked", function () {
            var sectionPicker = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.highlightButton).click();
            });
            var spy = sectionPicker.props.toggleHighlight;
            ok(spy.calledOnce, "toggleHighlight should be called exactly once");
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.highlightButton).click();
            });
            ok(spy.calledTwice, "toggleHighlight should be called again");
        });
        test("The changeFontFamily callback prop should be called with true when the serif button is clicked", function () {
            var sectionPicker = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.serif).click();
            });
            var spy = sectionPicker.props.changeFontFamily;
            ok(spy.calledOnce, "changeFontFamily should be called exactly once");
            ok(spy.calledWith(true), "changeFontFamily should be called with true");
        });
        test("The changeFontFamily callback prop should be called with false when the sans-serif button is clicked", function () {
            var sectionPicker = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.sansSerif).click();
            });
            var spy = sectionPicker.props.changeFontFamily;
            ok(spy.calledOnce, "changeFontFamily should be called exactly once");
            ok(spy.calledWith(false), "changeFontFamily should be called with false");
        });
        test("The changeFontSize callback prop should be called with true when the increase button is clicked", function () {
            var sectionPicker = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.incrementFontSize).click();
            });
            var spy = sectionPicker.props.changeFontSize;
            ok(spy.calledOnce, "changeFontSize should be called exactly once");
            ok(spy.calledWith(true), "changeFontSize should be called with true");
        });
        test("The changeFontSize callback prop should be called with false when the decrease button is clicked", function () {
            var sectionPicker = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.decrementFontSize).click();
            });
            var spy = sectionPicker.props.changeFontSize;
            ok(spy.calledOnce, "changeFontSize should be called exactly once");
            ok(spy.calledWith(false), "changeFontSize should be called with false");
        });
    };
    return PreviewViewerAugmentationHeaderTests;
}(testModule_1.TestModule));
exports.PreviewViewerAugmentationHeaderTests = PreviewViewerAugmentationHeaderTests;
(new PreviewViewerAugmentationHeaderTests()).runTests();
