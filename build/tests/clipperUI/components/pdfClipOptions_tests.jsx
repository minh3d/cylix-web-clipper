"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../scripts/constants");
var status_1 = require("../../../scripts/clipperUI/status");
var pdfClipOptions_1 = require("../../../scripts/clipperUI/components/pdfClipOptions");
var assert_1 = require("../../assert");
var mithrilUtils_1 = require("../../mithrilUtils");
var mockProps_1 = require("../../mockProps");
var testModule_1 = require("../../testModule");
var MockPdfDocument_1 = require("../../contentCapture/MockPdfDocument");
var PdfClipOptionsTests = (function (_super) {
    __extends(PdfClipOptionsTests, _super);
    function PdfClipOptionsTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.stringsJson = require("../../../strings.json");
        return _this;
    }
    PdfClipOptionsTests.prototype.module = function () {
        return "pdfClipOptions";
    };
    PdfClipOptionsTests.prototype.beforeEach = function () {
        var defaultClipperState = mockProps_1.MockProps.getMockClipperState();
        defaultClipperState.pdfResult.status = status_1.Status.Succeeded;
        defaultClipperState.pdfResult.data.set({
            pdf: new MockPdfDocument_1.MockPdfDocument(),
            viewportDimensions: MockPdfDocument_1.MockPdfValues.dimensions,
            byteLength: MockPdfDocument_1.MockPdfValues.byteLength
        });
        this.defaultPdfClipOptionsProps = {
            clipperState: defaultClipperState
        };
        this.defaultComponent = <pdfClipOptions_1.PdfClipOptions {...this.defaultPdfClipOptionsProps}/>;
    };
    PdfClipOptionsTests.prototype.tests = function () {
        // TODO test rendering based on props, click tests, test that all elements are rendered, text label rendering
        var _this = this;
        test("All elements that should always be present in the non-expanded state should be rendered correctly assuming all the props are true", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            ok(document.getElementById(constants_1.Constants.Ids.radioAllPagesLabel), "radioAllPagesLabel should exist");
            ok(document.getElementById(constants_1.Constants.Ids.radioPageRangeLabel), "radioPageRangeLabel should exist");
            ok(!document.getElementById(constants_1.Constants.Ids.checkboxToDistributePages), "checkboxToDistributePages should not exist");
            ok(!document.getElementById(constants_1.Constants.Ids.checkboxToAttachPdf), "checkboxToDistributePages should not exist");
        });
        test("After clicking the 'More' button, all elements should be rendered correctly assuming all the props are true", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.moreClipOptions).click();
            });
            ok(document.getElementById(constants_1.Constants.Ids.radioAllPagesLabel), "radioAllPagesLabel should exist");
            ok(document.getElementById(constants_1.Constants.Ids.radioPageRangeLabel), "radioPageRangeLabel should exist");
            ok(document.getElementById(constants_1.Constants.Ids.checkboxToDistributePages), "checkboxToDistributePages should exist");
            ok(document.getElementById(constants_1.Constants.Ids.checkboxToAttachPdf), "checkboxToAttachPdf should exist");
        });
        test("All elements that should always be present should be rendered correctly assuming all the props are false", function () {
            _this.defaultPdfClipOptionsProps.clipperState.pdfPreviewInfo.allPages = false;
            _this.defaultPdfClipOptionsProps.clipperState.pdfPreviewInfo.shouldAttachPdf = false;
            _this.defaultPdfClipOptionsProps.clipperState.pdfPreviewInfo.shouldDistributePages = false;
            mithrilUtils_1.MithrilUtils.mountToFixture(<pdfClipOptions_1.PdfClipOptions {..._this.defaultPdfClipOptionsProps}/>);
            ok(document.getElementById(constants_1.Constants.Ids.radioAllPagesLabel), "radioAllPagesLabel should exist");
            ok(document.getElementById(constants_1.Constants.Ids.radioPageRangeLabel), "radioPageRangeLabel should exist");
        });
        test("All elements that should always be present should be rendered correctly assuming all the props are false", function () {
            _this.defaultPdfClipOptionsProps.clipperState.pdfPreviewInfo.allPages = false;
            _this.defaultPdfClipOptionsProps.clipperState.pdfPreviewInfo.shouldAttachPdf = false;
            _this.defaultPdfClipOptionsProps.clipperState.pdfPreviewInfo.shouldDistributePages = false;
            mithrilUtils_1.MithrilUtils.mountToFixture(<pdfClipOptions_1.PdfClipOptions {..._this.defaultPdfClipOptionsProps}/>);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.moreClipOptions).click();
            });
            ok(document.getElementById(constants_1.Constants.Ids.checkboxToDistributePages), "checkboxToDistributePages should exist");
            ok(document.getElementById(constants_1.Constants.Ids.checkboxToAttachPdf), "checkboxToAttachPdf should exist");
        });
        test("The range input box should be present if allPages is selected", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            ok(document.getElementById(constants_1.Constants.Ids.rangeInput), "The range input box should be present");
        });
        test("The range input box should be present if pageRange is selected", function () {
            _this.defaultPdfClipOptionsProps.clipperState.pdfPreviewInfo.allPages = false;
            mithrilUtils_1.MithrilUtils.mountToFixture(<pdfClipOptions_1.PdfClipOptions {..._this.defaultPdfClipOptionsProps}/>);
            ok(document.getElementById(constants_1.Constants.Ids.rangeInput), "The range input box should be present");
        });
        test("The tab order should flow linearly between pdf options with the page range radio button appearing only when selected", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.moreClipOptions).click();
                document.getElementById(constants_1.Constants.Ids.radioAllPagesLabel).click();
            });
            assert_1.Assert.tabOrderIsIncremental([
                constants_1.Constants.Ids.radioAllPagesLabel, constants_1.Constants.Ids.checkboxToDistributePages, constants_1.Constants.Ids.checkboxToAttachPdf
            ]);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.radioPageRangeLabel).click();
            });
            assert_1.Assert.tabOrderIsIncremental([
                constants_1.Constants.Ids.radioPageRangeLabel, constants_1.Constants.Ids.checkboxToDistributePages, constants_1.Constants.Ids.checkboxToAttachPdf
            ]);
        });
        test("Given that the user selected page ranges, the tab order should flow linearly between pdf options", function () {
            _this.defaultPdfClipOptionsProps.clipperState.pdfPreviewInfo.allPages = false;
            mithrilUtils_1.MithrilUtils.mountToFixture(<pdfClipOptions_1.PdfClipOptions {..._this.defaultPdfClipOptionsProps}/>);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.moreClipOptions).click();
            });
            assert_1.Assert.tabOrderIsIncremental([
                constants_1.Constants.Ids.radioPageRangeLabel, constants_1.Constants.Ids.checkboxToDistributePages, constants_1.Constants.Ids.checkboxToAttachPdf
            ]);
        });
        test("Given that the allPages prop is true, the allPages radio should be selected, and the pageRange radio should not", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var allPagesElem = document.getElementById(constants_1.Constants.Ids.radioAllPagesLabel);
            var allPagesRadioElems = allPagesElem.getElementsByClassName(constants_1.Constants.Classes.radioIndicatorFill);
            strictEqual(allPagesRadioElems.length, 1, "The all pages radio should be filled");
            var pageRangeElem = document.getElementById(constants_1.Constants.Ids.radioPageRangeLabel);
            var pageRangeRadioElems = pageRangeElem.getElementsByClassName(constants_1.Constants.Classes.radioIndicatorFill);
            strictEqual(pageRangeRadioElems.length, 0, "The page range radio should not be filled");
        });
        test("Given that the allPages prop is false, the pageRange radio should be selected, and the allPages radio should not", function () {
            _this.defaultPdfClipOptionsProps.clipperState.pdfPreviewInfo.allPages = false;
            mithrilUtils_1.MithrilUtils.mountToFixture(<pdfClipOptions_1.PdfClipOptions {..._this.defaultPdfClipOptionsProps}/>);
            var allPagesElem = document.getElementById(constants_1.Constants.Ids.radioAllPagesLabel);
            var allPagesRadioElems = allPagesElem.getElementsByClassName(constants_1.Constants.Classes.radioIndicatorFill);
            strictEqual(allPagesRadioElems.length, 0, "The all pages radio should not be filled");
            var pageRangeElem = document.getElementById(constants_1.Constants.Ids.radioPageRangeLabel);
            var pageRangeRadioElems = pageRangeElem.getElementsByClassName(constants_1.Constants.Classes.radioIndicatorFill);
            strictEqual(pageRangeRadioElems.length, 1, "The page range radio should be filled");
        });
        test("Given that the shouldDistributePages prop is true, the checkboxToDistributePages should be filled", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.moreClipOptions).click();
            });
            var shouldDistributePagesElemn = document.getElementById(constants_1.Constants.Ids.checkboxToDistributePages);
            var checkboxCheckElems = shouldDistributePagesElemn.getElementsByClassName(constants_1.Constants.Classes.checkboxCheck);
            strictEqual(checkboxCheckElems.length, 1, "The checkbox to distribute the pages should be filled");
        });
        test("Given that the shouldDistributePages prop is false, the checkboxToDistributePages should not be filled", function () {
            _this.defaultPdfClipOptionsProps.clipperState.pdfPreviewInfo.shouldDistributePages = false;
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.moreClipOptions).click();
            });
            var shouldDistributePagesElemn = document.getElementById(constants_1.Constants.Ids.checkboxToDistributePages);
            var checkboxCheckElems = shouldDistributePagesElemn.getElementsByClassName(constants_1.Constants.Classes.checkboxCheck);
            strictEqual(checkboxCheckElems.length, 0, "The checkbox to distribute the pages should not be filled");
        });
        test("Given that the shouldAttachPdf prop is true, the attachment checkbox should be selected", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.moreClipOptions).click();
            });
            var shouldAttachPdfElem = document.getElementById(constants_1.Constants.Ids.checkboxToAttachPdf);
            var checkboxCheckElems = shouldAttachPdfElem.getElementsByClassName(constants_1.Constants.Classes.checkboxCheck);
            strictEqual(checkboxCheckElems.length, 1, "The checkbox to attach the pdf should be filled");
        });
        test("Given that the shouldAttachPdf prop is false, the attachment checkbox should not be selected", function () {
            _this.defaultPdfClipOptionsProps.clipperState.pdfPreviewInfo.shouldAttachPdf = false;
            mithrilUtils_1.MithrilUtils.mountToFixture(<pdfClipOptions_1.PdfClipOptions {..._this.defaultPdfClipOptionsProps}/>);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.moreClipOptions).click();
            });
            var shouldAttachPdfElem = document.getElementById(constants_1.Constants.Ids.checkboxToAttachPdf);
            var checkboxCheckElems = shouldAttachPdfElem.getElementsByClassName(constants_1.Constants.Classes.checkboxCheck);
            strictEqual(checkboxCheckElems.length, 0, "The checkbox to attach the pdf should not be filled");
        });
        test("Clicking on the allPages radio should set allPages in clipperState to true", function () {
            var pdfClipOptions = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var allPagesElem = document.getElementById(constants_1.Constants.Ids.radioAllPagesLabel);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                allPagesElem.click();
            });
            ok(pdfClipOptions.props.clipperState.pdfPreviewInfo.allPages, "allPages in clipperState should be set to true");
        });
        test("Clicking on the pageRange radio should set allPages in clipperState to false", function () {
            var pdfClipOptions = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var pageRangeElem = document.getElementById(constants_1.Constants.Ids.radioPageRangeLabel);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                pageRangeElem.click();
            });
            ok(!pdfClipOptions.props.clipperState.pdfPreviewInfo.allPages, "allPages in clipperState should be set to false");
        });
        test("Clicking on the 'More' button should toggle the moreOptionsOpened boolean", function () {
            var pdfClipOptions = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var initialMoreOptionsOpenedValue = pdfClipOptions.state.moreOptionsOpened;
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.moreClipOptions).click();
            });
            strictEqual(pdfClipOptions.state.moreOptionsOpened, !initialMoreOptionsOpenedValue, "moreOptionsOpened in pdfClipOptionsState should be toggled (first click)");
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.moreClipOptions).click();
            });
            strictEqual(pdfClipOptions.state.moreOptionsOpened, initialMoreOptionsOpenedValue, "moreOptionsOpened in pdfClipOptionsState should be toggled (second click)");
        });
        test("Clicking on shouldDistributesPagesCheckbox should toggle the shouldDistributePages boolean", function () {
            var pdfClipOptions = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var initialCheckboxValue = pdfClipOptions.props.clipperState.pdfPreviewInfo.shouldDistributePages;
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.moreClipOptions).click();
            });
            var checkboxToDistributePagesElem = document.getElementById(constants_1.Constants.Ids.checkboxToDistributePages);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                checkboxToDistributePagesElem.click();
            });
            strictEqual(pdfClipOptions.props.clipperState.pdfPreviewInfo.shouldDistributePages, !initialCheckboxValue, "shouldDistributePages in clipperState should be toggled (first click)");
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                checkboxToDistributePagesElem.click();
            });
            strictEqual(pdfClipOptions.props.clipperState.pdfPreviewInfo.shouldDistributePages, initialCheckboxValue, "shouldDistributePages in clipperState should be toggled (second click)");
        });
        test("Clicking on the attachment checkbox should toggle the shouldAttachPdf boolean", function () {
            var pdfClipOptions = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var initialCheckboxValue = pdfClipOptions.props.clipperState.pdfPreviewInfo.shouldAttachPdf;
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.moreClipOptions).click();
            });
            var attachmentCheckboxElem = document.getElementById(constants_1.Constants.Ids.checkboxToAttachPdf);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                attachmentCheckboxElem.click();
            });
            strictEqual(pdfClipOptions.props.clipperState.pdfPreviewInfo.shouldAttachPdf, !initialCheckboxValue, "shouldAttachPdf in clipperState should be toggled (first click)");
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                attachmentCheckboxElem.click();
            });
            strictEqual(pdfClipOptions.props.clipperState.pdfPreviewInfo.shouldAttachPdf, initialCheckboxValue, "shouldAttachPdf in clipperState should be toggled (second click)");
        });
        test("If the pdf is below the MIME size limit, the AttachPdfFile should be shown", function () {
            var pdfClipOptions = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.moreClipOptions).click();
            });
            var attachmentCheckboxElem = document.getElementById(constants_1.Constants.Ids.checkboxToAttachPdf);
            strictEqual(attachmentCheckboxElem.innerText, _this.stringsJson["WebClipper.Label.AttachPdfFile"] + " " + _this.stringsJson["WebClipper.Label.AttachPdfFileSubText"]);
        });
        test("If the pdf is above the MIME size limit, the PdfTooLargeToAttach should be shown instead of AttachPdfFile", function () {
            _this.defaultPdfClipOptionsProps.clipperState.pdfResult.data.get().byteLength = constants_1.Constants.Settings.maximumMimeSizeLimit + 1;
            var pdfClipOptions = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.moreClipOptions).click();
            });
            var attachmentCheckboxElem = document.getElementById(constants_1.Constants.Ids.pdfIsTooLargeToAttachIndicator);
            strictEqual(attachmentCheckboxElem.innerText.trim(), _this.stringsJson["WebClipper.Label.PdfTooLargeToAttach"]);
        });
        test("If the PDF result has not started, or has failed, the checkboxToAttachPdf should be visible but disabled", function () {
            _this.defaultPdfClipOptionsProps.clipperState.pdfResult.status = status_1.Status.NotStarted;
            var pdfClipOptions = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.moreClipOptions).click();
            });
            var checkbox = document.getElementById(constants_1.Constants.Ids.checkboxToAttachPdf);
            ok(checkbox, "The checkboxToAttachPdf should be visible");
            ok(checkbox.classList.contains("disabled"), "The checkboxToAttachPdf should be disabled");
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                _this.defaultPdfClipOptionsProps.clipperState.pdfResult.status = status_1.Status.InProgress;
            });
            ok(document.getElementById(constants_1.Constants.Ids.checkboxToAttachPdf), "The checkboxToAttachPdf should still be visible");
            ok(checkbox.classList.contains("disabled"), "The checkboxToAttachPdf should be disabled");
        });
        test("If the PDF result has finished, and the PDF is too large, the checkboxToAttachPdf should not be visible, and the pdfIsTooLargeToAttachIndicator should be visible", function () {
            var defaultClipperState = mockProps_1.MockProps.getMockClipperState();
            defaultClipperState.pdfResult.status = status_1.Status.Succeeded;
            defaultClipperState.pdfResult.data.set({
                pdf: new MockPdfDocument_1.MockPdfDocument(),
                viewportDimensions: MockPdfDocument_1.MockPdfValues.dimensions,
                byteLength: constants_1.Constants.Settings.maximumMimeSizeLimit + 5
            });
            _this.defaultPdfClipOptionsProps = {
                clipperState: defaultClipperState
            };
            _this.defaultComponent = <pdfClipOptions_1.PdfClipOptions {..._this.defaultPdfClipOptionsProps}/>;
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.moreClipOptions).click();
            });
            ok(document.getElementById(constants_1.Constants.Ids.pdfIsTooLargeToAttachIndicator), "The pdfIsTooLargeToAttachIndicator should be visible");
        });
        test("Given that shouldShowPopover is true, then the popover should be visible", function () {
            _this.defaultPdfClipOptionsProps.clipperState.pdfPreviewInfo.allPages = false;
            _this.defaultPdfClipOptionsProps.clipperState.pdfPreviewInfo.shouldShowPopover = true;
            var pdfClipOptions = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            ok(document.querySelector("." + constants_1.Constants.Classes.popover), "The popover should be visible");
        });
    };
    return PdfClipOptionsTests;
}(testModule_1.TestModule));
exports.PdfClipOptionsTests = PdfClipOptionsTests;
(new PdfClipOptionsTests()).runTests();
