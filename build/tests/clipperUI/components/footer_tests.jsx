"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../scripts/constants");
var stringUtils_1 = require("../../../scripts/stringUtils");
var frontEndGlobals_1 = require("../../../scripts/clipperUI/frontEndGlobals");
var footer_1 = require("../../../scripts/clipperUI/components/footer");
var assert_1 = require("../../assert");
var mithrilUtils_1 = require("../../mithrilUtils");
var mockProps_1 = require("../../mockProps");
var testModule_1 = require("../../testModule");
var TestConstants;
(function (TestConstants) {
    var LogCategories;
    (function (LogCategories) {
        LogCategories.oneNoteClipperUsage = "OneNoteClipperUsage";
    })(LogCategories = TestConstants.LogCategories || (TestConstants.LogCategories = {}));
    var Urls;
    (function (Urls) {
        Urls.clipperFeedbackUrl = "http://localhost/feedback";
    })(Urls = TestConstants.Urls || (TestConstants.Urls = {}));
})(TestConstants || (TestConstants = {}));
var FooterTests = (function (_super) {
    __extends(FooterTests, _super);
    function FooterTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FooterTests.prototype.module = function () {
        return "footer";
    };
    FooterTests.prototype.beforeEach = function () {
        this.startingState = mockProps_1.MockProps.getMockClipperState();
        frontEndGlobals_1.Clipper.sessionId.set(stringUtils_1.StringUtils.generateGuid());
        this.defaultComponent =
            <footer_1.Footer clipperState={this.startingState}/>;
    };
    FooterTests.prototype.tests = function () {
        var _this = this;
        test("The footer should be collapsed by default", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            ok(!document.getElementById(constants_1.Constants.Ids.userSettingsContainer), "The user settings container should not be rendered by default");
        });
        test("The footer should be expanded after clicking on the user control dropdown button", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var currentUserControl = document.getElementById(constants_1.Constants.Ids.currentUserControl);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                currentUserControl.click();
            });
            ok(document.getElementById(constants_1.Constants.Ids.userSettingsContainer), "The user settings container should be rendered after clicking the user control dropdown button");
        });
        test("The footer should be collapsed when clicking on the user control dropdown button twice", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var currentUserControl = document.getElementById(constants_1.Constants.Ids.currentUserControl);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                currentUserControl.click();
                currentUserControl.click();
            });
            ok(!document.getElementById(constants_1.Constants.Ids.userSettingsContainer), "The user settings container should not be rendered after clicking the user control dropdown button twice");
        });
        test("The footer should remain expanded after clicking on the user control dropdown button then losing focus", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var currentUserControl = document.getElementById(constants_1.Constants.Ids.currentUserControl);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                currentUserControl.focus();
                currentUserControl.click();
                currentUserControl.blur();
            });
            ok(document.getElementById(constants_1.Constants.Ids.userSettingsContainer), "The user settings container remain open regardless of focus");
        });
        test("The user settings opened state should match the visibility of the user settings container", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var currentUserControl = document.getElementById(constants_1.Constants.Ids.currentUserControl);
            strictEqual(controllerInstance.state.userSettingsOpened, false, "userSettingsOpened should be false by default");
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                currentUserControl.click();
            });
            strictEqual(controllerInstance.state.userSettingsOpened, true, "userSettingsOpened should be true after the user control dropdown button has been clicked once");
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                currentUserControl.click();
            });
            strictEqual(controllerInstance.state.userSettingsOpened, false, "userSettingsOpened should be false after the user control dropdown button has been clicked twice");
        });
        test("The footer fields should be populated with the current user's info", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var currentUserName = document.getElementById(constants_1.Constants.Ids.currentUserName);
            var currentUserId = document.getElementById(constants_1.Constants.Ids.currentUserId);
            strictEqual(currentUserName.innerText, _this.startingState.userResult.data.user.fullName);
            strictEqual(currentUserId.innerText, _this.startingState.userResult.data.user.emailAddress);
            var currentUserControl = document.getElementById(constants_1.Constants.Ids.currentUserControl);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                currentUserControl.click();
            });
            var currentUserEmail = document.getElementById(constants_1.Constants.Ids.currentUserEmail);
            strictEqual(currentUserEmail.innerText, _this.startingState.userResult.data.user.emailAddress);
        });
        test("On clicking the sign out button, the user state must be set to undefined and userSettingsOpened state should be set to false", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.currentUserControl).click();
            });
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.signOutButton).click();
            });
            strictEqual(controllerInstance.props.clipperState.user, undefined, "User token should be set to undefined on signout");
            strictEqual(controllerInstance.state.userSettingsOpened, false, "userSettingsOpened should be set to false on signout");
        });
        test("The feedback popup should not be open by default", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var feedbackWindowRef = controllerInstance.getFeedbackWindowRef();
            ok(!feedbackWindowRef || feedbackWindowRef.closed, "Popup should not be opened by default");
        });
        test("On clicking the feedback button, a popup should open", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.feedbackButton).click();
            });
            ok(!controllerInstance.getFeedbackWindowRef().closed, "Popup should open when feedback button is clicked");
        });
        test("The tabbing should flow from the feedback to dropdown to sign out buttons", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var dropdown = document.getElementById(constants_1.Constants.Ids.currentUserControl);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                dropdown.click();
            });
            assert_1.Assert.tabOrderIsIncremental([constants_1.Constants.Ids.feedbackButton, constants_1.Constants.Ids.currentUserControl, constants_1.Constants.Ids.signOutButton]);
        });
    };
    return FooterTests;
}(testModule_1.TestModule));
exports.FooterTests = FooterTests;
(new FooterTests()).runTests();
