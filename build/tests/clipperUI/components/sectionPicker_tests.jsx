"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var sinon = require("sinon");
var frontEndGlobals_1 = require("../../../scripts/clipperUI/frontEndGlobals");
var status_1 = require("../../../scripts/clipperUI/status");
var oneNoteApiUtils_1 = require("../../../scripts/clipperUI/oneNoteApiUtils");
var sectionPicker_1 = require("../../../scripts/clipperUI/components/sectionPicker");
var clipperStorageKeys_1 = require("../../../scripts/storage/clipperStorageKeys");
var mithrilUtils_1 = require("../../mithrilUtils");
var mockProps_1 = require("../../mockProps");
var testModule_1 = require("../../testModule");
var TestConstants;
(function (TestConstants) {
    var Ids;
    (function (Ids) {
        Ids.sectionLocationContainer = "sectionLocationContainer";
        Ids.sectionPickerContainer = "sectionPickerContainer";
    })(Ids = TestConstants.Ids || (TestConstants.Ids = {}));
    TestConstants.defaultUserInfoAsJsonString = JSON.stringify({
        emailAddress: "mockEmail@hotmail.com",
        fullName: "Mock Johnson",
        accessToken: "mockToken",
        accessTokenExpiration: 3000
    });
})(TestConstants || (TestConstants = {}));
// Mock out the Clipper.Storage functionality
var mockStorage = {};
frontEndGlobals_1.Clipper.getStoredValue = function (key, callback) {
    callback(mockStorage[key]);
};
frontEndGlobals_1.Clipper.storeValue = function (key, value) {
    mockStorage[key] = value;
};
function initializeClipperStorage(notebooks, curSection, userInfo) {
    mockStorage = {};
    frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks, notebooks);
    frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.currentSelectedSection, curSection);
    frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.userInformation, userInfo);
}
function createNotebook(id, isDefault, sectionGroups, sections) {
    return {
        name: id.toUpperCase(),
        isDefault: isDefault,
        userRole: undefined,
        isShared: true,
        links: undefined,
        id: id.toLowerCase(),
        self: undefined,
        createdTime: undefined,
        lastModifiedTime: undefined,
        createdBy: undefined,
        lastModifiedBy: undefined,
        sectionsUrl: undefined,
        sectionGroupsUrl: undefined,
        sections: sections,
        sectionGroups: sectionGroups
    };
}
;
function createSectionGroup(id, sectionGroups, sections) {
    return {
        name: id.toUpperCase(),
        id: id.toLowerCase(),
        self: undefined,
        createdTime: undefined,
        lastModifiedTime: undefined,
        createdBy: undefined,
        lastModifiedBy: undefined,
        sectionsUrl: undefined,
        sectionGroupsUrl: undefined,
        sections: sections,
        sectionGroups: sectionGroups
    };
}
;
function createSection(id, isDefault) {
    return {
        name: id.toUpperCase(),
        isDefault: isDefault,
        parentNotebook: undefined,
        id: id.toLowerCase(),
        self: undefined,
        createdTime: undefined,
        lastModifiedTime: undefined,
        createdBy: undefined,
        lastModifiedBy: undefined,
        pagesUrl: undefined,
        pages: undefined
    };
}
;
var SectionPickerTests = (function (_super) {
    __extends(SectionPickerTests, _super);
    function SectionPickerTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.mockClipperState = mockProps_1.MockProps.getMockClipperState();
        return _this;
    }
    SectionPickerTests.prototype.module = function () {
        return "sectionPicker";
    };
    SectionPickerTests.prototype.beforeEach = function () {
        this.defaultComponent = <sectionPicker_1.SectionPicker onPopupToggle={function () { }} clipperState={this.mockClipperState}/>;
    };
    SectionPickerTests.prototype.tests = function () {
        test("fetchCachedNotebookAndSectionInfoAsState should return the cached notebooks, cached current section, and the succeed status if cached information is found", function () {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            var mockNotebooks = mockProps_1.MockProps.getMockNotebooks();
            var mockSection = {
                section: mockNotebooks[0].sections[0],
                path: "A > B > C",
                parentId: mockNotebooks[0].id
            };
            initializeClipperStorage(JSON.stringify(mockNotebooks), JSON.stringify(mockSection));
            var component = <sectionPicker_1.SectionPicker onPopupToggle={function () { }} clipperState={clipperState}/>;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(component);
            controllerInstance.fetchCachedNotebookAndSectionInfoAsState(function (response) {
                strictEqual(JSON.stringify(response), JSON.stringify({ notebooks: mockNotebooks, status: status_1.Status.Succeeded, curSection: mockSection }), "The cached information should be returned as SectionPickerState");
            });
        });
        test("fetchCachedNotebookAndSectionInfoAsState should return undefined if no cached information is found", function () {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            initializeClipperStorage(undefined, undefined);
            var component = <sectionPicker_1.SectionPicker onPopupToggle={function () { }} clipperState={clipperState}/>;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(component);
            controllerInstance.fetchCachedNotebookAndSectionInfoAsState(function (response) {
                strictEqual(response, undefined, "The undefined notebooks and section information should be returned as SectionPickerState");
            });
        });
        test("fetchCachedNotebookAndSectionInfoAsState should return the cached notebooks, undefined section, and the succeed status if no cached section is found", function () {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            var mockNotebooks = mockProps_1.MockProps.getMockNotebooks();
            initializeClipperStorage(JSON.stringify(mockNotebooks), undefined);
            var component = <sectionPicker_1.SectionPicker onPopupToggle={function () { }} clipperState={clipperState}/>;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(component);
            controllerInstance.fetchCachedNotebookAndSectionInfoAsState(function (response) {
                strictEqual(JSON.stringify(response), JSON.stringify({ notebooks: mockNotebooks, status: status_1.Status.Succeeded, curSection: undefined }), "The cached information should be returned as SectionPickerState");
            });
        });
        test("fetchCachedNotebookAndSectionInfoAsState should return undefined when no notebooks are found, even if section information is found", function () {
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            var mockSection = {
                section: mockProps_1.MockProps.getMockNotebooks()[0].sections[0],
                path: "A > B > C",
                parentId: mockProps_1.MockProps.getMockNotebooks()[0].id
            };
            initializeClipperStorage(undefined, JSON.stringify(mockSection));
            var component = <sectionPicker_1.SectionPicker onPopupToggle={function () { }} clipperState={clipperState}/>;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(component);
            controllerInstance.fetchCachedNotebookAndSectionInfoAsState(function (response) {
                strictEqual(response, undefined, "The cached information should be returned as SectionPickerState");
            });
        });
        test("convertNotebookListToState should return the notebook list, success status, and default section in the general case", function () {
            var section = createSection("S", true);
            var sectionGroup2 = createSectionGroup("SG2", [], [section]);
            var sectionGroup1 = createSectionGroup("SG1", [sectionGroup2], []);
            var notebook = createNotebook("N", true, [sectionGroup1], []);
            var notebooks = [notebook];
            var actual = sectionPicker_1.SectionPickerClass.convertNotebookListToState(notebooks);
            strictEqual(actual.notebooks, notebooks, "The notebooks property is correct");
            strictEqual(actual.status, status_1.Status.Succeeded, "The status property is correct");
            deepEqual(actual.curSection, { path: "N > SG1 > SG2 > S", section: section }, "The curSection property is correct");
        });
        test("convertNotebookListToState should return the notebook list, success status, and undefined default section in case where there is no default section", function () {
            var sectionGroup2 = createSectionGroup("SG2", [], []);
            var sectionGroup1 = createSectionGroup("SG1", [sectionGroup2], []);
            var notebook = createNotebook("N", true, [sectionGroup1], []);
            var notebooks = [notebook];
            var actual = sectionPicker_1.SectionPickerClass.convertNotebookListToState(notebooks);
            strictEqual(actual.notebooks, notebooks, "The notebooks property is correct");
            strictEqual(actual.status, status_1.Status.Succeeded, "The status property is correct");
            strictEqual(actual.curSection, undefined, "The curSection property is undefined");
        });
        test("convertNotebookListToState should return the notebook list, success status, and undefined default section in case where there is only one empty notebook", function () {
            var notebook = createNotebook("N", true, [], []);
            var notebooks = [notebook];
            var actual = sectionPicker_1.SectionPickerClass.convertNotebookListToState(notebooks);
            strictEqual(actual.notebooks, notebooks, "The notebooks property is correct");
            strictEqual(actual.status, status_1.Status.Succeeded, "The status property is correct");
            strictEqual(actual.curSection, undefined, "The curSection property is undefined");
        });
        test("convertNotebookListToState should return the undefined notebook list, success status, and undefined default section if the input is undefined", function () {
            var actual = sectionPicker_1.SectionPickerClass.convertNotebookListToState(undefined);
            strictEqual(actual.notebooks, undefined, "The notebooks property is undefined");
            strictEqual(actual.status, status_1.Status.Succeeded, "The status property is correct");
            strictEqual(actual.curSection, undefined, "The curSection property is undefined");
        });
        test("convertNotebookListToState should return the empty notebook list, success status, and undefined default section if the input is undefined", function () {
            var actual = sectionPicker_1.SectionPickerClass.convertNotebookListToState([]);
            strictEqual(actual.notebooks.length, 0, "The notebooks property is the empty list");
            strictEqual(actual.status, status_1.Status.Succeeded, "The status property is correct");
            strictEqual(actual.curSection, undefined, "The curSection property is undefined");
        });
        test("formatSectionInfoForStorage should return a ' > ' delimited name path and the last element in the general case", function () {
            var section = createSection("4");
            var actual = sectionPicker_1.SectionPickerClass.formatSectionInfoForStorage([
                createNotebook("1"),
                createSectionGroup("2"),
                createSectionGroup("3"),
                section
            ]);
            deepEqual(actual, { path: "1 > 2 > 3 > 4", section: section }, "The section info should be formatted correctly");
        });
        test("formatSectionInfoForStorage should return a ' > ' delimited name path and the last element if there are no section groups", function () {
            var section = createSection("2");
            var actual = sectionPicker_1.SectionPickerClass.formatSectionInfoForStorage([
                createNotebook("1"),
                section
            ]);
            deepEqual(actual, { path: "1 > 2", section: section }, "The section info should be formatted correctly");
        });
        test("formatSectionInfoForStorage should return undefined if the list that is passed in is undefined", function () {
            var actual = sectionPicker_1.SectionPickerClass.formatSectionInfoForStorage(undefined);
            strictEqual(actual, undefined, "The section info should be formatted correctly");
        });
        test("formatSectionInfoForStorage should return undefined if the list that is passed in is empty", function () {
            var actual = sectionPicker_1.SectionPickerClass.formatSectionInfoForStorage([]);
            strictEqual(actual, undefined, "The section info should be formatted correctly");
        });
    };
    return SectionPickerTests;
}(testModule_1.TestModule));
exports.SectionPickerTests = SectionPickerTests;
var SectionPickerSinonTests = (function (_super) {
    __extends(SectionPickerSinonTests, _super);
    function SectionPickerSinonTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.mockClipperState = mockProps_1.MockProps.getMockClipperState();
        return _this;
    }
    SectionPickerSinonTests.prototype.module = function () {
        return "sectionPicker-sinon";
    };
    SectionPickerSinonTests.prototype.beforeEach = function () {
        this.defaultComponent = <sectionPicker_1.SectionPicker onPopupToggle={function () { }} clipperState={this.mockClipperState}/>;
        this.server = sinon.fakeServer.create();
        this.server.respondImmediately = true;
    };
    SectionPickerSinonTests.prototype.afterEach = function () {
        this.server.restore();
    };
    SectionPickerSinonTests.prototype.tests = function () {
        var _this = this;
        test("retrieveAndUpdateNotebookAndSectionSelection should update states correctly when there's notebook and curSection information found in storage," +
            "the user does not make a new selection, and then information is found on the server. Also the notebooks are the same in storage and on the server, " +
            "and the current section in storage is the same as the default section in the server's notebook list", function (assert) {
            var done = assert.async();
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            // Set up the storage mock
            var mockNotebooks = mockProps_1.MockProps.getMockNotebooks();
            var mockSection = {
                section: mockNotebooks[0].sections[0],
                path: "Clipper Test > Full Page",
                parentId: mockNotebooks[0].id
            };
            initializeClipperStorage(JSON.stringify(mockNotebooks), JSON.stringify(mockSection), TestConstants.defaultUserInfoAsJsonString);
            // After retrieving fresh notebooks, the storage should be updated with the fresh notebooks (although it's the same in this case)
            var freshNotebooks = mockProps_1.MockProps.getMockNotebooks();
            var responseJson = {
                "@odata.context": "https://www.onenote.com/api/v1.0/$metadata#me/notes/notebooks",
                value: freshNotebooks
            };
            _this.server.respondWith([200, { "Content-Type": "application/json" }, JSON.stringify(responseJson)]);
            var component = <sectionPicker_1.SectionPicker onPopupToggle={function () { }} clipperState={clipperState}/>;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(component);
            strictEqual(JSON.stringify(controllerInstance.state), JSON.stringify({ notebooks: mockNotebooks, status: status_1.Status.Succeeded, curSection: mockSection }), "After the component is mounted, the state should be updated to reflect the notebooks and section found in storage");
            controllerInstance.retrieveAndUpdateNotebookAndSectionSelection().then(function (response) {
                frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks, function (notebooks) {
                    frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.currentSelectedSection, function (curSection) {
                        strictEqual(notebooks, JSON.stringify(freshNotebooks), "After fresh notebooks have been retrieved, the storage should be updated with them. In this case, nothing should have changed.");
                        strictEqual(curSection, JSON.stringify(mockSection), "The current selected section in storage should not have changed");
                        strictEqual(JSON.stringify(controllerInstance.state.notebooks), JSON.stringify(freshNotebooks), "The state should always be updated with the fresh notebooks once it has been retrieved");
                        strictEqual(JSON.stringify(controllerInstance.state.curSection), JSON.stringify(mockSection), "Since curSection was found in storage, and the user did not make an action to select another section, it should remain the same in state");
                        strictEqual(controllerInstance.state.status, status_1.Status.Succeeded, "The status should be Succeeded");
                        done();
                    });
                });
            }, function (error) {
                ok(false, "reject should not be called");
            });
        });
        test("retrieveAndUpdateNotebookAndSectionSelection should update states correctly when there's notebook and curSection information found in storage," +
            "the user does not make a new selection, and then information is found on the server. The notebooks on the server is not the same as the ones in storage, " +
            "and the current section in storage is the same as the default section in the server's notebook list", function (assert) {
            var done = assert.async();
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            // Set up the storage mock
            var mockNotebooks = mockProps_1.MockProps.getMockNotebooks();
            var mockSection = {
                section: mockNotebooks[0].sections[0],
                path: "Clipper Test > Full Page",
                parentId: mockNotebooks[0].id
            };
            initializeClipperStorage(JSON.stringify(mockNotebooks), JSON.stringify(mockSection), TestConstants.defaultUserInfoAsJsonString);
            var component = <sectionPicker_1.SectionPicker onPopupToggle={function () { }} clipperState={clipperState}/>;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(component);
            strictEqual(JSON.stringify(controllerInstance.state), JSON.stringify({ notebooks: mockNotebooks, status: status_1.Status.Succeeded, curSection: mockSection }), "After the component is mounted, the state should be updated to reflect the notebooks and section found in storage");
            // After retrieving fresh notebooks, the storage should be updated with the fresh notebooks
            var freshNotebooks = mockProps_1.MockProps.getMockNotebooks();
            freshNotebooks.push(createNotebook("id", false, [], []));
            var responseJson = {
                "@odata.context": "https://www.onenote.com/api/v1.0/$metadata#me/notes/notebooks",
                value: freshNotebooks
            };
            _this.server.respondWith([200, { "Content-Type": "application/json" }, JSON.stringify(responseJson)]);
            controllerInstance.retrieveAndUpdateNotebookAndSectionSelection().then(function (response) {
                frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks, function (notebooks) {
                    frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.currentSelectedSection, function (curSection) {
                        strictEqual(notebooks, JSON.stringify(freshNotebooks), "After fresh notebooks have been retrieved, the storage should be updated with them. In this case, nothing should have changed.");
                        strictEqual(curSection, JSON.stringify(mockSection), "The current selected section in storage should not have changed");
                        strictEqual(JSON.stringify(controllerInstance.state.notebooks), JSON.stringify(freshNotebooks), "The state should always be updated with the fresh notebooks once it has been retrieved");
                        strictEqual(JSON.stringify(controllerInstance.state.curSection), JSON.stringify(mockSection), "Since curSection was found in storage, and the user did not make an action to select another section, it should remain the same in state");
                        strictEqual(controllerInstance.state.status, status_1.Status.Succeeded, "The status should be Succeeded");
                        done();
                    });
                });
            }, function (error) {
                ok(false, "reject should not be called");
            });
        });
        test("retrieveAndUpdateNotebookAndSectionSelection should update states correctly when there's notebook, but no curSection information found in storage," +
            "the user does not make a selection, and then information is found on the server. The notebooks on the server is the same as the ones in storage, " +
            "and the current section in storage is still undefined by the time the fresh notebooks have been retrieved", function (assert) {
            var done = assert.async();
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            // Set up the storage mock
            var mockNotebooks = mockProps_1.MockProps.getMockNotebooks();
            initializeClipperStorage(JSON.stringify(mockNotebooks), undefined, TestConstants.defaultUserInfoAsJsonString);
            var component = <sectionPicker_1.SectionPicker onPopupToggle={function () { }} clipperState={clipperState}/>;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(component);
            strictEqual(JSON.stringify(controllerInstance.state), JSON.stringify({ notebooks: mockNotebooks, status: status_1.Status.Succeeded, curSection: undefined }), "After the component is mounted, the state should be updated to reflect the notebooks and section found in storage");
            // After retrieving fresh notebooks, the storage should be updated with the fresh notebooks (although it's the same in this case)
            var freshNotebooks = mockProps_1.MockProps.getMockNotebooks();
            freshNotebooks.push(createNotebook("id", false, [], []));
            var responseJson = {
                "@odata.context": "https://www.onenote.com/api/v1.0/$metadata#me/notes/notebooks",
                value: freshNotebooks
            };
            _this.server.respondWith([200, { "Content-Type": "application/json" }, JSON.stringify(responseJson)]);
            // This is the default section in the mock notebooks, and this should be found in storage and state after fresh notebooks are retrieved
            var defaultSection = {
                path: "Clipper Test > Full Page",
                section: mockNotebooks[0].sections[0]
            };
            controllerInstance.retrieveAndUpdateNotebookAndSectionSelection().then(function (response) {
                frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks, function (notebooks) {
                    frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.currentSelectedSection, function (curSection) {
                        strictEqual(notebooks, JSON.stringify(freshNotebooks), "After fresh notebooks have been retrieved, the storage should be updated with them. In this case, nothing should have changed.");
                        strictEqual(curSection, JSON.stringify(defaultSection), "The current selected section in storage should have been updated with the default section since it was undefined before");
                        strictEqual(JSON.stringify(controllerInstance.state.notebooks), JSON.stringify(freshNotebooks), "The state should always be updated with the fresh notebooks once it has been retrieved");
                        strictEqual(JSON.stringify(controllerInstance.state.curSection), JSON.stringify(defaultSection), "Since curSection was not found in storage, and the user did not make an action to select another section, it should be updated in state");
                        strictEqual(controllerInstance.state.status, status_1.Status.Succeeded, "The status should be Succeeded");
                        done();
                    });
                });
            }, function (error) {
                ok(false, "reject should not be called");
            });
        });
        test("retrieveAndUpdateNotebookAndSectionSelection should update states correctly when there's notebook, but no curSection information found in storage," +
            "the user makes a new section selection, and then information is found on the server. The notebooks on the server is the same as the ones in storage, " +
            "and the current section in storage is still undefined by the time the fresh notebooks have been retrieved", function (assert) {
            var done = assert.async();
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            // Set up the storage mock
            var mockNotebooks = mockProps_1.MockProps.getMockNotebooks();
            initializeClipperStorage(JSON.stringify(mockNotebooks), undefined, TestConstants.defaultUserInfoAsJsonString);
            var component = <sectionPicker_1.SectionPicker onPopupToggle={function () { }} clipperState={clipperState}/>;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(component);
            strictEqual(JSON.stringify(controllerInstance.state), JSON.stringify({ notebooks: mockNotebooks, status: status_1.Status.Succeeded, curSection: undefined }), "After the component is mounted, the state should be updated to reflect the notebooks and section found in storage");
            // The user now clicks on a section (second section of second notebook)
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(TestConstants.Ids.sectionLocationContainer).click();
            });
            var sectionPicker = document.getElementById(TestConstants.Ids.sectionPickerContainer).firstElementChild;
            var second = sectionPicker.childNodes[1];
            var secondNotebook = second.childNodes[0];
            var secondSections = second.childNodes[1];
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                secondNotebook.click();
            });
            var newSelectedSection = secondSections.childNodes[1];
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                // The clickable element is actually the first childNode
                newSelectedSection.childNodes[0].click();
            });
            // This corresponds to the second section of the second notebook in the mock notebooks
            var selectedSection = {
                section: mockNotebooks[1].sections[1],
                path: "Clipper Test 2 > Section Y",
                parentId: "a-bc!d"
            };
            frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.currentSelectedSection, function (curSection1) {
                strictEqual(curSection1, JSON.stringify(selectedSection), "The current selected section in storage should have been updated with the selected section");
                strictEqual(JSON.stringify(controllerInstance.state.curSection), JSON.stringify(selectedSection), "The current selected section in state should have been updated with the selected section");
                // After retrieving fresh notebooks, the storage should be updated with the fresh notebooks (although it's the same in this case)
                var freshNotebooks = mockProps_1.MockProps.getMockNotebooks();
                freshNotebooks.push(createNotebook("id", false, [], []));
                var responseJson = {
                    "@odata.context": "https://www.onenote.com/api/v1.0/$metadata#me/notes/notebooks",
                    value: freshNotebooks
                };
                _this.server.respondWith([200, { "Content-Type": "application/json" }, JSON.stringify(responseJson)]);
                controllerInstance.retrieveAndUpdateNotebookAndSectionSelection().then(function (response) {
                    frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks, function (notebooks) {
                        frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.currentSelectedSection, function (curSection2) {
                            strictEqual(notebooks, JSON.stringify(freshNotebooks), "After fresh notebooks have been retrieved, the storage should be updated with them. In this case, nothing should have changed.");
                            strictEqual(curSection2, JSON.stringify(selectedSection), "The current selected section in storage should still be the selected section");
                            strictEqual(JSON.stringify(controllerInstance.state.notebooks), JSON.stringify(freshNotebooks), "The state should always be updated with the fresh notebooks once it has been retrieved");
                            strictEqual(JSON.stringify(controllerInstance.state.curSection), JSON.stringify(selectedSection), "The current selected section in state should still be the selected section");
                            strictEqual(controllerInstance.state.status, status_1.Status.Succeeded, "The status should be Succeeded");
                            done();
                        });
                    });
                }, function (error) {
                    ok(false, "reject should not be called");
                });
            });
        });
        test("retrieveAndUpdateNotebookAndSectionSelection should update states correctly when there's notebook and curSection information found in storage," +
            " and then information is found on the server, but that selected section no longer exists.", function (assert) {
            var done = assert.async();
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            // Set up the storage mock
            var mockNotebooks = mockProps_1.MockProps.getMockNotebooks();
            var mockSection = {
                section: mockNotebooks[0].sections[0],
                path: "Clipper Test > Full Page",
                parentId: mockNotebooks[0].id
            };
            initializeClipperStorage(JSON.stringify(mockNotebooks), JSON.stringify(mockSection), TestConstants.defaultUserInfoAsJsonString);
            var component = <sectionPicker_1.SectionPicker onPopupToggle={function () { }} clipperState={clipperState}/>;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(component);
            strictEqual(JSON.stringify(controllerInstance.state), JSON.stringify({ notebooks: mockNotebooks, status: status_1.Status.Succeeded, curSection: mockSection }), "After the component is mounted, the state should be updated to reflect the notebooks and section found in storage");
            // After retrieving fresh notebooks, the storage should be updated with the fresh notebooks (we deleted the cached currently selected section)
            var freshNotebooks = mockProps_1.MockProps.getMockNotebooks();
            freshNotebooks[0].sections = [];
            var responseJson = {
                "@odata.context": "https://www.onenote.com/api/v1.0/$metadata#me/notes/notebooks",
                value: freshNotebooks
            };
            _this.server.respondWith([200, { "Content-Type": "application/json" }, JSON.stringify(responseJson)]);
            controllerInstance.retrieveAndUpdateNotebookAndSectionSelection().then(function (response) {
                frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks, function (notebooks) {
                    frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.currentSelectedSection, function (curSection2) {
                        strictEqual(notebooks, JSON.stringify(freshNotebooks), "After fresh notebooks have been retrieved, the storage should be updated with them.");
                        strictEqual(curSection2, undefined, "The current selected section in storage should now be undefined as it no longer exists in the fresh notebooks");
                        strictEqual(JSON.stringify(controllerInstance.state.notebooks), JSON.stringify(freshNotebooks), "The state should always be updated with the fresh notebooks once it has been retrieved");
                        strictEqual(controllerInstance.state.curSection, undefined, "The current selected section in state should be undefined");
                        strictEqual(controllerInstance.state.status, status_1.Status.Succeeded, "The status should be Succeeded");
                        done();
                    });
                });
            }, function (error) {
                ok(false, "reject should not be called");
            });
        });
        test("retrieveAndUpdateNotebookAndSectionSelection should update states correctly when there's notebook and curSection information found in storage," +
            "the user does not make a new selection, and then notebooks is incorrectly returned as undefined or null from the server", function (assert) {
            var done = assert.async();
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            // Set up the storage mock
            var mockNotebooks = mockProps_1.MockProps.getMockNotebooks();
            var mockSection = {
                section: mockNotebooks[0].sections[0],
                path: "Clipper Test > Full Page",
                parentId: mockNotebooks[0].id
            };
            initializeClipperStorage(JSON.stringify(mockNotebooks), JSON.stringify(mockSection), TestConstants.defaultUserInfoAsJsonString);
            var component = <sectionPicker_1.SectionPicker onPopupToggle={function () { }} clipperState={clipperState}/>;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(component);
            strictEqual(JSON.stringify(controllerInstance.state), JSON.stringify({ notebooks: mockNotebooks, status: status_1.Status.Succeeded, curSection: mockSection }), "After the component is mounted, the state should be updated to reflect the notebooks and section found in storage");
            // After retrieving fresh undefined notebooks, the storage should not be updated with the undefined value, but should still keep the old cached information
            var responseJson = {
                "@odata.context": "https://www.onenote.com/api/v1.0/$metadata#me/notes/notebooks",
                value: undefined
            };
            _this.server.respondWith([200, { "Content-Type": "application/json" }, JSON.stringify(responseJson)]);
            controllerInstance.retrieveAndUpdateNotebookAndSectionSelection().then(function (response) {
                ok(false, "resolve should not be called");
            }, function (error) {
                frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks, function (notebooks) {
                    frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.currentSelectedSection, function (curSection) {
                        strictEqual(notebooks, JSON.stringify(mockNotebooks), "After undefined notebooks have been retrieved, the storage should not be updated with them.");
                        strictEqual(curSection, JSON.stringify(mockSection), "The current selected section in storage should not have changed");
                        strictEqual(JSON.stringify(controllerInstance.state.notebooks), JSON.stringify(mockNotebooks), "The state should not be updated as retrieving fresh notebooks returned undefined");
                        strictEqual(JSON.stringify(controllerInstance.state.curSection), JSON.stringify(mockSection), "Since curSection was found in storage, and the user did not make an action to select another section, it should remain the same in state");
                        strictEqual(controllerInstance.state.status, status_1.Status.Failed, "The status should be Failed");
                        done();
                    });
                });
            });
        });
        test("retrieveAndUpdateNotebookAndSectionSelection should update states correctly when there's notebook and curSection information found in storage," +
            "the user does not make a new selection, and the server returns an error status code", function (assert) {
            var done = assert.async();
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            // Set up the storage mock
            var mockNotebooks = mockProps_1.MockProps.getMockNotebooks();
            var mockSection = {
                section: mockNotebooks[0].sections[0],
                path: "Clipper Test > Full Page",
                parentId: mockNotebooks[0].id
            };
            initializeClipperStorage(JSON.stringify(mockNotebooks), JSON.stringify(mockSection), TestConstants.defaultUserInfoAsJsonString);
            var component = <sectionPicker_1.SectionPicker onPopupToggle={function () { }} clipperState={clipperState}/>;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(component);
            strictEqual(JSON.stringify(controllerInstance.state), JSON.stringify({ notebooks: mockNotebooks, status: status_1.Status.Succeeded, curSection: mockSection }), "After the component is mounted, the state should be updated to reflect the notebooks and section found in storage");
            // After retrieving fresh undefined notebooks, the storage should not be updated with the undefined value, but should still keep the old cached information
            var responseJson = {};
            _this.server.respondWith([404, { "Content-Type": "application/json" }, JSON.stringify(responseJson)]);
            controllerInstance.retrieveAndUpdateNotebookAndSectionSelection().then(function (response) {
                ok(false, "resolve should not be called");
            }, function (error) {
                frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks, function (notebooks) {
                    frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.currentSelectedSection, function (curSection) {
                        strictEqual(notebooks, JSON.stringify(mockNotebooks), "After undefined notebooks have been retrieved, the storage should not be updated with them.");
                        strictEqual(curSection, JSON.stringify(mockSection), "The current selected section in storage should not have changed");
                        strictEqual(JSON.stringify(controllerInstance.state.notebooks), JSON.stringify(mockNotebooks), "The state should not be updated as retrieving fresh notebooks returned undefined");
                        strictEqual(JSON.stringify(controllerInstance.state.curSection), JSON.stringify(mockSection), "Since curSection was found in storage, and the user did not make an action to select another section, it should remain the same in state");
                        strictEqual(controllerInstance.state.status, status_1.Status.Succeeded, "The status should be Succeeded since we have a fallback in storage");
                        done();
                    });
                });
            });
        });
        test("retrieveAndUpdateNotebookAndSectionSelection should update states correctly when there's no notebook and curSection information found in storage," +
            "the user does not make a new selection, and the server returns an error status code, therefore there's no fallback notebooks", function (assert) {
            var done = assert.async();
            var clipperState = mockProps_1.MockProps.getMockClipperState();
            // Set up the storage mock
            initializeClipperStorage(undefined, undefined, TestConstants.defaultUserInfoAsJsonString);
            var component = <sectionPicker_1.SectionPicker onPopupToggle={function () { }} clipperState={clipperState}/>;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(component);
            strictEqual(JSON.stringify(controllerInstance.state), JSON.stringify({ notebooks: undefined, status: status_1.Status.NotStarted, curSection: undefined }), "After the component is mounted, the state should be updated to reflect that notebooks and current section are not found in storage");
            // After retrieving fresh undefined notebooks, the storage should not be updated with the undefined value, but should still keep the old cached information
            var responseJson = {};
            _this.server.respondWith([404, { "Content-Type": "application/json" }, JSON.stringify(responseJson)]);
            controllerInstance.retrieveAndUpdateNotebookAndSectionSelection().then(function (response) {
                ok(false, "resolve should not be called");
            }, function (error) {
                frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks, function (notebooks) {
                    frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.currentSelectedSection, function (curSection) {
                        strictEqual(notebooks, undefined, "After undefined notebooks have been retrieved, the storage notebook value should still be undefined");
                        strictEqual(curSection, undefined, "After undefined notebooks have been retrieved, the storage section value should still be undefined as there was no default section present");
                        strictEqual(controllerInstance.state.notebooks, undefined, "After undefined notebooks have been retrieved, the state notebook value should still be undefined");
                        strictEqual(controllerInstance.state.curSection, undefined, "After undefined notebooks have been retrieved, the state section value should still be undefined as there was no default section present");
                        strictEqual(controllerInstance.state.status, status_1.Status.Failed, "The status should be Failed since getting fresh notebooks has failed and we don't have a fallback");
                        done();
                    });
                });
            });
        });
        test("fetchFreshNotebooks should parse out @odata.context from the raw 200 response and return the notebook object list and XHR in the resolve", function (assert) {
            var done = assert.async();
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var notebooks = mockProps_1.MockProps.getMockNotebooks();
            var responseJson = {
                "@odata.context": "https://www.onenote.com/api/v1.0/$metadata#me/notes/notebooks",
                value: notebooks
            };
            _this.server.respondWith([200, { "Content-Type": "application/json" }, JSON.stringify(responseJson)]);
            controllerInstance.fetchFreshNotebooks("sessionId").then(function (responsePackage) {
                strictEqual(JSON.stringify(responsePackage.parsedResponse), JSON.stringify(notebooks), "The notebook list should be present in the response");
                ok(!!responsePackage.request, "The XHR must be present in the response");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("fetchFreshNotebooks should parse out @odata.context from the raw 201 response and return the notebook object list and XHR in the resolve", function (assert) {
            var done = assert.async();
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var notebooks = mockProps_1.MockProps.getMockNotebooks();
            var responseJson = {
                "@odata.context": "https://www.onenote.com/api/v1.0/$metadata#me/notes/notebooks",
                value: notebooks
            };
            _this.server.respondWith([201, { "Content-Type": "application/json" }, JSON.stringify(responseJson)]);
            controllerInstance.fetchFreshNotebooks("sessionId").then(function (responsePackage) {
                strictEqual(JSON.stringify(responsePackage.parsedResponse), JSON.stringify(notebooks), "The notebook list should be present in the response");
                ok(!!responsePackage.request, "The XHR must be present in the response");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("fetchFreshNotebooks should reject with the error object and a copy of the response if the status code is 4XX", function (assert) {
            var done = assert.async();
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var responseJson = {
                error: "Unexpected response status",
                statusCode: 401,
                responseHeaders: { "Content-Type": "application/json" }
            };
            var expected = {
                error: responseJson.error,
                statusCode: responseJson.statusCode,
                responseHeaders: responseJson.responseHeaders,
                response: JSON.stringify(responseJson),
                timeout: 30000
            };
            _this.server.respondWith([expected.statusCode, expected.responseHeaders, expected.response]);
            controllerInstance.fetchFreshNotebooks("sessionId").then(function (responsePackage) {
                ok(false, "resolve should not be called");
            }, function (error) {
                deepEqual(error, expected, "The error object should be rejected");
                strictEqual(controllerInstance.state.apiResponseCode, undefined);
            }).then(function () {
                done();
            });
        });
        test("fetchFreshNotebooks should reject with the error object and an API response code if one is returned by the API", function (assert) {
            var done = assert.async();
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var responseJson = {
                error: {
                    code: "10008",
                    message: "The user's OneDrive, Group or Document Library contains more than 5000 items and cannot be queried using the API.",
                    "@api.url": "http://aka.ms/onenote-errors#C10008"
                }
            };
            var expected = {
                error: "Unexpected response status",
                statusCode: 403,
                responseHeaders: { "Content-Type": "application/json" },
                response: JSON.stringify(responseJson),
                timeout: 30000
            };
            _this.server.respondWith([expected.statusCode, expected.responseHeaders, expected.response]);
            controllerInstance.fetchFreshNotebooks("sessionId").then(function (responsePackage) {
                ok(false, "resolve should not be called");
            }, function (error) {
                deepEqual(error, expected, "The error object should be rejected");
                ok(!oneNoteApiUtils_1.OneNoteApiUtils.isRetryable(controllerInstance.state.apiResponseCode));
            }).then(function () {
                done();
            });
        });
        test("fetchFreshNotebooks should reject with the error object and a copy of the response if the status code is 5XX", function (assert) {
            var done = assert.async();
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var responseJson = {
                error: "Unexpected response status",
                statusCode: 501,
                responseHeaders: { "Content-Type": "application/json" }
            };
            _this.server.respondWith([501, responseJson.responseHeaders, JSON.stringify(responseJson)]);
            var expected = {
                error: responseJson.error,
                statusCode: responseJson.statusCode,
                responseHeaders: responseJson.responseHeaders,
                response: JSON.stringify(responseJson),
                timeout: 30000
            };
            controllerInstance.fetchFreshNotebooks("sessionId").then(function (responsePackage) {
                ok(false, "resolve should not be called");
            }, function (error) {
                deepEqual(error, expected, "The error object should be rejected");
            }).then(function () {
                done();
            });
        });
    };
    return SectionPickerSinonTests;
}(testModule_1.TestModule));
exports.SectionPickerSinonTests = SectionPickerSinonTests;
(new SectionPickerTests()).runTests();
(new SectionPickerSinonTests()).runTests();
