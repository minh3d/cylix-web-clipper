"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var augmentationHelper_1 = require("../../../scripts/contentCapture/augmentationHelper");
var modeButtonSelector_1 = require("../../../scripts/clipperUI/components/modeButtonSelector");
var clipMode_1 = require("../../../scripts/clipperUI/clipMode");
var status_1 = require("../../../scripts/clipperUI/status");
var smartValue_1 = require("../../../scripts/communicator/smartValue");
var invokeOptions_1 = require("../../../scripts/extensions/invokeOptions");
var clientType_1 = require("../../../scripts/clientType");
var assert_1 = require("../../assert");
var mithrilUtils_1 = require("../../mithrilUtils");
var mockProps_1 = require("../../mockProps");
var testModule_1 = require("../../testModule");
// These are not available in constants.ts as we currently dynamically generate them
// at runtime
var TestConstants;
(function (TestConstants) {
    var Classes;
    (function (Classes) {
        Classes.icon = "icon";
        Classes.label = "label";
        Classes.modeButton = "modeButton";
        Classes.selected = "selected";
    })(Classes = TestConstants.Classes || (TestConstants.Classes = {}));
    var Ids;
    (function (Ids) {
        Ids.pdfButton = "pdfButton";
        Ids.fullPageButton = "fullPageButton";
        Ids.regionButton = "regionButton";
        Ids.augmentationButton = "augmentationButton";
        Ids.selectionButton = "selectionButton";
        Ids.bookmarkButton = "bookmarkButton";
    })(Ids = TestConstants.Ids || (TestConstants.Ids = {}));
})(TestConstants || (TestConstants = {}));
var ModeButtonSelectorTests = (function (_super) {
    __extends(ModeButtonSelectorTests, _super);
    function ModeButtonSelectorTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.stringsJson = require("../../../strings.json");
        return _this;
    }
    ModeButtonSelectorTests.prototype.module = function () {
        return "modeButtonSelector";
    };
    ModeButtonSelectorTests.prototype.beforeEach = function () {
        this.defaultComponent =
            m.component(modeButtonSelector_1.ModeButtonSelector, {clipperState:mockProps_1.MockProps.getMockClipperState()});
    };
    ModeButtonSelectorTests.prototype.tests = function () {
        var _this = this;
        test("The region clipping button should not appear when enableRegionClipping is injected as false", function () {
            var startingState = mockProps_1.MockProps.getMockClipperState();
            startingState.injectOptions.enableRegionClipping = false;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(modeButtonSelector_1.ModeButtonSelector, {clipperState:startingState}));
            var modeButtonSelector = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var buttonElements = modeButtonSelector.getElementsByClassName(TestConstants.Classes.modeButton);
            strictEqual(buttonElements.length, 3, "There should only be three mode buttons");
            strictEqual(buttonElements[0].id, TestConstants.Ids.fullPageButton, "The first button should be the full page button");
            strictEqual(buttonElements[1].id, TestConstants.Ids.augmentationButton, "The second button should be the augmentation button");
            strictEqual(buttonElements[2].id, TestConstants.Ids.bookmarkButton, "The third button should be the bookmark button");
        });
        test("The region clipping button should appear when enableRegionClipping is injected as true", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var modeButtonSelector = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var buttonElements = modeButtonSelector.getElementsByClassName(TestConstants.Classes.modeButton);
            strictEqual(buttonElements.length, 4, "There should be four mode buttons");
            strictEqual(buttonElements[0].id, TestConstants.Ids.fullPageButton, "The first button should be the full page button");
            strictEqual(buttonElements[1].id, TestConstants.Ids.regionButton, "The second button should be the region button");
            strictEqual(buttonElements[2].id, TestConstants.Ids.augmentationButton, "The third button should be the augmentation button");
            strictEqual(buttonElements[3].id, TestConstants.Ids.bookmarkButton, "The fourth button should be the bookmark button");
        });
        test("The region clipping button should appear when enableRegionClipping is injected as false, but invokeMode is set to image selection", function () {
            var startingState = mockProps_1.MockProps.getMockClipperState();
            startingState.injectOptions.enableRegionClipping = false;
            startingState.invokeOptions.invokeMode = invokeOptions_1.InvokeMode.ContextImage;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(modeButtonSelector_1.ModeButtonSelector, {clipperState:startingState}));
            var modeButtonSelector = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var buttonElements = modeButtonSelector.getElementsByClassName(TestConstants.Classes.modeButton);
            strictEqual(buttonElements.length, 4, "There should be four mode buttons");
            strictEqual(buttonElements[0].id, TestConstants.Ids.fullPageButton, "The first button should be the full page button");
            strictEqual(buttonElements[1].id, TestConstants.Ids.regionButton, "The second button should be the region button");
            strictEqual(buttonElements[2].id, TestConstants.Ids.augmentationButton, "The third button should be the augmentation button");
            strictEqual(buttonElements[3].id, TestConstants.Ids.bookmarkButton, "The fourth button should be the bookmark button");
        });
        test("The region button should be labeled 'Region' in non-Edge browsers", function () {
            var startingState = mockProps_1.MockProps.getMockClipperState();
            startingState.clientInfo.clipperType = clientType_1.ClientType.ChromeExtension;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(modeButtonSelector_1.ModeButtonSelector, {clipperState:startingState}));
            var modeButtonSelector = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var buttonElements = modeButtonSelector.getElementsByClassName(TestConstants.Classes.modeButton);
            var regionButton = buttonElements[1];
            var label = regionButton.getElementsByClassName(TestConstants.Classes.label)[0];
            strictEqual(label.textContent, _this.stringsJson["WebClipper.ClipType.Region.Button"]);
        });
        test("The region button should be labeled 'Region' in non-Edge browsers and an image was selected", function () {
            var startingState = mockProps_1.MockProps.getMockClipperState();
            startingState.clientInfo.clipperType = clientType_1.ClientType.FirefoxExtension;
            startingState.invokeOptions = {
                invokeMode: invokeOptions_1.InvokeMode.ContextImage,
                invokeDataForMode: "dummy-img"
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(modeButtonSelector_1.ModeButtonSelector, {clipperState:startingState}));
            var modeButtonSelector = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var buttonElements = modeButtonSelector.getElementsByClassName(TestConstants.Classes.modeButton);
            var regionButton = buttonElements[1];
            var label = regionButton.getElementsByClassName(TestConstants.Classes.label)[0];
            strictEqual(label.textContent, _this.stringsJson["WebClipper.ClipType.Region.Button"]);
        });
        test("The selection button should appear when invokeMode is set to selection", function () {
            var startingState = mockProps_1.MockProps.getMockClipperState();
            startingState.invokeOptions.invokeMode = invokeOptions_1.InvokeMode.ContextTextSelection;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(modeButtonSelector_1.ModeButtonSelector, {clipperState:startingState}));
            var modeButtonSelector = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var buttonElements = modeButtonSelector.getElementsByClassName(TestConstants.Classes.modeButton);
            strictEqual(buttonElements.length, 5, "There should be five mode buttons");
            strictEqual(buttonElements[0].id, TestConstants.Ids.fullPageButton, "The first button should be the full page button");
            strictEqual(buttonElements[1].id, TestConstants.Ids.regionButton, "The second button should be the region button");
            strictEqual(buttonElements[2].id, TestConstants.Ids.augmentationButton, "The third button should be the augmentation button");
            strictEqual(buttonElements[3].id, TestConstants.Ids.selectionButton, "The fourth button should be the selection button");
            strictEqual(buttonElements[4].id, TestConstants.Ids.bookmarkButton, "The fifth button should be the bookmark button");
        });
        test("The selection button should appear when invokeMode is set to selection, and the region button should not appear when its disabled", function () {
            var startingState = mockProps_1.MockProps.getMockClipperState();
            startingState.injectOptions.enableRegionClipping = false;
            startingState.invokeOptions.invokeMode = invokeOptions_1.InvokeMode.ContextTextSelection;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(modeButtonSelector_1.ModeButtonSelector, {clipperState:startingState}));
            var modeButtonSelector = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var buttonElements = modeButtonSelector.getElementsByClassName(TestConstants.Classes.modeButton);
            strictEqual(buttonElements.length, 4, "There should be four mode buttons");
            strictEqual(buttonElements[0].id, TestConstants.Ids.fullPageButton, "The first button should be the full page button");
            strictEqual(buttonElements[1].id, TestConstants.Ids.augmentationButton, "The second button should be the augmentation button");
            strictEqual(buttonElements[2].id, TestConstants.Ids.selectionButton, "The third button should be the selection button");
            strictEqual(buttonElements[3].id, TestConstants.Ids.bookmarkButton, "The fourth button should be the bookmark button");
        });
        test("The aria-posinset attribute should flow in element order, assuming they are all available if the user has selected an image in context image mode", function () {
            var startingState = mockProps_1.MockProps.getMockClipperState();
            startingState.invokeOptions.invokeMode = invokeOptions_1.InvokeMode.ContextImage;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(modeButtonSelector_1.ModeButtonSelector, {clipperState:startingState}));
            assert_1.Assert.checkAriaSetAttributes([TestConstants.Ids.fullPageButton,
                TestConstants.Ids.regionButton, TestConstants.Ids.augmentationButton, TestConstants.Ids.bookmarkButton]);
        });
        test("The aria-posinset attribute should flow in element order, assuming they are all available if the user has selected text in context text mode", function () {
            var startingState = mockProps_1.MockProps.getMockClipperState();
            startingState.invokeOptions.invokeMode = invokeOptions_1.InvokeMode.Default;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(modeButtonSelector_1.ModeButtonSelector, {clipperState:startingState}));
            assert_1.Assert.checkAriaSetAttributes([TestConstants.Ids.fullPageButton,
                TestConstants.Ids.regionButton, TestConstants.Ids.augmentationButton, TestConstants.Ids.bookmarkButton]);
        });
        test("The aria-posinset attribute should flow in element order, assuming they are all available if the clipper is in default mode (no selected text or image)", function () {
            var startingState = mockProps_1.MockProps.getMockClipperState();
            startingState.invokeOptions.invokeMode = invokeOptions_1.InvokeMode.Default;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(modeButtonSelector_1.ModeButtonSelector, {clipperState:startingState}));
            assert_1.Assert.checkAriaSetAttributes([TestConstants.Ids.fullPageButton,
                TestConstants.Ids.regionButton, TestConstants.Ids.augmentationButton, TestConstants.Ids.bookmarkButton]);
        });
        test("The full page button should have the 'selected' class styling applied to it by default", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var modeButtonSelector = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var buttonElements = modeButtonSelector.getElementsByClassName(TestConstants.Classes.modeButton);
            var fullPageButton = buttonElements[0];
            var regionButton = buttonElements[1];
            var augmentationButton = buttonElements[2];
            ok(fullPageButton.classList.contains(TestConstants.Classes.selected), "The fullpage button is selected");
            ok(!regionButton.classList.contains(TestConstants.Classes.selected), "The region button is not selected");
            ok(!augmentationButton.classList.contains(TestConstants.Classes.selected), "The augmentation button is not selected");
        });
        test("Other modes' buttons should have the 'selected' class styling applied to it if it's initially set as the starting mode", function () {
            var startingState = mockProps_1.MockProps.getMockClipperState();
            startingState.currentMode = new smartValue_1.SmartValue(clipMode_1.ClipMode.Region);
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(modeButtonSelector_1.ModeButtonSelector, {clipperState:startingState}));
            var modeButtonSelector = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var buttonElements = modeButtonSelector.getElementsByClassName(TestConstants.Classes.modeButton);
            var fullPageButton = buttonElements[0];
            var regionButton = buttonElements[1];
            var augmentationButton = buttonElements[2];
            ok(!fullPageButton.classList.contains(TestConstants.Classes.selected), "The fullpage button is not selected");
            ok(regionButton.classList.contains(TestConstants.Classes.selected), "The region button is selected");
            ok(!augmentationButton.classList.contains(TestConstants.Classes.selected), "The augmentation button is not selected");
        });
        test("Other modes' buttons should have the 'selected' class styling applied to it if they are clicked on", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var fullPageButton = document.getElementById(TestConstants.Ids.fullPageButton);
            var regionButton = document.getElementById(TestConstants.Ids.regionButton);
            var augmentationButton = document.getElementById(TestConstants.Ids.augmentationButton);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                regionButton.click();
            });
            ok(!fullPageButton.classList.contains(TestConstants.Classes.selected), "The fullpage button is not selected");
            ok(regionButton.classList.contains(TestConstants.Classes.selected), "The region button is selected");
            ok(!augmentationButton.classList.contains(TestConstants.Classes.selected), "The augmentation button is not selected");
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                fullPageButton.click();
            });
            ok(fullPageButton.classList.contains(TestConstants.Classes.selected), "The fullpage button is selected");
            ok(!regionButton.classList.contains(TestConstants.Classes.selected), "The region button is not selected");
            ok(!augmentationButton.classList.contains(TestConstants.Classes.selected), "The augmentation button is not selected");
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                augmentationButton.click();
            });
            ok(!fullPageButton.classList.contains(TestConstants.Classes.selected), "The fullpage button is not selected");
            ok(!regionButton.classList.contains(TestConstants.Classes.selected), "The region button is not selected");
            ok(augmentationButton.classList.contains(TestConstants.Classes.selected), "The augmentation button is selected");
        });
        test("The 'selected' class styling should not go away if the user clicks away from a previously clicked mode button", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var fullPageButton = document.getElementById(TestConstants.Ids.fullPageButton);
            var regionButton = document.getElementById(TestConstants.Ids.regionButton);
            var augmentationButton = document.getElementById(TestConstants.Ids.augmentationButton);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                regionButton.click();
                regionButton.blur();
            });
            ok(!fullPageButton.classList.contains(TestConstants.Classes.selected), "The fullpage button is not selected");
            ok(regionButton.classList.contains(TestConstants.Classes.selected), "The region button is selected");
            ok(!augmentationButton.classList.contains(TestConstants.Classes.selected), "The augmentation button is not selected");
        });
        test("The current mode state should be updated accordingly depending on the mode button that was pressed", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var fullPageButton = document.getElementById(TestConstants.Ids.fullPageButton);
            var regionButton = document.getElementById(TestConstants.Ids.regionButton);
            var augmentationButton = document.getElementById(TestConstants.Ids.augmentationButton);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                regionButton.click();
            });
            strictEqual(controllerInstance.props.clipperState.currentMode.get(), clipMode_1.ClipMode.Region, "State of current mode should be region after clicking on region mode button");
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                fullPageButton.click();
            });
            strictEqual(controllerInstance.props.clipperState.currentMode.get(), clipMode_1.ClipMode.FullPage, "State of current mode should be full page after clicking on full page mode button");
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                augmentationButton.click();
            });
            strictEqual(controllerInstance.props.clipperState.currentMode.get(), clipMode_1.ClipMode.Augmentation, "State of current mode should be augmentation after clicking on augmentation mode button");
        });
        test("The augmentation button should be labeled as 'Article' by default", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var modeButtonSelector = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var buttonElements = modeButtonSelector.getElementsByClassName(TestConstants.Classes.modeButton);
            var augmentationButton = buttonElements[2];
            var label = augmentationButton.getElementsByClassName(TestConstants.Classes.label)[0];
            strictEqual(label.textContent, _this.stringsJson["WebClipper.ClipType.Article.Button"]);
        });
        test("The augmentation button should be labeled according to the content model of the augmentation result", function () {
            var startingState = mockProps_1.MockProps.getMockClipperState();
            startingState.augmentationResult = {
                data: {
                    ContentInHtml: "",
                    ContentModel: augmentationHelper_1.AugmentationModel.Recipe,
                    ContentObjects: [],
                    PageMetadata: {
                        AutoPageTags: "Recipe",
                        AutoPageTagsCodes: "Recipe"
                    }
                },
                status: status_1.Status.Succeeded
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(modeButtonSelector_1.ModeButtonSelector, {clipperState:startingState}));
            var modeButtonSelector = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var buttonElements = modeButtonSelector.getElementsByClassName(TestConstants.Classes.modeButton);
            var augmentationButton = buttonElements[2];
            var label = augmentationButton.getElementsByClassName(TestConstants.Classes.label)[0];
            strictEqual(label.textContent, _this.stringsJson["WebClipper.ClipType.Recipe.Button"]);
        });
        test("The augmentation button should have its image set to the article icon by default", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var modeButtonSelector = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var buttonElements = modeButtonSelector.getElementsByClassName(TestConstants.Classes.modeButton);
            var augmentationButton = buttonElements[2];
            var icon = augmentationButton.getElementsByClassName(TestConstants.Classes.icon)[0];
            // endsWith is polyfilled
            ok(icon.src.toLowerCase().endsWith("article.png"));
        });
        test("The augmentation button should have its image set according to the content model of the augmentation result", function () {
            var startingState = mockProps_1.MockProps.getMockClipperState();
            startingState.augmentationResult = {
                data: {
                    ContentInHtml: "",
                    ContentModel: augmentationHelper_1.AugmentationModel.Product,
                    ContentObjects: [],
                    PageMetadata: {
                        AutoPageTags: "Product",
                        AutoPageTagsCodes: "Product"
                    }
                },
                status: status_1.Status.Succeeded
            };
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(modeButtonSelector_1.ModeButtonSelector, {clipperState:startingState}));
            var modeButtonSelector = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var buttonElements = modeButtonSelector.getElementsByClassName(TestConstants.Classes.modeButton);
            var augmentationButton = buttonElements[2];
            var icon = augmentationButton.getElementsByClassName(TestConstants.Classes.icon)[0];
            // endsWith is polyfilled
            ok(icon.src.toLowerCase().endsWith("product.png"));
        });
        test("In PDF Mode, only the PDF, Region, and Bookmark Mode Buttons should be rendered, and in that order", function () {
            var startingState = mockProps_1.MockProps.getMockClipperState();
            startingState.currentMode.set(clipMode_1.ClipMode.Pdf);
            startingState.pageInfo.contentType = OneNoteApi.ContentType.EnhancedUrl;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(modeButtonSelector_1.ModeButtonSelector, {clipperState:startingState}));
            var modeButtonSelector = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var buttonElements = modeButtonSelector.getElementsByClassName(TestConstants.Classes.modeButton);
            strictEqual(buttonElements.length, 3, "There should be three mode buttons");
            strictEqual(buttonElements[0].id, TestConstants.Ids.regionButton, "The first button should be the region button");
            strictEqual(buttonElements[1].id, TestConstants.Ids.bookmarkButton, "The second button should be the bookmark button");
            strictEqual(buttonElements[2].id, TestConstants.Ids.pdfButton, "The third button should be the pdf button");
        });
        test("The bookmark clipping button should not appear when a PDF was detected but was on a local file", function () {
            var startingState = mockProps_1.MockProps.getMockClipperState();
            startingState.currentMode.set(clipMode_1.ClipMode.Pdf);
            startingState.pageInfo.contentType = OneNoteApi.ContentType.EnhancedUrl;
            startingState.pageInfo.rawUrl = "file:///local.pdf";
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(modeButtonSelector_1.ModeButtonSelector, {clipperState:startingState}));
            var modeButtonSelector = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var buttonElements = modeButtonSelector.getElementsByClassName(TestConstants.Classes.modeButton);
            strictEqual(buttonElements.length, 2, "There should be two mode buttons");
            strictEqual(buttonElements[0].id, TestConstants.Ids.regionButton, "The first button should be the region button");
            strictEqual(buttonElements[1].id, TestConstants.Ids.pdfButton, "The second button should be the pdf button");
        });
    };
    return ModeButtonSelectorTests;
}(testModule_1.TestModule));
exports.ModeButtonSelectorTests = ModeButtonSelectorTests;
(new ModeButtonSelectorTests()).runTests();
