"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var modeButton_1 = require("../../../scripts/clipperUI/components/modeButton");
var mithrilUtils_1 = require("../../mithrilUtils");
var mockProps_1 = require("../../mockProps");
var testModule_1 = require("../../testModule");
var TestConstants;
(function (TestConstants) {
    var Classes;
    (function (Classes) {
        Classes.icon = "icon";
        Classes.label = "label";
        Classes.selected = "selected";
    })(Classes = TestConstants.Classes || (TestConstants.Classes = {}));
})(TestConstants || (TestConstants = {}));
var ModeButtonTests = (function (_super) {
    __extends(ModeButtonTests, _super);
    function ModeButtonTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.mockModeButtonProps = mockProps_1.MockProps.getMockModeButtonProps();
        return _this;
    }
    ModeButtonTests.prototype.module = function () {
        return "modeButton";
    };
    ModeButtonTests.prototype.beforeEach = function () {
        this.defaultComponent = m.component(modeButton_1.ModeButton, {imgSrc:this.mockModeButtonProps.imgSrc, label:this.mockModeButtonProps.label, myMode:this.mockModeButtonProps.myMode, tabIndex:this.mockModeButtonProps.tabIndex, selected:this.mockModeButtonProps.selected, "aria-setsize":this.mockModeButtonProps["aria-setsize"], "aria-posinset":this.mockModeButtonProps["aria-posinset"], onModeSelected:this.mockModeButtonProps.onModeSelected, tooltipText:this.mockModeButtonProps.tooltipText});
    };
    ModeButtonTests.prototype.tests = function () {
        var _this = this;
        test("A non-selected button should not have extra styling applied to it", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var modeButton = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            ok(!modeButton.classList.contains(TestConstants.Classes.selected), "The mode button should not have extra styling applied to it");
        });
        test("A selected button should have extra styling applied to it", function () {
            var startingState = mockProps_1.MockProps.getMockModeButtonProps();
            startingState.selected = true;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(modeButton_1.ModeButton, {imgSrc:startingState.imgSrc, label:startingState.label, myMode:startingState.myMode, tabIndex:startingState.tabIndex, "aria-setsize":startingState["aria-setsize"], "aria-posinset":startingState["aria-posinset"], selected:startingState.selected, onModeSelected:startingState.onModeSelected, tooltipText:startingState.tooltipText}));
            var modeButton = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            ok(modeButton.classList.contains(TestConstants.Classes.selected), "The mode button should have extra styling applied to it");
        });
        test("A button should be labeled with its label prop", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var modeButton = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var label = modeButton.getElementsByClassName(TestConstants.Classes.label)[0];
            strictEqual(label.textContent, _this.mockModeButtonProps.label, "The mode button should be labeled with: " + _this.mockModeButtonProps.label);
        });
        test("A button's aria-pos attribute should match its position in the set", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var modeButton = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            strictEqual(modeButton.getAttribute("aria-posinset"), _this.mockModeButtonProps["aria-posinset"].toString(), "The mode button's aria-pos attribute should be: " + _this.mockModeButtonProps["aria-posinset"]);
        });
        test("A button's aria-setsize attribute should be equal to the amount of buttons on the page", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var modeButton = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            strictEqual(modeButton.getAttribute("aria-setsize"), _this.mockModeButtonProps["aria-setsize"].toString(), "The mode button's aria-setsize attribute should be: " + _this.mockModeButtonProps["aria-setsize"]);
        });
        test("A button's image src should match its imgSrc prop", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var modeButton = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var label = modeButton.getElementsByClassName(TestConstants.Classes.icon)[0];
            // endsWith is polyfilled
            ok(label.src.endsWith(_this.mockModeButtonProps.imgSrc), "The mode button's icon src should be: " + _this.mockModeButtonProps.imgSrc);
        });
        test("A button's title attribute should match its tooltipText prop", function () {
            mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var modeButton = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            strictEqual(modeButton.title, _this.mockModeButtonProps.tooltipText);
        });
        test("A button with undefined tooltipText should have an undefined title attribute", function () {
            var startingState = mockProps_1.MockProps.getMockModeButtonProps();
            startingState.tooltipText = undefined;
            mithrilUtils_1.MithrilUtils.mountToFixture(m.component(modeButton_1.ModeButton, {imgSrc:startingState.imgSrc, label:startingState.label, myMode:startingState.myMode, tabIndex:startingState.tabIndex, selected:startingState.selected, onModeSelected:startingState.onModeSelected, tooltipText:startingState.tooltipText}));
            var modeButton = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            strictEqual(modeButton.title, "");
        });
    };
    return ModeButtonTests;
}(testModule_1.TestModule));
exports.ModeButtonTests = ModeButtonTests;
(new ModeButtonTests()).runTests();
