"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../scripts/constants");
var annotationInput_1 = require("../../../scripts/clipperUI/components/annotationInput");
var mithrilUtils_1 = require("../../mithrilUtils");
var mockProps_1 = require("../../mockProps");
var testModule_1 = require("../../testModule");
var AnnotationInputTests = (function (_super) {
    __extends(AnnotationInputTests, _super);
    function AnnotationInputTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AnnotationInputTests.prototype.module = function () {
        return "annotationInput";
    };
    AnnotationInputTests.prototype.beforeEach = function () {
        this.defaultComponent =
            m.component(annotationInput_1.AnnotationInput, {clipperState:mockProps_1.MockProps.getMockClipperState()});
    };
    AnnotationInputTests.prototype.tests = function () {
        var _this = this;
        test("The annotation container should expand to reveal the annotation field when the annotation placeholder is clicked", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var annotationContainer = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var annotationContainerChildren = annotationContainer.children;
            strictEqual(annotationContainerChildren.length, 1, "By default, the annotation container should only contain one child");
            var annotationPlaceholder = annotationContainerChildren[0];
            strictEqual(annotationPlaceholder.id, constants_1.Constants.Ids.annotationPlaceholder, "The annotation placeholder should be the only child of the container");
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                annotationPlaceholder.click();
            });
            strictEqual(annotationContainerChildren.length, 2, "The annotation container should contain two children");
            strictEqual(annotationContainerChildren[0].id, constants_1.Constants.Ids.annotationFieldMirror, "The first child of the annotation container should be the annotation field mirror");
            strictEqual(annotationContainerChildren[1].id, constants_1.Constants.Ids.annotationField, "The second child of the annotation container should be the annotation field");
        });
        test("The annotation container should remain open on blur if the annotation field is populated with non whitespace", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var annotationContainer = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var annotationContainerChildren = annotationContainer.children;
            var annotationPlaceholder = annotationContainerChildren[0];
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                annotationPlaceholder.click();
            });
            var annotationField = document.getElementById(constants_1.Constants.Ids.annotationField);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                annotationField.focus();
                annotationField.value = "Non whitespace annotation";
                annotationField.blur();
                window.dispatchEvent(new Event("mouseup"));
            });
            strictEqual(annotationContainerChildren.length, 2, "The annotation container should contain two children");
            strictEqual(annotationContainerChildren[0].id, constants_1.Constants.Ids.annotationFieldMirror, "The first child of the annotation container should be the annotation field mirror");
            strictEqual(annotationContainerChildren[1].id, constants_1.Constants.Ids.annotationField, "The second child of the annotation container should be the annotation field");
        });
        test("The annotation container should close on blur if the annotation field is empty", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var annotationContainer = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var annotationContainerChildren = annotationContainer.children;
            var annotationPlaceholder = annotationContainerChildren[0];
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                annotationPlaceholder.click();
            });
            var annotationField = document.getElementById(constants_1.Constants.Ids.annotationField);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                annotationField.focus();
                annotationField.value = "";
                annotationField.blur();
                window.dispatchEvent(new Event("mouseup"));
            });
            ok(!document.getElementById(constants_1.Constants.Ids.annotationField), "The annotation field should not be displayed on blur if it is empty");
            strictEqual(annotationContainerChildren.length, 1, "The annotation container should only contain one child");
            strictEqual(annotationContainerChildren[0].id, constants_1.Constants.Ids.annotationPlaceholder, "The only child of the annotation container should be the annotation placeholder");
        });
        test("The annotation container should close on blur if the annotation field is whitespace", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var annotationContainer = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var annotationContainerChildren = annotationContainer.children;
            var annotationPlaceholder = annotationContainerChildren[0];
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                annotationPlaceholder.click();
            });
            var annotationField = document.getElementById(constants_1.Constants.Ids.annotationField);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                annotationField.focus();
                annotationField.value = "   \n";
                annotationField.blur();
                window.dispatchEvent(new Event("mouseup"));
            });
            ok(!document.getElementById(constants_1.Constants.Ids.annotationField), "The annotation field should not be displayed on blur if it is filled with whitespace");
            strictEqual(annotationContainerChildren.length, 1, "The annotation container should only contain one child");
            strictEqual(annotationContainerChildren[0].id, constants_1.Constants.Ids.annotationPlaceholder, "The only child of the annotation container should be the annotation placeholder");
        });
        test("The opened state should be false by default", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            strictEqual(controllerInstance.state.opened, false, "The opened state should be false by default");
        });
        test("The opened state should be true after clicking on the annotation placeholder", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var annotationContainer = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var annotationContainerChildren = annotationContainer.children;
            var annotationPlaceholder = annotationContainerChildren[0];
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                annotationPlaceholder.click();
            });
            strictEqual(controllerInstance.state.opened, true, "The opened state should be false by default");
        });
        test("The opened state should be true on blur after populating the annotation field with non whitespace", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var annotationContainer = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var annotationContainerChildren = annotationContainer.children;
            var annotationPlaceholder = annotationContainerChildren[0];
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                annotationPlaceholder.click();
            });
            var annotationField = document.getElementById(constants_1.Constants.Ids.annotationField);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                annotationField.focus();
                annotationField.value = "Non whitespace annotation";
                annotationField.blur();
                window.dispatchEvent(new Event("mouseup"));
            });
            strictEqual(controllerInstance.state.opened, true, "The opened state should remain true on blur");
        });
        test("The opened state should be false on blur after populating the annotation field with whitespace", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var annotationContainer = mithrilUtils_1.MithrilUtils.getFixture().firstElementChild;
            var annotationContainerChildren = annotationContainer.children;
            var annotationPlaceholder = annotationContainerChildren[0];
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                annotationPlaceholder.click();
            });
            var annotationField = document.getElementById(constants_1.Constants.Ids.annotationField);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                annotationField.focus();
                annotationField.value = "";
                annotationField.blur();
                window.dispatchEvent(new Event("mouseup"));
            });
            strictEqual(controllerInstance.state.opened, false, "The opened state should become false on blur");
        });
    };
    return AnnotationInputTests;
}(testModule_1.TestModule));
exports.AnnotationInputTests = AnnotationInputTests;
(new AnnotationInputTests()).runTests();
