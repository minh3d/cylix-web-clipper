"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var clipMode_1 = require("../../scripts/clipperUI/clipMode");
var mainController_1 = require("../../scripts/clipperUI/mainController");
var status_1 = require("../../scripts/clipperUI/status");
var smartValue_1 = require("../../scripts/communicator/smartValue");
var constants_1 = require("../../scripts/constants");
var assert_1 = require("../assert");
var mithrilUtils_1 = require("../mithrilUtils");
var mockProps_1 = require("../mockProps");
var testModule_1 = require("../testModule");
var TestConstants;
(function (TestConstants) {
    var Ids;
    (function (Ids) {
        // Dynamically generated, hence not in constants module
        Ids.fullPageButton = "fullPageButton";
        Ids.regionButton = "regionButton";
        Ids.augmentationButton = "augmentationButton";
        Ids.sectionLocationContainer = "sectionLocationContainer";
    })(Ids = TestConstants.Ids || (TestConstants.Ids = {}));
})(TestConstants || (TestConstants = {}));
// Currently we set a 100ms delay before the initial render, which breaks our tests
mainController_1.MainControllerClass.prototype.getInitialState = function () {
    return {
        currentPanel: this.getPanelTypeToShow()
    };
};
var MainControllerTests = (function (_super) {
    __extends(MainControllerTests, _super);
    function MainControllerTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MainControllerTests.prototype.module = function () {
        return "mainController";
    };
    MainControllerTests.prototype.beforeEach = function () {
        this.mockMainControllerProps = mockProps_1.MockProps.getMockMainControllerProps();
        this.defaultComponent = m.component(mainController_1.MainController, {clipperState:this.mockMainControllerProps.clipperState, onSignInInvoked:this.mockMainControllerProps.onSignInInvoked, onSignOutInvoked:this.mockMainControllerProps.onSignOutInvoked, updateFrameHeight:this.mockMainControllerProps.updateFrameHeight, onStartClip:this.mockMainControllerProps.onStartClip});
    };
    MainControllerTests.prototype.tests = function () {
        var _this = this;
        test("On the sign in panel, the tab order is correct", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                controllerInstance.state.currentPanel = mainController_1.PanelType.SignInNeeded;
            });
            assert_1.Assert.tabOrderIsIncremental([constants_1.Constants.Ids.signInButtonMsa, constants_1.Constants.Ids.signInButtonOrgId, constants_1.Constants.Ids.closeButton]);
        });
        test("On the clip options panel, the tab order is correct", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                controllerInstance.state.currentPanel = mainController_1.PanelType.ClipOptions;
            });
            assert_1.Assert.tabOrderIsIncremental([constants_1.Constants.Ids.clipButton, constants_1.Constants.Ids.feedbackButton, constants_1.Constants.Ids.currentUserControl, constants_1.Constants.Ids.closeButton]);
        });
        test("On the pdf clip options panel, tab order should flow linearly between pdf options with the page range radio button appearing only when selected", function () {
            _this.mockMainControllerProps.clipperState.currentMode.set(clipMode_1.ClipMode.Pdf);
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                controllerInstance.state.currentPanel = mainController_1.PanelType.ClipOptions;
                document.getElementById(constants_1.Constants.Ids.radioAllPagesLabel).click();
            });
            assert_1.Assert.tabOrderIsIncremental([constants_1.Constants.Ids.clipButton,
                constants_1.Constants.Ids.radioAllPagesLabel, TestConstants.Ids.sectionLocationContainer, constants_1.Constants.Ids.feedbackButton,
                constants_1.Constants.Ids.currentUserControl, constants_1.Constants.Ids.closeButton]);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.radioPageRangeLabel).click();
            });
            assert_1.Assert.tabOrderIsIncremental([constants_1.Constants.Ids.clipButton,
                constants_1.Constants.Ids.radioPageRangeLabel, TestConstants.Ids.sectionLocationContainer, constants_1.Constants.Ids.feedbackButton,
                constants_1.Constants.Ids.currentUserControl, constants_1.Constants.Ids.closeButton]);
        });
        test("On the pdf clip options panel, after clicking 'More' the tab order is correct", function () {
            _this.mockMainControllerProps.clipperState.currentMode.set(clipMode_1.ClipMode.Pdf);
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                controllerInstance.state.currentPanel = mainController_1.PanelType.ClipOptions;
            });
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.moreClipOptions).click();
            });
            assert_1.Assert.tabOrderIsIncremental([constants_1.Constants.Ids.clipButton, constants_1.Constants.Ids.radioAllPagesLabel, constants_1.Constants.Ids.checkboxToDistributePages, constants_1.Constants.Ids.checkboxToAttachPdf,
                constants_1.Constants.Ids.feedbackButton, constants_1.Constants.Ids.currentUserControl, constants_1.Constants.Ids.closeButton]);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.radioPageRangeLabel).click();
            });
            assert_1.Assert.tabOrderIsIncremental([constants_1.Constants.Ids.clipButton, constants_1.Constants.Ids.radioPageRangeLabel, constants_1.Constants.Ids.checkboxToDistributePages, constants_1.Constants.Ids.checkboxToAttachPdf,
                constants_1.Constants.Ids.feedbackButton, constants_1.Constants.Ids.currentUserControl, constants_1.Constants.Ids.closeButton]);
        });
        test("On the region instructions panel, the tab order is correct", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                controllerInstance.state.currentPanel = mainController_1.PanelType.RegionInstructions;
            });
            assert_1.Assert.tabOrderIsIncremental([constants_1.Constants.Ids.regionClipCancelButton, constants_1.Constants.Ids.closeButton]);
        });
        test("On the clip success panel, the tab order is correct", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                controllerInstance.state.currentPanel = mainController_1.PanelType.ClippingSuccess;
            });
            assert_1.Assert.tabOrderIsIncremental([constants_1.Constants.Ids.launchOneNoteButton, constants_1.Constants.Ids.closeButton]);
        });
        test("On the clip failure panel, the tab order is correct", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                controllerInstance.props.clipperState.oneNoteApiResult.data = _this.getMockRequestError();
                controllerInstance.state.currentPanel = mainController_1.PanelType.ClippingFailure;
            });
            assert_1.Assert.tabOrderIsIncremental([constants_1.Constants.Ids.currentUserControl, constants_1.Constants.Ids.closeButton]);
            var dialogButtons = document.getElementsByClassName("dialogButton");
            assert_1.Assert.equalTabIndexes(dialogButtons);
        });
        test("On the clip failure panel, the right message is displayed for a particular API error code", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                controllerInstance.props.clipperState.oneNoteApiResult.data = _this.getMockRequestError();
                controllerInstance.state.currentPanel = mainController_1.PanelType.ClippingFailure;
            });
            var stringsJson = require("../../strings.json");
            strictEqual(document.getElementById(constants_1.Constants.Ids.dialogMessage).innerText, stringsJson["WebClipper.Error.PasswordProtected"], "The correct message is displayed for the given status code");
        });
        test("If the close button is clicked, the uiExpanded prop should be set to false, and getPanelTypeToShow() should return the None panel", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                controllerInstance.state.currentPanel = mainController_1.PanelType.ClipOptions;
            });
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                document.getElementById(constants_1.Constants.Ids.closeButton).click();
            });
            ok(!controllerInstance.state.uiExpanded, "If the close button is clicked, the uiExpanded prop should be false");
            strictEqual(controllerInstance.getPanelTypeToShow(), mainController_1.PanelType.None, "If the close button is clicked, getPanelTypeToShow() should return the None panel");
        });
        test("If the uiExpanded prop is set to false, getPanelTypeToShow() should return the None panel", function () {
            var props = mockProps_1.MockProps.getMockMainControllerProps();
            props.clipperState.uiExpanded = false;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(m.component(mainController_1.MainController, {clipperState:props.clipperState, onSignInInvoked:_this.mockMainControllerProps.onSignInInvoked, onSignOutInvoked:_this.mockMainControllerProps.onSignOutInvoked, updateFrameHeight:_this.mockMainControllerProps.updateFrameHeight, onStartClip:_this.mockMainControllerProps.onStartClip}));
            strictEqual(controllerInstance.getPanelTypeToShow(), mainController_1.PanelType.None, "If the clipper state is set to not show the UI, getPanelTypeToShow() should return the None panel");
        });
        test("If loc strings have not been fetched, the Loading panel should be displayed", function () {
            var props = mockProps_1.MockProps.getMockMainControllerProps();
            props.clipperState.fetchLocStringStatus = status_1.Status.InProgress;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(m.component(mainController_1.MainController, {clipperState:props.clipperState, onSignInInvoked:_this.mockMainControllerProps.onSignInInvoked, onSignOutInvoked:_this.mockMainControllerProps.onSignOutInvoked, updateFrameHeight:_this.mockMainControllerProps.updateFrameHeight, onStartClip:_this.mockMainControllerProps.onStartClip}));
            ok(document.getElementById(constants_1.Constants.Ids.clipperLoadingContainer), "The Loading panel should be shown in the UI");
            strictEqual(controllerInstance.getPanelTypeToShow(), mainController_1.PanelType.Loading, "The Loading panel should be returned by getPanelTypeToShow()");
        });
        test("If user info is being fetched, the Loading panel should be displayed", function () {
            var props = mockProps_1.MockProps.getMockMainControllerProps();
            props.clipperState.userResult.status = status_1.Status.InProgress;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(m.component(mainController_1.MainController, {clipperState:props.clipperState, onSignInInvoked:_this.mockMainControllerProps.onSignInInvoked, onSignOutInvoked:_this.mockMainControllerProps.onSignOutInvoked, updateFrameHeight:_this.mockMainControllerProps.updateFrameHeight, onStartClip:_this.mockMainControllerProps.onStartClip}));
            ok(document.getElementById(constants_1.Constants.Ids.clipperLoadingContainer), "The Loading panel should be shown in the UI");
            strictEqual(controllerInstance.getPanelTypeToShow(), mainController_1.PanelType.Loading, "The Loading panel should be returned by getPanelTypeToShow()");
        });
        test("If invoke options is being fetched, the Loading panel should be displayed", function () {
            var props = mockProps_1.MockProps.getMockMainControllerProps();
            props.clipperState.invokeOptions = undefined;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(m.component(mainController_1.MainController, {clipperState:props.clipperState, onSignInInvoked:_this.mockMainControllerProps.onSignInInvoked, onSignOutInvoked:_this.mockMainControllerProps.onSignOutInvoked, updateFrameHeight:_this.mockMainControllerProps.updateFrameHeight, onStartClip:_this.mockMainControllerProps.onStartClip}));
            ok(document.getElementById(constants_1.Constants.Ids.clipperLoadingContainer), "The Loading panel should be shown in the UI");
            strictEqual(controllerInstance.getPanelTypeToShow(), mainController_1.PanelType.Loading, "The Loading panel should be returned by getPanelTypeToShow()");
        });
        test("If loc strings and user info have been fetched, getPanelTypeToShow() should not return the Loading panel", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            notStrictEqual(controllerInstance.getPanelTypeToShow(), mainController_1.PanelType.Loading, "The Loading panel should be not shown when the loc strings and user info have been fetched");
        });
        test("If the user's info is not available, the SignInNeeded panel should be displayed", function () {
            var props = mockProps_1.MockProps.getMockMainControllerProps();
            props.clipperState.userResult = { status: status_1.Status.Failed };
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(m.component(mainController_1.MainController, {clipperState:props.clipperState, onSignInInvoked:_this.mockMainControllerProps.onSignInInvoked, onSignOutInvoked:_this.mockMainControllerProps.onSignOutInvoked, updateFrameHeight:_this.mockMainControllerProps.updateFrameHeight, onStartClip:_this.mockMainControllerProps.onStartClip}));
            ok(document.getElementById(constants_1.Constants.Ids.signInContainer), "The SignInNeeded panel should be shown in the UI");
            strictEqual(controllerInstance.getPanelTypeToShow(), mainController_1.PanelType.SignInNeeded, "The SignInNeeded panel should be returned by getPanelTypeToShow()");
        });
        test("If the region mode is selected and region selection is progress, the Loading panel should be displayed", function () {
            var props = mockProps_1.MockProps.getMockMainControllerProps();
            props.clipperState.currentMode = new smartValue_1.SmartValue(clipMode_1.ClipMode.Region);
            props.clipperState.regionResult.status = status_1.Status.InProgress;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(m.component(mainController_1.MainController, {clipperState:props.clipperState, onSignInInvoked:_this.mockMainControllerProps.onSignInInvoked, onSignOutInvoked:_this.mockMainControllerProps.onSignOutInvoked, updateFrameHeight:_this.mockMainControllerProps.updateFrameHeight, onStartClip:_this.mockMainControllerProps.onStartClip}));
            ok(document.getElementById(constants_1.Constants.Ids.clipperLoadingContainer), "The Loading panel should be shown in the UI");
            strictEqual(controllerInstance.getPanelTypeToShow(), mainController_1.PanelType.Loading, "The Loading panel should be returned by getPanelTypeToShow()");
        });
        test("If the region mode is selected and region selection has not started, the RegionInstructions panel should be displayed", function () {
            var props = mockProps_1.MockProps.getMockMainControllerProps();
            props.clipperState.currentMode = new smartValue_1.SmartValue(clipMode_1.ClipMode.Region);
            props.clipperState.regionResult.status = status_1.Status.NotStarted;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(m.component(mainController_1.MainController, {clipperState:props.clipperState, onSignInInvoked:_this.mockMainControllerProps.onSignInInvoked, onSignOutInvoked:_this.mockMainControllerProps.onSignOutInvoked, updateFrameHeight:_this.mockMainControllerProps.updateFrameHeight, onStartClip:_this.mockMainControllerProps.onStartClip}));
            ok(document.getElementById(constants_1.Constants.Ids.regionInstructionsContainer), "The RegionInstructions panel should be shown in the UI");
            strictEqual(controllerInstance.getPanelTypeToShow(), mainController_1.PanelType.RegionInstructions, "The RegionInstructions panel should be returned by getPanelTypeToShow()");
        });
        test("If currently clipping to OneNote, the ClippingToApi panel should be displayed", function () {
            var props = mockProps_1.MockProps.getMockMainControllerProps();
            props.clipperState.oneNoteApiResult.status = status_1.Status.InProgress;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(m.component(mainController_1.MainController, {clipperState:props.clipperState, onSignInInvoked:_this.mockMainControllerProps.onSignInInvoked, onSignOutInvoked:_this.mockMainControllerProps.onSignOutInvoked, updateFrameHeight:_this.mockMainControllerProps.updateFrameHeight, onStartClip:_this.mockMainControllerProps.onStartClip}));
            ok(document.getElementById(constants_1.Constants.Ids.clipperApiProgressContainer), "The ClippingToApi panel should be shown in the UI");
            strictEqual(controllerInstance.getPanelTypeToShow(), mainController_1.PanelType.ClippingToApi, "The ClippingToApi panel should be returned by getPanelTypeToShow()");
        });
        test("If clipping to OneNote failed, the dialog panel should be displayed", function () {
            var props = mockProps_1.MockProps.getMockMainControllerProps();
            props.clipperState.oneNoteApiResult.data = _this.getMockRequestError();
            props.clipperState.oneNoteApiResult.status = status_1.Status.Failed;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(m.component(mainController_1.MainController, {clipperState:props.clipperState, onSignInInvoked:_this.mockMainControllerProps.onSignInInvoked, onSignOutInvoked:_this.mockMainControllerProps.onSignOutInvoked, updateFrameHeight:_this.mockMainControllerProps.updateFrameHeight, onStartClip:_this.mockMainControllerProps.onStartClip}));
            ok(document.getElementById(constants_1.Constants.Ids.dialogMessageContainer), "The ClippingFailure panel should be shown in the UI in the form of the dialog panel");
            strictEqual(controllerInstance.getPanelTypeToShow(), mainController_1.PanelType.ClippingFailure, "The ClippingFailure panel should be returned by getPanelTypeToShow()");
        });
        test("If clipping to OneNote succeeded, the ClippingSuccess panel should be displayed", function () {
            var props = mockProps_1.MockProps.getMockMainControllerProps();
            props.clipperState.oneNoteApiResult.status = status_1.Status.Succeeded;
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(m.component(mainController_1.MainController, {clipperState:props.clipperState, onSignInInvoked:_this.mockMainControllerProps.onSignInInvoked, onSignOutInvoked:_this.mockMainControllerProps.onSignOutInvoked, updateFrameHeight:_this.mockMainControllerProps.updateFrameHeight, onStartClip:_this.mockMainControllerProps.onStartClip}));
            ok(document.getElementById(constants_1.Constants.Ids.clipperSuccessContainer), "The ClippingSuccess panel should be shown in the UI");
            strictEqual(controllerInstance.getPanelTypeToShow(), mainController_1.PanelType.ClippingSuccess, "The ClippingSuccess panel should be returned by getPanelTypeToShow()");
        });
        test("The footer should not be rendered when the Loading panel is shown", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                controllerInstance.state.currentPanel = mainController_1.PanelType.Loading;
            });
            ok(!document.getElementById(constants_1.Constants.Ids.clipperFooterContainer), "The footer container should not render when the clipper is loading");
        });
        test("The footer should be rendered when the SignIn panel is shown", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                controllerInstance.state.currentPanel = mainController_1.PanelType.SignInNeeded;
            });
            ok(document.getElementById(constants_1.Constants.Ids.clipperFooterContainer), "The footer container should not render when the clipper is showing the sign in panel");
        });
        test("The footer should be rendered when the ClipOptions panel is shown", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                controllerInstance.state.currentPanel = mainController_1.PanelType.ClipOptions;
            });
            ok(document.getElementById(constants_1.Constants.Ids.clipperFooterContainer), "The footer container should render when the clipper is showing the clip options panel");
        });
        test("The footer should not be rendered when the RegionInstructions panel is shown", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                controllerInstance.state.currentPanel = mainController_1.PanelType.RegionInstructions;
            });
            ok(!document.getElementById(constants_1.Constants.Ids.clipperFooterContainer), "The footer container should not render when the clipper is showing the region instructions panel");
        });
        test("The footer should not be rendered when the ClippingToApi panel is shown", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                controllerInstance.state.currentPanel = mainController_1.PanelType.ClippingToApi;
            });
            ok(!document.getElementById(constants_1.Constants.Ids.clipperFooterContainer), "The footer container should not render when the clipper is clipping to OneNote API");
        });
        test("The footer should be rendered when the ClippingFailure panel is shown", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                controllerInstance.props.clipperState.oneNoteApiResult.data = _this.getMockRequestError();
                controllerInstance.state.currentPanel = mainController_1.PanelType.ClippingFailure;
            });
            ok(document.getElementById(constants_1.Constants.Ids.clipperFooterContainer), "The footer container should render when the clipper is showing the clip failure panel");
        });
        test("The footer should be not be rendered when the ClippingSuccess panel is shown", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                controllerInstance.state.currentPanel = mainController_1.PanelType.ClippingSuccess;
            });
            ok(!document.getElementById(constants_1.Constants.Ids.clipperFooterContainer), "The footer container should not be rendered when the clipper is showing the clip success panel");
        });
        test("The close button should not be rendered when the ClippingToApi panel is shown", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            mithrilUtils_1.MithrilUtils.simulateAction(function () {
                controllerInstance.state.currentPanel = mainController_1.PanelType.ClippingToApi;
            });
            ok(!document.getElementById(constants_1.Constants.Ids.closeButton), "The close button should not render when the clipper is clipping to OneNote API");
        });
        test("The close button should be rendered when the panel shown is not ClippingToApi", function () {
            var controllerInstance = mithrilUtils_1.MithrilUtils.mountToFixture(_this.defaultComponent);
            var panels = [
                mainController_1.PanelType.Loading,
                mainController_1.PanelType.SignInNeeded,
                mainController_1.PanelType.ClipOptions,
                mainController_1.PanelType.RegionInstructions,
                mainController_1.PanelType.ClippingFailure,
                mainController_1.PanelType.ClippingSuccess
            ];
            var _loop_1 = function (i) {
                var panel = panels[i];
                mithrilUtils_1.MithrilUtils.simulateAction(function () {
                    controllerInstance.state.currentPanel = panel;
                });
                ok(document.getElementById(constants_1.Constants.Ids.closeButton), "The close button should render when the clipper is not clipping to OneNote API");
            };
            // We have to write it this (ugly) way as the sensible way is only supported by ES6, which we're not using
            for (var i = 0; i < panels.length; i++) {
                _loop_1(i);
            }
        });
    };
    MainControllerTests.prototype.getMockRequestError = function () {
        return {
            error: "Unexpected response status",
            statusCode: 403,
            response: '{"error":{"code":"10004","message":"Unable to create a page in this section because it is password protected.","@api.url":"http://aka.ms/onenote-errors#C10004"}}'
        };
    };
    return MainControllerTests;
}(testModule_1.TestModule));
exports.MainControllerTests = MainControllerTests;
(new MainControllerTests()).runTests();
