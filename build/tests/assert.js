"use strict";
///<reference path="../../node_modules/@types/qunit/index.d.ts"/>
/**
 * A collection of commonly used QUnit assertions
 */
var Assert;
(function (Assert) {
    function tabOrderIsIncremental(elementIds) {
        var elementsInExpectedTabOrder = elementIds.map(function (id) { return { name: id, elem: document.getElementById(id) }; });
        Assert.tabOrderIsIncrementalForElements(elementsInExpectedTabOrder);
    }
    Assert.tabOrderIsIncremental = tabOrderIsIncremental;
    function tabOrderIsIncrementalForElements(elements) {
        // Assert positive tabIndexes
        for (var i = 0; i < elements.length; i++) {
            var tabIndex = elements[i].elem.tabIndex;
            ok(tabIndex >= 0, "Element " + elements[i].name + " should have a positive tabIndex, but was: " + tabIndex);
        }
        if (elements.length < 2) {
            return;
        }
        // Assert tab index ordering
        for (var i = 1; i < elements.length; i++) {
            ok(elements[i].elem.tabIndex > elements[i - 1].elem.tabIndex, "Element " + elements[i].name + " whose tabIndex is" + elements[i].elem.tabIndex + " should have a greater tabIndex than element " + elements[i - 1].name + " whose tabIndex is " + elements[i - 1].elem.tabIndex);
        }
    }
    Assert.tabOrderIsIncrementalForElements = tabOrderIsIncrementalForElements;
    function checkAriaSetAttributes(elementIds) {
        var elementsInExpectedPosInSetOrder = elementIds.map(function (id) { return { name: id, elem: document.getElementById(id) }; });
        Assert.posInSetAndSetSizeAreCorrect(elementsInExpectedPosInSetOrder);
    }
    Assert.checkAriaSetAttributes = checkAriaSetAttributes;
    function posInSetAndSetSizeAreCorrect(elements) {
        var expectedSetSize = elements.length.toString();
        for (var i = 0; i < elements.length; i++) {
            var expectedPosition = (i + 1).toString();
            strictEqual(elements[i].elem.getAttribute("aria-posinset"), expectedPosition, "Element " + elements[i].name + " has the wrong aria-posinset value");
            strictEqual(elements[i].elem.getAttribute("aria-setsize"), expectedSetSize, "Element " + elements[i].name + " has the wrong aria-setsize value");
        }
    }
    Assert.posInSetAndSetSizeAreCorrect = posInSetAndSetSizeAreCorrect;
    function equalTabIndexes(elements) {
        // Buttons should have equal tab indexes
        var expectedTabIndex = undefined;
        for (var i = 0; i < elements.length; i++) {
            var element = elements[i];
            if (!expectedTabIndex) {
                expectedTabIndex = element.tabIndex;
            }
            else {
                strictEqual(element.tabIndex, expectedTabIndex, "Dialog button tabs should have the same tab indexes");
            }
            ok(element.tabIndex >= 0);
        }
    }
    Assert.equalTabIndexes = equalTabIndexes;
})(Assert = exports.Assert || (exports.Assert = {}));
