"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var experiments_1 = require("../scripts/experiments");
var testModule_1 = require("./testModule");
var ExperimentsTests = (function (_super) {
    __extends(ExperimentsTests, _super);
    function ExperimentsTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ExperimentsTests.prototype.module = function () {
        return "experiments";
    };
    ExperimentsTests.prototype.tests = function () {
        // Test empty flights, garbage flights, real flights with empty experiment name, garbage experiment name
        test("isFeatureEnabled should return False (OFF) for an undefined Feature", function () {
            var fakeFlights = ["muidflt1-test", "muidflt2-test"];
            ok(!experiments_1.Experiments.isFeatureEnabled(undefined, fakeFlights), "isFeatureEnabled incorrectly returned TRUE For blank setting with blank flights");
        });
        test("isFeatureEnabled should return False (OFF) for a valid Feature, but an undefined array of flights", function () {
            var fakeFlights;
            ok(!experiments_1.Experiments.isFeatureEnabled(experiments_1.Experiments.Feature.DummyExperiment, fakeFlights), "isFeatureEnabled returned True (ON) for Feature that should be OFF because of blank flights");
        });
        test("isFeatureEnabled should return False (OFF) for an empty array of flights", function () {
            var fakeFlights = [];
            ok(!experiments_1.Experiments.isFeatureEnabled(experiments_1.Experiments.Feature.DummyExperiment, fakeFlights), "isFeatureEnabled incorrectly returned True (ON) for an empty flights array");
        });
        test("isFeatureEnabled should return False (OFF) for a valid Feature whose corresponding numberline doesn't exist in the array of flights", function () {
            var fakeFlights = ["muidflt-test1", "muidflt-test2"];
            ok(!experiments_1.Experiments.isFeatureEnabled(experiments_1.Experiments.Feature.DummyExperiment, fakeFlights), "isFeatureEnabled returned True (ON) for Feature that should be OFF because it does not appear in the array of flights");
        });
        test("isFeatureEnabled should return True (ON) for a valid Feature and when the corresponding numberline exists in the array of flights", function () {
            var fakeFlights = ["muidflt60-clprin", "dummy-flight", "muidflt62-aug"];
            ok(experiments_1.Experiments.isFeatureEnabled(experiments_1.Experiments.Feature.DummyExperiment, fakeFlights), "isFeatureEnabled returned False (OFF) for Feature that should be ON");
        });
        test("isFeatureEnabled should return False (OFF) for a valid Feature in Experiments, and the corresponding numberline is a substring of an individual flight in the flights passed in", function () {
            var fakeFlights = ["muidflt60-clprin", "premuidflt62-wne", "substringdummy-flight"];
            ok(!experiments_1.Experiments.isFeatureEnabled(experiments_1.Experiments.Feature.DummyExperiment, fakeFlights), "isFeatureEnabled incorrectly returned true when the numberline itself was not in flights, but it was a substring of another numberline");
        });
    };
    return ExperimentsTests;
}(testModule_1.TestModule));
exports.ExperimentsTests = ExperimentsTests;
(new ExperimentsTests()).runTests();
