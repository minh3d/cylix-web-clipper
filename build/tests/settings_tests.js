"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var settings_1 = require("../scripts/settings");
var testModule_1 = require("./testModule");
var SettingsTests = (function (_super) {
    __extends(SettingsTests, _super);
    function SettingsTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SettingsTests.prototype.module = function () {
        return "settings";
    };
    SettingsTests.prototype.tests = function () {
        var _this = this;
        var settings = require("../settings.json");
        test("helper method checkValueAndDescriptionOnObject should assert when given an object that lacks a value or description", function () {
            var nothing = {
                "Dummy": "blah"
            };
            var noValue = {
                "Dummy": {
                    "Description": "blah"
                }
            };
            var noDescription = {
                "Dummy": {
                    "Value": "test"
                }
            };
            var valid = {
                "Dummy": {
                    "Description": "valid",
                    "Value": "test"
                }
            };
            ok(!_this.checkValueAndDescriptionOnObject(nothing));
            ok(!_this.checkValueAndDescriptionOnObject(noValue));
            ok(!_this.checkValueAndDescriptionOnObject(noDescription));
            ok(_this.checkValueAndDescriptionOnObject(valid));
        });
        test("getSetting should return undefined when passed the empty String as a key, because no setting can be named the empty string", function () {
            var value = settings_1.Settings.getSetting("");
            strictEqual(value, undefined, "Blank key for Settings.GetSetting returned blank key back");
        });
        test("getSetting should return the key that was passed in when it cannot find the setting in settings.json", function () {
            var key = "jfkasdfjaksldjfa";
            var value = settings_1.Settings.getSetting(key);
            strictEqual(value, undefined, "Non-existent key returned the key instead of an object back");
        });
        test("getSetting should correctly fetch the Value of a setting when given a valid Key", function () {
            var value = settings_1.Settings.getSetting("DummyObjectForTestingPurposes");
            strictEqual(value, "Testing.", "DummyObject was not retrieved correctly");
        });
        test("settings.json object should be non-empty when required from any project", function () {
            notStrictEqual(settings, {}, "Settings object is empty and it shouldn't be.");
        });
    };
    SettingsTests.prototype.checkValueAndDescriptionOnObject = function (object) {
        for (var key in object) {
            if (object.hasOwnProperty(key)) {
                var obj = object[key];
                if (!obj.Value || !obj.Description) {
                    return false;
                }
            }
        }
        return true;
    };
    ;
    return SettingsTests;
}(testModule_1.TestModule));
exports.SettingsTests = SettingsTests;
(new SettingsTests()).runTests();
