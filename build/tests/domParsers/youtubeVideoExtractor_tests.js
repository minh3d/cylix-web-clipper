"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var youtubeVideoExtractor_1 = require("../../scripts/domParsers/youtubeVideoExtractor");
var testModule_1 = require("../testModule");
var YoutubeVideoExtractorTests = (function (_super) {
    __extends(YoutubeVideoExtractorTests, _super);
    function YoutubeVideoExtractorTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.unsupportedOtherUrls = [
            "https://www.hulu.com/",
            "https://www.google.com/",
            undefined,
            ""
        ];
        _this.supportedVimeoUrls = [
            "https://vimeo.com/45196609",
            "https://vimeo.com/45196609?autoplay=1",
            "https://vimeo.com/45196609#t=0",
            "https://vimeo.com/channels/staffpicks/45196609",
            "https://vimeo.com/album/45196609/",
            "https://vimeo.com/album/45196609/page:1",
            "https://vimeo.com/album/45196609/page:3/sort:preset/format:thumbnail",
            "https://vimeo.com/album/45196609/sort:preset/format:thumbnail/page:2",
            "https://vimeo.com/album/45196609/video/45196609",
            "https://vimeo.com/ondemand/45196609"
        ];
        _this.unsupportedYouTubeUrls = [
            "https://www.youtube.com/",
            "https://www.youtube.com/channel/UC38IQsAvIsxxjztdMZQtwHA"
        ];
        _this.supportedYouTubeUrls = [
            "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
            "https://www.youtube.com/watch?v=dQw4w9WgXcQ&feature=youtu.be&t=30s",
            "https://www.youtube.com/watch?v=dQw4w9WgXcQ#foo",
            "https://www.youtube.com/watch?feature=youtu.be&t=30s&v=dQw4w9WgXcQ",
            "https://m.youtube.com/watch?v=dQw4w9WgXcQ&feature=youtu.be",
            "https://www.youtube.com/embed/dQw4w9WgXcQ",
            "https://www.youtube.com/embed/dQw4w9WgXcQ/",
            "https://www.youtube.com/embed/dQw4w9WgXcQ?start=900"
        ];
        _this.youtubeExtractor = new youtubeVideoExtractor_1.YoutubeVideoExtractor();
        return _this;
    }
    YoutubeVideoExtractorTests.prototype.module = function () {
        return "youtubeVideoExtractor";
    };
    YoutubeVideoExtractorTests.prototype.tests = function () {
        var _this = this;
        test("createEmbeddedVimeosFromHtml should return a list of iframes with the src set to the embed url, and the data-original-src set to the watch url", function () {
            var ids = ["1", "2", "3"];
            var outerDiv = _this.createDivWithEmbeddedVideos(ids);
            var embedVideos = _this.youtubeExtractor.createEmbeddedVideosFromHtml(outerDiv.outerHTML);
            strictEqual(embedVideos.length, ids.length, "There should be one iframe for each unique video");
            for (var i = 0; i < embedVideos.length; i++) {
                strictEqual(embedVideos[i].src, "https://www.youtube.com/embed/" + ids[i], "The src should be set to the embed url");
                strictEqual(embedVideos[i].attributes.getNamedItem("data-original-src").value, "https://www.youtube.com/watch?v=" + ids[i], "The data original src attribute should be set to the watch url");
            }
        });
        test("createEmbeddedVimeosFromHtml should return a non-unique list of iframes given some videos are duplicates in the html", function () {
            var ids = ["1", "1", "2"];
            var outerDiv = _this.createDivWithEmbeddedVideos(ids);
            var embedVideos = _this.youtubeExtractor.createEmbeddedVideosFromHtml(outerDiv.outerHTML);
            strictEqual(embedVideos.length, ids.length, "There should be one iframe for each unique video");
            for (var i = 0; i < embedVideos.length; i++) {
                strictEqual(embedVideos[i].src, "https://www.youtube.com/embed/" + ids[i], "The src should be set to the embed url");
                strictEqual(embedVideos[i].attributes.getNamedItem("data-original-src").value, "https://www.youtube.com/watch?v=" + ids[i], "The data original src attribute should be set to the watch url");
            }
        });
        test("createEmbeddedVimeosFromHtml should return the empty list if there are no embed videos in the html", function () {
            var embedVideos = _this.youtubeExtractor.createEmbeddedVideosFromHtml("<div></div>");
            strictEqual(embedVideos.length, 0, "There should be 0 iframes in the returned");
            embedVideos = _this.youtubeExtractor.createEmbeddedVideosFromHtml("");
            strictEqual(embedVideos.length, 0, "There should be 0 iframes in the returned");
        });
        test("createEmbeddedVimeosFromHtml should return the empty list if the html is undefined", function () {
            var embedVideos = _this.youtubeExtractor.createEmbeddedVideosFromHtml(undefined);
            strictEqual(embedVideos.length, 0, "There should be 0 iframes in the returned");
        });
        test("createEmbedVideoFromUrl should return an iframe with the src set to the embed url, and the data-original-src set to the watch url", function () {
            // We use this same id in all each of the candidate urls for the test
            var id = "dQw4w9WgXcQ";
            for (var _i = 0, _a = _this.supportedYouTubeUrls; _i < _a.length; _i++) {
                var supportedYouTubeUrl = _a[_i];
                var embedVideo = _this.youtubeExtractor.createEmbeddedVideoFromUrl(supportedYouTubeUrl);
                strictEqual(embedVideo.src, "https://www.youtube.com/embed/" + id, "The src should be set to the embed url");
                strictEqual(embedVideo.attributes.getNamedItem("data-original-src").value, "https://www.youtube.com/watch?v=" + id, "The data original src attribute should be set to the watch url");
            }
        });
        test("createEmbeddedVideoFromUrl should return undefined when provided unsupported parameters for the YouTube domain", function () {
            for (var _i = 0, _a = _this.unsupportedOtherUrls; _i < _a.length; _i++) {
                var otherUrl = _a[_i];
                var embedVideo = _this.youtubeExtractor.createEmbeddedVideoFromUrl(otherUrl);
                strictEqual(embedVideo, undefined, otherUrl + " is unsupported by YouTube domain");
            }
            for (var _b = 0, _c = _this.supportedVimeoUrls; _b < _c.length; _b++) {
                var vimeoUrl = _c[_b];
                var embedVideo = _this.youtubeExtractor.createEmbeddedVideoFromUrl(vimeoUrl);
                strictEqual(embedVideo, undefined, vimeoUrl + " is unsupported by YouTube domain");
            }
            for (var _d = 0, _e = _this.unsupportedYouTubeUrls; _d < _e.length; _d++) {
                var youTubeUrl = _e[_d];
                var embedVideo = _this.youtubeExtractor.createEmbeddedVideoFromUrl(youTubeUrl);
                strictEqual(embedVideo, undefined, youTubeUrl + " is in incorrect format for the pageUrl");
            }
        });
        test("createEmbeddedVideoFromUrl should return undefined if the input value is empty string or undefined", function () {
            strictEqual(_this.youtubeExtractor.createEmbeddedVideoFromUrl(""), undefined);
            strictEqual(_this.youtubeExtractor.createEmbeddedVideoFromUrl(undefined), undefined);
        });
        test("createEmbeddedVideoFromId should create an iframe with the src set to the embed url, and the data-original-src set to the watch url", function () {
            var id = "12345";
            var embedVideo = _this.youtubeExtractor.createEmbeddedVideoFromId(id);
            strictEqual(embedVideo.src, "https://www.youtube.com/embed/" + id, "The src should be set to the embed url");
            strictEqual(embedVideo.attributes.getNamedItem("data-original-src").value, "https://www.youtube.com/watch?v=" + id, "The data original src attribute should be set to the watch url");
        });
        test("createEmbeddedVideoFromId should return undefined if the input value is empty string or undefined", function () {
            strictEqual(_this.youtubeExtractor.createEmbeddedVideoFromId(""), undefined);
            strictEqual(_this.youtubeExtractor.createEmbeddedVideoFromId(undefined), undefined);
        });
    };
    YoutubeVideoExtractorTests.prototype.createDivWithEmbeddedVideos = function (ids) {
        var outerDiv = document.createElement("div");
        for (var i = 0; i < ids.length; i++) {
            var video = document.createElement("iframe");
            video.src = "https://www.youtube.com/embed/" + ids[i];
            outerDiv.appendChild(video);
        }
        return outerDiv;
    };
    return YoutubeVideoExtractorTests;
}(testModule_1.TestModule));
exports.YoutubeVideoExtractorTests = YoutubeVideoExtractorTests;
(new YoutubeVideoExtractorTests()).runTests();
