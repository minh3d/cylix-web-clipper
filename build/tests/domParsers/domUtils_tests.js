"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../scripts/constants");
var augmentationHelper_1 = require("../../scripts/contentCapture/augmentationHelper");
var domUtils_1 = require("../../scripts/domParsers/domUtils");
var testModule_1 = require("../testModule");
var mithrilUtils_1 = require("../mithrilUtils");
var regionSelector_tests_dataUrls_1 = require("../clipperUI/regionSelector_tests_dataUrls");
var DomUtilsTests = (function (_super) {
    __extends(DomUtilsTests, _super);
    function DomUtilsTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DomUtilsTests.prototype.module = function () {
        return "domUtils";
    };
    DomUtilsTests.prototype.beforeEach = function () {
        // Since some of the domUtils functions expect a document object parameter, we will add
        // all our test elements to the fixture before passing in the document object.
        this.fixture = mithrilUtils_1.MithrilUtils.getFixture();
    };
    DomUtilsTests.prototype.tests = function () {
        var _this = this;
        test("removeClipperElements should remove the root script iframe", function () {
            _this.fixture.appendChild(_this.createRootScriptIFrame());
            ok(document.getElementById(constants_1.Constants.Ids.clipperRootScript), "Sanity check: the root script iframe should be present before the removeClipperElements call");
            domUtils_1.DomUtils.removeClipperElements(document);
            ok(!document.getElementById(constants_1.Constants.Ids.clipperRootScript), "The root script iframe should be removed");
        });
        test("removeClipperElements should remove the UI iframe", function () {
            _this.fixture.appendChild(_this.createUiIFrame());
            ok(document.getElementById(constants_1.Constants.Ids.clipperUiFrame), "Sanity check: the UI iframe should be present before the removeClipperElements call");
            domUtils_1.DomUtils.removeClipperElements(document);
            ok(!document.getElementById(constants_1.Constants.Ids.clipperUiFrame), "The UI iframe should be removed");
        });
        test("removeClipperElements should remove the Ext iframe", function () {
            _this.fixture.appendChild(_this.createExtIFrame());
            ok(document.getElementById(constants_1.Constants.Ids.clipperExtFrame), "Sanity check: the Ext iframe should be present before the removeClipperElements call");
            domUtils_1.DomUtils.removeClipperElements(document);
            ok(!document.getElementById(constants_1.Constants.Ids.clipperExtFrame), "The Ext iframe should be removed");
        });
        test("removeClipperElements should remove all Web Clipper iframes", function () {
            _this.fixture.appendChild(_this.createRootScriptIFrame());
            _this.fixture.appendChild(_this.createUiIFrame());
            _this.fixture.appendChild(_this.createExtIFrame());
            domUtils_1.DomUtils.removeClipperElements(document);
            ok(!document.getElementById(constants_1.Constants.Ids.clipperRootScript), "The root script iframe should be removed");
            ok(!document.getElementById(constants_1.Constants.Ids.clipperUiFrame), "The UI iframe should be removed");
            ok(!document.getElementById(constants_1.Constants.Ids.clipperExtFrame), "The Ext iframe should be removed");
        });
        test("removeClipperElements should remove all Web Clipper iframes, but not an arbitrary iframe", function () {
            _this.fixture.appendChild(_this.createRootScriptIFrame());
            _this.fixture.appendChild(_this.createUiIFrame());
            _this.fixture.appendChild(_this.createExtIFrame());
            var arbitraryIframe = document.createElement("iframe");
            var arbitraryIframeId = "whatever";
            arbitraryIframe.id = arbitraryIframeId;
            arbitraryIframe.src = "https://www.mywebsite.doesnotexist";
            _this.fixture.appendChild(arbitraryIframe);
            ok(document.getElementById(arbitraryIframeId), "Sanity check: the non Web Clipper iframe should be present");
            domUtils_1.DomUtils.removeClipperElements(document);
            ok(!document.getElementById(constants_1.Constants.Ids.clipperRootScript), "The root script iframe should be removed");
            ok(!document.getElementById(constants_1.Constants.Ids.clipperUiFrame), "The UI iframe should be removed");
            ok(!document.getElementById(constants_1.Constants.Ids.clipperExtFrame), "The Ext iframe should be removed");
            ok(document.getElementById(arbitraryIframeId), "The non Web Clipper iframe should still be present");
        });
        test("removeUnsupportedHrefs should remove all URLs which refer to local extensions, but not any others", function () {
            var linksToCreate = [
                { id: "chromeExtensionLink", url: "chrome-extension://abc123abc123abc123/test.css", shouldRemain: false },
                { id: "edgeExtensionLink", url: "edge-extension://abc123abc123abc123/test.css", shouldRemain: false },
                { id: "fileExtensionLink", url: "file://c:/foo/bar/test.css", shouldRemain: false },
                { id: "firefoxExtensionLink", url: "firefox-extension://abc123abc123abc123/test.css", shouldRemain: false },
                { id: "safariExtensionLink", url: "safari-extension://abc123abc123abc123/test.css", shouldRemain: false },
                { id: "normalHttplLink", url: "https://foo/bar/test.css", shouldRemain: true },
            ];
            for (var _i = 0, linksToCreate_1 = linksToCreate; _i < linksToCreate_1.length; _i++) {
                var link = linksToCreate_1[_i];
                _this.createLinkElementWithHref(link.id, link.url);
            }
            domUtils_1.DomUtils.removeUnsupportedHrefs(document);
            for (var _a = 0, linksToCreate_2 = linksToCreate; _a < linksToCreate_2.length; _a++) {
                var link = linksToCreate_2[_a];
                strictEqual(!!document.getElementById(link.id), link.shouldRemain, "Url not removed when it was expected to: " + link.url);
            }
        });
        test("removeUnwantedElements should remove elements with tags that we don't support in full page mode", function () {
            var tagsNotSupportedForFullPage = ["script", "noscript"];
            for (var i = 0; i < tagsNotSupportedForFullPage.length; i++) {
                var element = document.createElement(tagsNotSupportedForFullPage[i]);
                element.id = tagsNotSupportedForFullPage[i];
                _this.fixture.appendChild(element);
            }
            domUtils_1.DomUtils.removeUnwantedElements(document);
            for (var i = 0; i < tagsNotSupportedForFullPage.length; i++) {
                ok(!document.getElementById(tagsNotSupportedForFullPage[i]), "The element should no longer be present in the document");
            }
        });
        test("removeUnwantedElements should not remove elements with tags that we support in full page mode", function () {
            // Example subset
            var tagsSupportedForFullPage = ["applet", "img", "div", "style", "svg", "video"];
            for (var i = 0; i < tagsSupportedForFullPage.length; i++) {
                var element = document.createElement(tagsSupportedForFullPage[i]);
                element.id = tagsSupportedForFullPage[i];
                _this.fixture.appendChild(element);
            }
            domUtils_1.DomUtils.removeUnwantedElements(document);
            for (var i = 0; i < tagsSupportedForFullPage.length; i++) {
                ok(document.getElementById(tagsSupportedForFullPage[i]), "The element should still be present in the document");
            }
        });
        test("removeUnwantedElements should not remove elements with tags that we support in full page mode when the " +
            "document has both supported and unsupported elements", function () {
            var tagsNotSupportedForFullPage = ["script", "noscript"];
            for (var i = 0; i < tagsNotSupportedForFullPage.length; i++) {
                var element = document.createElement(tagsNotSupportedForFullPage[i]);
                element.id = tagsNotSupportedForFullPage[i];
                _this.fixture.appendChild(element);
            }
            // Example subset
            var tagsSupportedForFullPage = ["applet", "img", "div", "style", "svg", "video"];
            for (var i = 0; i < tagsSupportedForFullPage.length; i++) {
                var element = document.createElement(tagsSupportedForFullPage[i]);
                element.id = tagsSupportedForFullPage[i];
                _this.fixture.appendChild(element);
            }
            domUtils_1.DomUtils.removeUnwantedElements(document);
            for (var i = 0; i < tagsNotSupportedForFullPage.length; i++) {
                ok(!document.getElementById(tagsNotSupportedForFullPage[i]), "The element should no longer be present in the document");
            }
            for (var i = 0; i < tagsSupportedForFullPage.length; i++) {
                ok(document.getElementById(tagsSupportedForFullPage[i]), "The element should still be present in the document");
            }
        });
        test("removeUnwantedAttributes should remove the srcset on images without modifying other attributes", function () {
            var image = document.createElement("img");
            var id = "IMG";
            image.id = id;
            var src = "https://cdn.mywebsite.doesnotexist";
            image.setAttribute("src", src);
            image.setAttribute("srcset", "asdfasdf");
            _this.fixture.appendChild(image);
            domUtils_1.DomUtils.removeUnwantedAttributes(document);
            var imageFromDocument = document.getElementById(id);
            ok(imageFromDocument, "The image should still exist");
            ok(!(imageFromDocument.getAttribute("srcset") || imageFromDocument.srcset), "The srcset attribute should no longer be present");
            strictEqual((imageFromDocument.getAttribute("src") || imageFromDocument.src), src, "The src attribute should be unmodified");
        });
        test("removeUnwantedAttributes should remove the srcset on images without modifying other attributes " +
            "in the case of multiple images", function () {
            var numberOfImages = 3;
            var src = "https://cdn.mywebsite.doesnotexist";
            for (var i = 0; i < numberOfImages; i++) {
                var image = document.createElement("img");
                var id = "IMG" + i;
                image.id = id;
                image.setAttribute("src", src);
                image.setAttribute("srcset", "asdfasdf" + i);
                _this.fixture.appendChild(image);
            }
            domUtils_1.DomUtils.removeUnwantedAttributes(document);
            for (var i = 0; i < numberOfImages; i++) {
                var imageFromDocument = document.getElementById("IMG" + i);
                ok(imageFromDocument, "The image should still exist");
                ok(!(imageFromDocument.getAttribute("srcset") || imageFromDocument.srcset), "The srcset attribute should no longer be present");
                strictEqual((imageFromDocument.getAttribute("src") || imageFromDocument.src), src, "The src attribute should be unmodified");
            }
        });
        var vimeoPageContentWithNoClipId = "";
        var vimeoPageContentWithOneClipId = "<div id='clip_45196609'> </div>";
        var vimeoPageContentWithMultipleClipIds = "<div id='clip_45196609'></div> <DIV ID='clip_45196610'></DIV> <div id='clip_45196611'> </div>";
        test("addEmbeddedVideosWhereSupported should resolve with a video url on supported YouTube subdomains even if MainArticleContainer is undefined", function (assert) {
            var done = assert.async();
            var previewElement = augmentationHelper_1.AugmentationHelper.getArticlePreviewElement(document);
            var pageUrl = "https://www.youtube.com/watch?v=dQw4w9WgXcQ";
            var embedUrl = "https://www.youtube.com/embed/dQw4w9WgXcQ";
            domUtils_1.DomUtils.addEmbeddedVideosWhereSupported(previewElement, vimeoPageContentWithOneClipId, pageUrl).then(function (videoSrcUrls) {
                strictEqual(videoSrcUrls[0].dataOriginalSrcAttribute, pageUrl);
                strictEqual(videoSrcUrls[0].srcAttribute, embedUrl);
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("addEmbeddedVideosWhereSupported should resolve with a video url on supported YouTube subdomains", function (assert) {
            var done = assert.async();
            _this.fixture.appendChild(_this.createMainArticleContainer());
            var previewElement = _this.createMainArticleContainer();
            var pageUrl = "https://www.youtube.com/watch?v=dQw4w9WgXcQ";
            var embedUrl = "https://www.youtube.com/embed/dQw4w9WgXcQ";
            domUtils_1.DomUtils.addEmbeddedVideosWhereSupported(previewElement, vimeoPageContentWithOneClipId, pageUrl).then(function (videoSrcUrls) {
                strictEqual(videoSrcUrls[0].dataOriginalSrcAttribute, pageUrl);
                strictEqual(videoSrcUrls[0].srcAttribute, embedUrl);
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("addEmbeddedVideosWhereSupported should resolve with a video url on supported Vimeo subdomains", function (assert) {
            var done = assert.async();
            _this.fixture.appendChild(_this.createMainArticleContainer());
            var previewElement = augmentationHelper_1.AugmentationHelper.getArticlePreviewElement(document);
            domUtils_1.DomUtils.addEmbeddedVideosWhereSupported(previewElement, vimeoPageContentWithOneClipId, "https://vimeo.com/45196609").then(function (videoSrcUrls) {
                var embedUrl = "https://player.vimeo.com/video/45196609";
                strictEqual(videoSrcUrls[0].dataOriginalSrcAttribute, embedUrl);
                strictEqual(videoSrcUrls[0].srcAttribute, embedUrl);
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("addEmbeddedVideosWhereSupported should resolve with multiple video urls on supported Vimeo subdomains with multiple videos", function (assert) {
            var done = assert.async();
            _this.fixture.appendChild(_this.createMainArticleContainer());
            var previewElement = augmentationHelper_1.AugmentationHelper.getArticlePreviewElement(document);
            domUtils_1.DomUtils.addEmbeddedVideosWhereSupported(previewElement, vimeoPageContentWithMultipleClipIds, "https://vimeo.com/album/3637653/").then(function (videoSrcUrls) {
                var expectedVideoIds = ["45196609", "45196610", "45196611"];
                for (var i = 0; i < expectedVideoIds.length; i++) {
                    if (!videoSrcUrls[i]) {
                        ok(false, "The video id " + expectedVideoIds[i] + " is missing in the return videoSrcUrls");
                    }
                    else {
                        var expectedUrl = "https://player.vimeo.com/video/" + expectedVideoIds[i];
                        strictEqual(videoSrcUrls[i].dataOriginalSrcAttribute, expectedUrl, "expected dataOriginalSrcAttribute: " + expectedUrl);
                        strictEqual(videoSrcUrls[i].srcAttribute, expectedUrl, "expected srcAttribute: " + expectedUrl);
                    }
                }
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("addEmbeddedVideosWhereSupported should resolve undefined on unsupported domains", function (assert) {
            var done = assert.async();
            _this.fixture.appendChild(_this.createMainArticleContainer());
            var previewElement = augmentationHelper_1.AugmentationHelper.getArticlePreviewElement(document);
            domUtils_1.DomUtils.addEmbeddedVideosWhereSupported(previewElement, vimeoPageContentWithOneClipId, "https://www.google.com/").then(function (videoSrcUrls) {
                strictEqual(videoSrcUrls, undefined);
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                domUtils_1.DomUtils.addEmbeddedVideosWhereSupported(previewElement, vimeoPageContentWithOneClipId, "https://www.youtube.com/channel/UC38IQsAvIsxxjztdMZQtwHA").then(function (videoSrcUrls) {
                    strictEqual(videoSrcUrls, undefined);
                }, function (error) {
                    ok(false, "reject should not be called");
                }).then(function () {
                    domUtils_1.DomUtils.addEmbeddedVideosWhereSupported(previewElement, vimeoPageContentWithOneClipId, "https://www.vimeo.com/").then(function (videoSrcUrls) {
                        strictEqual(videoSrcUrls, undefined);
                    }, function (error) {
                        ok(false, "reject should not be called");
                    }).then(function () {
                        done();
                    });
                });
            });
        });
        test("imageIsBlank should return true if the image is purely white", function (assert) {
            var done = assert.async();
            var img = new Image();
            img.onload = function () {
                ok(domUtils_1.DomUtils.imageIsBlank(img), "A white pixel should be detected as blank");
                done();
            };
            img.src = regionSelector_tests_dataUrls_1.DataUrls.whitePixelUrl;
        });
        test("imageIsBlank should return false if the image is not purely white or fully transparent", function (assert) {
            var done = assert.async();
            var img = new Image();
            img.onload = function () {
                ok(!domUtils_1.DomUtils.imageIsBlank(img), "A black cube should not be detected as blank");
                done();
            };
            img.src = regionSelector_tests_dataUrls_1.DataUrls.twoByTwoUpperCornerBlackOnTransparentUrl;
        });
        test("imageIsBlank should return false if the image is not purely white or fully transparent for a huge image", function (assert) {
            var done = assert.async();
            var img = new Image();
            img.onload = function () {
                ok(!domUtils_1.DomUtils.imageIsBlank(img), "A huge non-blank should not be detected as blank");
                done();
            };
            img.src = regionSelector_tests_dataUrls_1.DataUrls.bigImgUrl;
        });
        test("imageIsBlank should return true if the image is purely white for a huge image", function (assert) {
            var done = assert.async();
            var img = new Image();
            img.onload = function () {
                ok(domUtils_1.DomUtils.imageIsBlank(img), "A huge white image should be detected as blank");
                done();
            };
            img.src = regionSelector_tests_dataUrls_1.DataUrls.bigWhiteUrl;
        });
        test("imageIsBlank should return true if the image is a mix of white and transparent for a huge 1200x1200 image", function (assert) {
            var done = assert.async();
            var img = new Image();
            img.onload = function () {
                ok(domUtils_1.DomUtils.imageIsBlank(img), "A huge white and transparent image should be detected as blank");
                done();
            };
            img.src = regionSelector_tests_dataUrls_1.DataUrls.bigBlankAndWhiteUrl;
        });
        test("toAbsoluteUrl should convert a relative url to an absolute one", function () {
            var url = "/1";
            var base = "http://www.base.url";
            strictEqual(domUtils_1.DomUtils.toAbsoluteUrl(url, base), "http://www.base.url/1");
            base = "https://www.base.url";
            strictEqual(domUtils_1.DomUtils.toAbsoluteUrl(url, base), "https://www.base.url/1");
            base = "https://www.base.url/";
            strictEqual(domUtils_1.DomUtils.toAbsoluteUrl(url, base), "https://www.base.url/1");
        });
        test("toAbsoluteUrl should convert a relative url to an absolute one for deeper relative paths", function () {
            var url = "/1/2";
            var base = "http://www.base.url";
            strictEqual(domUtils_1.DomUtils.toAbsoluteUrl(url, base), "http://www.base.url/1/2");
        });
        test("toAbsoluteUrl should convert a relative url to an absolute one for paths with an extension", function () {
            var url = "/1/index.html";
            var base = "http://www.base.url";
            strictEqual(domUtils_1.DomUtils.toAbsoluteUrl(url, base), "http://www.base.url/1/index.html");
        });
        test("toAbsoluteUrl should convert a relative url to an absolute one for paths that go backwards", function () {
            var url = "../index.html";
            var base = "http://www.base.url/1/2/";
            strictEqual(domUtils_1.DomUtils.toAbsoluteUrl(url, base), "http://www.base.url/1/index.html");
        });
        test("toAbsoluteUrl should return the absolute url if one is passed in", function () {
            var url = "http://www.base.url/1/2/";
            var base = "http://www.xyz.url/1/2/";
            strictEqual(domUtils_1.DomUtils.toAbsoluteUrl(url, base), "http://www.base.url/1/2/");
            url = "http://www.base.url/1/2";
            strictEqual(domUtils_1.DomUtils.toAbsoluteUrl(url, base), "http://www.base.url/1/2");
            url = "http://www.base.url/1/2.aspx";
            strictEqual(domUtils_1.DomUtils.toAbsoluteUrl(url, base), "http://www.base.url/1/2.aspx");
        });
        test("toAbsoluteUrl should convert inherited protocol urls to non-inherited ones", function () {
            var url = "//www.mysite.site/img.jpeg";
            var base = "www.mysite.site";
            strictEqual(domUtils_1.DomUtils.toAbsoluteUrl(url, base), location.protocol + url);
        });
        test("toAbsoluteUrl should throw an error if url is empty", function () {
            var url = "";
            var base = "http://www.xyz.url/1/2/";
            throws(function () {
                domUtils_1.DomUtils.toAbsoluteUrl(url, base);
            }, Error("parameters must be non-empty, but was: " + url + ", " + base));
        });
        test("toAbsoluteUrl should throw an error if url is undefined", function () {
            var url = undefined;
            var base = "http://www.xyz.url/1/2/";
            throws(function () {
                domUtils_1.DomUtils.toAbsoluteUrl(url, base);
            }, Error("parameters must be non-empty, but was: " + url + ", " + base));
        });
        test("toAbsoluteUrl should throw an error if base is empty", function () {
            var url = "/path";
            var base = "";
            throws(function () {
                domUtils_1.DomUtils.toAbsoluteUrl(url, base);
            }, Error("parameters must be non-empty, but was: " + url + ", " + base));
        });
        test("toAbsoluteUrl should throw an error if base is undefined", function () {
            var url = "/path";
            var base = undefined;
            throws(function () {
                domUtils_1.DomUtils.toAbsoluteUrl(url, base);
            }, Error("parameters must be non-empty, but was: " + url + ", " + base));
        });
    };
    DomUtilsTests.prototype.createRootScriptIFrame = function () {
        var iframe = document.createElement("iframe");
        iframe.id = constants_1.Constants.Ids.clipperRootScript;
        return iframe;
    };
    DomUtilsTests.prototype.createUiIFrame = function () {
        var iframe = document.createElement("iframe");
        iframe.id = constants_1.Constants.Ids.clipperUiFrame;
        return iframe;
    };
    DomUtilsTests.prototype.createExtIFrame = function () {
        var iframe = document.createElement("iframe");
        iframe.id = constants_1.Constants.Ids.clipperExtFrame;
        return iframe;
    };
    DomUtilsTests.prototype.createMainArticleContainer = function () {
        var div = document.createElement("div");
        div.className = "MainArticleContainer";
        return div;
    };
    DomUtilsTests.prototype.createLinkElementWithHref = function (id, url) {
        var linkElement = document.createElement("link");
        linkElement.id = id;
        linkElement.href = url;
        this.fixture.appendChild(linkElement);
    };
    return DomUtilsTests;
}(testModule_1.TestModule));
exports.DomUtilsTests = DomUtilsTests;
(new DomUtilsTests()).runTests();
