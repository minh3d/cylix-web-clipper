"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var videoExtractor_1 = require("../../scripts/domParsers/videoExtractor");
var testModule_1 = require("../testModule");
var MockVideoExtractor = (function (_super) {
    __extends(MockVideoExtractor, _super);
    function MockVideoExtractor() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MockVideoExtractor.prototype.createEmbeddedVideosFromHtml = function (html) {
        return [
            this.createMockIframe("https://www.myvideosite.xyz/id1"),
            this.createMockIframe("https://www.myvideosite.xyz/url"),
            this.createMockIframe("https://www.myvideosite.xyz/id1"),
            this.createMockIframe("https://www.myvideosite.xyz/id2")
        ];
    };
    MockVideoExtractor.prototype.createEmbeddedVideoFromUrl = function (url) {
        return this.createMockIframe("https://www.myvideosite.xyz/url");
    };
    MockVideoExtractor.prototype.createEmbeddedVideoFromId = function (id) {
        return this.createMockIframe("https://www.myvideosite.xyz/id");
    };
    ;
    MockVideoExtractor.prototype.createMockIframe = function (src) {
        var iframe = document.createElement("iframe");
        iframe.src = src;
        return iframe;
    };
    return MockVideoExtractor;
}(videoExtractor_1.VideoExtractor));
var KhanAcademyVideoExtractorTests = (function (_super) {
    __extends(KhanAcademyVideoExtractorTests, _super);
    function KhanAcademyVideoExtractorTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.mockVideoExtractor = new MockVideoExtractor();
        return _this;
    }
    KhanAcademyVideoExtractorTests.prototype.module = function () {
        return "videoExtractor";
    };
    KhanAcademyVideoExtractorTests.prototype.tests = function () {
        var _this = this;
        test("Given that both of the page url and content represents a bunch of embeddable videos, a list of iframes are returned with no duplicate srcs", function () {
            var videoEmbeds = _this.mockVideoExtractor.createEmbeddedVideosFromPage("", "");
            strictEqual(videoEmbeds.length, 3, "3 unique iframes should be returned");
            strictEqual(videoEmbeds[0].src, "https://www.myvideosite.xyz/url", "The video associated with the url should be returned first");
            strictEqual(videoEmbeds[1].src, "https://www.myvideosite.xyz/id1", "The videos in the html should be returned sequentially");
            strictEqual(videoEmbeds[2].src, "https://www.myvideosite.xyz/id2", "The videos in the html should be returned sequentially");
        });
    };
    return KhanAcademyVideoExtractorTests;
}(testModule_1.TestModule));
exports.KhanAcademyVideoExtractorTests = KhanAcademyVideoExtractorTests;
(new KhanAcademyVideoExtractorTests()).runTests();
