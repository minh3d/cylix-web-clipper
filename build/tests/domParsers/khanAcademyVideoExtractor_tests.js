"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var khanAcademyVideoExtractor_1 = require("../../scripts/domParsers/khanAcademyVideoExtractor");
var testModule_1 = require("../testModule");
var KhanAcademyVideoExtractorTests = (function (_super) {
    __extends(KhanAcademyVideoExtractorTests, _super);
    function KhanAcademyVideoExtractorTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.pageContentWithNoClipId = [
            undefined,
            "",
            "<div id='clip'> </div>",
            "<div id='clip_abcdefg'> </div>" // invalid clip id
        ];
        _this.pageContentWithHyphenatedVideoIds = [
            "<div id='video_8-5DTsl1V5k'> </div>",
            "<div id = ' video_8-5DTsl1V5k ' > </div> ",
            "<DIV ID='Video_8-5DTsl1V5k'> </DIV>",
            "<div id=\"video_8-5DTsl1V5k\"> </div>" // double quotations,
        ];
        _this.pageContentWithDataYoutubeIds = [
            "<div data-youtubeid='video_8-5DTsl1V5k'> </div>",
            "<div data-youtubeid = ' video_8-5DTsl1V5k ' > </div> ",
            "<DIV data-youtubeid='Video_8-5DTsl1V5k'> </DIV>",
            "<div data-youtubeid=\"video_8-5DTsl1V5k\"> </div>" // double quotations
        ];
        _this.pageContentWithMultipleHyphenatedVideoIds = [
            "<div id='video_8-5DTsl1V5k'></div> <DIV ID='video_8-53sl1V5k'></DIV> <div id='video_4ba196611'> </div>",
            "<div id='video_8-5DTsl1V5k'></div> <DIV ID='video_8-53sl1V5k'></div> <DIV ID='video_8-53sl1V5k'></DIV> <div id='video_4ba196611'> </div>" // has a duplicate
        ];
        _this.khanAcademyVideoExtractor = new khanAcademyVideoExtractor_1.KhanAcademyVideoExtractor();
        _this.pageUrl = "";
        return _this;
    }
    KhanAcademyVideoExtractorTests.prototype.module = function () {
        return "khanAcademyVideoExtractor";
    };
    KhanAcademyVideoExtractorTests.prototype.tests = function () {
        var _this = this;
        test("createEmbeddedVimeosFromHtml should return a list of iframes with the src set to the embed url, and the data-original-src set to the watch url, given the html has hyphenated ids", function () {
            var expectedId = "8-5DTsl1V5k";
            for (var i = 0; i < _this.pageContentWithHyphenatedVideoIds.length; i++) {
                var embedVideos = _this.khanAcademyVideoExtractor.createEmbeddedVideosFromHtml(_this.pageContentWithHyphenatedVideoIds[i]);
                strictEqual(embedVideos.length, 1, "There should be one returned iframe");
                strictEqual(embedVideos[0].src, "https://www.youtube.com/embed/" + expectedId, "The src should be set to the embed url");
                strictEqual(embedVideos[0].attributes.getNamedItem("data-original-src").value, "https://www.youtube.com/watch?v=" + expectedId, "The data original src attribute should be set to the watch url");
            }
        });
        test("createEmbeddedVimeosFromHtml should return a list of iframes with the src set to the embed url, and the data-original-src set to the watch url, given the html has data youtube ids", function () {
            var expectedId = "8-5DTsl1V5k";
            for (var i = 0; i < _this.pageContentWithDataYoutubeIds.length; i++) {
                var embedVideos = _this.khanAcademyVideoExtractor.createEmbeddedVideosFromHtml(_this.pageContentWithDataYoutubeIds[i]);
                strictEqual(embedVideos.length, 1, "There should be one returned iframe");
                strictEqual(embedVideos[0].src, "https://www.youtube.com/embed/" + expectedId, "The src should be set to the embed url");
                strictEqual(embedVideos[0].attributes.getNamedItem("data-original-src").value, "https://www.youtube.com/watch?v=" + expectedId, "The data original src attribute should be set to the watch url");
            }
        });
        test("createEmbeddedVimeosFromHtml should a list of iframes with the src and data-original-src set to the player url with the clip id when there's more than 0 video in the html", function () {
            var expectedIds = [
                ["8-5DTsl1V5k", "8-53sl1V5k", "4ba196611"],
                ["8-5DTsl1V5k", "8-53sl1V5k", "8-53sl1V5k", "4ba196611"]
            ];
            for (var i = 0; i < _this.pageContentWithMultipleHyphenatedVideoIds.length; i++) {
                var embedVideos = _this.khanAcademyVideoExtractor.createEmbeddedVideosFromHtml(_this.pageContentWithMultipleHyphenatedVideoIds[i]);
                strictEqual(embedVideos.length, expectedIds[i].length, "There should be " + expectedIds[i].length + " iframes");
                for (var j = 0; j < expectedIds[i].length; j++) {
                    strictEqual(embedVideos[j].src, "https://www.youtube.com/embed/" + expectedIds[i][j], "The src should be set to the embed url");
                    strictEqual(embedVideos[j].attributes.getNamedItem("data-original-src").value, "https://www.youtube.com/watch?v=" + expectedIds[i][j], "The data original src attribute should be set to the watch url");
                }
            }
        });
        test("createEmbeddedVimeosFromHtml should return the empty list if there are no embed videos in the html", function () {
            var embedVideos = _this.khanAcademyVideoExtractor.createEmbeddedVideosFromHtml("<div></div>");
            strictEqual(embedVideos.length, 0, "There should be 0 iframes in the returned");
            embedVideos = _this.khanAcademyVideoExtractor.createEmbeddedVideosFromHtml("");
            strictEqual(embedVideos.length, 0, "There should be 0 iframes in the returned");
        });
        test("createEmbeddedVimeosFromHtml should return the empty list if there elements with invalid ids in the html", function () {
            for (var i = 0; i < _this.pageContentWithNoClipId.length; i++) {
                var embedVideos = _this.khanAcademyVideoExtractor.createEmbeddedVideosFromHtml(_this.pageContentWithNoClipId[i]);
                strictEqual(embedVideos.length, 0, "There should be 0 iframes in the returned");
            }
        });
        test("createEmbeddedVimeosFromHtml should return the empty list if the html is undefined", function () {
            var embedVideos = _this.khanAcademyVideoExtractor.createEmbeddedVideosFromHtml(undefined);
            strictEqual(embedVideos.length, 0, "There should be 0 iframes in the returned");
        });
        test("createEmbeddedVideoFromUrl should return undefined, as KhanAcademy does not host their own videos", function () {
            strictEqual(_this.khanAcademyVideoExtractor.createEmbeddedVideoFromUrl("https://www.khanacademy.org"), undefined, "createEmbeddedVideoFromUrl should always return undefined");
            strictEqual(_this.khanAcademyVideoExtractor.createEmbeddedVideoFromUrl("https://www.khanacademy.org/humanities/art-1010/beginners-guide-20-21/v/representation-abstraction-looking-at-millais-and-newman"), undefined, "createEmbeddedVideoFromUrl should always return undefined");
            strictEqual(_this.khanAcademyVideoExtractor.createEmbeddedVideoFromUrl("https://www.khanacademy.org/college-admissions/applying-to-college/college-application-process/a/filling-out-the-college-application-common-application-walkthrough"), undefined, "createEmbeddedVideoFromUrl should always return undefined");
            strictEqual(_this.khanAcademyVideoExtractor.createEmbeddedVideoFromUrl(""), undefined, "createEmbeddedVideoFromUrl should always return undefined");
            strictEqual(_this.khanAcademyVideoExtractor.createEmbeddedVideoFromUrl(undefined), undefined, "createEmbeddedVideoFromUrl should always return undefined");
        });
        test("createEmbeddedVideoFromId should create an iframe with the src set to the embed url, and the data-original-src set to the watch url", function () {
            var id = "12345";
            var embedVideo = _this.khanAcademyVideoExtractor.createEmbeddedVideoFromId(id);
            strictEqual(embedVideo.src, "https://www.youtube.com/embed/" + id, "The src should be set to the embed url");
            strictEqual(embedVideo.attributes.getNamedItem("data-original-src").value, "https://www.youtube.com/watch?v=" + id, "The data original src attribute should be set to the watch url");
        });
        test("createEmbeddedVideoFromId should return undefined if the input value is empty string or undefined", function () {
            strictEqual(_this.khanAcademyVideoExtractor.createEmbeddedVideoFromId(""), undefined);
            strictEqual(_this.khanAcademyVideoExtractor.createEmbeddedVideoFromId(undefined), undefined);
        });
    };
    return KhanAcademyVideoExtractorTests;
}(testModule_1.TestModule));
exports.KhanAcademyVideoExtractorTests = KhanAcademyVideoExtractorTests;
(new KhanAcademyVideoExtractorTests()).runTests();
