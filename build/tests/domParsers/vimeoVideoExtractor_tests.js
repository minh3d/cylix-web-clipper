"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var vimeoVideoExtractor_1 = require("../../scripts/domParsers/vimeoVideoExtractor");
var testModule_1 = require("../testModule");
var VimeoVideoExtractorTests = (function (_super) {
    __extends(VimeoVideoExtractorTests, _super);
    function VimeoVideoExtractorTests() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.pageContentWithOneClipId = [
            "<div id='clip_45196609'> </div>",
            "<div id = ' clip_45196609 ' > </div> ",
            "<DIV ID='Clip_45196609'> </DIV>",
            "<div id=\"clip_45196609\"> </div>" // double quotations
        ];
        _this.pageContentWithNoClipId = [
            undefined,
            "",
            "<div id='clip'> </div>",
            "<div id='clip_abcdefg'> </div>" // invalid clip id
        ];
        _this.pageContentWithMultipleClipIds = [
            "<div id='clip_45196609'></div> <DIV ID='clip_45196610'></DIV> <div id='clip_45196611'> </div>",
            "<div id='clip_45196609'></div> <DIV ID='clip_45196610'></DIV> <DIV ID='clip_45196609'></DIV><div id='clip_45196611'> </div>" // has a duplicate
        ];
        _this.pageUrl = "";
        _this.vimeoExtractor = new vimeoVideoExtractor_1.VimeoVideoExtractor();
        return _this;
    }
    VimeoVideoExtractorTests.prototype.module = function () {
        return "vimeoVideoExtractor";
    };
    VimeoVideoExtractorTests.prototype.tests = function () {
        var _this = this;
        test("createEmbeddedVimeosFromHtml should a list of iframes with the src and data-original-src set to the player url with the clip id", function () {
            var expectedId = "45196609";
            for (var _i = 0, _a = _this.pageContentWithOneClipId; _i < _a.length; _i++) {
                var pageContentSnippet = _a[_i];
                var embedVideos = _this.vimeoExtractor.createEmbeddedVideosFromHtml(pageContentSnippet);
                strictEqual(embedVideos.length, 1, "There should be one iframe");
                strictEqual(embedVideos[0].src, "https://player.vimeo.com/video/" + expectedId, "The src should be set to the player url");
                strictEqual(embedVideos[0].attributes.getNamedItem("data-original-src").value, "https://player.vimeo.com/video/" + expectedId, "The data original src attribute should be set to the player url");
            }
        });
        test("createEmbeddedVimeosFromHtml should a list of iframes with the src and data-original-src set to the player url with the clip id when there's more than 0 video in the html", function () {
            var expectedIds = [
                ["45196609", "45196610", "45196611"],
                ["45196609", "45196610", "45196609", "45196611"]
            ];
            for (var i = 0; i < _this.pageContentWithMultipleClipIds.length; i++) {
                var embedVideos = _this.vimeoExtractor.createEmbeddedVideosFromHtml(_this.pageContentWithMultipleClipIds[i]);
                strictEqual(embedVideos.length, expectedIds[i].length, "There should be " + expectedIds[i].length + " iframes");
                for (var j = 0; j < expectedIds[i].length; j++) {
                    strictEqual(embedVideos[j].src, "https://player.vimeo.com/video/" + expectedIds[i][j], "The src should be set to the player url");
                    strictEqual(embedVideos[j].attributes.getNamedItem("data-original-src").value, "https://player.vimeo.com/video/" + expectedIds[i][j], "The data original src attribute should be set to the player url");
                }
            }
        });
        test("createEmbeddedVimeosFromHtml should return the empty list if there are no embed videos in the html", function () {
            var embedVideos = _this.vimeoExtractor.createEmbeddedVideosFromHtml("<div></div>");
            strictEqual(embedVideos.length, 0, "There should be 0 iframes in the returned");
            embedVideos = _this.vimeoExtractor.createEmbeddedVideosFromHtml("");
            strictEqual(embedVideos.length, 0, "There should be 0 iframes in the returned");
        });
        test("createEmbeddedVimeosFromHtml should return the empty list if the html is undefined", function () {
            var embedVideos = _this.vimeoExtractor.createEmbeddedVideosFromHtml(undefined);
            strictEqual(embedVideos.length, 0, "There should be 0 iframes in the returned");
        });
        test("createEmbedVideoFromUrl should return an iframe with the src set to the player url, and the data-original-src set to the player url", function () {
            var embedVideo = _this.vimeoExtractor.createEmbeddedVideoFromUrl("https://vimeo.com/193851364");
            strictEqual(embedVideo.src, "https://player.vimeo.com/video/193851364", "The src should be set to the player url");
            strictEqual(embedVideo.attributes.getNamedItem("data-original-src").value, "https://player.vimeo.com/video/193851364", "The data original src attribute should be set to the player url");
        });
        test("createEmbedVideoFromUrl should return an iframe with the src set to the player url without query parameters, and the data-original-src set to the player url", function () {
            var embedVideo = _this.vimeoExtractor.createEmbeddedVideoFromUrl("https://vimeo.com/193851364?foo=134");
            strictEqual(embedVideo.src, "https://player.vimeo.com/video/193851364", "The src should be set to the player url");
            strictEqual(embedVideo.attributes.getNamedItem("data-original-src").value, "https://player.vimeo.com/video/193851364", "The data original src attribute should be set to the player url");
        });
        test("createEmbeddedVideoFromUrl should return undefined when provided unsupported parameters for the Vimeo domain", function () {
            var embedVideo = _this.vimeoExtractor.createEmbeddedVideoFromUrl("https://vimeo.com/ondemand/lifeanimated");
            strictEqual(embedVideo, undefined, "The returned iframe should be undefined");
            embedVideo = _this.vimeoExtractor.createEmbeddedVideoFromUrl("https://vimeo.com");
            strictEqual(embedVideo, undefined, "The returned iframe should be undefined");
        });
        test("createEmbeddedVideoFromUrl should return undefined if the input value is empty string or undefined", function () {
            strictEqual(_this.vimeoExtractor.createEmbeddedVideoFromUrl(""), undefined);
            strictEqual(_this.vimeoExtractor.createEmbeddedVideoFromUrl(undefined), undefined);
        });
        test("createEmbeddedVideoFromId should create an iframe with the src set to the player url, and the data-original-src set to the player url", function () {
            var id = "12345";
            var embedVideo = _this.vimeoExtractor.createEmbeddedVideoFromId(id);
            strictEqual(embedVideo.src, "https://player.vimeo.com/video/" + id, "The src should be set to the embed url");
            strictEqual(embedVideo.attributes.getNamedItem("data-original-src").value, "https://player.vimeo.com/video/" + id, "The data original src attribute should be set to the watch url");
        });
        test("createEmbeddedVideoFromId should return undefined if the input value is empty string or undefined", function () {
            strictEqual(_this.vimeoExtractor.createEmbeddedVideoFromId(""), undefined);
            strictEqual(_this.vimeoExtractor.createEmbeddedVideoFromId(undefined), undefined);
        });
    };
    return VimeoVideoExtractorTests;
}(testModule_1.TestModule));
exports.VimeoVideoExtractorTests = VimeoVideoExtractorTests;
(new VimeoVideoExtractorTests()).runTests();
