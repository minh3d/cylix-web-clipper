"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var videoUtils_1 = require("../../scripts/domParsers/videoUtils");
var testModule_1 = require("../testModule");
var VideoUtilsTests = (function (_super) {
    __extends(VideoUtilsTests, _super);
    function VideoUtilsTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    VideoUtilsTests.prototype.module = function () {
        return "videoUtils";
    };
    VideoUtilsTests.prototype.tests = function () {
        test("videoDomainIfSupported should return undefined on the YouTube homepage", function () {
            strictEqual(videoUtils_1.VideoUtils.videoDomainIfSupported("https://www.youtube.com/"), undefined, "YouTube homepage should not be recognized as a page with a video");
        });
        test("videoDomainIfSupported should return undefined on the Vimeo homepage", function () {
            strictEqual(videoUtils_1.VideoUtils.videoDomainIfSupported("https://www.vimeo.com/"), undefined, "Vimeo homepage should not be recognized as a page with a video");
        });
        test("videoDomainIfSupported should return undefined on a YouTube channel page", function () {
            strictEqual(videoUtils_1.VideoUtils.videoDomainIfSupported("https://www.youtube.com/channel/UC38IQsAvIsxxjztdMZQtwHA"), undefined, "YouTube channel page should not be recognized as a page with a video");
        });
        test("videoDomainIfSupported should return undefined on a Vimeo user page", function () {
            strictEqual(videoUtils_1.VideoUtils.videoDomainIfSupported("https://vimeo.com/user12402347"), undefined, "Vimeo user page should not be recognized as a page with a video");
        });
        test("videoDomainIfSupported should return undefined on a Vimeo collections page", function () {
            strictEqual(videoUtils_1.VideoUtils.videoDomainIfSupported("https://vimeo.com/45196609/collections/channels"), undefined, "Vimeo collections page should not be recognized as a page with a video");
        });
        test("videoDomainIfSupported should return YouTube if the url represents a video, embedded video, or mobile video on YouTube", function () {
            var supportedYouTubeUrls = [
                "https://youtube.com/watch?v=dQw4w9WgXcQ",
                "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
                "https://www.youtube.com/watch?v=dQw4w9WgXcQ&feature=youtu.be&t=30s",
                "https://www.youtube.com/watch?v=dQw4w9WgXcQ#foo",
                "https://www.youtube.com/watch?feature=youtu.be&t=30s&v=dQw4w9WgXcQ",
                "https://m.youtube.com/watch?v=dQw4w9WgXcQ&feature=youtu.be",
                "https://www.youtube.com/embed/dQw4w9WgXcQ"
            ];
            for (var _i = 0, supportedYouTubeUrls_1 = supportedYouTubeUrls; _i < supportedYouTubeUrls_1.length; _i++) {
                var pageUrl = supportedYouTubeUrls_1[_i];
                var domain = videoUtils_1.VideoUtils.videoDomainIfSupported(pageUrl);
                strictEqual(domain, "YouTube", pageUrl + " should return YouTube");
            }
        });
        test("videoDomainIfSupported should return Vimeo if the url represents a video, staffpick, album, or ondemand video on Vimeo", function () {
            var supportedVimeoUrls = [
                "https://vimeo.com/45196609",
                "https://www.vimeo.com/45196609",
                "https://vimeo.com/45196609?autoplay=1",
                "https://vimeo.com/45196609#t=0",
                "https://vimeo.com/channels/staffpicks/45196609",
                "https://vimeo.com/album/45196609/",
                "https://vimeo.com/album/45196609/page:1",
                "https://vimeo.com/album/45196609/page:3/sort:preset/format:thumbnail",
                "https://vimeo.com/album/45196609/sort:preset/format:thumbnail/page:2",
                "https://vimeo.com/album/45196609/video/45196609",
                "https://vimeo.com/ondemand/45196609"
            ];
            for (var _i = 0, supportedVimeoUrls_1 = supportedVimeoUrls; _i < supportedVimeoUrls_1.length; _i++) {
                var pageUrl = supportedVimeoUrls_1[_i];
                var domain = videoUtils_1.VideoUtils.videoDomainIfSupported(pageUrl);
                strictEqual(domain, "Vimeo", pageUrl + " should return Vimeo");
            }
        });
        test("videoDomainIfSupported should return Vimeo on a Vimeo ondemand page", function () {
            strictEqual(videoUtils_1.VideoUtils.videoDomainIfSupported("https://vimeo.com/ondemand/reeltoreal"), "Vimeo", "Vimeo ondemand page should be recognized as a page with a video");
            strictEqual(videoUtils_1.VideoUtils.videoDomainIfSupported("https://vimeo.com/ondemand/"), undefined, "Vimeo ondemand root page should not be recognized as a page with a video");
        });
        test("videoDomainIfSupported should correctly return the domain if its a video site, regardless of the url's casing", function () {
            var supportedVimeoUrls = [
                "https://VIMEO.com/45196609",
                "https://vImEO.com/45196609?autoplay=1",
                "https://vimeo.com/45196609?AUTOPLAY=1"
            ];
            for (var _i = 0, supportedVimeoUrls_2 = supportedVimeoUrls; _i < supportedVimeoUrls_2.length; _i++) {
                var pageUrl = supportedVimeoUrls_2[_i];
                var domain = videoUtils_1.VideoUtils.videoDomainIfSupported(pageUrl);
                strictEqual(domain, "Vimeo", pageUrl + " should return Vimeo");
            }
        });
        test("videoDomainIfSupported should return undefined on arbitrary urls", function () {
            var unsupportedOtherUrls = [
                "https://www.hulu.com/",
                "https://www.google.com/",
                "https://fistbump.reviews/",
                "abcde",
                "12345.com"
            ];
            for (var _i = 0, unsupportedOtherUrls_1 = unsupportedOtherUrls; _i < unsupportedOtherUrls_1.length; _i++) {
                var pageUrl = unsupportedOtherUrls_1[_i];
                var domain = videoUtils_1.VideoUtils.videoDomainIfSupported(pageUrl);
                strictEqual(domain, undefined, pageUrl + " should not be recognized as a video page and should return undefined");
            }
        });
        test("videoDomainIfSupported should return undefined on arbitrary urls even if the url contains a domain we support as a substring", function () {
            var unsupportedOtherUrls = [
                "https://www.youtube1.com/watch?v=dQw4w9WgXcQ",
                "https://vimeoo.com/45196609",
                "https://avimeo.com/45196609"
            ];
            for (var _i = 0, unsupportedOtherUrls_2 = unsupportedOtherUrls; _i < unsupportedOtherUrls_2.length; _i++) {
                var pageUrl = unsupportedOtherUrls_2[_i];
                var domain = videoUtils_1.VideoUtils.videoDomainIfSupported(pageUrl);
                strictEqual(domain, undefined, pageUrl + " should not be recognized as a video page and should return undefined");
            }
        });
        test("videoDomainIfSupported should return undefined when passed undefined or empty string", function () {
            var unsupportedOtherUrls = [
                "",
                undefined
            ];
            for (var _i = 0, unsupportedOtherUrls_3 = unsupportedOtherUrls; _i < unsupportedOtherUrls_3.length; _i++) {
                var pageUrl = unsupportedOtherUrls_3[_i];
                var domain = videoUtils_1.VideoUtils.videoDomainIfSupported(pageUrl);
                strictEqual(domain, undefined, pageUrl + " should not be recognized as a video page and should return undefined");
            }
        });
    };
    return VideoUtilsTests;
}(testModule_1.TestModule));
exports.VideoUtilsTests = VideoUtilsTests;
(new VideoUtilsTests()).runTests();
