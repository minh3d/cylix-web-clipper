"use strict";
var clientType_1 = require("../scripts/clientType");
var clipMode_1 = require("../scripts/clipperUI/clipMode");
var status_1 = require("../scripts/clipperUI/status");
var smartValue_1 = require("../scripts/communicator/smartValue");
var invokeOptions_1 = require("../scripts/extensions/invokeOptions");
var localization_1 = require("../scripts/localization/localization");
var userInfo_1 = require("../scripts/userInfo");
/**
 * Collection of mock props used in our tests. Mostly intended for preventing
 * lots of boilerplate, and can be customized to suit the test's needs.
 */
var MockProps;
(function (MockProps) {
    function getMockClipperState() {
        var clipperState = {
            injectOptions: {
                frameUrl: "",
                enableAddANote: true,
                enableEditableTitle: true,
                enableRegionClipping: true
            },
            uiExpanded: true,
            fetchLocStringStatus: status_1.Status.Succeeded,
            invokeOptions: {
                invokeMode: invokeOptions_1.InvokeMode.Default
            },
            userResult: {
                status: status_1.Status.Succeeded,
                data: {
                    user: {
                        emailAddress: "mockEmail@hotmail.com",
                        fullName: "Mock Johnson",
                        accessToken: "mockToken",
                        accessTokenExpiration: 3000
                    },
                    lastUpdated: 10000000,
                    updateReason: userInfo_1.UpdateReason.InitialRetrieval
                }
            },
            pageInfo: {
                canonicalUrl: "http://www.canonical.xyzabc/",
                contentData: "",
                contentLocale: "en-US",
                contentTitle: "Jello World Website",
                contentType: undefined,
                rawUrl: "http://www.mock.xyzabc/mypage/"
            },
            clientInfo: {
                clipperId: "ON-12345678-12ab-1234-a1b2-1a23bcd45678",
                clipperType: clientType_1.ClientType.ChromeExtension,
                clipperVersion: "3.0.0",
                flightingInfo: []
            },
            currentMode: new smartValue_1.SmartValue(clipMode_1.ClipMode.FullPage),
            saveLocation: "",
            fullPageResult: {
                data: {},
                status: status_1.Status.NotStarted
            },
            pdfResult: {
                data: new smartValue_1.SmartValue({}),
                status: status_1.Status.NotStarted
            },
            regionResult: {
                data: undefined,
                status: status_1.Status.NotStarted
            },
            augmentationResult: {
                data: {},
                status: status_1.Status.NotStarted
            },
            bookmarkResult: {
                data: undefined,
                status: status_1.Status.NotStarted
            },
            previewGlobalInfo: {
                annotation: "",
                fontSize: parseInt(localization_1.Localization.getLocalizedString("WebClipper.FontSize.Preview.SansSerifDefault"), 10),
                highlighterEnabled: false,
                previewTitleText: "Edited title",
                serif: false
            },
            pdfPreviewInfo: {
                allPages: true,
                isLocalFileAndNotAllowed: false,
                selectedPageRange: "1,3,5",
                shouldAttachPdf: true,
                shouldDistributePages: true,
                shouldShowPopover: false
            },
            augmentationPreviewInfo: {
                previewBodyHtml: "Edited body"
            },
            selectionPreviewInfo: {
                previewBodyHtml: "Selected body"
            },
            oneNoteApiResult: {
                data: undefined,
                status: status_1.Status.NotStarted
            },
            clipSaveStatus: {
                numItemsCompleted: undefined,
                numItemsTotal: undefined
            },
            setState: function (newPartialState) {
                for (var key in newPartialState) {
                    if (newPartialState.hasOwnProperty(key)) {
                        clipperState[key] = newPartialState[key];
                    }
                }
            },
            reset: function () {
            }
        };
        return clipperState;
    }
    MockProps.getMockClipperState = getMockClipperState;
    function getMockNotebooks() {
        return [{
                isDefault: true,
                userRole: "Owner",
                isShared: false,
                sectionsUrl: "https://www.onenote.com/api/v1.0/me/notes/notebooks/0-EB15C30446636CBE!18732/sections",
                sectionGroupsUrl: "https://www.onenote.com/api/v1.0/me/notes/notebooks/0-EB15C30446636CBE!18732/sectionGroups",
                links: {
                    oneNoteClientUrl: {
                        href: "onenote:https://d.docs.live.net/eb15c30446636cbe/Documents/Clipper%20Test"
                    },
                    oneNoteWebUrl: {
                        href: "https://onedrive.live.com/redir.aspx?cid=eb15c30446636cbe&page=edit&resid=EB15C30446636CBE!18732&parId=EB15C30446636CBE!105"
                    }
                },
                name: "Clipper Test",
                createdBy: "Matthew Chiam",
                lastModifiedBy: "Matthew Chiam",
                lastModifiedTime: new Date("Mon Feb 22 2016"),
                id: "0-EB15C30446636CBE!18732",
                self: "https://www.onenote.com/api/v1.0/me/notes/notebooks/0-EB15C30446636CBE!18732",
                createdTime: new Date("Mon Feb 22 2016"),
                sections: [{
                        isDefault: true,
                        pagesUrl: "https://www.onenote.com/api/v1.0/me/notes/sections/0-EB15C30446636CBE!18742/pages",
                        name: "Full Page",
                        createdBy: "Matthew Chiam",
                        lastModifiedBy: "Matthew Chiam",
                        lastModifiedTime: new Date("Tue Feb 23 2016"),
                        id: "0-EB15C30446636CBE!18742",
                        self: "https://www.onenote.com/api/v1.0/me/notes/sections/0-EB15C30446636CBE!18742",
                        createdTime: new Date("Tue Feb 23 2016"),
                        pages: []
                    }, {
                        isDefault: false,
                        pagesUrl: "https://www.onenote.com/api/v1.0/me/notes/sections/0-EB15C30446636CBE!18738/pages",
                        name: "Pdfs",
                        createdBy: "Matthew Chiam",
                        lastModifiedBy: "Matthew Chiam",
                        lastModifiedTime: new Date("Tue Feb 23 2016"),
                        id: "0-EB15C30446636CBE!18738",
                        self: "https://www.onenote.com/api/v1.0/me/notes/sections/0-EB15C30446636CBE!18738",
                        createdTime: new Date("Tue Feb 23 2016"),
                        pages: []
                    }],
                sectionGroups: []
            }, {
                isDefault: false,
                userRole: "Owner",
                isShared: false,
                sectionsUrl: "https://www.onenote.com/api/v1.0/me/notes/notebooks/a-bc!d/sections",
                sectionGroupsUrl: "https://www.onenote.com/api/v1.0/me/notes/notebooks/a-bc!d/sectionGroups",
                links: {
                    oneNoteClientUrl: {
                        href: "onenote:https://d.docs.live.net/bc/Documents/Clipper%20Test%202"
                    },
                    oneNoteWebUrl: {
                        href: "https://onedrive.live.com/redir.aspx?cid=bc&page=edit&resid=bc!d&parId=bc!d"
                    }
                },
                name: "Clipper Test 2",
                createdBy: "Matthew Chiam",
                lastModifiedBy: "Matthew Chiam",
                lastModifiedTime: new Date("Mon Feb 22 2016"),
                id: "a-bc!d",
                self: "https://www.onenote.com/api/v1.0/me/notes/notebooks/abcd",
                createdTime: new Date("Mon Feb 22 2016"),
                sections: [{
                        isDefault: false,
                        pagesUrl: "https://www.onenote.com/api/v1.0/me/notes/sections/1234/pages",
                        name: "Section X",
                        createdBy: "Matthew Chiam",
                        lastModifiedBy: "Matthew Chiam",
                        lastModifiedTime: new Date("Tue Feb 23 2016"),
                        id: "1234",
                        self: "https://www.onenote.com/api/v1.0/me/notes/sections/1234",
                        createdTime: new Date("Tue Feb 23 2016"),
                        pages: []
                    }, {
                        isDefault: false,
                        pagesUrl: "https://www.onenote.com/api/v1.0/me/notes/sections/5678/pages",
                        name: "Section Y",
                        createdBy: "Matthew Chiam",
                        lastModifiedBy: "Matthew Chiam",
                        lastModifiedTime: new Date("Tue Feb 23 2016"),
                        id: "5678",
                        self: "https://www.onenote.com/api/v1.0/me/notes/sections/5678",
                        createdTime: new Date("Tue Feb 23 2016"),
                        pages: []
                    }],
                sectionGroups: []
            }];
    }
    MockProps.getMockNotebooks = getMockNotebooks;
    function getMockMainControllerProps() {
        return {
            clipperState: getMockClipperState(),
            onSignInInvoked: function () {
            },
            onSignOutInvoked: function () {
            },
            updateFrameHeight: function (newContainerHeight) {
            },
            onStartClip: function () {
            }
        };
    }
    MockProps.getMockMainControllerProps = getMockMainControllerProps;
    function getMockModeButtonProps() {
        return {
            imgSrc: "test.png",
            label: "My Button",
            myMode: clipMode_1.ClipMode.FullPage,
            tabIndex: 420,
            "aria-setsize": 4,
            "aria-posinset": 1,
            selected: false,
            onModeSelected: function (modeButton) {
            },
            tooltipText: "tooltip"
        };
    }
    MockProps.getMockModeButtonProps = getMockModeButtonProps;
})(MockProps = exports.MockProps || (exports.MockProps = {}));
