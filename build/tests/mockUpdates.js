"use strict";
var MockUpdates;
(function (MockUpdates) {
    function getMockUpdates() {
        return [{
                "version": "3.1.0",
                "date": "06/03/2016",
                "changes": [{
                        "title": "t1",
                        "description": "d1",
                        "supportedBrowsers": ["Edge", "Chrome", "Firefox", "Safari", "Bookmarklet"]
                    }, {
                        "title": "t2",
                        "description": "d2",
                        "supportedBrowsers": ["Edge", "Chrome", "Firefox", "Safari"]
                    }, {
                        "title": "t3",
                        "description": "d3",
                        "supportedBrowsers": ["Edge", "Chrome", "Firefox", "Safari"]
                    }, {
                        "title": "t4",
                        "description": "d4",
                        "supportedBrowsers": ["Edge", "Chrome", "Firefox", "Safari", "Bookmarklet"]
                    }]
            }];
    }
    MockUpdates.getMockUpdates = getMockUpdates;
    function getMockUpdatesWithSomeImages() {
        return [{
                "version": "3.1.0",
                "date": "06/03/2016",
                "changes": [{
                        "title": "t1",
                        "description": "d1",
                        "supportedBrowsers": ["Edge", "Chrome", "Firefox", "Safari", "Bookmarklet"]
                    }, {
                        "title": "t2",
                        "imageUrl": "http://www.mywebsite.fake/2.jpeg",
                        "description": "d2",
                        "supportedBrowsers": ["Edge", "Chrome", "Firefox", "Safari"]
                    }, {
                        "title": "t3",
                        "description": "d3",
                        "supportedBrowsers": ["Edge", "Chrome", "Firefox", "Safari"]
                    }, {
                        "title": "t4",
                        "description": "d4",
                        "imageUrl": "http://www.mywebsite.fake/4.jpeg",
                        "supportedBrowsers": ["Edge", "Chrome", "Firefox", "Safari", "Bookmarklet"]
                    }]
            }];
    }
    MockUpdates.getMockUpdatesWithSomeImages = getMockUpdatesWithSomeImages;
    function getMockMultipleUpdates() {
        return [{
                "version": "3.1.0",
                "date": "06/03/2016",
                "changes": [{
                        "title": "t1",
                        "description": "d1",
                        "supportedBrowsers": ["Edge", "Chrome", "Firefox", "Safari", "Bookmarklet"]
                    }, {
                        "title": "t2",
                        "description": "d2",
                        "supportedBrowsers": ["Edge", "Chrome", "Firefox", "Safari"]
                    }, {
                        "title": "t3",
                        "description": "d3",
                        "supportedBrowsers": ["Edge", "Chrome", "Firefox", "Safari"]
                    }, {
                        "title": "t4",
                        "description": "d4",
                        "supportedBrowsers": ["Edge", "Chrome", "Firefox", "Safari", "Bookmarklet"]
                    }]
            }, {
                "version": "3.0.0",
                "date": "06/03/2016",
                "changes": [{
                        "title": "t5",
                        "description": "d5",
                        "supportedBrowsers": ["Chrome", "Firefox", "Safari", "Bookmarklet"]
                    }, {
                        "title": "t6",
                        "description": "d6",
                        "supportedBrowsers": ["Edge", "Chrome", "Firefox", "Safari"]
                    }, {
                        "title": "t7",
                        "description": "d7",
                        "supportedBrowsers": ["Edge", "Chrome", "Firefox", "Safari"]
                    }]
            }];
    }
    MockUpdates.getMockMultipleUpdates = getMockMultipleUpdates;
})(MockUpdates = exports.MockUpdates || (exports.MockUpdates = {}));
