"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var cachedHttp_1 = require("../../scripts/http/cachedHttp");
var mockStorage_1 = require("../storage/mockStorage");
var testModule_1 = require("../testModule");
var CachedHttpTests = (function (_super) {
    __extends(CachedHttpTests, _super);
    function CachedHttpTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CachedHttpTests.prototype.module = function () {
        return "cachedHttp";
    };
    CachedHttpTests.prototype.beforeEach = function () {
        this.mockStorage = new mockStorage_1.MockStorage();
    };
    CachedHttpTests.prototype.tests = function () {
        var _this = this;
        test("valueHasExpired should return true if the timestamped data is older than the expiry time", function () {
            var expiry = 500;
            var value = {
                data: {},
                lastUpdated: Date.now() - (expiry + 1)
            };
            ok(cachedHttp_1.CachedHttp.valueHasExpired(value, expiry), "A timestamped value with a lastUpdated value older than the expiry should return true");
        });
        test("valueHasExpired should return false if the timestamped data is not older than the expiry time", function () {
            var expiry = 999999999999;
            var value = {
                data: {},
                lastUpdated: Date.now()
            };
            ok(!cachedHttp_1.CachedHttp.valueHasExpired(value, expiry), "A timestamped value with a lastUpdated value newer than the expiry should return false");
        });
        test("valueHasExpired should return true if value is undefined", function () {
            var expiry = 999999999999;
            ok(cachedHttp_1.CachedHttp.valueHasExpired(undefined, expiry), "A timestamped value with an undefined value should return true");
        });
        test("valueHasExpired should return true if value is undefined", function () {
            var expiry = 999999999999;
            var value = {
                data: {},
                lastUpdated: undefined
            };
            ok(cachedHttp_1.CachedHttp.valueHasExpired(value, expiry), "A timestamped value with an undefined value should return true");
        });
        test("getFreshValue where the value in storage is still fresh should not return or retrieve the value from the specified remote call", function (assert) {
            var done = assert.async();
            var expected = "expected";
            var timeOfStorage = Date.now();
            var valueInStorage = {
                data: expected,
                lastUpdated: timeOfStorage
            };
            var key = "k";
            _this.mockStorage.setValue(key, JSON.stringify(valueInStorage));
            var cachedHttp = new cachedHttp_1.CachedHttp(_this.mockStorage);
            var getRemoteValue = function () {
                return Promise.resolve({
                    parsedResponse: JSON.stringify("notexpected"),
                    request: undefined // We don't care about the request in the test
                });
            };
            cachedHttp.getFreshValue(key, getRemoteValue, 9999999999).then(function (timeStampedData) {
                strictEqual(timeStampedData.data, expected, "The storage item should be returned");
                strictEqual(timeStampedData.lastUpdated, timeOfStorage, "The returned item should have its lastUpdated value be preserved");
                var newStoredValue = JSON.parse(_this.mockStorage.getValue(key));
                strictEqual(newStoredValue.data, expected, "The storage item should be preserved");
                strictEqual(newStoredValue.lastUpdated, timeOfStorage, "The storage item's lastUpdated should be preserved");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("Given that lastUpdated is a small non-0 value, getFreshValue where the value in storage is too old should return the value from the specified remote call", function (assert) {
            var done = assert.async();
            var timeOfStorage = Date.now() - 9999999999;
            var valueInStorage = {
                data: "notexpected",
                lastUpdated: timeOfStorage
            };
            var key = "k";
            _this.mockStorage.setValue(key, JSON.stringify(valueInStorage));
            var cachedHttp = new cachedHttp_1.CachedHttp(_this.mockStorage);
            var expected = "expected";
            var getRemoteValue = function () {
                return Promise.resolve({
                    parsedResponse: JSON.stringify(expected),
                    request: undefined // We don't care about the request in the test
                });
            };
            cachedHttp.getFreshValue(key, getRemoteValue, 9999999999).then(function (timeStampedData) {
                strictEqual(timeStampedData.data, expected, "The remote item should be returned");
                ok(timeStampedData.lastUpdated > timeOfStorage, "The returned item's lastUpdated should be greater than that of the stale value's one");
                var newStoredValue = JSON.parse(_this.mockStorage.getValue(key));
                strictEqual(newStoredValue.data, expected, "The storage item should be updated to the remote value");
                ok(newStoredValue.lastUpdated > timeOfStorage, "The storage item's lastUpdated should be updated to be greater than the old value");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("getFreshValue with a forced remote call should set the timestamped value in storage when it is retrieved from the remote", function (assert) {
            var done = assert.async();
            var cachedHttp = new cachedHttp_1.CachedHttp(_this.mockStorage);
            // A JSON-parseable response is expected from the remote source
            var response = JSON.stringify("ASDF!");
            var key = "k";
            var getRemoteValue = function () {
                return Promise.resolve({
                    parsedResponse: response,
                    request: undefined // We don't care about the request in the test
                });
            };
            cachedHttp.getFreshValue(key, getRemoteValue, 0).then(function (timeStampedData) {
                ok(timeStampedData.lastUpdated > 0);
                strictEqual(timeStampedData.data, JSON.parse(response), "The parsed response text should be returned as part of the time stamped data");
                var timeStampedValueInStorage = JSON.parse(_this.mockStorage.getValue(key));
                deepEqual(timeStampedValueInStorage, timeStampedData, "The latest time stamped data should be cached in the storage object");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("getFreshValue with a forced remote call should not set anything in storage if the remote function rejected", function (assert) {
            var done = assert.async();
            var cachedHttp = new cachedHttp_1.CachedHttp(_this.mockStorage);
            var key = "k";
            var expectedError = { err: "err" };
            var getRemoteValue = function () {
                return Promise.reject(expectedError);
            };
            cachedHttp.getFreshValue(key, getRemoteValue, 0).then(function (timeStampedData) {
                ok(false, "resolve should not be called");
            }, function (actualError) {
                deepEqual(actualError, expectedError, "The reject object should be bubbled to getAndCacheRemoteValue's reject");
                strictEqual(_this.mockStorage.getValue(key), undefined, "Nothing should be stored in storage for the given key");
            }).then(function () {
                done();
            });
        });
        test("When getFreshValue is called with an undefined key, an Error should be thrown", function () {
            var cachedHttp = new cachedHttp_1.CachedHttp(_this.mockStorage);
            var getRemoteValue = function () {
                return Promise.reject({});
            };
            throws(function () {
                cachedHttp.getFreshValue(undefined, getRemoteValue, 100);
            }, Error("key must be a non-empty string, but was: undefined"));
        });
        test("When getFreshValue is called with an empty key, an Error should be thrown", function () {
            var cachedHttp = new cachedHttp_1.CachedHttp(_this.mockStorage);
            var getRemoteValue = function () {
                return Promise.reject({});
            };
            throws(function () {
                cachedHttp.getFreshValue("", getRemoteValue, 100);
            }, Error("key must be a non-empty string, but was: "));
        });
        test("When getFreshValue is called with an getRemoteValue key, an Error should be thrown", function () {
            var cachedHttp = new cachedHttp_1.CachedHttp(_this.mockStorage);
            throws(function () {
                cachedHttp.getFreshValue("k", undefined, 100);
            }, Error("getRemoteValue must be non-undefined"));
        });
    };
    return CachedHttpTests;
}(testModule_1.TestModule));
exports.CachedHttpTests = CachedHttpTests;
(new CachedHttpTests()).runTests();
