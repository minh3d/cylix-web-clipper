"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var sinon = require("sinon");
var http_1 = require("../../scripts/http/http");
var testModule_1 = require("../testModule");
var HttpTests = (function (_super) {
    __extends(HttpTests, _super);
    function HttpTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    HttpTests.prototype.module = function () {
        return "http";
    };
    HttpTests.prototype.beforeEach = function () {
        this.server = sinon.fakeServer.create();
        this.server.respondImmediately = true;
    };
    HttpTests.prototype.afterEach = function () {
        this.server.restore();
    };
    HttpTests.prototype.tests = function () {
        var _this = this;
        test("When get is called with valid parameters and the server returns 200, the request should be returned in the resolve", function (assert) {
            var done = assert.async();
            var response = "Success!";
            _this.server.respondWith([200, {}, response]);
            http_1.Http.get("https://not-exist.xyz/123/4567/abc.aspx").then(function (request) {
                ok(request, "The request object in the package must be non-undefined");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("When get is called with valid parameters and the server returns 204 when the caller specified that its a valid response code, the request should be returned in the resolve", function (assert) {
            var done = assert.async();
            var response = "Success!";
            _this.server.respondWith([204, {}, response]);
            http_1.Http.get("https://not-exist.xyz/123/4567/abc.aspx", {}, 30000, [200, 204]).then(function (request) {
                ok(request, "The request object in the package must be non-undefined");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("When get is called with valid parameters and the server returns non-200 2XX, the promise should be rejected with the request error object", function (assert) {
            var done = assert.async();
            var responseJson = {
                error: "Unexpected response status",
                statusCode: 204,
                responseHeaders: {}
            };
            var expected = {
                error: responseJson.error,
                statusCode: responseJson.statusCode,
                responseHeaders: responseJson.responseHeaders,
                response: JSON.stringify(responseJson)
            };
            _this.server.respondWith([expected.statusCode, expected.responseHeaders, expected.response]);
            http_1.Http.get("https://not-exist.xyz/123/4567/abc.aspx").then(function (responsePackage) {
                ok(false, "resolve should not be called");
            }, function (error) {
                deepEqual(error, expected, "The error object should be rejected");
            }).then(function () {
                done();
            });
        });
        test("When get is called with an undefined url, an Error should be thrown", function () {
            throws(function () {
                http_1.Http.get(undefined);
            }, Error("url must be a non-empty string, but was: undefined"));
        });
        test("When get is called with an empty url, an Error should be thrown", function () {
            throws(function () {
                http_1.Http.get("");
            }, Error("url must be a non-empty string, but was: "));
        });
        test("When get is called with valid parameters and the server returns 4XX, the promise should be rejected with the request error object", function (assert) {
            var done = assert.async();
            var responseJson = {
                error: "Unexpected response status",
                statusCode: 404,
                responseHeaders: {}
            };
            var expected = {
                error: responseJson.error,
                statusCode: responseJson.statusCode,
                responseHeaders: responseJson.responseHeaders,
                response: JSON.stringify(responseJson),
                timeout: 30000
            };
            _this.server.respondWith([expected.statusCode, expected.responseHeaders, expected.response]);
            http_1.Http.get("https://not-exist.xyz/123/4567/abc.aspx").then(function (responsePackage) {
                ok(false, "resolve should not be called");
            }, function (error) {
                deepEqual(error, expected, "The error object should be rejected");
            }).then(function () {
                done();
            });
        });
        test("When get is called with valid parameters and the server returns 5XX, the promise should be rejected with the request error object", function (assert) {
            var done = assert.async();
            var responseJson = {
                error: "Unexpected response status",
                statusCode: 500,
                responseHeaders: {}
            };
            var expected = {
                error: responseJson.error,
                statusCode: responseJson.statusCode,
                responseHeaders: responseJson.responseHeaders,
                response: JSON.stringify(responseJson),
                timeout: 30000
            };
            _this.server.respondWith([expected.statusCode, expected.responseHeaders, expected.response]);
            http_1.Http.get("https://not-exist.xyz/123/4567/abc.aspx").then(function (responsePackage) {
                ok(false, "resolve should not be called");
            }, function (error) {
                deepEqual(error, expected, "The error object should be rejected");
            }).then(function () {
                done();
            });
        });
        test("When post is called with valid parameters and the server returns 200, the request should be returned in the resolve", function (assert) {
            var done = assert.async();
            var response = "Success!";
            _this.server.respondWith([200, {}, response]);
            http_1.Http.post("https://not-exist.xyz/123/4567/abc.aspx", "DATA", {}).then(function (request) {
                ok(request, "The request object in the package must be non-undefined");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("When post is called with data as an empty string and the server returns 200, the request should be returned in the resolve", function (assert) {
            var done = assert.async();
            var response = "Success!";
            _this.server.respondWith([200, {}, response]);
            http_1.Http.post("https://not-exist.xyz/123/4567/abc.aspx", "", {}).then(function (request) {
                ok(request, "The request object in the package must be non-undefined");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("When post is called with valid parameters and the server returns 204 when the caller specified that its a valid response code, the request should be returned in the resolve", function (assert) {
            var done = assert.async();
            var response = "Success!";
            _this.server.respondWith([204, {}, response]);
            http_1.Http.post("https://not-exist.xyz/123/4567/abc.aspx", "DATA", {}, [200, 204]).then(function (request) {
                ok(request, "The request object in the package must be non-undefined");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("When post is called with valid parameters and the server returns non-200 2XX, the promise should be rejected with the request error object", function (assert) {
            var done = assert.async();
            var responseJson = {
                error: "Unexpected response status",
                statusCode: 204,
                responseHeaders: {}
            };
            var expected = {
                error: responseJson.error,
                statusCode: responseJson.statusCode,
                responseHeaders: responseJson.responseHeaders,
                response: JSON.stringify(responseJson)
            };
            _this.server.respondWith([expected.statusCode, expected.responseHeaders, expected.response]);
            http_1.Http.post("https://not-exist.xyz/123/4567/abc.aspx", "DATA", {}).then(function (responsePackage) {
                ok(false, "resolve should not be called");
            }, function (error) {
                deepEqual(error, expected, "The error object should be rejected");
            }).then(function () {
                done();
            });
        });
        test("When post is called with an undefined url, an Error should be thrown", function () {
            throws(function () {
                http_1.Http.post(undefined, "DATA");
            }, Error("url must be a non-empty string, but was: undefined"));
        });
        test("When post is called with an empty url, an Error should be thrown", function () {
            throws(function () {
                http_1.Http.post("", "DATA");
            }, Error("url must be a non-empty string, but was: "));
        });
        test("When post is called with an undefined data, an Error should be thrown", function () {
            throws(function () {
                http_1.Http.post("https://not-exist.xyz/123/4567/abc.aspx", undefined);
            }, Error("data must be a non-undefined object, but was: undefined"));
        });
        test("When post is called with valid parameters and the server returns 4XX, the promise should be rejected with the request error object", function (assert) {
            var done = assert.async();
            var responseJson = {
                error: "Unexpected response status",
                statusCode: 404,
                responseHeaders: {}
            };
            var expected = {
                error: responseJson.error,
                statusCode: responseJson.statusCode,
                responseHeaders: responseJson.responseHeaders,
                response: JSON.stringify(responseJson),
                timeout: 30000
            };
            _this.server.respondWith([expected.statusCode, expected.responseHeaders, expected.response]);
            http_1.Http.post("https://not-exist.xyz/123/4567/abc.aspx", "DATA").then(function (responsePackage) {
                ok(false, "resolve should not be called");
            }, function (error) {
                deepEqual(error, expected, "The error object should be rejected");
            }).then(function () {
                done();
            });
        });
        test("When post is called with valid parameters and the server returns 5XX, the promise should be rejected with the request error object", function (assert) {
            var done = assert.async();
            var responseJson = {
                error: "Unexpected response status",
                statusCode: 500,
                responseHeaders: {}
            };
            var expected = {
                error: responseJson.error,
                statusCode: responseJson.statusCode,
                responseHeaders: responseJson.responseHeaders,
                response: JSON.stringify(responseJson),
                timeout: 30000
            };
            _this.server.respondWith([expected.statusCode, expected.responseHeaders, expected.response]);
            http_1.Http.post("https://not-exist.xyz/123/4567/abc.aspx", "DATA").then(function (responsePackage) {
                ok(false, "resolve should not be called");
            }, function (error) {
                deepEqual(error, expected, "The error object should be rejected");
            }).then(function () {
                done();
            });
        });
    };
    return HttpTests;
}(testModule_1.TestModule));
exports.HttpTests = HttpTests;
(new HttpTests()).runTests();
