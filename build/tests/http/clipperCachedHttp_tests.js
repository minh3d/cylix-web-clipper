"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var sinon = require("sinon");
var clipperCachedHttp_1 = require("../../scripts/http/clipperCachedHttp");
var Log = require("../../scripts/logging/log");
var mockLogger_1 = require("../logging/mockLogger");
var mockStorage_1 = require("../storage/mockStorage");
var testModule_1 = require("../testModule");
var ClipperCachedHttpTests = (function (_super) {
    __extends(ClipperCachedHttpTests, _super);
    function ClipperCachedHttpTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ClipperCachedHttpTests.prototype.module = function () {
        return "clipperCachedHttp";
    };
    ClipperCachedHttpTests.prototype.beforeEach = function () {
        this.mockStorage = new mockStorage_1.MockStorage();
        this.stubLogger = sinon.createStubInstance(mockLogger_1.MockLogger);
    };
    ClipperCachedHttpTests.prototype.tests = function () {
        var _this = this;
        test("When getFreshValue is called with an undefined key, a failure should be logged", function () {
            var clipperCachedHttp = new clipperCachedHttp_1.ClipperCachedHttp(_this.mockStorage, _this.stubLogger);
            clipperCachedHttp.getFreshValue(undefined, mockStorage_1.MockStorage.fetchNonLocalData);
            var logFailureSpy = _this.stubLogger.logFailure;
            ok(logFailureSpy.calledOnce, "logFailure should be called");
            ok(logFailureSpy.calledWith(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected), "logFailure should be called as an unexpected failure caused by an invalid argument");
        });
        test("When getFreshValue is called with an empty key, a failure should be logged", function () {
            var clipperCachedHttp = new clipperCachedHttp_1.ClipperCachedHttp(_this.mockStorage, _this.stubLogger);
            clipperCachedHttp.getFreshValue("", mockStorage_1.MockStorage.fetchNonLocalData);
            var logFailureSpy = _this.stubLogger.logFailure;
            ok(logFailureSpy.calledOnce, "logFailure should be called");
            ok(logFailureSpy.calledWith(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected), "logFailure should be called as an unexpected failure caused by an invalid argument");
        });
        test("When getFreshValue is called with an undefined fetchNonLocalData parameter, a failure should be logged", function () {
            var clipperCachedHttp = new clipperCachedHttp_1.ClipperCachedHttp(_this.mockStorage, _this.stubLogger);
            clipperCachedHttp.getFreshValue("k", undefined);
            var logFailureSpy = _this.stubLogger.logFailure;
            ok(logFailureSpy.calledOnce, "logFailure should be called");
            ok(logFailureSpy.calledWith(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected), "logFailure should be called as an unexpected failure caused by an invalid argument");
        });
        test("When getFreshValue is called with an updateInterval of less than 0, a failure should be logged", function () {
            var clipperCachedHttp = new clipperCachedHttp_1.ClipperCachedHttp(_this.mockStorage, _this.stubLogger);
            clipperCachedHttp.getFreshValue("k", mockStorage_1.MockStorage.fetchNonLocalData, -1);
            var logFailureSpy = _this.stubLogger.logFailure;
            ok(logFailureSpy.calledOnce, "logFailure should be called");
            ok(logFailureSpy.calledWith(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected), "logFailure should be called as an unexpected failure caused by an invalid argument");
        });
        test("When getFreshValue is called with valid parameters (assuming when nothing is in storage), logEvent should be called once", function (assert) {
            var done = assert.async();
            var clipperCachedHttp = new clipperCachedHttp_1.ClipperCachedHttp(_this.mockStorage, _this.stubLogger);
            clipperCachedHttp.getFreshValue("k", mockStorage_1.MockStorage.fetchNonLocalData, 0).then(function (timeStampedData) {
                var logEventSpy = _this.stubLogger.logEvent;
                ok(logEventSpy.calledOnce, "logEvent should be called when the fetchNonLocalData function is executed");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("When getFreshValue is called with valid parameters (assuming something fresh is in storage), logEvent should not be called, as the remote function is not executed", function (assert) {
            var done = assert.async();
            var expiry = 99999999;
            var key = "k";
            var value = {
                lastUpdated: Date.now(),
                data: "{}"
            };
            _this.mockStorage.setValue(key, JSON.stringify(value));
            var clipperCachedHttp = new clipperCachedHttp_1.ClipperCachedHttp(_this.mockStorage, _this.stubLogger);
            clipperCachedHttp.getFreshValue(key, mockStorage_1.MockStorage.fetchNonLocalData, expiry).then(function (timeStampedData) {
                var logEventSpy = _this.stubLogger.logEvent;
                ok(logEventSpy.notCalled, "logEvent should not be called as the fetchNonLocalData function was not executed");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
    };
    return ClipperCachedHttpTests;
}(testModule_1.TestModule));
exports.ClipperCachedHttpTests = ClipperCachedHttpTests;
(new ClipperCachedHttpTests()).runTests();
