"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var communicator_1 = require("../../scripts/communicator/communicator");
var smartValue_1 = require("../../scripts/communicator/smartValue");
var testModule_1 = require("../testModule");
var mockMessageHandler_1 = require("./mockMessageHandler");
var TestConstants;
(function (TestConstants) {
    TestConstants.channel = "mockChannel";
    TestConstants.sampleFunction = "sampleFunction";
    TestConstants.sampleSmartValue = "sampleSmartValue";
})(TestConstants || (TestConstants = {}));
var CommunicatorTests = (function (_super) {
    __extends(CommunicatorTests, _super);
    function CommunicatorTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CommunicatorTests.prototype.module = function () {
        return "communicator";
    };
    CommunicatorTests.prototype.tests = function () {
        var _this = this;
        test("Calling a remote function with a data package should send it across to the other side untampered", function () {
            var comm = _this.getMockCommunicators();
            comm.alpha.registerFunction(TestConstants.sampleFunction, function (data) {
                strictEqual(data, "sample data", "Ensure remote function calls pass along data params");
            });
            comm.beta.callRemoteFunction(TestConstants.sampleFunction, { param: "sample data" });
        });
        test("callRemoteFunction should call the callback with the resolved data if it is specified", function (assert) {
            var done = assert.async();
            var comm = _this.getMockCommunicators();
            comm.alpha.registerFunction(TestConstants.sampleFunction, function () {
                return Promise.resolve("returned data");
            });
            comm.beta.callRemoteFunction(TestConstants.sampleFunction, { param: "initial data", callback: function (data) {
                    strictEqual(data, "returned data", "Ensure remote function calls pass along data params");
                    done();
                } });
        });
        test("If callRemoteFunction is called before the other side's function has been registered, it should be called when it is", function (assert) {
            var done = assert.async();
            var comm = _this.getMockCommunicators();
            comm.beta.callRemoteFunction(TestConstants.sampleFunction, { param: "initial data", callback: function (data) {
                    strictEqual(data, "returned data", "Ensure remote function calls pass along data params");
                    done();
                } });
            comm.alpha.registerFunction(TestConstants.sampleFunction, function () {
                return Promise.resolve("returned data");
            });
        });
        test("Test passing smart values across the communicator and updating them", function (assert) {
            var done = assert.async();
            var comm = _this.getMockCommunicators();
            var alphaValue = new smartValue_1.SmartValue("initial alpha");
            var betaValue = new smartValue_1.SmartValue("initial beta");
            var count = 0;
            comm.alpha.subscribeAcrossCommunicator(alphaValue, TestConstants.sampleSmartValue, function (data) {
                count++;
                if (count === 1) {
                    strictEqual(data, "initial beta");
                }
                else if (count === 2) {
                    strictEqual(data, "updated value");
                    done();
                }
            });
            comm.beta.broadcastAcrossCommunicator(betaValue, TestConstants.sampleSmartValue);
            betaValue.set("updated value");
        });
        test("Robustly test two Communicators coming online at different times and registering/calling methods", function () {
            var mock1 = new mockMessageHandler_1.MockMessageHandler();
            var mock2 = new mockMessageHandler_1.MockMessageHandler(mock1);
            mock1.setOtherSide(mock2);
            // Test initial connection from one side
            mock1.onMockMessageHook = function (dataFrom2) {
                ok(false, "We shouldn't get anything yet since the other side hasn't connected yet");
            };
            mock2.onMockMessageHook = function (dataFrom1) {
                var result = JSON.parse(dataFrom1);
                strictEqual(result.functionKey, communicator_1.Communicator.initializationKey, "Should be initializing");
                strictEqual(result.channel, "mockChannel", "Check the channel");
            };
            var comm1 = new communicator_1.Communicator(mock1, "mockChannel");
            // Test the initial connection registering a function before the other side is connected
            mock1.onMockMessageHook = function (dataFrom2) {
                ok(false, "We shouldn't get anything yet since the other side hasn't connected yet");
            };
            mock2.onMockMessageHook = function (dataFrom1) {
                ok(true, "It doesn't matter if we get anything yet");
            };
            comm1.registerFunction("functionName1", undefined);
            // Test calling a function before the other side is connected
            mock1.onMockMessageHook = function (dataFrom2) {
                ok(false, "We shouldn't get anything yet since the other side hasn't connected yet");
            };
            mock2.onMockMessageHook = function (dataFrom1) {
                ok(true, "It doesn't matter if we get anything yet");
            };
            comm1.callRemoteFunction("functionName2", { param: "data2" });
            // Test initial connection from the second side
            mock1.onMockMessageHook = function (dataFrom2) {
                var result = JSON.parse(dataFrom2);
                strictEqual(result.functionKey, communicator_1.Communicator.initializationKey, "Should be initializing");
                strictEqual(result.channel, "mockChannel", "Check the channel");
            };
            var counter = 0;
            mock2.onMockMessageHook = function (dataFrom1) {
                counter++;
                var result = JSON.parse(dataFrom1);
                if (counter === 1) {
                    strictEqual(result.functionKey, communicator_1.Communicator.acknowledgeKey, "Expecting the other side to acknoweledge the initialization");
                }
                else if (counter === 2) {
                    strictEqual(result.functionKey, communicator_1.Communicator.registrationKey, "Expecting the other side to register their function now");
                    strictEqual(result.data, "functionName1", "Check the function name");
                }
                else {
                    ok(false, "Not expecting anything else");
                }
            };
            var comm2 = new communicator_1.Communicator(mock2, "mockChannel");
            // Test calling the remote function
            mock1.onMockMessageHook = function (dataFrom2) {
                var result = JSON.parse(dataFrom2);
                strictEqual(result.functionKey, "functionName1", "Check function name");
                strictEqual(result.data, "data1", "Check we got the data");
            };
            mock2.onMockMessageHook = function (dataFrom1) {
                ok(false, "We don't expect any reposnse");
            };
            comm2.callRemoteFunction("functionName1", { param: "data1" });
            // Test finally registering the second method, and ensure the queued up call trickles through
            mock1.onMockMessageHook = function (dataFrom2) {
                var result = JSON.parse(dataFrom2);
                strictEqual(result.functionKey, communicator_1.Communicator.registrationKey, "Expecting the other side to register their function now");
                strictEqual(result.data, "functionName2", "Check the function name");
            };
            mock2.onMockMessageHook = function (dataFrom1) {
                var result = JSON.parse(dataFrom1);
                strictEqual(result.functionKey, "functionName2", "Check function name");
                strictEqual(result.data, "data2", "Check we got the data");
                ok(!result.callbackKey, "No callbackKey was sent, so there shouldn't be one");
            };
            comm2.registerFunction("functionName2", undefined);
        });
        test("Test parseMessage with malformed data", function () {
            var mockHandler = new mockMessageHandler_1.MockMessageHandler();
            var communicator = new communicator_1.Communicator(mockHandler, "myId");
            // Test sending malformed data to see if anything breaks
            communicator.parseMessage(undefined);
            communicator.parseMessage("");
            communicator.parseMessage("The quick brown fox...");
            communicator.parseMessage("{}");
            communicator.parseMessage("}^&0980@#$%^.,<>{))(}{");
            communicator.parseMessage(JSON.stringify({ foo: "bar" }));
            ok(true, "No exceptions thrown for malformed data");
        });
        test("Calling a remote function through a bad connection should throw an error and allow it to bubble", function () {
            var mock1 = new mockMessageHandler_1.MockMessageHandler();
            var mock2 = new mockMessageHandler_1.MockMessageHandler(mock1);
            mock1.setOtherSide(mock2);
            var alpha = new communicator_1.Communicator(mock1, TestConstants.channel);
            var beta = new communicator_1.Communicator(mock2, TestConstants.channel);
            alpha.registerFunction(TestConstants.sampleFunction, function (data) {
                ok(false, "The registered function should not be called");
            });
            mock2.isCorrupt = true;
            throws(function () {
                beta.callRemoteFunction(TestConstants.sampleFunction, { param: "sample data" });
            }, Error(mockMessageHandler_1.MockMessageHandler.corruptError));
        });
        test("Calling a remote function through a bad connection should call the error handler with the error object if it is set", function () {
            var mock1 = new mockMessageHandler_1.MockMessageHandler();
            var mock2 = new mockMessageHandler_1.MockMessageHandler(mock1);
            mock1.setOtherSide(mock2);
            var alpha = new communicator_1.Communicator(mock1, TestConstants.channel);
            var beta = new communicator_1.Communicator(mock2, TestConstants.channel);
            beta.setErrorHandler(function (e) {
                strictEqual(e.message, mockMessageHandler_1.MockMessageHandler.corruptError);
            });
            alpha.registerFunction(TestConstants.sampleFunction, function (data) {
                ok(false, "The registered function should not be called");
            });
            mock2.isCorrupt = true;
            beta.callRemoteFunction(TestConstants.sampleFunction, { param: "sample data" });
        });
    };
    CommunicatorTests.prototype.getMockCommunicators = function () {
        var mock1 = new mockMessageHandler_1.MockMessageHandler();
        var mock2 = new mockMessageHandler_1.MockMessageHandler(mock1);
        mock1.setOtherSide(mock2);
        return {
            alpha: new communicator_1.Communicator(mock1, TestConstants.channel),
            beta: new communicator_1.Communicator(mock2, TestConstants.channel)
        };
    };
    return CommunicatorTests;
}(testModule_1.TestModule));
exports.CommunicatorTests = CommunicatorTests;
(new CommunicatorTests()).runTests();
