"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var messageHandler_1 = require("../../scripts/communicator/messageHandler");
var MockMessageHandler = (function (_super) {
    __extends(MockMessageHandler, _super);
    function MockMessageHandler(otherSide) {
        var _this = _super.call(this) || this;
        _this.isCorrupt = false;
        _this.setOtherSide(otherSide);
        return _this;
    }
    MockMessageHandler.prototype.mockMessage = function (data) {
        this.onMockMessageHook(data);
        this.onMessageReceived(data);
    };
    MockMessageHandler.prototype.onMockMessageHook = function (data) { };
    MockMessageHandler.prototype.setOtherSide = function (otherSide) {
        this.otherSide = otherSide;
    };
    MockMessageHandler.prototype.initMessageHandler = function () { };
    MockMessageHandler.prototype.sendMessage = function (data) {
        if (this.isCorrupt) {
            throw new Error(MockMessageHandler.corruptError);
        }
        else if (this.otherSide) {
            this.otherSide.mockMessage(data);
        }
    };
    MockMessageHandler.prototype.tearDown = function () { };
    return MockMessageHandler;
}(messageHandler_1.MessageHandler));
MockMessageHandler.corruptError = "MessageHandler is corrupt!";
exports.MockMessageHandler = MockMessageHandler;
