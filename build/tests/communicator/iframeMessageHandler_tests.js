"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var iframeMessageHandler_1 = require("../../scripts/communicator/iframeMessageHandler");
var testModule_1 = require("../testModule");
// TODO: figure out a way to test passing between different windows/iframes
var IFrameMessageHandlerTests = (function (_super) {
    __extends(IFrameMessageHandlerTests, _super);
    function IFrameMessageHandlerTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    IFrameMessageHandlerTests.prototype.module = function () {
        return "iframeMessageHandler";
    };
    IFrameMessageHandlerTests.prototype.tests = function () {
        test("Test that we can send and receive a message", function (assert) {
            var done = assert.async();
            // Note: this is currently posting and recieving to itself
            var handler = new iframeMessageHandler_1.IFrameMessageHandler(function () { return window; });
            var counter = 0;
            handler.onMessageReceived = function (data) {
                counter++;
                if (counter === 1) {
                    strictEqual(data, "hi there");
                }
                else if (counter === 2) {
                    strictEqual(data, "hope you are well");
                    done();
                }
                else {
                    ok(false, "onMessageReceived was called more times than expected");
                }
            };
            handler.sendMessage("hi there");
            handler.sendMessage("hope you are well");
        });
    };
    return IFrameMessageHandlerTests;
}(testModule_1.TestModule));
exports.IFrameMessageHandlerTests = IFrameMessageHandlerTests;
(new IFrameMessageHandlerTests()).runTests();
