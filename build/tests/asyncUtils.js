"use strict";
var theRealSetTimeout;
var AsyncUtils;
(function (AsyncUtils) {
    function mockSetTimeout() {
        theRealSetTimeout = setTimeout;
        setTimeout = function (func, timeout) {
            return theRealSetTimeout(func, 0);
        };
    }
    AsyncUtils.mockSetTimeout = mockSetTimeout;
    function restoreSetTimeout() {
        setTimeout = theRealSetTimeout;
    }
    AsyncUtils.restoreSetTimeout = restoreSetTimeout;
})(AsyncUtils = exports.AsyncUtils || (exports.AsyncUtils = {}));
