"use strict";
var settings_1 = require("../scripts/settings");
var frontEndGlobals_1 = require("../scripts/clipperUI/frontEndGlobals");
var localization_1 = require("../scripts/localization/localization");
var TestModule = (function () {
    function TestModule() {
    }
    TestModule.prototype.runTests = function () {
        var beforeEach = this.beforeEach.bind(this);
        var resetFrontEndStorage = this.resetFrontEndStorage.bind(this);
        var afterEach = this.afterEach.bind(this);
        // We call the implemented beforeEach/afterEach after the common functionality
        // as it's a higher priority, and we don't want to override it
        QUnit.module(this.module(), {
            beforeEach: function () {
                resetFrontEndStorage();
                beforeEach();
            },
            afterEach: function () {
                // Unfortunately, we have some code that makes use of static, which
                // means that we might end up polluting other test modules if we don't
                // reset things. We declare these here so the developer doesn't have
                // to remember to do this in their modules.
                settings_1.Settings.setSettingsJsonForTesting(undefined);
                localization_1.Localization.setLocalizedStrings(undefined);
                afterEach();
            }
        });
        this.tests();
    };
    // Overridable
    TestModule.prototype.beforeEach = function () { };
    TestModule.prototype.afterEach = function () { };
    TestModule.prototype.resetFrontEndStorage = function () {
        var mockStorageRef = {};
        var mockStorageCacheRef = {};
        frontEndGlobals_1.Clipper.getStoredValue = function (key, callback, cacheValue) {
            if (cacheValue) {
                mockStorageCacheRef[key] = mockStorageRef[key];
            }
            callback(mockStorageRef[key]);
        };
        frontEndGlobals_1.Clipper.storeValue = function (key, value) {
            if (key in mockStorageCacheRef) {
                mockStorageCacheRef[key] = value;
            }
            mockStorageRef[key] = value;
        };
        frontEndGlobals_1.Clipper.preCacheStoredValues = function (storageKeys) {
            for (var _i = 0, storageKeys_1 = storageKeys; _i < storageKeys_1.length; _i++) {
                var key = storageKeys_1[_i];
                frontEndGlobals_1.Clipper.getStoredValue(key, function () { }, true);
            }
        };
        frontEndGlobals_1.Clipper.getCachedValue = function (key) {
            return mockStorageCacheRef[key];
        };
    };
    return TestModule;
}());
exports.TestModule = TestModule;
