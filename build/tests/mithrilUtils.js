"use strict";
/**
 * Common utilities for testing in Mithril
 */
var MithrilUtils;
(function (MithrilUtils) {
    function mountToFixture(component) {
        var fixture = MithrilUtils.getFixture();
        var controllerInstance = m.mount(fixture, component);
        m.redraw(true);
        return controllerInstance;
    }
    MithrilUtils.mountToFixture = mountToFixture;
    function getFixture() {
        return document.getElementById("qunit-fixture");
    }
    MithrilUtils.getFixture = getFixture;
    function tick(clock, time) {
        clock.tick(time);
        m.redraw(true);
    }
    MithrilUtils.tick = tick;
    function simulateAction(action) {
        action();
        m.redraw(true);
    }
    MithrilUtils.simulateAction = simulateAction;
})(MithrilUtils = exports.MithrilUtils || (exports.MithrilUtils = {}));
