"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var objectUtils_1 = require("../scripts/objectUtils");
var testModule_1 = require("./testModule");
var ObjectUtilsTests = (function (_super) {
    __extends(ObjectUtilsTests, _super);
    function ObjectUtilsTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ObjectUtilsTests.prototype.module = function () {
        return "objectUtils";
    };
    ObjectUtilsTests.prototype.tests = function () {
        test("isNumeric should return false when the value is a string", function () {
            var value = "string";
            ok(!objectUtils_1.ObjectUtils.isNumeric(value), "isNumeric should return false");
        });
        test("isNumeric should return false when the value is a string numeral", function () {
            var value = "5";
            ok(!objectUtils_1.ObjectUtils.isNumeric(value), "isNumeric should return false");
        });
        test("isNumeric should return false when the value is an empty object", function () {
            var value = {};
            ok(!objectUtils_1.ObjectUtils.isNumeric(value), "isNumeric should return false");
        });
        test("isNumeric should return false when the value is a undefined", function () {
            ok(!objectUtils_1.ObjectUtils.isNumeric(undefined), "isNumeric should return false");
        });
        test("isNumeric should return false when the value is a NaN", function () {
            var value = NaN;
            ok(!objectUtils_1.ObjectUtils.isNumeric(value), "isNumeric should return false");
        });
        test("isNumeric should return false when the value is a null", function () {
            /* tslint:disable:no-null-keyword */
            var value = null;
            /* tslint:enable:no-null-keyword */
            ok(!objectUtils_1.ObjectUtils.isNumeric(value), "isNumeric should return false");
        });
        test("isNumeric should return false when the value is a string", function () {
            var value = "";
            ok(!objectUtils_1.ObjectUtils.isNumeric(value), "isNumeric should return false");
        });
        test("isNumeric should return true when the value is a number", function () {
            var value = 5;
            ok(objectUtils_1.ObjectUtils.isNumeric(value), "isNumeric should return true");
        });
        test("isNumeric should return true when the value is a negative number", function () {
            var value = -5;
            ok(objectUtils_1.ObjectUtils.isNumeric(value), "isNumeric should return true");
        });
        test("isNumeric should return true when the value is a decimal number", function () {
            var value = 4.12345;
            ok(objectUtils_1.ObjectUtils.isNumeric(value), "isNumeric should return true");
        });
        test("isNumeric should return true when the value is a Infinity", function () {
            var value = Infinity;
            ok(objectUtils_1.ObjectUtils.isNumeric(value), "isNumeric should return true");
        });
    };
    return ObjectUtilsTests;
}(testModule_1.TestModule));
exports.ObjectUtilsTests = ObjectUtilsTests;
(new ObjectUtilsTests()).runTests();
