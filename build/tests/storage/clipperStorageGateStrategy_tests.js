"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var clipperStorageGateStrategy_1 = require("../../scripts/storage/clipperStorageGateStrategy");
var clipperStorageKeys_1 = require("../../scripts/storage/clipperStorageKeys");
var testModule_1 = require("../testModule");
var mockStorage_1 = require("./mockStorage");
var ClipperStorageGateStrategyTests = (function (_super) {
    __extends(ClipperStorageGateStrategyTests, _super);
    function ClipperStorageGateStrategyTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ClipperStorageGateStrategyTests.prototype.module = function () {
        return "clipperStorageGateStrategy";
    };
    ClipperStorageGateStrategyTests.prototype.beforeEach = function () {
        this.mockStorage = new mockStorage_1.MockStorage();
        this.strategy = new clipperStorageGateStrategy_1.ClipperStorageGateStrategy(this.mockStorage);
    };
    ClipperStorageGateStrategyTests.prototype.tests = function () {
        var _this = this;
        test("shouldSet to return true if notebooks is to be set and userInformation exists in storage", function () {
            _this.mockStorage.setValue(clipperStorageKeys_1.ClipperStorageKeys.userInformation, "{}");
            ok(_this.strategy.shouldSet(clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks, "{}"), "Setting of cached notebooks should be allowed if userInformation exists");
        });
        test("shouldSet to return false if notebooks is to be set and userInformation does not exist in storage", function () {
            ok(!_this.strategy.shouldSet(clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks, "{}"), "Setting of cached notebooks should not be allowed if userInformation does not exist");
        });
        test("shouldSet to return true if undefined or empty string notebooks is to be set and userInformation does not exist in storage", function () {
            ok(_this.strategy.shouldSet(clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks, undefined), "Setting of cached notebooks should not be allowed if value is undefined");
            ok(_this.strategy.shouldSet(clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks, ""), "Setting of cached notebooks should not be allowed if value is empty string");
        });
        test("shouldSet to return true if current section is to be set and userInformation exists in storage", function () {
            _this.mockStorage.setValue(clipperStorageKeys_1.ClipperStorageKeys.userInformation, "{}");
            ok(_this.strategy.shouldSet(clipperStorageKeys_1.ClipperStorageKeys.currentSelectedSection, "{}"), "Setting of cached current section should be allowed if userInformation does not exist");
        });
        test("shouldSet to return false if current section is to be set and userInformation does not exist in storage", function () {
            ok(!_this.strategy.shouldSet(clipperStorageKeys_1.ClipperStorageKeys.currentSelectedSection, "{}"), "Setting of cached current section should not be allowed if userInformation does not exist");
        });
        test("shouldSet to return true if undefined or empty string current section is to be set and userInformation does not exist in storage", function () {
            ok(_this.strategy.shouldSet(clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks, undefined), "Setting of cached current section should be allowed if value is undefined");
            ok(_this.strategy.shouldSet(clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks, ""), "Setting of cached current section should be allowed if value is empty string");
        });
        test("shouldSet to return true for non-personal-items even if userInformation does not exist", function () {
            ok(_this.strategy.shouldSet(clipperStorageKeys_1.ClipperStorageKeys.locStrings, "{}"), "Setting of non-personal information should always be allowed");
        });
    };
    return ClipperStorageGateStrategyTests;
}(testModule_1.TestModule));
exports.ClipperStorageGateStrategyTests = ClipperStorageGateStrategyTests;
(new ClipperStorageGateStrategyTests()).runTests();
