"use strict";
var MockStorage = (function () {
    function MockStorage() {
        this.storedValues = {};
        // We set lastUpdated this way so that our tests are consistent
        var existingValue = { data: MockStorage.localData, lastUpdated: Date.now() - 1 };
        this.setValue(MockStorage.existingKey, JSON.stringify(existingValue));
    }
    MockStorage.prototype.getValue = function (key) {
        return this.storedValues[key];
    };
    MockStorage.prototype.getValues = function (keys) {
        var values = {};
        for (var i = 0; i < keys.length; i++) {
            values[keys[i]] = this.storedValues[keys[i]];
        }
        return values;
    };
    MockStorage.prototype.setValue = function (key, value) {
        this.storedValues[key] = value;
    };
    MockStorage.prototype.removeKey = function (key) {
        this.storedValues[key] = undefined;
        return true;
    };
    return MockStorage;
}());
MockStorage.existingKey = "exist";
MockStorage.nonLocalData = { myKey: "nonLocal" };
MockStorage.localData = { myKey: "local" };
MockStorage.fetchNonLocalData = function () {
    return new Promise(function (resolve, reject) {
        resolve({ parsedResponse: JSON.stringify(MockStorage.nonLocalData), request: undefined });
    });
};
exports.MockStorage = MockStorage;
