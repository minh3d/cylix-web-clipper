"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var clipperData_1 = require("../../scripts/storage/clipperData");
var clipperStorageKeys_1 = require("../../scripts/storage/clipperStorageKeys");
var testModule_1 = require("../testModule");
var mockStorage_1 = require("./mockStorage");
var ClipperDataTests = (function (_super) {
    __extends(ClipperDataTests, _super);
    function ClipperDataTests() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ClipperDataTests.prototype.module = function () {
        return "clipperData";
    };
    ClipperDataTests.prototype.beforeEach = function () {
        this.mockStorage = new mockStorage_1.MockStorage();
    };
    ClipperDataTests.prototype.tests = function () {
        var _this = this;
        test("getFreshValue should not store anything if it's retrieving notebooks and no userInformation is in storage", function (assert) {
            var done = assert.async();
            var key = clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks;
            var parsedResponse = {};
            var expectedTimeStampedData = {
                parsedResponse: JSON.stringify(parsedResponse),
                request: undefined
            };
            var getRemoteValue = function () {
                return Promise.resolve(expectedTimeStampedData);
            };
            var clipperData = new clipperData_1.ClipperData(_this.mockStorage);
            clipperData.getFreshValue(key, getRemoteValue, 0).then(function (timeStampedData) {
                strictEqual(_this.mockStorage.getValue(key), undefined, "Notebooks should not be cached if userInformation does not exist in storage");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("getFreshValue should store notebooks if it's retrieving notebooks and userInformation is in storage", function (assert) {
            var done = assert.async();
            var key = clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks;
            var parsedResponse = {};
            var expectedTimeStampedData = {
                parsedResponse: JSON.stringify(parsedResponse),
                request: undefined
            };
            var getRemoteValue = function () {
                return Promise.resolve(expectedTimeStampedData);
            };
            _this.mockStorage.setValue(clipperStorageKeys_1.ClipperStorageKeys.userInformation, "{ name: Leeeeeroy }");
            var clipperData = new clipperData_1.ClipperData(_this.mockStorage);
            clipperData.getFreshValue(key, getRemoteValue, 0).then(function (timeStampedData) {
                var actualStored = JSON.parse(_this.mockStorage.getValue(key));
                deepEqual(actualStored.data, {}, "Notebooks should be cached if userInformation exists in storage");
            }, function (error) {
                ok(false, "reject should not be called");
            }).then(function () {
                done();
            });
        });
        test("getFreshValue should not store anything if it's setting notebooks and no userInformation is in storage", function () {
            var key = clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks;
            var expectedTimeStampedData = JSON.stringify({
                parsedResponse: "{ notebooks: {} }",
                request: undefined
            });
            var clipperData = new clipperData_1.ClipperData(_this.mockStorage);
            clipperData.setValue(key, expectedTimeStampedData);
            strictEqual(_this.mockStorage.getValue(key), undefined, "Notebooks should not be stored if userInformation does not exist in storage");
        });
        test("getFreshValue should store notebooks if it's setting notebooks and userInformation is in storage", function () {
            var key = clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks;
            var expectedTimeStampedData = JSON.stringify({
                parsedResponse: "{ notebooks: {} }",
                request: undefined
            });
            _this.mockStorage.setValue(clipperStorageKeys_1.ClipperStorageKeys.userInformation, "{ name: Leeeeeroy }");
            var clipperData = new clipperData_1.ClipperData(_this.mockStorage);
            clipperData.setValue(key, expectedTimeStampedData);
            strictEqual(_this.mockStorage.getValue(key), expectedTimeStampedData, "Notebooks should be stored if userInformation exists in storage");
        });
        test("getFreshValue should not store anything if it's setting current section and no userInformation is in storage", function () {
            var key = clipperStorageKeys_1.ClipperStorageKeys.currentSelectedSection;
            var expectedTimeStampedData = JSON.stringify({
                parsedResponse: "{ section: {} }",
                request: undefined
            });
            var clipperData = new clipperData_1.ClipperData(_this.mockStorage);
            clipperData.setValue(key, expectedTimeStampedData);
            strictEqual(_this.mockStorage.getValue(key), undefined, "Current section should not be stored if userInformation does not exist in storage");
        });
        test("getFreshValue should store notebooks if it's setting current section and userInformation is in storage", function () {
            var key = clipperStorageKeys_1.ClipperStorageKeys.currentSelectedSection;
            var expectedTimeStampedData = JSON.stringify({
                parsedResponse: "{ section: {} }",
                request: undefined
            });
            _this.mockStorage.setValue(clipperStorageKeys_1.ClipperStorageKeys.userInformation, "{ name: Leeeeeroy }");
            var clipperData = new clipperData_1.ClipperData(_this.mockStorage);
            clipperData.setValue(key, expectedTimeStampedData);
            strictEqual(_this.mockStorage.getValue(key), expectedTimeStampedData, "Current section should be stored if userInformation exists in storage");
        });
    };
    return ClipperDataTests;
}(testModule_1.TestModule));
exports.ClipperDataTests = ClipperDataTests;
(new ClipperDataTests()).runTests();
