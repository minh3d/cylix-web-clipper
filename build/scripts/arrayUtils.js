"use strict";
var ArrayUtils;
(function (ArrayUtils) {
    /**
     * Given a specified number of items, returns a array of bucket counts where buckets
     * are as evenly divided as possible, and every bucket contains no more than maxPerBucket
     * items.
     */
    function createEvenBuckets(numItems, maxPerBucket) {
        if (numItems < 1) {
            return [];
        }
        if (maxPerBucket < 1) {
            throw new Error("maxPerBucket cannot be less than 1 but was: " + maxPerBucket);
        }
        if (numItems <= maxPerBucket) {
            return [numItems];
        }
        // Calculate the smallest divisor where the largest bucket is size <= maxPerBucket
        var divisor = Math.ceil(numItems / maxPerBucket);
        var integerDivideResult = Math.floor(numItems / divisor);
        var remainder = numItems % divisor;
        var bucketCounts = [];
        for (var i = 0; i < divisor; i++) {
            // If there is a remainder, we need to distribute the extra items among the first few buckets
            bucketCounts.push(integerDivideResult + (i < remainder ? 1 : 0));
        }
        return bucketCounts;
    }
    ArrayUtils.createEvenBuckets = createEvenBuckets;
    /**
     * Given an array of items, bucket them into partitions where buckets are as evenly
     * divided as possible, and every bucket contains no more than maxPerBucket items.
     * Also retains the ordering of the items when partitions are flattened.
     */
    function partition(items, maxPerBucket) {
        if (items.length === 0) {
            return [];
        }
        var bucketCounts = ArrayUtils.createEvenBuckets(items.length, maxPerBucket);
        var partitions = [];
        var sliceStart = 0;
        for (var i = 0; i < bucketCounts.length; i++) {
            var sliceEnd = sliceStart + bucketCounts[i];
            partitions.push(items.slice(sliceStart, sliceEnd));
            sliceStart = sliceEnd;
        }
        return partitions;
    }
    ArrayUtils.partition = partition;
    /**
     * Given an array, returns true if all items are defined; false otherwise.
     */
    function isArrayComplete(arr) {
        for (var i = 0; i < arr.length; i++) {
            if (!arr[i]) {
                return false;
            }
        }
        return true;
    }
    ArrayUtils.isArrayComplete = isArrayComplete;
})(ArrayUtils = exports.ArrayUtils || (exports.ArrayUtils = {}));
