"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var domUtils_1 = require("../domParsers/domUtils");
var Log = require("../logging/log");
var constants_1 = require("../constants");
var clientType_1 = require("../clientType");
var status_1 = require("./status");
var componentBase_1 = require("./componentBase");
var frontEndGlobals_1 = require("./frontEndGlobals");
;
var RegionSelectorClass = (function (_super) {
    __extends(RegionSelectorClass, _super);
    function RegionSelectorClass(props) {
        var _this = _super.call(this, props) || this;
        _this.devicePixelRatio = 1;
        _this.resizeHandler = _this.handleResize.bind(_this);
        _this.resetState();
        window.addEventListener("resize", _this.resizeHandler);
        return _this;
    }
    RegionSelectorClass.prototype.getInitialState = function () {
        return {
            selectionInProgress: false,
            winHeight: window.innerHeight,
            winWidth: window.innerWidth
        };
    };
    RegionSelectorClass.prototype.onunload = function () {
        window.removeEventListener("resize", this.resizeHandler);
    };
    /**
     * Start the selection process over
     */
    RegionSelectorClass.prototype.resetState = function () {
        this.setState({ firstPoint: undefined, secondPoint: undefined, selectionInProgress: false });
        this.props.clipperState.setState({ regionResult: { status: status_1.Status.NotStarted, data: this.props.clipperState.regionResult.data } });
    };
    /**
     * Define the starting point for the selection
     */
    RegionSelectorClass.prototype.startSelection = function (point) {
        if (this.props.clipperState.regionResult.status !== status_1.Status.InProgress) {
            this.setState({ firstPoint: point, secondPoint: undefined, selectionInProgress: true });
            this.props.clipperState.setState({ regionResult: { status: status_1.Status.InProgress, data: this.props.clipperState.regionResult.data } });
        }
    };
    /**
     * The selection is complete
     */
    RegionSelectorClass.prototype.completeSelection = function (dataUrl) {
        var regionList = this.props.clipperState.regionResult.data;
        if (!regionList) {
            regionList = [];
        }
        regionList.push(dataUrl);
        this.props.clipperState.setState({ regionResult: { status: status_1.Status.Succeeded, data: regionList } });
    };
    /**
     * They are selecting, update the second point
     */
    RegionSelectorClass.prototype.moveSelection = function (point) {
        this.setState({ secondPoint: point });
    };
    /**
     * Define the ending point, and notify the main UI
     */
    RegionSelectorClass.prototype.stopSelection = function (point) {
        if (this.state.selectionInProgress) {
            if (!this.state.firstPoint || !this.state.secondPoint || this.state.firstPoint.x === this.state.secondPoint.x || this.state.firstPoint.y === this.state.secondPoint.y) {
                // Nothing to clip, start over
                this.resetState();
            }
            else {
                this.setState({ secondPoint: point, selectionInProgress: false });
                // Get the image immediately
                this.startRegionClip();
            }
        }
    };
    RegionSelectorClass.prototype.mouseDownHandler = function (e) {
        // Prevent default "dragging" which sometimes occurs
        e.preventDefault();
        this.startSelection({ x: e.pageX, y: e.pageY });
    };
    RegionSelectorClass.prototype.mouseMoveHandler = function (e) {
        if (this.state.selectionInProgress) {
            if (e.buttons === 0) {
                // They let go of the mouse while outside the window, stop the selection where they went out
                this.stopSelection(this.state.secondPoint);
                return;
            }
            this.moveSelection({ x: e.pageX, y: e.pageY });
        }
    };
    RegionSelectorClass.prototype.mouseUpHandler = function (e) {
        this.stopSelection({ x: e.pageX, y: e.pageY });
    };
    RegionSelectorClass.prototype.touchStartHandler = function (e) {
        var eventPoint = e.changedTouches[0];
        this.startSelection({ x: eventPoint.clientX, y: eventPoint.clientY });
    };
    RegionSelectorClass.prototype.touchMoveHandler = function (e) {
        if (this.state.selectionInProgress) {
            var eventPoint = e.changedTouches[0];
            this.moveSelection({ x: eventPoint.clientX, y: eventPoint.clientY });
            e.preventDefault();
        }
    };
    RegionSelectorClass.prototype.touchEndHandler = function (e) {
        var eventPoint = e.changedTouches[0];
        this.stopSelection({ x: eventPoint.clientX, y: eventPoint.clientY });
    };
    RegionSelectorClass.prototype.handleResize = function () {
        this.setState({ winHeight: window.innerHeight, winWidth: window.innerWidth });
    };
    /**
     * Update all of the frames and elements according to the selection
     */
    RegionSelectorClass.prototype.updateVisualElements = function () {
        var outerFrame = this.refs.outerFrame;
        if (!outerFrame) {
            return;
        }
        var xMin;
        var yMin;
        var xMax;
        var yMax;
        if (!this.state.firstPoint || !this.state.secondPoint) {
            xMin = 0;
            yMin = 0;
            xMax = 0;
            yMax = 0;
        }
        else {
            xMin = Math.min(this.state.firstPoint.x, this.state.secondPoint.x);
            yMin = Math.min(this.state.firstPoint.y, this.state.secondPoint.y);
            xMax = Math.max(this.state.firstPoint.x, this.state.secondPoint.x);
            yMax = Math.max(this.state.firstPoint.y, this.state.secondPoint.y);
            var innerFrame = this.refs.innerFrame;
            if (innerFrame) {
                // We don't worry about -1 values as they simply go offscreen neatly
                var borderWidth = 1;
                innerFrame.style.top = yMin - borderWidth + "px";
                innerFrame.style.left = xMin - borderWidth + "px";
                innerFrame.style.height = yMax - yMin + "px";
                innerFrame.style.width = xMax - xMin + "px";
            }
        }
        var winWidth = this.state.winWidth;
        var winHeight = this.state.winHeight;
        var context = outerFrame.getContext("2d");
        context.canvas.width = winWidth;
        context.canvas.height = winHeight;
        context.beginPath();
        context.fillStyle = "black";
        context.fillRect(0, 0, xMin, winHeight);
        context.fillRect(xMin, 0, xMax - xMin, yMin);
        context.fillRect(xMax, 0, winWidth - xMax, winHeight);
        context.fillRect(xMin, yMax, xMax - xMin, winHeight - yMax);
    };
    /**
     * Get the browser to capture a screenshot, and save off the portion they selected (which may be compressed until it's below
     * the maximum allowed size)
     */
    RegionSelectorClass.prototype.startRegionClip = function () {
        var _this = this;
        // Taken from https://www.kirupa.com/html5/detecting_retina_high_dpi.htm
        // We check this here so that we can log it as a custom property on the regionSelectionProcessingEvent
        var query = "(-webkit-min-device-pixel-ratio: 2), (min-device-pixel-ratio: 2), (min-resolution: 192dpi)";
        var isHighDpiScreen = matchMedia(query).matches;
        var isFirefoxWithHighDpiDisplay = this.props.clipperState.clientInfo.clipperType === clientType_1.ClientType.FirefoxExtension && isHighDpiScreen;
        // Firefox reports this value incorrectly if this iframe is hidden, so store it now since we know we're visible
        // In addition to this, Firefox currently has a bug where they are not using devicePixelRatio correctly 
        // on HighDPI screens such as Retina screens or the Surface Pro 4
        // Bug link: https://bugzilla.mozilla.org/show_bug.cgi?id=1278507 
        this.devicePixelRatio = isFirefoxWithHighDpiDisplay ? window.devicePixelRatio / 2 : window.devicePixelRatio;
        var regionSelectionProcessingEvent = new Log.Event.BaseEvent(Log.Event.Label.RegionSelectionProcessing);
        var regionSelectionCapturingEvent = new Log.Event.BaseEvent(Log.Event.Label.RegionSelectionCapturing);
        regionSelectionCapturingEvent.setCustomProperty(Log.PropertyName.Custom.Width, Math.abs(this.state.firstPoint.x - this.state.secondPoint.x));
        regionSelectionCapturingEvent.setCustomProperty(Log.PropertyName.Custom.Height, Math.abs(this.state.firstPoint.y - this.state.secondPoint.y));
        frontEndGlobals_1.Clipper.getExtensionCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.takeTabScreenshot, {
            callback: function (dataUrl) {
                frontEndGlobals_1.Clipper.logger.logEvent(regionSelectionCapturingEvent);
                _this.saveCompressedSelectionToState(dataUrl).then(function (canvas) {
                    regionSelectionProcessingEvent.setCustomProperty(Log.PropertyName.Custom.Width, canvas.width);
                    regionSelectionProcessingEvent.setCustomProperty(Log.PropertyName.Custom.Height, canvas.height);
                    regionSelectionProcessingEvent.setCustomProperty(Log.PropertyName.Custom.IsHighDpiScreen, isHighDpiScreen);
                    frontEndGlobals_1.Clipper.logger.logEvent(regionSelectionProcessingEvent);
                });
            }
        });
    };
    /**
     * Given a base image in url form, captures the sub-image defined by the state's first and second points, compresses it if
     * necessary, then saves it to state if the process was successful
     */
    RegionSelectorClass.prototype.saveCompressedSelectionToState = function (baseDataUrl) {
        var _this = this;
        return this.createSelectionAsCanvas(baseDataUrl).then(function (canvas) {
            var compressedSelection = _this.getCompressedDataUrl(canvas);
            _this.completeSelection(compressedSelection);
            return Promise.resolve(canvas);
        })["catch"](function (error) {
            frontEndGlobals_1.Clipper.logger.logFailure(Log.Failure.Label.RegionSelectionProcessing, Log.Failure.Type.Unexpected, { error: error.message });
            _this.resetState();
            return Promise.reject(error);
        });
    };
    /**
     * Given a base image in url form, creates a canvas containing the sub-image defined by the state's first and second points
     */
    RegionSelectorClass.prototype.createSelectionAsCanvas = function (baseDataUrl) {
        var _this = this;
        if (!baseDataUrl) {
            return Promise.reject(new Error("baseDataUrl should be a non-empty string, but was: " + baseDataUrl));
        }
        if (!this.state.firstPoint || !this.state.secondPoint) {
            return Promise.reject(new Error("Expected the two points to be set, but they were not"));
        }
        var devicePixelRatio = this.devicePixelRatio;
        return new Promise(function (resolve) {
            var regionSelectionLoadingEvent = new Log.Event.BaseEvent(Log.Event.Label.RegionSelectionLoading);
            var img = new Image();
            img.onload = function () {
                frontEndGlobals_1.Clipper.logger.logEvent(regionSelectionLoadingEvent);
                var xMin = Math.min(_this.state.firstPoint.x, _this.state.secondPoint.x);
                var yMin = Math.min(_this.state.firstPoint.y, _this.state.secondPoint.y);
                var xMax = Math.min(img.width, Math.max(_this.state.firstPoint.x, _this.state.secondPoint.x));
                var yMax = Math.min(img.height, Math.max(_this.state.firstPoint.y, _this.state.secondPoint.y));
                var destinationOffsetX = 0;
                var destinationOffsetY = 0;
                var width = (xMax - xMin);
                var height = (yMax - yMin);
                var sourceOffsetX = xMin * devicePixelRatio;
                var sourceOffsetY = yMin * devicePixelRatio;
                var sourceWidth = (xMax - xMin) * devicePixelRatio;
                var sourceHeight = (yMax - yMin) * devicePixelRatio;
                var canvas = document.createElement("canvas");
                canvas.width = width;
                canvas.height = height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, sourceOffsetX, sourceOffsetY, sourceWidth, sourceHeight, destinationOffsetX, destinationOffsetY, width, height);
                resolve(canvas);
            };
            img.src = baseDataUrl;
        });
    };
    /*
     * Converts the canvas to a Base64 encoded URI, compressing it by lowering its quality if above the maxBytes threshold
     */
    RegionSelectorClass.prototype.getCompressedDataUrl = function (node) {
        var compressEvent = new Log.Event.BaseEvent(Log.Event.Label.CompressRegionSelection);
        var canvas = node;
        // First, see if the best quality PNG will work.
        var dataUrl = canvas.toDataURL("image/png");
        compressEvent.setCustomProperty(Log.PropertyName.Custom.InitialDataUrlLength, dataUrl.length);
        dataUrl = domUtils_1.DomUtils.adjustImageQualityIfNecessary(canvas, dataUrl);
        compressEvent.setCustomProperty(Log.PropertyName.Custom.FinalDataUrlLength, dataUrl.length);
        frontEndGlobals_1.Clipper.logger.logEvent(compressEvent);
        return dataUrl;
    };
    RegionSelectorClass.prototype.getInnerFrame = function () {
        if (this.state.secondPoint) {
            return {tag: "div", attrs: Object.assign({id:constants_1.Constants.Ids.innerFrame},  this.ref(constants_1.Constants.Ids.innerFrame))};
        }
        return undefined;
    };
    RegionSelectorClass.prototype.render = function () {
        var innerFrameElement = this.getInnerFrame();
        return ({tag: "div", attrs: {config:this.updateVisualElements.bind(this), id:constants_1.Constants.Ids.regionSelectorContainer, onmousedown:this.mouseDownHandler.bind(this), onmousemove:this.mouseMoveHandler.bind(this), onmouseup:this.mouseUpHandler.bind(this), ontouchstart:this.touchStartHandler.bind(this), ontouchmove:this.touchMoveHandler.bind(this), ontouchend:this.touchEndHandler.bind(this)}, children: [
				{tag: "canvas", attrs: Object.assign({id:constants_1.Constants.Ids.outerFrame},  this.ref(constants_1.Constants.Ids.outerFrame))}, 
				innerFrameElement
			]});
    };
    return RegionSelectorClass;
}(componentBase_1.ComponentBase));
var component = RegionSelectorClass.componentize();
exports.RegionSelector = component;
