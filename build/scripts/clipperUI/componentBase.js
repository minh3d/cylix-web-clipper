"use strict";
var constants_1 = require("../constants");
var frontEndGlobals_1 = require("./frontEndGlobals");
var ComponentBase = (function () {
    function ComponentBase(props) {
        this.props = props;
        this.state = this.getInitialState();
        this.refs = {};
    }
    ComponentBase.prototype.getInitialState = function () {
        return {};
    };
    ComponentBase.prototype.setState = function (newPartialState) {
        m.startComputation();
        for (var key in newPartialState) {
            if (newPartialState.hasOwnProperty(key)) {
                this.state[key] = newPartialState[key];
            }
        }
        m.endComputation();
    };
    ComponentBase.prototype.ref = function (name) {
        var _this = this;
        return {
            config: function (element) {
                _this.refs[name] = element;
            }
        };
    };
    ComponentBase.prototype.onElementDraw = function (handleMethod) {
        // Because of the way mithril does the callbacks, we need to rescope it so that "this" points to the class
        handleMethod = handleMethod.bind(this);
        return {
            config: function (element, isInitialized) {
                handleMethod(element, !isInitialized);
            }
        };
    };
    ComponentBase.prototype.onElementFirstDraw = function (handleMethod) {
        // Because of the way mithril does the callbacks, we need to rescope it so that "this" points to the class
        handleMethod = handleMethod.bind(this);
        return {
            config: function (element, isInitialized) {
                if (!isInitialized) {
                    handleMethod(element);
                }
            }
        };
    };
    /*
     * Helper which handles tabIndex, clicks, and keyboard navigation for a component that is part of an Aria Set
    *
     * Also hides the outline if they are using a mouse, but shows it if they are using the keyboard
     * (idea from http://www.paciellogroup.com/blog/2012/04/how-to-remove-css-outlines-in-an-accessible-manner/)
     */
    ComponentBase.prototype.enableAriaInvoke = function (_a) {
        var callback = _a.callback, tabIndex = _a.tabIndex, args = _a.args, idOverride = _a.idOverride, ariaSetName = _a.ariaSetName, _b = _a.autoSelect, autoSelect = _b === void 0 ? false : _b;
        if (callback) {
            callback = callback.bind(this, args);
        }
        var invokeAttributes = this.enableInvoke({ callback: callback, tabIndex: tabIndex, args: args, idOverride: idOverride });
        var oldKeyUp = invokeAttributes.onkeyup;
        invokeAttributes.onkeyup = function (e) {
            var currentTargetElement = e.currentTarget;
            oldKeyUp(e);
            if (e.which === constants_1.Constants.KeyCodes.home) {
                var firstInSet = 1;
                ComponentBase.focusOnButton(ariaSetName, firstInSet, autoSelect);
            }
            else if (e.which === constants_1.Constants.KeyCodes.end) {
                var lastInSet = parseInt(currentTargetElement.getAttribute("aria-setsize"), 10);
                ComponentBase.focusOnButton(ariaSetName, lastInSet, autoSelect);
            }
            var posInSet = parseInt(currentTargetElement.getAttribute("aria-posinset"), 10);
            if (e.which === constants_1.Constants.KeyCodes.up || e.which === constants_1.Constants.KeyCodes.left) {
                if (posInSet <= 1) {
                    return;
                }
                var nextPosInSet = posInSet - 1;
                ComponentBase.focusOnButton(ariaSetName, nextPosInSet, autoSelect);
            }
            else if (e.which === constants_1.Constants.KeyCodes.down || e.which === constants_1.Constants.KeyCodes.right) {
                var setSize = parseInt(currentTargetElement.getAttribute("aria-setsize"), 10);
                if (posInSet >= setSize) {
                    return;
                }
                var nextPosInSet = posInSet + 1;
                ComponentBase.focusOnButton(ariaSetName, nextPosInSet, autoSelect);
            }
        };
        invokeAttributes["data-" + constants_1.Constants.CustomHtmlAttributes.setNameForArrowKeyNav] = ariaSetName;
        return invokeAttributes;
    };
    /*
     * Helper which handles tabIndex, clicks, and keyboard navigation.
     *
     * Also hides the outline if they are using a mouse, but shows it if they are using the keyboard
     * (idea from http://www.paciellogroup.com/blog/2012/04/how-to-remove-css-outlines-in-an-accessible-manner/)
     *
     * Example use:
     *      <a id="myCoolButton" {...this.enableInvoke(this.myButtonHandler, 0)}>Click Me</a>
     */
    ComponentBase.prototype.enableInvoke = function (_a) {
        var callback = _a.callback, tabIndex = _a.tabIndex, args = _a.args, idOverride = _a.idOverride;
        // Because of the way mithril does the callbacks, we need to rescope it so that "this" points to the class
        if (callback) {
            callback = callback.bind(this, args);
        }
        return {
            onclick: function (e) {
                var element = e.currentTarget;
                ComponentBase.triggerSelection(element, idOverride, callback, e);
            },
            onkeyup: function (e) {
                var element = e.currentTarget;
                if (e.which === constants_1.Constants.KeyCodes.enter || e.which === constants_1.Constants.KeyCodes.space) {
                    // Hitting Enter on <a> tags that contains an href automatically fire the click event, so don't do it again
                    if (!(element.tagName === "A" && element.hasAttribute("href"))) {
                        ComponentBase.triggerSelection(element, undefined, callback, e);
                    }
                }
                else if (e.which === constants_1.Constants.KeyCodes.tab) {
                    // Since they are using the keyboard, revert to the default value of the outline so it is visible
                    element.style.outlineStyle = "";
                }
            },
            onkeydown: function (e) {
                if (e.which === constants_1.Constants.KeyCodes.space || e.which === constants_1.Constants.KeyCodes.up
                    || e.which === constants_1.Constants.KeyCodes.down || e.which === constants_1.Constants.KeyCodes.left
                    || e.which === constants_1.Constants.KeyCodes.right || e.which === constants_1.Constants.KeyCodes.home
                    || e.which === constants_1.Constants.KeyCodes.end) {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                }
            },
            onmousedown: function (e) {
                var element = e.currentTarget;
                element.style.outlineStyle = "none";
            },
            tabIndex: tabIndex
        };
    };
    ComponentBase.triggerSelection = function (element, idOverride, callback, e) {
        // Intentionally sending click event before handling the method
        // TODO replace this comment with a test that validates the call order is correct
        var id = idOverride ? idOverride : element.id;
        frontEndGlobals_1.Clipper.logger.logClickEvent(id);
        if (callback) {
            callback(e);
        }
    };
    ComponentBase.focusOnButton = function (setNameForArrowKeyNav, posInSet, autoSelect) {
        var buttons = document.querySelectorAll("[data-" + constants_1.Constants.CustomHtmlAttributes.setNameForArrowKeyNav + "=" + setNameForArrowKeyNav + "]");
        for (var i = 0; i < buttons.length; i++) {
            var selectable = buttons[i];
            var ariaIntForEach = parseInt(selectable.getAttribute("aria-posinset"), 10);
            if (ariaIntForEach === posInSet) {
                selectable.style.outlineStyle = "";
                autoSelect ? selectable.click() : selectable.focus();
                return;
            }
        }
    };
    // Note: currently all components NEED either a child or attribute to work with the MSX transformer.
    // This <MyButton/> won't work, but this <MyButton dummyProp /> will work.
    ComponentBase.componentize = function () {
        var _this = this;
        var returnValue = function () {
        };
        returnValue.controller = function (props) {
            // Instantiate an instance of the inheriting class
            return new _this(props);
        };
        returnValue.view = function (controller, props) {
            controller.props = props;
            return controller.render();
        };
        return returnValue;
    };
    return ComponentBase;
}());
exports.ComponentBase = ComponentBase;
