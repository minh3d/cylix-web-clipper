"use strict";
var ClipMode;
(function (ClipMode) {
    ClipMode[ClipMode["Augmentation"] = 0] = "Augmentation";
    ClipMode[ClipMode["Bookmark"] = 1] = "Bookmark";
    ClipMode[ClipMode["FullPage"] = 2] = "FullPage";
    ClipMode[ClipMode["Pdf"] = 3] = "Pdf";
    ClipMode[ClipMode["Region"] = 4] = "Region";
    ClipMode[ClipMode["Selection"] = 5] = "Selection";
})(ClipMode = exports.ClipMode || (exports.ClipMode = {}));
