"use strict";
var Status;
(function (Status) {
    Status[Status["NotStarted"] = 0] = "NotStarted";
    Status[Status["InProgress"] = 1] = "InProgress";
    Status[Status["Succeeded"] = 2] = "Succeeded";
    Status[Status["Failed"] = 3] = "Failed";
})(Status = exports.Status || (exports.Status = {}));
