"use strict";
var objectUtils_1 = require("../objectUtils");
var clipMode_1 = require("./clipMode");
var status_1 = require("./status");
var ClipperStateUtilities;
(function (ClipperStateUtilities) {
    function isUserLoggedIn(state) {
        return (state.userResult && state.userResult.status && state.userResult.data && !!state.userResult.data.user);
    }
    ClipperStateUtilities.isUserLoggedIn = isUserLoggedIn;
    function clipButtonEnabled(clipperState) {
        var currentMode = clipperState.currentMode.get();
        switch (currentMode) {
            case clipMode_1.ClipMode.Pdf:
                if (!clipperState.pdfPreviewInfo.isLocalFileAndNotAllowed) {
                    return false;
                }
                else if (clipperState.pdfResult.status !== status_1.Status.Succeeded) {
                    return false;
                }
                else if (clipperState.pdfPreviewInfo.allPages) {
                    return true;
                }
                else if (!clipperState.pdfPreviewInfo.allPages && objectUtils_1.ObjectUtils.isNullOrUndefined(clipperState.pdfPreviewInfo.selectedPageRange)) {
                    return false;
                }
                // If the user has an invalidPageRange, the clipButton is still enabled,
                // but when the user clips, we short circuit it and display a message instead
                return true;
            case clipMode_1.ClipMode.FullPage:
                // In the past, we used to allow clips while this is pending, however, we found some pages can't be clipped in full page mode	
                var fullPageScreenshotResult = clipperState.fullPageResult;
                return fullPageScreenshotResult.status === status_1.Status.Succeeded;
            case clipMode_1.ClipMode.Region:
                var regionResult = clipperState.regionResult;
                return regionResult.status === status_1.Status.Succeeded && regionResult.data && regionResult.data.length > 0;
            case clipMode_1.ClipMode.Augmentation:
                var augmentationResult = clipperState.augmentationResult;
                return augmentationResult.status === status_1.Status.Succeeded && augmentationResult.data && !!augmentationResult.data.ContentInHtml;
            case clipMode_1.ClipMode.Bookmark:
                var bookmarkResult = clipperState.bookmarkResult;
                return bookmarkResult.status === status_1.Status.Succeeded;
            case clipMode_1.ClipMode.Selection:
                // The availability of this mode is passed together with the selected text, so it's always available
                return true;
            default:
                return undefined;
        }
    }
    ClipperStateUtilities.clipButtonEnabled = clipButtonEnabled;
})(ClipperStateUtilities = exports.ClipperStateUtilities || (exports.ClipperStateUtilities = {}));
