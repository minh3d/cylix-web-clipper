"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../constants");
var extensionUtils_1 = require("../extensions/extensionUtils");
var invokeSource_1 = require("../extensions/invokeSource");
var localization_1 = require("../localization/localization");
var frontEndGlobals_1 = require("./frontEndGlobals");
var componentBase_1 = require("./componentBase");
var animatedTooltip_1 = require("./animatedTooltip");
var tooltipType_1 = require("./tooltipType");
var changeLogPanel_1 = require("./panels/changeLogPanel");
var dialogPanel_1 = require("./panels/dialogPanel");
var TooltipRendererClass = (function (_super) {
    __extends(TooltipRendererClass, _super);
    function TooltipRendererClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TooltipRendererClass.prototype.getInitialState = function () {
        return {};
    };
    TooltipRendererClass.prototype.getChangeLogPanel = function () {
        var whatsNewProps = this.props.tooltipProps;
        var handleProceedToWebClipperButton = function () {
            frontEndGlobals_1.Clipper.getExtensionCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.invokeClipperFromPageNav, {
                param: invokeSource_1.InvokeSource.WhatsNewTooltip
            });
        };
        return (<div>
				<changeLogPanel_1.ChangeLogPanel updates={whatsNewProps.updates}/>
				<div className="wideButtonContainer changelog-button">
					<a id={constants_1.Constants.Ids.proceedToWebClipperButton} {...this.enableInvoke({ callback: handleProceedToWebClipperButton, tabIndex: 10 })}>
						<span className="wideButtonFont wideActionButton" style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Semibold)}>
							{localization_1.Localization.getLocalizedString("WebClipper.Label.ProceedToWebClipperFun")}
						</span>
					</a>
				</div>
			</div>);
    };
    TooltipRendererClass.prototype.getContentClasses = function () {
        var currentPanel = this.getCurrentPanelType();
        var classes = [];
        switch (currentPanel) {
            case tooltipType_1.TooltipType.Pdf:
            /* falls through */
            case tooltipType_1.TooltipType.Product:
            /* falls through */
            case tooltipType_1.TooltipType.Recipe:
            /* falls through */
            case tooltipType_1.TooltipType.Video:
                classes.push("tooltip-upsell");
                return classes;
            case tooltipType_1.TooltipType.ChangeLog:
                classes.push("changelog-content");
                return classes;
            default:
                return classes;
        }
    };
    TooltipRendererClass.prototype.getCurrentPanelType = function () {
        return this.state.hasOwnProperty("tooltipToRenderOverride") ? this.state.tooltipToRenderOverride : this.props.tooltipToRender;
    };
    TooltipRendererClass.prototype.getCurrentTitle = function () {
        var currentPanel = this.getCurrentPanelType();
        switch (currentPanel) {
            case tooltipType_1.TooltipType.ChangeLog:
                return localization_1.Localization.getLocalizedString("WebClipper.Label.WhatsNew");
            default:
                return "";
        }
    };
    TooltipRendererClass.prototype.getTooltipPanel = function (tooltipType) {
        var handleProceedToWebClipperButton = function () {
            frontEndGlobals_1.Clipper.getExtensionCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.invokeClipperFromPageNav, {
                param: tooltipType_1.TooltipTypeUtils.toInvokeSource(tooltipType)
            });
        };
        var tooltipAsString = tooltipType_1.TooltipType[tooltipType];
        var tooltipImagePath = "tooltips/" + tooltipAsString + ".png";
        tooltipImagePath = tooltipImagePath.toLowerCase();
        var content = [(<img className="tooltip-image" src={extensionUtils_1.ExtensionUtils.getImageResourceUrl(tooltipImagePath)}/>)];
        var buttons = [{
                id: constants_1.Constants.Ids.proceedToWebClipperButton,
                label: localization_1.Localization.getLocalizedString("WebClipper.Label.ProceedToWebClipperFun"),
                handler: handleProceedToWebClipperButton
            }];
        var message = "WebClipper.Label." + tooltipAsString + "Tooltip";
        return <dialogPanel_1.DialogPanel message={localization_1.Localization.getLocalizedString(message)} content={content} buttons={buttons} fontFamily={localization_1.Localization.FontFamily.Semilight}/>;
    };
    TooltipRendererClass.prototype.getWhatsNewPanel = function () {
        var _this = this;
        var onShowChangeLogButton = function () {
            _this.setState({ tooltipToRenderOverride: tooltipType_1.TooltipType.ChangeLog });
        };
        var buttons = [{
                id: constants_1.Constants.Ids.checkOutWhatsNewButton,
                label: localization_1.Localization.getLocalizedString("WebClipper.Label.OpenChangeLogFromTooltip"),
                handler: onShowChangeLogButton.bind(this)
            }];
        return <dialogPanel_1.DialogPanel message={localization_1.Localization.getLocalizedString("WebClipper.Label.WebClipperWasUpdatedFun")} buttons={buttons}/>;
    };
    TooltipRendererClass.prototype.createTooltipPanelToShow = function () {
        var currentPanel = this.getCurrentPanelType();
        if (currentPanel === undefined) {
            return undefined;
        }
        switch (currentPanel) {
            case tooltipType_1.TooltipType.WhatsNew:
                return this.getWhatsNewPanel();
            case tooltipType_1.TooltipType.ChangeLog:
                return this.getChangeLogPanel();
            default:
                return this.getTooltipPanel(currentPanel);
        }
    };
    TooltipRendererClass.prototype.getBrandingImage = function () {
        var currentPanel = this.getCurrentPanelType();
        switch (currentPanel) {
            case tooltipType_1.TooltipType.ChangeLog:
                return undefined;
            default:
                return (<div id={constants_1.Constants.Ids.brandingContainer}>
						<p className="tooltip-corner-branding">
							<img src={extensionUtils_1.ExtensionUtils.getImageResourceUrl("tooltips/onenote_tooltip_branding.png")}/>
						</p>
					</div>);
        }
    };
    TooltipRendererClass.prototype.render = function () {
        return (<animatedTooltip_1.AnimatedTooltip brandingImage={this.getBrandingImage()} elementId={constants_1.Constants.Ids.pageNavAnimatedTooltip} title={this.getCurrentTitle()} onAfterCollapse={this.props.onTooltipClose} onCloseButtonHandler={this.props.onCloseButtonHandler} onHeightChange={this.props.onHeightChange} renderablePanel={this.createTooltipPanelToShow()} contentClasses={this.getContentClasses()}/>);
    };
    return TooltipRendererClass;
}(componentBase_1.ComponentBase));
var component = TooltipRendererClass.componentize();
exports.TooltipRenderer = component;
