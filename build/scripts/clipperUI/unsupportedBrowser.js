"use strict";
var constants_1 = require("../constants");
var polyfills_1 = require("../polyfills");
var extensionUtils_1 = require("../extensions/extensionUtils");
var localization_1 = require("../localization/localization");
var localizationHelper_1 = require("../localization/localizationHelper");
var status_1 = require("./status");
/**
 * A very browser-compliant class that notifies the user that their browser is unsupported
 * by the Web Clipper.
 */
var UnsupportedBrowserClass = (function () {
    function UnsupportedBrowserClass() {
        this.state = this.getInitialState();
    }
    UnsupportedBrowserClass.prototype.getInitialState = function () {
        return {
            localizedStringFetchAttemptCompleted: status_1.Status.NotStarted
        };
    };
    UnsupportedBrowserClass.prototype.setState = function (newPartialState) {
        m.startComputation();
        for (var key in newPartialState) {
            if (newPartialState.hasOwnProperty(key)) {
                this.state[key] = newPartialState[key];
            }
        }
        m.endComputation();
    };
    UnsupportedBrowserClass.componentize = function () {
        var _this = this;
        var returnValue = function () { };
        returnValue.controller = function (props) {
            return new _this(props);
        };
        returnValue.view = function (controller, props) {
            controller.props = props;
            return controller.render();
        };
        return returnValue;
    };
    UnsupportedBrowserClass.prototype.fetchLocalizedStrings = function (locale) {
        var _this = this;
        this.setState({
            localizedStringFetchAttemptCompleted: status_1.Status.InProgress
        });
        localizationHelper_1.LocalizationHelper.makeLocStringsFetchRequest(locale).then(function (responsePackage) {
            try {
                localization_1.Localization.setLocalizedStrings(JSON.parse(responsePackage.parsedResponse));
                _this.setState({
                    localizedStringFetchAttemptCompleted: status_1.Status.Succeeded
                });
            }
            catch (e) {
                _this.setState({
                    localizedStringFetchAttemptCompleted: status_1.Status.Failed
                });
            }
        })["catch"](function () {
            _this.setState({
                localizedStringFetchAttemptCompleted: status_1.Status.Failed
            });
        });
    };
    UnsupportedBrowserClass.prototype.attemptingFetchLocalizedStrings = function () {
        return this.state.localizedStringFetchAttemptCompleted === status_1.Status.NotStarted ||
            this.state.localizedStringFetchAttemptCompleted === status_1.Status.InProgress;
    };
    UnsupportedBrowserClass.prototype.render = function () {
        if (this.state.localizedStringFetchAttemptCompleted === status_1.Status.NotStarted) {
            // navigator.userLanguage is only available in IE, and Typescript will not recognize this property
            this.fetchLocalizedStrings(navigator.language || navigator.userLanguage);
        }
        // In IE8 and below, 'class' is a reserved keyword and cannot be used as a key in a JSON object
        return ({ tag: "div", attrs: { id: constants_1.Constants.Ids.unsupportedBrowserContainer }, children: [
                { tag: "div", attrs: { id: constants_1.Constants.Ids.unsupportedBrowserPanel, "class": "panelContent" }, children: [
                        { tag: "div", attrs: { className: constants_1.Constants.Classes.heightAnimator, style: "min-height: 276px; max-height: 276px;" }, children: [
                                { tag: "div", attrs: { className: constants_1.Constants.Classes.panelAnimator, style: "left: 0px; opacity: 1;" }, children: [
                                        { tag: "div", attrs: { id: constants_1.Constants.Ids.signInContainer }, children: [
                                                { tag: "div", attrs: { className: "signInPadding" }, children: [
                                                        { tag: "img", attrs: { id: constants_1.Constants.Ids.signInLogo, src: extensionUtils_1.ExtensionUtils.getImageResourceUrl("onenote_logo_clipper.png") } },
                                                        { tag: "div", attrs: { id: constants_1.Constants.Ids.signInMessageLabelContainer, "class": "messageLabelContainer" }, children: [
                                                                { tag: "span", attrs: { "class": "messageLabel", style: localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular) }, children: [
                                                                        this.attemptingFetchLocalizedStrings() ? "" : localization_1.Localization.getLocalizedString("WebClipper.Label.OneNoteClipper")
                                                                    ] }
                                                            ] },
                                                        { tag: "div", attrs: { "class": "signInDescription" }, children: [
                                                                { tag: "span", attrs: { id: constants_1.Constants.Ids.signInText, style: localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Light) }, children: [
                                                                        this.attemptingFetchLocalizedStrings() ? "" : localization_1.Localization.getLocalizedString("WebClipper.Label.UnsupportedBrowser")
                                                                    ] }
                                                            ] }
                                                    ] }
                                            ] }
                                    ] }
                            ] }
                    ] }
            ] });
    };
    return UnsupportedBrowserClass;
}());
polyfills_1.Polyfills.init();
var component = UnsupportedBrowserClass.componentize();
exports.UnsupportedBrowser = component;
m.mount(document.getElementById("clipperUIPlaceholder"), component);
