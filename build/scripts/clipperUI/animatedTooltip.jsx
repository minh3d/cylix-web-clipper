"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../constants");
var componentBase_1 = require("./componentBase");
var tooltip_1 = require("./tooltip");
var expandFromRightAnimationStrategy_1 = require("./animations/expandFromRightAnimationStrategy");
var slidingHeightAnimationStrategy_1 = require("./animations/slidingHeightAnimationStrategy");
var AnimatedTooltipClass = (function (_super) {
    __extends(AnimatedTooltipClass, _super);
    function AnimatedTooltipClass(props) {
        var _this = _super.call(this, props) || this;
        _this.tooltipAnimationStrategy = new expandFromRightAnimationStrategy_1.ExpandFromRightAnimationStrategy({
            extShouldAnimateIn: function () { return _this.state.uiExpanded; },
            extShouldAnimateOut: function () { return !_this.state.uiExpanded; },
            onAfterAnimateOut: _this.props.onAfterCollapse
        });
        _this.heightAnimationStrategy = new slidingHeightAnimationStrategy_1.SlidingHeightAnimationStrategy(_this.props.elementId, {
            onAfterHeightAnimatorDraw: _this.props.onHeightChange
        });
        return _this;
    }
    AnimatedTooltipClass.prototype.getInitialState = function () {
        return {
            uiExpanded: true
        };
    };
    AnimatedTooltipClass.prototype.closeTooltip = function () {
        this.setState({ uiExpanded: false });
        if (this.props.onCloseButtonHandler) {
            this.props.onCloseButtonHandler();
        }
    };
    AnimatedTooltipClass.prototype.onHeightAnimatorDraw = function (heightAnimator) {
        this.heightAnimationStrategy.animate(heightAnimator);
    };
    AnimatedTooltipClass.prototype.onTooltipDraw = function (tooltipElement) {
        this.tooltipAnimationStrategy.animate(tooltipElement);
    };
    AnimatedTooltipClass.prototype.render = function () {
        // We have to make the renderablePanel undefined on the collapse for the vertical shrink animation to function correctly
        var renderablePanel = (<div className={constants_1.Constants.Classes.heightAnimator + " " + constants_1.Constants.Classes.clearfix} {...this.onElementDraw(this.onHeightAnimatorDraw)}>
				{this.state.uiExpanded ? this.props.renderablePanel : undefined}
			</div>);
        return (<tooltip_1.Tooltip brandingImage={this.props.brandingImage} elementId={this.props.elementId} title={this.props.title} onCloseButtonHandler={this.closeTooltip.bind(this)} onElementDraw={this.onTooltipDraw.bind(this)} renderablePanel={renderablePanel} contentClasses={this.props.contentClasses}/>);
    };
    return AnimatedTooltipClass;
}(componentBase_1.ComponentBase));
exports.AnimatedTooltipClass = AnimatedTooltipClass;
var component = AnimatedTooltipClass.componentize();
exports.AnimatedTooltip = component;
