"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var localization_1 = require("../localization/localization");
var componentBase_1 = require("./componentBase");
var closeButton_1 = require("./components/closeButton");
/**
 * Renders content with consistent tooltip styling
 */
var TooltipClass = (function (_super) {
    __extends(TooltipClass, _super);
    function TooltipClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TooltipClass.prototype.getInitialState = function () {
        return {};
    };
    TooltipClass.prototype.render = function () {
        var additionalContentClasses = "";
        if (this.props.contentClasses) {
            additionalContentClasses = this.props.contentClasses.join(" ");
        }
        return ({tag: "div", attrs: Object.assign({id:this.props.elementId, className:"tooltip"},  this.onElementDraw(this.props.onElementDraw)), children: [
				this.props.brandingImage, 
				this.props.title ? {tag: "div", attrs: {style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Semibold), className:"tooltip-title"}, children: [this.props.title]} : undefined, 
				m.component(closeButton_1.CloseButton, {onClickHandler:this.props.onCloseButtonHandler, onClickHandlerParams:undefined}), 
				{tag: "div", attrs: {className:"tooltip-content " + additionalContentClasses}, children: [
					this.props.renderablePanel
				]}
			]});
    };
    return TooltipClass;
}(componentBase_1.ComponentBase));
exports.TooltipClass = TooltipClass;
var component = TooltipClass.componentize();
exports.Tooltip = component;
