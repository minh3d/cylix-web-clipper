"use strict";
var rtl_1 = require("../localization/rtl");
var clipperStorageKeys_1 = require("../storage/clipperStorageKeys");
/*
 * Responsible for executing locale-specific tasks before initializing and displaying
 * the Clipper.
 */
var LocaleSpecificTasks = (function () {
    function LocaleSpecificTasks() {
    }
    LocaleSpecificTasks.execute = function (locale) {
        this.appendDirectionalCssToHead(locale);
    };
    /*
     * Appends either the LTR or RTL css, whichever is suitable for the given locale
     */
    LocaleSpecificTasks.appendDirectionalCssToHead = function (locale) {
        var filenamePostfix = rtl_1.Rtl.isRtl(locale) ? "-rtl.css" : ".css";
        var cssFileNames = ["clipper", "sectionPicker"];
        for (var i = 0; i < cssFileNames.length; i++) {
            var clipperCssFilename = cssFileNames[i] + filenamePostfix;
            var clipperCssElem = document.createElement("link");
            clipperCssElem.setAttribute("rel", "stylesheet");
            clipperCssElem.setAttribute("type", "text/css");
            clipperCssElem.setAttribute("href", clipperCssFilename);
            document.getElementsByTagName("head")[0].appendChild(clipperCssElem);
        }
    };
    return LocaleSpecificTasks;
}());
exports.LocaleSpecificTasks = LocaleSpecificTasks;
var localeOverride;
try {
    localeOverride = window.localStorage.getItem(clipperStorageKeys_1.ClipperStorageKeys.displayLanguageOverride);
}
catch (e) { }
// navigator.userLanguage is only available in IE, and Typescript will not recognize this property
LocaleSpecificTasks.execute(localeOverride || navigator.language || navigator.userLanguage);
