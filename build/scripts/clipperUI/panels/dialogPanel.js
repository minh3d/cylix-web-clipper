"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var componentBase_1 = require("../componentBase");
var constants_1 = require("../../constants");
var objectUtils_1 = require("../../objectUtils");
var localization_1 = require("../../localization/localization");
var DialogPanelClass = (function (_super) {
    __extends(DialogPanelClass, _super);
    function DialogPanelClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DialogPanelClass.prototype.getExtraMessages = function () {
        return undefined;
    };
    DialogPanelClass.prototype.onPanelAnimatorDraw = function (panelAnimator) {
        if (this.props.panelAnimationStrategy) {
            this.props.panelAnimationStrategy.animate(panelAnimator);
        }
    };
    DialogPanelClass.prototype.render = function () {
        var _this = this;
        var fontFamily = !objectUtils_1.ObjectUtils.isNullOrUndefined(this.props.fontFamily) ? this.props.fontFamily : localization_1.Localization.FontFamily.Semibold;
        var buttonFontFamily = !objectUtils_1.ObjectUtils.isNullOrUndefined(this.props.buttonFontFamily) ? this.props.buttonFontFamily : localization_1.Localization.FontFamily.Semibold;
        var containerId = this.props.containerId ? this.props.containerId : "";
        return ({tag: "div", attrs: {id:containerId}, children: [
				{tag: "div", attrs: Object.assign({className:constants_1.Constants.Classes.panelAnimator},  this.onElementDraw(this.onPanelAnimatorDraw)), children: [
					{tag: "div", attrs: {id:constants_1.Constants.Ids.dialogMessageContainer, className:"resultPagePadding"}, children: [
						{tag: "div", attrs: {className:"messageLabelContainer", style:localization_1.Localization.getFontFamilyAsStyle(fontFamily)}, children: [
							{tag: "div", attrs: {id:constants_1.Constants.Ids.dialogMessage, className:"dialogMessageFont messageLabel", role:"alert"}, children: [
								this.props.message
							]}, 
							this.getExtraMessages()
						]}
					]}, 
					{tag: "div", attrs: {id:constants_1.Constants.Ids.dialogContentContainer, className:""}, children: [
						this.props.content
					]}, 
					{tag: "div", attrs: {id:constants_1.Constants.Ids.dialogButtonContainer}, children: [
						this.props.buttons.map(function (button, i) {
            return ({tag: "a", attrs: Object.assign({id:button.id, className:"dialogButton"},  _this.enableInvoke({ callback: button.handler, tabIndex: 70 })), children: [
									{tag: "div", attrs: {className:"wideButtonContainer"}, children: [
										{tag: "span", attrs: {className:"wideButtonFont wideActionButton", style:localization_1.Localization.getFontFamilyAsStyle(buttonFontFamily)}, children: [
											button.label
										]}
									]}
								]});
        })
					]}
				]}
			]});
    };
    return DialogPanelClass;
}(componentBase_1.ComponentBase));
exports.DialogPanelClass = DialogPanelClass;
var component = DialogPanelClass.componentize();
exports.DialogPanel = component;
