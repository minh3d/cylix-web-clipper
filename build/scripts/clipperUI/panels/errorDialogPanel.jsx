"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../constants");
var frontEndGlobals_1 = require("../frontEndGlobals");
var dialogPanel_1 = require("./dialogPanel");
var ErrorDialogPanelClass = (function (_super) {
    __extends(ErrorDialogPanelClass, _super);
    function ErrorDialogPanelClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // Override
    ErrorDialogPanelClass.prototype.getExtraMessages = function () {
        return (<div id={constants_1.Constants.Ids.dialogDebugMessageContainer}>
				{this.getDebugSessionId()}
			</div>);
    };
    ErrorDialogPanelClass.prototype.getDebugSessionId = function () {
        return "Usid: " + frontEndGlobals_1.Clipper.getUserSessionId();
    };
    return ErrorDialogPanelClass;
}(dialogPanel_1.DialogPanelClass));
var component = ErrorDialogPanelClass.componentize();
exports.ErrorDialogPanel = component;
