"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../constants");
var extensionUtils_1 = require("../../extensions/extensionUtils");
var componentBase_1 = require("../componentBase");
var spriteAnimation_1 = require("../components/spriteAnimation");
var LoadingPanelClass = (function (_super) {
    __extends(LoadingPanelClass, _super);
    function LoadingPanelClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LoadingPanelClass.prototype.render = function () {
        return ({tag: "div", attrs: {id:constants_1.Constants.Ids.clipperLoadingContainer, className:"progressPadding"}, children: [
				m.component(spriteAnimation_1.SpriteAnimation, {spriteUrl:extensionUtils_1.ExtensionUtils.getImageResourceUrl("spinner_loop.png"), imageHeight:32, totalFrameCount:21, loop:true})
			]});
    };
    return LoadingPanelClass;
}(componentBase_1.ComponentBase));
var component = LoadingPanelClass.componentize();
exports.LoadingPanel = component;
