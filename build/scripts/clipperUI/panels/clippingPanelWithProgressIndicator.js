"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../constants");
var objectUtils_1 = require("../../objectUtils");
var localization_1 = require("../../localization/localization");
var componentBase_1 = require("../componentBase");
var clippingPanel_1 = require("./clippingPanel");
var ClippingPanelWithProgressIndicatorClass = (function (_super) {
    __extends(ClippingPanelWithProgressIndicatorClass, _super);
    function ClippingPanelWithProgressIndicatorClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ClippingPanelWithProgressIndicatorClass.prototype.getMessageElement = function () {
        var pdfProgress = this.props.clipperState.clipSaveStatus;
        // We would rather show no message at all, than show a broken message like "Clipping page -1 of -5...""
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(pdfProgress.numItemsCompleted) || objectUtils_1.ObjectUtils.isNullOrUndefined(pdfProgress.numItemsTotal)) {
            return undefined;
        }
        else if (pdfProgress.numItemsCompleted > pdfProgress.numItemsTotal) {
            return undefined;
        }
        else if (pdfProgress.numItemsCompleted < 0 || pdfProgress.numItemsTotal < 0) {
            return undefined;
        }
        var message = localization_1.Localization.getLocalizedString("WebClipper.ClipType.Pdf.IncrementalProgressMessage").replace("{0}", (pdfProgress.numItemsCompleted + 1).toString());
        message = message.replace("{1}", pdfProgress.numItemsTotal.toString());
        return ({tag: "span", attrs: {className:"actionLabelFont messageLabel", id:constants_1.Constants.Ids.clipProgressIndicatorMessage, style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)}, children: [
				message
			]});
    };
    ClippingPanelWithProgressIndicatorClass.prototype.render = function () {
        return ({tag: "div", attrs: {}, children: [
				m.component(clippingPanel_1.ClippingPanel, {clipperState:this.props.clipperState}), 
				this.getMessageElement()
			]});
    };
    return ClippingPanelWithProgressIndicatorClass;
}(componentBase_1.ComponentBase));
var component = ClippingPanelWithProgressIndicatorClass.componentize();
exports.ClippingPanelWithProgressIndicator = component;
