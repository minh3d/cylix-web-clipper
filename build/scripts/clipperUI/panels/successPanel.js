"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../constants");
var urlUtils_1 = require("../../urlUtils");
var extensionUtils_1 = require("../../extensions/extensionUtils");
var localization_1 = require("../../localization/localization");
var Log = require("../../logging/log");
var componentBase_1 = require("../componentBase");
var frontEndGlobals_1 = require("../frontEndGlobals");
var spriteAnimation_1 = require("../components/spriteAnimation");
var SuccessPanelClass = (function (_super) {
    __extends(SuccessPanelClass, _super);
    function SuccessPanelClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SuccessPanelClass.prototype.onLaunchOneNoteButton = function () {
        frontEndGlobals_1.Clipper.logger.logUserFunnel(Log.Funnel.Label.ViewInWac);
        var data = this.props.clipperState.oneNoteApiResult.data;
        if (data && data.links && data.links.oneNoteWebUrl && data.links.oneNoteWebUrl.href) {
            var urlWithFromClipperParam = urlUtils_1.UrlUtils.addUrlQueryValue(data.links.oneNoteWebUrl.href, constants_1.Constants.Urls.QueryParams.wdFromClipper, "1");
            window.open(urlWithFromClipperParam, "_blank");
        }
        else {
            frontEndGlobals_1.Clipper.logger.logFailure(Log.Failure.Label.OnLaunchOneNoteButton, Log.Failure.Type.Unexpected, { error: "Page created and returned by API is either missing entirely, or missing its links, oneNoteWebUrl, or oneNoteWebUrl.href. Page: " + data });
        }
    };
    SuccessPanelClass.prototype.render = function () {
        return ({tag: "div", attrs: {id:constants_1.Constants.Ids.clipperSuccessContainer}, children: [
				{tag: "div", attrs: {className:"messageLabelContainer successPagePadding"}, children: [
					m.component(spriteAnimation_1.SpriteAnimation, {spriteUrl:extensionUtils_1.ExtensionUtils.getImageResourceUrl("checkmark.png"), imageHeight:28, totalFrameCount:30, loop:false}), 
					{tag: "span", attrs: {className:"actionLabelFont messageLabel", role:"alert", style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Light)}, children: [
						localization_1.Localization.getLocalizedString("WebClipper.Label.ClipSuccessful")
					]}
				]}, 
				{tag: "a", attrs: Object.assign({id:constants_1.Constants.Ids.launchOneNoteButton},  this.enableInvoke({ callback: this.onLaunchOneNoteButton, tabIndex: 70 })), children: [
					{tag: "div", attrs: {className:"wideButtonContainer"}, children: [
						{tag: "span", attrs: {className:"wideButtonFont wideActionButton", style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)}, children: [
							localization_1.Localization.getLocalizedString("WebClipper.Action.ViewInOneNote")
						]}
					]}
				]}
			]});
    };
    return SuccessPanelClass;
}(componentBase_1.ComponentBase));
var component = SuccessPanelClass.componentize();
exports.SuccessPanel = component;
