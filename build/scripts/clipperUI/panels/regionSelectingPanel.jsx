"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../constants");
var localization_1 = require("../../localization/localization");
var componentBase_1 = require("../componentBase");
var RegionSelectingPanelClass = (function (_super) {
    __extends(RegionSelectingPanelClass, _super);
    function RegionSelectingPanelClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RegionSelectingPanelClass.prototype.handleCancelButton = function () {
        this.props.clipperState.reset();
    };
    RegionSelectingPanelClass.prototype.render = function () {
        return (<div id={constants_1.Constants.Ids.regionInstructionsContainer}>
				<div className="regionClipPadding">
					<div className="messageLabelContainer">
						<div className={constants_1.Constants.Classes.srOnly} role="alert">
							{localization_1.Localization.getLocalizedString("WebClipper.Label.DragAndRelease")}
						</div>
						<span className="informationLabelFont messageLabel" style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Light)}>
							{localization_1.Localization.getLocalizedString("WebClipper.Label.DragAndRelease")}
						</span>
					</div>
					<div className="wideButtonContainer">
						<a id={constants_1.Constants.Ids.regionClipCancelButton} role="button" {...this.enableInvoke({ callback: this.handleCancelButton, tabIndex: 70 })}>
							<span className="wideButtonFont wideActionButton" style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Semibold)}>
								{localization_1.Localization.getLocalizedString("WebClipper.Action.BackToHome")}
							</span>
						</a>
					</div>
				</div>
			</div>);
    };
    return RegionSelectingPanelClass;
}(componentBase_1.ComponentBase));
var component = RegionSelectingPanelClass.componentize();
exports.RegionSelectingPanel = component;
