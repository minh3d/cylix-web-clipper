"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../constants");
var localization_1 = require("../../localization/localization");
var componentBase_1 = require("../componentBase");
var clippingPanel_1 = require("./clippingPanel");
var ClippingPanelWithDelayedMessageClass = (function (_super) {
    __extends(ClippingPanelWithDelayedMessageClass, _super);
    function ClippingPanelWithDelayedMessageClass(props) {
        var _this = _super.call(this, props) || this;
        setTimeout(function () {
            _this.setState({
                showMessage: true
            });
        }, Math.max(_this.props.delay, 0));
        return _this;
    }
    ClippingPanelWithDelayedMessageClass.prototype.getInitialState = function () {
        return {
            showMessage: false
        };
    };
    ClippingPanelWithDelayedMessageClass.prototype.getMessageElement = function () {
        return ({tag: "span", attrs: {className:"actionLabelFont messageLabel", id:constants_1.Constants.Ids.clipProgressDelayedMessage, style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)}, children: [
				this.props.message
			]});
    };
    ClippingPanelWithDelayedMessageClass.prototype.render = function () {
        return ({tag: "div", attrs: {}, children: [
				m.component(clippingPanel_1.ClippingPanel, {clipperState:this.props.clipperState}), 
				this.state.showMessage ? this.getMessageElement() : undefined
			]});
    };
    return ClippingPanelWithDelayedMessageClass;
}(componentBase_1.ComponentBase));
var component = ClippingPanelWithDelayedMessageClass.componentize();
exports.ClippingPanelWithDelayedMessage = component;
