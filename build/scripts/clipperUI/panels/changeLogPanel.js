"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../constants");
var localization_1 = require("../../localization/localization");
var componentBase_1 = require("../componentBase");
var ChangeLogPanelClass = (function (_super) {
    __extends(ChangeLogPanelClass, _super);
    function ChangeLogPanelClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ChangeLogPanelClass.prototype.getInitialState = function () {
        return {};
    };
    ChangeLogPanelClass.prototype.createChangeElement = function (change) {
        var image = change.imageUrl ?
            {tag: "img", attrs: {src:change.imageUrl}} :
            undefined;
        return ({tag: "div", attrs: {className:constants_1.Constants.Classes.change}, children: [
				image ?
            {tag: "div", attrs: {className:constants_1.Constants.Classes.changeImage}, children: [
						image
					]} :
            undefined, 
				{tag: "div", attrs: {className:constants_1.Constants.Classes.changeBody}, children: [
					{tag: "div", attrs: {className:constants_1.Constants.Classes.changeTitle}, children: [
						{tag: "span", attrs: {className:"changeTitleFont", style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Semibold)}, children: [
							change.title
						]}
					]}, 
					{tag: "div", attrs: {className:constants_1.Constants.Classes.changeDescription}, children: [
						{tag: "span", attrs: {className:"changeDescriptionFont", style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)}, children: [
							change.description
						]}
					]}
				]}
			]});
    };
    ChangeLogPanelClass.prototype.getChangeElements = function () {
        var changeElements = [];
        for (var i = 0; i < this.props.updates.length; i++) {
            for (var j = 0; j < this.props.updates[i].changes.length; j++) {
                changeElements.push(this.createChangeElement(this.props.updates[i].changes[j]));
            }
        }
        return changeElements;
    };
    ChangeLogPanelClass.prototype.render = function () {
        return ({tag: "div", attrs: {className:constants_1.Constants.Classes.changes}, children: [
				this.getChangeElements()
			]});
    };
    return ChangeLogPanelClass;
}(componentBase_1.ComponentBase));
var component = ChangeLogPanelClass.componentize();
exports.ChangeLogPanel = component;
