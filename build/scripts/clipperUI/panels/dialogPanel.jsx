"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var componentBase_1 = require("../componentBase");
var constants_1 = require("../../constants");
var objectUtils_1 = require("../../objectUtils");
var localization_1 = require("../../localization/localization");
var DialogPanelClass = (function (_super) {
    __extends(DialogPanelClass, _super);
    function DialogPanelClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DialogPanelClass.prototype.getExtraMessages = function () {
        return undefined;
    };
    DialogPanelClass.prototype.onPanelAnimatorDraw = function (panelAnimator) {
        if (this.props.panelAnimationStrategy) {
            this.props.panelAnimationStrategy.animate(panelAnimator);
        }
    };
    DialogPanelClass.prototype.render = function () {
        var _this = this;
        var fontFamily = !objectUtils_1.ObjectUtils.isNullOrUndefined(this.props.fontFamily) ? this.props.fontFamily : localization_1.Localization.FontFamily.Semibold;
        var buttonFontFamily = !objectUtils_1.ObjectUtils.isNullOrUndefined(this.props.buttonFontFamily) ? this.props.buttonFontFamily : localization_1.Localization.FontFamily.Semibold;
        var containerId = this.props.containerId ? this.props.containerId : "";
        return (<div id={containerId}>
				<div className={constants_1.Constants.Classes.panelAnimator} {...this.onElementDraw(this.onPanelAnimatorDraw)}>
					<div id={constants_1.Constants.Ids.dialogMessageContainer} className="resultPagePadding">
						<div className="messageLabelContainer" style={localization_1.Localization.getFontFamilyAsStyle(fontFamily)}>
							<div id={constants_1.Constants.Ids.dialogMessage} className="dialogMessageFont messageLabel" role="alert">
								{this.props.message}
							</div>
							{this.getExtraMessages()}
						</div>
					</div>
					<div id={constants_1.Constants.Ids.dialogContentContainer} className="">
						{this.props.content}
					</div>
					<div id={constants_1.Constants.Ids.dialogButtonContainer}>
						{this.props.buttons.map(function (button, i) {
            return (<a id={button.id} className="dialogButton" {..._this.enableInvoke({ callback: button.handler, tabIndex: 70 })}>
									<div className="wideButtonContainer">
										<span className="wideButtonFont wideActionButton" style={localization_1.Localization.getFontFamilyAsStyle(buttonFontFamily)}>
											{button.label}
										</span>
									</div>
								</a>);
        })}
					</div>
				</div>
			</div>);
    };
    return DialogPanelClass;
}(componentBase_1.ComponentBase));
exports.DialogPanelClass = DialogPanelClass;
var component = DialogPanelClass.componentize();
exports.DialogPanel = component;
