"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../constants");
var localization_1 = require("../../localization/localization");
var componentBase_1 = require("../componentBase");
var RegionSelectingPanelClass = (function (_super) {
    __extends(RegionSelectingPanelClass, _super);
    function RegionSelectingPanelClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RegionSelectingPanelClass.prototype.handleCancelButton = function () {
        this.props.clipperState.reset();
    };
    RegionSelectingPanelClass.prototype.render = function () {
        return ({tag: "div", attrs: {id:constants_1.Constants.Ids.regionInstructionsContainer}, children: [
				{tag: "div", attrs: {className:"regionClipPadding"}, children: [
					{tag: "div", attrs: {className:"messageLabelContainer"}, children: [
						{tag: "div", attrs: {className:constants_1.Constants.Classes.srOnly, role:"alert"}, children: [
							localization_1.Localization.getLocalizedString("WebClipper.Label.DragAndRelease")
						]}, 
						{tag: "span", attrs: {className:"informationLabelFont messageLabel", style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Light)}, children: [
							localization_1.Localization.getLocalizedString("WebClipper.Label.DragAndRelease")
						]}
					]}, 
					{tag: "div", attrs: {className:"wideButtonContainer"}, children: [
						{tag: "a", attrs: Object.assign({id:constants_1.Constants.Ids.regionClipCancelButton, role:"button"},  this.enableInvoke({ callback: this.handleCancelButton, tabIndex: 70 })), children: [
							{tag: "span", attrs: {className:"wideButtonFont wideActionButton", style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Semibold)}, children: [
								localization_1.Localization.getLocalizedString("WebClipper.Action.BackToHome")
							]}
						]}
					]}
				]}
			]});
    };
    return RegionSelectingPanelClass;
}(componentBase_1.ComponentBase));
var component = RegionSelectingPanelClass.componentize();
exports.RegionSelectingPanel = component;
