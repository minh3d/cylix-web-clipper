"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../constants");
var objectUtils_1 = require("../../objectUtils");
var localization_1 = require("../../localization/localization");
var clipperStorageKeys_1 = require("../../storage/clipperStorageKeys");
var componentBase_1 = require("../componentBase");
var frontEndGlobals_1 = require("../frontEndGlobals");
var ratingsHelper_1 = require("../ratingsHelper");
var slideContentInFromTopAnimationStrategy_1 = require("../animations/slideContentInFromTopAnimationStrategy");
var dialogPanel_1 = require("./dialogPanel");
var RatingsPanelClass = (function (_super) {
    __extends(RatingsPanelClass, _super);
    function RatingsPanelClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RatingsPanelClass.prototype.getInitialState = function () {
        return {
            currentRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.Init
        };
    };
    /**
     * Get the panel animation strategy for the ratings subpanel of the success panel provided
     */
    RatingsPanelClass.prototype.getPanelAnimationStrategy = function (panel) {
        if (this.props.animationState) {
            return new slideContentInFromTopAnimationStrategy_1.SlideContentInFromTopAnimationStrategy({
                currentAnimationState: this.props.animationState,
                contentToAnimate: this.getContentToAnimate(),
                extShouldAnimateIn: function () {
                    return (objectUtils_1.ObjectUtils.isNullOrUndefined(panel.state.userSelectedRatingsPromptStage) ||
                        panel.state.userSelectedRatingsPromptStage === panel.state.currentRatingsPromptStage);
                },
                extShouldAnimateOut: function () {
                    return panel.state.userSelectedRatingsPromptStage > panel.state.currentRatingsPromptStage;
                },
                onAfterAnimateOut: function () { panel.setState({ currentRatingsPromptStage: panel.state.userSelectedRatingsPromptStage }); }
            });
        }
    };
    RatingsPanelClass.prototype.getContentToAnimate = function () {
        return [
            {
                cssSelector: ".messageLabel",
                animateInOptions: {
                    slideDownDeltas: [50],
                    delaysInMs: [33]
                }
            },
            {
                cssSelector: ".dialogButton .wideButtonContainer",
                animateInOptions: {
                    slideDownDeltas: [48, 48],
                    delaysInMs: [50, 0]
                }
            }
        ];
    };
    /**
     * Get appropriate dialog panel message for the ratings prompt stage provided
     */
    RatingsPanelClass.prototype.getMessage = function (stage) {
        switch (stage) {
            case ratingsHelper_1.RatingsPromptStage.Init:
                return localization_1.Localization.getLocalizedString("WebClipper.Label.Ratings.Message.Init");
            case ratingsHelper_1.RatingsPromptStage.Rate:
                return localization_1.Localization.getLocalizedString("WebClipper.Label.Ratings.Message.Rate");
            case ratingsHelper_1.RatingsPromptStage.Feedback:
                return localization_1.Localization.getLocalizedString("WebClipper.Label.Ratings.Message.Feedback");
            case ratingsHelper_1.RatingsPromptStage.End:
                return localization_1.Localization.getLocalizedString("WebClipper.Label.Ratings.Message.End");
            default:
            case ratingsHelper_1.RatingsPromptStage.None:
                return;
        }
    };
    /**
     * Get appropriate dialog panel buttons for the panel (with its internal states) provided
     */
    RatingsPanelClass.prototype.getDialogButtons = function (panel) {
        var _this = this;
        var stage = panel.state.currentRatingsPromptStage;
        var clipperState = panel.props.clipperState;
        var clientType = clipperState.clientInfo.clipperType;
        var buttons = [];
        switch (stage) {
            case ratingsHelper_1.RatingsPromptStage.Init:
                buttons.push({
                    id: constants_1.Constants.Ids.ratingsButtonInitYes,
                    label: localization_1.Localization.getLocalizedString("WebClipper.Label.Ratings.Button.Init.Positive"),
                    handler: function () {
                        ratingsHelper_1.RatingsHelper.setDoNotPromptStatus();
                        var rateUrl = ratingsHelper_1.RatingsHelper.getRateUrlIfExists(clientType);
                        if (rateUrl) {
                            panel.setState({
                                userSelectedRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.Rate
                            });
                        }
                        else {
                            panel.setState({
                                userSelectedRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.End
                            });
                        }
                        _this.forceTransitionIfAnimationsAreOff(panel);
                    }
                }, {
                    id: constants_1.Constants.Ids.ratingsButtonInitNo,
                    label: localization_1.Localization.getLocalizedString("WebClipper.Label.Ratings.Button.Init.Negative"),
                    handler: function () {
                        if (ratingsHelper_1.RatingsHelper.badRatingAlreadyOccurred()) {
                            // setting this to prevent additional ratings prompts after the second bad rating
                            ratingsHelper_1.RatingsHelper.setDoNotPromptStatus();
                        }
                        var lastSeenVersion = frontEndGlobals_1.Clipper.getCachedValue(clipperStorageKeys_1.ClipperStorageKeys.lastSeenVersion);
                        frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingDate, Date.now().toString());
                        frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingVersion, lastSeenVersion);
                        var feedbackUrl = ratingsHelper_1.RatingsHelper.getFeedbackUrlIfExists(clipperState);
                        if (feedbackUrl) {
                            panel.setState({
                                userSelectedRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.Feedback
                            });
                        }
                        else {
                            panel.setState({
                                userSelectedRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.End
                            });
                        }
                        _this.forceTransitionIfAnimationsAreOff(panel);
                    }
                });
                break;
            case ratingsHelper_1.RatingsPromptStage.Rate:
                var rateUrl_1 = ratingsHelper_1.RatingsHelper.getRateUrlIfExists(clientType);
                if (rateUrl_1) {
                    buttons.push({
                        id: constants_1.Constants.Ids.ratingsButtonRateYes,
                        label: localization_1.Localization.getLocalizedString("WebClipper.Label.Ratings.Button.Rate"),
                        handler: function () {
                            window.open(rateUrl_1, "_blank");
                            panel.setState({
                                userSelectedRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.End
                            });
                            _this.forceTransitionIfAnimationsAreOff(panel);
                        }
                    }, {
                        id: constants_1.Constants.Ids.ratingsButtonRateNo,
                        label: localization_1.Localization.getLocalizedString("WebClipper.Label.Ratings.Button.NoThanks"),
                        handler: function () {
                            panel.setState({
                                userSelectedRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.None
                            });
                            _this.forceTransitionIfAnimationsAreOff(panel);
                        }
                    });
                }
                else {
                    // this shouldn't happen
                    panel.setState({
                        userSelectedRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.None
                    });
                }
                break;
            case ratingsHelper_1.RatingsPromptStage.Feedback:
                var feedbackUrl_1 = ratingsHelper_1.RatingsHelper.getFeedbackUrlIfExists(clipperState);
                if (feedbackUrl_1) {
                    buttons.push({
                        id: constants_1.Constants.Ids.ratingsButtonFeedbackYes,
                        label: localization_1.Localization.getLocalizedString("WebClipper.Label.Ratings.Button.Feedback"),
                        handler: function () {
                            window.open(feedbackUrl_1, "_blank");
                            panel.setState({
                                userSelectedRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.End
                            });
                            _this.forceTransitionIfAnimationsAreOff(panel);
                        }
                    }, {
                        id: constants_1.Constants.Ids.ratingsButtonFeedbackNo,
                        label: localization_1.Localization.getLocalizedString("WebClipper.Label.Ratings.Button.NoThanks"),
                        handler: function () {
                            panel.setState({
                                userSelectedRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.None
                            });
                            _this.forceTransitionIfAnimationsAreOff(panel);
                        }
                    });
                }
                else {
                    // this shouldn't happen
                    panel.setState({
                        userSelectedRatingsPromptStage: ratingsHelper_1.RatingsPromptStage.None
                    });
                }
                break;
            default:
            case ratingsHelper_1.RatingsPromptStage.End:
            case ratingsHelper_1.RatingsPromptStage.None:
                break;
        }
        return buttons;
    };
    RatingsPanelClass.prototype.forceTransitionIfAnimationsAreOff = function (panel) {
        if (!panel.props.animationState) {
            panel.setState({ currentRatingsPromptStage: panel.state.userSelectedRatingsPromptStage });
        }
    };
    RatingsPanelClass.prototype.render = function () {
        if (!this.props.clipperState.showRatingsPrompt) {
            return {tag: "div", attrs: {}};
        }
        var message = this.getMessage(this.state.currentRatingsPromptStage);
        if (!objectUtils_1.ObjectUtils.isNullOrUndefined(message)) {
            var buttons = this.getDialogButtons(this);
            var panelAnimationStrategy = this.getPanelAnimationStrategy(this);
            return (m.component(dialogPanel_1.DialogPanel, {message:message, buttons:buttons, buttonFontFamily:localization_1.Localization.FontFamily.Regular, containerId:constants_1.Constants.Ids.ratingsPromptContainer, panelAnimationStrategy:panelAnimationStrategy}));
        }
        return {tag: "div", attrs: {}};
    };
    return RatingsPanelClass;
}(componentBase_1.ComponentBase));
var component = RatingsPanelClass.componentize();
exports.RatingsPanel = component;
