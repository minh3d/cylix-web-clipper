"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var clientType_1 = require("../../clientType");
var constants_1 = require("../../constants");
var userInfo_1 = require("../../userInfo");
var extensionUtils_1 = require("../../extensions/extensionUtils");
var localization_1 = require("../../localization/localization");
var frontEndGlobals_1 = require("../frontEndGlobals");
var componentBase_1 = require("../componentBase");
var SignInPanelClass = (function (_super) {
    __extends(SignInPanelClass, _super);
    function SignInPanelClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SignInPanelClass.prototype.getInitialState = function () {
        return { debugInformationShowing: false };
    };
    SignInPanelClass.prototype.onSignInMsa = function () {
        this.props.onSignInInvoked(userInfo_1.AuthType.Msa);
    };
    SignInPanelClass.prototype.onSignInOrgId = function () {
        this.props.onSignInInvoked(userInfo_1.AuthType.OrgId);
    };
    SignInPanelClass.prototype.getSignInDescription = function () {
        // Since the user is not signed in, we show a message depending on the reason of the last update to the userResult
        if (!this.props.clipperState.userResult || !this.props.clipperState.userResult.data) {
            return localization_1.Localization.getLocalizedString("WebClipper.Label.SignInDescription");
        }
        switch (this.props.clipperState.userResult.data.updateReason) {
            case userInfo_1.UpdateReason.SignInAttempt:
                return localization_1.Localization.getLocalizedString("WebClipper.Error.SignInUnsuccessful");
            case userInfo_1.UpdateReason.TokenRefreshForPendingClip:
                return localization_1.Localization.getLocalizedString("WebClipper.Error.GenericExpiredTokenRefreshError");
            default:
            case userInfo_1.UpdateReason.SignInCancel:
            case userInfo_1.UpdateReason.SignOutAction:
            case userInfo_1.UpdateReason.InitialRetrieval:
                return localization_1.Localization.getLocalizedString("WebClipper.Label.SignInDescription");
        }
    };
    SignInPanelClass.prototype.signInAttempted = function () {
        return !!this.props.clipperState.userResult && !!this.props.clipperState.userResult.data
            && this.props.clipperState.userResult.data.updateReason === userInfo_1.UpdateReason.SignInAttempt;
    };
    SignInPanelClass.prototype.signInFailureContainsErrorDescription = function () {
        return this.signInAttempted()
            && this.props.clipperState.userResult.data.errorDescription
            && this.props.clipperState.userResult.data.errorDescription.indexOf("OrgId") === 0;
    };
    SignInPanelClass.prototype.signInFailureThirdPartyCookiesBlocked = function () {
        return this.signInAttempted()
            && !this.props.clipperState.userResult.data.user
            && !this.props.clipperState.userResult.data.writeableCookies;
    };
    SignInPanelClass.prototype.debugInformationControlHandler = function () {
        this.setState({ debugInformationShowing: !this.state.debugInformationShowing });
    };
    SignInPanelClass.prototype.signInErrorDetected = function () {
        return this.signInFailureContainsErrorDescription() || this.signInFailureThirdPartyCookiesBlocked();
    };
    SignInPanelClass.prototype.errorMoreInformationTogggle = function () {
        if (this.signInErrorDetected()) {
            return <div id="signInErrorToggleInformation">
				<a id={constants_1.Constants.Ids.signInErrorMoreInformation} {...this.enableInvoke({ callback: this.debugInformationControlHandler, tabIndex: 10 })}>
					<img id={constants_1.Constants.Ids.signInToggleErrorDropdownArrow} src={extensionUtils_1.ExtensionUtils.getImageResourceUrl("dropdown_arrow.png")}/>
					<span id={constants_1.Constants.Ids.signInToggleErrorInformationText} style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Light)}>
						{this.state.debugInformationShowing
                ? localization_1.Localization.getLocalizedString("WebClipper.Label.SignInUnsuccessfulLessInformation")
                : localization_1.Localization.getLocalizedString("WebClipper.Label.SignInUnsuccessfulMoreInformation")}
					</span>
				</a>
				{this.debugInformation()}
			</div>;
        }
        return undefined;
    };
    SignInPanelClass.prototype.debugInformation = function () {
        if (this.signInErrorDetected() && this.state.debugInformationShowing) {
            return <div id={constants_1.Constants.Ids.signInErrorDebugInformation}>
				<div id={constants_1.Constants.Ids.signInErrorDebugInformationContainer} style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Light)}>
					<ul id={constants_1.Constants.Ids.signInErrorDebugInformationList}>
						<li>{clientType_1.ClientType[this.props.clipperState.clientInfo.clipperType]}: {this.props.clipperState.clientInfo.clipperVersion}</li>
						<li>ID: {this.props.clipperState.clientInfo.clipperId}</li>
						<li>USID: {frontEndGlobals_1.Clipper.getUserSessionId()}</li>
					</ul>
				</div>
			</div>;
        }
    };
    SignInPanelClass.prototype.getErrorDescription = function () {
        if (this.signInFailureContainsErrorDescription()) {
            return this.props.clipperState.userResult.data.errorDescription;
        }
        else if (this.signInFailureThirdPartyCookiesBlocked()) {
            var browserSpecificMessage = "";
            switch (this.props.clipperState.clientInfo.clipperType) {
                case clientType_1.ClientType.ChromeExtension:
                    browserSpecificMessage = localization_1.Localization.getLocalizedString("WebClipper.Error.CookiesDisabled.Chrome");
                    break;
                case clientType_1.ClientType.EdgeExtension:
                    browserSpecificMessage = localization_1.Localization.getLocalizedString("WebClipper.Error.CookiesDisabled.Edge");
                    break;
                case clientType_1.ClientType.FirefoxExtension:
                    browserSpecificMessage = localization_1.Localization.getLocalizedString("WebClipper.Error.CookiesDisabled.Firefox");
                    break;
                default:
                    browserSpecificMessage = localization_1.Localization.getLocalizedString("WebClipper.Error.CookiesDisabled.Line2");
                    break;
            }
            return <div>
				<div class={constants_1.Constants.Ids.signInErrorCookieInformation}>{localization_1.Localization.getLocalizedString("WebClipper.Error.CookiesDisabled.Line1")}</div>
				<div class={constants_1.Constants.Ids.signInErrorCookieInformation}>{browserSpecificMessage}</div>
			</div>;
        }
        return undefined;
    };
    SignInPanelClass.prototype.errorInformationDescription = function () {
        if (this.signInErrorDetected()) {
            return <div id={constants_1.Constants.Ids.signInErrorDescription}>
				<span id={constants_1.Constants.Ids.signInErrorDescriptionContainer} style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Light)}>
					{this.getErrorDescription()}
				</span>
				{this.errorMoreInformationTogggle()}
			</div>;
        }
        return undefined;
    };
    SignInPanelClass.prototype.render = function () {
        return (<div id={constants_1.Constants.Ids.signInContainer}>
				<div className="signInPadding">
					<img id={constants_1.Constants.Ids.signInLogo} src={extensionUtils_1.ExtensionUtils.getImageResourceUrl("onenote_logo_clipper.png")}/>
					<div id={constants_1.Constants.Ids.signInMessageLabelContainer} className="messageLabelContainer">
						<span className="messageLabel" style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)}>
							{localization_1.Localization.getLocalizedString("WebClipper.Label.OneNoteClipper")}
						</span>
					</div>
					<div className="signInDescription">
						<span id={constants_1.Constants.Ids.signInText} style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Light)}>
							{this.getSignInDescription()}
						</span>
					</div>
					{this.errorInformationDescription()}
					<a id={constants_1.Constants.Ids.signInButtonMsa} role="button" {...this.enableInvoke({ callback: this.onSignInMsa, tabIndex: 11, args: userInfo_1.AuthType.Msa })}>
						<div className="wideButtonContainer">
							<span className="wideButtonFont wideActionButton" style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)}>
								{localization_1.Localization.getLocalizedString("WebClipper.Action.SigninMsa")}
							</span>
						</div>
					</a>
					<a id={constants_1.Constants.Ids.signInButtonOrgId} role="button" {...this.enableInvoke({ callback: this.onSignInOrgId, tabIndex: 12, args: userInfo_1.AuthType.OrgId })}>
						<div className="wideButtonContainer">
							<span className="wideButtonFont wideActionButton" style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)}>
								{localization_1.Localization.getLocalizedString("WebClipper.Action.SigninOrgId")}
							</span>
						</div>
					</a>
				</div>
			</div>);
    };
    return SignInPanelClass;
}(componentBase_1.ComponentBase));
var component = SignInPanelClass.componentize();
exports.SignInPanel = component;
