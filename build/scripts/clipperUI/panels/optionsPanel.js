"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../constants");
var operationResult_1 = require("../../operationResult");
var stringUtils_1 = require("../../stringUtils");
var localization_1 = require("../../localization/localization");
var modeButtonSelector_1 = require("../components/modeButtonSelector");
var clipMode_1 = require("../clipMode");
var clipperStateUtilities_1 = require("../clipperStateUtilities");
var componentBase_1 = require("../componentBase");
var status_1 = require("../status");
var pdfClipOptions_1 = require("../components/pdfClipOptions");
var _ = require("lodash");
var OptionsPanelClass = (function (_super) {
    __extends(OptionsPanelClass, _super);
    function OptionsPanelClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    OptionsPanelClass.prototype.getCurrentClippingOptions = function () {
        var currentMode = this.props.clipperState.currentMode.get();
        switch (currentMode) {
            case clipMode_1.ClipMode.Pdf:
                return m.component(pdfClipOptions_1.PdfClipOptions, {clipperState:this.props.clipperState});
            default:
                return undefined;
        }
    };
    OptionsPanelClass.prototype.checkOptionsBeforeStartClip = function () {
        var clipperState = this.props.clipperState;
        var pdfPreviewInfo = clipperState.pdfPreviewInfo;
        // If the user is in PDF mode and selected a page range, disallow the clip if it's an invalid range
        if (clipperState.currentMode.get() === clipMode_1.ClipMode.Pdf && !pdfPreviewInfo.allPages && clipperState.pdfResult.status === status_1.Status.Succeeded) {
            var parsePageRangeOperation = stringUtils_1.StringUtils.parsePageRange(pdfPreviewInfo.selectedPageRange, clipperState.pdfResult.data.get().pdf.numPages());
            if (parsePageRangeOperation.status !== operationResult_1.OperationResult.Succeeded) {
                var newPdfPreviewInfo = _.extend(_.cloneDeep(pdfPreviewInfo), { shouldShowPopover: true });
                clipperState.setState({
                    pdfPreviewInfo: newPdfPreviewInfo
                });
                return;
            }
        }
        this.props.onStartClip();
    };
    // This function is passed into Mithril's config property
    // The config property is a function that allows you to hook into 
    // a component's lifecycle such as mounting and un-mounting
    OptionsPanelClass.prototype.attachClipHotKeyListener = function (element, isInitialized, context) {
        var _this = this;
        // If this is the first time we are initializing this element,
        // 	then attach the listener
        if (!isInitialized) {
            var oldOnKeyDown_1 = document.onkeydown;
            document.onkeydown = function (ev) {
                // TODO: KeyboardEvent::which is deprecated but PhantomJs doesn't support
                // 'event constructors', which is necessary to use the recommended KeyboardEvent::key 
                if (ev.altKey && ev.which === constants_1.Constants.KeyCodes.c) {
                    _this.checkOptionsBeforeStartClip();
                }
                if (oldOnKeyDown_1) {
                    oldOnKeyDown_1.call(document, ev);
                }
            };
            // Remove listener when this element is unmounted
            context.onunload = function () {
                document.onkeydown = oldOnKeyDown_1 ? oldOnKeyDown_1.bind(document) : undefined;
            };
        }
    };
    OptionsPanelClass.prototype.render = function () {
        var clipButtonEnabled = clipperStateUtilities_1.ClipperStateUtilities.clipButtonEnabled(this.props.clipperState);
        var clipButtonContainerClassName = clipButtonEnabled ? "wideButtonContainer" : "wideButtonContainer disabled";
        var clippingOptionsToRender = this.getCurrentClippingOptions();
        return ({tag: "div", attrs: {className:"optionsPanel", config:this.attachClipHotKeyListener.bind(this)}, children: [
				m.component(modeButtonSelector_1.ModeButtonSelector, {clipperState:this.props.clipperState}), 
				clippingOptionsToRender, 
				
				{tag: "div", attrs: {id:constants_1.Constants.Ids.clipButtonContainer, className:clipButtonContainerClassName}, children: [
					clipButtonEnabled
            ? {tag: "a", attrs: Object.assign({id:constants_1.Constants.Ids.clipButton, className:"wideActionButton"},  this.enableInvoke({ callback: this.checkOptionsBeforeStartClip.bind(this), tabIndex: 71 }),{role:"button"}), children: [
							{tag: "span", attrs: {className:"wideButtonFont", style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Semibold)}, children: [
								localization_1.Localization.getLocalizedString("WebClipper.Action.Clip")
							]}
						]}
            : {tag: "a", attrs: {id:constants_1.Constants.Ids.clipButton, className:"wideActionButton", role:"button"}, children: [
							{tag: "span", attrs: {className:"wideButtonFont", style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Semibold)}, children: [
								localization_1.Localization.getLocalizedString("WebClipper.Action.Clip")
							]}
						]}
				]}
			]});
    };
    return OptionsPanelClass;
}(componentBase_1.ComponentBase));
var component = OptionsPanelClass.componentize();
exports.OptionsPanel = component;
