"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var clientType_1 = require("../../clientType");
var constants_1 = require("../../constants");
var userInfo_1 = require("../../userInfo");
var extensionUtils_1 = require("../../extensions/extensionUtils");
var localization_1 = require("../../localization/localization");
var frontEndGlobals_1 = require("../frontEndGlobals");
var componentBase_1 = require("../componentBase");
var SignInPanelClass = (function (_super) {
    __extends(SignInPanelClass, _super);
    function SignInPanelClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SignInPanelClass.prototype.getInitialState = function () {
        return { debugInformationShowing: false };
    };
    SignInPanelClass.prototype.onSignInMsa = function () {
        this.props.onSignInInvoked(userInfo_1.AuthType.Msa);
    };
    SignInPanelClass.prototype.onSignInOrgId = function () {
        this.props.onSignInInvoked(userInfo_1.AuthType.OrgId);
    };
    SignInPanelClass.prototype.getSignInDescription = function () {
        // Since the user is not signed in, we show a message depending on the reason of the last update to the userResult
        if (!this.props.clipperState.userResult || !this.props.clipperState.userResult.data) {
            return localization_1.Localization.getLocalizedString("WebClipper.Label.SignInDescription");
        }
        switch (this.props.clipperState.userResult.data.updateReason) {
            case userInfo_1.UpdateReason.SignInAttempt:
                return localization_1.Localization.getLocalizedString("WebClipper.Error.SignInUnsuccessful");
            case userInfo_1.UpdateReason.TokenRefreshForPendingClip:
                return localization_1.Localization.getLocalizedString("WebClipper.Error.GenericExpiredTokenRefreshError");
            default:
            case userInfo_1.UpdateReason.SignInCancel:
            case userInfo_1.UpdateReason.SignOutAction:
            case userInfo_1.UpdateReason.InitialRetrieval:
                return localization_1.Localization.getLocalizedString("WebClipper.Label.SignInDescription");
        }
    };
    SignInPanelClass.prototype.signInAttempted = function () {
        return !!this.props.clipperState.userResult && !!this.props.clipperState.userResult.data
            && this.props.clipperState.userResult.data.updateReason === userInfo_1.UpdateReason.SignInAttempt;
    };
    SignInPanelClass.prototype.signInFailureContainsErrorDescription = function () {
        return this.signInAttempted()
            && this.props.clipperState.userResult.data.errorDescription
            && this.props.clipperState.userResult.data.errorDescription.indexOf("OrgId") === 0;
    };
    SignInPanelClass.prototype.signInFailureThirdPartyCookiesBlocked = function () {
        return this.signInAttempted()
            && !this.props.clipperState.userResult.data.user
            && !this.props.clipperState.userResult.data.writeableCookies;
    };
    SignInPanelClass.prototype.debugInformationControlHandler = function () {
        this.setState({ debugInformationShowing: !this.state.debugInformationShowing });
    };
    SignInPanelClass.prototype.signInErrorDetected = function () {
        return this.signInFailureContainsErrorDescription() || this.signInFailureThirdPartyCookiesBlocked();
    };
    SignInPanelClass.prototype.errorMoreInformationTogggle = function () {
        if (this.signInErrorDetected()) {
            return {tag: "div", attrs: {id:"signInErrorToggleInformation"}, children: [
				{tag: "a", attrs: Object.assign({id:constants_1.Constants.Ids.signInErrorMoreInformation},  this.enableInvoke({ callback: this.debugInformationControlHandler, tabIndex: 10 })), children: [
					{tag: "img", attrs: {id:constants_1.Constants.Ids.signInToggleErrorDropdownArrow, src:extensionUtils_1.ExtensionUtils.getImageResourceUrl("dropdown_arrow.png")}}, 
					{tag: "span", attrs: {id:constants_1.Constants.Ids.signInToggleErrorInformationText, style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Light)}, children: [
						this.state.debugInformationShowing
                ? localization_1.Localization.getLocalizedString("WebClipper.Label.SignInUnsuccessfulLessInformation")
                : localization_1.Localization.getLocalizedString("WebClipper.Label.SignInUnsuccessfulMoreInformation")
					]}
				]}, 
				this.debugInformation()
			]};
        }
        return undefined;
    };
    SignInPanelClass.prototype.debugInformation = function () {
        if (this.signInErrorDetected() && this.state.debugInformationShowing) {
            return {tag: "div", attrs: {id:constants_1.Constants.Ids.signInErrorDebugInformation}, children: [
				{tag: "div", attrs: {id:constants_1.Constants.Ids.signInErrorDebugInformationContainer, style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Light)}, children: [
					{tag: "ul", attrs: {id:constants_1.Constants.Ids.signInErrorDebugInformationList}, children: [
						{tag: "li", attrs: {}, children: [clientType_1.ClientType[this.props.clipperState.clientInfo.clipperType], ": ", this.props.clipperState.clientInfo.clipperVersion]}, 
						{tag: "li", attrs: {}, children: ["ID: ", this.props.clipperState.clientInfo.clipperId]}, 
						{tag: "li", attrs: {}, children: ["USID: ", frontEndGlobals_1.Clipper.getUserSessionId()]}
					]}
				]}
			]};
        }
    };
    SignInPanelClass.prototype.getErrorDescription = function () {
        if (this.signInFailureContainsErrorDescription()) {
            return this.props.clipperState.userResult.data.errorDescription;
        }
        else if (this.signInFailureThirdPartyCookiesBlocked()) {
            var browserSpecificMessage = "";
            switch (this.props.clipperState.clientInfo.clipperType) {
                case clientType_1.ClientType.ChromeExtension:
                    browserSpecificMessage = localization_1.Localization.getLocalizedString("WebClipper.Error.CookiesDisabled.Chrome");
                    break;
                case clientType_1.ClientType.EdgeExtension:
                    browserSpecificMessage = localization_1.Localization.getLocalizedString("WebClipper.Error.CookiesDisabled.Edge");
                    break;
                case clientType_1.ClientType.FirefoxExtension:
                    browserSpecificMessage = localization_1.Localization.getLocalizedString("WebClipper.Error.CookiesDisabled.Firefox");
                    break;
                default:
                    browserSpecificMessage = localization_1.Localization.getLocalizedString("WebClipper.Error.CookiesDisabled.Line2");
                    break;
            }
            return {tag: "div", attrs: {}, children: [
				{tag: "div", attrs: {class:constants_1.Constants.Ids.signInErrorCookieInformation}, children: [localization_1.Localization.getLocalizedString("WebClipper.Error.CookiesDisabled.Line1")]}, 
				{tag: "div", attrs: {class:constants_1.Constants.Ids.signInErrorCookieInformation}, children: [browserSpecificMessage]}
			]};
        }
        return undefined;
    };
    SignInPanelClass.prototype.errorInformationDescription = function () {
        if (this.signInErrorDetected()) {
            return {tag: "div", attrs: {id:constants_1.Constants.Ids.signInErrorDescription}, children: [
				{tag: "span", attrs: {id:constants_1.Constants.Ids.signInErrorDescriptionContainer, style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Light)}, children: [
					this.getErrorDescription()
				]}, 
				this.errorMoreInformationTogggle()
			]};
        }
        return undefined;
    };
    SignInPanelClass.prototype.render = function () {
        return ({tag: "div", attrs: {id:constants_1.Constants.Ids.signInContainer}, children: [
				{tag: "div", attrs: {className:"signInPadding"}, children: [
					{tag: "img", attrs: {id:constants_1.Constants.Ids.signInLogo, src:extensionUtils_1.ExtensionUtils.getImageResourceUrl("onenote_logo_clipper.png")}}, 
					{tag: "div", attrs: {id:constants_1.Constants.Ids.signInMessageLabelContainer, className:"messageLabelContainer"}, children: [
						{tag: "span", attrs: {className:"messageLabel", style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)}, children: [
							localization_1.Localization.getLocalizedString("WebClipper.Label.OneNoteClipper")
						]}
					]}, 
					{tag: "div", attrs: {className:"signInDescription"}, children: [
						{tag: "span", attrs: {id:constants_1.Constants.Ids.signInText, style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Light)}, children: [
							this.getSignInDescription()
						]}
					]}, 
					this.errorInformationDescription(), 
					{tag: "a", attrs: Object.assign({id:constants_1.Constants.Ids.signInButtonMsa, role:"button"},  this.enableInvoke({ callback: this.onSignInMsa, tabIndex: 11, args: userInfo_1.AuthType.Msa })), children: [
						{tag: "div", attrs: {className:"wideButtonContainer"}, children: [
							{tag: "span", attrs: {className:"wideButtonFont wideActionButton", style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)}, children: [
								localization_1.Localization.getLocalizedString("WebClipper.Action.SigninMsa")
							]}
						]}
					]}, 
					{tag: "a", attrs: Object.assign({id:constants_1.Constants.Ids.signInButtonOrgId, role:"button"},  this.enableInvoke({ callback: this.onSignInOrgId, tabIndex: 12, args: userInfo_1.AuthType.OrgId })), children: [
						{tag: "div", attrs: {className:"wideButtonContainer"}, children: [
							{tag: "span", attrs: {className:"wideButtonFont wideActionButton", style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)}, children: [
								localization_1.Localization.getLocalizedString("WebClipper.Action.SigninOrgId")
							]}
						]}
					]}
				]}
			]});
    };
    return SignInPanelClass;
}(componentBase_1.ComponentBase));
var component = SignInPanelClass.componentize();
exports.SignInPanel = component;
