"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../constants");
var augmentationHelper_1 = require("../../contentCapture/augmentationHelper");
var extensionUtils_1 = require("../../extensions/extensionUtils");
var localization_1 = require("../../localization/localization");
var Log = require("../../logging/log");
var clipMode_1 = require("../clipMode");
var frontEndGlobals_1 = require("../frontEndGlobals");
var componentBase_1 = require("../componentBase");
var spriteAnimation_1 = require("../components/spriteAnimation");
var ClippingPanelClass = (function (_super) {
    __extends(ClippingPanelClass, _super);
    function ClippingPanelClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ClippingPanelClass.prototype.getProgressLabel = function () {
        switch (this.props.clipperState.currentMode.get()) {
            case clipMode_1.ClipMode.Pdf:
                return localization_1.Localization.getLocalizedString("WebClipper.ClipType.Pdf.ProgressLabel");
            case clipMode_1.ClipMode.Region:
                return localization_1.Localization.getLocalizedString("WebClipper.ClipType.Region.ProgressLabel");
            case clipMode_1.ClipMode.Augmentation:
                var retVal = void 0;
                try {
                    var augmentationModelStr = augmentationHelper_1.AugmentationModel[this.props.clipperState.augmentationResult.data.ContentModel];
                    retVal = localization_1.Localization.getLocalizedString("WebClipper.ClipType." + augmentationModelStr + ".ProgressLabel");
                }
                catch (e) {
                    frontEndGlobals_1.Clipper.logger.logFailure(Log.Failure.Label.GetLocalizedString, Log.Failure.Type.Unexpected, { error: e.message }, clipMode_1.ClipMode[this.props.clipperState.currentMode.get()]);
                    // Fallback string
                    retVal = localization_1.Localization.getLocalizedString("WebClipper.ClipType.ScreenShot.ProgressLabel");
                }
                return retVal;
            case clipMode_1.ClipMode.Bookmark:
                return localization_1.Localization.getLocalizedString("WebClipper.ClipType.Bookmark.ProgressLabel");
            case clipMode_1.ClipMode.Selection:
                return localization_1.Localization.getLocalizedString("WebClipper.ClipType.Selection.ProgressLabel");
            default:
            case clipMode_1.ClipMode.FullPage:
                return localization_1.Localization.getLocalizedString("WebClipper.ClipType.ScreenShot.ProgressLabel");
        }
    };
    ClippingPanelClass.prototype.render = function () {
        return (<div id={constants_1.Constants.Ids.clipperApiProgressContainer} className="progressPadding">
				<spriteAnimation_1.SpriteAnimation spriteUrl={extensionUtils_1.ExtensionUtils.getImageResourceUrl("spinner_loop.png")} imageHeight={32} totalFrameCount={21} loop={true}/>
				<span className="actionLabelFont messageLabel" role="alert" style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)}>
					{this.getProgressLabel()}
				</span>
			</div>);
    };
    return ClippingPanelClass;
}(componentBase_1.ComponentBase));
var component = ClippingPanelClass.componentize();
exports.ClippingPanel = component;
