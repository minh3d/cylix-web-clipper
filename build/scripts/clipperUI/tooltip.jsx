"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var localization_1 = require("../localization/localization");
var componentBase_1 = require("./componentBase");
var closeButton_1 = require("./components/closeButton");
/**
 * Renders content with consistent tooltip styling
 */
var TooltipClass = (function (_super) {
    __extends(TooltipClass, _super);
    function TooltipClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TooltipClass.prototype.getInitialState = function () {
        return {};
    };
    TooltipClass.prototype.render = function () {
        var additionalContentClasses = "";
        if (this.props.contentClasses) {
            additionalContentClasses = this.props.contentClasses.join(" ");
        }
        return (<div id={this.props.elementId} className="tooltip" {...this.onElementDraw(this.props.onElementDraw)}>
				{this.props.brandingImage}
				{this.props.title ? <div style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Semibold)} className="tooltip-title">{this.props.title}</div> : undefined}
				<closeButton_1.CloseButton onClickHandler={this.props.onCloseButtonHandler} onClickHandlerParams={undefined}/>
				<div className={"tooltip-content " + additionalContentClasses}>
					{this.props.renderablePanel}
				</div>
			</div>);
    };
    return TooltipClass;
}(componentBase_1.ComponentBase));
exports.TooltipClass = TooltipClass;
var component = TooltipClass.componentize();
exports.Tooltip = component;
