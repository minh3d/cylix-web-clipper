"use strict";
var clientType_1 = require("../clientType");
var clipperUrls_1 = require("../clipperUrls");
var constants_1 = require("../constants");
var objectUtils_1 = require("../objectUtils");
var settings_1 = require("../settings");
var Log = require("../logging/log");
var clipperStorageKeys_1 = require("../storage/clipperStorageKeys");
var version_1 = require("../versioning/version");
var frontEndGlobals_1 = require("./frontEndGlobals");
// ordered by stage progression
var RatingsPromptStage;
(function (RatingsPromptStage) {
    RatingsPromptStage[RatingsPromptStage["Init"] = 0] = "Init";
    RatingsPromptStage[RatingsPromptStage["Rate"] = 1] = "Rate";
    RatingsPromptStage[RatingsPromptStage["Feedback"] = 2] = "Feedback";
    RatingsPromptStage[RatingsPromptStage["End"] = 3] = "End";
    RatingsPromptStage[RatingsPromptStage["None"] = 4] = "None";
})(RatingsPromptStage = exports.RatingsPromptStage || (exports.RatingsPromptStage = {}));
var RatingsHelper = (function () {
    function RatingsHelper() {
    }
    /**
     * Returns true if ClipperStorageKeys.lastBadRatingDate already contained a cached value
     * (meaning the user had already rated us negatively)
     */
    RatingsHelper.badRatingAlreadyOccurred = function () {
        var lastBadRatingDateAsStr = frontEndGlobals_1.Clipper.getCachedValue(clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingDate);
        var lastBadRatingDate = parseInt(lastBadRatingDateAsStr, 10);
        if (!isNaN(lastBadRatingDate) && RatingsHelper.isValidDate(lastBadRatingDate)) {
            return true;
        }
        return false;
    };
    /**
     * Get the feedback URL with the special ratings prompt log category, if it exists
     */
    RatingsHelper.getFeedbackUrlIfExists = function (clipperState) {
        var ratingsPromptLogCategory = settings_1.Settings.getSetting("LogCategory_RatingsPrompt");
        if (!objectUtils_1.ObjectUtils.isNullOrUndefined(ratingsPromptLogCategory) && ratingsPromptLogCategory.length > 0) {
            return clipperUrls_1.ClipperUrls.generateFeedbackUrl(clipperState, frontEndGlobals_1.Clipper.getUserSessionId(), ratingsPromptLogCategory);
        }
    };
    /**
     * Get ratings/reviews URL for the provided ClientType/ClipperType, if it exists
     */
    RatingsHelper.getRateUrlIfExists = function (clientType) {
        var settingName = RatingsHelper.getRateUrlSettingNameForClient(clientType);
        return settings_1.Settings.getSetting(settingName);
    };
    /**
     * Pre-cache values needed to determine whether to show the ratings prompt
     */
    RatingsHelper.preCacheNeededValues = function () {
        var ratingsPromptStorageKeys = [
            clipperStorageKeys_1.ClipperStorageKeys.doNotPromptRatings,
            clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingDate,
            clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingVersion,
            clipperStorageKeys_1.ClipperStorageKeys.lastSeenVersion,
            clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClips,
            clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClipsRatingsEnablement
        ];
        frontEndGlobals_1.Clipper.preCacheStoredValues(ratingsPromptStorageKeys);
    };
    /**
     * Set ClipperStorageKeys.doNotPromptRatings value to "true"
     */
    RatingsHelper.setDoNotPromptStatus = function () {
        frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.doNotPromptRatings, "true");
        frontEndGlobals_1.Clipper.logger.logEvent(new Log.Event.BaseEvent(Log.Event.Label.SetDoNotPromptRatings));
    };
    /**
     * We will show the ratings prompt if ALL of the below applies:
     *   * Ratings prompt is enabled for the ClientType/ClipperType
     *   * If ClipperStorageKeys.doNotPromptRatings is not "true"
     *   * If RatingsHelper.badRatingTimingDelayIsOver(...) returns true when provided ClipperStorageKeys.lastBadRatingDate
     *   * If RatingsHelper.badRatingVersionDelayIsOver(...) returns true when provided ClipperStorageKeys.lastBadRatingVersion and ClipperStorageKeys.lastSeenVersion
     *   * If RatingsHelper.clipSuccessDelayIsOver(...) returns true when provided ClipperStorageKeys.numClipSuccess
     */
    RatingsHelper.shouldShowRatingsPrompt = function (clipperState) {
        var shouldShowRatingsPromptEvent = new Log.Event.PromiseEvent(Log.Event.Label.ShouldShowRatingsPrompt);
        var shouldShowRatingsPromptInfo = {};
        var shouldShowRatingsPrompt = RatingsHelper.shouldShowRatingsPromptInternal(clipperState, shouldShowRatingsPromptEvent, shouldShowRatingsPromptInfo);
        shouldShowRatingsPromptEvent.setCustomProperty(Log.PropertyName.Custom.ShouldShowRatingsPrompt, shouldShowRatingsPrompt);
        shouldShowRatingsPromptEvent.setCustomProperty(Log.PropertyName.Custom.RatingsInfo, JSON.stringify(shouldShowRatingsPromptInfo));
        frontEndGlobals_1.Clipper.logger.logEvent(shouldShowRatingsPromptEvent);
        return shouldShowRatingsPrompt;
    };
    /**
     * Returns true if the ratings prompt is enabled for ClientType/ClipperType provided
     *
     * Public for testing
     */
    RatingsHelper.ratingsPromptEnabledForClient = function (clientType) {
        var settingName = RatingsHelper.getRatingsPromptEnabledSettingNameForClient(clientType);
        var isEnabledAsStr = settings_1.Settings.getSetting(settingName);
        return !objectUtils_1.ObjectUtils.isNullOrUndefined(isEnabledAsStr) && isEnabledAsStr.toLowerCase() === "true";
    };
    /**
     * Returns true if ONE of the below applies:
     *   1) A bad rating has never been given by the user, OR
     *   2) Bad rating date provided is valid and occurred more than {Constants.Settings.minTimeBetweenBadRatings} ago
     *      from the valid comparison date (e.g., the current date) provided
     *
     * Public for testing
     */
    RatingsHelper.badRatingTimingDelayIsOver = function (badRatingDate, comparisonDate) {
        if (isNaN(badRatingDate)) {
            // value has never been set, no bad rating given
            return true;
        }
        if (!RatingsHelper.isValidDate(badRatingDate) || !RatingsHelper.isValidDate(comparisonDate)) {
            return false;
        }
        return (comparisonDate - badRatingDate) >= constants_1.Constants.Settings.minTimeBetweenBadRatings;
    };
    /**
     * Returns true if ONE of the below applies:
     *   1) A bad rating has never been given by the user, OR
     *   2) There has been a non-patch version update since the bad rating, i.e.,
     *      a) The major version last seen by the user is greater than the major version at the time of the last bad rating, OR
     *      b) The major version remained the same, while the minor version last seen by the user is greater than
     *          the minor version at the time of the last bad rating
     *
     * Public for testing
     */
    RatingsHelper.badRatingVersionDelayIsOver = function (badRatingVersionAsStr, lastSeenVersionAsStr) {
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(badRatingVersionAsStr)) {
            // value has never been set, no bad rating given
            return true;
        }
        var badRatingVersion;
        var lastSeenVersion;
        try {
            badRatingVersion = new version_1.Version(badRatingVersionAsStr);
            lastSeenVersion = new version_1.Version(lastSeenVersionAsStr);
        }
        catch (e) {
            return false;
        }
        return lastSeenVersion.isGreaterThan(badRatingVersion, true /* ignorePatchUpdate */);
    };
    /**
     * Returns true if ALL of the below applies:
     *   * (Number of successful clips - Anchor clip value) >= {Constants.Settings.minClipSuccessForRatingsPrompt}
     *   * (Number of successful clips - Anchor clip value) <= {Constants.Settings.maxClipSuccessForRatingsPrompt}
     *
     * Public for testing
     */
    RatingsHelper.clipSuccessDelayIsOver = function (numClips, anchorClipValue) {
        if (isNaN(numClips)) {
            return false;
        }
        if (isNaN(anchorClipValue)) {
            anchorClipValue = 0;
        }
        var numClipsAdjusted = numClips - anchorClipValue;
        return numClipsAdjusted >= constants_1.Constants.Settings.minClipSuccessForRatingsPrompt &&
            numClipsAdjusted <= constants_1.Constants.Settings.maxClipSuccessForRatingsPrompt;
    };
    /**
     * Sets ClipperStorageKeys.numSuccessfulClipsRatingsEnablement to be
     * the current value of (ClipperStorageKeys.numSuccessfulClips - 1), if needed.
     *
     * The set is "needed" if ALL of the below applies:
     *   * The user has not already interacted with the prompt (ClipperStorageKeys.doNotPromptRatings is not set)
     *   * ClipperStorageKeys.numSuccessfulClipsRatingsEnablement has not already been set
     *
     * Public for testing
     *
     * NOTE OF EXPLANATION: We first check if the user has already interacted with the prompt for backwards compatibility
     * with the original implementation of the ratings prompt that did not include this method. It ensures that we will not
     * re-raise the prompt for users who have already interacted with it (although it is possible users who didn't interact
     * with the original prompt see it up to twice as many times as originally planned).
     */
    RatingsHelper.setNumSuccessfulClipsRatingsEnablement = function () {
        var doNotPromptRatingsAsStr = frontEndGlobals_1.Clipper.getCachedValue(clipperStorageKeys_1.ClipperStorageKeys.doNotPromptRatings);
        if (RatingsHelper.doNotPromptRatingsIsSet(doNotPromptRatingsAsStr)) {
            return;
        }
        var numSuccessfulClipsRatingsEnablementAsStr = frontEndGlobals_1.Clipper.getCachedValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClipsRatingsEnablement);
        if (parseInt(numSuccessfulClipsRatingsEnablementAsStr, 10) >= 0) {
            return;
        }
        var numSuccessfulClips = parseInt(frontEndGlobals_1.Clipper.getCachedValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClips), 10);
        // subtracting 1 below to account for the fact that this set is occuring after one already successful clip
        frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClipsRatingsEnablement, (numSuccessfulClips - 1).toString());
    };
    /**
     * Implementation of the logic described in the setShowRatingsPromptState(...) description
     */
    RatingsHelper.shouldShowRatingsPromptInternal = function (clipperState, event, logEventInfo) {
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(clipperState)) {
            event.setStatus(Log.Status.Failed);
            event.setFailureInfo({ error: "Clipper state is null or undefined" });
            return false;
        }
        if (!objectUtils_1.ObjectUtils.isNullOrUndefined(clipperState.showRatingsPrompt)) {
            // Return cached value in clipper state since it already exists
            logEventInfo.usedCachedValue = true;
            return clipperState.showRatingsPrompt;
        }
        var ratingsPromptEnabled = RatingsHelper.ratingsPromptEnabledForClient(clipperState.clientInfo.clipperType);
        logEventInfo.ratingsPromptEnabledForClient = ratingsPromptEnabled;
        if (!ratingsPromptEnabled) {
            return false;
        }
        RatingsHelper.setNumSuccessfulClipsRatingsEnablement();
        var doNotPromptRatingsStr = frontEndGlobals_1.Clipper.getCachedValue(clipperStorageKeys_1.ClipperStorageKeys.doNotPromptRatings);
        var lastBadRatingDateAsStr = frontEndGlobals_1.Clipper.getCachedValue(clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingDate);
        var lastBadRatingVersion = frontEndGlobals_1.Clipper.getCachedValue(clipperStorageKeys_1.ClipperStorageKeys.lastBadRatingVersion);
        var lastSeenVersion = frontEndGlobals_1.Clipper.getCachedValue(clipperStorageKeys_1.ClipperStorageKeys.lastSeenVersion);
        var numClipsAsStr = frontEndGlobals_1.Clipper.getCachedValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClips);
        var numClipsAnchorAsStr = frontEndGlobals_1.Clipper.getCachedValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClipsRatingsEnablement);
        if (RatingsHelper.doNotPromptRatingsIsSet(doNotPromptRatingsStr)) {
            logEventInfo.doNotPromptRatings = true;
            return false;
        }
        var lastBadRatingDate = parseInt(lastBadRatingDateAsStr, 10);
        var numClips = parseInt(numClipsAsStr, 10);
        var numClipsAnchor = parseInt(numClipsAnchorAsStr, 10);
        /* tslint:disable:no-null-keyword */
        // null is the value storage gives back; also, setting to undefined will keep this kvp from being logged at all
        logEventInfo.lastBadRatingDate = lastBadRatingDate ? new Date(lastBadRatingDate).toString() : null;
        /* tslint:enable:no-null-keyword */
        logEventInfo.lastBadRatingVersion = lastBadRatingVersion;
        logEventInfo.lastSeenVersion = lastSeenVersion;
        logEventInfo.numSuccessfulClips = numClips;
        logEventInfo.numSuccessfulClipsAnchor = numClipsAnchor;
        var badRatingTimingDelayIsOver = RatingsHelper.badRatingTimingDelayIsOver(lastBadRatingDate, Date.now());
        var badRatingVersionDelayIsOver = RatingsHelper.badRatingVersionDelayIsOver(lastBadRatingVersion, lastSeenVersion);
        var clipSuccessDelayIsOver = RatingsHelper.clipSuccessDelayIsOver(numClips, numClipsAnchor);
        logEventInfo.badRatingTimingDelayIsOver = badRatingTimingDelayIsOver;
        logEventInfo.badRatingVersionDelayIsOver = badRatingVersionDelayIsOver;
        logEventInfo.clipSuccessDelayIsOver = clipSuccessDelayIsOver;
        if (badRatingTimingDelayIsOver && badRatingVersionDelayIsOver && clipSuccessDelayIsOver) {
            return true;
        }
        return false;
    };
    RatingsHelper.combineClientTypeAndSuffix = function (clientType, suffix) {
        return clientType_1.ClientType[clientType] + suffix;
    };
    RatingsHelper.getRateUrlSettingNameForClient = function (clientType) {
        return RatingsHelper.combineClientTypeAndSuffix(clientType, RatingsHelper.rateUrlSettingNameSuffix);
    };
    RatingsHelper.getRatingsPromptEnabledSettingNameForClient = function (clientType) {
        return RatingsHelper.combineClientTypeAndSuffix(clientType, RatingsHelper.ratingsPromptEnabledSettingNameSuffix);
    };
    RatingsHelper.isValidDate = function (date) {
        var minimumTimeValue = (constants_1.Constants.Settings.maximumJSTimeValue * -1);
        return date >= minimumTimeValue && date <= constants_1.Constants.Settings.maximumJSTimeValue;
    };
    RatingsHelper.doNotPromptRatingsIsSet = function (doNotPromptRatingsStr) {
        return !objectUtils_1.ObjectUtils.isNullOrUndefined(doNotPromptRatingsStr) && doNotPromptRatingsStr.toLowerCase() === "true";
    };
    return RatingsHelper;
}());
RatingsHelper.rateUrlSettingNameSuffix = "_RatingUrl";
RatingsHelper.ratingsPromptEnabledSettingNameSuffix = "_RatingsEnabled";
exports.RatingsHelper = RatingsHelper;
