"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var userInfo_1 = require("../userInfo");
var browserUtils_1 = require("../browserUtils");
var constants_1 = require("../constants");
var objectUtils_1 = require("../objectUtils");
var polyfills_1 = require("../polyfills");
var tooltipType_1 = require("./tooltipType");
var urlUtils_1 = require("../urlUtils");
var communicator_1 = require("../communicator/communicator");
var iframeMessageHandler_1 = require("../communicator/iframeMessageHandler");
var inlineMessageHandler_1 = require("../communicator/inlineMessageHandler");
var smartValue_1 = require("../communicator/smartValue");
var augmentationHelper_1 = require("../contentCapture/augmentationHelper");
var bookmarkHelper_1 = require("../contentCapture/bookmarkHelper");
var fullPageScreenshotHelper_1 = require("../contentCapture/fullPageScreenshotHelper");
var pdfScreenshotHelper_1 = require("../contentCapture/pdfScreenshotHelper");
var domUtils_1 = require("../domParsers/domUtils");
var videoUtils_1 = require("../domParsers/videoUtils");
var invokeOptions_1 = require("../extensions/invokeOptions");
var inlineExtension_1 = require("../extensions/bookmarklet/inlineExtension");
var cachedHttp_1 = require("../http/cachedHttp");
var localization_1 = require("../localization/localization");
var Log = require("../logging/log");
var communicatorLoggerPure_1 = require("../logging/communicatorLoggerPure");
var oneNoteSaveableFactory_1 = require("../saveToOneNote/oneNoteSaveableFactory");
var saveToOneNote_1 = require("../saveToOneNote/saveToOneNote");
var saveToOneNoteLogger_1 = require("../saveToOneNote/saveToOneNoteLogger");
var clipperStorageKeys_1 = require("../storage/clipperStorageKeys");
var clipMode_1 = require("./clipMode");
var frontEndGlobals_1 = require("./frontEndGlobals");
var clipperStateUtilities_1 = require("./clipperStateUtilities");
var componentBase_1 = require("./componentBase");
var mainController_1 = require("./mainController");
var oneNoteApiUtils_1 = require("./oneNoteApiUtils");
var previewViewer_1 = require("./previewViewer");
var ratingsHelper_1 = require("./ratingsHelper");
var regionSelector_1 = require("./regionSelector");
var status_1 = require("./status");
var _ = require("lodash");
var ClipperClass = (function (_super) {
    __extends(ClipperClass, _super);
    function ClipperClass(props) {
        var _this = _super.call(this, props) || this;
        _this.isFullScreen = new smartValue_1.SmartValue(false);
        _this.initializeCommunicators();
        _this.initializeSmartValues();
        return _this;
    }
    ClipperClass.prototype.getInitialState = function () {
        var _this = this;
        return {
            uiExpanded: true,
            currentMode: new smartValue_1.SmartValue(this.getDefaultClipMode()),
            userResult: { status: status_1.Status.NotStarted },
            fullPageResult: { status: status_1.Status.NotStarted },
            pdfResult: { data: new smartValue_1.SmartValue(), status: status_1.Status.NotStarted },
            regionResult: { status: status_1.Status.NotStarted, data: [] },
            augmentationResult: { status: status_1.Status.NotStarted },
            oneNoteApiResult: { status: status_1.Status.NotStarted },
            bookmarkResult: { status: status_1.Status.NotStarted },
            setState: function (partialState) {
                _this.setState(partialState);
                _this.isFullScreen.set(ClipperClass.shouldShowPreviewViewer(_this.state) || ClipperClass.shouldShowRegionSelector(_this.state));
            },
            previewGlobalInfo: {
                annotation: "",
                fontSize: parseInt(localization_1.Localization.getLocalizedString("WebClipper.FontSize.Preview.SansSerifDefault"), 10 /* radix */),
                highlighterEnabled: false,
                serif: false
            },
            augmentationPreviewInfo: {},
            selectionPreviewInfo: {},
            pdfPreviewInfo: {
                allPages: true,
                isLocalFileAndNotAllowed: true,
                selectedPageRange: "",
                shouldAttachPdf: false,
                shouldDistributePages: false,
                shouldShowPopover: false
            },
            clipSaveStatus: {
                numItemsTotal: undefined,
                numItemsCompleted: undefined
            },
            reset: function () {
                _this.state.setState(_this.getResetState());
            }
        };
    };
    ClipperClass.prototype.getResetState = function () {
        return {
            currentMode: this.state.currentMode.set(this.getDefaultClipMode()),
            oneNoteApiResult: { status: status_1.Status.NotStarted }
        };
    };
    ClipperClass.prototype.initializeInjectCommunicator = function (pageInfo, clientInfo) {
        var _this = this;
        // Clear the inject no-op tracker
        frontEndGlobals_1.Clipper.getInjectCommunicator().registerFunction(constants_1.Constants.FunctionKeys.noOpTracker, function (trackerStartTime) {
            var clearNoOpTrackerEvent = new Log.Event.BaseEvent(Log.Event.Label.ClearNoOpTracker);
            clearNoOpTrackerEvent.setCustomProperty(Log.PropertyName.Custom.TimeToClearNoOpTracker, new Date().getTime() - trackerStartTime);
            clearNoOpTrackerEvent.setCustomProperty(Log.PropertyName.Custom.Channel, constants_1.Constants.CommunicationChannels.injectedAndUi);
            frontEndGlobals_1.Clipper.logger.logEvent(clearNoOpTrackerEvent);
            return Promise.resolve();
        });
        // Register functions for Inject
        frontEndGlobals_1.Clipper.getInjectCommunicator().registerFunction(constants_1.Constants.FunctionKeys.showRefreshClipperMessage, function (errorMessage) {
            if (!_this.state.badState) {
                frontEndGlobals_1.Clipper.logger.logFailure(Log.Failure.Label.OrphanedWebClippersDueToExtensionRefresh, Log.Failure.Type.Expected, { error: errorMessage });
                _this.state.setState({
                    badState: true
                });
            }
        });
        frontEndGlobals_1.Clipper.getInjectCommunicator().registerFunction(constants_1.Constants.FunctionKeys.toggleClipper, function () {
            _this.state.setState({ uiExpanded: !_this.state.uiExpanded });
        });
        frontEndGlobals_1.Clipper.getInjectCommunicator().registerFunction(constants_1.Constants.FunctionKeys.onSpaNavigate, function () {
            // This could have been called when the UI is already toggled off
            if (_this.state.uiExpanded) {
                var hideClipperDueToSpaNavigateEvent = new Log.Event.BaseEvent(Log.Event.Label.HideClipperDueToSpaNavigate);
                frontEndGlobals_1.Clipper.logger.logEvent(hideClipperDueToSpaNavigateEvent);
                _this.state.setState({ uiExpanded: false });
            }
        });
        frontEndGlobals_1.Clipper.getInjectCommunicator().registerFunction(constants_1.Constants.FunctionKeys.setInvokeOptions, function (options) {
            _this.setInvokeOptions(options);
        });
        // Register smartValues for Inject
        frontEndGlobals_1.Clipper.getInjectCommunicator().broadcastAcrossCommunicator(this.isFullScreen, constants_1.Constants.SmartValueKeys.isFullScreen);
        frontEndGlobals_1.Clipper.getInjectCommunicator().subscribeAcrossCommunicator(pageInfo, constants_1.Constants.SmartValueKeys.pageInfo, function (updatedPageInfo) {
            if (updatedPageInfo) {
                var newPreviewGlobalInfo = _.extend(_this.state.previewGlobalInfo, {
                    previewTitleText: updatedPageInfo.contentTitle
                });
                _this.state.setState({
                    pageInfo: updatedPageInfo,
                    previewGlobalInfo: newPreviewGlobalInfo
                });
                _this.capturePdfScreenshotContent();
                _this.captureFullPageScreenshotContent();
                _this.captureAugmentedContent();
                _this.captureBookmarkContent();
                frontEndGlobals_1.Clipper.logger.setContextProperty(Log.Context.Custom.ContentType, OneNoteApi.ContentType[updatedPageInfo.contentType]);
                frontEndGlobals_1.Clipper.logger.setContextProperty(Log.Context.Custom.InvokeHostname, urlUtils_1.UrlUtils.getHostname(updatedPageInfo.rawUrl));
                frontEndGlobals_1.Clipper.logger.setContextProperty(Log.Context.Custom.PageLanguage, updatedPageInfo.contentLocale);
            }
        });
        frontEndGlobals_1.Clipper.getInjectCommunicator().setErrorHandler(function (e) {
            Log.ErrorUtils.handleCommunicatorError(constants_1.Constants.CommunicationChannels.injectedAndUi, e, clientInfo);
        });
    };
    ClipperClass.prototype.capturePdfScreenshotContent = function () {
        var _this = this;
        // We don't capture anything. If the type is not EnhancedUrl, the mode will never show.
        if (this.state.pageInfo.contentType !== OneNoteApi.ContentType.EnhancedUrl) {
            return;
        }
        // The PDF isn't going to change on the same url, so we avoid multiple GETs in the same page
        if (this.state.pdfResult.status === status_1.Status.NotStarted) {
            // If network file, send XHR, get bytes back, convert to PDFDocumentProxy
            // If local file, get bytes back, convert to PDFDocumentProxy
            this.state.setState({ pdfResult: { data: new smartValue_1.SmartValue(undefined), status: status_1.Status.InProgress } });
            this.getPdfScreenshotResultFromRawUrl(this.state.pageInfo.rawUrl)
                .then(function (pdfScreenshotResult) {
                _this.state.pdfResult.data.set(pdfScreenshotResult);
                _this.state.setState({
                    pdfResult: {
                        data: _this.state.pdfResult.data,
                        status: status_1.Status.Succeeded
                    }
                });
            })["catch"](function () {
                _this.state.pdfResult.data.set({
                    failureMessage: localization_1.Localization.getLocalizedString("WebClipper.Preview.FullPageModeGenericError")
                });
                _this.state.setState({
                    pdfResult: {
                        data: _this.state.pdfResult.data,
                        status: status_1.Status.Failed
                    }
                });
                // The clip action might be waiting on the result, so do this to consistently trigger its callback
                _this.state.pdfResult.data.forceUpdate();
            });
        }
    };
    ClipperClass.prototype.getPdfScreenshotResultFromRawUrl = function (rawUrl) {
        if (rawUrl.indexOf("file:///") === 0) {
            return pdfScreenshotHelper_1.PdfScreenshotHelper.getLocalPdfData(rawUrl);
        }
        else {
            return pdfScreenshotHelper_1.PdfScreenshotHelper.getPdfData(rawUrl);
        }
    };
    ClipperClass.prototype.captureFullPageScreenshotContent = function () {
        var _this = this;
        if (this.state.pageInfo.contentType === OneNoteApi.ContentType.EnhancedUrl) {
            this.state.setState({
                fullPageResult: {
                    data: {
                        failureMessage: localization_1.Localization.getLocalizedString("WebClipper.Preview.NoContentFound")
                    },
                    status: status_1.Status.Failed
                }
            });
        }
        else {
            this.state.setState({ fullPageResult: { status: status_1.Status.InProgress } });
            fullPageScreenshotHelper_1.FullPageScreenshotHelper.getFullPageScreenshot(this.state.pageInfo.contentData).then(function (result) {
                _this.state.setState({ fullPageResult: { data: result, status: status_1.Status.Succeeded } });
            }, function () {
                _this.state.setState({
                    fullPageResult: {
                        data: {
                            failureMessage: localization_1.Localization.getLocalizedString("WebClipper.Preview.NoFullPageScreenshotFound")
                        },
                        status: status_1.Status.Failed
                    }
                });
            });
        }
    };
    ClipperClass.prototype.captureAugmentedContent = function () {
        var _this = this;
        if (this.state.pageInfo.contentType === OneNoteApi.ContentType.EnhancedUrl) {
            this.state.setState({
                augmentationResult: {
                    data: {
                        failureMessage: localization_1.Localization.getLocalizedString("WebClipper.Preview.NoContentFound")
                    },
                    status: status_1.Status.Failed
                }
            });
        }
        else {
            this.state.setState({ augmentationResult: { status: status_1.Status.InProgress } });
            var pageInfo = this.state.pageInfo;
            var augmentationUrl = pageInfo.canonicalUrl;
            if (pageInfo.rawUrl.indexOf("youtube.com") > -1 || pageInfo.rawUrl.indexOf("vimeo.com") > -1) {
                augmentationUrl = pageInfo.rawUrl;
            }
            augmentationHelper_1.AugmentationHelper.augmentPage(augmentationUrl, pageInfo.contentLocale, pageInfo.contentData).then(function (result) {
                result.ContentInHtml = domUtils_1.DomUtils.cleanHtml(result.ContentInHtml);
                _this.state.setState({
                    augmentationResult: { data: result, status: status_1.Status.Succeeded },
                    augmentationPreviewInfo: { previewBodyHtml: result.ContentInHtml }
                });
            }, function () {
                _this.state.setState({
                    augmentationResult: {
                        data: {
                            failureMessage: localization_1.Localization.getLocalizedString("WebClipper.Preview.AugmentationModeGenericError")
                        },
                        status: status_1.Status.Failed
                    }
                });
            });
        }
    };
    ClipperClass.prototype.captureBookmarkContent = function () {
        var _this = this;
        this.state.setState({ bookmarkResult: { status: status_1.Status.InProgress } });
        var pageInfo = this.state.pageInfo;
        var pageUrl = pageInfo.rawUrl;
        var pageTitle = pageInfo.contentTitle;
        var doc = domUtils_1.DomUtils.getDocumentFromDomString(pageInfo.contentData);
        var metadataElements = bookmarkHelper_1.BookmarkHelper.getElementsByTagName(doc, bookmarkHelper_1.BookmarkHelper.metadataTagNames);
        var imageElements = bookmarkHelper_1.BookmarkHelper.getElementsByTagName(doc.body, [domUtils_1.DomUtils.tags.img]);
        // because we are cleaning the document in order to get the cleanest text possible in our description,
        // make sure this is the last operation being performed on the document
        var textElements = [];
        try {
            textElements = bookmarkHelper_1.BookmarkHelper.getNonWhiteSpaceTextElements(doc, true /* cleanDoc */);
        }
        catch (e) {
        }
        bookmarkHelper_1.BookmarkHelper.bookmarkPage(pageUrl, pageTitle, metadataElements, true /* allowFallback */, imageElements, textElements).then(function (result) {
            _this.state.setState({
                bookmarkResult: { data: result, status: status_1.Status.Succeeded }
            });
        }, function (error) {
            _this.state.setState({
                bookmarkResult: {
                    data: {
                        url: error.url,
                        title: pageTitle,
                        description: error.description,
                        thumbnailSrc: error.thumbnailSrc,
                        failureMessage: localization_1.Localization.getLocalizedString("WebClipper.Preview.BookmarkModeGenericError")
                    },
                    status: status_1.Status.Failed
                }
            });
        });
    };
    ClipperClass.prototype.setInvokeOptions = function (options) {
        this.setState({ invokeOptions: options });
        // This needs to happen after the invokeOptions set as it is reliant on that order
        this.setState({ currentMode: this.state.currentMode.set(this.getDefaultClipMode()) });
        // We assume that invokeDataForMode is always a non-undefined value where it makes sense
        // and that it's the background's responsibility to ensure that is the case
        switch (options.invokeMode) {
            case invokeOptions_1.InvokeMode.ContextImage:
                // invokeDataForMode is the src url
                this.setState({
                    regionResult: {
                        data: [options.invokeDataForMode],
                        status: status_1.Status.Succeeded
                    }
                });
                break;
            case invokeOptions_1.InvokeMode.ContextTextSelection:
                // invokeDataForMode is scrubbed selected html as a string
                this.state.setState({
                    selectionPreviewInfo: {
                        previewBodyHtml: options.invokeDataForMode
                    }
                });
                break;
            default:
                break;
        }
    };
    ClipperClass.prototype.initializeExtensionCommunicator = function (clientInfo) {
        var _this = this;
        // Clear the extension no-op tracker
        frontEndGlobals_1.Clipper.getExtensionCommunicator().registerFunction(constants_1.Constants.FunctionKeys.noOpTracker, function (trackerStartTime) {
            var clearNoOpTrackerEvent = new Log.Event.BaseEvent(Log.Event.Label.ClearNoOpTracker);
            clearNoOpTrackerEvent.setCustomProperty(Log.PropertyName.Custom.TimeToClearNoOpTracker, new Date().getTime() - trackerStartTime);
            clearNoOpTrackerEvent.setCustomProperty(Log.PropertyName.Custom.Channel, constants_1.Constants.CommunicationChannels.extensionAndUi);
            frontEndGlobals_1.Clipper.logger.logEvent(clearNoOpTrackerEvent);
            return Promise.resolve();
        });
        frontEndGlobals_1.Clipper.getExtensionCommunicator().registerFunction(constants_1.Constants.FunctionKeys.createHiddenIFrame, function (url) {
            browserUtils_1.BrowserUtils.appendHiddenIframeToDocument(url);
        });
        var userInfoUpdateCb = function (updatedUser) {
            if (updatedUser) {
                var userInfoUpdatedEvent = new Log.Event.BaseEvent(Log.Event.Label.UserInfoUpdated);
                userInfoUpdatedEvent.setCustomProperty(Log.PropertyName.Custom.UserUpdateReason, userInfo_1.UpdateReason[updatedUser.updateReason]);
                userInfoUpdatedEvent.setCustomProperty(Log.PropertyName.Custom.LastUpdated, new Date(updatedUser.lastUpdated).toUTCString());
                frontEndGlobals_1.Clipper.logger.logEvent(userInfoUpdatedEvent);
            }
            if (updatedUser && updatedUser.user) {
                var timeStampedData = {
                    data: updatedUser.user,
                    lastUpdated: updatedUser.lastUpdated
                };
                // The user SV should never be set with expired user information
                var tokenHasExpiredForLoggedInUser = cachedHttp_1.CachedHttp.valueHasExpired(timeStampedData, (updatedUser.user.accessTokenExpiration * 1000) - 180000);
                if (tokenHasExpiredForLoggedInUser) {
                    frontEndGlobals_1.Clipper.logger.logFailure(Log.Failure.Label.UserSetWithInvalidExpiredData, Log.Failure.Type.Unexpected);
                }
                _this.state.setState({ userResult: { status: status_1.Status.Succeeded, data: updatedUser } });
                frontEndGlobals_1.Clipper.logger.setContextProperty(Log.Context.Custom.AuthType, updatedUser.user.authType);
                frontEndGlobals_1.Clipper.logger.setContextProperty(Log.Context.Custom.UserInfoId, updatedUser.user.cid);
            }
            else {
                _this.state.setState({ userResult: { status: status_1.Status.Failed, data: updatedUser } });
            }
        };
        this.state.setState({ userResult: { status: status_1.Status.InProgress } });
        frontEndGlobals_1.Clipper.getExtensionCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.getInitialUser, {
            callback: function (freshInitialUser) {
                if (freshInitialUser && freshInitialUser.user) {
                    frontEndGlobals_1.Clipper.logger.logUserFunnel(Log.Funnel.Label.AuthAlreadySignedIn);
                }
                else if (!freshInitialUser) {
                    userInfoUpdateCb(freshInitialUser);
                }
                frontEndGlobals_1.Clipper.getExtensionCommunicator().subscribeAcrossCommunicator(new smartValue_1.SmartValue(), constants_1.Constants.SmartValueKeys.user, function (updatedUser) {
                    userInfoUpdateCb(updatedUser);
                });
            }
        });
        this.state.setState({ fetchLocStringStatus: status_1.Status.InProgress });
        frontEndGlobals_1.Clipper.getExtensionCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.clipperStrings, {
            callback: function (data) {
                if (data) {
                    localization_1.Localization.setLocalizedStrings(data);
                }
                _this.state.setState({ fetchLocStringStatus: status_1.Status.Succeeded });
            }
        });
        frontEndGlobals_1.Clipper.getExtensionCommunicator().registerFunction(constants_1.Constants.FunctionKeys.extensionNotAllowedToAccessLocalFiles, function () {
            // We only want to log one time per session
            if (_this.state.pdfPreviewInfo.isLocalFileAndNotAllowed) {
                frontEndGlobals_1.Clipper.logger.logEvent(new Log.Event.BaseEvent(Log.Event.Label.LocalFilesNotAllowedPanelShown));
            }
            _.assign(_.extend(_this.state.pdfPreviewInfo, {
                isLocalFileAndNotAllowed: false
            }), _this.state.setState);
        });
        frontEndGlobals_1.Clipper.getExtensionCommunicator().subscribeAcrossCommunicator(clientInfo, constants_1.Constants.SmartValueKeys.clientInfo, function (updatedClientInfo) {
            if (updatedClientInfo) {
                _this.state.setState({
                    clientInfo: updatedClientInfo
                });
            }
        });
        frontEndGlobals_1.Clipper.getExtensionCommunicator().setErrorHandler(function (e) {
            Log.ErrorUtils.handleCommunicatorError(constants_1.Constants.CommunicationChannels.extensionAndUi, e, clientInfo);
        });
    };
    ClipperClass.prototype.initializeCommunicators = function () {
        var _this = this;
        var pageInfo = new smartValue_1.SmartValue();
        var clientInfo = new smartValue_1.SmartValue();
        frontEndGlobals_1.Clipper.setInjectCommunicator(new communicator_1.Communicator(new iframeMessageHandler_1.IFrameMessageHandler(function () { return parent; }), constants_1.Constants.CommunicationChannels.injectedAndUi));
        // Check the options passed in to determine what kind of Communicator we need to talk to the background task
        frontEndGlobals_1.Clipper.getInjectCommunicator().registerFunction(constants_1.Constants.FunctionKeys.setInjectOptions, function (options) {
            _this.setState({ injectOptions: options });
            if (_this.state.injectOptions.useInlineBackgroundWorker) {
                var background = new inlineExtension_1.InlineExtension();
                var backgroundMessageHandler = background.getInlineMessageHandler();
                var uiMessageHandler = new inlineMessageHandler_1.InlineMessageHandler(backgroundMessageHandler);
                backgroundMessageHandler.setOtherSide(uiMessageHandler);
                frontEndGlobals_1.Clipper.setExtensionCommunicator(new communicator_1.Communicator(uiMessageHandler, constants_1.Constants.CommunicationChannels.extensionAndUi));
            }
            else {
                frontEndGlobals_1.Clipper.setExtensionCommunicator(new communicator_1.Communicator(new iframeMessageHandler_1.IFrameMessageHandler(function () { return parent; }), constants_1.Constants.CommunicationChannels.extensionAndUi));
            }
            _this.initializeExtensionCommunicator(clientInfo);
            frontEndGlobals_1.Clipper.getExtensionCommunicator().subscribeAcrossCommunicator(frontEndGlobals_1.Clipper.sessionId, constants_1.Constants.SmartValueKeys.sessionId);
            frontEndGlobals_1.Clipper.logger = new communicatorLoggerPure_1.CommunicatorLoggerPure(frontEndGlobals_1.Clipper.getExtensionCommunicator());
            _this.initializeInjectCommunicator(pageInfo, clientInfo);
            // When tabbing from outside the iframe, we want to set focus to the lowest tabindex element in our iframe
            frontEndGlobals_1.Clipper.getInjectCommunicator().registerFunction(constants_1.Constants.FunctionKeys.tabToLowestIndexedElement, function () {
                var tabbables = document.querySelectorAll("[tabindex]");
                var lowestTabIndexElement;
                if (tabbables.length > 0) {
                    for (var i = 0; i < tabbables.length; i++) {
                        var tabbable = tabbables[i];
                        if (!lowestTabIndexElement || tabbable.tabIndex < lowestTabIndexElement.tabIndex) {
                            if (tabbable.tabIndex >= 0) {
                                lowestTabIndexElement = tabbable;
                            }
                        }
                    }
                    lowestTabIndexElement.focus();
                }
            });
            // initialize here since it depends on storage in the extension
            _this.initializeNumSuccessfulClips();
            ratingsHelper_1.RatingsHelper.preCacheNeededValues();
        });
        clientInfo.subscribe(function (updatedClientInfo) {
            if (updatedClientInfo) {
                // The default Clip mode now also depends on clientInfo, in addition to pageInfo
                // TODO: Don't do this if they already have a mode chosen (once we are updating the pageInfo more object more often)
                _this.state.setState({ currentMode: _this.state.currentMode.set(_this.getDefaultClipMode()) });
            }
        });
    };
    ClipperClass.prototype.initializeSmartValues = function () {
        this.state.currentMode.subscribe(function (newMode) {
            switch (newMode) {
                case clipMode_1.ClipMode.FullPage:
                case clipMode_1.ClipMode.Augmentation:
                    frontEndGlobals_1.Clipper.getInjectCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.updatePageInfoIfUrlChanged);
                    break;
                default:
                    break;
            }
        }, { callOnSubscribe: false });
    };
    ClipperClass.prototype.initializeNumSuccessfulClips = function () {
        var _this = this;
        frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClips, function (numClipsAsStr) {
            var numClips = parseInt(numClipsAsStr, 10);
            _this.state.numSuccessfulClips = objectUtils_1.ObjectUtils.isNullOrUndefined(numClips) || isNaN(numClips) ? 0 : numClips;
        });
    };
    ClipperClass.prototype.getDefaultClipMode = function () {
        if (this.state && this.state.invokeOptions) {
            switch (this.state.invokeOptions.invokeMode) {
                case invokeOptions_1.InvokeMode.ContextImage:
                    // We don't want to be stuck in region mode if there are 0 images
                    if (this.state.regionResult.data.length > 0) {
                        return clipMode_1.ClipMode.Region;
                    }
                    break;
                case invokeOptions_1.InvokeMode.ContextTextSelection:
                    return clipMode_1.ClipMode.Selection;
                default:
                    break;
            }
        }
        if (this.state && this.state.pageInfo) {
            if (this.state.pageInfo.contentType === OneNoteApi.ContentType.EnhancedUrl) {
                return clipMode_1.ClipMode.Pdf;
            }
            if (urlUtils_1.UrlUtils.onWhitelistedDomain(this.state.pageInfo.rawUrl)) {
                return clipMode_1.ClipMode.Augmentation;
            }
        }
        return clipMode_1.ClipMode.FullPage;
    };
    ClipperClass.prototype.updateFrameHeight = function (newContainerHeight) {
        frontEndGlobals_1.Clipper.getInjectCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.updateFrameHeight, { param: newContainerHeight });
    };
    ClipperClass.prototype.handleSignIn = function (authType) {
        var _this = this;
        frontEndGlobals_1.Clipper.logger.logUserFunnel(Log.Funnel.Label.AuthAttempted);
        var handleSignInEvent = new Log.Event.PromiseEvent(Log.Event.Label.HandleSignInEvent);
        this.setState({ userResult: { status: status_1.Status.InProgress } });
        frontEndGlobals_1.Clipper.getExtensionCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.signInUser, {
            param: authType, callback: function (data) {
                // For cleaner referencing
                // TODO: This kind of thing should go away as we move the communicator to be Promise based.
                var updatedUser = data;
                var errorObject = data;
                var errorsFound = errorObject.error || errorObject.errorDescription;
                if (errorsFound) {
                    errorObject.errorDescription = userInfo_1.AuthType[authType] + ": " + errorObject.error + ": " + errorObject.errorDescription;
                    _this.state.setState({ userResult: { status: status_1.Status.Failed, data: errorObject } });
                    handleSignInEvent.setStatus(Log.Status.Failed);
                    handleSignInEvent.setFailureInfo({ error: errorObject.errorDescription });
                    handleSignInEvent.setCustomProperty(Log.PropertyName.Custom.CorrelationId, errorObject.correlationId);
                    frontEndGlobals_1.Clipper.logger.logUserFunnel(Log.Funnel.Label.AuthSignInFailed);
                }
                var userInfoReturned = updatedUser && !!updatedUser.user;
                if (userInfoReturned) {
                    frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.hasPatchPermissions, "true");
                    frontEndGlobals_1.Clipper.logger.logUserFunnel(Log.Funnel.Label.AuthSignInCompleted);
                }
                handleSignInEvent.setCustomProperty(Log.PropertyName.Custom.UserInformationReturned, userInfoReturned);
                handleSignInEvent.setCustomProperty(Log.PropertyName.Custom.SignInCancelled, !errorsFound && !userInfoReturned);
                frontEndGlobals_1.Clipper.logger.logEvent(handleSignInEvent);
            }
        });
    };
    ClipperClass.prototype.handleSignOut = function (authType) {
        this.state.setState(this.getSignOutState());
        frontEndGlobals_1.Clipper.getExtensionCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.signOutUser, { param: userInfo_1.AuthType[authType] });
        frontEndGlobals_1.Clipper.logger.logUserFunnel(Log.Funnel.Label.SignOut);
        frontEndGlobals_1.Clipper.logger.logSessionEnd(Log.Session.EndTrigger.SignOut);
        frontEndGlobals_1.Clipper.logger.logSessionStart();
    };
    ClipperClass.prototype.getSignOutState = function () {
        var signOutState = this.getResetState();
        signOutState.saveLocation = undefined;
        signOutState.userResult = undefined;
        return signOutState;
    };
    ClipperClass.prototype.handleStartClip = function () {
        var _this = this;
        frontEndGlobals_1.Clipper.logger.logUserFunnel(Log.Funnel.Label.ClipAttempted);
        this.state.setState({ userResult: { status: status_1.Status.InProgress, data: this.state.userResult.data } });
        frontEndGlobals_1.Clipper.getExtensionCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.ensureFreshUserBeforeClip, { callback: function (updatedUser) {
                if (updatedUser && updatedUser.user) {
                    var currentMode = _this.state.currentMode.get();
                    if (currentMode === clipMode_1.ClipMode.FullPage) {
                        // A page info refresh needs to be triggered if the url has changed right before the clip action
                        frontEndGlobals_1.Clipper.getInjectCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.updatePageInfoIfUrlChanged, {
                            callback: function () {
                                _this.startClip();
                            }
                        });
                    }
                    else if (currentMode === clipMode_1.ClipMode.Bookmark) {
                        // set the rendered bookmark preview HTML as the exact HTML to send to OneNote
                        var previewBodyHtml = document.getElementById("previewBody").innerHTML;
                        _this.state.setState({
                            bookmarkPreviewInfo: { previewBodyHtml: previewBodyHtml }
                        });
                        _this.startClip();
                    }
                    else {
                        _this.startClip();
                    }
                }
            } });
    };
    ClipperClass.prototype.startClip = function () {
        var _this = this;
        this.state.setState({ oneNoteApiResult: { status: status_1.Status.InProgress } });
        this.storeLastClippedInformation();
        saveToOneNoteLogger_1.SaveToOneNoteLogger.logClip(this.state);
        var clipEvent = new Log.Event.PromiseEvent(Log.Event.Label.ClipToOneNoteAction);
        (new oneNoteSaveableFactory_1.OneNoteSaveableFactory(this.state)).getSaveable().then(function (saveable) {
            var saveOptions = {
                page: saveable,
                saveLocation: _this.state.saveLocation,
                progressCallback: _this.updateClipSaveProgress.bind(_this)
            };
            var saveToOneNote = new saveToOneNote_1.SaveToOneNote(_this.state.userResult.data.user.accessToken);
            saveToOneNote.save(saveOptions).then(function (responsePackage) {
                var createPageResponse = Array.isArray(responsePackage) ? responsePackage[0] : responsePackage;
                clipEvent.setCustomProperty(Log.PropertyName.Custom.CorrelationId, createPageResponse.request.getResponseHeader(constants_1.Constants.HeaderValues.correlationId));
                var numSuccessfulClips = _this.state.numSuccessfulClips + 1;
                frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.numSuccessfulClips, numSuccessfulClips.toString());
                _this.state.setState({
                    oneNoteApiResult: { data: createPageResponse.parsedResponse, status: status_1.Status.Succeeded },
                    numSuccessfulClips: numSuccessfulClips,
                    showRatingsPrompt: ratingsHelper_1.RatingsHelper.shouldShowRatingsPrompt(_this.state)
                });
            })["catch"](function (error) {
                oneNoteApiUtils_1.OneNoteApiUtils.logOneNoteApiRequestError(clipEvent, error);
                _this.state.setState({ oneNoteApiResult: { data: error, status: status_1.Status.Failed } });
            }).then(function () {
                frontEndGlobals_1.Clipper.logger.logEvent(clipEvent);
            });
        });
    };
    ClipperClass.prototype.updateClipSaveProgress = function (numItemsCompleted, numItemsTotal) {
        this.state.setState({
            clipSaveStatus: {
                numItemsCompleted: numItemsCompleted,
                numItemsTotal: numItemsTotal
            }
        });
    };
    ClipperClass.prototype.storeLastClippedInformation = function () {
        frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastClippedDate, Date.now().toString());
        if (this.state.currentMode.get() === clipMode_1.ClipMode.Pdf) {
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastClippedTooltipTimeBase + tooltipType_1.TooltipType[tooltipType_1.TooltipType.Pdf], Date.now().toString());
        }
        if (this.state.currentMode.get() === clipMode_1.ClipMode.Augmentation) {
            // Record lastClippedDate for each different augmentationMode so we can upsell the augmentation mode
            // to users who haven't Clipped this mode in a while
            var augmentationTypeAsString = augmentationHelper_1.AugmentationHelper.getAugmentationType(this.state);
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastClippedTooltipTimeBase + augmentationTypeAsString, Date.now().toString());
        }
        if (videoUtils_1.VideoUtils.videoDomainIfSupported(this.state.pageInfo.rawUrl)) {
            frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.lastClippedTooltipTimeBase + tooltipType_1.TooltipType[tooltipType_1.TooltipType.Video], Date.now().toString());
        }
    };
    ClipperClass.shouldShowOptions = function (state) {
        return (state.uiExpanded &&
            clipperStateUtilities_1.ClipperStateUtilities.isUserLoggedIn(state) &&
            state.oneNoteApiResult.status === status_1.Status.NotStarted &&
            !state.badState);
    };
    ClipperClass.shouldShowPreviewViewer = function (state) {
        return (this.shouldShowOptions(state) &&
            (state.currentMode.get() !== clipMode_1.ClipMode.Region ||
                state.regionResult.status === status_1.Status.Succeeded));
    };
    ClipperClass.shouldShowRegionSelector = function (state) {
        return (this.shouldShowOptions(state) &&
            state.currentMode.get() === clipMode_1.ClipMode.Region &&
            state.regionResult.status !== status_1.Status.Succeeded);
    };
    ClipperClass.shouldShowMainController = function (state) {
        return state.regionResult.status !== status_1.Status.InProgress || state.badState;
    };
    ClipperClass.prototype.render = function () {
        var previewViewerItem = ClipperClass.shouldShowPreviewViewer(this.state) ?
            m.component(previewViewer_1.PreviewViewer, {clipperState:this.state}) :
            undefined;
        var regionSelectorItem = ClipperClass.shouldShowRegionSelector(this.state) ? m.component(regionSelector_1.RegionSelector, {clipperState:this.state}) : undefined;
        var mainControllerStyle = ClipperClass.shouldShowMainController(this.state) ? {} : { display: "none" };
        return ({tag: "div", attrs: {}, children: [
				previewViewerItem, 
				regionSelectorItem, 
				{tag: "div", attrs: {style:mainControllerStyle}, children: [
					m.component(mainController_1.MainController, {clipperState:this.state, onSignInInvoked:this.handleSignIn.bind(this), onSignOutInvoked:this.handleSignOut.bind(this), updateFrameHeight:this.updateFrameHeight.bind(this), onStartClip:this.handleStartClip.bind(this)})
				]}
			]});
    };
    return ClipperClass;
}(componentBase_1.ComponentBase));
polyfills_1.Polyfills.init();
// Catch any unhandled exceptions and log them
var oldOnError = window.onerror;
window.onerror = function (message, filename, lineno, colno, error) {
    var callStack = error ? Log.Failure.getStackTrace(error) : "[unknown stacktrace]";
    frontEndGlobals_1.Clipper.logger.logFailure(Log.Failure.Label.UnhandledExceptionThrown, Log.Failure.Type.Unexpected, { error: message + " (" + filename + ":" + lineno + ":" + colno + ") at " + callStack }, "ClipperUI");
    if (oldOnError) {
        oldOnError(message, filename, lineno, colno, error);
    }
};
var component = ClipperClass.componentize();
exports.Clipper = component;
m.mount(document.getElementById("clipperUIPlaceholder"), component);
