"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var _ = require("lodash");
var constants_1 = require("../../constants");
var extensionUtils_1 = require("../../extensions/extensionUtils");
var localization_1 = require("../../localization/localization");
var componentBase_1 = require("../componentBase");
var AnnotationInputClass = (function (_super) {
    __extends(AnnotationInputClass, _super);
    function AnnotationInputClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AnnotationInputClass.prototype.getInitialState = function () {
        return { opened: !!this.props.clipperState.previewGlobalInfo.annotation };
    };
    AnnotationInputClass.prototype.handleAnnotateButton = function () {
        this.setState({ opened: !this.state.opened });
    };
    AnnotationInputClass.prototype.setFocus = function (textArea) {
        textArea.focus();
    };
    // TODO: change this to a config passed into the textarea?
    AnnotationInputClass.prototype.addTextAreaListener = function () {
        var _this = this;
        document.addEventListener("input", function (event) {
            var element = event.target;
            var annotationField = document.getElementById(constants_1.Constants.Ids.annotationField);
            if (!!element && element === annotationField) {
                _this.handleAnnotationFieldChanged(annotationField.value);
            }
        });
    };
    AnnotationInputClass.prototype.handleAnnotationFieldChanged = function (annotationValue) {
        this.props.clipperState.setState({
            previewGlobalInfo: {
                previewTitleText: this.props.clipperState.previewGlobalInfo.previewTitleText,
                annotation: annotationValue,
                fontSize: this.props.clipperState.previewGlobalInfo.fontSize,
                serif: this.props.clipperState.previewGlobalInfo.serif
            }
        });
    };
    AnnotationInputClass.prototype.onDoneEditing = function (e) {
        var _this = this;
        var value = e.target.value.trim();
        _.assign(_.extend(this.props.clipperState.previewGlobalInfo, {
            annotation: value
        }), this.props.clipperState.setState);
        // We do this as if we trigger this on the mousedown instead, the hide causes some buttons to
        // reposition themselves, and we cannot guarantee that the subsequent mouseup will be on the
        // button the user originally intended to click
        if (value === "") {
            var nextMouseupEvent_1 = function () {
                _this.setState({ opened: false });
                window.removeEventListener("mouseup", nextMouseupEvent_1);
            };
            window.addEventListener("mouseup", nextMouseupEvent_1);
        }
    };
    AnnotationInputClass.prototype.render = function () {
        if (!AnnotationInputClass.textAreaListenerAttached) {
            this.addTextAreaListener();
            AnnotationInputClass.textAreaListenerAttached = true;
        }
        if (!this.state.opened) {
            return (<div id={constants_1.Constants.Ids.annotationContainer}>
					<div id={constants_1.Constants.Ids.annotationPlaceholder} style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)} {...this.enableInvoke({ callback: this.handleAnnotateButton, tabIndex: 210 })} role="button">
						<img src={extensionUtils_1.ExtensionUtils.getImageResourceUrl("editorOptions/add_icon_purple.png")}/>
						<span aria-label={localization_1.Localization.getLocalizedString("WebClipper.Label.AnnotationPlaceholder")}>{localization_1.Localization.getLocalizedString("WebClipper.Label.AnnotationPlaceholder")} </span>
					</div>
				</div>);
        }
        else {
            return (<div id={constants_1.Constants.Ids.annotationContainer}>
					<pre id={constants_1.Constants.Ids.annotationFieldMirror} className={constants_1.Constants.Classes.textAreaInputMirror}>
						<span style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)}>
							{!!this.props.clipperState.previewGlobalInfo.annotation ? this.props.clipperState.previewGlobalInfo.annotation : ""}
						</span>
						<br />
					</pre>
					<textarea id={constants_1.Constants.Ids.annotationField} className={constants_1.Constants.Classes.textAreaInput} role="textbox" rows={1} tabIndex={211} aria-label={localization_1.Localization.getLocalizedString("WebClipper.Accessibility.ScreenReader.InputBoxToChangeTitleOfOneNotePage")} style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)} value={!!this.props.clipperState.previewGlobalInfo.annotation ? this.props.clipperState.previewGlobalInfo.annotation : ""} onblur={this.onDoneEditing.bind(this)} {...this.onElementFirstDraw(this.setFocus)}>
					</textarea>
				</div>);
        }
    };
    return AnnotationInputClass;
}(componentBase_1.ComponentBase));
AnnotationInputClass.textAreaListenerAttached = false;
var component = AnnotationInputClass.componentize();
exports.AnnotationInput = component;
