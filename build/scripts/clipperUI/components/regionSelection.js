"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Log = require("../../logging/log");
var extensionUtils_1 = require("../../extensions/extensionUtils");
var componentBase_1 = require("../componentBase");
var RegionSelectionClass = (function (_super) {
    __extends(RegionSelectionClass, _super);
    function RegionSelectionClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RegionSelectionClass.prototype.buttonHandler = function () {
        if (this.props.onRemove) {
            this.props.onRemove(this.props.index);
        }
    };
    RegionSelectionClass.prototype.getRemoveButton = function () {
        // No remove button is rendered if there's no callback specified
        return (this.props.onRemove
            ? {tag: "a", attrs: Object.assign({className:"region-selection-remove-button", role:"button"},  this.enableInvoke({ callback: this.buttonHandler, tabIndex: 300, idOverride: Log.Click.Label.regionSelectionRemoveButton })), children: [
					{tag: "img", attrs: {src:extensionUtils_1.ExtensionUtils.getImageResourceUrl("editorOptions/delete_button.png")}}]}
            : undefined);
    };
    RegionSelectionClass.prototype.render = function () {
        return ({tag: "div", attrs: {}, children: [
				{tag: "p", attrs: {className:"region-selection"}, children: [
					this.getRemoveButton(), 
					{tag: "img", attrs: {className:"region-selection-image", src:this.props.imageSrc}}
				]}
			]});
    };
    return RegionSelectionClass;
}(componentBase_1.ComponentBase));
var component = RegionSelectionClass.componentize();
exports.RegionSelection = component;
