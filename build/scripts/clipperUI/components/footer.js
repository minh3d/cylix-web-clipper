"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var browserUtils_1 = require("../../browserUtils");
var clientType_1 = require("../../clientType");
var clipperUrls_1 = require("../../clipperUrls");
var constants_1 = require("../../constants");
var extensionUtils_1 = require("../../extensions/extensionUtils");
var localization_1 = require("../../localization/localization");
var Log = require("../../logging/log");
var frontEndGlobals_1 = require("../frontEndGlobals");
var componentBase_1 = require("../componentBase");
var FooterClass = (function (_super) {
    __extends(FooterClass, _super);
    function FooterClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FooterClass.prototype.getFeedbackWindowRef = function () {
        return this.feedbackWindowRef;
    };
    FooterClass.prototype.getInitialState = function () {
        return { userSettingsOpened: false };
    };
    FooterClass.prototype.userControlHandler = function () {
        this.setState({ userSettingsOpened: !this.state.userSettingsOpened });
    };
    FooterClass.prototype.handleSignOutButton = function () {
        this.setState({ userSettingsOpened: false });
        if (this.props.onSignOutInvoked) {
            this.props.onSignOutInvoked(this.props.clipperState.userResult.data.user.authType);
        }
    };
    FooterClass.prototype.handleFeedbackButton = function (args, event) {
        // In order to make it easy to collect some information from the user, we're hijacking
        // the feedback button to show the relevant info.
        if (event.altKey && event.shiftKey) {
            var debugMessage = clientType_1.ClientType[this.props.clipperState.clientInfo.clipperType] + ": " + this.props.clipperState.clientInfo.clipperVersion;
            debugMessage += "\nID: " + this.props.clipperState.clientInfo.clipperId;
            debugMessage += "\nUsid: " + frontEndGlobals_1.Clipper.getUserSessionId();
            frontEndGlobals_1.Clipper.logger.logEvent(new Log.Event.BaseEvent(Log.Event.Label.DebugFeedback));
            window.alert(debugMessage);
            return;
        }
        if (!this.feedbackWindowRef || this.feedbackWindowRef.closed) {
            if (!this.feedbackUrl) {
                this.feedbackUrl = clipperUrls_1.ClipperUrls.generateFeedbackUrl(this.props.clipperState, frontEndGlobals_1.Clipper.getUserSessionId(), constants_1.Constants.LogCategories.oneNoteClipperUsage);
            }
            this.feedbackWindowRef = browserUtils_1.BrowserUtils.openPopupWindow(this.feedbackUrl);
        }
    };
    FooterClass.prototype.render = function () {
        return ({tag: "div", attrs: {id:constants_1.Constants.Ids.clipperFooterContainer, className:"footerFont", style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)}, children: [
				{tag: "div", attrs: {className:constants_1.Constants.Ids.footerButtonsContainer}, children: [
					{tag: "div", attrs: {className:"footerButtonsLeft"}, children: [
						{tag: "a", attrs: Object.assign({id:constants_1.Constants.Ids.feedbackButton, role:"button"},  this.enableInvoke({ callback: this.handleFeedbackButton, tabIndex: 80 })), children: [
							{tag: "img", attrs: {id:constants_1.Constants.Ids.feedbackImage, src:extensionUtils_1.ExtensionUtils.getImageResourceUrl("feedback_smiley.png")}}, 
							{tag: "span", attrs: {id:constants_1.Constants.Ids.feedbackLabel}, children: [localization_1.Localization.getLocalizedString("WebClipper.Action.Feedback")]}
						]}
					]}
				]}
			]});
    };
    return FooterClass;
}(componentBase_1.ComponentBase));
var component = FooterClass.componentize();
exports.Footer = component;
