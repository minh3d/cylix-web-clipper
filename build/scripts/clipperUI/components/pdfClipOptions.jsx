"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var localization_1 = require("../../localization/localization");
var constants_1 = require("../../constants");
var extensionUtils_1 = require("../../extensions/extensionUtils");
var componentBase_1 = require("../componentBase");
var status_1 = require("../status");
var animationHelper_1 = require("../animations/animationHelper");
var animationState_1 = require("../animations/animationState");
var fadeInAnimationStrategy_1 = require("../animations/fadeInAnimationStrategy");
var pdfPageSelectionRadioButton_1 = require("./pdfPageSelectionRadioButton");
var _ = require("lodash");
var PdfClipOptionsClass = (function (_super) {
    __extends(PdfClipOptionsClass, _super);
    function PdfClipOptionsClass(props) {
        var _this = _super.call(this, props) || this;
        _this.hiddenOptionsAnimationStrategy = new fadeInAnimationStrategy_1.FadeInAnimationStrategy({
            extShouldAnimateIn: function () { return _this.state.moreOptionsOpened; },
            extShouldAnimateOut: function () { return !_this.state.moreOptionsOpened; }
        });
        return _this;
    }
    PdfClipOptionsClass.prototype.getInitialState = function () {
        return {
            moreOptionsOpened: false
        };
    };
    PdfClipOptionsClass.prototype.onCheckboxChange = function (checked) {
        var pdfHasSucceeded = this.props.clipperState.pdfResult.status === status_1.Status.Succeeded;
        var pdfIsTooLarge = pdfHasSucceeded && this.props.clipperState.pdfResult.data.get().byteLength > constants_1.Constants.Settings.maximumMimeSizeLimit;
        if (!pdfHasSucceeded || pdfIsTooLarge) {
            return;
        }
        _.assign(_.extend(this.props.clipperState.pdfPreviewInfo, {
            shouldAttachPdf: checked
        }), this.props.clipperState.setState);
    };
    PdfClipOptionsClass.prototype.onDistributionChange = function (checked) {
        _.assign(_.extend(this.props.clipperState.pdfPreviewInfo, {
            shouldDistributePages: checked
        }), this.props.clipperState.setState);
    };
    PdfClipOptionsClass.prototype.onMoreClicked = function () {
        this.setState({
            moreOptionsOpened: !this.state.moreOptionsOpened
        });
    };
    PdfClipOptionsClass.prototype.getDistributePagesCheckbox = function () {
        var pdfPreviewInfo = this.props.clipperState.pdfPreviewInfo;
        return (<div className="pdf-control" id={constants_1.Constants.Ids.checkboxToDistributePages} aria-checked={pdfPreviewInfo.shouldDistributePages} {...this.enableInvoke({ callback: this.onDistributionChange, tabIndex: 65, args: !pdfPreviewInfo.shouldDistributePages })}>
				<div className="pdf-indicator pdf-checkbox-indicator"></div>
				{pdfPreviewInfo.shouldDistributePages ? <div className={constants_1.Constants.Classes.checkboxCheck}></div> : ""}
				<div className="pdf-label-margin">
					<span className="pdf-label focused">{localization_1.Localization.getLocalizedString("WebClipper.Label.PdfDistributePagesCheckbox")}</span>
				</div>
			</div>);
    };
    PdfClipOptionsClass.prototype.getAttachmentCheckbox = function () {
        var pdfHasSucceeded = this.props.clipperState.pdfResult.status === status_1.Status.Succeeded;
        var pdfIsTooLarge = pdfHasSucceeded && this.props.clipperState.pdfResult.data.get().byteLength > constants_1.Constants.Settings.maximumMimeSizeLimit;
        var disableCheckbox = pdfIsTooLarge || !pdfHasSucceeded;
        if (pdfIsTooLarge) {
            return this.getAttachmentIsTooLargeCheckbox();
        }
        return this.getAttachmentPdfCheckbox(pdfHasSucceeded);
    };
    PdfClipOptionsClass.prototype.getAttachmentIsTooLargeCheckbox = function () {
        var disabledClassName = " disabled";
        return (<div className={"pdf-control" + disabledClassName} id={constants_1.Constants.Ids.pdfIsTooLargeToAttachIndicator} tabIndex={67}>
				<img className="warning-image" src={extensionUtils_1.ExtensionUtils.getImageResourceUrl("warning.png")}></img>
				<div className="pdf-label-margin">
					<span className="pdf-label disabled">{localization_1.Localization.getLocalizedString("WebClipper.Label.PdfTooLargeToAttach")}</span>
				</div>
			</div>);
    };
    PdfClipOptionsClass.prototype.getAttachmentPdfCheckbox = function (enabled) {
        var pdfPreviewInfo = this.props.clipperState.pdfPreviewInfo;
        var disabledClassName = enabled ? "" : " disabled";
        return (<div className={"pdf-control" + disabledClassName} id={constants_1.Constants.Ids.checkboxToAttachPdf} aria-checked={enabled} {...this.enableInvoke({ callback: enabled ? this.onCheckboxChange : undefined, tabIndex: 66, args: !pdfPreviewInfo.shouldAttachPdf })}>
				<div className={"pdf-indicator pdf-checkbox-indicator" + disabledClassName}></div>
				{pdfPreviewInfo.shouldAttachPdf ? <div className={constants_1.Constants.Classes.checkboxCheck}></div> : ""}
				<div className="pdf-label-margin">
					<span className={"pdf-label focused" + disabledClassName}>{localization_1.Localization.getLocalizedString("WebClipper.Label.AttachPdfFile") + " "}
						<span className={"sub-label" + disabledClassName}>{localization_1.Localization.getLocalizedString("WebClipper.Label.AttachPdfFileSubText")}</span>
					</span>
				</div>
			</div>);
    };
    PdfClipOptionsClass.prototype.onHiddenOptionsDraw = function (hiddenOptionsAnimator) {
        var _this = this;
        this.hiddenOptionsAnimationStrategy.animate(hiddenOptionsAnimator);
        // If the user is rapidly clicking the More button, we want to cancel the current animation to kick off the next one
        var currentAnimationState = this.hiddenOptionsAnimationStrategy.getAnimationState();
        if (currentAnimationState === animationState_1.AnimationState.GoingOut && this.state.moreOptionsOpened) {
            animationHelper_1.AnimationHelper.stopAnimationsThen(hiddenOptionsAnimator, function () {
                _this.hiddenOptionsAnimationStrategy.setAnimationState(animationState_1.AnimationState.Out);
                _this.setState({});
            });
        }
    };
    PdfClipOptionsClass.prototype.render = function () {
        var expandOptionLabel = this.state.moreOptionsOpened ? localization_1.Localization.getLocalizedString("WebClipper.Action.Less") : localization_1.Localization.getLocalizedString("WebClipper.Action.More");
        return (<div className="clipOptionsContainer">
				<div className="clipOptionsTitleContainer">
					<span className="clipOptionsTitle">{localization_1.Localization.getLocalizedString("WebClipper.Label.PdfOptions")}</span>
					<span className="moreClipOptions" id={constants_1.Constants.Ids.moreClipOptions} {...this.enableInvoke({ callback: this.onMoreClicked, tabIndex: 60 })}>
						{expandOptionLabel}<img className="arrow" src={extensionUtils_1.ExtensionUtils.getImageResourceUrl("dropdown_arrow.png")}/>
					</span>
				</div>
				<pdfPageSelectionRadioButton_1.PdfPageSelectionRadioButton clipperState={this.props.clipperState}/>
				<div className="hiddenOptionsAnimator" {...this.onElementDraw(this.onHiddenOptionsDraw)}>
					{this.state.moreOptionsOpened ?
            <div className="hiddenOptions">
							{this.getDistributePagesCheckbox()}
							{this.getAttachmentCheckbox()}
						</div> : undefined}
				</div>
			</div>);
    };
    return PdfClipOptionsClass;
}(componentBase_1.ComponentBase));
var component = PdfClipOptionsClass.componentize();
exports.PdfClipOptions = component;
