"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var componentBase_1 = require("../componentBase");
var constants_1 = require("../../constants");
var SpriteAnimationClass = (function (_super) {
    __extends(SpriteAnimationClass, _super);
    function SpriteAnimationClass() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.currentFrame = 0;
        _this.currentTime = _this.rightNow();
        return _this;
    }
    SpriteAnimationClass.prototype.getInitialState = function () {
        var _this = this;
        requestAnimationFrame(function () {
            _this.updateFrame();
        });
        return {
            inProgress: true
        };
    };
    SpriteAnimationClass.prototype.updateFrame = function () {
        var _this = this;
        if (!this.spinnerElement) {
            this.stop();
            return;
        }
        if (!this.state.inProgress) {
            return;
        }
        var time = this.rightNow();
        var fps = 24;
        var delta = (time - this.currentTime) / 1000;
        this.currentTime = time;
        this.currentFrame += (delta * fps);
        var frameNumber = Math.floor(this.currentFrame);
        if (frameNumber >= this.props.totalFrameCount) {
            this.onFinishLoop();
            if (this.props.loop) {
                frameNumber = this.currentFrame = 0;
            }
            else {
                this.stop();
                frameNumber = this.currentFrame = this.props.totalFrameCount - 1;
            }
        }
        var backgroundPosition = (frameNumber * this.props.imageHeight);
        this.spinnerElement.style.backgroundPosition = "0 -" + backgroundPosition + "px";
        requestAnimationFrame(function () {
            _this.updateFrame();
        });
    };
    SpriteAnimationClass.prototype.stop = function () {
        this.setState({ inProgress: false });
    };
    /**
     * Fires when the animation finishes its loop
     * Note: Intended to be overwritten as an event handler
     */
    SpriteAnimationClass.prototype.onFinishLoop = function () {
        ////console.log("onFinishLoop");
    };
    /**
     * If window.performance exists, then use window.performance.now since it is faster,
     * else use Date.now()
     */
    SpriteAnimationClass.prototype.rightNow = function () {
        if (window.performance && window.performance.now) {
            return window.performance.now();
        }
        else {
            return Date.now();
        }
    };
    SpriteAnimationClass.prototype.configForSpinner = function (element, isInit, context) {
        var _this = this;
        this.spinnerElement = element;
        context.onunload = function () {
            _this.stop();
        };
    };
    SpriteAnimationClass.prototype.render = function () {
        var imageWidth = this.props.imageWidth ? this.props.imageWidth : 32;
        var style = {
            backgroundImage: "url(" + this.props.spriteUrl + ")",
            backgroundRepeat: "no-repeat",
            height: this.props.imageHeight + "px",
            width: imageWidth + "px"
        };
        return (<div className={constants_1.Constants.Classes.spinner} config={this.configForSpinner.bind(this)} style={style}>
			</div>);
    };
    return SpriteAnimationClass;
}(componentBase_1.ComponentBase));
var component = SpriteAnimationClass.componentize();
exports.SpriteAnimation = component;
