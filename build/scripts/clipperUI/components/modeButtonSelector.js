"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../constants");
var augmentationHelper_1 = require("../../contentCapture/augmentationHelper");
var extensionUtils_1 = require("../../extensions/extensionUtils");
var invokeOptions_1 = require("../../extensions/invokeOptions");
var localization_1 = require("../../localization/localization");
var clipMode_1 = require("../clipMode");
var componentBase_1 = require("../componentBase");
var modeButton_1 = require("./modeButton");
var ModeButtonSelectorClass = (function (_super) {
    __extends(ModeButtonSelectorClass, _super);
    function ModeButtonSelectorClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ModeButtonSelectorClass.prototype.onModeSelected = function (newMode) {
        this.props.clipperState.setState({
            currentMode: this.props.clipperState.currentMode.set(newMode)
        });
    };
    ;
    ModeButtonSelectorClass.prototype.getScreenReaderThatAnnouncesCurrentModeProps = function (currentMode) {
        var stringToTellUserModeHasChanged = localization_1.Localization.getLocalizedString("WebClipper.Accessibility.ScreenReader.CurrentModeHasChanged");
        stringToTellUserModeHasChanged = stringToTellUserModeHasChanged.replace("{0}", clipMode_1.ClipMode[currentMode]);
        return ({tag: "div", attrs: {"aria-live":"polite", "aria-relevant":"text", className:constants_1.Constants.Classes.srOnly}, children: [stringToTellUserModeHasChanged]});
    };
    ModeButtonSelectorClass.prototype.getPdfButtonProps = function (currentMode) {
        if (this.props.clipperState.pageInfo.contentType !== OneNoteApi.ContentType.EnhancedUrl) {
            return undefined;
        }
        return {
            imgSrc: extensionUtils_1.ExtensionUtils.getImageResourceUrl("pdf.png"),
            label: localization_1.Localization.getLocalizedString("WebClipper.ClipType.Pdf.Button"),
            myMode: clipMode_1.ClipMode.Pdf,
            selected: currentMode === clipMode_1.ClipMode.Pdf,
            onModeSelected: this.onModeSelected.bind(this),
            tooltipText: localization_1.Localization.getLocalizedString("WebClipper.ClipType.Pdf.Button.Tooltip")
        };
    };
    ModeButtonSelectorClass.prototype.getAugmentationButtonProps = function (currentMode) {
        if (this.props.clipperState.pageInfo.contentType === OneNoteApi.ContentType.EnhancedUrl) {
            return undefined;
        }
        var augmentationType = augmentationHelper_1.AugmentationHelper.getAugmentationType(this.props.clipperState);
        var augmentationLabel = localization_1.Localization.getLocalizedString("WebClipper.ClipType." + augmentationType + ".Button");
        var augmentationTooltip = localization_1.Localization.getLocalizedString("WebClipper.ClipType.Button.Tooltip").replace("{0}", augmentationLabel);
        var buttonSelected = currentMode === clipMode_1.ClipMode.Augmentation;
        return {
            imgSrc: extensionUtils_1.ExtensionUtils.getImageResourceUrl(augmentationType + ".png"),
            label: augmentationLabel,
            myMode: clipMode_1.ClipMode.Augmentation,
            selected: buttonSelected,
            onModeSelected: this.onModeSelected.bind(this),
            tooltipText: augmentationTooltip
        };
    };
    ModeButtonSelectorClass.prototype.getFullPageButtonProps = function (currentMode) {
        if (this.props.clipperState.pageInfo.contentType === OneNoteApi.ContentType.EnhancedUrl) {
            return undefined;
        }
        return {
            imgSrc: extensionUtils_1.ExtensionUtils.getImageResourceUrl("fullpage.png"),
            label: localization_1.Localization.getLocalizedString("WebClipper.ClipType.ScreenShot.Button"),
            myMode: clipMode_1.ClipMode.FullPage,
            selected: currentMode === clipMode_1.ClipMode.FullPage,
            onModeSelected: this.onModeSelected.bind(this),
            tooltipText: localization_1.Localization.getLocalizedString("WebClipper.ClipType.ScreenShot.Button.Tooltip")
        };
    };
    ModeButtonSelectorClass.prototype.getRegionButtonProps = function (currentMode) {
        var enableRegionClipping = this.props.clipperState.injectOptions && this.props.clipperState.injectOptions.enableRegionClipping;
        var contextImageModeUsed = this.props.clipperState.invokeOptions && this.props.clipperState.invokeOptions.invokeMode === invokeOptions_1.InvokeMode.ContextImage;
        if (!enableRegionClipping && !contextImageModeUsed) {
            return undefined;
        }
        return {
            imgSrc: extensionUtils_1.ExtensionUtils.getImageResourceUrl("region.png"),
            label: localization_1.Localization.getLocalizedString(this.getRegionButtonLabel()),
            myMode: clipMode_1.ClipMode.Region,
            selected: currentMode === clipMode_1.ClipMode.Region,
            onModeSelected: this.onModeSelected.bind(this),
            tooltipText: localization_1.Localization.getLocalizedString("WebClipper.ClipType.MultipleRegions.Button.Tooltip")
        };
    };
    ModeButtonSelectorClass.prototype.getRegionButtonLabel = function () {
        return "WebClipper.ClipType.Region.Button";
    };
    ModeButtonSelectorClass.prototype.getSelectionButtonProps = function (currentMode) {
        if (this.props.clipperState.invokeOptions.invokeMode !== invokeOptions_1.InvokeMode.ContextTextSelection) {
            return undefined;
        }
        return {
            imgSrc: extensionUtils_1.ExtensionUtils.getImageResourceUrl("select.png"),
            label: localization_1.Localization.getLocalizedString("WebClipper.ClipType.Selection.Button"),
            myMode: clipMode_1.ClipMode.Selection,
            selected: currentMode === clipMode_1.ClipMode.Selection,
            onModeSelected: this.onModeSelected.bind(this),
            tooltipText: localization_1.Localization.getLocalizedString("WebClipper.ClipType.Selection.Button.Tooltip")
        };
    };
    ModeButtonSelectorClass.prototype.getBookmarkButtonProps = function (currentMode) {
        if (this.props.clipperState.pageInfo.rawUrl.indexOf("file:///") === 0) {
            return undefined;
        }
        return {
            imgSrc: extensionUtils_1.ExtensionUtils.getImageResourceUrl("bookmark.png"),
            label: localization_1.Localization.getLocalizedString("WebClipper.ClipType.Bookmark.Button"),
            myMode: clipMode_1.ClipMode.Bookmark,
            selected: currentMode === clipMode_1.ClipMode.Bookmark,
            onModeSelected: this.onModeSelected.bind(this),
            tooltipText: localization_1.Localization.getLocalizedString("WebClipper.ClipType.Bookmark.Button.Tooltip")
        };
    };
    ModeButtonSelectorClass.prototype.getListOfButtons = function () {
        var currentMode = this.props.clipperState.currentMode.get();
        var buttonProps = [
            this.getFullPageButtonProps(currentMode),
            this.getRegionButtonProps(currentMode),
            this.getAugmentationButtonProps(currentMode),
            this.getSelectionButtonProps(currentMode),
            this.getBookmarkButtonProps(currentMode),
            this.getPdfButtonProps(currentMode),
        ];
        var visibleButtons = [];
        var propsForVisibleButtons = buttonProps.filter(function (attributes) { return !!attributes; });
        for (var i = 0; i < propsForVisibleButtons.length; i++) {
            var attributes = propsForVisibleButtons[i];
            var ariaPos = i + 1;
            visibleButtons.push(m.component(modeButton_1.ModeButton, Object.assign({},  attributes,{"aria-setsize":propsForVisibleButtons.length, "aria-posinset":ariaPos, tabIndex:attributes.selected ? 40 : -1})));
        }
        return visibleButtons;
    };
    ModeButtonSelectorClass.prototype.render = function () {
        var currentMode = this.props.clipperState.currentMode.get();
        return ({tag: "div", attrs: {}, children: [
				this.getScreenReaderThatAnnouncesCurrentModeProps(currentMode), 
				{tag: "div", attrs: {style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Semilight), role:"listbox", className:"modeButtonContainer"}, children: [
					this.getListOfButtons()
				]}
			]});
    };
    return ModeButtonSelectorClass;
}(componentBase_1.ComponentBase));
var component = ModeButtonSelectorClass.componentize();
exports.ModeButtonSelector = component;
