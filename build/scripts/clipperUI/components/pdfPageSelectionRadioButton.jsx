"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../constants");
var localization_1 = require("../../localization/localization");
var popover_1 = require("./popover");
var _ = require("lodash");
var stringUtils_1 = require("../../stringUtils");
var operationResult_1 = require("../../operationResult");
var componentBase_1 = require("../componentBase");
var PdfPageSelectionRadioButton = (function (_super) {
    __extends(PdfPageSelectionRadioButton, _super);
    function PdfPageSelectionRadioButton(props) {
        var _this = _super.call(this, props) || this;
        if (!PdfPageSelectionRadioButton.textAreaListenerAttached) {
            _this.addTextAreaListener();
            PdfPageSelectionRadioButton.textAreaListenerAttached = true;
        }
        return _this;
    }
    PdfPageSelectionRadioButton.prototype.getRadioButtonGroups = function () {
        return [this.getRadioButtons()];
    };
    PdfPageSelectionRadioButton.prototype.addTextAreaListener = function () {
        var _this = this;
        document.addEventListener("input", function (event) {
            var element = event.target;
            var pageRangeField = document.getElementById(constants_1.Constants.Ids.rangeInput);
            if (!!element && element === pageRangeField) {
                _this.onTextChange(pageRangeField.value);
            }
        });
    };
    PdfPageSelectionRadioButton.prototype.onTextChange = function (text) {
        _.assign(_.extend(this.props.clipperState.pdfPreviewInfo, {
            selectedPageRange: text
        }), this.props.clipperState.setState);
    };
    PdfPageSelectionRadioButton.prototype.onTextInputFocus = function () {
        if (this.props.clipperState.pdfPreviewInfo.shouldShowPopover) {
            _.assign(_.extend(this.props.clipperState.pdfPreviewInfo, {
                shouldShowPopover: false
            }), this.props.clipperState.setState);
        }
    };
    PdfPageSelectionRadioButton.prototype.onSelectionChange = function (selection) {
        _.assign(_.extend(this.props.clipperState.pdfPreviewInfo, {
            allPages: selection,
            shouldShowPopover: false
        }), this.props.clipperState.setState);
        document.getElementById(selection ? constants_1.Constants.Ids.radioAllPagesLabel : constants_1.Constants.Ids.rangeInput).focus();
    };
    PdfPageSelectionRadioButton.prototype.getErrorMessageForInvalidPageRange = function () {
        var pdfPreviewInfo = this.props.clipperState.pdfPreviewInfo;
        var parsePageRangeOperation = stringUtils_1.StringUtils.parsePageRange(pdfPreviewInfo.selectedPageRange, this.props.clipperState.pdfResult.data.get().pdf.numPages());
        if (parsePageRangeOperation.status === operationResult_1.OperationResult.Succeeded) {
            throw Error("Given that shouldShowPopover is true, parsing the pageRange should never succeed: PageRange: " + pdfPreviewInfo.selectedPageRange);
        }
        return localization_1.Localization.getLocalizedString("WebClipper.Popover.PdfInvalidPageRange").replace("{0}", parsePageRangeOperation.result);
    };
    PdfPageSelectionRadioButton.prototype.getRadioButtons = function () {
        var pdfPreviewInfo = this.props.clipperState.pdfPreviewInfo;
        var invalidClassName = pdfPreviewInfo.shouldShowPopover ? "invalid" : "";
        var selectedTabIndex = 60;
        var unselectedTabIndex = -1;
        return {
            role: "radiogroup",
            isAriaSet: true,
            innerElements: [
                <div role="radio" id={constants_1.Constants.Ids.radioAllPagesLabel} className="pdf-control" aria-selected={pdfPreviewInfo.allPages} {...this.enableAriaInvoke({ callback: this.onSelectionChange, tabIndex: pdfPreviewInfo.allPages ? selectedTabIndex : unselectedTabIndex, args: true, ariaSetName: constants_1.Constants.AriaSet.pdfPageSelection, autoSelect: true })}>
					<div className={"pdf-indicator pdf-radio-indicator"}>
						{pdfPreviewInfo.allPages ? <div className={constants_1.Constants.Classes.radioIndicatorFill}></div> : undefined}
					</div>
					<div className="pdf-label-margin">
						<span className={"pdf-label" + (pdfPreviewInfo.allPages ? " focused" : "")}>{localization_1.Localization.getLocalizedString("WebClipper.Label.PdfAllPagesRadioButton")}</span>
					</div>
				</div>,
                <div id={constants_1.Constants.Ids.radioPageRangeLabel} className={"pdf-control" + (!pdfPreviewInfo.allPages ? " focused" : "")} aria-selected={!pdfPreviewInfo.allPages} {...this.enableAriaInvoke({ callback: this.onSelectionChange, tabIndex: !pdfPreviewInfo.allPages ? selectedTabIndex : unselectedTabIndex, args: false, ariaSetName: constants_1.Constants.AriaSet.pdfPageSelection, autoSelect: true })}>
					<div className={"pdf-indicator pdf-radio-indicator"}>
						{!pdfPreviewInfo.allPages ?
                    <div className={constants_1.Constants.Classes.radioIndicatorFill}></div> : undefined}
					</div>
					<input type="text" id={constants_1.Constants.Ids.rangeInput} className={invalidClassName + (!pdfPreviewInfo.allPages ? " selected" : "")} placeholder="e.g. 1-5, 7, 9-12" onFocus={this.onTextInputFocus.bind(this)} value={this.props.clipperState.pdfPreviewInfo.selectedPageRange} {...this.enableInvoke({ callback: this.onSelectionChange, tabIndex: unselectedTabIndex, args: false })}/>
					{pdfPreviewInfo.shouldShowPopover ?
                    <popover_1.Popover referenceElementId={constants_1.Constants.Ids.rangeInput} placement="bottom" parentId={constants_1.Constants.Ids.mainController} content={this.getErrorMessageForInvalidPageRange()} classNames={[constants_1.Constants.Classes.popover]} arrowClassNames={[constants_1.Constants.Classes.popoverArrow]} modifiersIgnored={["flip"]} removeOnDestroy={true}/> : undefined}
				</div>
            ]
        };
    };
    PdfPageSelectionRadioButton.prototype.render = function () {
        var renderables = [];
        var buttonGroups = this.getRadioButtonGroups();
        for (var i = 0; i < buttonGroups.length; i++) {
            var currentButtonGroup = buttonGroups[i];
            var role = currentButtonGroup.role;
            var isAriaSet = currentButtonGroup.isAriaSet;
            if (isAriaSet) {
                var setSize = currentButtonGroup.innerElements.length;
                for (var j = 0; j < setSize; j++) {
                    currentButtonGroup.innerElements[j].attrs["aria-posinset"] = j + 1;
                    currentButtonGroup.innerElements[j].attrs["aria-setsize"] = setSize;
                }
            }
            renderables.push(<div role={role ? role : ""}>
					{currentButtonGroup.innerElements}
				</div>);
        }
        return (<div>
				{renderables}
			</div>);
    };
    return PdfPageSelectionRadioButton;
}(componentBase_1.ComponentBase));
PdfPageSelectionRadioButton.textAreaListenerAttached = false;
var component = PdfPageSelectionRadioButton.componentize();
exports.PdfPageSelectionRadioButton = component;
