"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../constants");
var localization_1 = require("../../../localization/localization");
var previewViewerTitleOnlyHeaderComponentBase_1 = require("./previewViewerTitleOnlyHeaderComponentBase");
var PreviewViewerPdfHeaderClass = (function (_super) {
    __extends(PreviewViewerPdfHeaderClass, _super);
    function PreviewViewerPdfHeaderClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PreviewViewerPdfHeaderClass.prototype.getControlGroupId = function () {
        return constants_1.Constants.Ids.pdfControl;
    };
    PreviewViewerPdfHeaderClass.prototype.getHeader = function () {
        return localization_1.Localization.getLocalizedString("WebClipper.ClipType.Pdf.Button");
    };
    PreviewViewerPdfHeaderClass.prototype.getHeaderId = function () {
        return constants_1.Constants.Ids.pdfHeaderTitle;
    };
    return PreviewViewerPdfHeaderClass;
}(previewViewerTitleOnlyHeaderComponentBase_1.PreviewViewerTitleOnlyHeaderComponentBase));
var component = PreviewViewerPdfHeaderClass.componentize();
exports.PreviewViewerPdfHeader = component;
