"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../constants");
var extensionUtils_1 = require("../../../extensions/extensionUtils");
var componentBase_1 = require("../../componentBase");
/**
 * Provides page number overlay and selected/unselected stylings on top of the
 * pdf page viewport's functionality.
 */
var PdfPreviewAttachmentClass = (function (_super) {
    __extends(PdfPreviewAttachmentClass, _super);
    function PdfPreviewAttachmentClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PdfPreviewAttachmentClass.prototype.render = function () {
        return (<span className={constants_1.Constants.Classes.attachmentOverlay}>
				<img src={extensionUtils_1.ExtensionUtils.getImageResourceUrl("editorOptions/pdf_attachment_icon.png")}></img>
				<div className="file-name">{this.props.name}</div>
			</span>);
    };
    return PdfPreviewAttachmentClass;
}(componentBase_1.ComponentBase));
var component = PdfPreviewAttachmentClass.componentize();
exports.PdfPreviewAttachment = component;
