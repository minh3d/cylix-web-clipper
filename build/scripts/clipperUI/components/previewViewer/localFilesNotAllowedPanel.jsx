"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../constants");
var localization_1 = require("../../../localization/localization");
var componentBase_1 = require("../../componentBase");
var LocalFilesNotAllowedPanelClass = (function (_super) {
    __extends(LocalFilesNotAllowedPanelClass, _super);
    function LocalFilesNotAllowedPanelClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LocalFilesNotAllowedPanelClass.prototype.render = function () {
        return (<div id={constants_1.Constants.Ids.previewOuterContainer} style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)}>
				<div id={constants_1.Constants.Ids.previewInnerWrapper}>
					<div id={constants_1.Constants.Ids.previewInnerContainer} style="height: auto;">
						<div id={constants_1.Constants.Ids.previewOptionsContainer}>
							<div className="control-button-group">
								<span id={constants_1.Constants.Ids.localPdfFileTitle} style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)} className="buttonLabelFont">
									{this.props.header}
								</span>
							</div>
						</div>
						<div id={constants_1.Constants.Ids.previewContentContainer} className="local-pdf-file-content-container">
							<div className={constants_1.Constants.Classes.localPdfPanelTitle}>{this.props.title}</div>
							<div className={constants_1.Constants.Classes.localPdfPanelSubtitle}>{this.props.subtitle}</div>
						</div>
					</div>
				</div>
			</div>);
    };
    return LocalFilesNotAllowedPanelClass;
}(componentBase_1.ComponentBase));
exports.LocalFilesNotAllowedPanelClass = LocalFilesNotAllowedPanelClass;
var component = LocalFilesNotAllowedPanelClass.componentize();
exports.LocalFilesNotAllowedPanel = component;
