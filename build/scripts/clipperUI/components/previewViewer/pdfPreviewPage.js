"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../constants");
var componentBase_1 = require("../../componentBase");
var PdfPageViewport_1 = require("./PdfPageViewport");
/**
 * Provides page number overlay and selected/unselected stylings on top of the
 * pdf page viewport's functionality.
 */
var PdfPreviewPageClass = (function (_super) {
    __extends(PdfPreviewPageClass, _super);
    function PdfPreviewPageClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PdfPreviewPageClass.prototype.render = function () {
        return ({tag: "div", attrs: {className:"pdf-preview-image-container"}, children: [
				{tag: "div", attrs: {className:constants_1.Constants.Classes.pdfPreviewImageCanvas + (this.props.isSelected ? "" : " " + constants_1.Constants.Classes.unselected)}, children: [
					m.component(PdfPageViewport_1.PdfPageViewport, {viewportDimensions:this.props.viewportDimensions, imgUrl:this.props.imgUrl, index:this.props.index})
				]}, 
				{tag: "div", attrs: {className:constants_1.Constants.Classes.overlay + (this.props.showPageNumber ? "" : (" " + constants_1.Constants.Classes.overlayHidden))}, children: [
					{tag: "span", attrs: {className:constants_1.Constants.Classes.overlayNumber}, children: [this.props.index + 1]}
				]}
			]});
    };
    return PdfPreviewPageClass;
}(componentBase_1.ComponentBase));
var component = PdfPreviewPageClass.componentize();
exports.PdfPreviewPage = component;
