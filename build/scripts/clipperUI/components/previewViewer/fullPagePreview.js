"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../constants");
var extensionUtils_1 = require("../../../extensions/extensionUtils");
var localization_1 = require("../../../localization/localization");
var status_1 = require("../../status");
var rotatingMessageSpriteAnimation_1 = require("../../components/rotatingMessageSpriteAnimation");
var previewComponentBase_1 = require("./previewComponentBase");
var previewViewerFullPageHeader_1 = require("./previewViewerFullPageHeader");
var FullPagePreview = (function (_super) {
    __extends(FullPagePreview, _super);
    function FullPagePreview() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FullPagePreview.prototype.getContentBodyForCurrentStatus = function () {
        var state = this.props.clipperState;
        if (!state.pageInfo) {
            return [this.getSpinner()];
        }
        return this.convertFullPageResultToContentData(state.fullPageResult);
    };
    FullPagePreview.prototype.getHeader = function () {
        return m.component(previewViewerFullPageHeader_1.PreviewViewerFullPageHeader, {clipperState:this.props.clipperState});
    };
    FullPagePreview.prototype.getStatus = function () {
        if (!this.props.clipperState.pageInfo) {
            return status_1.Status.NotStarted;
        }
        return this.props.clipperState.fullPageResult.status;
    };
    FullPagePreview.prototype.getTitleTextForCurrentStatus = function () {
        var noContentFoundString = localization_1.Localization.getLocalizedString("WebClipper.Preview.NoContentFound");
        var failureMessage;
        var previewStatus = this.getStatus();
        var pageInfo = this.props.clipperState.pageInfo;
        switch (previewStatus) {
            case status_1.Status.Succeeded:
                if (pageInfo && !this.props.clipperState.fullPageResult.data) {
                    return localization_1.Localization.getLocalizedString("WebClipper.Preview.NoContentFound");
                }
                return this.props.clipperState.previewGlobalInfo.previewTitleText;
            case status_1.Status.NotStarted:
            case status_1.Status.InProgress:
                return localization_1.Localization.getLocalizedString("WebClipper.Preview.LoadingMessage");
            default:
            case status_1.Status.Failed:
                failureMessage = this.props.clipperState.fullPageResult.data.failureMessage;
                return !!failureMessage ? failureMessage : noContentFoundString;
        }
    };
    FullPagePreview.prototype.convertFullPageResultToContentData = function (result) {
        var contentBody = [];
        switch (result.status) {
            case status_1.Status.Succeeded:
                var pageTitle = this.props.clipperState.pageInfo ? this.props.clipperState.pageInfo.contentTitle : "";
                var altTag = localization_1.Localization.getLocalizedString("WebClipper.Preview.FullPageModeScreenshotDescription").replace("{0}", pageTitle);
                if (this.props.clipperState.fullPageResult.data) {
                    var screenshotImages = this.props.clipperState.fullPageResult.data;
                    for (var _i = 0, _a = screenshotImages.Images; _i < _a.length; _i++) {
                        var imageData = _a[_i];
                        var dataUrl = "data:image/" + screenshotImages.ImageFormat + ";" + screenshotImages.ImageEncoding + "," + imageData;
                        contentBody.push({tag: "img", attrs: {src:dataUrl, alt:altTag}});
                    }
                }
                break;
            case status_1.Status.NotStarted:
            case status_1.Status.InProgress:
                contentBody.push(this.getSpinner());
                break;
            default:
            case status_1.Status.Failed:
                break;
        }
        return contentBody;
    };
    FullPagePreview.prototype.getSpinner = function () {
        var spinner = m.component(rotatingMessageSpriteAnimation_1.RotatingMessageSpriteAnimation, {spriteUrl:extensionUtils_1.ExtensionUtils.getImageResourceUrl("spinner_loop_colored.png"), imageHeight:65, imageWidth:45, totalFrameCount:21, loop:true, shouldDisplayMessage:false});
        return {tag: "div", attrs: {className:constants_1.Constants.Classes.centeredInPreview}, children: [spinner]};
    };
    return FullPagePreview;
}(previewComponentBase_1.PreviewComponentBase));
var component = FullPagePreview.componentize();
exports.FullPagePreview = component;
