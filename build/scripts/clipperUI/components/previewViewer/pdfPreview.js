"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../constants");
var objectUtils_1 = require("../../../objectUtils");
var operationResult_1 = require("../../../operationResult");
var stringUtils_1 = require("../../../stringUtils");
var urlUtils_1 = require("../../../urlUtils");
var domUtils_1 = require("../../../domParsers/domUtils");
var extensionUtils_1 = require("../../../extensions/extensionUtils");
var localization_1 = require("../../../localization/localization");
var clipMode_1 = require("../../clipMode");
var status_1 = require("../../status");
var rotatingMessageSpriteAnimation_1 = require("../../components/rotatingMessageSpriteAnimation");
var pdfPreviewAttachment_1 = require("./pdfPreviewAttachment");
var pdfPreviewPage_1 = require("./pdfPreviewPage");
var previewComponentBase_1 = require("./previewComponentBase");
var previewViewerPdfHeader_1 = require("./previewViewerPdfHeader");
var _ = require("lodash");
var PdfPreviewClass = (function (_super) {
    __extends(PdfPreviewClass, _super);
    function PdfPreviewClass(props) {
        var _this = _super.call(this, props) || this;
        _this.initPageRenderCalled = false;
        // We need to do this on every constructor to ensure the reference to the state
        // object is correct
        _this.addScrollListener();
        return _this;
    }
    PdfPreviewClass.prototype.getInitialState = function () {
        return {
            showPageNumbers: false,
            renderedPageIndexes: {}
        };
    };
    PdfPreviewClass.prototype.searchForVisiblePageBoundary = function (allPages, initPageIndex, incrementer) {
        var pageIndexToTest = initPageIndex;
        var guessAtPageBoundary = allPages[pageIndexToTest];
        while (guessAtPageBoundary && this.pageIsVisible(guessAtPageBoundary)) {
            pageIndexToTest += incrementer;
            guessAtPageBoundary = allPages[pageIndexToTest];
        }
        // result of adding last incrementer was a non-visible page, so return the page num from before that
        return Math.max(pageIndexToTest -= incrementer, 0);
    };
    /**
     * Get an approximation of the centermost visible page currently in the viewport (based on scroll percentage).
     * If the page is indeed in the viewport, search up and down for the visible page boundaries.
     * If the page approximation was incorrect and the page is not visible, begin a fan-out search of the area around the approximate page
     * for a visible page. When a visible page boundary is found, find the other visible page boundary.
     */
    PdfPreviewClass.prototype.getIndicesOfVisiblePages = function () {
        var allPages = document.querySelectorAll("div[data-pageindex]");
        var initGuessAtCurrentPageIndexer = Math.floor(domUtils_1.DomUtils.getScrollPercent(document.getElementById("previewContentContainer"), true /* asDecimalValue */) * (allPages.length - 1));
        var firstVisiblePageIndexer;
        var lastVisiblePageIndexer;
        if (this.pageIsVisible(allPages[initGuessAtCurrentPageIndexer])) {
            firstVisiblePageIndexer = this.searchForVisiblePageBoundary(allPages, initGuessAtCurrentPageIndexer, -1);
            lastVisiblePageIndexer = this.searchForVisiblePageBoundary(allPages, initGuessAtCurrentPageIndexer, 1);
        }
        else {
            var incrementer = 1;
            var guessAtVisiblePageIndexer = initGuessAtCurrentPageIndexer + incrementer;
            var guessAtVisiblePageBoundary = allPages[guessAtVisiblePageIndexer];
            while (!this.pageIsVisible(guessAtVisiblePageBoundary)) {
                if (incrementer > 0) {
                    incrementer *= -1;
                }
                else {
                    incrementer = (incrementer * -1) + 1;
                }
                guessAtVisiblePageIndexer = initGuessAtCurrentPageIndexer + incrementer;
                guessAtVisiblePageBoundary = allPages[guessAtVisiblePageIndexer];
            }
            if (incrementer > 0) {
                firstVisiblePageIndexer = guessAtVisiblePageIndexer;
                lastVisiblePageIndexer = this.searchForVisiblePageBoundary(allPages, firstVisiblePageIndexer, 1);
            }
            else {
                lastVisiblePageIndexer = guessAtVisiblePageIndexer;
                firstVisiblePageIndexer = this.searchForVisiblePageBoundary(allPages, lastVisiblePageIndexer, -1);
            }
        }
        // _.range does not include the end number, so add 1
        return _.range(firstVisiblePageIndexer, lastVisiblePageIndexer + 1);
    };
    PdfPreviewClass.prototype.setDataUrlsOfImagesInViewportInState = function () {
        var pageIndicesToRender = this.getIndicesOfVisiblePages();
        // Pad pages to each end of the list to increase the scroll distance before the user hits a blank page
        if (pageIndicesToRender.length > 0) {
            var first = pageIndicesToRender[0];
            var extraPagesToPrepend = _.range(Math.max(first - constants_1.Constants.Settings.pdfExtraPageLoadEachSide, 0), first);
            var afterLast = pageIndicesToRender[pageIndicesToRender.length - 1] + 1;
            var extraPagesToAppend = _.range(afterLast, Math.min(afterLast + constants_1.Constants.Settings.pdfExtraPageLoadEachSide, this.props.clipperState.pdfResult.data.get().pdf.numPages()));
            pageIndicesToRender = extraPagesToPrepend.concat(pageIndicesToRender).concat(extraPagesToAppend);
        }
        this.setDataUrlsOfImagesInState(pageIndicesToRender);
    };
    PdfPreviewClass.prototype.pageIsVisible = function (element) {
        if (!element) {
            return false;
        }
        var rect = element.getBoundingClientRect();
        return rect.top <= window.innerHeight && rect.bottom >= 0;
    };
    PdfPreviewClass.prototype.setDataUrlsOfImagesInState = function (pageIndicesToRender) {
        var _this = this;
        this.props.clipperState.pdfResult.data.get().pdf.getPageListAsDataUrls(pageIndicesToRender).then(function (dataUrls) {
            var renderedIndexes = {};
            for (var i = 0; i < dataUrls.length; i++) {
                renderedIndexes[pageIndicesToRender[i]] = dataUrls[i];
            }
            _this.setState({
                renderedPageIndexes: renderedIndexes
            });
        });
    };
    /**
     * Gets the page components to be rendered in the preview
     */
    PdfPreviewClass.prototype.getPageComponents = function () {
        var pages = [];
        var pdfResult = this.props.clipperState.pdfResult.data.get();
        // Determine which pages should be marked as selected vs unselected
        var pagesToShow;
        var parsePageRangeOperation = stringUtils_1.StringUtils.parsePageRange(this.props.clipperState.pdfPreviewInfo.selectedPageRange);
        if (parsePageRangeOperation.status !== operationResult_1.OperationResult.Succeeded) {
            pagesToShow = [];
        }
        else {
            // If the operation Succeeded, the result should always be a number[]
            pagesToShow = parsePageRangeOperation.result;
        }
        pagesToShow = pagesToShow.map(function (ind) { return ind - 1; });
        for (var i = 0; i < pdfResult.pdf.numPages(); i++) {
            pages.push(m.component(pdfPreviewPage_1.PdfPreviewPage, {showPageNumber:this.state.showPageNumbers, isSelected:this.props.clipperState.pdfPreviewInfo.allPages || pagesToShow.indexOf(i) >= 0, viewportDimensions:pdfResult.viewportDimensions[i], imgUrl:this.state.renderedPageIndexes[i], index:i}));
        }
        return pages;
    };
    PdfPreviewClass.prototype.addScrollListener = function () {
        var _this = this;
        if (PdfPreviewClass.latestScrollListener) {
            window.removeEventListener("scroll", PdfPreviewClass.latestScrollListener, true);
        }
        // When we detect a scroll, show page numbers immediately.
        // When the user doesn't scroll for some period of time, fade them out.
        PdfPreviewClass.latestScrollListener = function (event) {
            var element = event.target;
            if (!!element && element.id === constants_1.Constants.Ids.previewContentContainer) {
                if (objectUtils_1.ObjectUtils.isNumeric(PdfPreviewClass.scrollListenerTimeout)) {
                    clearTimeout(PdfPreviewClass.scrollListenerTimeout);
                }
                PdfPreviewClass.scrollListenerTimeout = setTimeout(function () {
                    _this.setState({
                        showPageNumbers: false
                    });
                    // We piggyback the scroll listener to determine what pages the user is looking at, then render them
                    if (_this.props.clipperState.currentMode.get() === clipMode_1.ClipMode.Pdf && _this.props.clipperState.pdfResult.status === status_1.Status.Succeeded) {
                        _this.setDataUrlsOfImagesInViewportInState();
                    }
                }, constants_1.Constants.Settings.timeUntilPdfPageNumbersFadeOutAfterScroll);
                // A little optimization to prevent us from calling render a large number of times
                if (!_this.state.showPageNumbers) {
                    _this.setState({
                        showPageNumbers: true
                    });
                }
            }
        };
        // TODO does this work on touch and pageup/down too?
        window.addEventListener("scroll", PdfPreviewClass.latestScrollListener, true /* allows the listener to listen to all elements */);
    };
    PdfPreviewClass.prototype.getContentBodyForCurrentStatus = function () {
        var state = this.props.clipperState;
        if (state.pdfResult.status === status_1.Status.InProgress || state.pdfResult.status === status_1.Status.NotStarted) {
            return [this.getSpinner()];
        }
        return this.convertPdfResultToContentData(state.pdfResult);
    };
    PdfPreviewClass.prototype.getHeader = function () {
        return m.component(previewViewerPdfHeader_1.PreviewViewerPdfHeader, {dummy:true});
    };
    PdfPreviewClass.prototype.getStatus = function () {
        if (!this.props.clipperState.pageInfo) {
            return status_1.Status.NotStarted;
        }
        return this.props.clipperState.pdfResult.status;
    };
    PdfPreviewClass.prototype.getTitleTextForCurrentStatus = function () {
        var noContentFoundString = localization_1.Localization.getLocalizedString("WebClipper.Preview.NoContentFound");
        var failureMessage;
        var previewStatus = this.getStatus();
        var pdfResult = this.props.clipperState.pdfResult;
        switch (previewStatus) {
            case status_1.Status.Succeeded:
                // TODO: verify this is actually what happens
                if (pdfResult && !pdfResult.data.get()) {
                    return localization_1.Localization.getLocalizedString("WebClipper.Preview.NoContentFound");
                }
                return this.props.clipperState.previewGlobalInfo.previewTitleText;
            case status_1.Status.NotStarted:
            case status_1.Status.InProgress:
                return localization_1.Localization.getLocalizedString("WebClipper.Preview.LoadingMessage");
            default:
            case status_1.Status.Failed:
                failureMessage = this.props.clipperState.pdfResult.data.get().failureMessage;
                return !!failureMessage ? failureMessage : noContentFoundString;
        }
    };
    PdfPreviewClass.prototype.convertPdfResultToContentData = function (result) {
        var contentBody = [];
        switch (result.status) {
            case status_1.Status.Succeeded:
                if (!this.initPageRenderCalled) {
                    // Load the first n pages (or fewer if numPages < n) as soon as we are able to
                    this.initPageRenderCalled = true;
                    this.setDataUrlsOfImagesInState(_.range(Math.min(constants_1.Constants.Settings.pdfInitialPageLoadCount, result.data.get().pdf.numPages())));
                }
                // In OneNote we don't display the extension
                var shouldAttachPdf = this.props.clipperState.pdfPreviewInfo.shouldAttachPdf;
                var defaultAttachmentName = "Original.pdf";
                var fullAttachmentName = this.props.clipperState.pageInfo ? urlUtils_1.UrlUtils.getFileNameFromUrl(this.props.clipperState.pageInfo.rawUrl, defaultAttachmentName) : defaultAttachmentName;
                if (shouldAttachPdf) {
                    contentBody.push(m.component(pdfPreviewAttachment_1.PdfPreviewAttachment, {name:fullAttachmentName.split(".")[0]}));
                }
                contentBody = contentBody.concat(this.getPageComponents());
                break;
            case status_1.Status.NotStarted:
            case status_1.Status.InProgress:
                contentBody.push(this.getSpinner());
                break;
            default:
            case status_1.Status.Failed:
                break;
        }
        return contentBody;
    };
    PdfPreviewClass.prototype.getSpinner = function () {
        var spinner = m.component(rotatingMessageSpriteAnimation_1.RotatingMessageSpriteAnimation, {spriteUrl:extensionUtils_1.ExtensionUtils.getImageResourceUrl("spinner_loop_colored.png"), imageHeight:65, imageWidth:45, totalFrameCount:21, loop:true, shouldDisplayMessage:false});
        return {tag: "div", attrs: {className:constants_1.Constants.Classes.centeredInPreview}, children: [spinner]};
    };
    return PdfPreviewClass;
}(previewComponentBase_1.PreviewComponentBase));
var component = PdfPreviewClass.componentize();
exports.PdfPreview = component;
