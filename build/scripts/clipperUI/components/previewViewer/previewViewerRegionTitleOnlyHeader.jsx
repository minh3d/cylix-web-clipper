"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../constants");
var localization_1 = require("../../../localization/localization");
var previewViewerTitleOnlyHeaderComponentBase_1 = require("./previewViewerTitleOnlyHeaderComponentBase");
// Used by Edge as we need a title-only header for the Region mode used when the user takes an image selection
var PreviewViewerRegionTitleOnlyHeaderClass = (function (_super) {
    __extends(PreviewViewerRegionTitleOnlyHeaderClass, _super);
    function PreviewViewerRegionTitleOnlyHeaderClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PreviewViewerRegionTitleOnlyHeaderClass.prototype.getControlGroupId = function () {
        return constants_1.Constants.Ids.regionControl;
    };
    PreviewViewerRegionTitleOnlyHeaderClass.prototype.getHeader = function () {
        return localization_1.Localization.getLocalizedString("WebClipper.ClipType.Region.Button");
    };
    PreviewViewerRegionTitleOnlyHeaderClass.prototype.getHeaderId = function () {
        return constants_1.Constants.Ids.regionHeaderTitle;
    };
    return PreviewViewerRegionTitleOnlyHeaderClass;
}(previewViewerTitleOnlyHeaderComponentBase_1.PreviewViewerTitleOnlyHeaderComponentBase));
var component = PreviewViewerRegionTitleOnlyHeaderClass.componentize();
exports.PreviewViewerRegionTitleOnlyHeader = component;
