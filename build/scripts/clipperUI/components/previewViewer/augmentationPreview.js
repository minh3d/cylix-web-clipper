"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../constants");
var augmentationHelper_1 = require("../../../contentCapture/augmentationHelper");
var extensionUtils_1 = require("../../../extensions/extensionUtils");
var localization_1 = require("../../../localization/localization");
var status_1 = require("../../status");
var spriteAnimation_1 = require("../../components/spriteAnimation");
var editorPreviewComponentBase_1 = require("./editorPreviewComponentBase");
var AugmentationPreview = (function (_super) {
    __extends(AugmentationPreview, _super);
    function AugmentationPreview() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AugmentationPreview.prototype.getHighlightableContentBodyForCurrentStatus = function () {
        var state = this.props.clipperState;
        return this.convertAugmentationResultToContentData(state.augmentationResult);
    };
    AugmentationPreview.prototype.getStatus = function () {
        return this.props.clipperState.augmentationResult.status;
    };
    AugmentationPreview.prototype.getTitleTextForCurrentStatus = function () {
        var noContentFoundString = localization_1.Localization.getLocalizedString("WebClipper.Preview.NoContentFound");
        var failureMessage;
        var previewStatus = this.getStatus();
        switch (previewStatus) {
            case status_1.Status.Succeeded:
                if (!this.props.clipperState.augmentationResult.data || this.props.clipperState.augmentationResult.data.ContentModel === augmentationHelper_1.AugmentationModel.None) {
                    return localization_1.Localization.getLocalizedString("WebClipper.Preview.NoContentFound");
                }
                return this.props.clipperState.previewGlobalInfo.previewTitleText;
            case status_1.Status.NotStarted:
            case status_1.Status.InProgress:
                return localization_1.Localization.getLocalizedString("WebClipper.Preview.LoadingMessage");
            default:
            case status_1.Status.Failed:
                failureMessage = this.props.clipperState.augmentationResult.data.failureMessage;
                return !!failureMessage ? failureMessage : noContentFoundString;
        }
    };
    AugmentationPreview.prototype.handleBodyChange = function (newBodyHtml) {
        this.props.clipperState.setState({
            augmentationPreviewInfo: { previewBodyHtml: newBodyHtml }
        });
    };
    AugmentationPreview.prototype.convertAugmentationResultToContentData = function (result) {
        switch (result.status) {
            case status_1.Status.Succeeded:
                if (this.props.clipperState.augmentationResult.data && this.props.clipperState.augmentationResult.data.ContentModel !== augmentationHelper_1.AugmentationModel.None) {
                    return m.trust(this.props.clipperState.augmentationPreviewInfo.previewBodyHtml);
                }
                break;
            case status_1.Status.NotStarted:
            case status_1.Status.InProgress:
                return this.getSpinner();
            default:
            case status_1.Status.Failed:
                break;
        }
        return undefined;
    };
    AugmentationPreview.prototype.getSpinner = function () {
        var spinner = m.component(spriteAnimation_1.SpriteAnimation, {spriteUrl:extensionUtils_1.ExtensionUtils.getImageResourceUrl("spinner_loop_colored.png"), imageHeight:65, imageWidth:45, totalFrameCount:21, loop:true});
        return {tag: "div", attrs: {className:constants_1.Constants.Classes.centeredInPreview}, children: [spinner]};
    };
    return AugmentationPreview;
}(editorPreviewComponentBase_1.EditorPreviewComponentBase));
var component = AugmentationPreview.componentize();
exports.AugmentationPreview = component;
