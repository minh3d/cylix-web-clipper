"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var status_1 = require("../../status");
var editorPreviewComponentBase_1 = require("./editorPreviewComponentBase");
var SelectionPreview = (function (_super) {
    __extends(SelectionPreview, _super);
    function SelectionPreview() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SelectionPreview.prototype.getHighlightableContentBodyForCurrentStatus = function () {
        return m.trust(this.props.clipperState.selectionPreviewInfo.previewBodyHtml);
    };
    SelectionPreview.prototype.getStatus = function () {
        return status_1.Status.Succeeded;
    };
    SelectionPreview.prototype.getTitleTextForCurrentStatus = function () {
        return this.props.clipperState.previewGlobalInfo.previewTitleText;
    };
    SelectionPreview.prototype.handleBodyChange = function (newBodyHtml) {
        this.props.clipperState.setState({
            selectionPreviewInfo: { previewBodyHtml: newBodyHtml }
        });
    };
    return SelectionPreview;
}(editorPreviewComponentBase_1.EditorPreviewComponentBase));
var component = SelectionPreview.componentize();
exports.SelectionPreview = component;
