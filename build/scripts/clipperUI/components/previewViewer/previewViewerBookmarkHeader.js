"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../constants");
var localization_1 = require("../../../localization/localization");
var previewViewerTitleOnlyHeaderComponentBase_1 = require("./previewViewerTitleOnlyHeaderComponentBase");
var PreviewViewerBookmarkHeaderClass = (function (_super) {
    __extends(PreviewViewerBookmarkHeaderClass, _super);
    function PreviewViewerBookmarkHeaderClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PreviewViewerBookmarkHeaderClass.prototype.getControlGroupId = function () {
        return constants_1.Constants.Ids.bookmarkControl;
    };
    PreviewViewerBookmarkHeaderClass.prototype.getHeader = function () {
        return localization_1.Localization.getLocalizedString("WebClipper.ClipType.Bookmark.Button");
    };
    PreviewViewerBookmarkHeaderClass.prototype.getHeaderId = function () {
        return constants_1.Constants.Ids.bookmarkHeaderTitle;
    };
    return PreviewViewerBookmarkHeaderClass;
}(previewViewerTitleOnlyHeaderComponentBase_1.PreviewViewerTitleOnlyHeaderComponentBase));
var component = PreviewViewerBookmarkHeaderClass.componentize();
exports.PreviewViewerBookmarkHeader = component;
