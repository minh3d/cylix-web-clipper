"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var localization_1 = require("../../../localization/localization");
var status_1 = require("../../status");
var regionSelection_1 = require("../../components/regionSelection");
var previewComponentBase_1 = require("./previewComponentBase");
var previewViewerRegionHeader_1 = require("./previewViewerRegionHeader");
var previewViewerRegionTitleOnlyHeader_1 = require("./previewViewerRegionTitleOnlyHeader");
var RegionPreview = (function (_super) {
    __extends(RegionPreview, _super);
    function RegionPreview() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RegionPreview.prototype.getContentBodyForCurrentStatus = function () {
        var state = this.props.clipperState;
        return this.convertRegionResultToContentData(state.regionResult);
    };
    RegionPreview.prototype.getHeader = function () {
        if (this.props.clipperState.injectOptions.enableRegionClipping) {
            return <previewViewerRegionHeader_1.PreviewViewerRegionHeader clipperState={this.props.clipperState}/>;
        }
        // This mode is made possible through image-selection clipping on supporting browsers
        return <previewViewerRegionTitleOnlyHeader_1.PreviewViewerRegionTitleOnlyHeader clipperState={this.props.clipperState}/>;
    };
    RegionPreview.prototype.getStatus = function () {
        return this.props.clipperState.regionResult.status;
    };
    RegionPreview.prototype.getTitleTextForCurrentStatus = function () {
        var noContentFoundString = localization_1.Localization.getLocalizedString("WebClipper.Preview.NoContentFound");
        var previewStatus = this.getStatus();
        switch (previewStatus) {
            case status_1.Status.Succeeded:
                return this.props.clipperState.previewGlobalInfo.previewTitleText;
            case status_1.Status.NotStarted:
            case status_1.Status.InProgress:
                return localization_1.Localization.getLocalizedString("WebClipper.Preview.LoadingMessage");
            default:
            case status_1.Status.Failed:
                return noContentFoundString;
        }
    };
    RegionPreview.prototype.convertRegionResultToContentData = function (result) {
        var _this = this;
        var contentBody = [];
        switch (result.status) {
            case status_1.Status.Succeeded:
                var regions = this.props.clipperState.regionResult.data;
                // We want to disallow removal of regions in image-selection mode where the browser does not support the screenshot API
                var onRemove = this.props.clipperState.injectOptions && this.props.clipperState.injectOptions.enableRegionClipping ?
                    function (index) {
                        var newRegions = _this.props.clipperState.regionResult.data;
                        newRegions.splice(index, 1);
                        if (newRegions.length === 0) {
                            _this.props.clipperState.setState({ regionResult: { status: status_1.Status.NotStarted, data: newRegions } });
                        }
                        else {
                            _this.props.clipperState.setState({ regionResult: { status: status_1.Status.Succeeded, data: newRegions } });
                        }
                    } : undefined;
                for (var i = 0; i < regions.length; i++) {
                    contentBody.push(<regionSelection_1.RegionSelection index={i} imageSrc={regions[i]} onRemove={onRemove}/>);
                }
                break;
            default:
            case status_1.Status.NotStarted:
            case status_1.Status.InProgress:
            case status_1.Status.Failed:
                break;
        }
        return contentBody;
    };
    return RegionPreview;
}(previewComponentBase_1.PreviewComponentBase));
var component = RegionPreview.componentize();
exports.RegionPreview = component;
