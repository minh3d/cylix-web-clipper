"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../constants");
var objectUtils_1 = require("../../../objectUtils");
var extensionUtils_1 = require("../../../extensions/extensionUtils");
var localization_1 = require("../../../localization/localization");
var status_1 = require("../../status");
var spriteAnimation_1 = require("../../components/spriteAnimation");
var previewComponentBase_1 = require("./previewComponentBase");
var previewViewerBookmarkHeader_1 = require("./previewViewerBookmarkHeader");
var BookmarkPreview = (function (_super) {
    __extends(BookmarkPreview, _super);
    function BookmarkPreview() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    BookmarkPreview.prototype.getStatus = function () {
        return this.props.clipperState.bookmarkResult.status;
    };
    BookmarkPreview.prototype.getTitleTextForCurrentStatus = function () {
        var failureMessage;
        var defaultFailureMessage = localization_1.Localization.getLocalizedString("WebClipper.Preview.BookmarkModeGenericError");
        var previewStatus = this.getStatus();
        switch (previewStatus) {
            case status_1.Status.Succeeded:
                return this.props.clipperState.previewGlobalInfo.previewTitleText;
            case status_1.Status.NotStarted:
            case status_1.Status.InProgress:
                return localization_1.Localization.getLocalizedString("WebClipper.Preview.LoadingMessage");
            default:
            case status_1.Status.Failed:
                failureMessage = this.props.clipperState.bookmarkResult.data.failureMessage;
                return !!failureMessage ? failureMessage : defaultFailureMessage;
        }
    };
    BookmarkPreview.prototype.getContentBodyForCurrentStatus = function () {
        var previewStatus = this.getStatus();
        switch (previewStatus) {
            case status_1.Status.Succeeded:
                return this.convertBookmarkResultToContentData(this.props.clipperState.bookmarkResult.data);
            case status_1.Status.NotStarted:
            case status_1.Status.InProgress:
                return [this.getSpinner()];
            default:
            case status_1.Status.Failed:
                return [];
        }
    };
    BookmarkPreview.prototype.getSpinner = function () {
        var spinner = m.component(spriteAnimation_1.SpriteAnimation, {spriteUrl:extensionUtils_1.ExtensionUtils.getImageResourceUrl("spinner_loop_colored.png"), imageHeight:65, imageWidth:45, totalFrameCount:21, loop:true});
        return {tag: "div", attrs: {className:constants_1.Constants.Classes.centeredInPreview}, children: [spinner]};
    };
    BookmarkPreview.prototype.getHeader = function () {
        return m.component(previewViewerBookmarkHeader_1.PreviewViewerBookmarkHeader, {clipperState:this.props.clipperState});
    };
    // Override
    BookmarkPreview.prototype.getPreviewContentContainerClass = function () {
        return constants_1.Constants.Ids.bookmarkPreviewContentContainer;
    };
    // Override
    BookmarkPreview.prototype.getPreviewInnerContainerClass = function () {
        return constants_1.Constants.Ids.bookmarkPreviewInnerContainer;
    };
    BookmarkPreview.prototype.convertBookmarkResultToContentData = function (result) {
        var url = result.url;
        var secondColumnTdStyle = "";
        if (!objectUtils_1.ObjectUtils.isNullOrUndefined(result.thumbnailSrc)) {
            secondColumnTdStyle += "padding-left:16px;";
        }
        var urlTdStyle = "white-space:nowrap;overflow:hidden;text-overflow:ellipsis;";
        if (!objectUtils_1.ObjectUtils.isNullOrUndefined(result.description)) {
            urlTdStyle += "padding-bottom:13px;";
        }
        return ({tag: "table", attrs: {style:"table-layout:auto;border-collapse:collapse;margin-bottom:24px;"}, children: [
				{tag: "tr", attrs: {style:"vertical-align:top;"}, children: [
					this.renderThumbnailIfExists(result.thumbnailSrc), 
					{tag: "td", attrs: {style:secondColumnTdStyle}, children: [
						{tag: "table", attrs: {}, children: [
							this.renderTitleIfExists(result.title), 
							{tag: "tr", attrs: {}, children: [
								{tag: "td", attrs: {style:urlTdStyle}, children: [
									{tag: "a", attrs: {tabIndex:-1, href:url, target:"_blank", title:url}, children: [url]}
								]}
							]}, 
							this.renderDescriptionIfExists(result.description)
						]}
					]}
				]}
			]});
    };
    BookmarkPreview.prototype.renderTitleIfExists = function (title) {
        if (!objectUtils_1.ObjectUtils.isNullOrUndefined(title) && title.length > 0) {
            return ({tag: "tr", attrs: {}, children: [
					{tag: "td", attrs: {}, children: [
						{tag: "h2", attrs: {style:"margin:0;margin-bottom:13px;"}, children: [title]}
					]}
				]});
        }
    };
    BookmarkPreview.prototype.renderDescriptionIfExists = function (description) {
        if (!objectUtils_1.ObjectUtils.isNullOrUndefined(description) && description.length > 0) {
            return ({tag: "tr", attrs: {}, children: [
					{tag: "td", attrs: {style:"word-wrap:break-word;"}, children: [description]}
				]});
        }
    };
    BookmarkPreview.prototype.renderThumbnailIfExists = function (thumbnailSrc) {
        if (!objectUtils_1.ObjectUtils.isNullOrUndefined(thumbnailSrc) && thumbnailSrc.length > 0) {
            var thumbnailSize = "112";
            return ({tag: "td", attrs: {width:thumbnailSize, style:"padding-top:9px;"}, children: [
					{tag: "img", attrs: {id:constants_1.Constants.Ids.bookmarkThumbnail, src:thumbnailSrc, alt:"thumbnail", width:thumbnailSize}}
				]});
        }
    };
    return BookmarkPreview;
}(previewComponentBase_1.PreviewComponentBase));
var component = BookmarkPreview.componentize();
exports.BookmarkPreview = component;
