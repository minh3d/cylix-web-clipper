"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../constants");
var localization_1 = require("../../../localization/localization");
var previewViewerTitleOnlyHeaderComponentBase_1 = require("./previewViewerTitleOnlyHeaderComponentBase");
var PreviewViewerFullPageHeaderClass = (function (_super) {
    __extends(PreviewViewerFullPageHeaderClass, _super);
    function PreviewViewerFullPageHeaderClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PreviewViewerFullPageHeaderClass.prototype.getControlGroupId = function () {
        return constants_1.Constants.Ids.fullPageControl;
    };
    PreviewViewerFullPageHeaderClass.prototype.getHeader = function () {
        return localization_1.Localization.getLocalizedString("WebClipper.ClipType.ScreenShot.Button");
    };
    PreviewViewerFullPageHeaderClass.prototype.getHeaderId = function () {
        return constants_1.Constants.Ids.fullPageHeaderTitle;
    };
    return PreviewViewerFullPageHeaderClass;
}(previewViewerTitleOnlyHeaderComponentBase_1.PreviewViewerTitleOnlyHeaderComponentBase));
var component = PreviewViewerFullPageHeaderClass.componentize();
exports.PreviewViewerFullPageHeader = component;
