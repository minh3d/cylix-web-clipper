"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../constants");
var extensionUtils_1 = require("../../../extensions/extensionUtils");
var localization_1 = require("../../../localization/localization");
var previewViewerHeaderComponentBase_1 = require("./previewViewerHeaderComponentBase");
var PreviewViewerAugmentationHeaderClass = (function (_super) {
    __extends(PreviewViewerAugmentationHeaderClass, _super);
    function PreviewViewerAugmentationHeaderClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PreviewViewerAugmentationHeaderClass.prototype.getControlGroups = function () {
        return [this.getScreenReaderTitleGroup(), this.getHighlightGroup(), this.getSerifGroup(), this.getFontSizeGroup()];
    };
    PreviewViewerAugmentationHeaderClass.prototype.getScreenReaderTitleGroup = function () {
        return {
            className: constants_1.Constants.Classes.srOnly,
            innerElements: [
                <div>{localization_1.Localization.getLocalizedString("WebClipper.ClipType.Article.Button")}</div>
            ]
        };
    };
    PreviewViewerAugmentationHeaderClass.prototype.getHighlightGroup = function () {
        var highlighterEnabled = this.props.textHighlighterEnabled;
        var classForHighlighter = highlighterEnabled ? previewViewerHeaderComponentBase_1.HeaderClasses.Button.active : "";
        var imgSrc = highlighterEnabled ? "editorOptions/highlight_tool_on.png" : "editorOptions/highlight_tool_off.png";
        return {
            id: constants_1.Constants.Ids.highlightControl,
            innerElements: [
                <img role="button" aria-label={localization_1.Localization.getLocalizedString("WebClipper.Accessibility.ScreenReader.ToggleHighlighterForArticleMode")} aria-pressed={highlighterEnabled ? "true" : "false"} id={constants_1.Constants.Ids.highlightButton} {...this.enableInvoke({ callback: this.props.toggleHighlight, tabIndex: 100 })} className={classForHighlighter} src={extensionUtils_1.ExtensionUtils.getImageResourceUrl(imgSrc)}/>
            ]
        };
    };
    PreviewViewerAugmentationHeaderClass.prototype.getSerifGroup = function () {
        var tabIndexOn = 101;
        var tabIndexOff = -1;
        return {
            id: constants_1.Constants.Ids.serifControl,
            role: "radiogroup",
            isAriaSet: true,
            innerElements: [
                <button aria-label={localization_1.Localization.getLocalizedString("WebClipper.Accessibility.ScreenReader.ChangeFontToSansSerif")} aria-checked={!this.props.serif + ""} id={constants_1.Constants.Ids.sansSerif} {...this.enableAriaInvoke({ callback: this.props.changeFontFamily, tabIndex: !this.props.serif ? tabIndexOn : tabIndexOff, args: false, ariaSetName: constants_1.Constants.AriaSet.serifGroupSet })} className={!this.props.serif ? previewViewerHeaderComponentBase_1.HeaderClasses.Button.activeControlButton : previewViewerHeaderComponentBase_1.HeaderClasses.Button.controlButton} role="radio">
					{localization_1.Localization.getLocalizedString("WebClipper.Preview.Header.SansSerifButtonLabel")}
				</button>,
                <button aria-label={localization_1.Localization.getLocalizedString("WebClipper.Accessibility.ScreenReader.ChangeFontToSerif")} aria-checked={this.props.serif + ""} id={constants_1.Constants.Ids.serif} {...this.enableAriaInvoke({ callback: this.props.changeFontFamily, tabIndex: this.props.serif ? tabIndexOn : tabIndexOff, args: true, ariaSetName: constants_1.Constants.AriaSet.serifGroupSet })} className={this.props.serif ? previewViewerHeaderComponentBase_1.HeaderClasses.Button.activeControlButton : previewViewerHeaderComponentBase_1.HeaderClasses.Button.controlButton} role="radio">
					{localization_1.Localization.getLocalizedString("WebClipper.Preview.Header.SerifButtonLabel")}
				</button>
            ]
        };
    };
    PreviewViewerAugmentationHeaderClass.prototype.getFontSizeGroup = function () {
        return {
            id: constants_1.Constants.Ids.fontSizeControl,
            className: previewViewerHeaderComponentBase_1.HeaderClasses.Button.relatedButtons,
            isAriaSet: true,
            innerElements: [
                <button className={previewViewerHeaderComponentBase_1.HeaderClasses.Button.controlButton} aria-label={localization_1.Localization.getLocalizedString("WebClipper.Accessibility.ScreenReader.DecreaseFontSize")} type="button" {...this.enableInvoke({ callback: this.props.changeFontSize, tabIndex: 103, args: false })} id={constants_1.Constants.Ids.decrementFontSize}>
					<img src={extensionUtils_1.ExtensionUtils.getImageResourceUrl("editorOptions/font_down.png")}/>
				</button>,
                <button className={previewViewerHeaderComponentBase_1.HeaderClasses.Button.controlButton} aria-label={localization_1.Localization.getLocalizedString("WebClipper.Accessibility.ScreenReader.IncreaseFontSize")} type="button" {...this.enableInvoke({ callback: this.props.changeFontSize, tabIndex: 104, args: true })} id={constants_1.Constants.Ids.incrementFontSize}>
					<img src={extensionUtils_1.ExtensionUtils.getImageResourceUrl("editorOptions/font_up.png")}/>
				</button>
            ]
        };
    };
    return PreviewViewerAugmentationHeaderClass;
}(previewViewerHeaderComponentBase_1.PreviewViewerHeaderComponentBase));
var component = PreviewViewerAugmentationHeaderClass.componentize();
exports.PreviewViewerAugmentationHeader = component;
