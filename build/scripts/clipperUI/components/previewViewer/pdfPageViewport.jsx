"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../constants");
var componentBase_1 = require("../../componentBase");
var spriteAnimation_1 = require("../../components/spriteAnimation");
var extensionUtils_1 = require("../../../extensions/extensionUtils");
var PdfPageViewportClass = (function (_super) {
    __extends(PdfPageViewportClass, _super);
    function PdfPageViewportClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PdfPageViewportClass.prototype.getViewportStyle = function () {
        var styleString = "max-width: " + this.props.viewportDimensions.width + "px;";
        return styleString + "max-height: " + this.props.viewportDimensions.height + "px;";
    };
    PdfPageViewportClass.prototype.getPlaceholderStyle = function () {
        return "padding-bottom: " + ((this.props.viewportDimensions.height / this.props.viewportDimensions.width) * 100) + "%;";
    };
    PdfPageViewportClass.prototype.getSpinner = function () {
        var spinner = <spriteAnimation_1.SpriteAnimation spriteUrl={extensionUtils_1.ExtensionUtils.getImageResourceUrl("spinner_loop_colored.png")} imageHeight={Math.min(65, this.props.viewportDimensions.height)} imageWidth={Math.min(45, this.props.viewportDimensions.width)} totalFrameCount={21} loop={true}/>;
        return <div className={constants_1.Constants.Classes.centeredInCanvas}>{spinner}</div>;
    };
    PdfPageViewportClass.prototype.render = function () {
        return (<div data-pageindex={this.props.index} style={this.getViewportStyle()}>
				{this.props.imgUrl ?
            <img className={constants_1.Constants.Classes.pdfPreviewImage} src={this.props.imgUrl} style={this.getViewportStyle()}></img> :
            <div style={this.getPlaceholderStyle()}>{this.getSpinner()}</div>}
			</div>);
    };
    return PdfPageViewportClass;
}(componentBase_1.ComponentBase));
var component = PdfPageViewportClass.componentize();
exports.PdfPageViewport = component;
