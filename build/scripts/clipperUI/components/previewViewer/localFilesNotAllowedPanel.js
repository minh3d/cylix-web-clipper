"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../constants");
var localization_1 = require("../../../localization/localization");
var componentBase_1 = require("../../componentBase");
var LocalFilesNotAllowedPanelClass = (function (_super) {
    __extends(LocalFilesNotAllowedPanelClass, _super);
    function LocalFilesNotAllowedPanelClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LocalFilesNotAllowedPanelClass.prototype.render = function () {
        return ({tag: "div", attrs: {id:constants_1.Constants.Ids.previewOuterContainer, style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)}, children: [
				{tag: "div", attrs: {id:constants_1.Constants.Ids.previewInnerWrapper}, children: [
					{tag: "div", attrs: {id:constants_1.Constants.Ids.previewInnerContainer, style:"height: auto;"}, children: [
						{tag: "div", attrs: {id:constants_1.Constants.Ids.previewOptionsContainer}, children: [
							{tag: "div", attrs: {className:"control-button-group"}, children: [
								{tag: "span", attrs: {id:constants_1.Constants.Ids.localPdfFileTitle, style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular), className:"buttonLabelFont"}, children: [
									this.props.header
								]}
							]}
						]}, 
						{tag: "div", attrs: {id:constants_1.Constants.Ids.previewContentContainer, className:"local-pdf-file-content-container"}, children: [
							{tag: "div", attrs: {className:constants_1.Constants.Classes.localPdfPanelTitle}, children: [this.props.title]}, 
							{tag: "div", attrs: {className:constants_1.Constants.Classes.localPdfPanelSubtitle}, children: [this.props.subtitle]}
						]}
					]}
				]}
			]});
    };
    return LocalFilesNotAllowedPanelClass;
}(componentBase_1.ComponentBase));
exports.LocalFilesNotAllowedPanelClass = LocalFilesNotAllowedPanelClass;
var component = LocalFilesNotAllowedPanelClass.componentize();
exports.LocalFilesNotAllowedPanel = component;
