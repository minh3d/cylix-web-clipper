"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../constants");
var extensionUtils_1 = require("../../../extensions/extensionUtils");
var highlighter_1 = require("../../../highlighting/highlighter");
var previewComponentBase_1 = require("./previewComponentBase");
var previewViewerAugmentationHeader_1 = require("./previewViewerAugmentationHeader");
var _ = require("lodash");
/**
 * Child components will inherit the editor header where users can make rich edits to their content, such as highlighting
 * and font changes. Within the preview body element, this component renders a highlightable preview body element underneath
 * it that the highlighter is attached to. Highlighting logic can only take place within this element, and this prevents
 * other types of preview bodies from accidentally providing highlighting functionality.
 */
var EditorPreviewComponentBase = (function (_super) {
    __extends(EditorPreviewComponentBase, _super);
    function EditorPreviewComponentBase(props) {
        var _this = _super.call(this, props) || this;
        _this.clickHandler = _this.handleClick.bind(_this);
        // This is to make sure we cleanly override the old click handler before we attach the current child's one
        if (EditorPreviewComponentBase.currentClickHandler) {
            window.removeEventListener("click", EditorPreviewComponentBase.currentClickHandler);
        }
        // We have to do it this way as we lose old onclick event listeners when we try and attach them to buttons individually
        window.addEventListener("click", _this.clickHandler);
        EditorPreviewComponentBase.currentClickHandler = _this.clickHandler;
        return _this;
    }
    // Override
    EditorPreviewComponentBase.prototype.getContentBodyForCurrentStatus = function () {
        return [
            {tag: "div", attrs: {id:constants_1.Constants.Ids.highlightablePreviewBody}, children: [this.getHighlightableContentBodyForCurrentStatus()]}
        ];
    };
    // Override
    EditorPreviewComponentBase.prototype.getPreviewBodyConfig = function () {
        if (!this.state.textHighlighter) {
            this.setHighlighter();
        }
        if (this.props.clipperState.previewGlobalInfo.highlighterEnabled) {
            this.state.textHighlighter.enable();
        }
        else {
            this.state.textHighlighter.disable();
        }
    };
    EditorPreviewComponentBase.prototype.getHeader = function () {
        return m.component(previewViewerAugmentationHeader_1.PreviewViewerAugmentationHeader, {toggleHighlight:this.toggleHighlight.bind(this), changeFontFamily:this.changeFontFamily.bind(this), changeFontSize:this.changeFontSize.bind(this), serif:this.props.clipperState.previewGlobalInfo.serif, textHighlighterEnabled:this.props.clipperState.previewGlobalInfo.highlighterEnabled});
    };
    // Override
    EditorPreviewComponentBase.prototype.getPreviewBodyClass = function () {
        return this.state.textHighlighter && this.state.textHighlighter.isEnabled() ? constants_1.Constants.Classes.highlightable : "";
    };
    EditorPreviewComponentBase.prototype.changeFontFamily = function (serif) {
        _.assign(_.extend(this.props.clipperState.previewGlobalInfo, {
            serif: serif
        }), this.props.clipperState.setState);
    };
    EditorPreviewComponentBase.prototype.changeFontSize = function (increase) {
        var newFontSize = this.props.clipperState.previewGlobalInfo.fontSize + (increase ? 2 : -2);
        if (newFontSize < constants_1.Constants.Settings.minimumFontSize) {
            newFontSize = constants_1.Constants.Settings.minimumFontSize;
        }
        else if (newFontSize > constants_1.Constants.Settings.maximumFontSize) {
            newFontSize = constants_1.Constants.Settings.maximumFontSize;
        }
        _.assign(_.extend(this.props.clipperState.previewGlobalInfo, {
            fontSize: newFontSize
        }), this.props.clipperState.setState);
    };
    EditorPreviewComponentBase.prototype.deleteHighlight = function (timestamp) {
        var highlightablePreviewBody = document.getElementById(constants_1.Constants.Ids.highlightablePreviewBody);
        var highlightedElements = highlightablePreviewBody.querySelectorAll("span.highlighted[data-timestamp='" + timestamp + "']");
        for (var i = 0; i < highlightedElements.length; i++) {
            var current = highlightedElements[i];
            var parent_1 = current.parentNode;
            parent_1.insertBefore(document.createTextNode(current.innerText), current);
            parent_1.removeChild(current);
        }
        this.handleBodyChange(highlightablePreviewBody.innerHTML);
    };
    EditorPreviewComponentBase.prototype.handleClick = function (event) {
        if (event && event.target) {
            var element = event.target;
            if (element.className && element.className.indexOf(constants_1.Constants.Classes.deleteHighlightButton) !== -1) {
                this.deleteHighlight(parseInt(element.getAttribute("data-timestamp"), 10));
                // If the button lives inside an anchor, we don't want to trigger that anchor if the button was clicked
                event.preventDefault();
            }
        }
    };
    EditorPreviewComponentBase.prototype.setHighlighter = function () {
        var _this = this;
        var addDeleteButton = function (range, normalizedHighlights) {
            if (normalizedHighlights && normalizedHighlights.length > 0) {
                var highlightablePreviewBody = document.getElementById(constants_1.Constants.Ids.highlightablePreviewBody);
                // We need to get the latest timestamp for normalizing all encompassed highlights later
                var timestamps = normalizedHighlights.map(function (span) { return parseInt(span.getAttribute("data-timestamp"), 10 /* radix */); });
                var timestamp = Math.max.apply(undefined, timestamps);
                // The highlight may have intersected another highlight ...
                for (var i = 0; i < normalizedHighlights.length; i++) {
                    // ... so we should delete their old delete buttons, and normalize them to the same timestamp
                    var oldHighlightTimestamp = normalizedHighlights[i].getAttribute("data-timestamp");
                    var oldHighlights = highlightablePreviewBody.querySelectorAll("span." + constants_1.Constants.Classes.highlighted + "[data-timestamp='" + oldHighlightTimestamp + "']");
                    for (var j = 0; j < oldHighlights.length; j++) {
                        // Delete old delete buttons
                        var oldButtons = oldHighlights[j].querySelectorAll("img." + constants_1.Constants.Classes.deleteHighlightButton);
                        for (var k = 0; k < oldButtons.length; k++) {
                            oldButtons[k].parentNode.removeChild(oldButtons[k]);
                        }
                        // Normalize timestamp
                        oldHighlights[j].setAttribute("data-timestamp", "" + timestamp);
                    }
                }
                // Find the first instance of the highlight and add the delete button
                var firstHighlighted = highlightablePreviewBody.querySelector("span.highlighted[data-timestamp='" + timestamp + "']");
                if (firstHighlighted) {
                    var deleteHighlight = document.createElement("img");
                    deleteHighlight.src = extensionUtils_1.ExtensionUtils.getImageResourceUrl("editoroptions/delete_button.png");
                    deleteHighlight.className = constants_1.Constants.Classes.deleteHighlightButton;
                    deleteHighlight.setAttribute("data-timestamp", "" + timestamp);
                    firstHighlighted.insertBefore(deleteHighlight, firstHighlighted.childNodes[0]);
                }
                _this.handleBodyChange(highlightablePreviewBody.innerHTML);
            }
        };
        var textHighlighter = highlighter_1.Highlighter.reconstructInstance(document.getElementById(constants_1.Constants.Ids.highlightablePreviewBody), {
            color: constants_1.Constants.Styles.Colors.oneNoteHighlightColor,
            contextClass: constants_1.Constants.Classes.highlightable,
            onAfterHighlight: addDeleteButton
        });
        this.setState({
            textHighlighter: textHighlighter
        });
    };
    EditorPreviewComponentBase.prototype.toggleHighlight = function () {
        if (!this.props.clipperState.previewGlobalInfo.highlighterEnabled && !window.getSelection().isCollapsed && this.selectionIsInPreviewBody()) {
            // If the user selects something and clicks the highlighter button, we behave traditionally (i.e., perform highlighting, not toggling)
            this.state.textHighlighter.doHighlight();
        }
        else {
            // No selection found, so we actually toggle the highlighter functionality
            _.assign(_.extend(this.props.clipperState.previewGlobalInfo, {
                highlighterEnabled: !this.props.clipperState.previewGlobalInfo.highlighterEnabled
            }), this.props.clipperState.setState);
        }
    };
    // Similarly adopted from: http://stackoverflow.com/questions/8339857/how-to-know-if-selected-text-is-inside-a-specific-div
    EditorPreviewComponentBase.prototype.selectionIsInPreviewBody = function () {
        var previewBody = document.getElementById(constants_1.Constants.Ids.highlightablePreviewBody);
        if (!previewBody) {
            return false;
        }
        var selection = window.getSelection();
        if (selection.rangeCount > 0) {
            // Check that all range parts belong to the preview body
            for (var i = 0; i < selection.rangeCount; i++) {
                var commonAncestorContainer = selection.getRangeAt(i).commonAncestorContainer;
                if (!(commonAncestorContainer === previewBody || previewBody.contains(commonAncestorContainer))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    };
    return EditorPreviewComponentBase;
}(previewComponentBase_1.PreviewComponentBase));
exports.EditorPreviewComponentBase = EditorPreviewComponentBase;
