"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var _ = require("lodash");
var clientType_1 = require("../../../clientType");
var constants_1 = require("../../../constants");
var localization_1 = require("../../../localization/localization");
var clipMode_1 = require("../../clipMode");
var componentBase_1 = require("../../componentBase");
var status_1 = require("../../status");
var annotationInput_1 = require("../annotationInput");
var PreviewComponentBase = (function (_super) {
    __extends(PreviewComponentBase, _super);
    function PreviewComponentBase() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Returns a config callback that should be added to the preview body, and is meant to be
     * overridden by child classes on a per-need basis
     */
    PreviewComponentBase.prototype.getPreviewBodyConfig = function () {
        return undefined;
    };
    PreviewComponentBase.prototype.addTextAreaListener = function () {
        var _this = this;
        document.addEventListener("input", function (event) {
            var element = event.target;
            var previewHeaderInput = document.getElementById(constants_1.Constants.Ids.previewHeaderInput);
            if (!!element && element === previewHeaderInput) {
                _this.handleTitleChange(previewHeaderInput.value);
            }
        });
    };
    PreviewComponentBase.prototype.handleTitleChange = function (newTitleText) {
        _.assign(_.extend(this.props.clipperState.previewGlobalInfo, {
            previewTitleText: newTitleText
        }), this.props.clipperState.setState);
    };
    PreviewComponentBase.prototype.getPreviewTitle = function (contentTitle, titleIsEditable, inProgressClassIfApplicable) {
        if (this.props.clipperState.currentMode.get() !== clipMode_1.ClipMode.Bookmark) {
            return ({tag: "div", attrs: {id:constants_1.Constants.Ids.previewTitleContainer}, children: [
					{tag: "pre", attrs: {className:constants_1.Constants.Classes.textAreaInputMirror}, children: [{tag: "span", attrs: {}, children: [contentTitle]}, {tag: "br", attrs: {}}]}, 
					{tag: "textarea", attrs: {rows:"1", id:constants_1.Constants.Ids.previewHeaderInput, tabIndex:this.getStatus() === status_1.Status.Succeeded ? 200 : -1, "aria-label":localization_1.Localization.getLocalizedString("WebClipper.Accessibility.ScreenReader.InputBoxToChangeTitleOfOneNotePage"), className:!titleIsEditable ? constants_1.Constants.Classes.textAreaInput + inProgressClassIfApplicable : constants_1.Constants.Classes.textAreaInput, value:contentTitle, readOnly:!titleIsEditable}
					}
				]});
        }
    };
    PreviewComponentBase.prototype.getPreviewSubtitle = function () {
        var sourceUrlCitationPrefix = localization_1.Localization.getLocalizedString("WebClipper.FromCitation")
            .replace("{0}", ""); // TODO can we change this loc string to remove the {0}?
        var sourceUrl = this.props.clipperState.pageInfo ? this.props.clipperState.pageInfo.rawUrl : "";
        return ({tag: "div", attrs: {id:constants_1.Constants.Ids.previewSubtitleContainer}, children: [
				this.props.clipperState.injectOptions && this.props.clipperState.injectOptions.enableAddANote ?
            m.component(annotationInput_1.AnnotationInput, {clipperState:this.props.clipperState})
            : undefined, 
				this.props.clipperState.currentMode.get() !== clipMode_1.ClipMode.Bookmark ?
            {tag: "div", attrs: {id:constants_1.Constants.Ids.previewUrlContainer}, children: [
						{tag: "span", attrs: {"aria-label":sourceUrlCitationPrefix}, children: [sourceUrlCitationPrefix]}, 
						{tag: "a", attrs: {tabIndex:-1, href:sourceUrl, target:"_blank", "aria-label":sourceUrl, title:sourceUrl}, children: [sourceUrl]}
					]}
            : undefined
			]});
    };
    /**
     * Returns a class string that should be added to the preview body, and is meant to be
     * overridden by child classes on a per-need basis
     */
    PreviewComponentBase.prototype.getPreviewBodyClass = function () {
        return "";
    };
    /**
     * Returns a class string that should be added to the preview content container
     * and is meant to be overriden by child classes on a per-need basis
     */
    PreviewComponentBase.prototype.getPreviewContentContainerClass = function () {
        return "";
    };
    /**
     * Returns a class string that should be added to the preview inner container
     * and is meant to be overriden by child classes on a per-need basis
     */
    PreviewComponentBase.prototype.getPreviewInnerContainerClass = function () {
        return "";
    };
    // Can be overriden by child classes to disable the title
    PreviewComponentBase.prototype.isTitleEnabled = function () {
        return true;
    };
    PreviewComponentBase.prototype.render = function () {
        if (!PreviewComponentBase.textAreaListenerAttached) {
            this.addTextAreaListener();
            PreviewComponentBase.textAreaListenerAttached = true;
        }
        var contentTitle = this.getTitleTextForCurrentStatus();
        var contentBody = this.getContentBodyForCurrentStatus();
        var editableTitleEnabled = this.props.clipperState.injectOptions && this.props.clipperState.injectOptions.enableEditableTitle;
        var titleIsEditable = editableTitleEnabled && this.getStatus() === status_1.Status.Succeeded &&
            contentTitle === this.props.clipperState.previewGlobalInfo.previewTitleText;
        var fontFamilyString = (this.props.clipperState.previewGlobalInfo.serif) ? "WebClipper.FontFamily.Preview.SerifDefault" : "WebClipper.FontFamily.Preview.SansSerifDefault";
        var previewStyle = {
            fontFamily: localization_1.Localization.getLocalizedString(fontFamilyString),
            fontSize: this.props.clipperState.previewGlobalInfo.fontSize.toString() + "px"
        };
        var statusForCurrentMode = this.getStatus();
        var inProgressClassIfApplicable = statusForCurrentMode === status_1.Status.InProgress ? " in-progress" : "";
        // In IE, height: auto will result in a height of 0 so we have to set it to 100% like the other modes
        var previewInnerContainerClass = this.props.clipperState.clientInfo.clipperType === clientType_1.ClientType.Bookmarklet ? "" : this.getPreviewInnerContainerClass();
        return ({tag: "div", attrs: {id:constants_1.Constants.Ids.previewOuterContainer, style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)}, children: [
				{tag: "div", attrs: {id:constants_1.Constants.Ids.previewInnerWrapper}, children: [
					{tag: "div", attrs: {id:constants_1.Constants.Ids.previewInnerContainer, className:previewInnerContainerClass}, children: [
						{tag: "div", attrs: {id:constants_1.Constants.Ids.previewOptionsContainer}, children: [
							this.getHeader()
						]}, 
						{tag: "div", attrs: {id:constants_1.Constants.Ids.previewContentContainer, className:inProgressClassIfApplicable + " " + this.getPreviewContentContainerClass()}, children: [
							this.isTitleEnabled() ? {tag: "div", attrs: {id:constants_1.Constants.Ids.previewHeaderContainer}, children: [
								this.getPreviewTitle(contentTitle, titleIsEditable, inProgressClassIfApplicable), 
								this.getPreviewSubtitle()
							]} : "", 
							{tag: "div", attrs: {style:previewStyle, id:constants_1.Constants.Ids.previewBody, className:inProgressClassIfApplicable + " " + this.getPreviewBodyClass(), config:this.getPreviewBodyConfig.bind(this)}, children: [
								contentBody
							]}
						]}
					]}
				]}
			]});
    };
    return PreviewComponentBase;
}(componentBase_1.ComponentBase));
PreviewComponentBase.textAreaListenerAttached = false;
exports.PreviewComponentBase = PreviewComponentBase;
