"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../../constants");
var extensionUtils_1 = require("../../../extensions/extensionUtils");
var localization_1 = require("../../../localization/localization");
var status_1 = require("../../status");
var previewViewerHeaderComponentBase_1 = require("./previewViewerHeaderComponentBase");
var PreviewViewerRegionHeaderClass = (function (_super) {
    __extends(PreviewViewerRegionHeaderClass, _super);
    function PreviewViewerRegionHeaderClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PreviewViewerRegionHeaderClass.prototype.addAnotherRegion = function () {
        this.props.clipperState.setState({
            regionResult: {
                status: status_1.Status.InProgress,
                data: this.props.clipperState.regionResult.data
            }
        });
    };
    PreviewViewerRegionHeaderClass.prototype.getControlGroups = function () {
        return [this.getScreenReaderTitleGroup(), this.getAddRegionGroup()];
    };
    PreviewViewerRegionHeaderClass.prototype.getScreenReaderTitleGroup = function () {
        return {
            className: constants_1.Constants.Classes.srOnly,
            innerElements: [
                {tag: "div", attrs: {}, children: [localization_1.Localization.getLocalizedString("WebClipper.ClipType.Region.Button")]}
            ]
        };
    };
    PreviewViewerRegionHeaderClass.prototype.getAddRegionGroup = function () {
        return {
            id: constants_1.Constants.Ids.addRegionControl,
            innerElements: [
                {tag: "button", attrs: Object.assign({id:constants_1.Constants.Ids.addAnotherRegionButton},  this.enableInvoke({ callback: this.addAnotherRegion.bind(this), tabIndex: 100 }),{className:previewViewerHeaderComponentBase_1.HeaderClasses.Button.controlButton, style:localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular), type:"button"}), children: [
					{tag: "img", attrs: {src:extensionUtils_1.ExtensionUtils.getImageResourceUrl("editorOptions/add_icon.png")}}, 
					{tag: "span", attrs: {}, children: [localization_1.Localization.getLocalizedString("WebClipper.Preview.Header.AddAnotherRegionButtonLabel")]}
				]}
            ]
        };
    };
    return PreviewViewerRegionHeaderClass;
}(previewViewerHeaderComponentBase_1.PreviewViewerHeaderComponentBase));
var component = PreviewViewerRegionHeaderClass.componentize();
exports.PreviewViewerRegionHeader = component;
