"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var localization_1 = require("../../../localization/localization");
var previewViewerHeaderComponentBase_1 = require("./previewViewerHeaderComponentBase");
var PreviewViewerTitleOnlyHeaderComponentBase = (function (_super) {
    __extends(PreviewViewerTitleOnlyHeaderComponentBase, _super);
    function PreviewViewerTitleOnlyHeaderComponentBase() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PreviewViewerTitleOnlyHeaderComponentBase.prototype.getControlGroups = function () {
        return [this.getTitleGroup(this.getControlGroupId(), this.getHeader(), this.getHeaderId())];
    };
    PreviewViewerTitleOnlyHeaderComponentBase.prototype.getTitleGroup = function (controlGroupId, header, headerId) {
        return {
            id: controlGroupId,
            innerElements: [
                <div id={headerId} style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)} className="buttonLabelFont">
					<span>{header}</span>
				</div>
            ]
        };
    };
    return PreviewViewerTitleOnlyHeaderComponentBase;
}(previewViewerHeaderComponentBase_1.PreviewViewerHeaderComponentBase));
exports.PreviewViewerTitleOnlyHeaderComponentBase = PreviewViewerTitleOnlyHeaderComponentBase;
