"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var componentBase_1 = require("../../componentBase");
/**
 * Shared class names used by header elements for consistent styling.
 */
var HeaderClasses;
(function (HeaderClasses) {
    var Button;
    (function (Button) {
        Button.active = " active";
        Button.controlButton = " control-button";
        Button.relatedButtons = " related-buttons";
        Button.activeControlButton = Button.active + Button.controlButton;
    })(Button = HeaderClasses.Button || (HeaderClasses.Button = {}));
})(HeaderClasses = exports.HeaderClasses || (exports.HeaderClasses = {}));
/**
 * Represents a preview header that can contain multiple control button groups, i.e., groups
 * of buttons or similar header entities. Child classes need to simply declare each control's
 * buttons.
 */
var PreviewViewerHeaderComponentBase = (function (_super) {
    __extends(PreviewViewerHeaderComponentBase, _super);
    function PreviewViewerHeaderComponentBase() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PreviewViewerHeaderComponentBase.prototype.render = function () {
        var controlButtonGroup = "control-button-group";
        var renderables = [];
        var buttonGroups = this.getControlGroups();
        for (var i = 0; i < buttonGroups.length; i++) {
            var currentButtonGroup = buttonGroups[i];
            var id = currentButtonGroup.id;
            var className = currentButtonGroup.className;
            var role = currentButtonGroup.role;
            var isAriaSet = currentButtonGroup.isAriaSet;
            if (isAriaSet) {
                var setSize = currentButtonGroup.innerElements.length;
                for (var j = 0; j < setSize; j++) {
                    currentButtonGroup.innerElements[j].attrs["aria-posinset"] = j + 1;
                    currentButtonGroup.innerElements[j].attrs["aria-setsize"] = setSize;
                }
            }
            renderables.push(<div id={id ? id : ""} className={className ? className : controlButtonGroup} role={role ? role : ""}>
					{currentButtonGroup.innerElements}
				</div>);
        }
        return (<div>
				{renderables}
			</div>);
    };
    return PreviewViewerHeaderComponentBase;
}(componentBase_1.ComponentBase));
exports.PreviewViewerHeaderComponentBase = PreviewViewerHeaderComponentBase;
