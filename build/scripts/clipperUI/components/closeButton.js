"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../constants");
var extensionUtils_1 = require("../../extensions/extensionUtils");
var localization_1 = require("../../localization/localization");
var componentBase_1 = require("../componentBase");
var CloseButtonClass = (function (_super) {
    __extends(CloseButtonClass, _super);
    function CloseButtonClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CloseButtonClass.prototype.getInitialState = function () {
        return {};
    };
    CloseButtonClass.prototype.render = function () {
        return ({tag: "div", attrs: {id:constants_1.Constants.Ids.closeButtonContainer}, children: [
				{tag: "a", attrs: Object.assign({id:constants_1.Constants.Ids.closeButton, role:"button", "aria-label":localization_1.Localization.getLocalizedString("WebClipper.Action.CloseTheClipper")},  this.enableInvoke({ callback: this.props.onClickHandler, tabIndex: 300, args: this.props.onClickHandlerParams })), children: [
					{tag: "img", attrs: {className:"closeButtonIcon", src:extensionUtils_1.ExtensionUtils.getImageResourceUrl("close.png")}}
				]}
			]});
    };
    return CloseButtonClass;
}(componentBase_1.ComponentBase));
var component = CloseButtonClass.componentize();
exports.CloseButton = component;
