"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../constants");
var localization_1 = require("../../localization/localization");
var componentBase_1 = require("../componentBase");
var spriteAnimation_1 = require("./spriteAnimation");
var RotatingMessageSpriteAnimationClass = (function (_super) {
    __extends(RotatingMessageSpriteAnimationClass, _super);
    function RotatingMessageSpriteAnimationClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    RotatingMessageSpriteAnimationClass.prototype.render = function () {
        var shouldDisplayMessage = "shouldDisplayMessage" in this.props ? this.props.shouldDisplayMessage : true;
        return (<div>
				<spriteAnimation_1.SpriteAnimation spriteUrl={this.props.spriteUrl} imageHeight={this.props.imageHeight} imageWidth={this.props.imageWidth} totalFrameCount={this.props.totalFrameCount} loop={this.props.loop}/>
				{shouldDisplayMessage ?
            <div id={constants_1.Constants.Ids.spinnerText} className="spinnerText" style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Semilight)}>
						{localization_1.Localization.getLocalizedString("WebClipper.Preview.Spinner.ClipAnyTimeInFullPage")}
					</div>
            : ""}
			</div>);
    };
    return RotatingMessageSpriteAnimationClass;
}(componentBase_1.ComponentBase));
var component = RotatingMessageSpriteAnimationClass.componentize();
exports.RotatingMessageSpriteAnimation = component;
