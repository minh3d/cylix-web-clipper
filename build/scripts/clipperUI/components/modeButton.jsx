"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../constants");
var localization_1 = require("../../localization/localization");
var clipMode_1 = require("../clipMode");
var componentBase_1 = require("../componentBase");
var ModeButtonClass = (function (_super) {
    __extends(ModeButtonClass, _super);
    function ModeButtonClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ModeButtonClass.prototype.buttonHandler = function () {
        this.props.onModeSelected(this.props.myMode);
    };
    ModeButtonClass.prototype.render = function () {
        var className = "modeButton";
        if (this.props.selected) {
            className += " selected";
        }
        var clipMode = clipMode_1.ClipMode[this.props.myMode];
        clipMode = clipMode[0].toLowerCase() + clipMode.slice(1);
        var idName = clipMode + "Button";
        return (<a className={className} role="option" aria-selected={this.props.selected} id={idName} title={this.props.tooltipText ? this.props.tooltipText : ""} aria-setsize={this.props["aria-setsize"]} aria-posinset={this.props["aria-posinset"]} {...this.enableAriaInvoke({ callback: this.buttonHandler, tabIndex: this.props.tabIndex, ariaSetName: constants_1.Constants.AriaSet.modeButtonSet })}>
				<img className="icon" src={this.props.imgSrc}/>
				<span className="label buttonLabelFont" style={localization_1.Localization.getFontFamilyAsStyle(localization_1.Localization.FontFamily.Regular)}>
					{this.props.label}
				</span>
			</a>);
    };
    return ModeButtonClass;
}(componentBase_1.ComponentBase));
var component = ModeButtonClass.componentize();
exports.ModeButton = component;
