"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var componentBase_1 = require("../componentBase");
var popperJS = require("popper.js");
var PopoverClass = (function (_super) {
    __extends(PopoverClass, _super);
    function PopoverClass(props) {
        return _super.call(this, props) || this;
    }
    PopoverClass.prototype.handlePopoverLifecycle = function (element, isInitialized, context) {
        var _this = this;
        if (!isInitialized) {
            var popperElement = this.generatePopperElement(this.props.parentId);
            // TODO temporarily typed this way until definitions is updated for popperJS.PopperOptions
            var popperOptions = {
                placement: this.props.placement,
                removeOnDestroy: this.props.removeOnDestroy,
                modifiers: {}
            };
            if (this.props.modifiersIgnored) {
                for (var i = 0; i < this.props.modifiersIgnored.length; i++) {
                    popperOptions.modifiers[this.props.modifiersIgnored[i]] = { enabled: false };
                }
            }
            this.refToPopper = new popperJS(document.getElementById(this.props.referenceElementId), popperElement, popperOptions);
        }
        if (isInitialized) {
            if (this.refToPopper) {
                this.refToPopper.update();
            }
        }
        context.onunload = function () {
            if (_this.refToPopper) {
                _this.refToPopper.destroy();
                _this.refToPopper = undefined;
            }
        };
    };
    PopoverClass.prototype.generatePopperElement = function (parentId) {
        var popperElement = document.createElement("div");
        popperElement.innerText = this.props.content;
        if (this.props.classNames) {
            for (var i = 0; i < this.props.classNames.length; i++) {
                popperElement.classList.add(this.props.classNames[i]);
            }
        }
        if (this.props.arrowClassNames) {
            var arrowElement = document.createElement("div");
            for (var i = 0; i < this.props.arrowClassNames.length; i++) {
                arrowElement.classList.add(this.props.arrowClassNames[i]);
            }
            arrowElement.setAttribute("x-arrow", "");
            popperElement.appendChild(arrowElement);
        }
        var parent = parentId ? document.getElementById(parentId) : undefined;
        if (parent) {
            // We want to set the parent lower in the HTML hierarchy to avoid z-index issues relating to stacking contexts
            parent.appendChild(popperElement);
        }
        else {
            document.body.appendChild(popperElement);
        }
        return popperElement;
    };
    PopoverClass.prototype.render = function () {
        return ({tag: "div", attrs: {config:this.handlePopoverLifecycle.bind(this)}});
    };
    return PopoverClass;
}(componentBase_1.ComponentBase));
var component = PopoverClass.componentize();
exports.Popover = component;
