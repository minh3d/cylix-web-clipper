"use strict";
var invokeSource_1 = require("../extensions/invokeSource");
var TooltipType;
(function (TooltipType) {
    TooltipType[TooltipType["ChangeLog"] = 0] = "ChangeLog";
    TooltipType[TooltipType["Pdf"] = 1] = "Pdf";
    TooltipType[TooltipType["Product"] = 2] = "Product";
    TooltipType[TooltipType["Recipe"] = 3] = "Recipe";
    TooltipType[TooltipType["Video"] = 4] = "Video";
    TooltipType[TooltipType["WhatsNew"] = 5] = "WhatsNew";
})(TooltipType = exports.TooltipType || (exports.TooltipType = {}));
var TooltipTypeUtils;
(function (TooltipTypeUtils) {
    function toInvokeSource(tooltipType) {
        switch (tooltipType) {
            case TooltipType.Pdf:
                return invokeSource_1.InvokeSource.PdfTooltip;
            case TooltipType.Product:
                return invokeSource_1.InvokeSource.ProductTooltip;
            case TooltipType.Recipe:
                return invokeSource_1.InvokeSource.RecipeTooltip;
            case TooltipType.Video:
                return invokeSource_1.InvokeSource.VideoTooltip;
            case TooltipType.WhatsNew:
                return invokeSource_1.InvokeSource.WhatsNewTooltip;
            default:
                throw Error("Invalid TooltipType passed in TooltipType.toInvokeSource");
        }
    }
    TooltipTypeUtils.toInvokeSource = toInvokeSource;
})(TooltipTypeUtils = exports.TooltipTypeUtils || (exports.TooltipTypeUtils = {}));
