"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../constants");
var polyfills_1 = require("../polyfills");
var communicator_1 = require("../communicator/communicator");
var iframeMessageHandler_1 = require("../communicator/iframeMessageHandler");
var localization_1 = require("../localization/localization");
var Log = require("../logging/log");
var communicatorLoggerPure_1 = require("../logging/communicatorLoggerPure");
var frontEndGlobals_1 = require("./frontEndGlobals");
var componentBase_1 = require("./componentBase");
var tooltipRenderer_1 = require("./tooltipRenderer");
var tooltipType_1 = require("./tooltipType");
/**
 * Root component of the Page Nav experience. Has the responsibility of initialization, and does not
 * render any UI itself.
 */
var PageNavClass = (function (_super) {
    __extends(PageNavClass, _super);
    function PageNavClass(props) {
        var _this = _super.call(this, props) || this;
        _this.initializeCommunicators();
        _this.setFrontLoadedLocalizedStrings();
        _this.getAndStoreDataFromExtension();
        return _this;
    }
    PageNavClass.prototype.getInitialState = function () {
        return {};
    };
    PageNavClass.prototype.onClosePageNavButton = function () {
        var closeEvent = new Log.Event.BaseEvent(Log.Event.Label.ClosePageNavTooltip);
        closeEvent.setCustomProperty(Log.PropertyName.Custom.PageNavTooltipType, this.state.tooltipToRender ? tooltipType_1.TooltipType[this.state.tooltipToRender] : "unknown");
        frontEndGlobals_1.Clipper.logger.logEvent(closeEvent);
    };
    PageNavClass.prototype.closePageNav = function () {
        frontEndGlobals_1.Clipper.getInjectCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.closePageNavTooltip);
    };
    PageNavClass.prototype.getAndStoreDataFromExtension = function () {
        var _this = this;
        frontEndGlobals_1.Clipper.getExtensionCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.getTooltipToRenderInPageNav, {
            callback: function (tooltipType) {
                _this.setState({ tooltipToRender: tooltipType });
            }
        });
        frontEndGlobals_1.Clipper.getExtensionCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.getPageNavTooltipProps, {
            callback: function (tooltipProps) {
                _this.setState({ tooltipProps: tooltipProps });
            }
        });
    };
    PageNavClass.prototype.initializeCommunicators = function () {
        frontEndGlobals_1.Clipper.setInjectCommunicator(new communicator_1.Communicator(new iframeMessageHandler_1.IFrameMessageHandler(function () { return parent; }), constants_1.Constants.CommunicationChannels.pageNavInjectedAndPageNavUi));
        frontEndGlobals_1.Clipper.setExtensionCommunicator(new communicator_1.Communicator(new iframeMessageHandler_1.IFrameMessageHandler(function () { return parent; }), constants_1.Constants.CommunicationChannels.extensionAndPageNavUi));
        frontEndGlobals_1.Clipper.logger = new communicatorLoggerPure_1.CommunicatorLoggerPure(frontEndGlobals_1.Clipper.getExtensionCommunicator());
    };
    PageNavClass.prototype.setFrontLoadedLocalizedStrings = function () {
        frontEndGlobals_1.Clipper.getExtensionCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.clipperStringsFrontLoaded, {
            callback: function (locStringsObj) {
                localization_1.Localization.setLocalizedStrings(locStringsObj);
            }
        });
    };
    PageNavClass.prototype.updateFrameHeight = function (newHeightInfo) {
        frontEndGlobals_1.Clipper.getInjectCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.updateFrameHeight, {
            param: newHeightInfo.newContainerHeight
        });
    };
    PageNavClass.prototype.render = function () {
        return (m.component(tooltipRenderer_1.TooltipRenderer, {onCloseButtonHandler:this.onClosePageNavButton.bind(this), onHeightChange:this.updateFrameHeight.bind(this), onTooltipClose:this.closePageNav.bind(this), tooltipToRender:this.state.tooltipToRender, tooltipProps:this.state.tooltipProps}));
    };
    return PageNavClass;
}(componentBase_1.ComponentBase));
polyfills_1.Polyfills.init();
// Catch any unhandled exceptions and log them
var oldOnError = window.onerror;
window.onerror = function (message, filename, lineno, colno, error) {
    var callStack = error ? Log.Failure.getStackTrace(error) : "[unknown stacktrace]";
    frontEndGlobals_1.Clipper.logger.logFailure(Log.Failure.Label.UnhandledExceptionThrown, Log.Failure.Type.Unexpected, { error: message + " (" + filename + ":" + lineno + ":" + colno + ") at " + callStack }, "PageNavUI");
    if (oldOnError) {
        oldOnError(message, filename, lineno, colno, error);
    }
};
var component = PageNavClass.componentize();
exports.PageNav = component;
m.mount(document.getElementById("pageNavUIPlaceholder"), component);
