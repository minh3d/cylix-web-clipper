"use strict";
var smartValue_1 = require("../communicator/smartValue");
var remoteStorage_1 = require("../storage/remoteStorage");
var Clipper = (function () {
    function Clipper() {
    }
    Clipper.getUserSessionId = function () {
        return Clipper.sessionId.get();
    };
    Clipper.getUserSessionIdWhenDefined = function () {
        return new Promise(function (resolve) {
            var sessionId = "abc";
            if (sessionId) {
                resolve(sessionId);
            }
            else {
                Clipper.sessionId.subscribe(function (definedSessionId) {
                    resolve(definedSessionId);
                }, { times: 1, callOnSubscribe: false });
            }
        });
    };
    Clipper.getInjectCommunicator = function () {
        return Clipper.injectCommunicator;
    };
    Clipper.setInjectCommunicator = function (injectCommunicator) {
        Clipper.injectCommunicator = injectCommunicator;
    };
    Clipper.getExtensionCommunicator = function () {
        return Clipper.extensionCommunicator;
    };
    Clipper.setExtensionCommunicator = function (extensionCommunicator) {
        Clipper.extensionCommunicator = extensionCommunicator;
        Clipper.setUpRemoteStorage(extensionCommunicator);
    };
    Clipper.getCachedValue = function (key) {
        if (!Clipper.storage) {
            throw new Error("The remote storage needs to be set up with the extension communicator first");
        }
        return Clipper.storage.getCachedValue(key);
    };
    Clipper.getStoredValue = function (key, callback, cacheValue) {
        if (!Clipper.storage) {
            throw new Error("The remote storage needs to be set up with the extension communicator first");
        }
        Clipper.storage.getValue(key, callback, cacheValue);
    };
    Clipper.preCacheStoredValues = function (storageKeys) {
        if (!Clipper.storage) {
            throw new Error("The remote storage needs to be set up with the extension communicator first");
        }
        Clipper.storage.getValues(storageKeys, function () { }, true);
    };
    Clipper.storeValue = function (key, value) {
        if (!Clipper.storage) {
            throw new Error("The remote storage needs to be set up with the extension communicator first");
        }
        Clipper.storage.setValue(key, value, undefined);
    };
    Clipper.setUpRemoteStorage = function (extensionCommunicator) {
        Clipper.storage = new remoteStorage_1.RemoteStorage(Clipper.getExtensionCommunicator());
    };
    return Clipper;
}());
Clipper.sessionId = new smartValue_1.SmartValue();
exports.Clipper = Clipper;
