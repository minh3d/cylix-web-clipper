"use strict";
var constants_1 = require("../constants");
var localization_1 = require("../localization/localization");
var Log = require("../logging/log");
var frontEndGlobals_1 = require("./frontEndGlobals");
var OneNoteApiUtils;
(function (OneNoteApiUtils) {
    var Limits;
    (function (Limits) {
        Limits.imagesPerRequestLimit = 30;
    })(Limits = OneNoteApiUtils.Limits || (OneNoteApiUtils.Limits = {}));
    function logOneNoteApiRequestError(event, error) {
        if (!event || !error) {
            return;
        }
        event.setCustomProperty(Log.PropertyName.Custom.CorrelationId, error.responseHeaders[constants_1.Constants.HeaderValues.correlationId]);
        event.setStatus(Log.Status.Failed);
        event.setFailureInfo(error);
        var apiResponseCode = getApiResponseCode(error);
        if (!apiResponseCode) {
            return;
        }
        var responseCodeInfo = getResponseCodeInformation(apiResponseCode);
        if (!responseCodeInfo) {
            frontEndGlobals_1.Clipper.logger.logFailure(Log.Failure.Label.UnhandledApiCode, Log.Failure.Type.Unexpected, undefined, apiResponseCode);
        }
        if (OneNoteApiUtils.isExpected(apiResponseCode)) {
            event.setFailureType(Log.Failure.Type.Expected);
        }
        event.setCustomProperty(Log.PropertyName.Custom.IsRetryable, OneNoteApiUtils.isRetryable(apiResponseCode));
    }
    OneNoteApiUtils.logOneNoteApiRequestError = logOneNoteApiRequestError;
    function getApiResponseCode(error) {
        if (!error || !error.response) {
            return;
        }
        var responseAsJson;
        try {
            responseAsJson = JSON.parse(error.response);
        }
        catch (e) {
            frontEndGlobals_1.Clipper.logger.logJsonParseUnexpected(error.response);
        }
        var apiResponseCode;
        if (responseAsJson && responseAsJson.error && responseAsJson.error.code) {
            apiResponseCode = responseAsJson.error.code;
        }
        return apiResponseCode ? apiResponseCode : undefined;
    }
    OneNoteApiUtils.getApiResponseCode = getApiResponseCode;
    function getLocalizedErrorMessage(apiResponseCode) {
        var responseCodeInfo = getResponseCodeInformation(apiResponseCode);
        return responseCodeInfo ? responseCodeInfo.message : localization_1.Localization.getLocalizedString("WebClipper.Error.GenericError");
    }
    OneNoteApiUtils.getLocalizedErrorMessage = getLocalizedErrorMessage;
    /**
     * Retrieves an error message for the response returned from fetching notebooks as HTML.
     */
    function getLocalizedErrorMessageForGetNotebooks(apiResponseCode) {
        var fallback = localization_1.Localization.getLocalizedString("WebClipper.SectionPicker.NotebookLoadUnretryableFailureMessage");
        // Actionable codes have a message that have a hyperlink to documentation that users can use to solve their issue
        var actionableResponseCodes = ["10008", "10013"];
        var responseCodeIsActionable = actionableResponseCodes.indexOf(apiResponseCode) > -1;
        if (responseCodeIsActionable) {
            var actionableLink = document.createElement("A");
            actionableLink.href = "https://aka.ms/onapi-too-many-items-actionable";
            actionableLink.innerText = localization_1.Localization.getLocalizedString("WebClipper.SectionPicker.NotebookLoadUnretryableFailureLinkMessage");
            var actionableMessageAsHtml = localization_1.Localization.getLocalizedString("WebClipper.SectionPicker.NotebookLoadUnretryableFailureMessageWithExplanation") + "\n" + actionableLink.outerHTML;
            return actionableMessageAsHtml;
        }
        // See if there's a specific message we can show
        var responseCodeInfo = getResponseCodeInformation(apiResponseCode);
        if (responseCodeInfo && responseCodeInfo.message) {
            return responseCodeInfo.message;
        }
        // Fall back to a non-retryable message
        return fallback;
    }
    OneNoteApiUtils.getLocalizedErrorMessageForGetNotebooks = getLocalizedErrorMessageForGetNotebooks;
    function requiresSignout(apiResponseCode) {
        var responseCodeInfo = getResponseCodeInformation(apiResponseCode);
        return responseCodeInfo ? responseCodeInfo.requiresSignout : false;
    }
    OneNoteApiUtils.requiresSignout = requiresSignout;
    function isExpected(apiResponseCode) {
        var responseCodeInfo = getResponseCodeInformation(apiResponseCode);
        return responseCodeInfo ? responseCodeInfo.isExpected : false;
    }
    OneNoteApiUtils.isExpected = isExpected;
    function isRetryable(apiResponseCode) {
        var responseCodeInfo = getResponseCodeInformation(apiResponseCode);
        return responseCodeInfo ? responseCodeInfo.isRetryable : false;
    }
    OneNoteApiUtils.isRetryable = isRetryable;
    /**
     * Retrieves response code information given that the context is in POSTing a clip.
     */
    function getResponseCodeInformation(apiResponseCode) {
        var handledExtendedResponseCodes = {
            "10001": { message: localization_1.Localization.getLocalizedString("WebClipper.Error.GenericError"), isRetryable: true, isExpected: true },
            "10002": { message: localization_1.Localization.getLocalizedString("WebClipper.Error.GenericError"), isRetryable: true, isExpected: true },
            "10004": { message: localization_1.Localization.getLocalizedString("WebClipper.Error.PasswordProtected"), isRetryable: false, isExpected: true },
            "10006": { message: localization_1.Localization.getLocalizedString("WebClipper.Error.CorruptedSection"), isRetryable: false, isExpected: true },
            "10007": { message: localization_1.Localization.getLocalizedString("WebClipper.Error.GenericError"), isRetryable: true, isExpected: true },
            "19999": { message: localization_1.Localization.getLocalizedString("WebClipper.Error.GenericError"), isRetryable: true, isExpected: false },
            "20102": { message: localization_1.Localization.getLocalizedString("WebClipper.Error.ResourceDoesNotExist"), isRetryable: false, isExpected: true },
            "30101": { message: localization_1.Localization.getLocalizedString("WebClipper.Error.QuotaExceeded"), isRetryable: false, isExpected: true },
            "30102": { message: localization_1.Localization.getLocalizedString("WebClipper.Error.SectionTooLarge"), isRetryable: false, isExpected: true },
            "30103": { message: localization_1.Localization.getLocalizedString("WebClipper.Error.GenericError"), isRetryable: true, isExpected: true },
            "30104": { message: localization_1.Localization.getLocalizedString("WebClipper.Error.UserAccountSuspended"), isRetryable: false, isExpected: true },
            "30105": { message: localization_1.Localization.getLocalizedString("WebClipper.Error.NotProvisioned"), isRetryable: false, isExpected: true },
            "40004": { message: localization_1.Localization.getLocalizedString("WebClipper.Error.UserDoesNotHaveUpdatePermission"), isRetryable: false, isExpected: true, requiresSignout: true } // UserOnlyHasCreatePermissions
        };
        if (!apiResponseCode) {
            return;
        }
        return handledExtendedResponseCodes[apiResponseCode];
    }
    function createPatchRequestBody(dataUrls) {
        var requestBody = [];
        dataUrls.forEach(function (dataUrl) {
            var content = "<p><img src=\"" + dataUrl + "\" /></p>&nbsp;";
            requestBody.push({
                target: "body",
                action: "append",
                content: content
            });
        });
        return requestBody;
    }
    OneNoteApiUtils.createPatchRequestBody = createPatchRequestBody;
})(OneNoteApiUtils = exports.OneNoteApiUtils || (exports.OneNoteApiUtils = {}));
