"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var objectUtils_1 = require("../../objectUtils");
var smartValue_1 = require("../../communicator/smartValue");
var animationHelper_1 = require("./animationHelper");
var animationState_1 = require("./animationState");
var animationStrategy_1 = require("./animationStrategy");
/**
 * Represents the family of animations where elements are able to toggle their visibility completely.
 *
 * Assumes that the decision to animate out vs animate in is relient on both external and internal
 * factors. Implementing classes will implement the internal factors, but can leave room for external
 * factors to weigh in on the decision as well.
 */
var TransitioningAnimationStrategy = (function (_super) {
    __extends(TransitioningAnimationStrategy, _super);
    function TransitioningAnimationStrategy(animationDuration, options, animationState) {
        var _this = this;
        animationState = animationState || new smartValue_1.SmartValue();
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(animationState.get())) {
            animationState.set(animationState_1.AnimationState.Out);
        }
        _this = _super.call(this, animationDuration, animationState) || this;
        _this.options = options;
        return _this;
    }
    // Override
    TransitioningAnimationStrategy.prototype.animate = function (el) {
        // We only stop animations when we actually animate, so we call stopAnimationsThen
        // in the animateIn and animateOut functions instead of here
        this.doAnimate(el);
    };
    TransitioningAnimationStrategy.prototype.doAnimate = function (el) {
        if (this.options.extShouldAnimateIn() && this.intShouldAnimateIn(el)) {
            return this.animateIn(el);
        }
        else if (this.options.extShouldAnimateOut() && this.intShouldAnimateOut(el)) {
            return this.animateOut(el);
        }
    };
    TransitioningAnimationStrategy.prototype.animateIn = function (el) {
        var _this = this;
        return new Promise(function (resolve) {
            animationHelper_1.AnimationHelper.stopAnimationsThen(el, function () {
                if (_this.options.onBeforeAnimateIn) {
                    _this.options.onBeforeAnimateIn(el);
                }
                _this.setAnimationState(animationState_1.AnimationState.GoingIn);
                _this.doAnimateIn(el).then(function () {
                    _this.setAnimationState(animationState_1.AnimationState.In);
                    if (_this.options.onAfterAnimateIn) {
                        _this.options.onAfterAnimateIn(el);
                    }
                    resolve();
                });
            });
        });
    };
    TransitioningAnimationStrategy.prototype.animateOut = function (el) {
        var _this = this;
        return new Promise(function (resolve) {
            animationHelper_1.AnimationHelper.stopAnimationsThen(el, function () {
                if (_this.options.onBeforeAnimateOut) {
                    _this.options.onBeforeAnimateOut(el);
                }
                _this.setAnimationState(animationState_1.AnimationState.GoingOut);
                _this.doAnimateOut(el).then(function () {
                    _this.setAnimationState(animationState_1.AnimationState.Out);
                    if (_this.options.onAfterAnimateOut) {
                        _this.options.onAfterAnimateOut(el);
                    }
                    resolve();
                });
            });
        });
    };
    return TransitioningAnimationStrategy;
}(animationStrategy_1.AnimationStrategy));
exports.TransitioningAnimationStrategy = TransitioningAnimationStrategy;
