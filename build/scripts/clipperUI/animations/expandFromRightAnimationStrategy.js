"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../../constants");
var animationState_1 = require("./animationState");
var transitioningAnimationStrategy_1 = require("./transitioningAnimationStrategy");
/**
 * Represents an animation where elements transition in by expanding from the top right.
 * When transitioning out, the opposite happens.
 */
var ExpandFromRightAnimationStrategy = (function (_super) {
    __extends(ExpandFromRightAnimationStrategy, _super);
    function ExpandFromRightAnimationStrategy(options) {
        var _this = _super.call(this, 500 /* animationDuration */, options) || this;
        _this.animationTimeout = 100;
        _this.reverseChildAnimations = true;
        return _this;
    }
    ExpandFromRightAnimationStrategy.prototype.doAnimateIn = function (el) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.reverseChildAnimations = true;
            clearTimeout(_this.animationTimeoutId);
            if (_this.options.onAnimateInExpand) {
                _this.animationTimeoutId = setTimeout(function () {
                    _this.options.onAnimateInExpand(el);
                }, _this.animationTimeout);
            }
            Velocity.animate(el, {
                opacity: 1,
                right: 20,
                width: constants_1.Constants.Styles.clipperUiWidth
            }, {
                complete: function () {
                    // The first transition is reversed; once it is done, do the normal transitions
                    _this.reverseChildAnimations = false;
                    resolve();
                },
                duration: _this.animationDuration,
                easing: "easeOutExpo"
            });
        });
    };
    ExpandFromRightAnimationStrategy.prototype.doAnimateOut = function (el) {
        var _this = this;
        return new Promise(function (resolve) {
            clearTimeout(_this.animationTimeoutId);
            _this.animationTimeoutId = setTimeout(function () {
                Velocity.animate(el, {
                    opacity: 0,
                    right: 0,
                    width: 0
                }, {
                    complete: function () {
                        resolve();
                    },
                    duration: _this.animationDuration,
                    easing: "easeOutExpo"
                });
            }, _this.animationTimeout);
        });
    };
    ExpandFromRightAnimationStrategy.prototype.intShouldAnimateIn = function (el) {
        return this.getAnimationState() === animationState_1.AnimationState.GoingOut || this.getAnimationState() === animationState_1.AnimationState.Out;
    };
    ExpandFromRightAnimationStrategy.prototype.intShouldAnimateOut = function (el) {
        return this.getAnimationState() === animationState_1.AnimationState.GoingIn || this.getAnimationState() === animationState_1.AnimationState.In;
    };
    return ExpandFromRightAnimationStrategy;
}(transitioningAnimationStrategy_1.TransitioningAnimationStrategy));
exports.ExpandFromRightAnimationStrategy = ExpandFromRightAnimationStrategy;
