"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var animationStrategy_1 = require("./animationStrategy");
/**
 * Represents an animation where element are able to adjust their height by performing
 * a 'slide' animation.
 */
var SlidingHeightAnimationStrategy = (function (_super) {
    __extends(SlidingHeightAnimationStrategy, _super);
    function SlidingHeightAnimationStrategy(containerId, options) {
        var _this = _super.call(this, 200 /* animationDuration */) || this;
        _this.containerId = containerId;
        _this.options = options;
        return _this;
    }
    SlidingHeightAnimationStrategy.prototype.doAnimate = function (el) {
        var _this = this;
        return new Promise(function (resolve) {
            var container = document.getElementById(_this.containerId);
            var newHeightInfo = _this.getContainerTrueHeight(container, el);
            if (_this.options.onBeforeHeightAnimatorDraw) {
                _this.options.onBeforeHeightAnimatorDraw(newHeightInfo);
            }
            // If there's nothing to animate then call it good.
            if (newHeightInfo.actualPanelHeight === newHeightInfo.newPanelHeight) {
                resolve();
                return;
            }
            var delayResize = newHeightInfo.actualPanelHeight > newHeightInfo.newPanelHeight;
            if (!delayResize && _this.options.onAfterHeightAnimatorDraw) {
                _this.options.onAfterHeightAnimatorDraw(newHeightInfo);
            }
            Velocity.animate(el, {
                maxHeight: newHeightInfo.newPanelHeight,
                minHeight: newHeightInfo.newPanelHeight
            }, {
                complete: function () {
                    if (delayResize && _this.options.onAfterHeightAnimatorDraw) {
                        _this.options.onAfterHeightAnimatorDraw(newHeightInfo);
                    }
                    resolve();
                },
                duration: _this.animationDuration,
                easing: "easeOutQuad"
            });
        });
    };
    SlidingHeightAnimationStrategy.prototype.getContainerTrueHeight = function (container, heightAnimator) {
        var actualPanelHeight = parseFloat(heightAnimator.style.maxHeight.replace("px", ""));
        if (isNaN(actualPanelHeight)) {
            actualPanelHeight = 0;
        }
        // Temporarily remove these so we can calculate the destination heights.
        heightAnimator.style.maxHeight = "";
        heightAnimator.style.minHeight = "";
        // At this point the new container size has been set, so we need to grab it so that we know where to animate to.
        var newContainerHeight = container ? container.offsetHeight : 0;
        var newPanelHeight = heightAnimator.offsetHeight;
        // Now set to the height back to what it was so that there is something to animate from.
        heightAnimator.style.maxHeight = actualPanelHeight + "px";
        heightAnimator.style.minHeight = actualPanelHeight + "px";
        return { actualPanelHeight: actualPanelHeight, newContainerHeight: newContainerHeight, newPanelHeight: newPanelHeight };
    };
    return SlidingHeightAnimationStrategy;
}(animationStrategy_1.AnimationStrategy));
exports.SlidingHeightAnimationStrategy = SlidingHeightAnimationStrategy;
