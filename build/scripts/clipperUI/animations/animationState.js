"use strict";
var AnimationState;
(function (AnimationState) {
    // Used for when elements transition in and out (i.e., the next element is replacing the first)
    AnimationState[AnimationState["GoingIn"] = 0] = "GoingIn";
    AnimationState[AnimationState["GoingOut"] = 1] = "GoingOut";
    AnimationState[AnimationState["In"] = 2] = "In";
    AnimationState[AnimationState["Out"] = 3] = "Out";
    // Used for when the same element is transitioning from one state to the next (e.g., changing dimensions)
    AnimationState[AnimationState["Transitioning"] = 4] = "Transitioning";
    AnimationState[AnimationState["Stopped"] = 5] = "Stopped";
})(AnimationState = exports.AnimationState || (exports.AnimationState = {}));
