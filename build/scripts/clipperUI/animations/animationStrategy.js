"use strict";
var smartValue_1 = require("../../communicator/smartValue");
var animationHelper_1 = require("./animationHelper");
var animationState_1 = require("./animationState");
/**
 * Represents a strategy object for handling the animations for the given element.
 * Child classes should only have to implement the animation itself, as well as any
 * additional functional requirements, such as callbacks.
 */
var AnimationStrategy = (function () {
    function AnimationStrategy(animationDuration, animationState) {
        this.animationDuration = animationDuration;
        this.animationState = animationState || new smartValue_1.SmartValue(animationState_1.AnimationState.Stopped);
    }
    AnimationStrategy.prototype.getAnimationState = function () {
        return this.animationState.get();
    };
    AnimationStrategy.prototype.setAnimationState = function (animationState) {
        this.animationState.set(animationState);
    };
    AnimationStrategy.prototype.animate = function (el) {
        var _this = this;
        animationHelper_1.AnimationHelper.stopAnimationsThen(el, function () {
            _this.setAnimationState(animationState_1.AnimationState.Transitioning);
            _this.doAnimate(el).then(function () {
                _this.setAnimationState(animationState_1.AnimationState.Stopped);
            });
        });
    };
    return AnimationStrategy;
}());
exports.AnimationStrategy = AnimationStrategy;
