"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var animationState_1 = require("./animationState");
var transitioningAnimationStrategy_1 = require("./transitioningAnimationStrategy");
/**
 * Represents an animation where content fades in and slides downward into its position within the parent element provided.
 * When animating out, content will fade out and slide upwards.
 */
var SlideContentInFromTopAnimationStrategy = (function (_super) {
    __extends(SlideContentInFromTopAnimationStrategy, _super);
    function SlideContentInFromTopAnimationStrategy(options) {
        var _this = _super.call(this, undefined /* animationDuration */, options, options.currentAnimationState) || this;
        _this.animateInDuration = 367;
        _this.animateOutDuration = 267;
        _this.animateOutSlideUpDelta = 23;
        _this.contentToAnimate = options.contentToAnimate;
        return _this;
    }
    SlideContentInFromTopAnimationStrategy.prototype.doAnimateIn = function (parentEl) {
        var _this = this;
        return new Promise(function (resolve) {
            for (var contentIndex = 0; contentIndex < _this.contentToAnimate.length; contentIndex++) {
                var content = _this.contentToAnimate[contentIndex];
                var animatables = parentEl.querySelectorAll(content.cssSelector);
                for (var animatableIndex = 0; animatableIndex < animatables.length; animatableIndex++) {
                    var isLastElementToAnimate = (contentIndex === _this.contentToAnimate.length - 1) && (animatableIndex === animatables.length - 1);
                    _this.animateElementIn(animatables[animatableIndex], content.animateInOptions.slideDownDeltas[animatableIndex], content.animateInOptions.delaysInMs[animatableIndex], isLastElementToAnimate)
                        .then(function (lastElementFinishedAnimating) {
                        if (lastElementFinishedAnimating) {
                            resolve();
                        }
                    });
                }
            }
        });
    };
    SlideContentInFromTopAnimationStrategy.prototype.doAnimateOut = function (parentEl) {
        var _this = this;
        return new Promise(function (resolve) {
            var childrenSelectors = _this.contentToAnimate.map(function (content) { return content.cssSelector; }).join(", ");
            var animatables = parentEl.querySelectorAll(childrenSelectors);
            Velocity.animate(animatables, {
                top: -(_this.animateOutSlideUpDelta),
                opacity: [0, "linear"]
            }, {
                complete: function () {
                    resolve();
                },
                duration: _this.animateOutDuration,
                easing: [1, 0, 1, 1]
            });
        });
    };
    SlideContentInFromTopAnimationStrategy.prototype.intShouldAnimateIn = function (el) {
        return this.getAnimationState() === animationState_1.AnimationState.Out;
    };
    SlideContentInFromTopAnimationStrategy.prototype.intShouldAnimateOut = function (el) {
        return this.getAnimationState() === animationState_1.AnimationState.In;
    };
    /**
     * Apply animate-in styling to a single element of content
     */
    SlideContentInFromTopAnimationStrategy.prototype.animateElementIn = function (elem, slideDownDelta, delayInMs, isLastElementToAnimate) {
        var _this = this;
        return new Promise(function (resolve) {
            elem.style.top = -(slideDownDelta) + "px";
            elem.style.opacity = "0";
            Velocity.animate(elem, {
                top: 0,
                opacity: [1, "linear"]
            }, {
                complete: function () {
                    if (isLastElementToAnimate) {
                        resolve(true);
                    }
                    else {
                        resolve(false);
                    }
                },
                delay: delayInMs,
                duration: _this.animateInDuration,
                easing: [0, 0, 0, 1]
            });
        });
    };
    return SlideContentInFromTopAnimationStrategy;
}(transitioningAnimationStrategy_1.TransitioningAnimationStrategy));
exports.SlideContentInFromTopAnimationStrategy = SlideContentInFromTopAnimationStrategy;
