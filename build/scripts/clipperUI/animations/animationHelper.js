"use strict";
var AnimationHelper = (function () {
    function AnimationHelper() {
    }
    AnimationHelper.stopAnimationsThen = function (el, callback) {
        Velocity.animate(el, "stop", true);
        setTimeout(callback, 1);
    };
    return AnimationHelper;
}());
exports.AnimationHelper = AnimationHelper;
