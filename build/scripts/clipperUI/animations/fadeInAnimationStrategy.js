"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var animationState_1 = require("./animationState");
var transitioningAnimationStrategy_1 = require("./transitioningAnimationStrategy");
/**
 * Represents an animation where elements fade in.
 * When transitioning out, elements fade away.
 */
var FadeInAnimationStrategy = (function (_super) {
    __extends(FadeInAnimationStrategy, _super);
    function FadeInAnimationStrategy(options) {
        return _super.call(this, 200 /* animationDuration */, options) || this;
    }
    FadeInAnimationStrategy.prototype.doAnimateIn = function (el) {
        var _this = this;
        return new Promise(function (resolve) {
            el.style.opacity = "0";
            Velocity.animate(el, {
                opacity: 1
            }, {
                complete: function () {
                    resolve();
                },
                duration: _this.animationDuration,
                easing: "easeInOutQuad"
            });
        });
    };
    FadeInAnimationStrategy.prototype.doAnimateOut = function (el) {
        var _this = this;
        return new Promise(function (resolve) {
            Velocity.animate(el, {
                opacity: 0
            }, {
                complete: function () {
                    resolve();
                },
                duration: _this.animationDuration,
                easing: "easeInOutQuad"
            });
        });
    };
    FadeInAnimationStrategy.prototype.intShouldAnimateIn = function (el) {
        return this.getAnimationState() === animationState_1.AnimationState.Out;
    };
    FadeInAnimationStrategy.prototype.intShouldAnimateOut = function (el) {
        return this.getAnimationState() === animationState_1.AnimationState.In;
    };
    return FadeInAnimationStrategy;
}(transitioningAnimationStrategy_1.TransitioningAnimationStrategy));
exports.FadeInAnimationStrategy = FadeInAnimationStrategy;
