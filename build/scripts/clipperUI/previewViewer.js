"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var clientType_1 = require("../clientType");
var localization_1 = require("../localization/localization");
var clipMode_1 = require("./clipMode");
var componentBase_1 = require("./componentBase");
var augmentationPreview_1 = require("./components/previewViewer/augmentationPreview");
var bookmarkPreview_1 = require("./components/previewViewer/bookmarkPreview");
var fullPagePreview_1 = require("./components/previewViewer/fullPagePreview");
var pdfPreview_1 = require("./components/previewViewer/pdfPreview");
var regionPreview_1 = require("./components/previewViewer/regionPreview");
var selectionPreview_1 = require("./components/previewViewer/selectionPreview");
var localFilesNotAllowedPanel_1 = require("./components/previewViewer/localFilesNotAllowedPanel");
var PreviewViewerClass = (function (_super) {
    __extends(PreviewViewerClass, _super);
    function PreviewViewerClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PreviewViewerClass.prototype.render = function () {
        var state = this.props.clipperState;
        switch (state.currentMode.get()) {
            case clipMode_1.ClipMode.Pdf:
                if (!state.pdfPreviewInfo.isLocalFileAndNotAllowed) {
                    if (state.clientInfo.clipperType === clientType_1.ClientType.ChromeExtension) {
                        return m.component(localFilesNotAllowedPanel_1.LocalFilesNotAllowedPanel, {title:localization_1.Localization.getLocalizedString("WebClipper.ClipType.Pdf.AskPermissionToClipLocalFile"), subtitle:localization_1.Localization.getLocalizedString("WebClipper.ClipType.Pdf.InstructionsForClippingLocalFiles"), header:localization_1.Localization.getLocalizedString("WebClipper.ClipType.Pdf.Button")});
                    }
                    return m.component(localFilesNotAllowedPanel_1.LocalFilesNotAllowedPanel, {title:localization_1.Localization.getLocalizedString("WebClipper.Preview.UnableToClipLocalFile"), subtitle:"", header:localization_1.Localization.getLocalizedString("WebClipper.ClipType.Pdf.Button")});
                }
                return m.component(pdfPreview_1.PdfPreview, {clipperState:state});
            case clipMode_1.ClipMode.FullPage:
                return m.component(fullPagePreview_1.FullPagePreview, {clipperState:state});
            case clipMode_1.ClipMode.Region:
                return m.component(regionPreview_1.RegionPreview, {clipperState:state});
            case clipMode_1.ClipMode.Augmentation:
                return m.component(augmentationPreview_1.AugmentationPreview, {clipperState:state});
            case clipMode_1.ClipMode.Bookmark:
                return m.component(bookmarkPreview_1.BookmarkPreview, {clipperState:state});
            case clipMode_1.ClipMode.Selection:
                return m.component(selectionPreview_1.SelectionPreview, {clipperState:state});
            default:
                return {tag: "div", attrs: {}};
        }
    };
    return PreviewViewerClass;
}(componentBase_1.ComponentBase));
var component = PreviewViewerClass.componentize();
exports.PreviewViewer = component;
