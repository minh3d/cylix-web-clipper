"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var smartValue_1 = require("../communicator/smartValue");
var constants_1 = require("../constants");
var localization_1 = require("../localization/localization");
var Log = require("../logging/log");
var animationHelper_1 = require("./animations/animationHelper");
var animationState_1 = require("./animations/animationState");
var expandFromRightAnimationStrategy_1 = require("./animations/expandFromRightAnimationStrategy");
var fadeInAnimationStrategy_1 = require("./animations/fadeInAnimationStrategy");
var slidingHeightAnimationStrategy_1 = require("./animations/slidingHeightAnimationStrategy");
var clipMode_1 = require("./clipMode");
var clipperStateUtilities_1 = require("./clipperStateUtilities");
var componentBase_1 = require("./componentBase");
var closeButton_1 = require("./components/closeButton");
var footer_1 = require("./components/footer");
var frontEndGlobals_1 = require("./frontEndGlobals");
var oneNoteApiUtils_1 = require("./oneNoteApiUtils");
var clippingPanel_1 = require("./panels/clippingPanel");
var clippingPanelWithDelayedMessage_1 = require("./panels/clippingPanelWithDelayedMessage");
var clippingPanelWithProgressIndicator_1 = require("./panels/clippingPanelWithProgressIndicator");
var errorDialogPanel_1 = require("./panels/errorDialogPanel");
var loadingPanel_1 = require("./panels/loadingPanel");
var optionsPanel_1 = require("./panels/optionsPanel");
var ratingsPanel_1 = require("./panels/ratingsPanel");
var regionSelectingPanel_1 = require("./panels/regionSelectingPanel");
var signInPanel_1 = require("./panels/signInPanel");
var successPanel_1 = require("./panels/successPanel");
var status_1 = require("./status");
var CloseReason;
(function (CloseReason) {
    CloseReason[CloseReason["CloseButton"] = 0] = "CloseButton";
    CloseReason[CloseReason["EscPress"] = 1] = "EscPress";
})(CloseReason = exports.CloseReason || (exports.CloseReason = {}));
var PanelType;
(function (PanelType) {
    PanelType[PanelType["None"] = 0] = "None";
    PanelType[PanelType["BadState"] = 1] = "BadState";
    PanelType[PanelType["Loading"] = 2] = "Loading";
    PanelType[PanelType["SignInNeeded"] = 3] = "SignInNeeded";
    PanelType[PanelType["ClipOptions"] = 4] = "ClipOptions";
    PanelType[PanelType["RegionInstructions"] = 5] = "RegionInstructions";
    PanelType[PanelType["ClippingToApi"] = 6] = "ClippingToApi";
    PanelType[PanelType["ClippingFailure"] = 7] = "ClippingFailure";
    PanelType[PanelType["ClippingSuccess"] = 8] = "ClippingSuccess";
})(PanelType = exports.PanelType || (exports.PanelType = {}));
var MainControllerClass = (function (_super) {
    __extends(MainControllerClass, _super);
    function MainControllerClass(props) {
        var _this = _super.call(this, props) || this;
        _this.initAnimationStrategy();
        // The user could have pressed esc when Clipper iframe was not in focus
        frontEndGlobals_1.Clipper.getInjectCommunicator().registerFunction(constants_1.Constants.FunctionKeys.escHandler, function () {
            _this.handleEscPress();
        });
        document.onkeydown = function (event) {
            if (event.keyCode === constants_1.Constants.KeyCodes.esc) {
                _this.handleEscPress();
            }
        };
        return _this;
    }
    MainControllerClass.prototype.getInitialState = function () {
        return {
            currentPanel: PanelType.None
        };
    };
    MainControllerClass.prototype.handleEscPress = function () {
        if (this.isCloseable()) {
            this.closeClipper(CloseReason.EscPress);
        }
    };
    MainControllerClass.prototype.initAnimationStrategy = function () {
        var _this = this;
        this.controllerAnimationStrategy = new expandFromRightAnimationStrategy_1.ExpandFromRightAnimationStrategy({
            extShouldAnimateIn: function () { return _this.props.clipperState.uiExpanded; },
            extShouldAnimateOut: function () { return !_this.props.clipperState.uiExpanded; },
            onBeforeAnimateOut: function () { _this.setState({ currentPanel: PanelType.None }); },
            onBeforeAnimateIn: function () { _this.props.clipperState.reset(); },
            onAnimateInExpand: function () { _this.setState({ currentPanel: _this.getPanelTypeToShow() }); },
            onAfterAnimateOut: function () { frontEndGlobals_1.Clipper.getInjectCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.hideUi); }
        });
        this.panelAnimationStrategy = new fadeInAnimationStrategy_1.FadeInAnimationStrategy({
            extShouldAnimateIn: function () { return _this.state.currentPanel !== PanelType.None; },
            extShouldAnimateOut: function () { return _this.getPanelTypeToShow() !== _this.state.currentPanel; },
            onAfterAnimateOut: function () { _this.setState({ currentPanel: _this.getPanelTypeToShow() }); },
            onAfterAnimateIn: function () { _this.setState({ currentPanel: _this.getPanelTypeToShow() }); }
        });
        this.heightAnimationStrategy = new slidingHeightAnimationStrategy_1.SlidingHeightAnimationStrategy(constants_1.Constants.Ids.mainController, {
            onAfterHeightAnimatorDraw: function (newHeightInfo) {
                if (_this.popupIsOpen) {
                    // Sometimes during the delay, we open the popup, so the frame height update needs to account for that too
                    _this.updateFrameHeightAfterPopupToggle(_this.popupIsOpen);
                }
                else {
                    _this.props.updateFrameHeight(newHeightInfo.newContainerHeight);
                }
            }
        });
    };
    MainControllerClass.prototype.isCloseable = function () {
        var panelType = this.state.currentPanel;
        return panelType !== PanelType.None && panelType !== PanelType.ClippingToApi;
    };
    MainControllerClass.prototype.onPopupToggle = function (shouldNowBeOpen) {
        this.popupIsOpen = shouldNowBeOpen;
        this.updateFrameHeightAfterPopupToggle(shouldNowBeOpen);
    };
    MainControllerClass.prototype.updateFrameHeightAfterPopupToggle = function (shouldNowBeOpen) {
        var saveToLocationContainer = document.getElementById(constants_1.Constants.Ids.saveToLocationContainer);
        if (saveToLocationContainer) {
            var currentLocationContainerPosition = saveToLocationContainer.getBoundingClientRect();
            var aboutToOpenHeight = constants_1.Constants.Styles.sectionPickerContainerHeight + currentLocationContainerPosition.top + currentLocationContainerPosition.height;
            var aboutToCloseHeight = document.getElementById(constants_1.Constants.Ids.mainController).offsetHeight;
            var newHeight = shouldNowBeOpen ? aboutToOpenHeight : aboutToCloseHeight;
            this.props.updateFrameHeight(newHeight);
        }
    };
    MainControllerClass.prototype.getPanelTypeToShow = function () {
        if (this.props.clipperState.badState) {
            return PanelType.BadState;
        }
        if (!this.props.clipperState.uiExpanded) {
            return PanelType.None;
        }
        if ((this.props.clipperState.userResult && this.props.clipperState.userResult.status === status_1.Status.InProgress) ||
            this.props.clipperState.fetchLocStringStatus === status_1.Status.InProgress || !this.props.clipperState.invokeOptions) {
            return PanelType.Loading;
        }
        if (!clipperStateUtilities_1.ClipperStateUtilities.isUserLoggedIn(this.props.clipperState)) {
            return PanelType.SignInNeeded;
        }
        if (this.props.clipperState.currentMode.get() === clipMode_1.ClipMode.Region && this.props.clipperState.regionResult.status !== status_1.Status.Succeeded) {
            switch (this.props.clipperState.regionResult.status) {
                case status_1.Status.InProgress:
                    return PanelType.Loading;
                default:
                    return PanelType.RegionInstructions;
            }
        }
        switch (this.props.clipperState.oneNoteApiResult.status) {
            default:
            case status_1.Status.NotStarted:
                return PanelType.ClipOptions;
            case status_1.Status.InProgress:
                return PanelType.ClippingToApi;
            case status_1.Status.Failed:
                return PanelType.ClippingFailure;
            case status_1.Status.Succeeded:
                return PanelType.ClippingSuccess;
        }
    };
    MainControllerClass.prototype.closeClipper = function (closeReason) {
        var closeEvent = new Log.Event.BaseEvent(Log.Event.Label.CloseClipper);
        closeEvent.setCustomProperty(Log.PropertyName.Custom.CurrentPanel, PanelType[this.state.currentPanel]);
        closeEvent.setCustomProperty(Log.PropertyName.Custom.CloseReason, CloseReason[closeReason]);
        frontEndGlobals_1.Clipper.logger.logEvent(closeEvent);
        // Clear region selections on clipper exit rather than invoke to avoid conflicting logic with scenarios like context menu image selection
        this.props.clipperState.setState({
            uiExpanded: false,
            regionResult: { status: status_1.Status.NotStarted, data: [] }
        });
    };
    MainControllerClass.prototype.onMainControllerDraw = function (mainControllerElement) {
        this.controllerAnimationStrategy.animate(mainControllerElement);
    };
    MainControllerClass.prototype.onPanelAnimatorDraw = function (panelAnimator) {
        var _this = this;
        var panelTypeToShow = this.getPanelTypeToShow();
        var panelIsCorrect = panelTypeToShow === this.state.currentPanel;
        if (panelTypeToShow === PanelType.ClipOptions && this.state.currentPanel !== PanelType.ClipOptions) {
            this.popupIsOpen = false;
        }
        this.panelAnimationStrategy.animate(panelAnimator);
        if (!panelIsCorrect && this.panelAnimationStrategy.getAnimationState() === animationState_1.AnimationState.GoingIn) {
            // We'll speed things up by stopping the current one, and trigger the next one
            animationHelper_1.AnimationHelper.stopAnimationsThen(panelAnimator, function () {
                _this.panelAnimationStrategy.setAnimationState(animationState_1.AnimationState.In);
                _this.setState({});
            });
        }
    };
    MainControllerClass.prototype.onHeightAnimatorDraw = function (heightAnimator) {
        this.heightAnimationStrategy.animate(heightAnimator);
    };
    MainControllerClass.prototype.getCurrentPanel = function () {
        var _this = this;
        var panelType = this.state.currentPanel;
        var buttons = [];
        switch (panelType) {
            case PanelType.BadState:
                buttons.push({
                    id: constants_1.Constants.Ids.refreshPageButton,
                    label: localization_1.Localization.getLocalizedString("WebClipper.Action.RefreshPage"),
                    handler: function () {
                        frontEndGlobals_1.Clipper.getInjectCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.refreshPage);
                    }
                });
                return <errorDialogPanel_1.ErrorDialogPanel message={localization_1.Localization.getLocalizedString("WebClipper.Error.OrphanedWebClipperDetected")} buttons={buttons}/>;
            case PanelType.Loading:
                return <loadingPanel_1.LoadingPanel clipperState={this.props.clipperState}/>;
            case PanelType.SignInNeeded:
                return <signInPanel_1.SignInPanel clipperState={this.props.clipperState} onSignInInvoked={this.props.onSignInInvoked}/>;
            case PanelType.ClipOptions:
                return <optionsPanel_1.OptionsPanel onPopupToggle={this.onPopupToggle.bind(this)} clipperState={this.props.clipperState} onStartClip={this.props.onStartClip}/>;
            case PanelType.RegionInstructions:
                return <regionSelectingPanel_1.RegionSelectingPanel clipperState={this.props.clipperState}/>;
            case PanelType.ClippingToApi:
                if (this.props.clipperState.currentMode.get() === clipMode_1.ClipMode.Pdf) {
                    if (this.props.clipperState.pdfPreviewInfo.shouldDistributePages) {
                        return <clippingPanelWithProgressIndicator_1.ClippingPanelWithProgressIndicator clipperState={this.props.clipperState}/>;
                    }
                    return <clippingPanelWithDelayedMessage_1.ClippingPanelWithDelayedMessage clipperState={this.props.clipperState} delay={constants_1.Constants.Settings.pdfClippingMessageDelay} message={localization_1.Localization.getLocalizedString("WebClipper.ClipType.Pdf.ProgressLabelDelay")}/>;
                }
                return <clippingPanel_1.ClippingPanel clipperState={this.props.clipperState}/>;
            case PanelType.ClippingFailure:
                var error = this.props.clipperState.oneNoteApiResult.data;
                var apiResponseCode = oneNoteApiUtils_1.OneNoteApiUtils.getApiResponseCode(error);
                if (oneNoteApiUtils_1.OneNoteApiUtils.isRetryable(apiResponseCode)) {
                    buttons.push({
                        id: constants_1.Constants.Ids.dialogTryAgainButton,
                        label: localization_1.Localization.getLocalizedString("WebClipper.Action.TryAgain"),
                        handler: this.props.onStartClip
                    });
                }
                if (oneNoteApiUtils_1.OneNoteApiUtils.requiresSignout(apiResponseCode)) {
                    buttons.push({
                        id: constants_1.Constants.Ids.dialogSignOutButton,
                        label: localization_1.Localization.getLocalizedString("WebClipper.Action.SignOut"),
                        handler: function () {
                            if (_this.props.onSignOutInvoked) {
                                _this.props.onSignOutInvoked(_this.props.clipperState.userResult.data.user.authType);
                            }
                        }
                    });
                }
                else {
                    buttons.push({
                        id: constants_1.Constants.Ids.dialogBackButton,
                        label: localization_1.Localization.getLocalizedString("WebClipper.Action.BackToHome"),
                        handler: function () {
                            _this.props.clipperState.setState({
                                oneNoteApiResult: {
                                    data: _this.props.clipperState.oneNoteApiResult.data,
                                    status: status_1.Status.NotStarted
                                }
                            });
                        }
                    });
                }
                return <errorDialogPanel_1.ErrorDialogPanel message={oneNoteApiUtils_1.OneNoteApiUtils.getLocalizedErrorMessage(apiResponseCode)} buttons={buttons}/>;
            case PanelType.ClippingSuccess:
                var panels = [<successPanel_1.SuccessPanel clipperState={this.props.clipperState}/>];
                if (!this.state.ratingsPanelAnimationState) {
                    this.state.ratingsPanelAnimationState = new smartValue_1.SmartValue(animationState_1.AnimationState.Out);
                }
                if (this.props.clipperState.showRatingsPrompt) {
                    panels.push(<ratingsPanel_1.RatingsPanel clipperState={this.props.clipperState} animationState={this.state.ratingsPanelAnimationState}/>);
                }
                return panels;
            default:
                return undefined;
        }
    };
    MainControllerClass.prototype.getCloseButtonForType = function () {
        if (this.isCloseable()) {
            return (<closeButton_1.CloseButton onClickHandler={this.closeClipper.bind(this)} onClickHandlerParams={[CloseReason.CloseButton]}/>);
        }
        else {
            return undefined;
        }
    };
    MainControllerClass.prototype.getCurrentFooter = function () {
        var panelType = this.state.currentPanel;
        switch (panelType) {
            case PanelType.ClipOptions:
            case PanelType.ClippingFailure:
                var error = this.props.clipperState.oneNoteApiResult.data;
                var apiResponseCode = oneNoteApiUtils_1.OneNoteApiUtils.getApiResponseCode(error);
                if (oneNoteApiUtils_1.OneNoteApiUtils.requiresSignout(apiResponseCode)) {
                    // If the ResponseCode requires a SignOut to fix, then the dialogButton will handle SignOut
                    // so we will not show the footer. If it doesn't require signout, show the Footer
                    return undefined;
                }
            /* falls through */
            case PanelType.SignInNeeded:
                return <footer_1.Footer clipperState={this.props.clipperState} onSignOutInvoked={this.props.onSignOutInvoked}/>;
            case PanelType.ClippingSuccess:
            /* falls through */
            default:
                return undefined;
        }
    };
    MainControllerClass.prototype.render = function () {
        var panelToRender = this.getCurrentPanel();
        var closeButtonToRender = this.getCloseButtonForType();
        var footerToRender = this.getCurrentFooter();
        return (<div id={constants_1.Constants.Ids.mainController} {...this.onElementDraw(this.onMainControllerDraw)}>
				{closeButtonToRender}
				<div className="panelContent">
					<div className={constants_1.Constants.Classes.heightAnimator} {...this.onElementDraw(this.onHeightAnimatorDraw)}>
						<div className={constants_1.Constants.Classes.panelAnimator} {...this.onElementDraw(this.onPanelAnimatorDraw)}>
							{panelToRender}
							{footerToRender}
						</div>
					</div>
				</div>
			</div>);
    };
    return MainControllerClass;
}(componentBase_1.ComponentBase));
exports.MainControllerClass = MainControllerClass;
var component = MainControllerClass.componentize();
exports.MainController = component;
