"use strict";
var OperationResult;
(function (OperationResult) {
    OperationResult[OperationResult["Succeeded"] = 0] = "Succeeded";
    OperationResult[OperationResult["Failed"] = 1] = "Failed";
})(OperationResult = exports.OperationResult || (exports.OperationResult = {}));
