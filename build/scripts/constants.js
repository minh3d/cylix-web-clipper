"use strict";
var Constants;
(function (Constants) {
    var Classes;
    (function (Classes) {
        // animators
        Classes.heightAnimator = "height-animator";
        Classes.panelAnimator = "panel-animator";
        Classes.clearfix = "clearfix";
        // changeLogPanel
        Classes.change = "change";
        Classes.changes = "changes";
        Classes.changeBody = "change-body";
        Classes.changeDescription = "change-description";
        Classes.changeImage = "change-image";
        Classes.changeTitle = "change-title";
        // checkbox
        Classes.checkboxCheck = "checkboxCheck";
        // textArea input control
        Classes.textAreaInput = "textAreaInput";
        Classes.textAreaInputMirror = "textAreaInputMirror";
        // popover
        Classes.popover = "popover";
        Classes.popoverArrow = "popover-arrow";
        // previewViewer
        Classes.deleteHighlightButton = "delete-highlight";
        Classes.highlightable = "highlightable";
        Classes.highlighted = "highlighted";
        Classes.regionSelection = "region-selection";
        Classes.regionSelectionImage = "region-selection-image";
        Classes.regionSelectionRemoveButton = "region-selection-remove-button";
        // pdfPreviewViewer
        Classes.attachmentOverlay = "attachment-overlay";
        Classes.centeredInCanvas = "centered-in-canvas";
        Classes.overlay = "overlay";
        Classes.overlayHidden = "overlay-hidden";
        Classes.overlayNumber = "overlay-number";
        Classes.pdfPreviewImage = "pdf-preview-image";
        Classes.pdfPreviewImageCanvas = "pdf-preview-image-canvas";
        Classes.unselected = "unselected";
        Classes.localPdfPanelTitle = "local-pdf-panel-title";
        Classes.localPdfPanelSubtitle = "local-pdf-panel-subtitle";
        // radioButton
        Classes.radioIndicatorFill = "radio-indicator-fill";
        // spriteAnimation
        Classes.spinner = "spinner";
        // Accessibility 
        Classes.srOnly = "sr-only";
        // tooltip
        Classes.tooltip = "tooltip";
        // rotatingMessageSpriteAnimation
        Classes.centeredInPreview = "centered-in-preview";
    })(Classes = Constants.Classes || (Constants.Classes = {}));
    var Cookies;
    (function (Cookies) {
        Cookies.clipperInfo = "ClipperInfo";
    })(Cookies = Constants.Cookies || (Constants.Cookies = {}));
    var Extension;
    (function (Extension) {
        var NotificationIds;
        (function (NotificationIds) {
            NotificationIds.conflictingExtension = "conflictingExtension";
        })(NotificationIds = Extension.NotificationIds || (Extension.NotificationIds = {}));
    })(Extension = Constants.Extension || (Constants.Extension = {}));
    var Ids;
    (function (Ids) {
        // annotationInput
        Ids.annotationContainer = "annotationContainer";
        Ids.annotationField = "annotationField";
        Ids.annotationFieldMirror = "annotationFieldMirror";
        Ids.annotationPlaceholder = "annotationPlaceholder";
        // bookmarkPreview
        Ids.bookmarkThumbnail = "bookmarkThumbnail";
        Ids.bookmarkPreviewContentContainer = "bookmarkPreviewContentContainer";
        Ids.bookmarkPreviewInnerContainer = "bookmarkPreviewInnerContainer";
        // clippingPanel
        Ids.clipperApiProgressContainer = "clipperApiProgressContainer";
        // clippingPanel
        Ids.clipProgressDelayedMessage = "clipProgressDelayedMessage";
        Ids.clipProgressIndicatorMessage = "clipProgressIndicatorMessage";
        // dialogPanel
        Ids.dialogBackButton = "dialogBackButton";
        Ids.dialogButtonContainer = "dialogButtonContainer";
        Ids.dialogDebugMessageContainer = "dialogDebugMessageContainer";
        Ids.dialogMessageContainer = "dialogMessageContainer";
        Ids.dialogContentContainer = "dialogContentContainer";
        Ids.dialogMessage = "dialogMessage";
        Ids.dialogSignOutButton = "dialogSignoutButton";
        Ids.dialogTryAgainButton = "dialogTryAgainButton";
        // editorPreviewComponentBase
        Ids.highlightablePreviewBody = "highlightablePreviewBody";
        // failurePanel
        Ids.apiErrorMessage = "apiErrorMessage";
        Ids.backToHomeButton = "backToHomeButton";
        Ids.clipperFailureContainer = "clipperFailureContainer";
        Ids.refreshPageButton = "refreshPageButton";
        Ids.tryAgainButton = "tryAgainButton";
        // footer
        Ids.clipperFooterContainer = "clipperFooterContainer";
        Ids.currentUserControl = "currentUserControl";
        Ids.currentUserDetails = "currentUserDetails";
        Ids.currentUserEmail = "currentUserEmail";
        Ids.currentUserId = "currentUserId";
        Ids.currentUserName = "currentUserName";
        Ids.feedbackButton = "feedbackButton";
        Ids.feedbackImage = "feedbackImage";
        Ids.signOutButton = "signOutButton";
        Ids.userDropdownArrow = "userDropdownArrow";
        Ids.userSettingsContainer = "userSettingsContainer";
        Ids.feedbackLabel = "feedbackLabel";
        Ids.footerButtonsContainer = "footerButtonsContainer";
        // loadingPanel
        Ids.clipperLoadingContainer = "clipperLoadingContainer";
        // mainController
        Ids.closeButton = "closeButton";
        Ids.closeButtonContainer = "closeButtonContainer";
        Ids.mainController = "mainController";
        // OneNotePicker
        Ids.saveToLocationContainer = "saveToLocationContainer";
        // optionsPanel
        Ids.clipButton = "clipButton";
        Ids.clipButtonContainer = "clipButtonContainer";
        Ids.optionLabel = "optionLabel";
        // previewViewerPdfHeader
        Ids.radioAllPagesLabel = "radioAllPagesLabel";
        Ids.radioPageRangeLabel = "radioPageRangeLabel";
        Ids.rangeInput = "rangeInput";
        // previewViewer
        Ids.previewBody = "previewBody";
        Ids.previewContentContainer = "previewContentContainer";
        Ids.previewHeader = "previewHeader";
        Ids.previewHeaderContainer = "previewHeaderContainer";
        Ids.previewHeaderInput = "previewHeaderInput";
        Ids.previewHeaderInputMirror = "previewHeaderInputMirror";
        Ids.previewTitleContainer = "previewTitleContainer";
        Ids.previewSubtitleContainer = "previewSubtitleContainer";
        Ids.previewInnerContainer = "previewInnerContainer";
        Ids.previewOptionsContainer = "previewOptionsContainer";
        Ids.previewInnerWrapper = "previewInnerWrapper";
        Ids.previewOuterContainer = "previewOuterContainer";
        Ids.previewUrlContainer = "previewUrlContainer";
        Ids.previewNotesContainer = "previewNotesContainer";
        // previewViewerFullPageHeader
        Ids.fullPageControl = "fullPageControl";
        Ids.fullPageHeaderTitle = "fullPageHeaderTitle";
        // previewViewerPdfHeader
        Ids.localPdfFileTitle = "localPdfFileTitle";
        Ids.pdfControl = "pdfControl";
        Ids.pdfHeaderTitle = "pdfHeaderTitle";
        Ids.pageRangeControl = "pageRangeControl";
        // pdfClipOptions
        Ids.checkboxToDistributePages = "checkboxToDistributePages";
        Ids.pdfIsTooLargeToAttachIndicator = "pdfIsTooLargeToAttachIndicator";
        Ids.checkboxToAttachPdf = "checkboxToAttachPdf";
        Ids.moreClipOptions = "moreClipOptions";
        // previewViewerRegionHeader
        Ids.addAnotherRegionButton = "addAnotherRegionButton";
        Ids.addRegionControl = "addRegionControl";
        // previewViewerRegionTitleOnlyHeader
        Ids.regionControl = "regionControl";
        Ids.regionHeaderTitle = "regionHeaderTitle";
        // previewViewerAugmentationHeader
        Ids.decrementFontSize = "decrementFontSize";
        Ids.fontSizeControl = "fontSizeControl";
        Ids.highlightButton = "highlightButton";
        Ids.highlightControl = "highlightControl";
        Ids.incrementFontSize = "incrementFontSize";
        Ids.serifControl = "serifControl";
        Ids.sansSerif = "sansSerif";
        Ids.serif = "serif";
        // previewViewerBookmarkHeader
        Ids.bookmarkControl = "bookmarkControl";
        Ids.bookmarkHeaderTitle = "bookmarkHeaderTitle";
        // ratingsPrompt
        Ids.ratingsButtonFeedbackNo = "ratingsButtonFeedbackNo";
        Ids.ratingsButtonFeedbackYes = "ratingsButtonFeedbackYes";
        Ids.ratingsButtonInitNo = "ratingsButtonInitNo";
        Ids.ratingsButtonInitYes = "ratingsButtonInitYes";
        Ids.ratingsButtonRateNo = "ratingsButtonRateNo";
        Ids.ratingsButtonRateYes = "ratingsButtonRateYes";
        Ids.ratingsPromptContainer = "ratingsPromptContainer";
        // regionSelectingPanel
        Ids.regionInstructionsContainer = "regionInstructionsContainer";
        Ids.regionClipCancelButton = "regionClipCancelButton";
        // regionSelector
        Ids.innerFrame = "innerFrame";
        Ids.outerFrame = "outerFrame";
        Ids.regionSelectorContainer = "regionSelectorContainer";
        // rotatingMessageSpriteAnimation
        Ids.spinnerText = "spinnerText";
        // sectionPicker
        Ids.locationPickerContainer = "locationPickerContainer";
        // signInPanel
        Ids.signInButtonMsa = "signInButtonMsa";
        Ids.signInButtonOrgId = "signInButtonOrgId";
        Ids.signInContainer = "signInContainer";
        Ids.signInErrorCookieInformation = "signInErrorCookieInformation";
        Ids.signInErrorDebugInformation = "signInErrorDebugInformation";
        Ids.signInErrorDebugInformationDescription = "signInErrorDebugInformationDescription";
        Ids.signInErrorDebugInformationContainer = "signInErrorDebugInformationContainer";
        Ids.signInErrorDebugInformationList = "signInErrorDebugInformationList";
        Ids.signInErrorDescription = "signInErrorDescription";
        Ids.signInErrorDescriptionContainer = "signInErrorDescriptionContainer";
        Ids.signInErrorMoreInformation = "signInErrorMoreInformation";
        Ids.signInLogo = "signInLogo";
        Ids.signInMessageLabelContainer = "signInMessageLabelContainer";
        Ids.signInText = "signInText";
        Ids.signInToggleErrorDropdownArrow = "signInToggleErrorDropdownArrow";
        Ids.signInToggleErrorInformationText = "signInToggleErrorInformationText";
        // successPanel
        Ids.clipperSuccessContainer = "clipperSuccessContainer";
        Ids.launchOneNoteButton = "launchOneNoteButton";
        // tooltipRenderer
        Ids.pageNavAnimatedTooltip = "pageNavAnimatedTooltip";
        // unsupportedBrowser
        Ids.unsupportedBrowserContainer = "unsupportedBrowserContainer";
        Ids.unsupportedBrowserPanel = "unsupportedBrowserPanel";
        // whatsNewPanel
        Ids.changeLogSubPanel = "changeLogSubPanel";
        Ids.checkOutWhatsNewButton = "checkOutWhatsNewButton";
        Ids.proceedToWebClipperButton = "proceedToWebClipperButton";
        Ids.whatsNewTitleSubPanel = "whatsNewTitleSubPanel";
        Ids.clipperRootScript = "oneNoteCaptureRootScript";
        Ids.clipperUiFrame = "oneNoteWebClipper";
        Ids.clipperPageNavFrame = "oneNoteWebClipperPageNav";
        Ids.clipperExtFrame = "oneNoteWebClipperExtension";
        // tooltips
        Ids.brandingContainer = "brandingContainer";
    })(Ids = Constants.Ids || (Constants.Ids = {}));
    var HeaderValues;
    (function (HeaderValues) {
        HeaderValues.accept = "Accept";
        HeaderValues.appIdKey = "MS-Int-AppId";
        HeaderValues.correlationId = "X-CorrelationId";
        HeaderValues.noAuthKey = "X-NoAuth";
        HeaderValues.userSessionIdKey = "X-UserSessionId";
    })(HeaderValues = Constants.HeaderValues || (Constants.HeaderValues = {}));
    var CommunicationChannels;
    (function (CommunicationChannels) {
        // Debug Logging
        CommunicationChannels.debugLoggingInjectedAndExtension = "DEBUGLOGGINGINJECTED_AND_EXTENSION";
        // Web Clipper
        CommunicationChannels.extensionAndUi = "EXTENSION_AND_UI";
        CommunicationChannels.injectedAndUi = "INJECTED_AND_UI";
        CommunicationChannels.injectedAndExtension = "INJECTED_AND_EXTENSION";
        // What's New
        CommunicationChannels.extensionAndPageNavUi = "EXTENSION_AND_PAGENAVUI";
        CommunicationChannels.pageNavInjectedAndPageNavUi = "PAGENAVINJECTED_AND_PAGENAVUI";
        CommunicationChannels.pageNavInjectedAndExtension = "PAGENAVINJECTED_AND_EXTENSION";
    })(CommunicationChannels = Constants.CommunicationChannels || (Constants.CommunicationChannels = {}));
    var FunctionKeys;
    (function (FunctionKeys) {
        FunctionKeys.clipperStrings = "CLIPPER_STRINGS";
        FunctionKeys.clipperStringsFrontLoaded = "CLIPPER_STRINGS_FRONT_LOADED";
        FunctionKeys.closePageNavTooltip = "CLOSE_PAGE_NAV_TOOLTIP";
        FunctionKeys.createHiddenIFrame = "CREATE_HIDDEN_IFRAME";
        FunctionKeys.ensureFreshUserBeforeClip = "ENSURE_FRESH_USER_BEFORE_CLIP";
        FunctionKeys.escHandler = "ESC_HANDLER";
        FunctionKeys.getInitialUser = "GET_INITIAL_USER";
        FunctionKeys.getPageNavTooltipProps = "GET_PAGE_NAV_TOOLTIP_PROPS";
        FunctionKeys.getStorageValue = "GET_STORAGE_VALUE";
        FunctionKeys.getMultipleStorageValues = "GET_MULTIPLE_STORAGE_VALUES";
        FunctionKeys.getTooltipToRenderInPageNav = "GET_TOOLTIP_TO_RENDER_IN_PAGE_NAV";
        FunctionKeys.hideUi = "HIDE_UI";
        FunctionKeys.invokeClipper = "INVOKE_CLIPPER";
        FunctionKeys.invokeClipperFromPageNav = "INVOKE_CLIPPER_FROM_PAGE_NAV";
        FunctionKeys.invokeDebugLogging = "INVOKE_DEBUG_LOGGING";
        FunctionKeys.invokePageNav = "INVOKE_PAGE_NAV";
        FunctionKeys.extensionNotAllowedToAccessLocalFiles = "EXTENSION_NOT_ALLOWED_TO_ACCESS_LOCAL_FILES";
        FunctionKeys.noOpTracker = "NO_OP_TRACKER";
        FunctionKeys.onSpaNavigate = "ON_SPA_NAVIGATE";
        FunctionKeys.refreshPage = "REFRESH_PAGE";
        FunctionKeys.showRefreshClipperMessage = "SHOW_REFRESH_CLIPPER_MESSAGE";
        FunctionKeys.setInjectOptions = "SET_INJECT_OPTIONS";
        FunctionKeys.setInvokeOptions = "SET_INVOKE_OPTIONS";
        FunctionKeys.setStorageValue = "SET_STORAGE_VALUE";
        FunctionKeys.signInUser = "SIGN_IN_USER";
        FunctionKeys.signOutUser = "SIGN_OUT_USER";
        FunctionKeys.tabToLowestIndexedElement = "TAB_TO_LOWEST_INDEXED_ELEMENT";
        FunctionKeys.takeTabScreenshot = "TAKE_TAB_SCREENSHOT";
        FunctionKeys.telemetry = "TELEMETRY";
        FunctionKeys.toggleClipper = "TOGGLE_CLIPPER";
        FunctionKeys.unloadHandler = "UNLOAD_HANDLER";
        FunctionKeys.updateFrameHeight = "UPDATE_FRAME_HEIGHT";
        FunctionKeys.updatePageInfoIfUrlChanged = "UPDATE_PAGE_INFO_IF_URL_CHANGED";
    })(FunctionKeys = Constants.FunctionKeys || (Constants.FunctionKeys = {}));
    var KeyCodes;
    (function (KeyCodes) {
        // event.which is deprecated -.-
        KeyCodes.tab = 9;
        KeyCodes.enter = 13;
        KeyCodes.esc = 27;
        KeyCodes.c = 67;
        KeyCodes.down = 40;
        KeyCodes.up = 38;
        KeyCodes.left = 37;
        KeyCodes.right = 39;
        KeyCodes.space = 32;
        KeyCodes.home = 36;
        KeyCodes.end = 35;
    })(KeyCodes = Constants.KeyCodes || (Constants.KeyCodes = {}));
    var StringKeyCodes;
    (function (StringKeyCodes) {
        StringKeyCodes.c = "KeyC";
    })(StringKeyCodes = Constants.StringKeyCodes || (Constants.StringKeyCodes = {}));
    var SmartValueKeys;
    (function (SmartValueKeys) {
        SmartValueKeys.clientInfo = "CLIENT_INFO";
        SmartValueKeys.isFullScreen = "IS_FULL_SCREEN";
        SmartValueKeys.pageInfo = "PAGE_INFO";
        SmartValueKeys.sessionId = "SESSION_ID";
        SmartValueKeys.user = "USER";
    })(SmartValueKeys = Constants.SmartValueKeys || (Constants.SmartValueKeys = {}));
    var Styles;
    (function (Styles) {
        Styles.sectionPickerContainerHeight = 280;
        Styles.clipperUiWidth = 322;
        Styles.clipperUiTopRightOffset = 20;
        Styles.clipperUiDropShadowBuffer = 7;
        Styles.clipperUiInnerPadding = 30;
        var Colors;
        (function (Colors) {
            Colors.oneNoteHighlightColor = "#fefe56";
        })(Colors = Styles.Colors || (Styles.Colors = {}));
    })(Styles = Constants.Styles || (Constants.Styles = {}));
    var Urls;
    (function (Urls) {
        Urls.serviceDomain = "http://localhost";
        Urls.augmentationApiUrl = Urls.serviceDomain + "/onaugmentation/clipperextract/v1.0/";
        Urls.changelogUrl = Urls.serviceDomain + "/whatsnext/webclipper";
        Urls.clipperFeedbackUrl = Urls.serviceDomain + "/feedback";
        Urls.clipperInstallPageUrl = Urls.serviceDomain + "/clipper/installed";
        Urls.fullPageScreenshotUrl = Urls.serviceDomain + "/onaugmentation/clipperDomEnhancer/v1.0/";
        Urls.localizedStringsUrlBase = Urls.serviceDomain + "/strings?ids=WebClipper.";
        Urls.userFlightingEndpoint = Urls.serviceDomain + "/webclipper/userflight";
        Urls.msaDomain = "http://localhost";
        Urls.orgIdDomain = "http://localhost";
        var Authentication;
        (function (Authentication) {
            Authentication.authRedirectUrl = Urls.serviceDomain + "/webclipper/auth";
            Authentication.signInUrl = Urls.serviceDomain + "/webclipper/signin";
            Authentication.signOutUrl = Urls.serviceDomain + "/webclipper/signout";
            Authentication.userInformationUrl = Urls.serviceDomain + "/webclipper/userinfo";
        })(Authentication = Urls.Authentication || (Urls.Authentication = {}));
        var QueryParams;
        (function (QueryParams) {
            QueryParams.authType = "authType";
            QueryParams.category = "category";
            QueryParams.changelogLocale = "omkt";
            QueryParams.channel = "channel";
            QueryParams.clientType = "clientType";
            QueryParams.clipperId = "clipperId";
            QueryParams.clipperVersion = "clipperVersion";
            QueryParams.correlationId = "correlationId";
            QueryParams.error = "error";
            QueryParams.errorDescription = "error_?description";
            QueryParams.event = "event";
            QueryParams.eventName = "eventName";
            QueryParams.failureId = "failureId";
            QueryParams.failureInfo = "failureInfo";
            QueryParams.failureType = "failureType";
            QueryParams.inlineInstall = "inlineInstall";
            QueryParams.label = "label";
            QueryParams.noOpType = "noOpType";
            QueryParams.stackTrace = "stackTrace";
            QueryParams.timeoutInMs = "timeoutInMs";
            QueryParams.url = "url";
            QueryParams.userSessionId = "userSessionId";
            QueryParams.wdFromClipper = "wdfromclipper"; // This naming convention is standard in OneNote Online
        })(QueryParams = Urls.QueryParams || (Urls.QueryParams = {}));
    })(Urls = Constants.Urls || (Constants.Urls = {}));
    var LogCategories;
    (function (LogCategories) {
        LogCategories.oneNoteClipperUsage = "OneNoteClipperUsage";
    })(LogCategories = Constants.LogCategories || (Constants.LogCategories = {}));
    var Settings;
    (function (Settings) {
        Settings.fontSizeStep = 2;
        Settings.maxClipSuccessForRatingsPrompt = 12;
        Settings.maximumJSTimeValue = 1000 * 60 * 60 * 24 * 100000000; // 100M days in milliseconds, http://ecma-international.org/ecma-262/5.1/#sec-15.9.1.1
        Settings.maximumFontSize = 72;
        Settings.maximumNumberOfTimesToShowTooltips = 3;
        Settings.maximumMimeSizeLimit = 24900000;
        Settings.minClipSuccessForRatingsPrompt = 4;
        Settings.minimumFontSize = 8;
        Settings.minTimeBetweenBadRatings = 1000 * 60 * 60 * 24 * 7 * 10; // 10 weeks
        Settings.noOpTrackerTimeoutDuration = 20 * 1000; // 20 seconds
        Settings.numRetriesPerPatchRequest = 3;
        Settings.pdfCheckCreatePageInterval = 2000; // 2 seconds
        Settings.pdfClippingMessageDelay = 5000; // 5 seconds
        Settings.pdfExtraPageLoadEachSide = 1;
        Settings.pdfInitialPageLoadCount = 3;
        Settings.timeBetweenDifferentTooltips = 1000 * 60 * 60 * 24 * 7 * 1; // 1 week
        Settings.timeBetweenSameTooltip = 1000 * 60 * 60 * 24 * 7 * 3; // 3 weeks
        Settings.timeBetweenTooltips = 1000 * 60 * 60 * 24 * 7 * 3; // 21 days
        Settings.timeUntilPdfPageNumbersFadeOutAfterScroll = 1000; // 1 second
    })(Settings = Constants.Settings || (Constants.Settings = {}));
    var CustomHtmlAttributes;
    (function (CustomHtmlAttributes) {
        CustomHtmlAttributes.setNameForArrowKeyNav = "setnameforarrowkeynav";
    })(CustomHtmlAttributes = Constants.CustomHtmlAttributes || (Constants.CustomHtmlAttributes = {}));
    var AriaSet;
    (function (AriaSet) {
        AriaSet.modeButtonSet = "ariaModeButtonSet";
        AriaSet.pdfPageSelection = "pdfPageSelection";
        AriaSet.serifGroupSet = "serifGroupSet";
    })(AriaSet = Constants.AriaSet || (Constants.AriaSet = {}));
})(Constants = exports.Constants || (exports.Constants = {}));
