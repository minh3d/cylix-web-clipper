"use strict";
var SmartValue = (function () {
    function SmartValue(t) {
        this.subscriptions = [];
        this.t = t;
    }
    SmartValue.prototype.subscribe = function (func, options) {
        if (options === void 0) { options = {}; }
        // Setup the defaults if they weren't specified
        if (options.times === undefined) {
            options.times = Infinity;
        }
        if (options.callOnSubscribe === undefined) {
            options.callOnSubscribe = true;
        }
        if (options.callOnSubscribe) {
            func(this.t);
        }
        if (options.times > 0) {
            this.subscriptions.push({ func: func, times: options.times });
        }
    };
    SmartValue.prototype.unsubscribe = function (func) {
        for (var i = 0; i < this.subscriptions.length; i++) {
            if (func === this.subscriptions[i].func) {
                this.subscriptions.splice(i, 1);
                return;
            }
        }
    };
    SmartValue.prototype.set = function (t) {
        if (this.t !== t) {
            this.t = t;
            this.notifySubscribers();
        }
        return this;
    };
    SmartValue.prototype.get = function () {
        return this.t;
    };
    SmartValue.prototype.forceUpdate = function () {
        this.notifySubscribers();
    };
    SmartValue.prototype.equals = function (t) {
        return this.t === t;
    };
    SmartValue.prototype.toString = function () {
        return !this.t ? "undefined" : this.t.toString();
    };
    SmartValue.prototype.notifySubscribers = function () {
        var numSubscribers = this.subscriptions.length;
        for (var i = 0; i < numSubscribers; i++) {
            this.subscriptions[i].times--;
            this.subscriptions[i].func(this.t);
            var noMoreExecutions = this.subscriptions[i] && this.subscriptions[i].times === 0;
            if (noMoreExecutions) {
                this.subscriptions.splice(i, 1);
            }
            // We check for undefined as the callback could have called unsubscribe
            if (!this.subscriptions[i] || noMoreExecutions) {
                numSubscribers--;
                i--;
            }
        }
    };
    // Subscribe to multiple SVs.
    // Example:
    // var appleColor: SmartValue<string>;
    // var appleCount: SmartValue<number>;
    // Subscribe( [appleColor, appleCount], function(color, count) { /*Do something*/});
    SmartValue.subscribe = function (values, func) {
        for (var i = 0; i < values.length; i++) {
            values[i].subscribe(function () {
                var currValues = [];
                for (var j = 0; j < values.length; j++) {
                    currValues.push(values[j].get());
                }
                // ReSharper disable once SuspiciousThisUsage
                func.apply(this, currValues);
            });
        }
    };
    return SmartValue;
}());
exports.SmartValue = SmartValue;
