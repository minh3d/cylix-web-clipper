"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var messageHandler_1 = require("./messageHandler");
// Communication manager class for handling message passing between windows
var IFrameMessageHandler = (function (_super) {
    __extends(IFrameMessageHandler, _super);
    function IFrameMessageHandler(getOtherWindow) {
        var _this = _super.call(this) || this;
        _this.getOtherWindow = getOtherWindow;
        _this.initMessageHandler();
        window.addEventListener("message", _this.messageHandler);
        return _this;
    }
    IFrameMessageHandler.prototype.initMessageHandler = function () {
        var _this = this;
        this.messageHandler = function (event) {
            _this.onMessageReceived(event.data);
            // Since the message was correctly handled, we don't want any pre-established handlers getting called
            if (event.stopPropagation) {
                event.stopPropagation();
            }
            else {
                event.cancelBubble = true;
            }
        };
    };
    IFrameMessageHandler.prototype.sendMessage = function (dataString) {
        var otherWindow = this.getOtherWindow();
        otherWindow.postMessage(dataString, "*");
    };
    IFrameMessageHandler.prototype.tearDown = function () {
        window.removeEventListener("message", this.messageHandler);
    };
    return IFrameMessageHandler;
}(messageHandler_1.MessageHandler));
exports.IFrameMessageHandler = IFrameMessageHandler;
