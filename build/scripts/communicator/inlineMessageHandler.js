"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var messageHandler_1 = require("./messageHandler");
var InlineMessageHandler = (function (_super) {
    __extends(InlineMessageHandler, _super);
    function InlineMessageHandler(otherSide) {
        var _this = _super.call(this) || this;
        _this.setOtherSide(otherSide);
        return _this;
    }
    InlineMessageHandler.prototype.initMessageHandler = function () { };
    InlineMessageHandler.prototype.inlineMessage = function (data) {
        this.onMessageReceived(data);
    };
    InlineMessageHandler.prototype.setOtherSide = function (otherSide) {
        this.otherSide = otherSide;
    };
    InlineMessageHandler.prototype.sendMessage = function (data) {
        if (this.otherSide) {
            this.otherSide.inlineMessage(data);
        }
    };
    InlineMessageHandler.prototype.tearDown = function () { };
    return InlineMessageHandler;
}(messageHandler_1.MessageHandler));
exports.InlineMessageHandler = InlineMessageHandler;
