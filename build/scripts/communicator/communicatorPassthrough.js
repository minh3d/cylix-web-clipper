"use strict";
var communicator_1 = require("./communicator");
/**
 * Links two communicators together and after initialization, passes all communication from either to the other
 * The general logic to do this is as follows:
 *     Initialize with handler1
 *     After an initial response from handler1, start queueing messages from handler1, and start initializing with handler2
 *     After an initial response from handler2, flush any queued messages, and pass-through the rest of the communications from either
 *  Note: The channel specified should be the same as the channel the other sides are using
 */
var CommunicatorPassthrough = (function () {
    function CommunicatorPassthrough(messageHandler1, messageHandler2, channel, errorHandler) {
        var comm1 = new communicator_1.Communicator(messageHandler1, channel);
        comm1.setErrorHandler(errorHandler);
        comm1.onInitialized = function () {
            // Queue up any messages passed until we can initialize with the other side
            var queuedMessages = [];
            comm1.handleDataPackage = function (dataPackage) {
                queuedMessages.push(dataPackage);
            };
            var comm2 = new communicator_1.Communicator(messageHandler2, channel);
            comm2.setErrorHandler(errorHandler);
            comm2.onInitialized = function () {
                comm1.onInitialized = undefined;
                comm2.onInitialized = undefined;
                // flush any queued up messages
                for (var _i = 0, queuedMessages_1 = queuedMessages; _i < queuedMessages_1.length; _i++) {
                    var dataPackage = queuedMessages_1[_i];
                    comm2.postMessage(dataPackage);
                }
                // Have each side simply pass packages to the other side untouched
                comm1.handleDataPackage = function (dataPackage) {
                    comm2.postMessage(dataPackage);
                };
                comm2.handleDataPackage = function (dataPackage) {
                    comm1.postMessage(dataPackage);
                };
            };
        };
    }
    return CommunicatorPassthrough;
}());
exports.CommunicatorPassthrough = CommunicatorPassthrough;
