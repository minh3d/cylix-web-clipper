"use strict";
var MessageHandler = (function () {
    function MessageHandler() {
    }
    /*
     * Event handler that the Communicator uses to know when this MessageHandler received a message
     */
    MessageHandler.prototype.onMessageReceived = function (data) {
        // This method is overwritten by the parent Communicator
        // Should be called when this MessageHandler receives a message
    };
    return MessageHandler;
}());
exports.MessageHandler = MessageHandler;
