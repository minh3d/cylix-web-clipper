"use strict";
/**
 * Communication interface for handling message passing between two scripts (separate windows, extensions, etc...)
 */
var Communicator = (function () {
    function Communicator(messageHandler, channel) {
        this.otherSideKeys = {};
        this.queuedCalls = {};
        // We do not want to override the callback if we call a remote function more than once, so each
        // time we register a callback, we need to add this and increment it accordingly.
        this.callbackIdPostfix = 0;
        this.functionMap = {};
        this.svFunctions = {};
        this.trackedSmartValues = {};
        this.channel = channel;
        this.messageHandler = messageHandler;
        this.messageHandler.onMessageReceived = this.parseMessage.bind(this);
        this.sendInitializationMessage();
    }
    Communicator.prototype.getMessageHandler = function () {
        return this.messageHandler;
    };
    /*
     * Event handler for when the other side has responded
     */
    Communicator.prototype.onInitialized = function () {
    };
    /**
     * Does any cleanup work needed
     */
    Communicator.prototype.tearDown = function () {
        // Unsubscribe to SVs
        for (var svKey in this.trackedSmartValues) {
            if (this.trackedSmartValues.hasOwnProperty(svKey)) {
                if (this.svFunctions[svKey]) {
                    for (var i = 0; i < this.svFunctions[svKey].length; i++) {
                        this.trackedSmartValues[svKey].unsubscribe(this.svFunctions[svKey][i]);
                    }
                }
            }
        }
        this.messageHandler.tearDown();
    };
    /**
     * Sets the error handler for when trying to communicate throws an error
     */
    Communicator.prototype.setErrorHandler = function (errorHandler) {
        this.communicatorErrorHandler = errorHandler;
    };
    /**
     * Parses the message and determines what action to take
     */
    Communicator.prototype.parseMessage = function (dataString) {
        var dataPackage;
        try {
            dataPackage = JSON.parse(dataString);
        }
        catch (error) {
            // Ignore messages that aren't in the expected format
            return;
        }
        // If it came from myself, ignore it :)
        if (!dataPackage) {
            return;
        }
        // If we specified a channel, then check it, if we didn't, then we ignore anything with one
        if ((this.channel && (!dataPackage.channel || dataPackage.channel !== this.channel)) ||
            (!this.channel && dataPackage.channel)) {
            return;
        }
        try {
            this.handleDataPackage(dataPackage);
        }
        catch (e) {
            if (this.communicatorErrorHandler) {
                this.communicatorErrorHandler(e);
            }
            else {
                throw e;
            }
        }
    };
    /**
     * Determines the correct way to handle the given data package.
     */
    Communicator.prototype.handleDataPackage = function (dataPackage) {
        var _this = this;
        if (dataPackage.functionKey === Communicator.initializationKey) {
            // The other side is coming online; acknowledge, and tell it about our existing functions
            this.sendAcknowledgementMessage();
            for (var functionName in this.functionMap) {
                if (this.functionMap.hasOwnProperty(functionName)) {
                    this.postMessage({ data: functionName, functionKey: Communicator.registrationKey });
                }
            }
            // Both sides are online now (we were first)
            this.onInitialized();
        }
        else if (dataPackage.functionKey === Communicator.acknowledgeKey) {
            // Both sides are online now (we were second)
            this.onInitialized();
        }
        else if (dataPackage.functionKey === Communicator.registrationKey) {
            // The other side is registering a function with us.
            var newKey = dataPackage.data.toString();
            if (!this.otherSideKeys[newKey]) {
                this.otherSideKeys[newKey] = true;
            }
            if (this.isSmartValueSubscription(newKey)) {
                // Make sure we immediately pass the latest value we have
                var smartValueName = newKey.substr(Communicator.setValuePrefix.length);
                var smartValue = this.trackedSmartValues[smartValueName];
                if (smartValue) {
                    this.updateRemoteSmartValue(smartValueName, smartValue.get());
                }
            }
            else if (this.queuedCalls[newKey]) {
                // Pass any calls to that function that we had saved up
                var calls = this.queuedCalls[newKey];
                for (var i = 0; i < calls.length; i++) {
                    this.postMessage(calls[i]);
                }
                delete this.queuedCalls[newKey];
            }
        }
        else {
            // Handle a normal function call from the other side
            var func = this.functionMap[dataPackage.functionKey];
            if (func) {
                var promiseResult = func(dataPackage.data);
                if (promiseResult && promiseResult.then && dataPackage.callbackKey) {
                    promiseResult.then(function (result) {
                        _this.callRemoteFunction(dataPackage.callbackKey, { param: result });
                    }, function (error) {
                        _this.callRemoteFunction(dataPackage.callbackKey, { param: error });
                    });
                }
            }
        }
    };
    /**
     * Registers a function name that can be called from the remote
     */
    Communicator.prototype.registerFunction = function (name, func) {
        if (!name) {
            throw new Error("param 'name' is invalid");
        }
        this.functionMap[name] = func;
        this.postMessage({ data: name, functionKey: Communicator.registrationKey });
    };
    /**
     * Triggers the call of a remote function that was registered with the given name
     */
    Communicator.prototype.callRemoteFunction = function (name, options) {
        if (!name) {
            throw new Error("param 'name' is invalid");
        }
        var paramData = options ? options.param : undefined;
        var callbackKey = undefined;
        if (options && options.callback) {
            callbackKey = name + Communicator.callbackPostfix + "-" + this.callbackIdPostfix++;
            this.registerFunction(callbackKey, options.callback);
        }
        var dataPackage = { data: paramData, functionKey: name };
        if (callbackKey) {
            dataPackage.callbackKey = callbackKey;
        }
        if (this.otherSideKeys[name]) {
            this.postMessage(dataPackage);
        }
        else if (!this.isSmartValueSubscription(name)) {
            // If it is a regular function call, queue it up to send when the other side comes online. SmartValues will happen automatically
            this.queuedCalls[name] = this.queuedCalls[name] || [];
            this.queuedCalls[name].push(dataPackage);
        }
    };
    /**
     * Subscribes to all changes for the SmartValue from the remote's version
     */
    Communicator.prototype.subscribeAcrossCommunicator = function (sv, name, subscribeCallback) {
        if (subscribeCallback) {
            sv.subscribe(subscribeCallback, { callOnSubscribe: false });
        }
        this.registerFunction(Communicator.setValuePrefix + name, function (val) {
            sv.set(val);
        });
    };
    /**
     * Broadcast all changes for the SmartValue to the remote's version
     */
    Communicator.prototype.broadcastAcrossCommunicator = function (sv, name) {
        var _this = this;
        var callback = function (val) {
            _this.updateRemoteSmartValue(name, val);
        };
        if (!this.svFunctions[name]) {
            this.svFunctions[name] = [];
        }
        this.svFunctions[name].push(callback);
        this.trackedSmartValues[name] = sv;
        sv.subscribe(callback);
    };
    Communicator.prototype.updateRemoteSmartValue = function (smartValueName, value) {
        this.callRemoteFunction(Communicator.setValuePrefix + smartValueName, { param: value });
    };
    /**
     * Sends a message to the other side to let them know we are connected for the firstTime
     */
    Communicator.prototype.sendInitializationMessage = function () {
        this.postMessage({ functionKey: Communicator.initializationKey });
    };
    /**
     * Sends a message to the other side to let them know we saw their initialization message
     */
    Communicator.prototype.sendAcknowledgementMessage = function () {
        this.postMessage({ functionKey: Communicator.acknowledgeKey });
    };
    Communicator.prototype.isSmartValueSubscription = function (functionKey) {
        return functionKey.substr(0, Communicator.setValuePrefix.length) === Communicator.setValuePrefix;
    };
    /**
     * Update the dataPackage with the channel, and send it as a JSON string to the MessageHandler
     */
    Communicator.prototype.postMessage = function (dataPackage) {
        // If we specified a channel, then we always send that with the message
        if (this.channel) {
            dataPackage.channel = this.channel;
        }
        try {
            this.messageHandler.sendMessage(JSON.stringify(dataPackage));
        }
        catch (e) {
            if (this.communicatorErrorHandler) {
                this.communicatorErrorHandler(e);
            }
            else {
                throw e;
            }
        }
    };
    return Communicator;
}());
Communicator.initializationKey = "INITIALIZATION-K3Y";
Communicator.acknowledgeKey = "ACKNOWLEDGE-K3Y";
Communicator.registrationKey = "REGISTER-FUNCTION-K3Y";
Communicator.setValuePrefix = "SETVALUE-";
Communicator.callbackPostfix = "-CALLBACK";
exports.Communicator = Communicator;
