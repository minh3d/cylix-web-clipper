"use strict";
var Experiments;
(function (Experiments) {
    var featuresToNumberlinesMap = {
        DummyExperiment: "dummy-flight"
    };
    Experiments.updateIntervalForFlights = 60 * 60 * 5 * 1000; // 5 hours
    var Feature;
    (function (Feature) {
        Feature[Feature["DummyExperiment"] = 0] = "DummyExperiment";
    })(Feature = Experiments.Feature || (Experiments.Feature = {}));
    ;
    /**
     * This functions returns true/false if a particular feature (identified by a feature name set in Experiments.ts)
     * is enabled for this user
     * @param nameOfFeature the name of the feature, as described in constants.ts
     * @returns True/False
     */
    function isFeatureEnabled(feature, flights) {
        var nameOfFeature = Feature[feature];
        if (!featuresToNumberlinesMap || !featuresToNumberlinesMap[nameOfFeature]) {
            return false;
        }
        var numberline = featuresToNumberlinesMap[nameOfFeature];
        if (!flights || flights.indexOf(numberline) === -1) {
            return false;
        }
        return true;
    }
    Experiments.isFeatureEnabled = isFeatureEnabled;
})(Experiments = exports.Experiments || (exports.Experiments = {}));
;
