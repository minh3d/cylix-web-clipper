"use strict";
var clientType_1 = require("./clientType");
var constants_1 = require("./constants");
var urlUtils_1 = require("./urlUtils");
var ClipperUrls;
(function (ClipperUrls) {
    function generateFeedbackUrl(clipperState, usid, logCategory) {
        var generatedFeedbackUrl = urlUtils_1.UrlUtils.addUrlQueryValue(constants_1.Constants.Urls.clipperFeedbackUrl, "LogCategory", logCategory);
        generatedFeedbackUrl = urlUtils_1.UrlUtils.addUrlQueryValue(generatedFeedbackUrl, "originalUrl", clipperState.pageInfo.rawUrl);
        generatedFeedbackUrl = urlUtils_1.UrlUtils.addUrlQueryValue(generatedFeedbackUrl, "clipperId", clipperState.clientInfo.clipperId);
        generatedFeedbackUrl = urlUtils_1.UrlUtils.addUrlQueryValue(generatedFeedbackUrl, "usid", usid);
        generatedFeedbackUrl = urlUtils_1.UrlUtils.addUrlQueryValue(generatedFeedbackUrl, "type", clientType_1.ClientType[clipperState.clientInfo.clipperType]);
        generatedFeedbackUrl = urlUtils_1.UrlUtils.addUrlQueryValue(generatedFeedbackUrl, "version", clipperState.clientInfo.clipperVersion);
        return generatedFeedbackUrl;
    }
    ClipperUrls.generateFeedbackUrl = generateFeedbackUrl;
    function generateSignInUrl(clipperId, sessionId, authType) {
        return addAuthenticationQueryValues(constants_1.Constants.Urls.Authentication.signInUrl, clipperId, sessionId, authType);
    }
    ClipperUrls.generateSignInUrl = generateSignInUrl;
    function generateSignOutUrl(clipperId, sessionId, authType) {
        return addAuthenticationQueryValues(constants_1.Constants.Urls.Authentication.signOutUrl, clipperId, sessionId, authType);
    }
    ClipperUrls.generateSignOutUrl = generateSignOutUrl;
    function addAuthenticationQueryValues(originalUrl, clipperId, sessionId, authType) {
        var authenticationUrl = urlUtils_1.UrlUtils.addUrlQueryValue(originalUrl, constants_1.Constants.Urls.QueryParams.authType, authType);
        authenticationUrl = urlUtils_1.UrlUtils.addUrlQueryValue(authenticationUrl, constants_1.Constants.Urls.QueryParams.clipperId, clipperId);
        authenticationUrl = urlUtils_1.UrlUtils.addUrlQueryValue(authenticationUrl, constants_1.Constants.Urls.QueryParams.userSessionId, sessionId);
        return authenticationUrl;
    }
})(ClipperUrls = exports.ClipperUrls || (exports.ClipperUrls = {}));
