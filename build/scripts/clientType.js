"use strict";
var ClientType;
(function (ClientType) {
    ClientType[ClientType["Bookmarklet"] = 0] = "Bookmarklet";
    ClientType[ClientType["ChromeExtension"] = 1] = "ChromeExtension";
    ClientType[ClientType["EdgeExtension"] = 2] = "EdgeExtension";
    ClientType[ClientType["FirefoxExtension"] = 3] = "FirefoxExtension";
    ClientType[ClientType["SafariExtension"] = 4] = "SafariExtension";
})(ClientType = exports.ClientType || (exports.ClientType = {}));
