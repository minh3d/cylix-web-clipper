"use strict";
var Rtl;
(function (Rtl) {
    var rtlLanguageCodes = ["ar", "fa", "he", "sd", "ug", "ur"];
    /*
     * Given a ISO 639-1 code (with optional ISO 3166 postfix), returns true
     * if and only if our localized string servers support that language, and
     * that language is RTL.
     */
    function isRtl(locale) {
        if (!locale) {
            return false;
        }
        var iso639P1LocaleCode = getIso639P1LocaleCode(locale);
        for (var i = 0; i < rtlLanguageCodes.length; i++) {
            if (iso639P1LocaleCode === rtlLanguageCodes[i]) {
                return true;
            }
        }
        return false;
    }
    Rtl.isRtl = isRtl;
    function getIso639P1LocaleCode(locale) {
        if (!locale) {
            return "";
        }
        return locale.split("-")[0].split("_")[0].toLowerCase();
    }
    Rtl.getIso639P1LocaleCode = getIso639P1LocaleCode;
})(Rtl = exports.Rtl || (exports.Rtl = {}));
