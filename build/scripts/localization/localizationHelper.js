"use strict";
var constants_1 = require("../constants");
var urlUtils_1 = require("../urlUtils");
var HttpWithRetries_1 = require("../http/HttpWithRetries");
var LocalizationHelper = (function () {
    function LocalizationHelper() {
    }
    LocalizationHelper.makeLocStringsFetchRequest = function (locale) {
        var url = urlUtils_1.UrlUtils.addUrlQueryValue(constants_1.Constants.Urls.localizedStringsUrlBase, "locale", locale);
        return HttpWithRetries_1.HttpWithRetries.get(url).then(function (request) {
            return Promise.resolve({
                request: request,
                parsedResponse: request.responseText
            });
        });
    };
    return LocalizationHelper;
}());
exports.LocalizationHelper = LocalizationHelper;
