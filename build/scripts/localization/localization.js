"use strict";
var Localization;
(function (Localization) {
    var FontFamily;
    (function (FontFamily) {
        FontFamily[FontFamily["Regular"] = 0] = "Regular";
        FontFamily[FontFamily["Bold"] = 1] = "Bold";
        FontFamily[FontFamily["Light"] = 2] = "Light";
        FontFamily[FontFamily["Semibold"] = 3] = "Semibold";
        FontFamily[FontFamily["Semilight"] = 4] = "Semilight";
    })(FontFamily = Localization.FontFamily || (Localization.FontFamily = {}));
    var localizedStrings;
    var formattedFontFamilies = {};
    // The fallback for when we are unable to fetch locstrings from our server
    var backupStrings = require("../../strings.json");
    /*
     * Gets the matching localized string, or the fallback (unlocalized) string if
     * unavailable.
     */
    function getLocalizedString(stringId) {
        if (!stringId) {
            throw new Error("stringId must be a non-empty string, but was: " + stringId);
        }
        if (localizedStrings) {
            var localResult = localizedStrings[stringId];
            if (localResult) {
                return localResult;
            }
        }
        var backupResult = backupStrings[stringId];
        if (backupResult) {
            return backupResult;
        }
        throw new Error("getLocalizedString could not find a localized or fallback string: " + stringId);
    }
    Localization.getLocalizedString = getLocalizedString;
    function setLocalizedStrings(localizedStringsAsJson) {
        localizedStrings = localizedStringsAsJson;
    }
    Localization.setLocalizedStrings = setLocalizedStrings;
    function getFontFamilyAsStyle(family) {
        return "font-family: " + getFontFamily(family) + ";";
    }
    Localization.getFontFamilyAsStyle = getFontFamilyAsStyle;
    function getFontFamily(family) {
        // Check cache first
        if (formattedFontFamilies[family]) {
            return formattedFontFamilies[family];
        }
        var stringId = "WebClipper.FontFamily." + FontFamily[family].toString();
        var fontFamily = getLocalizedString(stringId);
        formattedFontFamilies[family] = formatFontFamily(fontFamily);
        return formattedFontFamilies[family];
    }
    Localization.getFontFamily = getFontFamily;
    /*
     * If we want to set font families through JavaScript, it uses a specific
     * format. This helper function returns the formatted font family input.
     */
    function formatFontFamily(fontFamily) {
        if (!fontFamily) {
            return "";
        }
        var splits = fontFamily.split(",");
        for (var i = 0; i < splits.length; i++) {
            splits[i] = splits[i].trim();
            if (splits[i].length > 0 && splits[i].indexOf(" ") >= 0 && splits[i][0] !== "'" && splits[i][splits.length - 1] !== "'") {
                splits[i] = "'" + splits[i] + "'";
            }
        }
        return splits.join(",");
    }
    Localization.formatFontFamily = formatFontFamily;
})(Localization = exports.Localization || (exports.Localization = {}));
