"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var iframeMessageHandler_1 = require("../communicator/iframeMessageHandler");
var injectBase_1 = require("./injectBase");
var FrameInjectBase = (function (_super) {
    __extends(FrameInjectBase, _super);
    function FrameInjectBase(options) {
        var _this = _super.call(this, options) || this;
        try {
            _this.initFrameInDom();
            _this.initializeUiCommunicator();
            _this.initializePassthroughCommunicator(_this.getExtMessageHandlerThunk());
            _this.checkForNoOps();
        }
        catch (e) {
            _this.handleConstructorError(e);
            throw e;
        }
        return _this;
    }
    FrameInjectBase.prototype.getFrame = function () {
        return this.frame;
    };
    FrameInjectBase.prototype.closeFrame = function () {
        if (this.frame) {
            this.frame.parentNode.removeChild(this.frame);
            this.frame = undefined;
        }
    };
    /**
     * Generates the extension message handler thunk for the inline extension
     */
    FrameInjectBase.prototype.generateInlineExtThunk = function () {
        var _this = this;
        if (!this.frame) {
            this.initFrameInDom();
        }
        return function () { return new iframeMessageHandler_1.IFrameMessageHandler(function () { return _this.frame.contentWindow; }); };
    };
    /**
     * Instantiates the frame object, styles/sets attributes accordingly, then adds it to the DOM
     */
    FrameInjectBase.prototype.initFrameInDom = function () {
        // The frame will already be initialized in the case of the bookmarklet as it gets generated in order
        // to set up the ext communicator
        if (!this.frame) {
            this.createFrame();
            // Attach ourselves below the body where it is safer against the page and programmatic styling
            document.documentElement.appendChild(this.frame);
        }
    };
    return FrameInjectBase;
}(injectBase_1.InjectBase));
exports.FrameInjectBase = FrameInjectBase;
