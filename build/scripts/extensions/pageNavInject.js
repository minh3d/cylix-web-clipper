"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../constants");
var urlUtils_1 = require("../urlUtils");
var domUtils_1 = require("../domParsers/domUtils");
var communicator_1 = require("../communicator/communicator");
var communicatorPassthrough_1 = require("../communicator/communicatorPassthrough");
var iframeMessageHandler_1 = require("../communicator/iframeMessageHandler");
var smartValue_1 = require("../communicator/smartValue");
var Log = require("../logging/log");
var communicatorLoggerPure_1 = require("../logging/communicatorLoggerPure");
var styledFrameFactory_1 = require("./styledFrameFactory");
var frameInjectBase_1 = require("./frameInjectBase");
var PageNavInject = (function (_super) {
    __extends(PageNavInject, _super);
    function PageNavInject(options) {
        var _this = _super.call(this, options) || this;
        _this.logger = new communicatorLoggerPure_1.CommunicatorLoggerPure(_this.extCommunicator);
        _this.setPageInfoContextProperties();
        return _this;
    }
    PageNavInject.main = function (options) {
        // There is no toggling functionality in the What's New experience
        if (!document.getElementById(constants_1.Constants.Ids.clipperPageNavFrame)) {
            // Rather than using a static field (i.e., traditional singleton pattern), we have to attach
            // the singleton to the window object because each time we inject a new inject script, they are
            // sandboxed from each other, so having a static field will not work.
            window.pageNavInjectObject = new PageNavInject(options);
        }
    };
    PageNavInject.prototype.checkForNoOps = function () {
        // Nothing needed
    };
    PageNavInject.prototype.createFrame = function () {
        this.frame = styledFrameFactory_1.StyledFrameFactory.getStyledFrame(styledFrameFactory_1.Frame.PageNav);
        this.frame.id = constants_1.Constants.Ids.clipperPageNavFrame;
        this.frame.src = this.options.frameUrl;
    };
    PageNavInject.prototype.handleConstructorError = function (e) {
        Log.ErrorUtils.sendFailureLogRequest({
            label: Log.Failure.Label.UnhandledExceptionThrown,
            properties: {
                failureType: Log.Failure.Type.Unexpected,
                failureInfo: { error: JSON.stringify({ error: e.toString(), url: window.location.href }) },
                failureId: "PageNavInject",
                stackTrace: Log.Failure.getStackTrace(e)
            }
        });
    };
    PageNavInject.prototype.init = function () {
        this.pageInfo = new smartValue_1.SmartValue();
        // Nothing needed
    };
    PageNavInject.prototype.initializeExtCommunicator = function (extMessageHandlerThunk) {
        var _this = this;
        this.extCommunicator = new communicator_1.Communicator(extMessageHandlerThunk(), constants_1.Constants.CommunicationChannels.pageNavInjectedAndExtension);
        this.extCommunicator.registerFunction(constants_1.Constants.FunctionKeys.closePageNavTooltip, function () {
            _this.closeFrame();
        });
    };
    PageNavInject.prototype.initializeEventListeners = function () {
        var _this = this;
        // Notify the background when we're unloading
        window.onbeforeunload = function (event) {
            _this.extCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.unloadHandler);
        };
    };
    PageNavInject.prototype.initializePassthroughCommunicator = function (extMessageHandlerThunk) {
        var _this = this;
        var passthroughCommunicator = new communicatorPassthrough_1.CommunicatorPassthrough(extMessageHandlerThunk(), new iframeMessageHandler_1.IFrameMessageHandler(function () { return _this.frame.contentWindow; }), constants_1.Constants.CommunicationChannels.extensionAndPageNavUi);
    };
    PageNavInject.prototype.initializeUiCommunicator = function () {
        var _this = this;
        this.uiCommunicator = new communicator_1.Communicator(new iframeMessageHandler_1.IFrameMessageHandler(function () { return _this.frame.contentWindow; }), constants_1.Constants.CommunicationChannels.pageNavInjectedAndPageNavUi);
        this.uiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.closePageNavTooltip, function () {
            _this.closeFrame();
        });
        this.uiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.updateFrameHeight, function (newHeight) {
            _this.frame.style.height = newHeight + constants_1.Constants.Styles.clipperUiTopRightOffset + (constants_1.Constants.Styles.clipperUiDropShadowBuffer * 2) + "px";
        });
    };
    /**
     * Sets context properties relating to page info
     */
    PageNavInject.prototype.setPageInfoContextProperties = function () {
        this.logger.setContextProperty(Log.Context.Custom.ContentType, OneNoteApi.ContentType[domUtils_1.DomUtils.getPageContentType(document)]);
        this.logger.setContextProperty(Log.Context.Custom.InvokeHostname, urlUtils_1.UrlUtils.getHostname(document.URL));
        this.logger.setContextProperty(Log.Context.Custom.PageLanguage, domUtils_1.DomUtils.getLocale(document));
    };
    return PageNavInject;
}(frameInjectBase_1.FrameInjectBase));
exports.PageNavInject = PageNavInject;
