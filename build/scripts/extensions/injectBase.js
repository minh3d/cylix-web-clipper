"use strict";
var polyfills_1 = require("../polyfills");
var InjectBase = (function () {
    function InjectBase(options) {
        try {
            polyfills_1.Polyfills.init();
            this.options = options;
            this.init();
            this.initializeExtCommunicator(this.getExtMessageHandlerThunk());
            this.initializeEventListeners();
        }
        catch (e) {
            this.handleConstructorError(e);
            throw e;
        }
    }
    InjectBase.prototype.getExtMessageHandlerThunk = function () {
        // If not specified, assume this is an inline environment
        return this.options.extMessageHandlerThunk ?
            this.options.extMessageHandlerThunk :
            this.generateInlineExtThunk();
    };
    return InjectBase;
}());
exports.InjectBase = InjectBase;
