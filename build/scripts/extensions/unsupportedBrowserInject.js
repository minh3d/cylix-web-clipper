"use strict";
var extensionBase_1 = require("./extensionBase");
var browserUtils_1 = require("../browserUtils");
var constants_1 = require("../constants");
var clientType_1 = require("../clientType");
var rtl_1 = require("../localization/rtl");
var Log = require("../logging/log");
var UnsupportedBrowserInject = (function () {
    function UnsupportedBrowserInject() {
        this.frameHeight = 100;
        this.logUnsupportedBrowserEvent();
        this.clipperFrame = this.createUnsupportedBrowserFrame();
    }
    UnsupportedBrowserInject.main = function () {
        var clipperFrame = document.getElementById(constants_1.Constants.Ids.clipperUiFrame);
        if (!clipperFrame) {
            var injectBase = new UnsupportedBrowserInject();
        }
        else {
            clipperFrame.style.display = clipperFrame.style.display === "none" ? "block" : "none";
        }
    };
    UnsupportedBrowserInject.prototype.createUnsupportedBrowserFrame = function () {
        var element = document.createElement("iframe");
        element.id = constants_1.Constants.Ids.clipperUiFrame;
        element.src = bookmarkletRoot + "/unsupportedBrowser.html";
        var cssText = "margin: 0px; padding: 0px; border: 0px currentColor; border-image: none;" +
            "top: 0px; width: 349px; height: 250px; overflow: hidden;" +
            "display: block; position: fixed; z-index: 2147483647; ";
        // navigator.userLanguage is only available in IE, and Typescript will not recognize this property
        cssText += rtl_1.Rtl.isRtl(navigator.language || navigator.userLanguage) ? "left: 0;" : "right: 0;";
        element.style.cssText = cssText;
        // Attach to the body, unless we're in a frameset, then attach to the document itself
        if (document.body.nodeName === "FRAMESET") {
            document.documentElement.appendChild(element);
        }
        else {
            document.body.appendChild(element);
        }
        // Any CSS "transform"s on the html or body elements break our fixed position, override them so we display properly
        document.documentElement.style.transform = document.documentElement.style.webkitTransform = "none";
        document.body.style.transform = document.body.style.webkitTransform = "none";
        return element;
    };
    UnsupportedBrowserInject.prototype.logUnsupportedBrowserEvent = function () {
        var logData = {
            label: Log.Failure.Label.UnsupportedBrowser,
            properties: {
                failureType: Log.Failure.Type.Expected,
                failureInfo: { error: JSON.stringify({ browserVersion: browserUtils_1.BrowserUtils.ieVersion(), clipperVersion: extensionBase_1.ExtensionBase.getExtensionVersion(), clientType: clientType_1.ClientType[clientType_1.ClientType.Bookmarklet] }) },
                stackTrace: Log.Failure.getStackTrace()
            }
        };
        Log.ErrorUtils.sendFailureLogRequest(logData);
    };
    return UnsupportedBrowserInject;
}());
exports.UnsupportedBrowserInject = UnsupportedBrowserInject;
