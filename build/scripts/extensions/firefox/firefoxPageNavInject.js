"use strict";
var webExtensionPageNavInject_1 = require("../webExtensionBase/webExtensionPageNavInject");
var webExtension_1 = require("../webExtensionBase/webExtension");
var webExtensionMessageHandler_1 = require("../webExtensionBase/webExtensionMessageHandler");
webExtension_1.WebExtension.browser = chrome;
webExtensionPageNavInject_1.invoke({
    frameUrl: frameUrl,
    extMessageHandlerThunk: function () { return new webExtensionMessageHandler_1.WebExtensionContentMessageHandler(); }
});
