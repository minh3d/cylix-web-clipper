"use strict";
var ExtensionUtils;
(function (ExtensionUtils) {
    /*
     * Returns the relative path to the images directory.
     */
    function getImageResourceUrl(imageName) {
        // Since Chromebook has case-sensitive urls, we always go with lowercase image names.
        // See the use of "lowerCasePathName" in gulpfile.js where the images names are lower-cased
        // when copied)
        return ("images/" + imageName).toLowerCase();
    }
    ExtensionUtils.getImageResourceUrl = getImageResourceUrl;
})(ExtensionUtils = exports.ExtensionUtils || (exports.ExtensionUtils = {}));
