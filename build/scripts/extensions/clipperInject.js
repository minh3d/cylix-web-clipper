"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../constants");
var objectUtils_1 = require("../objectUtils");
var communicator_1 = require("../communicator/communicator");
var communicatorPassthrough_1 = require("../communicator/communicatorPassthrough");
var iframeMessageHandler_1 = require("../communicator/iframeMessageHandler");
var smartValue_1 = require("../communicator/smartValue");
var domUtils_1 = require("../domParsers/domUtils");
var Log = require("../logging/log");
var communicatorLoggerPure_1 = require("../logging/communicatorLoggerPure");
var clipperStorageKeys_1 = require("../storage/clipperStorageKeys");
var styledFrameFactory_1 = require("./styledFrameFactory");
var frameInjectBase_1 = require("./frameInjectBase");
var invokeOptions_1 = require("./invokeOptions");
/**
 * Loads up the Clipper iframe and manages it
 */
var ClipperInject = (function (_super) {
    __extends(ClipperInject, _super);
    function ClipperInject(options) {
        var _this = _super.call(this, options) || this;
        _this.frameHeight = 100;
        try {
            _this.updateUiSizeAttributes();
            _this.overrideTransformStyles(document.documentElement);
            _this.logger = new communicatorLoggerPure_1.CommunicatorLoggerPure(_this.uiCommunicator);
            _this.updatePageInfo();
            _this.extCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.setStorageValue, {
                param: { key: clipperStorageKeys_1.ClipperStorageKeys.lastInvokedDate, value: Date.now().toString() }
            });
            // We set up this call here as it requires both ui and ext communicators to be set up first
            _this.extCommunicator.registerFunction(constants_1.Constants.FunctionKeys.setInvokeOptions, function (invokeOptions) {
                // Some modes are gated here in the inject for extra processing
                switch (invokeOptions.invokeMode) {
                    case invokeOptions_1.InvokeMode.ContextTextSelection:
                        // In the case of PDF, the selection is passed to us from the WebExtension API, so we use that instead as Rangy won't work
                        if (invokeOptions.invokeDataForMode) {
                            invokeOptions.invokeDataForMode = _this.toScrubbedHtml(invokeOptions.invokeDataForMode);
                            _this.sendInvokeOptionsToUi(invokeOptions);
                            break;
                        }
                        // Rangy initializes itself on the page load, so we need to initialize it if the user invoked before this happens
                        if (!rangy.initialized && rangy.init) {
                            rangy.init();
                        }
                        // We get the selection here as the WebExtension API only allows us to get text
                        // We assume there is rangeCount of 1: https://developer.mozilla.org/en-US/docs/Web/API/Selection/rangeCount
                        var selection = rangy.getSelection();
                        var range = selection.getRangeAt(0);
                        var doc_1 = (new DOMParser()).parseFromString(range.toHtml(), "text/html");
                        domUtils_1.DomUtils.toOnml(doc_1).then(function () {
                            // Selections are prone to not having an outer html element, which can lead to anomalies in preview
                            invokeOptions.invokeDataForMode = _this.toScrubbedHtml(doc_1.body.innerHTML);
                            _this.sendInvokeOptionsToUi(invokeOptions);
                        });
                        break;
                    default:
                        _this.sendInvokeOptionsToUi(invokeOptions);
                        break;
                }
            });
            document.onkeydown = function (event) {
                if (event.keyCode === constants_1.Constants.KeyCodes.tab) {
                    if (window.getComputedStyle(_this.frame).display !== "none") {
                        _this.uiCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.tabToLowestIndexedElement);
                    }
                }
            };
        }
        catch (e) {
            _this.handleConstructorError(e);
            throw e;
        }
        return _this;
    }
    /**
     * Called to toggle the clipper's visibility, or to invoke it if it does not exist
     */
    ClipperInject.main = function (oneNoteClipperOptions) {
        // Rather than using a static field (i.e., traditional singleton pattern), we have to attach
        // the singleton to the window object because each time we inject a new inject script, they are
        // sandboxed from each other, so having a static field will not work.
        var oneNoteInjectBaseObject = window.oneNoteInjectBaseObject;
        if (!!document.getElementById(constants_1.Constants.Ids.clipperUiFrame) && oneNoteInjectBaseObject) {
            // The page could have changed between invokes e.g., single page apps
            if (window.getComputedStyle(oneNoteInjectBaseObject.frame).display === "none") {
                oneNoteInjectBaseObject.updatePageInfo();
            }
            oneNoteInjectBaseObject.toggleClipper();
            return oneNoteInjectBaseObject;
        }
        else if (!document.getElementById(constants_1.Constants.Ids.clipperUiFrame)) {
            // The Web Clipper has not been invoked on this page yet
            var clipperInject = new ClipperInject(oneNoteClipperOptions);
            window.oneNoteInjectBaseObject = clipperInject;
            return clipperInject;
        }
        else {
            // The inject base object has been lost, likely due to an extension refresh or update while the Web Clipper is invoked,
            // so we prevent further action. It technically still exists but it has been orphaned (i.e., we lost a reference to it).
            return undefined;
        }
    };
    ClipperInject.prototype.checkForNoOps = function () {
        // Ensure the frame was created and is visible
        var clipperFrame = document.getElementById(constants_1.Constants.Ids.clipperUiFrame);
        var frameWasNotInjected = objectUtils_1.ObjectUtils.isNullOrUndefined(clipperFrame);
        var url = window.location.href;
        if (frameWasNotInjected) {
            Log.ErrorUtils.sendNoOpTrackerRequest({
                label: Log.NoOp.Label.WebClipperUiFrameDidNotExist,
                channel: constants_1.Constants.CommunicationChannels.injectedAndUi,
                clientInfo: this.clientInfo,
                url: url
            });
        }
        else {
            var isFrameVisible = window.getComputedStyle(clipperFrame).display !== "none";
            if (!isFrameVisible) {
                Log.ErrorUtils.sendNoOpTrackerRequest({
                    label: Log.NoOp.Label.WebClipperUiFrameIsNotVisible,
                    channel: constants_1.Constants.CommunicationChannels.injectedAndUi,
                    clientInfo: this.clientInfo,
                    url: url
                }, true);
            }
        }
        // No-op tracker for communication with the UI
        var noOpTrackerTimeout = Log.ErrorUtils.setNoOpTrackerRequestTimeout({
            label: Log.NoOp.Label.InitializeCommunicator,
            channel: constants_1.Constants.CommunicationChannels.injectedAndUi,
            clientInfo: this.clientInfo,
            url: window.location.href
        });
        this.uiCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.noOpTracker, {
            param: new Date().getTime(),
            callback: function () {
                clearTimeout(noOpTrackerTimeout);
            }
        });
    };
    ClipperInject.prototype.createFrame = function () {
        this.frame = styledFrameFactory_1.StyledFrameFactory.getStyledFrame(styledFrameFactory_1.Frame.WebClipper);
        this.frame.id = constants_1.Constants.Ids.clipperUiFrame;
        this.frame.src = this.options.frameUrl;
    };
    ClipperInject.prototype.handleConstructorError = function (e) {
        Log.ErrorUtils.sendFailureLogRequest({
            label: Log.Failure.Label.UnhandledExceptionThrown,
            properties: {
                failureType: Log.Failure.Type.Unexpected,
                failureInfo: { error: JSON.stringify({ error: e.toString(), url: window.location.href }) },
                failureId: "InjectBase",
                stackTrace: Log.Failure.getStackTrace(e)
            },
            clientInfo: this.clientInfo
        });
    };
    ClipperInject.prototype.init = function () {
        this.clientInfo = new smartValue_1.SmartValue();
        this.pageInfo = new smartValue_1.SmartValue();
        this.isFullScreen = new smartValue_1.SmartValue(false);
    };
    ClipperInject.prototype.initializeExtCommunicator = function (extMessageHandlerThunk) {
        var _this = this;
        this.extCommunicator = new communicator_1.Communicator(extMessageHandlerThunk(), constants_1.Constants.CommunicationChannels.injectedAndExtension);
        // Clear the extension no-op tracker
        this.extCommunicator.registerFunction(constants_1.Constants.FunctionKeys.noOpTracker, function (trackerStartTime) {
            var clearNoOpTrackerEvent = new Log.Event.BaseEvent(Log.Event.Label.ClearNoOpTracker);
            clearNoOpTrackerEvent.setCustomProperty(Log.PropertyName.Custom.TimeToClearNoOpTracker, new Date().getTime() - trackerStartTime);
            clearNoOpTrackerEvent.setCustomProperty(Log.PropertyName.Custom.Channel, constants_1.Constants.CommunicationChannels.injectedAndExtension);
            _this.logger.logEvent(clearNoOpTrackerEvent);
            return Promise.resolve();
        });
        this.extCommunicator.setErrorHandler(function (e) {
            _this.uiCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.showRefreshClipperMessage, {
                param: "Communicator " + constants_1.Constants.CommunicationChannels.injectedAndExtension + " caught an error: " + e.message
            });
            // Orphaned web clippers are a handled communicator error, so no further handling is needed here
        });
        this.extCommunicator.subscribeAcrossCommunicator(this.clientInfo, constants_1.Constants.SmartValueKeys.clientInfo);
    };
    ClipperInject.prototype.initializeEventListeners = function () {
        var _this = this;
        // Preserve any onkeydown events the page might have set
        var oldOnKeyDown = document.onkeydown;
        document.onkeydown = function (event) {
            if (event.keyCode === constants_1.Constants.KeyCodes.esc) {
                _this.uiCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.escHandler);
            }
            if (oldOnKeyDown) {
                oldOnKeyDown.call(document, event);
            }
        };
        // Notify the background when we're unloading
        window.onbeforeunload = function (event) {
            _this.extCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.unloadHandler);
        };
        // On single-page-app 'navigates' we want to be able to toggle off the Clipper. We don't destroy it completely as it causes
        // the extension to be in a weird state.
        // See: http://stackoverflow.com/questions/4570093/how-to-get-notified-about-changes-of-the-history-via-history-pushstate
        var history = window.history;
        history.pushState = function (state) {
            if (typeof history.onpushstate === "function") {
                history.onpushstate({ state: state });
            }
            return history.pushState.apply(history, arguments);
        };
        window.onpopstate = history.onpushstate = function (event) {
            _this.uiCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.onSpaNavigate);
        };
        // SPF is a SPA framework that some sites use, e.g., youtube. It sends an spfdone event when the page has 'navigated'.
        document.addEventListener("spfdone", function () {
            _this.uiCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.onSpaNavigate);
        });
    };
    ClipperInject.prototype.initializePassthroughCommunicator = function (extMessageHandlerThunk) {
        var _this = this;
        if (!this.options.useInlineBackgroundWorker) {
            // Create a link for the extension to pass communication to the ui without having to re-register all the functions, smartValues, etc.
            var passthroughCommunicator = new communicatorPassthrough_1.CommunicatorPassthrough(extMessageHandlerThunk(), new iframeMessageHandler_1.IFrameMessageHandler(function () { return _this.frame.contentWindow; }), constants_1.Constants.CommunicationChannels.extensionAndUi, function (e) {
                _this.uiCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.showRefreshClipperMessage, {
                    param: "Communicator " + constants_1.Constants.CommunicationChannels.extensionAndUi + " caught an error: " + e.message
                });
                Log.ErrorUtils.handleCommunicatorError(constants_1.Constants.CommunicationChannels.extensionAndUi, e, _this.clientInfo);
            });
        }
    };
    ClipperInject.prototype.initializeUiCommunicator = function () {
        var _this = this;
        this.uiCommunicator = new communicator_1.Communicator(new iframeMessageHandler_1.IFrameMessageHandler(function () { return _this.frame.contentWindow; }), constants_1.Constants.CommunicationChannels.injectedAndUi);
        this.uiCommunicator.setErrorHandler(function (e) {
            Log.ErrorUtils.handleCommunicatorError(constants_1.Constants.CommunicationChannels.injectedAndUi, e, _this.clientInfo);
        });
        this.uiCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.setInjectOptions, { param: this.options });
        this.uiCommunicator.broadcastAcrossCommunicator(this.pageInfo, constants_1.Constants.SmartValueKeys.pageInfo);
        this.uiCommunicator.subscribeAcrossCommunicator(this.isFullScreen, constants_1.Constants.SmartValueKeys.isFullScreen, function () {
            _this.updateUiSizeAttributes();
        });
        this.uiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.updateFrameHeight, function (newHeight) {
            _this.frameHeight = newHeight + constants_1.Constants.Styles.clipperUiTopRightOffset + (constants_1.Constants.Styles.clipperUiDropShadowBuffer * 2);
            _this.updateUiSizeAttributes();
        });
        this.uiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.updatePageInfoIfUrlChanged, function () {
            if (document.URL !== _this.pageInfo.get().rawUrl) {
                _this.updatePageInfo();
            }
            return Promise.resolve();
        });
        this.uiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.hideUi, function () {
            _this.frame.style.display = "none";
        });
        this.uiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.refreshPage, function () {
            location.reload();
        });
    };
    /**
     * Creates a PageInfo object from the document data
     */
    ClipperInject.prototype.createPageInfo = function () {
        var contentType = domUtils_1.DomUtils.getPageContentType(document);
        return {
            canonicalUrl: domUtils_1.DomUtils.fetchCanonicalUrl(document),
            contentData: this.getTrimmedDomString(),
            contentLocale: domUtils_1.DomUtils.getLocale(document),
            contentTitle: (contentType === OneNoteApi.ContentType.EnhancedUrl) ? domUtils_1.DomUtils.getFileNameFromUrl(document) : document.title,
            contentType: contentType,
            rawUrl: document.URL
        };
    };
    ClipperInject.prototype.getTrimmedDomString = function () {
        var maxPartLength = 2097152;
        var cleanDomEvent = new Log.Event.BaseEvent(Log.Event.Label.GetCleanDom);
        var fullDomString = domUtils_1.DomUtils.getCleanDomOfCurrentPage(document);
        var totalBytes = domUtils_1.DomUtils.getByteSize(fullDomString);
        cleanDomEvent.setCustomProperty(Log.PropertyName.Custom.DomSizeInBytes, totalBytes);
        var trimmedString = domUtils_1.DomUtils.truncateStringToByteSize(fullDomString, maxPartLength);
        var trimmedBytes = domUtils_1.DomUtils.getByteSize(trimmedString);
        if (trimmedBytes !== totalBytes) {
            cleanDomEvent.setCustomProperty(Log.PropertyName.Custom.BytesTrimmed, totalBytes - trimmedBytes);
        }
        this.logger.logEvent(cleanDomEvent);
        return trimmedString;
    };
    ClipperInject.prototype.overrideTransformStyles = function (element) {
        var parentStyles = window.getComputedStyle(element);
        if (parentStyles) {
            if (parentStyles.transform !== "none" || parentStyles.webkitTransform !== "none") {
                element.style.transform = element.style.webkitTransform = "none";
            }
        }
    };
    ClipperInject.prototype.sendInvokeOptionsToUi = function (invokeOptions) {
        this.uiCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.setInvokeOptions, {
            param: invokeOptions
        });
    };
    ClipperInject.prototype.toggleClipper = function () {
        if (this.frame.style.display === "none") {
            this.frame.style.display = "";
        }
        this.uiCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.toggleClipper);
    };
    ClipperInject.prototype.toScrubbedHtml = function (content) {
        var divContainer = document.createElement("div");
        divContainer.innerHTML = domUtils_1.DomUtils.cleanHtml(content);
        return divContainer.outerHTML;
    };
    ClipperInject.prototype.updatePageInfo = function () {
        this.pageInfo.set(this.createPageInfo());
    };
    ClipperInject.prototype.updateUiSizeAttributes = function () {
        if (this.isFullScreen.get()) {
            this.frame.style.width = "100%";
            this.frame.style.height = "100%";
        }
        else {
            this.frame.style.width = constants_1.Constants.Styles.clipperUiWidth + constants_1.Constants.Styles.clipperUiDropShadowBuffer + constants_1.Constants.Styles.clipperUiTopRightOffset + "px";
            this.frame.style.height = this.frameHeight + "px";
        }
    };
    return ClipperInject;
}(frameInjectBase_1.FrameInjectBase));
exports.ClipperInject = ClipperInject;
