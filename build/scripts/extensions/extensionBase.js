"use strict";
var clientType_1 = require("../clientType");
var constants_1 = require("../constants");
var experiments_1 = require("../experiments");
var stringUtils_1 = require("../stringUtils");
var urlUtils_1 = require("../urlUtils");
var tooltipType_1 = require("../clipperUI/tooltipType");
var smartValue_1 = require("../communicator/smartValue");
var HttpWithRetries_1 = require("../http/HttpWithRetries");
var localization_1 = require("../localization/localization");
var localizationHelper_1 = require("../localization/localizationHelper");
var Log = require("../logging/log");
var clipperStorageKeys_1 = require("../storage/clipperStorageKeys");
var changeLog_1 = require("../versioning/changeLog");
var changeLogHelper_1 = require("../versioning/changeLogHelper");
var version_1 = require("../versioning/version");
var authenticationHelper_1 = require("./authenticationHelper");
var tooltipHelper_1 = require("./tooltipHelper");
var workerPassthroughLogger_1 = require("./workerPassthroughLogger");
/**
 * The abstract base class for all of the extensions
 */
var ExtensionBase = (function () {
    function ExtensionBase(clipperType, clipperData) {
        this.setUnhandledExceptionLogging();
        this.workers = [];
        this.logger = new workerPassthroughLogger_1.WorkerPassthroughLogger(this.workers);
        ExtensionBase.extensionId = stringUtils_1.StringUtils.generateGuid();
        this.clipperData = clipperData;
        this.clipperData.setLogger(this.logger);
        this.auth = new authenticationHelper_1.AuthenticationHelper(this.clipperData, this.logger);
        this.tooltip = new tooltipHelper_1.TooltipHelper(this.clipperData);
        var clipperFirstRun = false;
        var clipperId = this.clipperData.getValue(clipperStorageKeys_1.ClipperStorageKeys.clipperId);
        if (!clipperId) {
            // New install
            clipperFirstRun = true;
            clipperId = ExtensionBase.generateClipperId();
            this.clipperData.setValue(clipperStorageKeys_1.ClipperStorageKeys.clipperId, clipperId);
            // Ensure fresh installs don't trigger thats What's New experience
            this.updateLastSeenVersionInStorageToCurrent();
        }
        this.clientInfo = new smartValue_1.SmartValue({
            clipperType: clipperType,
            clipperVersion: ExtensionBase.getExtensionVersion(),
            clipperId: clipperId
        });
        if (clipperFirstRun) {
            this.onFirstRun();
        }
        this.initializeUserFlighting();
        this.listenForOpportunityToShowPageNavTooltip();
    }
    ExtensionBase.getExtensionId = function () {
        return ExtensionBase.extensionId;
    };
    ExtensionBase.getExtensionVersion = function () {
        return ExtensionBase.version;
    };
    ExtensionBase.shouldCheckForMajorUpdates = function (lastSeenVersion, currentVersion) {
        return !!currentVersion && (!lastSeenVersion || lastSeenVersion.isLesserThan(currentVersion));
    };
    ;
    ExtensionBase.prototype.addWorker = function (worker) {
        var _this = this;
        worker.setOnUnloading(function () {
            worker.destroy();
            _this.removeWorker(worker);
        });
        this.workers.push(worker);
    };
    ExtensionBase.prototype.getWorkers = function () {
        return this.workers;
    };
    ExtensionBase.prototype.removeWorker = function (worker) {
        var index = this.workers.indexOf(worker);
        if (index > -1) {
            this.workers.splice(index, 1);
        }
    };
    /**
     * Determines if the url is on our domain or not
     */
    ExtensionBase.isOnOneNoteDomain = function (url) {
        return true;
    };
    ExtensionBase.prototype.fetchAndStoreLocStrings = function () {
        var _this = this;
        // navigator.userLanguage is only available in IE, and Typescript will not recognize this property
        var locale = navigator.language || navigator.userLanguage;
        return localizationHelper_1.LocalizationHelper.makeLocStringsFetchRequest(locale).then(function (responsePackage) {
            try {
                var locStringsDict = JSON.parse(responsePackage.parsedResponse);
                if (locStringsDict) {
                    _this.clipperData.setValue(clipperStorageKeys_1.ClipperStorageKeys.locale, locale);
                    _this.clipperData.setValue(clipperStorageKeys_1.ClipperStorageKeys.locStrings, responsePackage.parsedResponse);
                    localization_1.Localization.setLocalizedStrings(locStringsDict);
                }
                return Promise.resolve(locStringsDict);
            }
            catch (e) {
                return Promise.reject(undefined);
            }
        });
    };
    /**
     * Returns the URL for more information about the Clipper
     */
    ExtensionBase.prototype.getClipperInstalledPageUrl = function (clipperId, clipperType, isInlineInstall) {
        var installUrl = constants_1.Constants.Urls.clipperInstallPageUrl;
        installUrl = urlUtils_1.UrlUtils.addUrlQueryValue(installUrl, constants_1.Constants.Urls.QueryParams.clientType, clientType_1.ClientType[clipperType]);
        installUrl = urlUtils_1.UrlUtils.addUrlQueryValue(installUrl, constants_1.Constants.Urls.QueryParams.clipperId, clipperId);
        installUrl = urlUtils_1.UrlUtils.addUrlQueryValue(installUrl, constants_1.Constants.Urls.QueryParams.clipperVersion, ExtensionBase.getExtensionVersion());
        installUrl = urlUtils_1.UrlUtils.addUrlQueryValue(installUrl, constants_1.Constants.Urls.QueryParams.inlineInstall, isInlineInstall.toString());
        this.logger.logTrace(Log.Trace.Label.RequestForClipperInstalledPageUrl, Log.Trace.Level.Information, installUrl);
        return installUrl;
    };
    ExtensionBase.prototype.getExistingWorkerForTab = function (tabUniqueId) {
        var workers = this.getWorkers();
        for (var _i = 0, workers_1 = workers; _i < workers_1.length; _i++) {
            var worker = workers_1[_i];
            if (worker.getUniqueId() === tabUniqueId) {
                return worker;
            }
        }
        return undefined;
    };
    /**
     * Gets the last seen version from storage, and returns undefined if there is none in storage
     */
    ExtensionBase.prototype.getLastSeenVersion = function () {
        var lastSeenVersionStr = this.clipperData.getValue(clipperStorageKeys_1.ClipperStorageKeys.lastSeenVersion);
        return lastSeenVersionStr ? new version_1.Version(lastSeenVersionStr) : undefined;
    };
    ExtensionBase.prototype.getNewUpdates = function (lastSeenVersion, currentVersion) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var localeOverride = _this.clipperData.getValue(clipperStorageKeys_1.ClipperStorageKeys.displayLanguageOverride);
            var localeToGet = localeOverride || navigator.language || navigator.userLanguage;
            var changelogUrl = urlUtils_1.UrlUtils.addUrlQueryValue(constants_1.Constants.Urls.changelogUrl, constants_1.Constants.Urls.QueryParams.changelogLocale, localeToGet);
            HttpWithRetries_1.HttpWithRetries.get(changelogUrl).then(function (request) {
                try {
                    var schemas = JSON.parse(request.responseText);
                    var allUpdates = void 0;
                    for (var i = 0; i < schemas.length; i++) {
                        if (schemas[i].schemaVersion === changeLog_1.ChangeLog.schemaVersionSupported) {
                            allUpdates = schemas[i].updates;
                            break;
                        }
                    }
                    if (allUpdates) {
                        var updatesSinceLastVersion = changeLogHelper_1.ChangeLogHelper.getUpdatesBetweenVersions(allUpdates, lastSeenVersion, currentVersion);
                        resolve(updatesSinceLastVersion);
                    }
                    else {
                        throw new Error("No matching schemas were found.");
                    }
                }
                catch (error) {
                    reject(error);
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    ExtensionBase.prototype.getOrCreateWorkerForTab = function (tab, tabToIdMapping) {
        var tabId = tabToIdMapping(tab);
        var worker = this.getExistingWorkerForTab(tabId);
        if (!worker) {
            worker = this.createWorker(tab);
            this.addWorker(worker);
        }
        return worker;
    };
    /**
     * Generates a new clipperId, should only be called on first run
     */
    ExtensionBase.generateClipperId = function () {
        var clipperPrefix = "ON";
        return clipperPrefix + "-" + stringUtils_1.StringUtils.generateGuid();
    };
    /**
     * Initializes the flighting info for the user.
     */
    ExtensionBase.prototype.initializeUserFlighting = function () {
        var _this = this;
        var getFlightingEvent = new Log.Event.PromiseEvent(Log.Event.Label.GetFlightingAssignments);
        this.getFlightingAssignments(this.clientInfo.get().clipperId).then(function (flights) {
            _this.updateClientInfoWithFlightInformation(flights);
        })["catch"](function (error) {
            _this.updateClientInfoWithFlightInformation([]);
            getFlightingEvent.setStatus(Log.Status.Failed);
            getFlightingEvent.setFailureInfo(error);
        }).then(function () {
            _this.logger.logEvent(getFlightingEvent);
        });
    };
    /**
     * Returns the current flighting assignment for the user
     */
    ExtensionBase.prototype.getFlightingAssignments = function (clipperId) {
        var fetchNonLocalData = function () {
            return new Promise(function (resolve, reject) {
                var userFlightUrl = urlUtils_1.UrlUtils.addUrlQueryValue(constants_1.Constants.Urls.userFlightingEndpoint, constants_1.Constants.Urls.QueryParams.clipperId, clipperId);
                HttpWithRetries_1.HttpWithRetries.get(userFlightUrl).then(function (request) {
                    resolve({
                        request: request,
                        parsedResponse: request.responseText
                    });
                });
            });
        };
        return this.clipperData.getFreshValue(clipperStorageKeys_1.ClipperStorageKeys.flightingInfo, fetchNonLocalData, experiments_1.Experiments.updateIntervalForFlights).then(function (successfulResponse) {
            // The response comes as a string array in the form [flight1, flight2, flight3],
            // needs to be in CSV format for ODIN cooker, so we rejoin w/o spaces
            var parsedResponse = successfulResponse.data.Features ? successfulResponse.data.Features : [];
            return Promise.resolve(parsedResponse);
        });
    };
    ExtensionBase.prototype.shouldShowTooltip = function (tab, tooltipTypes) {
        var type = this.checkIfTabMatchesATooltipType(tab, tooltipTypes);
        if (!type) {
            return;
        }
        if (!this.tooltip.tooltipDelayIsOver(type, Date.now())) {
            return;
        }
        return type;
    };
    ExtensionBase.prototype.shouldShowVideoTooltip = function (tab) {
        if (this.checkIfTabIsAVideoDomain(tab) && this.tooltip.tooltipDelayIsOver(tooltipType_1.TooltipType.Video, Date.now())) {
            return true;
        }
        return false;
    };
    ExtensionBase.prototype.showTooltip = function (tab, tooltipType) {
        var _this = this;
        var worker = this.getOrCreateWorkerForTab(tab, this.getIdFromTab);
        var tooltipImpressionEvent = new Log.Event.BaseEvent(Log.Event.Label.TooltipImpression);
        tooltipImpressionEvent.setCustomProperty(Log.PropertyName.Custom.TooltipType, tooltipType_1.TooltipType[tooltipType]);
        tooltipImpressionEvent.setCustomProperty(Log.PropertyName.Custom.LastSeenTooltipTime, this.tooltip.getTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.lastSeenTooltipTimeBase, tooltipType));
        tooltipImpressionEvent.setCustomProperty(Log.PropertyName.Custom.NumTimesTooltipHasBeenSeen, this.tooltip.getTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.numTimesTooltipHasBeenSeenBase, tooltipType));
        worker.invokeTooltip(tooltipType).then(function (wasInvoked) {
            if (wasInvoked) {
                _this.tooltip.setTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.lastSeenTooltipTimeBase, tooltipType, Date.now().toString());
                var numSeenStorageKey = clipperStorageKeys_1.ClipperStorageKeys.numTimesTooltipHasBeenSeenBase;
                var numTimesSeen = _this.tooltip.getTooltipInformation(numSeenStorageKey, tooltipType) + 1;
                _this.tooltip.setTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.numTimesTooltipHasBeenSeenBase, tooltipType, numTimesSeen.toString());
            }
            tooltipImpressionEvent.setCustomProperty(Log.PropertyName.Custom.FeatureEnabled, wasInvoked);
            worker.getLogger().logEvent(tooltipImpressionEvent);
        });
    };
    ExtensionBase.prototype.shouldShowWhatsNewTooltip = function (tab, lastSeenVersion, extensionVersion) {
        // We explicitly check for control group as well so we prevent updating lastSeenVersion on everyone before the experiment starts
        return this.checkIfTabIsOnWhitelistedUrl(tab) && ExtensionBase.shouldCheckForMajorUpdates(lastSeenVersion, extensionVersion);
    };
    ExtensionBase.prototype.showWhatsNewTooltip = function (tab, lastSeenVersion, extensionVersion) {
        var _this = this;
        this.getNewUpdates(lastSeenVersion, extensionVersion).then(function (newUpdates) {
            var filteredUpdates = changeLogHelper_1.ChangeLogHelper.filterUpdatesThatDontApplyToBrowser(newUpdates, clientType_1.ClientType[_this.clientInfo.get().clipperType]);
            if (!!filteredUpdates && filteredUpdates.length > 0) {
                var worker_1 = _this.getOrCreateWorkerForTab(tab, _this.getIdFromTab);
                worker_1.invokeWhatsNewTooltip(filteredUpdates).then(function (wasInvoked) {
                    if (wasInvoked) {
                        var whatsNewImpressionEvent = new Log.Event.BaseEvent(Log.Event.Label.WhatsNewImpression);
                        whatsNewImpressionEvent.setCustomProperty(Log.PropertyName.Custom.FeatureEnabled, wasInvoked);
                        worker_1.getLogger().logEvent(whatsNewImpressionEvent);
                        // We don't want to do this if the tooltip was not invoked (e.g., on about:blank) so we can show it at the next opportunity
                        _this.updateLastSeenVersionInStorageToCurrent();
                    }
                });
            }
            else {
                _this.updateLastSeenVersionInStorageToCurrent();
            }
        }, function (error) {
            Log.ErrorUtils.sendFailureLogRequest({
                label: Log.Failure.Label.GetChangeLog,
                properties: {
                    failureType: Log.Failure.Type.Unexpected,
                    failureInfo: { error: error },
                    failureId: "GetChangeLog",
                    stackTrace: error
                },
                clientInfo: _this.clientInfo
            });
        });
    };
    /**
     * Skeleton method that sets a listener that listens for the opportunity to show a tooltip,
     * then invokes it when appropriate.
     */
    ExtensionBase.prototype.listenForOpportunityToShowPageNavTooltip = function () {
        var _this = this;
        this.addPageNavListener(function (tab) {
            // Fallback behavior for if the last seen version in storage is somehow in a corrupted format
            var lastSeenVersion;
            try {
                lastSeenVersion = _this.getLastSeenVersion();
            }
            catch (e) {
                _this.updateLastSeenVersionInStorageToCurrent();
                return;
            }
            var extensionVersion = new version_1.Version(ExtensionBase.getExtensionVersion());
            if (_this.clientInfo.get().clipperType !== clientType_1.ClientType.FirefoxExtension) {
                var tooltips = [tooltipType_1.TooltipType.Pdf, tooltipType_1.TooltipType.Product, tooltipType_1.TooltipType.Recipe];
                // Returns the Type of tooltip to show IF the delay is over and the tab has the correct content type
                var typeToShow = _this.shouldShowTooltip(tab, tooltips);
                if (typeToShow) {
                    _this.showTooltip(tab, typeToShow);
                    return;
                }
                if (_this.shouldShowVideoTooltip(tab)) {
                    _this.showTooltip(tab, tooltipType_1.TooltipType.Video);
                    return;
                }
            }
            // We don't show updates more recent than the local version for now, as it is easy
            // for a changelog to be released before a version is actually out
            if (_this.shouldShowWhatsNewTooltip(tab, lastSeenVersion, extensionVersion)) {
                _this.showWhatsNewTooltip(tab, lastSeenVersion, extensionVersion);
                return;
            }
        });
    };
    ExtensionBase.prototype.setUnhandledExceptionLogging = function () {
        var _this = this;
        var oldOnError = window.onerror;
        window.onerror = function (message, filename, lineno, colno, error) {
            var callStack = error ? Log.Failure.getStackTrace(error) : "[unknown stacktrace]";
            Log.ErrorUtils.sendFailureLogRequest({
                label: Log.Failure.Label.UnhandledExceptionThrown,
                properties: {
                    failureType: Log.Failure.Type.Unexpected,
                    failureInfo: { error: JSON.stringify({ error: error.toString(), message: message, lineno: lineno, colno: colno }) },
                    failureId: "ExtensionBase",
                    stackTrace: callStack
                },
                clientInfo: _this.clientInfo
            });
            if (oldOnError) {
                oldOnError(message, filename, lineno, colno, error);
            }
        };
    };
    /**
     * Updates the ClientInfo with the given flighting info.
     */
    ExtensionBase.prototype.updateClientInfoWithFlightInformation = function (flightingInfo) {
        this.clientInfo.set({
            clipperType: this.clientInfo.get().clipperType,
            clipperVersion: this.clientInfo.get().clipperVersion,
            clipperId: this.clientInfo.get().clipperId,
            flightingInfo: flightingInfo
        });
    };
    ExtensionBase.prototype.updateLastSeenVersionInStorageToCurrent = function () {
        this.clipperData.setValue(clipperStorageKeys_1.ClipperStorageKeys.lastSeenVersion, ExtensionBase.getExtensionVersion());
    };
    return ExtensionBase;
}());
ExtensionBase.version = "3.8.0";
exports.ExtensionBase = ExtensionBase;
