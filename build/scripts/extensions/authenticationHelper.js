"use strict";
var smartValue_1 = require("../communicator/smartValue");
var Log = require("../logging/log");
var clipperStorageKeys_1 = require("../storage/clipperStorageKeys");
var userInfo_1 = require("../userInfo");
var constants_1 = require("../constants");
var objectUtils_1 = require("../objectUtils");
var stringUtils_1 = require("../stringUtils");
var urlUtils_1 = require("../urlUtils");
var AuthenticationHelper = (function () {
    function AuthenticationHelper(clipperData, logger) {
        this.user = new smartValue_1.SmartValue();
        this.logger = logger;
        this.clipperData = clipperData;
    }
    /**
     * Updates the current user's information.
     */
    AuthenticationHelper.prototype.updateUserInfoData = function (clipperId, updateReason) {
        var _this = this;
        return new Promise(function (resolve) {
            var updateInterval = 0;
            var storedUserInformation = _this.clipperData.getValue(clipperStorageKeys_1.ClipperStorageKeys.userInformation);
            if (storedUserInformation) {
                var currentInfo = void 0;
                try {
                    currentInfo = JSON.parse(storedUserInformation);
                }
                catch (e) {
                    _this.logger.logJsonParseUnexpected(storedUserInformation);
                }
                if (currentInfo && currentInfo.data && objectUtils_1.ObjectUtils.isNumeric(currentInfo.data.accessTokenExpiration)) {
                    // Expiration is in seconds, not milliseconds. Give additional leniency to account for response time.
                    updateInterval = Math.max((currentInfo.data.accessTokenExpiration * 1000) - 180000, 0);
                }
            }
            var getUserInformationFunction = function () {
                return _this.retrieveUserInformation(clipperId, undefined);
            };
            var getInfoEvent = new Log.Event.PromiseEvent(Log.Event.Label.GetExistingUserInformation);
            getInfoEvent.setCustomProperty(Log.PropertyName.Custom.UserInformationStored, !!storedUserInformation);
            _this.clipperData.getFreshValue(clipperStorageKeys_1.ClipperStorageKeys.userInformation, getUserInformationFunction, updateInterval).then(function (response) {
                var isValidUser = _this.isValidUserInformation(response.data);
                getInfoEvent.setCustomProperty(Log.PropertyName.Custom.FreshUserInfoAvailable, isValidUser);
                var writeableCookies = _this.isThirdPartyCookiesEnabled(response.data);
                getInfoEvent.setCustomProperty(Log.PropertyName.Custom.WriteableCookies, writeableCookies);
                getInfoEvent.setCustomProperty(Log.PropertyName.Custom.UserUpdateReason, userInfo_1.UpdateReason[updateReason]);
                if (isValidUser) {
                    _this.user.set({ user: response.data, lastUpdated: response.lastUpdated, updateReason: updateReason, writeableCookies: writeableCookies });
                }
                else {
                    _this.user.set({ updateReason: updateReason, writeableCookies: writeableCookies });
                }
            }, function (error) {
                getInfoEvent.setStatus(Log.Status.Failed);
                getInfoEvent.setFailureInfo(error);
                _this.user.set({ updateReason: updateReason });
            }).then(function () {
                _this.logger.logEvent(getInfoEvent);
                resolve(_this.user.get());
            });
        });
    };
    /**
     * Makes a call to the authentication proxy to retrieve the user's information.
     */
    AuthenticationHelper.prototype.retrieveUserInformation = function (clipperId, cookie) {
        if (cookie === void 0) { cookie = undefined; }
        return new Promise(function (resolve, reject) {
            var userInfoUrl = urlUtils_1.UrlUtils.addUrlQueryValue(constants_1.Constants.Urls.Authentication.userInformationUrl, constants_1.Constants.Urls.QueryParams.clipperId, clipperId);
            var retrieveUserInformationEvent = new Log.Event.PromiseEvent(Log.Event.Label.RetrieveUserInformation);
            var correlationId = stringUtils_1.StringUtils.generateGuid();
            retrieveUserInformationEvent.setCustomProperty(Log.PropertyName.Custom.CorrelationId, correlationId);
            var headers = {};
            headers["Content-type"] = "application/x-www-form-urlencoded";
            headers[constants_1.Constants.HeaderValues.correlationId] = correlationId;
            var postData = "";
            if (!objectUtils_1.ObjectUtils.isNullOrUndefined(cookie)) {
                // The data is encoded/decoded automatically, but because the '+' sign can also be interpreted as a space, we want to explicitly encode this one.
                postData = cookie.replace(/\+/g, "%2B");
            }
            resolve({ parsedResponse: '{"accessToken":"EwAQA61DBAAUcSSzoTJJsy+XrnQXgAKO5cj4yc8AAf/sgrDRo7P0h+BylMY6DjYnrc7u5x8TM3I58GWhtBrhSGWkt1C6jRaYz3N2yNf685hyMggPqycnhiR50luMGyYzMjnbG7ZJNZ8ut7PfkJXRYWav3nLxPjt+ZVfyt2/LDretROGA+4fZ9fQPwBRxXnsqyDJnDHbGbXEC1DzvP+HV2Tt39aNrlQ/OPdQTm+YlseKhKQEnv5gyngOePrNYSw/uOsOgM1r1RNw594YcUVqhH5tH6L3TzqUfMAZqwtwiLr+/FZu5+H2/x6FszTs0VXqBagWThz1Zgsv48ogvmtr+NdTOW/VK+LD1CKrsG89YlqvhTXqIlp5tmr4Vv881qjkDZgAACKDcfNlKRTUu4AELlaAIn3VeAEeRNjvumkVptZ/hGbYxP6WfUyaUTAZ3Ok8W6GraD0MkqIXgw87O/Nl86c5/wY0F5jCCdYqJgvoiFpMAkd/nCfszgr0BZZucu7EbG993+smChL+bpYTWh9OszEATm+a7mq3OXrj13JRE5vyRdGRSlLLX5glgiFHe7CXD5uk7qx263zTKFbCtkDMl0iAM9LMp5kn2O/LPFbdCdwdlMwa2xQmZSiLzWZ1bdGDHnrpFNRPiCtMylfGh51cphVh0GAVKg9ALc+9//iar3KVfD7sDx/ZBBMyq/d76+N+8ij3HPPWsuYYUm190jzIwygSDSa2+NrRP4eN7LvCN2FinuTXyTHb/DiiDDZkWUQNcOwCyTLUZGukOGmtf4luNFnjOfeKX0nSVLPJjlOkxoOoljTsVFGiDkuuigC9Vy9vl7QmfkGwgoJr2X0ZD1hFcYIvpk8m7Wdv4Ow3jVjp9iQA6+c6OFOF7VmWNq2RgXq+yWoc3VB/KEU6HCDaDHnIV/AICKyKSlKy73xPnVmYZ9AEjPlLJtuzYFlDu/LVg/CwLRGvSlUu6fLwkKfwjdznrxnrW3Vt9Ex2PzKb6qamXxA11Aw8YBL3dlm6p78u1QCP/WtfrPtLAAalrsBe/9+YbAg==","accessTokenExpiration":3600,"authType":"Msa","cid":"d4df477bc2fc57d5","cookieInRequest":true,"emailAddress":"minhnvc@vng.com.vn","fullName":"Minh Nguyen","locale":"en_US"}', request: null });
        });
    };
    /**
     * Determines whether or not the given string is valid JSON and has the required elements.
     */
    AuthenticationHelper.prototype.isValidUserInformation = function (userInfo) {
        if (userInfo && userInfo.accessToken && userInfo.accessTokenExpiration > 0 && userInfo.authType) {
            return true;
        }
        return false;
    };
    /**
     * Determines whether or not the given string is valid JSON and has the flag which lets us know if cookies are enabled.
     */
    AuthenticationHelper.prototype.isThirdPartyCookiesEnabled = function (userInfo) {
        // Note that we are returning true by default to ensure the N-1 scenario.
        return userInfo.cookieInRequest !== undefined ? userInfo.cookieInRequest : true;
    };
    return AuthenticationHelper;
}());
exports.AuthenticationHelper = AuthenticationHelper;
