"use strict";
var clientType_1 = require("../../clientType");
var webExtension_1 = require("../webExtensionBase/webExtension");
webExtension_1.WebExtension.browser = browser;
var clipperBackground = new webExtension_1.WebExtension(clientType_1.ClientType.EdgeExtension, {
    debugLoggingInjectUrl: "edgeDebugLoggingInject.js",
    webClipperInjectUrl: "edgeInject.js",
    pageNavInjectUrl: "edgePageNavInject.js"
});
