"use strict";
var webExtensionDebugLoggingInject_1 = require("../webExtensionBase/webExtensionDebugLoggingInject");
var webExtension_1 = require("../webExtensionBase/webExtension");
var webExtensionMessageHandler_1 = require("../webExtensionBase/webExtensionMessageHandler");
webExtension_1.WebExtension.browser = browser;
webExtensionDebugLoggingInject_1.invoke({ extMessageHandlerThunk: function () { return new webExtensionMessageHandler_1.WebExtensionContentMessageHandler(); } });
