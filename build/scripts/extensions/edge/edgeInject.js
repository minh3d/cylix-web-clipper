"use strict";
var webExtensionInject_1 = require("../webExtensionBase/webExtensionInject");
var webExtension_1 = require("../webExtensionBase/webExtension");
var webExtensionMessageHandler_1 = require("../webExtensionBase/webExtensionMessageHandler");
webExtension_1.WebExtension.browser = browser;
webExtensionInject_1.invoke({
    frameUrl: frameUrl,
    enableAddANote: true,
    enableEditableTitle: true,
    enableRegionClipping: true,
    extMessageHandlerThunk: function () { return new webExtensionMessageHandler_1.WebExtensionContentMessageHandler(); }
});
