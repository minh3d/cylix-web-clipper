"use strict";
var constants_1 = require("../../constants");
var clipperInject_1 = require("../clipperInject");
var safariContext_1 = require("./safariContext");
var safariMessageHandler_1 = require("./safariMessageHandler");
/*
 * Since there is no way for a Safari Extension to inject scripts On Demand,
 * this code is injected into every page, and waits for a command from the extension
 * to execute the code
 */
// This is injected into every page loaded (including frames), but we only want to set the listener on the main document
if (window.top === window) {
    safari.self.addEventListener("message", function (event) {
        if (event.name === constants_1.Constants.FunctionKeys.invokeClipper) {
            var options = {
                frameUrl: event.message,
                enableAddANote: true,
                enableEditableTitle: true,
                enableRegionClipping: true,
                extMessageHandlerThunk: function () { return new safariMessageHandler_1.SafariContentMessageHandler(); }
            };
            clipperInject_1.ClipperInject.main(options);
        }
    });
    // Send 'what the user clicked on' back to the extension's contextmenu event
    document.addEventListener("contextmenu", function (event) {
        var objToSend;
        if (!window.getSelection().isCollapsed) {
            objToSend = {
                type: safariContext_1.ContextType.Selection,
                parameters: {}
            };
        }
        else if (event.target && event.target.nodeName === "IMG") {
            var node = event.target;
            objToSend = {
                type: safariContext_1.ContextType.Img,
                parameters: {
                    src: node.src
                }
            };
        }
        if (objToSend) {
            // You can only set one user info context object, and it can not be an object!
            safari.self.tab.setContextMenuEventUserInfo(event, JSON.stringify(objToSend));
        }
    }, false);
}
