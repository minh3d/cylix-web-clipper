"use strict";
var ContextType;
(function (ContextType) {
    ContextType[ContextType["Img"] = 0] = "Img";
    ContextType[ContextType["Selection"] = 1] = "Selection";
})(ContextType = exports.ContextType || (exports.ContextType = {}));
