"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var clientType_1 = require("../../clientType");
var urlUtils_1 = require("../../urlUtils");
var videoUtils_1 = require("../../domParsers/videoUtils");
var localization_1 = require("../../localization/localization");
var clipperData_1 = require("../../storage/clipperData");
var LocalStorage_1 = require("../../storage/LocalStorage");
var extensionBase_1 = require("../extensionBase");
var invokeOptions_1 = require("../invokeOptions");
var invokeSource_1 = require("../invokeSource");
var safariContext_1 = require("./safariContext");
var safariWorker_1 = require("./safariWorker");
var SafariExtension = (function (_super) {
    __extends(SafariExtension, _super);
    function SafariExtension() {
        var _this = _super.call(this, clientType_1.ClientType.SafariExtension, new clipperData_1.ClipperData(new LocalStorage_1.LocalStorage())) || this;
        // Listen for the Toolbar button it be invoked
        safari.application.addEventListener("command", function (event) {
            switch (event.command) {
                case "ClipperInvoker":
                    _this.invokeClipperInCurrentTab({ invokeSource: invokeSource_1.InvokeSource.ExtensionButton }, { invokeMode: invokeOptions_1.InvokeMode.Default });
                    break;
                case "ContextClipperInvoker":
                    _this.invokeClipperInCurrentTab({ invokeSource: invokeSource_1.InvokeSource.ContextMenu }, { invokeMode: invokeOptions_1.InvokeMode.Default });
                    break;
                case "ContextClipperInvokerWithImage":
                    _this.invokeClipperInCurrentTab({ invokeSource: invokeSource_1.InvokeSource.ContextMenu }, {
                        invokeDataForMode: _this.currentContextItemParameter.parameters.src,
                        invokeMode: invokeOptions_1.InvokeMode.ContextImage
                    });
                    break;
                case "ContextClipperInvokerWithSelection":
                    _this.invokeClipperInCurrentTab({ invokeSource: invokeSource_1.InvokeSource.ContextMenu }, {
                        invokeMode: invokeOptions_1.InvokeMode.ContextTextSelection
                    });
                    break;
                default:
                    break;
            }
        }, false);
        _this.registerContextMenuItems();
        return _this;
    }
    SafariExtension.getExtensionVersion = function () {
        return safari.extension.bundleVersion;
    };
    SafariExtension.prototype.addPageNavListener = function (callback) {
        safari.application.addEventListener("navigate", function (event) {
            callback(event.target);
        }, true);
    };
    SafariExtension.prototype.checkIfTabIsOnWhitelistedUrl = function (tab) {
        return !urlUtils_1.UrlUtils.onBlacklistedDomain(tab.url) && urlUtils_1.UrlUtils.onWhitelistedDomain(tab.url);
    };
    SafariExtension.prototype.createWorker = function (tab) {
        return new safariWorker_1.SafariWorker(tab, this.clientInfo, this.auth);
    };
    SafariExtension.prototype.getIdFromTab = function (tab) {
        return tab;
    };
    SafariExtension.prototype.onFirstRun = function () {
        // Send users to our installed page (redirect if they're already on our page, else open a new tab)
        var installUrl = this.getClipperInstalledPageUrl(this.clientInfo.get().clipperId, this.clientInfo.get().clipperType, false);
        safari.application.activeBrowserWindow.activeTab.url = installUrl;
    };
    SafariExtension.prototype.checkIfTabMatchesATooltipType = function (tab, tooltipTypes) {
        if (urlUtils_1.UrlUtils.onBlacklistedDomain(tab.url)) {
            return undefined;
        }
        return urlUtils_1.UrlUtils.checkIfUrlMatchesAContentType(tab.url, tooltipTypes);
    };
    SafariExtension.prototype.checkIfTabIsAVideoDomain = function (tab) {
        return !!videoUtils_1.VideoUtils.videoDomainIfSupported(tab.url);
    };
    SafariExtension.prototype.invokeClipperInCurrentTab = function (invokeInfo, options) {
        var activeTab = safari.application.activeBrowserWindow.activeTab;
        var worker = this.getOrCreateWorkerForTab(activeTab, this.getIdFromTab);
        worker.closeAllFramesAndInvokeClipper(invokeInfo, options);
    };
    /**
     * A contextmenu event is fired when the user right-clicks, and we have to add the relevant buttons
     * programmatically depending on what we think the user right-clicked on
     */
    SafariExtension.prototype.registerContextMenuItems = function () {
        var _this = this;
        // We need to clear old context item parameter data from any previous right-clicks
        this.currentContextItemParameter = undefined;
        this.fetchAndStoreLocStrings().then(function () {
            safari.application.addEventListener("contextmenu", function (event) {
                if (event.userInfo) {
                    // User info was passed from the inject script. If we can do something 'interesting' e.g., invoking the
                    // Clipper with a specific image, we provide that option. Otherwise, provide the 'normal' invoke option.
                    _this.currentContextItemParameter = JSON.parse(event.userInfo);
                    if (_this.currentContextItemParameter.type === safariContext_1.ContextType.Img && _this.currentContextItemParameter.parameters.src) {
                        event.contextMenu.appendContextMenuItem("ContextClipperInvokerWithImage", localization_1.Localization.getLocalizedString("WebClipper.Label.ClipImageToOneNote"));
                    }
                    else if (_this.currentContextItemParameter.type === safariContext_1.ContextType.Selection) {
                        event.contextMenu.appendContextMenuItem("ContextClipperInvokerWithSelection", localization_1.Localization.getLocalizedString("WebClipper.Label.ClipSelectionToOneNote"));
                    }
                    else {
                        _this.registerSimpleInvokeContextMenuItem(event);
                    }
                }
                else {
                    // No user info was passed from the inject script, so we assume it's just an arbitrary right-click on some
                    // point in the page
                    _this.registerSimpleInvokeContextMenuItem(event);
                }
            }, false);
        });
    };
    /**
     * Register the context menu item which the user can use to invoke the Clipper in 'normal' mode
     */
    SafariExtension.prototype.registerSimpleInvokeContextMenuItem = function (event) {
        event.contextMenu.appendContextMenuItem("ContextClipperInvoker", localization_1.Localization.getLocalizedString("WebClipper.Label.OneNoteWebClipper"));
    };
    return SafariExtension;
}(extensionBase_1.ExtensionBase));
exports.SafariExtension = SafariExtension;
var safariExtension = new SafariExtension();
