"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var userInfo_1 = require("../../userInfo");
var clipperUrls_1 = require("../../clipperUrls");
var constants_1 = require("../../constants");
var objectUtils_1 = require("../../objectUtils");
var urlUtils_1 = require("../../urlUtils");
var Log = require("../../logging/log");
var clipperData_1 = require("../../storage/clipperData");
var localStorage_1 = require("../../storage/localStorage");
var extensionWorkerBase_1 = require("../extensionWorkerBase");
var safariMessageHandler_1 = require("./safariMessageHandler");
var SafariWorker = (function (_super) {
    __extends(SafariWorker, _super);
    function SafariWorker(tab, clientInfo, auth) {
        var _this = this;
        var messageHandlerThunk = function () { return new safariMessageHandler_1.SafariBackgroundMessageHandler(tab); };
        _this = _super.call(this, clientInfo, auth, new clipperData_1.ClipperData(new localStorage_1.LocalStorage()), messageHandlerThunk, messageHandlerThunk) || this;
        // The safari browser tab does not have an id so we use the object itself
        _this.tab = tab;
        _this.tabId = tab;
        _this.noOpTrackerInvoked = false;
        _this.logger.setContextProperty(Log.Context.Custom.InPrivateBrowsing, tab.private.toString());
        _this.invokeDebugLoggingIfEnabled();
        return _this;
    }
    /**
     * Get the url associated with this worker's tab
     */
    SafariWorker.prototype.getUrl = function () {
        return this.tab.url;
    };
    /**
     * Launches the sign in window, rejecting with an error object if something went wrong on the server during
     * authentication. Otherwise, it resolves with true if the redirect endpoint was hit as a result of a successful
     * sign in attempt, and false if it was not hit (e.g., user manually closed the popup)
     */
    SafariWorker.prototype.doSignInAction = function (authType) {
        var usidQueryParamValue = this.getUserSessionIdQueryParamValue();
        var signInUrl = clipperUrls_1.ClipperUrls.generateSignInUrl(this.clientInfo.get().clipperId, usidQueryParamValue, userInfo_1.AuthType[authType]);
        return this.launchSafariPopup(signInUrl, constants_1.Constants.Urls.Authentication.authRedirectUrl);
    };
    /**
     * Signs the user out
     */
    SafariWorker.prototype.doSignOutAction = function (authType) {
        var usidQueryParamValue = this.getUserSessionIdQueryParamValue();
        var signOutUrl = clipperUrls_1.ClipperUrls.generateSignOutUrl(this.clientInfo.get().clipperId, usidQueryParamValue, userInfo_1.AuthType[authType]);
        // The signout doesn't work in an iframe in the Safari background page, so we need to launch a popup instead
        this.launchSafariPopup(signOutUrl);
    };
    /**
     * Notify the UI to invoke the clipper. Resolve with true if it was thought to be successfully
     * injected; otherwise resolves with false.
     */
    SafariWorker.prototype.invokeClipperBrowserSpecific = function () {
        this.tab.page.dispatchMessage(constants_1.Constants.FunctionKeys.invokeClipper, safari.extension.baseURI + "clipper.html");
        if (!this.noOpTrackerInvoked) {
            this.setUpNoOpTrackers(this.tab.url);
            this.noOpTrackerInvoked = true;
        }
        return Promise.resolve(true);
    };
    /**
     * Notify the UI to invoke the frontend script that handles logging to the conosle. Resolve with
     * true if it was thought to be successfully injected; otherwise resolves with false.
     */
    SafariWorker.prototype.invokeDebugLoggingBrowserSpecific = function () {
        this.tab.page.dispatchMessage(constants_1.Constants.FunctionKeys.invokeDebugLogging);
        return Promise.resolve(true);
    };
    SafariWorker.prototype.invokePageNavBrowserSpecific = function () {
        this.tab.page.dispatchMessage(constants_1.Constants.FunctionKeys.invokePageNav, safari.extension.baseURI + "pageNav.html");
        return Promise.resolve(true);
    };
    /**
     * Notify the UI to invoke the What's New tooltip. Resolve with true if it was thought to be successfully
     * injected; otherwise resolves with false.
     */
    SafariWorker.prototype.invokeWhatsNewTooltipBrowserSpecific = function (newVersions) {
        return this.invokePageNavBrowserSpecific();
    };
    SafariWorker.prototype.invokeTooltipBrowserSpecific = function () {
        return this.invokePageNavBrowserSpecific();
    };
    SafariWorker.prototype.isAllowedFileSchemeAccessBrowserSpecific = function (callback) {
        // When Safari opens a pdf, it is no longer a web environment and won't allow extensions to run
        if (this.tab.url.indexOf("file:///") === 0) {
            callback(false);
        }
        else {
            callback(true);
        }
    };
    /**
     * Gets the visible tab's screenshot as an image url
     */
    SafariWorker.prototype.takeTabScreenshot = function () {
        return new Promise(function (resolve) {
            // Note: To work around a safari bug, this may be synchronous or asynchronous
            // See http://stackoverflow.com/questions/13765174/weird-behaviour-of-visiblecontentsasdataurl
            var imageUrl = safari.application.activeBrowserWindow.activeTab.visibleContentsAsDataURL();
            if (imageUrl) {
                resolve(imageUrl);
            }
            else {
                safari.application.activeBrowserWindow.activeTab.visibleContentsAsDataURL(function (imageUrl2) {
                    resolve(imageUrl2);
                });
            }
        });
    };
    /**
     * Launches a new tab and navigates to the given url. If autoCloseDestinationUrl is defined, then a
     * listener is set that will wait until the given URL is navigated to, the window is closed.
     */
    SafariWorker.prototype.launchSafariPopup = function (url, autoCloseDestinationUrl) {
        if (autoCloseDestinationUrl === void 0) { autoCloseDestinationUrl = undefined; }
        return new Promise(function (resolve, reject) {
            var newWindow = safari.application.openBrowserWindow();
            var redirectOccurred = false;
            var errorObject;
            if (!objectUtils_1.ObjectUtils.isNullOrUndefined(autoCloseDestinationUrl)) {
                newWindow.addEventListener("navigate", function (event) {
                    if (event && event.target && event.target.url && !event.target.url.toLowerCase().indexOf(autoCloseDestinationUrl)) {
                        redirectOccurred = true;
                        var error = urlUtils_1.UrlUtils.getQueryValue(event.target.url, constants_1.Constants.Urls.QueryParams.error);
                        var errorDescription = urlUtils_1.UrlUtils.getQueryValue(event.target.url, constants_1.Constants.Urls.QueryParams.errorDescription);
                        if (error || errorDescription) {
                            errorObject = { error: error, errorDescription: errorDescription };
                        }
                        if (newWindow.visible) {
                            newWindow.close();
                        }
                    }
                });
                newWindow.addEventListener("close", function (event) {
                    errorObject ? reject(errorObject) : resolve(redirectOccurred);
                });
            }
            newWindow.activeTab.url = url;
        });
    };
    return SafariWorker;
}(extensionWorkerBase_1.ExtensionWorkerBase));
exports.SafariWorker = SafariWorker;
