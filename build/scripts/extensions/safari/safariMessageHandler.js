"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var messageHandler_1 = require("../../communicator/messageHandler");
var SafariBackgroundMessageHandler = (function (_super) {
    __extends(SafariBackgroundMessageHandler, _super);
    function SafariBackgroundMessageHandler(tabHandle) {
        var _this = _super.call(this) || this;
        _this.tabHandle = tabHandle;
        _this.initMessageHandler();
        _this.tabHandle.addEventListener("message", _this.messageHandler);
        return _this;
    }
    SafariBackgroundMessageHandler.prototype.initMessageHandler = function () {
        var _this = this;
        this.messageHandler = function (event) {
            _this.onMessageReceived(event.message);
        };
    };
    SafariBackgroundMessageHandler.prototype.sendMessage = function (data) {
        this.tabHandle.page.dispatchMessage("message", data);
    };
    SafariBackgroundMessageHandler.prototype.tearDown = function () {
        this.tabHandle.removeEventListener("message", this.messageHandler);
    };
    return SafariBackgroundMessageHandler;
}(messageHandler_1.MessageHandler));
exports.SafariBackgroundMessageHandler = SafariBackgroundMessageHandler;
var SafariContentMessageHandler = (function (_super) {
    __extends(SafariContentMessageHandler, _super);
    function SafariContentMessageHandler() {
        var _this = _super.call(this) || this;
        _this.initMessageHandler();
        safari.self.addEventListener("message", _this.messageHandler);
        return _this;
    }
    SafariContentMessageHandler.prototype.initMessageHandler = function () {
        var _this = this;
        this.messageHandler = function (event) {
            _this.onMessageReceived(event.message);
        };
    };
    SafariContentMessageHandler.prototype.sendMessage = function (data) {
        safari.self.tab.dispatchMessage("message", data);
    };
    SafariContentMessageHandler.prototype.tearDown = function () {
        safari.self.removeEventListener("message", this.messageHandler);
    };
    return SafariContentMessageHandler;
}(messageHandler_1.MessageHandler));
exports.SafariContentMessageHandler = SafariContentMessageHandler;
