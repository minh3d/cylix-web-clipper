"use strict";
var constants_1 = require("../../constants");
var debugLoggingInject_1 = require("../debugLoggingInject");
var safariMessageHandler_1 = require("./safariMessageHandler");
/*
 * Since there is no way for a Safari Extension to inject scripts On Demand,
 * this code is injected into every page, and waits for a command from the extension
 * to execute the code
 */
// This is injected into every page loaded (including frames), but we only want to set the listener on the main document
if (window.top === window) {
    safari.self.addEventListener("message", function (event) {
        if (event.name === constants_1.Constants.FunctionKeys.invokeDebugLogging) {
            var extMessageHandlerThunk = function () { return new safariMessageHandler_1.SafariContentMessageHandler(); };
            debugLoggingInject_1.DebugLoggingInject.main({ extMessageHandlerThunk: extMessageHandlerThunk });
        }
    });
}
