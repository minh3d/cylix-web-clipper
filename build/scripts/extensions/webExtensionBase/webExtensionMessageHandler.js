"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var messageHandler_1 = require("../../communicator/messageHandler");
var webExtension_1 = require("./webExtension");
var WebExtensionBackgroundMessageHandler = (function (_super) {
    __extends(WebExtensionBackgroundMessageHandler, _super);
    function WebExtensionBackgroundMessageHandler(tabId) {
        var _this = _super.call(this) || this;
        _this.tabId = tabId;
        _this.initMessageHandler();
        webExtension_1.WebExtension.browser.runtime.onMessage.addListener(_this.messageHandler);
        return _this;
    }
    WebExtensionBackgroundMessageHandler.prototype.initMessageHandler = function () {
        var _this = this;
        this.messageHandler = function (message, sender) {
            if (sender.tab.id === _this.tabId) {
                _this.onMessageReceived(message);
            }
        };
    };
    WebExtensionBackgroundMessageHandler.prototype.sendMessage = function (data) {
        webExtension_1.WebExtension.browser.tabs.sendMessage(this.tabId, data);
    };
    WebExtensionBackgroundMessageHandler.prototype.tearDown = function () {
        webExtension_1.WebExtension.browser.runtime.onMessage.removeListener(this.messageHandler);
    };
    return WebExtensionBackgroundMessageHandler;
}(messageHandler_1.MessageHandler));
exports.WebExtensionBackgroundMessageHandler = WebExtensionBackgroundMessageHandler;
var WebExtensionContentMessageHandler = (function (_super) {
    __extends(WebExtensionContentMessageHandler, _super);
    function WebExtensionContentMessageHandler() {
        var _this = _super.call(this) || this;
        _this.initMessageHandler();
        webExtension_1.WebExtension.browser.runtime.onMessage.addListener(_this.messageHandler);
        return _this;
    }
    WebExtensionContentMessageHandler.prototype.initMessageHandler = function () {
        var _this = this;
        this.messageHandler = function (message) {
            _this.onMessageReceived(message);
        };
    };
    WebExtensionContentMessageHandler.prototype.sendMessage = function (data) {
        webExtension_1.WebExtension.browser.runtime.sendMessage(data);
    };
    WebExtensionContentMessageHandler.prototype.tearDown = function () {
        webExtension_1.WebExtension.browser.runtime.onMessage.removeListener(this.messageHandler);
    };
    return WebExtensionContentMessageHandler;
}(messageHandler_1.MessageHandler));
exports.WebExtensionContentMessageHandler = WebExtensionContentMessageHandler;
