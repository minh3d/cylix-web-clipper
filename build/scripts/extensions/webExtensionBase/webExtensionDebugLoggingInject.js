"use strict";
var debugLoggingInject_1 = require("../debugLoggingInject");
function invoke(options) {
    debugLoggingInject_1.DebugLoggingInject.main(options);
}
exports.invoke = invoke;
