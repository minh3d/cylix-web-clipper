"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var userInfo_1 = require("../../userInfo");
var browserUtils_1 = require("../../browserUtils");
var clientType_1 = require("../../clientType");
var clipperUrls_1 = require("../../clipperUrls");
var constants_1 = require("../../constants");
var urlUtils_1 = require("../../urlUtils");
var Log = require("../../logging/log");
var clipperData_1 = require("../../storage/clipperData");
var LocalStorage_1 = require("../../storage/LocalStorage");
var extensionWorkerBase_1 = require("../extensionWorkerBase");
var injectHelper_1 = require("../injectHelper");
var webExtension_1 = require("./webExtension");
var webExtensionMessageHandler_1 = require("./webExtensionMessageHandler");
var WebExtensionWorker = (function (_super) {
    __extends(WebExtensionWorker, _super);
    function WebExtensionWorker(injectUrls, tab, clientInfo, auth) {
        var _this = this;
        var messageHandlerThunk = function () { return new webExtensionMessageHandler_1.WebExtensionBackgroundMessageHandler(tab.id); };
        _this = _super.call(this, clientInfo, auth, new clipperData_1.ClipperData(new LocalStorage_1.LocalStorage()), messageHandlerThunk, messageHandlerThunk) || this;
        _this.injectUrls = injectUrls;
        _this.tab = tab;
        _this.tabId = tab.id;
        _this.noOpTrackerInvoked = false;
        var isPrivateWindow = !!tab.incognito || !!tab.inPrivate;
        _this.logger.setContextProperty(Log.Context.Custom.InPrivateBrowsing, isPrivateWindow.toString());
        _this.invokeDebugLoggingIfEnabled();
        return _this;
    }
    /**
     * Get the url associated with this worker's tab
     */
    WebExtensionWorker.prototype.getUrl = function () {
        return this.tab.url;
    };
    /**
     * Launches the sign in window, rejecting with an error object if something went wrong on the server during
     * authentication. Otherwise, it resolves with true if the redirect endpoint was hit as a result of a successful
     * sign in attempt, and false if it was not hit (e.g., user manually closed the popup)
     */
    WebExtensionWorker.prototype.doSignInAction = function (authType) {
        var usidQueryParamValue = this.getUserSessionIdQueryParamValue();
        var signInUrl = clipperUrls_1.ClipperUrls.generateSignInUrl(this.clientInfo.get().clipperId, usidQueryParamValue, userInfo_1.AuthType[authType]);
        return this.launchWebExtensionPopupAndWaitForClose(signInUrl, constants_1.Constants.Urls.Authentication.authRedirectUrl);
    };
    /**
     * Signs the user out
     */
    WebExtensionWorker.prototype.doSignOutAction = function (authType) {
        var usidQueryParamValue = this.getUserSessionIdQueryParamValue();
        var signOutUrl = clipperUrls_1.ClipperUrls.generateSignOutUrl(this.clientInfo.get().clipperId, usidQueryParamValue, userInfo_1.AuthType[authType]);
        browserUtils_1.BrowserUtils.appendHiddenIframeToDocument(signOutUrl);
    };
    /**
     * Notify the UI to invoke the clipper. Resolve with true if it was thought to be successfully
     * injected; otherwise resolves with false.
     */
    WebExtensionWorker.prototype.invokeClipperBrowserSpecific = function () {
        var _this = this;
        return new Promise(function (resolve) {
            webExtension_1.WebExtension.browser.tabs.executeScript(_this.tab.id, {
                code: 'var frameUrl = "' + webExtension_1.WebExtension.browser.extension.getURL("clipper.html") + '";'
            }, function () {
                if (webExtension_1.WebExtension.browser.runtime.lastError) {
                    Log.ErrorUtils.sendFailureLogRequest({
                        label: Log.Failure.Label.UnclippablePage,
                        properties: {
                            failureType: Log.Failure.Type.Expected,
                            failureInfo: { error: JSON.stringify({ error: webExtension_1.WebExtension.browser.runtime.lastError.message, url: _this.tab.url }) },
                            stackTrace: Log.Failure.getStackTrace()
                        },
                        clientInfo: _this.clientInfo
                    });
                    // In Firefox, alert() is not callable from the background, so it looks like we have to no-op here
                    if (_this.clientInfo.get().clipperType !== clientType_1.ClientType.FirefoxExtension) {
                        injectHelper_1.InjectHelper.alertUserOfUnclippablePage();
                    }
                    resolve(false);
                }
                else {
                    webExtension_1.WebExtension.browser.tabs.executeScript(_this.tab.id, { file: _this.injectUrls.webClipperInjectUrl });
                    if (!_this.noOpTrackerInvoked) {
                        _this.setUpNoOpTrackers(_this.tab.url);
                        _this.noOpTrackerInvoked = true;
                    }
                    resolve(true);
                }
            });
        });
    };
    /**
     * Notify the UI to invoke the frontend script that handles logging to the conosle. Resolve with
     * true if it was thought to be successfully injected; otherwise resolves with false.
     */
    WebExtensionWorker.prototype.invokeDebugLoggingBrowserSpecific = function () {
        var _this = this;
        return new Promise(function (resolve) {
            webExtension_1.WebExtension.browser.tabs.executeScript(_this.tab.id, { file: _this.injectUrls.debugLoggingInjectUrl }, function () {
                if (webExtension_1.WebExtension.browser.runtime.lastError) {
                    // We are probably on a page like about:blank, which is pretty normal
                    resolve(false);
                }
                else {
                    resolve(true);
                }
            });
        });
    };
    WebExtensionWorker.prototype.invokePageNavBrowserSpecific = function () {
        var _this = this;
        return new Promise(function (resolve) {
            webExtension_1.WebExtension.browser.tabs.executeScript(_this.tab.id, {
                code: 'var frameUrl = "' + webExtension_1.WebExtension.browser.extension.getURL("pageNav.html") + '";'
            }, function () {
                // It's safest to not use lastError in the resolve due to special behavior in the Chrome API
                if (webExtension_1.WebExtension.browser.runtime.lastError) {
                    // We are probably on a page like about:blank, which is pretty normal
                    resolve(false);
                }
                else {
                    webExtension_1.WebExtension.browser.tabs.executeScript(_this.tab.id, { file: _this.injectUrls.pageNavInjectUrl });
                    resolve(true);
                }
            });
        });
    };
    /**
     * Notify the UI to invoke the What's New tooltip. Resolve with true if it was thought to be successfully
     * injected; otherwise resolves with false.
     */
    WebExtensionWorker.prototype.invokeWhatsNewTooltipBrowserSpecific = function (newVersions) {
        return this.invokePageNavBrowserSpecific();
    };
    WebExtensionWorker.prototype.invokeTooltipBrowserSpecific = function () {
        return this.invokePageNavBrowserSpecific();
    };
    WebExtensionWorker.prototype.isAllowedFileSchemeAccessBrowserSpecific = function (callback) {
        var _this = this;
        if (!webExtension_1.WebExtension.browser.extension.isAllowedFileSchemeAccess) {
            callback(true);
            return;
        }
        webExtension_1.WebExtension.browser.extension.isAllowedFileSchemeAccess(function (isAllowed) {
            if (!isAllowed && _this.tab.url.indexOf("file:///") === 0) {
                callback(false);
            }
            else {
                callback(true);
            }
        });
    };
    /**
     * Gets the visible tab's screenshot as an image url
     */
    WebExtensionWorker.prototype.takeTabScreenshot = function () {
        return new Promise(function (resolve) {
            webExtension_1.WebExtension.browser.tabs.query({ active: true, lastFocusedWindow: true }, function () {
                webExtension_1.WebExtension.browser.tabs.captureVisibleTab({ format: "png" }, function (dataUrl) {
                    resolve(dataUrl);
                });
            });
        });
    };
    WebExtensionWorker.prototype.launchWebExtensionPopupAndWaitForClose = function (url, autoCloseDestinationUrl) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var popupWidth = 1000;
            var popupHeight = 700;
            var leftPosition = (screen && screen.width) ? Math.round((screen.width - popupWidth) / 2) : 0;
            var topPosition = (screen && screen.height) ? Math.round((screen.height - popupHeight) / 2) : 0;
            try {
                /* As of 7/19/2016, Firefox does not yet supported the "focused" key for windows.create in the WebExtensions API */
                /* See bug filed here: https://bugzilla.mozilla.org/show_bug.cgi?id=1213484 */
                var windowOptions = {
                    height: popupHeight,
                    left: leftPosition,
                    top: topPosition,
                    type: "popup",
                    url: url,
                    width: popupWidth
                };
                if (_this.clientInfo.get().clipperType !== clientType_1.ClientType.FirefoxExtension) {
                    windowOptions.focused = true;
                }
                webExtension_1.WebExtension.browser.windows.create(windowOptions, function (newWindow) {
                    var redirectOccurred = false;
                    var errorObject;
                    var correlationId;
                    var redirectListener = function (details) {
                        redirectOccurred = true;
                        // Find and get correlation id
                        if (details.responseHeaders) {
                            for (var i = 0; i < details.responseHeaders.length; i++) {
                                if (details.responseHeaders[i].name === constants_1.Constants.HeaderValues.correlationId) {
                                    correlationId = details.responseHeaders[i].value;
                                    break;
                                }
                            }
                        }
                        var redirectUrl = details.url;
                        var error = urlUtils_1.UrlUtils.getQueryValue(redirectUrl, constants_1.Constants.Urls.QueryParams.error);
                        var errorDescription = urlUtils_1.UrlUtils.getQueryValue(redirectUrl, constants_1.Constants.Urls.QueryParams.errorDescription);
                        if (error || errorDescription) {
                            errorObject = { error: error, errorDescription: errorDescription, correlationId: correlationId };
                        }
                        webExtension_1.WebExtension.browser.webRequest.onCompleted.removeListener(redirectListener);
                        webExtension_1.WebExtension.browser.tabs.remove(details.tabId);
                    };
                    webExtension_1.WebExtension.browser.webRequest.onCompleted.addListener(redirectListener, {
                        windowId: newWindow.id, urls: [autoCloseDestinationUrl + "*"]
                    }, ["responseHeaders"]);
                    var closeListener = function (tabId, tabRemoveInfo) {
                        if (tabRemoveInfo.windowId === newWindow.id) {
                            errorObject ? reject(errorObject) : resolve(redirectOccurred);
                            webExtension_1.WebExtension.browser.tabs.onRemoved.removeListener(closeListener);
                        }
                    };
                    webExtension_1.WebExtension.browser.tabs.onRemoved.addListener(closeListener);
                });
            }
            catch (e) {
                // In the event that there was an exception thrown during the creation of the popup, fallback to using window.open with a monitor
                _this.logger.logFailure(Log.Failure.Label.WebExtensionWindowCreate, Log.Failure.Type.Unexpected, { error: e.message });
                _this.launchPopupAndWaitForClose(url).then(function (redirectOccurred) {
                    // From chrome's background, we currently are unable to reliably determine if the redirect happened
                    resolve(true /* redirectOccurred */);
                }, function (errorObject) {
                    reject(errorObject);
                });
            }
        });
    };
    return WebExtensionWorker;
}(extensionWorkerBase_1.ExtensionWorkerBase));
exports.WebExtensionWorker = WebExtensionWorker;
