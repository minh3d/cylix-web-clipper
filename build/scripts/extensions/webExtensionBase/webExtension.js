"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var clientType_1 = require("../../clientType");
var urlUtils_1 = require("../../urlUtils");
var videoUtils_1 = require("../../domParsers/videoUtils");
var localization_1 = require("../../localization/localization");
var clipperData_1 = require("../../storage/clipperData");
var localStorage_1 = require("../../storage/localStorage");
var extensionBase_1 = require("../extensionBase");
var invokeSource_1 = require("../invokeSource");
var invokeOptions_1 = require("../invokeOptions");
var webExtensionWorker_1 = require("./webExtensionWorker");
var WebExtension = (function (_super) {
    __extends(WebExtension, _super);
    function WebExtension(clientType, injectUrls) {
        var _this = _super.call(this, clientType, new clipperData_1.ClipperData(new localStorage_1.LocalStorage())) || this;
        _this.injectUrls = injectUrls;
        _this.registerBrowserButton();
        _this.registerContextMenuItems();
        _this.registerInstallListener();
        _this.registerTabRemoveListener();
        return _this;
    }
    WebExtension.getExtensionVersion = function () {
        return WebExtension.browser.runtime.getManifest().version;
    };
    WebExtension.prototype.addPageNavListener = function (callback) {
        WebExtension.browser.webNavigation.onCompleted.addListener(function (details) {
            // The callback is called on iframes as well, so we ignore those as we are only interested in main frame navigation
            if (details.frameId === 0) {
                WebExtension.browser.tabs.get(details.tabId, function (tab) {
                    if (!WebExtension.browser.runtime.lastError && tab) {
                        callback(tab);
                    }
                });
            }
        });
    };
    WebExtension.prototype.checkIfTabIsOnWhitelistedUrl = function (tab) {
        return !urlUtils_1.UrlUtils.onBlacklistedDomain(tab.url) && urlUtils_1.UrlUtils.onWhitelistedDomain(tab.url);
    };
    WebExtension.prototype.createWorker = function (tab) {
        return new webExtensionWorker_1.WebExtensionWorker(this.injectUrls, tab, this.clientInfo, this.auth);
    };
    WebExtension.prototype.getIdFromTab = function (tab) {
        return tab.id;
    };
    WebExtension.prototype.onFirstRun = function () {
        // Don't do anything since we're using the onInstalled functionality instead, unless it's not available
        // then we use our 'missing-clipperId' heuristic
        if (!this.onInstalledSupported()) {
            this.onInstalled();
        }
    };
    WebExtension.prototype.checkIfTabMatchesATooltipType = function (tab, tooltipTypes) {
        if (urlUtils_1.UrlUtils.onBlacklistedDomain(tab.url)) {
            return undefined;
        }
        return urlUtils_1.UrlUtils.checkIfUrlMatchesAContentType(tab.url, tooltipTypes);
    };
    WebExtension.prototype.checkIfTabIsAVideoDomain = function (tab) {
        var domain = videoUtils_1.VideoUtils.videoDomainIfSupported(tab.url);
        return !!domain;
    };
    WebExtension.prototype.invokeClipperInTab = function (tab, invokeInfo, options) {
        var worker = this.getOrCreateWorkerForTab(tab, this.getIdFromTab);
        worker.closeAllFramesAndInvokeClipper(invokeInfo, options);
    };
    WebExtension.prototype.onInstalled = function () {
        var _this = this;
        // Send users to our installed page (redirect if they're already on our page, else open a new tab)
        WebExtension.browser.tabs.query({ active: true, lastFocusedWindow: true }, function (tabs) {
            var isInlineInstall = extensionBase_1.ExtensionBase.isOnOneNoteDomain(tabs[0].url);
            var installUrl = _this.getClipperInstalledPageUrl(_this.clientInfo.get().clipperId, _this.clientInfo.get().clipperType, isInlineInstall);
            if (isInlineInstall) {
                WebExtension.browser.tabs.update(tabs[0].id, { url: installUrl });
            }
            else {
                WebExtension.browser.tabs.create({ url: installUrl });
            }
        });
    };
    WebExtension.prototype.registerBrowserButton = function () {
        var _this = this;
        WebExtension.browser.browserAction.onClicked.addListener(function (tab) {
            _this.invokeClipperInTab(tab, { invokeSource: invokeSource_1.InvokeSource.ExtensionButton }, { invokeMode: invokeOptions_1.InvokeMode.Default });
        });
    };
    WebExtension.prototype.registerContextMenuItems = function () {
        var _this = this;
        // Front-load our localization so our context menu is always localized
        this.fetchAndStoreLocStrings().then(function () {
            WebExtension.browser.contextMenus.removeAll(function () {
                var menus = [{
                        title: localization_1.Localization.getLocalizedString("WebClipper.Label.OneNoteWebClipper"),
                        contexts: ["page"],
                        onclick: function (info, tab) {
                            _this.invokeClipperInTab(tab, { invokeSource: invokeSource_1.InvokeSource.ContextMenu }, { invokeMode: invokeOptions_1.InvokeMode.Default });
                        }
                    }, {
                        title: localization_1.Localization.getLocalizedString("WebClipper.Label.ClipSelectionToOneNote"),
                        contexts: ["selection"],
                        onclick: function (info, tab) {
                            var invokeOptions = { invokeMode: invokeOptions_1.InvokeMode.ContextTextSelection };
                            // If the tab index is negative, chances are the user is using some sort of PDF plugin,
                            // and the tab object will be invalid. We need to get the parent tab in this scenario.
                            if (tab.index < 0) {
                                // Since we are in a PDF plugin, Rangy won't work, so we rely on WebExtension API to grab pure text
                                invokeOptions.invokeDataForMode = info.selectionText;
                                WebExtension.browser.tabs.query({ active: true, currentWindow: true }, function (tabs) {
                                    // There will only be one tab that meets this criteria
                                    var parentTab = tabs[0];
                                    _this.invokeClipperInTab(parentTab, { invokeSource: invokeSource_1.InvokeSource.ContextMenu }, invokeOptions);
                                });
                            }
                            else {
                                _this.invokeClipperInTab(tab, { invokeSource: invokeSource_1.InvokeSource.ContextMenu }, invokeOptions);
                            }
                        }
                    }, {
                        title: localization_1.Localization.getLocalizedString("WebClipper.Label.ClipImageToOneNote"),
                        contexts: ["image"],
                        onclick: function (info, tab) {
                            // Even though we know the user right-clicked an image, srcUrl is only present if the src attr exists
                            _this.invokeClipperInTab(tab, { invokeSource: invokeSource_1.InvokeSource.ContextMenu }, info.srcUrl ? {
                                // srcUrl will always be the full url, not relative
                                invokeDataForMode: info.srcUrl, invokeMode: invokeOptions_1.InvokeMode.ContextImage
                            } : undefined);
                        }
                    }];
                var documentUrlPatternList;
                switch (_this.clientInfo.get().clipperType) {
                    case clientType_1.ClientType.ChromeExtension:
                        documentUrlPatternList = [
                            "http://*/*",
                            "https://*/*",
                            "chrome-extension://encfpfilknmenlmjemepncnlbbjlabkc/*",
                            "chrome-extension://oemmndcbldboiebfnladdacbdfmadadm/*",
                            "chrome-extension://mhjfbmdgcfjbbpaeojofohoefgiehjai/*" // Chrome PDF Viewer
                        ];
                        break;
                    case clientType_1.ClientType.EdgeExtension:
                        // Note that in Edge, the ms-browser-extension:// URL causes the context menus to break.
                        documentUrlPatternList = [
                            "http://*/*",
                            "https://*/*"
                        ];
                        break;
                    case clientType_1.ClientType.FirefoxExtension:
                        // Note that documentUrlPatterns is not supported in Firefox as of 07/22/16
                        // https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Chrome_incompatibilities
                        // If you include documentUrlPatterns in Firefox, the context menu won't be added!
                        break;
                    default:
                        break;
                }
                for (var i = 0; i < menus.length; i++) {
                    if (documentUrlPatternList) {
                        menus[i].documentUrlPatterns = documentUrlPatternList;
                    }
                    WebExtension.browser.contextMenus.create(menus[i]);
                }
            });
        });
    };
    WebExtension.prototype.registerInstallListener = function () {
        var _this = this;
        // onInstalled is undefined as of Firefox 48
        if (this.onInstalledSupported()) {
            WebExtension.browser.runtime.onInstalled.addListener(function (details) {
                if (details.reason === "install") {
                    _this.onInstalled();
                }
            });
        }
    };
    WebExtension.prototype.registerTabRemoveListener = function () {
        var _this = this;
        WebExtension.browser.tabs.onRemoved.addListener(function (tabId) {
            var worker = _this.getExistingWorkerForTab(tabId);
            if (worker) {
                _this.removeWorker(worker);
            }
        });
    };
    WebExtension.prototype.onInstalledSupported = function () {
        return !!WebExtension.browser.runtime.onInstalled;
    };
    return WebExtension;
}(extensionBase_1.ExtensionBase));
exports.WebExtension = WebExtension;
