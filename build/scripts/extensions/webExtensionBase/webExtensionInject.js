"use strict";
var clipperInject_1 = require("../clipperInject");
function invoke(oneNoteClipperOptions) {
    clipperInject_1.ClipperInject.main(oneNoteClipperOptions);
}
exports.invoke = invoke;
