"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var logger_1 = require("../logging/logger");
var WorkerPassthroughLogger = (function (_super) {
    __extends(WorkerPassthroughLogger, _super);
    function WorkerPassthroughLogger(workers) {
        var _this = _super.call(this) || this;
        _this.workers = workers;
        return _this;
    }
    WorkerPassthroughLogger.prototype.logEvent = function (event) {
        for (var _i = 0, _a = this.workers; _i < _a.length; _i++) {
            var worker = _a[_i];
            worker.getLogger().logEvent(event);
        }
    };
    WorkerPassthroughLogger.prototype.pushToStream = function (label, value) {
        for (var _i = 0, _a = this.workers; _i < _a.length; _i++) {
            var worker = _a[_i];
            worker.getLogger().pushToStream(label, value);
        }
    };
    WorkerPassthroughLogger.prototype.logFailure = function (label, failureType, failureInfo, id) {
        for (var _i = 0, _a = this.workers; _i < _a.length; _i++) {
            var worker = _a[_i];
            worker.getLogger().logFailure(label, failureType, failureInfo, id);
        }
    };
    WorkerPassthroughLogger.prototype.logUserFunnel = function (label) {
        for (var _i = 0, _a = this.workers; _i < _a.length; _i++) {
            var worker = _a[_i];
            worker.getLogger().logUserFunnel(label);
        }
    };
    WorkerPassthroughLogger.prototype.logSessionStart = function () {
        for (var _i = 0, _a = this.workers; _i < _a.length; _i++) {
            var worker = _a[_i];
            worker.getLogger().logSessionStart();
        }
    };
    WorkerPassthroughLogger.prototype.logSessionEnd = function (endTrigger) {
        for (var _i = 0, _a = this.workers; _i < _a.length; _i++) {
            var worker = _a[_i];
            worker.getLogger().logSessionEnd(endTrigger);
        }
    };
    WorkerPassthroughLogger.prototype.logTrace = function (label, level, message) {
        for (var _i = 0, _a = this.workers; _i < _a.length; _i++) {
            var worker = _a[_i];
            worker.getLogger().logTrace(label, level, message);
        }
    };
    WorkerPassthroughLogger.prototype.logClickEvent = function (clickId) {
        for (var _i = 0, _a = this.workers; _i < _a.length; _i++) {
            var worker = _a[_i];
            worker.getLogger().logClickEvent(clickId);
        }
    };
    WorkerPassthroughLogger.prototype.setContextProperty = function (key, value) {
        for (var _i = 0, _a = this.workers; _i < _a.length; _i++) {
            var worker = _a[_i];
            worker.getLogger().setContextProperty(key, value);
        }
    };
    return WorkerPassthroughLogger;
}(logger_1.Logger));
exports.WorkerPassthroughLogger = WorkerPassthroughLogger;
