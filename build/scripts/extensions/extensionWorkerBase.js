"use strict";
var browserUtils_1 = require("../browserUtils");
var clientType_1 = require("../clientType");
var clipperUrls_1 = require("../clipperUrls");
var cookieUtils_1 = require("../cookieUtils");
var constants_1 = require("../constants");
var polyfills_1 = require("../polyfills");
var userInfo_1 = require("../userInfo");
var settings_1 = require("../settings");
var tooltipType_1 = require("../clipperUI/tooltipType");
var communicator_1 = require("../communicator/communicator");
var smartValue_1 = require("../communicator/smartValue");
var clipperCachedHttp_1 = require("../http/clipperCachedHttp");
var localizationHelper_1 = require("../localization/localizationHelper");
var Log = require("../logging/log");
var logHelpers_1 = require("../logging/logHelpers");
var clipperStorageKeys_1 = require("../storage/clipperStorageKeys");
var extensionBase_1 = require("./extensionBase");
var invokeSource_1 = require("./invokeSource");
var invokeOptions_1 = require("./invokeOptions");
/**
 * The abstract base class for all of the extension workers
 */
var ExtensionWorkerBase = (function () {
    function ExtensionWorkerBase(clientInfo, auth, clipperData, uiMessageHandlerThunk, injectMessageHandlerThunk) {
        this.clipperFunnelAlreadyLogged = false;
        polyfills_1.Polyfills.init();
        this.onUnloading = function () { };
        this.uiCommunicator = new communicator_1.Communicator(uiMessageHandlerThunk(), constants_1.Constants.CommunicationChannels.extensionAndUi);
        this.pageNavUiCommunicator = new communicator_1.Communicator(uiMessageHandlerThunk(), constants_1.Constants.CommunicationChannels.extensionAndPageNavUi);
        this.debugLoggingInjectCommunicator = new communicator_1.Communicator(injectMessageHandlerThunk(), constants_1.Constants.CommunicationChannels.debugLoggingInjectedAndExtension);
        this.injectCommunicator = new communicator_1.Communicator(injectMessageHandlerThunk(), constants_1.Constants.CommunicationChannels.injectedAndExtension);
        this.pageNavInjectCommunicator = new communicator_1.Communicator(injectMessageHandlerThunk(), constants_1.Constants.CommunicationChannels.pageNavInjectedAndExtension);
        this.sessionId = new smartValue_1.SmartValue();
        this.logger = LogManager.createExtLogger(this.sessionId, logHelpers_1.LogHelpers.isConsoleOutputEnabled() ? this.debugLoggingInjectCommunicator : undefined);
        this.logger.logSessionStart();
        this.clipperData = clipperData;
        this.clipperData.setLogger(this.logger);
        this.auth = auth;
        this.clientInfo = clientInfo;
        // TODO Remove this function after ~some~ amount of time has passed
        // This is a temporary event shipping with v3.2.0 that is needed to map
        // the "accidental" device ids we were logging (that came from a cookie)
        // to the "real" device ids (ON-xxx) found in local storage that we weren't logging
        this.logDeviceIdMapEvent();
        this.initializeCommunicators();
        this.initializeContextProperties();
    }
    ExtensionWorkerBase.prototype.initializeContextProperties = function () {
        var _this = this;
        var clientInfo = this.clientInfo.get();
        this.logger.setContextProperty(Log.Context.Custom.AppInfoId, settings_1.Settings.getSetting("App_Id"));
        this.logger.setContextProperty(Log.Context.Custom.ExtensionLifecycleId, extensionBase_1.ExtensionBase.getExtensionId());
        this.logger.setContextProperty(Log.Context.Custom.UserInfoId, undefined);
        this.logger.setContextProperty(Log.Context.Custom.AuthType, "None");
        this.logger.setContextProperty(Log.Context.Custom.AppInfoVersion, clientInfo.clipperVersion);
        this.logger.setContextProperty(Log.Context.Custom.DeviceInfoId, clientInfo.clipperId);
        this.logger.setContextProperty(Log.Context.Custom.ClipperType, clientType_1.ClientType[clientInfo.clipperType]);
        // Sometimes the worker is created really early (e.g., pageNav, inline extension), so we need to wait
        // for flighting info to be returned before we set the context property
        if (!clientInfo.flightingInfo) {
            var clientInfoSetCb_1 = (function (newClientInfo) {
                if (newClientInfo.flightingInfo) {
                    _this.clientInfo.unsubscribe(clientInfoSetCb_1);
                    _this.logger.setContextProperty(Log.Context.Custom.FlightInfo, newClientInfo.flightingInfo.join(","));
                }
            }).bind(this);
            this.clientInfo.subscribe(clientInfoSetCb_1, { callOnSubscribe: false });
        }
        else {
            this.logger.setContextProperty(Log.Context.Custom.FlightInfo, clientInfo.flightingInfo.join(","));
        }
    };
    /**
     * Get the unique id associated with this worker's tab. The type is any type that allows us to distinguish
     * between tabs, and is dependent on the browser itself.
     */
    ExtensionWorkerBase.prototype.getUniqueId = function () {
        return this.tabId;
    };
    /**
     * Closes all active frames and notifies the UI to invoke the clipper.
     */
    ExtensionWorkerBase.prototype.closeAllFramesAndInvokeClipper = function (invokeInfo, options) {
        this.pageNavInjectCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.closePageNavTooltip);
        this.invokeClipper(invokeInfo, options);
    };
    ExtensionWorkerBase.prototype.getLogger = function () {
        return this.logger;
    };
    /**
     * Skeleton method that notifies the UI to invoke the Clipper. Also performs logging.
     */
    ExtensionWorkerBase.prototype.invokeClipper = function (invokeInfo, options) {
        var _this = this;
        // For safety, we enforce that the object we send is never undefined.
        var invokeOptionsToSend = {
            invokeDataForMode: options ? options.invokeDataForMode : undefined,
            invokeMode: options ? options.invokeMode : invokeOptions_1.InvokeMode.Default
        };
        this.sendInvokeOptionsToInject(invokeOptionsToSend);
        this.isAllowedFileSchemeAccessBrowserSpecific(function (isAllowed) {
            if (!isAllowed) {
                _this.uiCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.extensionNotAllowedToAccessLocalFiles);
            }
        });
        this.invokeClipperBrowserSpecific().then(function (wasInvoked) {
            if (wasInvoked && !_this.clipperFunnelAlreadyLogged) {
                _this.logger.logUserFunnel(Log.Funnel.Label.Invoke);
                _this.clipperFunnelAlreadyLogged = true;
            }
            _this.logClipperInvoke(invokeInfo, invokeOptionsToSend);
        });
    };
    /**
     * Notify the UI to invoke the What's New experience. Resolve with true if it was thought to be successfully
     * injected; otherwise resolves with false.
     */
    ExtensionWorkerBase.prototype.invokeWhatsNewTooltip = function (newVersions) {
        var _this = this;
        var invokeWhatsNewEvent = new Log.Event.PromiseEvent(Log.Event.Label.InvokeWhatsNew);
        return this.registerLocalizedStringsForPageNav().then(function (successful) {
            if (successful) {
                _this.registerWhatsNewCommunicatorFunctions(newVersions);
                return _this.invokeWhatsNewTooltipBrowserSpecific(newVersions).then(function (wasInvoked) {
                    if (!wasInvoked) {
                        invokeWhatsNewEvent.setStatus(Log.Status.Failed);
                        invokeWhatsNewEvent.setFailureInfo({ error: "invoking the What's New experience failed" });
                    }
                    _this.logger.logEvent(invokeWhatsNewEvent);
                    return Promise.resolve(wasInvoked);
                });
            }
            else {
                invokeWhatsNewEvent.setStatus(Log.Status.Failed);
                invokeWhatsNewEvent.setFailureInfo({ error: "getLocalizedStringsForBrowser returned undefined/null" });
                _this.logger.logEvent(invokeWhatsNewEvent);
                return Promise.resolve(false);
            }
        });
    };
    ExtensionWorkerBase.prototype.invokeTooltip = function (tooltipType) {
        var _this = this;
        var tooltipInvokeEvent = new Log.Event.PromiseEvent(Log.Event.Label.InvokeTooltip);
        tooltipInvokeEvent.setCustomProperty(Log.PropertyName.Custom.TooltipType, tooltipType_1.TooltipType[tooltipType]);
        return this.registerLocalizedStringsForPageNav().then(function (successful) {
            if (successful) {
                _this.registerTooltipCommunicatorFunctions(tooltipType);
                return _this.invokeTooltipBrowserSpecific(tooltipType).then(function (wasInvoked) {
                    _this.logger.logEvent(tooltipInvokeEvent);
                    return Promise.resolve(wasInvoked);
                });
            }
            else {
                tooltipInvokeEvent.setStatus(Log.Status.Failed);
                tooltipInvokeEvent.setFailureInfo({ error: "getLocalizedStringsForBrowser returned undefined/null" });
                _this.logger.logEvent(tooltipInvokeEvent);
                return Promise.resolve(false);
            }
        });
    };
    /**
     * Sets the hook method that will be called when this worker object goes away.
     */
    ExtensionWorkerBase.prototype.setOnUnloading = function (callback) {
        this.onUnloading = callback;
    };
    /**
     * Clean up anything related to the worker before it stops being used (aka the tab or window was closed)
     */
    ExtensionWorkerBase.prototype.destroy = function () {
        this.logger.logSessionEnd(Log.Session.EndTrigger.Unload);
    };
    /**
     * Returns the current version of localized strings used in the UI.
     */
    ExtensionWorkerBase.prototype.getLocalizedStrings = function (locale, callback) {
        var _this = this;
        this.logger.setContextProperty(Log.Context.Custom.BrowserLanguage, locale);
        var storedLocale = this.clipperData.getValue(clipperStorageKeys_1.ClipperStorageKeys.locale);
        var localeInStorageIsDifferent = !storedLocale || storedLocale !== locale;
        var getLocaleEvent = new Log.Event.BaseEvent(Log.Event.Label.GetLocale);
        getLocaleEvent.setCustomProperty(Log.PropertyName.Custom.StoredLocaleDifferentThanRequested, localeInStorageIsDifferent);
        this.logger.logEvent(getLocaleEvent);
        var fetchStringDataFunction = function () { return localizationHelper_1.LocalizationHelper.makeLocStringsFetchRequest(locale); };
        var updateInterval = localeInStorageIsDifferent ? 0 : clipperCachedHttp_1.ClipperCachedHttp.getDefaultExpiry();
        var getLocalizedStringsEvent = new Log.Event.PromiseEvent(Log.Event.Label.GetLocalizedStrings);
        getLocalizedStringsEvent.setCustomProperty(Log.PropertyName.Custom.ForceRetrieveFreshLocStrings, localeInStorageIsDifferent);
        this.clipperData.getFreshValue(clipperStorageKeys_1.ClipperStorageKeys.locStrings, fetchStringDataFunction, updateInterval).then(function (response) {
            _this.clipperData.setValue(clipperStorageKeys_1.ClipperStorageKeys.locale, locale);
            if (callback) {
                callback(response ? response.data : undefined);
            }
        }, function (error) {
            getLocalizedStringsEvent.setStatus(Log.Status.Failed);
            getLocalizedStringsEvent.setFailureInfo(error);
            // Still proceed, as we have backup strings on the client
            if (callback) {
                callback(undefined);
            }
        }).then(function () {
            _this.logger.logEvent(getLocalizedStringsEvent);
        });
    };
    ExtensionWorkerBase.prototype.getLocalizedStringsForBrowser = function (callback) {
        var localeOverride = this.clipperData.getValue(clipperStorageKeys_1.ClipperStorageKeys.displayLanguageOverride);
        // navigator.userLanguage is only available in IE, and Typescript will not recognize this property
        var locale = localeOverride || navigator.language || navigator.userLanguage;
        this.getLocalizedStrings(locale, callback);
    };
    ExtensionWorkerBase.prototype.getUserSessionIdQueryParamValue = function () {
        var usidQueryParamValue = this.logger.getUserSessionId();
        return usidQueryParamValue ? usidQueryParamValue : this.clientInfo.get().clipperId;
    };
    ExtensionWorkerBase.prototype.invokeDebugLoggingIfEnabled = function () {
        if (logHelpers_1.LogHelpers.isConsoleOutputEnabled()) {
            return this.invokeDebugLoggingBrowserSpecific();
        }
        return Promise.resolve(false);
    };
    ExtensionWorkerBase.prototype.launchPopupAndWaitForClose = function (url) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var signInWindow = browserUtils_1.BrowserUtils.openPopupWindow(url);
            var errorObject;
            var popupMessageHandler = function (event) {
                if (event.source === signInWindow) {
                    var dataAsJson = void 0;
                    try {
                        dataAsJson = JSON.parse(event.data);
                    }
                    catch (e) {
                        _this.logger.logJsonParseUnexpected(event.data);
                    }
                    if (dataAsJson && (dataAsJson[constants_1.Constants.Urls.QueryParams.error] || dataAsJson[constants_1.Constants.Urls.QueryParams.errorDescription])) {
                        errorObject = {
                            correlationId: dataAsJson[constants_1.Constants.Urls.QueryParams.correlationId],
                            error: dataAsJson[constants_1.Constants.Urls.QueryParams.error],
                            errorDescription: dataAsJson[constants_1.Constants.Urls.QueryParams.errorDescription]
                        };
                    }
                }
            };
            window.addEventListener("message", popupMessageHandler);
            var timer = setInterval(function () {
                if (!signInWindow || signInWindow.closed) {
                    clearInterval(timer);
                    window.removeEventListener("message", popupMessageHandler);
                    // We always resolve with true in the non-error case as we can't reliably detect redirects
                    // on non-IE bookmarklets
                    errorObject ? reject(errorObject) : resolve(true);
                }
            }, 100);
        });
    };
    ExtensionWorkerBase.prototype.logClipperInvoke = function (invokeInfo, options) {
        var invokeClipperEvent = new Log.Event.BaseEvent(Log.Event.Label.InvokeClipper);
        invokeClipperEvent.setCustomProperty(Log.PropertyName.Custom.InvokeSource, invokeSource_1.InvokeSource[invokeInfo.invokeSource]);
        invokeClipperEvent.setCustomProperty(Log.PropertyName.Custom.InvokeMode, invokeOptions_1.InvokeMode[options.invokeMode]);
        this.logger.logEvent(invokeClipperEvent);
    };
    /**
     * Registers the tooltip type that needs to appear in the Page Nav experience, as well as any props it needs
     */
    ExtensionWorkerBase.prototype.registerTooltipToRenderInPageNav = function (tooltipType, tooltipProps) {
        this.pageNavUiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.getTooltipToRenderInPageNav, function () {
            return Promise.resolve(tooltipType);
        });
        this.pageNavUiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.getPageNavTooltipProps, function () {
            return Promise.resolve(tooltipProps);
        });
    };
    /**
     * Register communicator functions specific to the What's New experience
     */
    ExtensionWorkerBase.prototype.registerWhatsNewCommunicatorFunctions = function (newVersions) {
        this.registerTooltipToRenderInPageNav(tooltipType_1.TooltipType.WhatsNew, {
            updates: newVersions
        });
    };
    ExtensionWorkerBase.prototype.registerTooltipCommunicatorFunctions = function (tooltipType) {
        this.registerTooltipToRenderInPageNav(tooltipType);
    };
    ExtensionWorkerBase.prototype.sendInvokeOptionsToInject = function (options) {
        this.injectCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.setInvokeOptions, {
            param: options
        });
    };
    ExtensionWorkerBase.prototype.setUpNoOpTrackers = function (url) {
        // No-op tracker for communication with inject
        var injectNoOpTrackerTimeout = Log.ErrorUtils.setNoOpTrackerRequestTimeout({
            label: Log.NoOp.Label.InitializeCommunicator,
            channel: constants_1.Constants.CommunicationChannels.injectedAndExtension,
            clientInfo: this.clientInfo,
            url: url
        }, true);
        this.injectCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.noOpTracker, {
            param: new Date().getTime(),
            callback: function () {
                clearTimeout(injectNoOpTrackerTimeout);
            }
        });
        // No-op tracker for communication with the UI
        var uiNoOpTrackerTimeout = Log.ErrorUtils.setNoOpTrackerRequestTimeout({
            label: Log.NoOp.Label.InitializeCommunicator,
            channel: constants_1.Constants.CommunicationChannels.extensionAndUi,
            clientInfo: this.clientInfo,
            url: url
        }, true);
        this.uiCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.noOpTracker, {
            param: new Date().getTime(),
            callback: function () {
                clearTimeout(uiNoOpTrackerTimeout);
            }
        });
    };
    /**
     * Signs the user out on in the frontend. TODO: this was implemented as an Edge workaround, and
     * should be removed when they fix their iframes not properly loading in the background.
     */
    ExtensionWorkerBase.prototype.doSignOutActionInFrontEnd = function (authType) {
        var usidQueryParamValue = this.getUserSessionIdQueryParamValue();
        var signOutUrl = clipperUrls_1.ClipperUrls.generateSignOutUrl(this.clientInfo.get().clipperId, usidQueryParamValue, userInfo_1.AuthType[authType]);
        this.uiCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.createHiddenIFrame, {
            param: signOutUrl
        });
    };
    ExtensionWorkerBase.prototype.initializeCommunicators = function () {
        this.initializeDebugLoggingCommunicators();
        this.initializeClipperCommunicators();
        this.initializePageNavCommunicators();
    };
    ExtensionWorkerBase.prototype.initializeClipperCommunicators = function () {
        this.initializeClipperUiCommunicator();
        this.initializeClipperInjectCommunicator();
    };
    ExtensionWorkerBase.prototype.initializeClipperUiCommunicator = function () {
        var _this = this;
        this.uiCommunicator.broadcastAcrossCommunicator(this.auth.user, constants_1.Constants.SmartValueKeys.user);
        this.uiCommunicator.broadcastAcrossCommunicator(this.clientInfo, constants_1.Constants.SmartValueKeys.clientInfo);
        this.uiCommunicator.broadcastAcrossCommunicator(this.sessionId, constants_1.Constants.SmartValueKeys.sessionId);
        this.uiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.clipperStrings, function () {
            return new Promise(function (resolve) {
                _this.getLocalizedStringsForBrowser(function (dataResult) {
                    resolve(dataResult);
                });
            });
        });
        this.uiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.getStorageValue, function (key) {
            return new Promise(function (resolve) {
                var value = _this.clipperData.getValue(key);
                resolve(value);
            });
        });
        this.uiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.getMultipleStorageValues, function (keys) {
            return new Promise(function (resolve) {
                var values = {};
                for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
                    var key = keys_1[_i];
                    values[key] = _this.clipperData.getValue(key);
                }
                resolve(values);
            });
        });
        this.uiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.setStorageValue, function (keyValuePair) {
            _this.clipperData.setValue(keyValuePair.key, keyValuePair.value);
        });
        this.uiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.getInitialUser, function () {
            return _this.auth.updateUserInfoData(_this.clientInfo.get().clipperId, userInfo_1.UpdateReason.InitialRetrieval);
        });
        this.uiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.signInUser, function (authType) {
            return _this.doSignInAction(authType).then(function (redirectOccurred) {
                // Recently, a change in sign-in flow broke our redirect detection, so now we give the benefit of the doubt
                // and always attempt to update userInfo regardless
                return _this.auth.updateUserInfoData(_this.clientInfo.get().clipperId, userInfo_1.UpdateReason.SignInAttempt).then(function (updatedUser) {
                    // While redirect detection is somewhat unreliable, it's still sometimes correct. So we try and
                    // detect this case only after we try get the latest userInfo
                    if ((!updatedUser || !updatedUser.user) && !redirectOccurred) {
                        var userInfoToSet = { updateReason: userInfo_1.UpdateReason.SignInCancel };
                        _this.auth.user.set(userInfoToSet);
                        return Promise.resolve(userInfoToSet);
                    }
                    return Promise.resolve(updatedUser);
                });
            })["catch"](function (errorObject) {
                // Set the user info object to undefined as a result of an attempted sign in
                _this.auth.user.set({ updateReason: userInfo_1.UpdateReason.SignInAttempt });
                // Right now we're adding the update reason to the errorObject as well so that it is preserved in the callback.
                // The right thing to do is revise the way we use callbacks in the communicator and instead use Promises so that
                // we are able to return distinct objects.
                errorObject.updateReason = userInfo_1.UpdateReason.SignInAttempt;
                return Promise.reject(errorObject);
            });
        });
        this.uiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.signOutUser, function (authType) {
            if (_this.clientInfo.get().clipperType === clientType_1.ClientType.EdgeExtension) {
                _this.doSignOutActionInFrontEnd(authType);
            }
            else {
                _this.doSignOutAction(authType);
            }
            _this.auth.user.set({ updateReason: userInfo_1.UpdateReason.SignOutAction });
            _this.clipperData.setValue(clipperStorageKeys_1.ClipperStorageKeys.userInformation, undefined);
            _this.clipperData.setValue(clipperStorageKeys_1.ClipperStorageKeys.currentSelectedSection, undefined);
            _this.clipperData.setValue(clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks, undefined);
        });
        this.uiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.telemetry, function (data) {
            Log.parseAndLogDataPackage(data, _this.logger);
        });
        this.uiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.ensureFreshUserBeforeClip, function () {
            return _this.auth.updateUserInfoData(_this.clientInfo.get().clipperId, userInfo_1.UpdateReason.TokenRefreshForPendingClip);
        });
        this.uiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.takeTabScreenshot, function () {
            return _this.takeTabScreenshot();
        });
        this.uiCommunicator.setErrorHandler(function (e) {
            Log.ErrorUtils.handleCommunicatorError(constants_1.Constants.CommunicationChannels.extensionAndUi, e, _this.clientInfo);
        });
    };
    ExtensionWorkerBase.prototype.initializeClipperInjectCommunicator = function () {
        var _this = this;
        this.injectCommunicator.broadcastAcrossCommunicator(this.clientInfo, constants_1.Constants.SmartValueKeys.clientInfo);
        this.injectCommunicator.registerFunction(constants_1.Constants.FunctionKeys.unloadHandler, function () {
            _this.tearDownCommunicators();
            _this.onUnloading();
        });
        this.injectCommunicator.registerFunction(constants_1.Constants.FunctionKeys.setStorageValue, function (keyValuePair) {
            _this.clipperData.setValue(keyValuePair.key, keyValuePair.value);
        });
        this.injectCommunicator.setErrorHandler(function (e) {
            Log.ErrorUtils.handleCommunicatorError(constants_1.Constants.CommunicationChannels.injectedAndExtension, e, _this.clientInfo);
        });
    };
    ExtensionWorkerBase.prototype.initializeDebugLoggingCommunicators = function () {
        var _this = this;
        this.debugLoggingInjectCommunicator.registerFunction(constants_1.Constants.FunctionKeys.unloadHandler, function () {
            _this.tearDownCommunicators();
            _this.onUnloading();
        });
    };
    ExtensionWorkerBase.prototype.initializePageNavCommunicators = function () {
        this.initializePageNavUiCommunicator();
        this.initializePageNavInjectCommunicator();
    };
    ExtensionWorkerBase.prototype.initializePageNavUiCommunicator = function () {
        var _this = this;
        this.pageNavUiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.telemetry, function (data) {
            Log.parseAndLogDataPackage(data, _this.logger);
        });
        this.pageNavUiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.invokeClipperFromPageNav, function (invokeSource) {
            _this.closeAllFramesAndInvokeClipper({ invokeSource: invokeSource }, { invokeMode: invokeOptions_1.InvokeMode.Default });
        });
    };
    ExtensionWorkerBase.prototype.initializePageNavInjectCommunicator = function () {
        var _this = this;
        this.pageNavInjectCommunicator.registerFunction(constants_1.Constants.FunctionKeys.telemetry, function (data) {
            Log.parseAndLogDataPackage(data, _this.logger);
        });
        this.pageNavInjectCommunicator.registerFunction(constants_1.Constants.FunctionKeys.unloadHandler, function () {
            _this.tearDownCommunicators();
            _this.onUnloading();
        });
    };
    /**
     * Fetches fresh localized strings and prepares a remote function for the Page Nav UI to fetch them.
     * Resolves with true if successful; false otherwise.
     */
    ExtensionWorkerBase.prototype.registerLocalizedStringsForPageNav = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.getLocalizedStringsForBrowser(function (localizedStrings) {
                if (localizedStrings) {
                    _this.pageNavUiCommunicator.registerFunction(constants_1.Constants.FunctionKeys.clipperStringsFrontLoaded, function () {
                        return Promise.resolve(localizedStrings);
                    });
                }
                resolve(!!localizedStrings);
            });
        });
    };
    ExtensionWorkerBase.prototype.tearDownCommunicators = function () {
        this.uiCommunicator.tearDown();
        this.pageNavUiCommunicator.tearDown();
        this.injectCommunicator.tearDown();
        this.pageNavInjectCommunicator.tearDown();
    };
    // TODO Temporary workaround introduced in v3.2.0
    // Remove after some time...
    ExtensionWorkerBase.prototype.logDeviceIdMapEvent = function () {
        var deviceIdInStorage = this.clientInfo.get().clipperId;
        var deviceIdInCookie = cookieUtils_1.CookieUtils.readCookie("MicrosoftApplicationsTelemetryDeviceId");
        if (deviceIdInCookie !== deviceIdInStorage) {
            var deviceIdMapEvent = new Log.Event.BaseEvent(Log.Event.Label.DeviceIdMap);
            deviceIdMapEvent.setCustomProperty(Log.PropertyName.Custom.DeviceIdInStorage, deviceIdInStorage);
            deviceIdMapEvent.setCustomProperty(Log.PropertyName.Custom.DeviceIdInCookie, deviceIdInCookie);
            this.logger.logEvent(deviceIdMapEvent);
        }
    };
    return ExtensionWorkerBase;
}());
exports.ExtensionWorkerBase = ExtensionWorkerBase;
