"use strict";
var InvokeMode;
(function (InvokeMode) {
    InvokeMode[InvokeMode["ContextImage"] = 0] = "ContextImage";
    InvokeMode[InvokeMode["ContextTextSelection"] = 1] = "ContextTextSelection";
    InvokeMode[InvokeMode["Default"] = 2] = "Default";
})(InvokeMode = exports.InvokeMode || (exports.InvokeMode = {}));
