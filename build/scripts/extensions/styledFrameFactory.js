"use strict";
var constants_1 = require("../constants");
var rtl_1 = require("../localization/rtl");
var Frame;
(function (Frame) {
    Frame[Frame["WebClipper"] = 0] = "WebClipper";
    Frame[Frame["PageNav"] = 1] = "PageNav";
})(Frame = exports.Frame || (exports.Frame = {}));
/**
 * Responsible for returning pre-styled iframes. Does not deal with ids or srces.
 */
var StyledFrameFactory = (function () {
    function StyledFrameFactory() {
    }
    StyledFrameFactory.getStyledFrame = function (frame) {
        switch (frame) {
            case Frame.WebClipper:
                return StyledFrameFactory.getStyledWebClipperFrame();
            default:
            case Frame.PageNav:
                return StyledFrameFactory.getStyledPageNavFrame();
        }
    };
    StyledFrameFactory.applyGlobalStyles = function (iframe) {
        if (rtl_1.Rtl.isRtl(navigator.language || navigator.userLanguage)) {
            iframe.style.left = "0px";
            iframe.style.right = "auto";
        }
        else {
            iframe.style.left = "auto";
            iframe.style.right = "0px";
        }
        iframe.style.top = "0px";
        iframe.style.bottom = "auto";
        iframe.style.border = "none";
        iframe.style.display = "block";
        iframe.style.margin = "0px";
        iframe.style.maxHeight = "none";
        iframe.style.maxWidth = "none";
        iframe.style.minHeight = "0px";
        iframe.style.minWidth = "0px";
        iframe.style.overflow = "hidden";
        iframe.style.padding = "0px";
        iframe.style.position = "fixed";
        iframe.style.transition = "initial";
        iframe.style.zIndex = "2147483647";
    };
    StyledFrameFactory.getGloballyStyledFrame = function () {
        var element = document.createElement("iframe");
        StyledFrameFactory.applyGlobalStyles(element);
        return element;
    };
    StyledFrameFactory.getStyledPageNavFrame = function () {
        var element = StyledFrameFactory.getGloballyStyledFrame();
        element.style.width = constants_1.Constants.Styles.clipperUiWidth + constants_1.Constants.Styles.clipperUiTopRightOffset + constants_1.Constants.Styles.clipperUiDropShadowBuffer + "px";
        return element;
    };
    StyledFrameFactory.getStyledWebClipperFrame = function () {
        var element = StyledFrameFactory.getGloballyStyledFrame();
        return element;
    };
    return StyledFrameFactory;
}());
exports.StyledFrameFactory = StyledFrameFactory;
