"use strict";
var localization_1 = require("../localization/localization");
var InjectHelper = (function () {
    function InjectHelper() {
    }
    InjectHelper.alertUserOfUnclippablePage = function () {
        alert(localization_1.Localization.getLocalizedString("WebClipper.Error.CannotClipPage"));
    };
    InjectHelper.isKnownUninjectablePage = function (url) {
        if (!url) {
            return false;
        }
        for (var i = 0; i < InjectHelper.isKnownUninjectablePage.length; i++) {
            if (InjectHelper.uninjectableUrlRegexes[i].test(url)) {
                return true;
            }
        }
        return false;
    };
    return InjectHelper;
}());
InjectHelper.uninjectableUrlRegexes = [
    /^about:/
];
exports.InjectHelper = InjectHelper;
