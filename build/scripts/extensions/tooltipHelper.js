"use strict";
var constants_1 = require("../constants");
var objectUtils_1 = require("../objectUtils");
var tooltipType_1 = require("../clipperUI/tooltipType");
var clipperStorageKeys_1 = require("../storage/clipperStorageKeys");
var TooltipHelper = (function () {
    function TooltipHelper(storage) {
        this.storage = storage;
        this.validTypes = [tooltipType_1.TooltipType.Pdf, tooltipType_1.TooltipType.Product, tooltipType_1.TooltipType.Recipe, tooltipType_1.TooltipType.Video];
    }
    TooltipHelper.prototype.getTooltipInformation = function (storageKeyBase, tooltipType) {
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(storageKeyBase) || objectUtils_1.ObjectUtils.isNullOrUndefined(tooltipType)) {
            throw new Error("Invalid argument passed to getTooltipInformation");
        }
        var storageKey = TooltipHelper.getStorageKeyForTooltip(storageKeyBase, tooltipType);
        var tooltipInfoAsString = this.storage.getValue(storageKey);
        var info = parseInt(tooltipInfoAsString, 10 /* radix */);
        return !isNaN(info) ? info : 0;
    };
    TooltipHelper.prototype.setTooltipInformation = function (storageKeyBase, tooltipType, value) {
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(storageKeyBase) || objectUtils_1.ObjectUtils.isNullOrUndefined(tooltipType)) {
            throw new Error("Invalid argument passed to setTooltipInformation");
        }
        var storageKey = TooltipHelper.getStorageKeyForTooltip(storageKeyBase, tooltipType);
        this.storage.setValue(storageKey, value);
    };
    TooltipHelper.prototype.tooltipDelayIsOver = function (tooltipType, curTime) {
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(tooltipType) || objectUtils_1.ObjectUtils.isNullOrUndefined(curTime)) {
            throw new Error("Invalid argument passed to tooltipDelayIsOver");
        }
        // If the user has clipped this content type
        var lastClipTime = this.getTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.lastClippedTooltipTimeBase, tooltipType);
        if (lastClipTime !== 0) {
            return false;
        }
        // If the user has seen enough of our tooltips :P 
        var numTimesTooltipHasBeenSeen = this.getTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.numTimesTooltipHasBeenSeenBase, tooltipType);
        if (numTimesTooltipHasBeenSeen >= constants_1.Constants.Settings.maximumNumberOfTimesToShowTooltips) {
            return false;
        }
        // If not enough time has passed since the user saw this specific tooltip, return false
        var lastSeenTooltipTime = this.getTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.lastSeenTooltipTimeBase, tooltipType);
        if (this.tooltipHasBeenSeenInLastTimePeriod(tooltipType, curTime, constants_1.Constants.Settings.timeBetweenSameTooltip)) {
            return false;
        }
        // If not enought time has been since the user saw ANY OTHER tooltip, then return false
        var indexOfThisTooltip = this.validTypes.indexOf(tooltipType);
        var validTypesWithCurrentTypeRemoved = this.validTypes.slice();
        validTypesWithCurrentTypeRemoved.splice(indexOfThisTooltip, 1);
        if (this.hasAnyTooltipBeenSeenInLastTimePeriod(curTime, validTypesWithCurrentTypeRemoved, constants_1.Constants.Settings.timeBetweenDifferentTooltips)) {
            return false;
        }
        return true;
    };
    TooltipHelper.getStorageKeyForTooltip = function (storageKeyBase, tooltipType) {
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(storageKeyBase) || objectUtils_1.ObjectUtils.isNullOrUndefined(tooltipType)) {
            throw new Error("Invalid argument passed to getStorageKeyForTooltip");
        }
        return storageKeyBase + tooltipType_1.TooltipType[tooltipType];
    };
    /**
     * Returns true if the lastSeenTooltipTime of @tooltipType is within @timePeriod of @curTime
     */
    TooltipHelper.prototype.tooltipHasBeenSeenInLastTimePeriod = function (tooltipType, curTime, timePeriod) {
        var lastSeenTooltipTime = this.getTooltipInformation(clipperStorageKeys_1.ClipperStorageKeys.lastSeenTooltipTimeBase, tooltipType);
        if (lastSeenTooltipTime === 0) {
            return false;
        }
        return (curTime - lastSeenTooltipTime) < timePeriod;
    };
    /**
     * Returns true if any of the @tooltipTypesToCheck have been seen in the last @timePeriod, given the current @time
     */
    TooltipHelper.prototype.hasAnyTooltipBeenSeenInLastTimePeriod = function (curTime, typesToCheck, timePeriod) {
        var _this = this;
        return typesToCheck.some(function (tooltipType) {
            var tooltipWasSeen = _this.tooltipHasBeenSeenInLastTimePeriod(tooltipType, curTime, timePeriod);
            return tooltipWasSeen;
        });
    };
    return TooltipHelper;
}());
exports.TooltipHelper = TooltipHelper;
