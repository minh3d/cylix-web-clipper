"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../constants");
var communicator_1 = require("../communicator/communicator");
var Log = require("../logging/log");
var consoleLoggerPure_1 = require("../logging/consoleLoggerPure");
var webConsole_1 = require("../logging/webConsole");
var injectBase_1 = require("./injectBase");
var DebugLoggingInject = (function (_super) {
    __extends(DebugLoggingInject, _super);
    function DebugLoggingInject(options) {
        return _super.call(this, options) || this;
    }
    DebugLoggingInject.main = function (options) {
        // Rather than using a static field (i.e., traditional singleton pattern), we have to attach
        // the singleton to the window object because each time we inject a new inject script, they are
        // sandboxed from each other, so having a static field will not work.
        var theWindow = window;
        if (!theWindow.debugLoggingInjectObject) {
            theWindow.debugLoggingInjectObject = new DebugLoggingInject(options);
        }
    };
    DebugLoggingInject.prototype.generateInlineExtThunk = function () {
        // For the inline extension, the thunk to the iframe window must be passed through main
        // as this inject script has no concept of an iframe to take care of
        return this.options.extMessageHandlerThunk;
    };
    DebugLoggingInject.prototype.init = function () {
        this.debugLogger = new consoleLoggerPure_1.ConsoleLoggerPure(new webConsole_1.WebConsole());
    };
    DebugLoggingInject.prototype.initializeExtCommunicator = function (extMessageHandlerThunk) {
        var _this = this;
        this.extCommunicator = new communicator_1.Communicator(extMessageHandlerThunk(), constants_1.Constants.CommunicationChannels.debugLoggingInjectedAndExtension);
        this.extCommunicator.registerFunction(constants_1.Constants.FunctionKeys.telemetry, function (data) {
            Log.parseAndLogDataPackage(data, _this.debugLogger);
        });
    };
    DebugLoggingInject.prototype.initializeEventListeners = function () {
        var _this = this;
        // Notify the background when we're unloading
        window.onbeforeunload = function (event) {
            _this.extCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.unloadHandler);
        };
    };
    DebugLoggingInject.prototype.handleConstructorError = function (e) {
        Log.ErrorUtils.sendFailureLogRequest({
            label: Log.Failure.Label.UnhandledExceptionThrown,
            properties: {
                failureType: Log.Failure.Type.Unexpected,
                failureInfo: { error: JSON.stringify({ error: e.toString(), url: window.location.href }) },
                failureId: "PageNavInject",
                stackTrace: Log.Failure.getStackTrace(e)
            }
        });
    };
    return DebugLoggingInject;
}(injectBase_1.InjectBase));
exports.DebugLoggingInject = DebugLoggingInject;
