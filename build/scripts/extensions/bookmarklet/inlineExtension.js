"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var clientType_1 = require("../../clientType");
var clipperData_1 = require("../../storage/clipperData");
var localStorage_1 = require("../../storage/localStorage");
var extensionBase_1 = require("../extensionBase");
var inlineWorker_1 = require("./inlineWorker");
/**
 * This class is used for the cases where we don't really have an extension running in the background
 * for us to talk to. For example, when the clipper is invoked via bookmarklet. There is still
 * functionality that we need from the "background" that we can provide within the clipper ui itself.
 * To simplify and hide that distinction away from the rest of the code, we create this "inlined" extension
 * to provide the functionality we need.
 *
 * Due to the 'uniqueness' of the inline extension, stub method implementations are treated as scenarios
 * that don't make sense, and are assumed to be intentional.
 *
 * Note: this is determined by the InjectOptions.useInlineBackgroundWorker flag
 */
var InlineExtension = (function (_super) {
    __extends(InlineExtension, _super);
    function InlineExtension() {
        var _this = _super.call(this, clientType_1.ClientType.Bookmarklet, new clipperData_1.ClipperData(new localStorage_1.LocalStorage())) || this;
        _this.worker = new inlineWorker_1.InlineWorker(_this.clientInfo, _this.auth);
        _this.addWorker(_this.worker);
        return _this;
    }
    InlineExtension.prototype.getIdFromTab = function (tab) {
        return undefined;
    };
    InlineExtension.prototype.getInlineMessageHandler = function () {
        return this.worker.getUiMessageHandler();
    };
    InlineExtension.prototype.getUniqueId = function () {
        return undefined;
    };
    InlineExtension.prototype.addPageNavListener = function (callback) {
    };
    InlineExtension.prototype.checkIfTabMatchesATooltipType = function (tab, tooltipTypes) {
        return undefined;
    };
    InlineExtension.prototype.checkIfTabIsAVideoDomain = function (tab) {
        return false;
    };
    InlineExtension.prototype.checkIfTabIsOnWhitelistedUrl = function (tab) {
        return false;
    };
    InlineExtension.prototype.createWorker = function (tab) {
        return undefined;
    };
    InlineExtension.prototype.onFirstRun = function () {
    };
    return InlineExtension;
}(extensionBase_1.ExtensionBase));
exports.InlineExtension = InlineExtension;
