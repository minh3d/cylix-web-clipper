"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var iframeMessageHandler_1 = require("../../communicator/iframeMessageHandler");
var inlineMessageHandler_1 = require("../../communicator/inlineMessageHandler");
var Log = require("../../logging/log");
var clipperData_1 = require("../../storage/clipperData");
var localStorage_1 = require("../../storage/localStorage");
var clipperUrls_1 = require("../../clipperUrls");
var userInfo_1 = require("../../userInfo");
var extensionWorkerBase_1 = require("../extensionWorkerBase");
var invokeOptions_1 = require("../invokeOptions");
var invokeSource_1 = require("../invokeSource");
var InlineWorker = (function (_super) {
    __extends(InlineWorker, _super);
    function InlineWorker(clientInfo, auth) {
        var _this = this;
        var uiMessageHandlerThunk = function () { return new inlineMessageHandler_1.InlineMessageHandler(); };
        var injectMessageHandlerThunk = function () { return new iframeMessageHandler_1.IFrameMessageHandler(function () { return parent; }); };
        _this = _super.call(this, clientInfo, auth, new clipperData_1.ClipperData(new localStorage_1.LocalStorage()), uiMessageHandlerThunk, injectMessageHandlerThunk) || this;
        _this.logger.setContextProperty(Log.Context.Custom.InPrivateBrowsing, Log.unknownValue);
        var invokeOptions = {
            invokeMode: invokeOptions_1.InvokeMode.Default
        };
        _this.sendInvokeOptionsToInject(invokeOptions);
        // The inline worker gets created after the UI was successfully inject, so we can safely log this here
        _this.logger.logUserFunnel(Log.Funnel.Label.Invoke);
        _this.logClipperInvoke({
            invokeSource: invokeSource_1.InvokeSource.Bookmarklet
        }, invokeOptions);
        return _this;
    }
    InlineWorker.prototype.getUiMessageHandler = function () {
        return this.uiCommunicator.getMessageHandler();
    };
    InlineWorker.prototype.getUrl = function () {
        return "inline - unknown";
    };
    InlineWorker.prototype.invokeClipperBrowserSpecific = function () {
        return this.throwNotImplementedFailure();
    };
    InlineWorker.prototype.invokeDebugLoggingBrowserSpecific = function () {
        return this.throwNotImplementedFailure();
    };
    InlineWorker.prototype.invokeWhatsNewTooltipBrowserSpecific = function (newVersions) {
        return this.throwNotImplementedFailure();
    };
    InlineWorker.prototype.invokeTooltipBrowserSpecific = function () {
        return this.throwNotImplementedFailure();
    };
    InlineWorker.prototype.isAllowedFileSchemeAccessBrowserSpecific = function (callback) {
        callback(false);
    };
    InlineWorker.prototype.takeTabScreenshot = function () {
        return this.throwNotImplementedFailure();
    };
    /**
     * Launches the sign in window, rejecting with an error object if something went wrong on the server during
     * authentication. Otherwise, it resolves with true if the redirect endpoint was hit as a result of a successful
     * sign in attempt, and false if it was not hit (e.g., user manually closed the popup)
     */
    InlineWorker.prototype.doSignInAction = function (authType) {
        var usidQueryParamValue = this.getUserSessionIdQueryParamValue();
        var signInUrl = clipperUrls_1.ClipperUrls.generateSignInUrl(this.clientInfo.get().clipperId, usidQueryParamValue, userInfo_1.AuthType[authType]);
        return this.launchPopupAndWaitForClose(signInUrl);
    };
    /**
     * Signs the user out
     */
    InlineWorker.prototype.doSignOutAction = function (authType) {
        var usidQueryParamValue = this.getUserSessionIdQueryParamValue();
        var signOutUrl = clipperUrls_1.ClipperUrls.generateSignOutUrl(this.clientInfo.get().clipperId, usidQueryParamValue, userInfo_1.AuthType[authType]);
        var iframe = document.createElement("iframe");
        iframe.hidden = true;
        iframe.style.display = "none";
        iframe.src = signOutUrl;
        document.body.appendChild(iframe);
    };
    InlineWorker.prototype.throwNotImplementedFailure = function () {
        this.logger.logFailure(Log.Failure.Label.NotImplemented, Log.Failure.Type.Unexpected);
        throw new Error("not implemented");
    };
    return InlineWorker;
}(extensionWorkerBase_1.ExtensionWorkerBase));
exports.InlineWorker = InlineWorker;
