"use strict";
var iframeMessageHandler_1 = require("../../communicator/iframeMessageHandler");
var clipperInject_1 = require("../../extensions/clipperInject");
var debugLoggingInject_1 = require("../../extensions/debugLoggingInject");
var unsupportedBrowserInject_1 = require("../../extensions/unsupportedBrowserInject");
var browserUtils_1 = require("../../browserUtils");
var injectHelper_1 = require("../injectHelper");
if (browserUtils_1.BrowserUtils.browserNotSupported()) {
    unsupportedBrowserInject_1.UnsupportedBrowserInject.main();
}
else if (injectHelper_1.InjectHelper.isKnownUninjectablePage(window.location.href)) {
    injectHelper_1.InjectHelper.alertUserOfUnclippablePage();
}
else {
    var clipperUrl = bookmarkletRoot + "/clipper.html";
    var options = {
        frameUrl: clipperUrl,
        enableAddANote: false,
        enableEditableTitle: false,
        enableRegionClipping: false,
        useInlineBackgroundWorker: true
    };
    // We don't pass a messageHandler for the background Extension since we set useInlineBackgroundWorker above
    var clipperInject_2 = clipperInject_1.ClipperInject.main(options);
    // We need to pass the frame of the Clipper into the debug logging inject code as that's where the extension lives
    debugLoggingInject_1.DebugLoggingInject.main({
        extMessageHandlerThunk: function () {
            return new iframeMessageHandler_1.IFrameMessageHandler(function () { return clipperInject_2.getFrame().contentWindow; });
        }
    });
}
