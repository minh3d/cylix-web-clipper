"use strict";
var clientType_1 = require("../../clientType");
var webExtension_1 = require("../webExtensionBase/webExtension");
webExtension_1.WebExtension.browser = chrome;
var clipperBackground = new webExtension_1.WebExtension(clientType_1.ClientType.ChromeExtension, {
    debugLoggingInjectUrl: "chromeDebugLoggingInject.js",
    webClipperInjectUrl: "chromeInject.js",
    pageNavInjectUrl: "chromePageNavInject.js"
});
