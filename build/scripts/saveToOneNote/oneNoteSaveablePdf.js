"use strict";
var arrayUtils_1 = require("../arrayUtils");
var OneNoteSaveablePdf = (function () {
    function OneNoteSaveablePdf(page, pdf, pageIndexes, necessaryPdfOptions) {
        this.page = page;
        this.pdf = pdf;
        this.buckets = arrayUtils_1.ArrayUtils.partition(pageIndexes, OneNoteSaveablePdf.maxImagesPerPatchRequest);
        this.pageData = necessaryPdfOptions;
    }
    OneNoteSaveablePdf.prototype.getPage = function () {
        return Promise.resolve(this.page);
    };
    OneNoteSaveablePdf.prototype.getNumPages = function () {
        return 1;
    };
    OneNoteSaveablePdf.prototype.getPatch = function (index) {
        return this.pdf.getPageListAsDataUrls(this.buckets[index]).then(this.createPatchRequestBody);
    };
    OneNoteSaveablePdf.prototype.createPatchRequestBody = function (dataUrls) {
        var requestBody = [];
        dataUrls.forEach(function (dataUrl) {
            var content = "<p><img src=\"" + dataUrl + "\" /></p>&nbsp;";
            requestBody.push({
                target: "body",
                action: "append",
                content: content
            });
        });
        return requestBody;
    };
    OneNoteSaveablePdf.prototype.getNumPatches = function () {
        return this.buckets.length;
    };
    OneNoteSaveablePdf.prototype.getNumBatches = function () {
        return 0;
    };
    OneNoteSaveablePdf.prototype.getBatch = function (index) {
        return Promise.resolve(undefined);
    };
    return OneNoteSaveablePdf;
}());
OneNoteSaveablePdf.maxImagesPerPatchRequest = 15;
exports.OneNoteSaveablePdf = OneNoteSaveablePdf;
