"use strict";
var Log = require("../logging/log");
var constants_1 = require("../constants");
var stringUtils_1 = require("../stringUtils");
var clipMode_1 = require("../clipperUI/clipMode");
var frontEndGlobals_1 = require("../clipperUI/frontEndGlobals");
var augmentationHelper_1 = require("../contentCapture/augmentationHelper");
var domUtils_1 = require("../domParsers/domUtils");
/**
 * Solely responsible for logging clip attempts (note: attempts, not successes)
 */
var SaveToOneNoteLogger = (function () {
    function SaveToOneNoteLogger() {
    }
    SaveToOneNoteLogger.logClip = function (clipperState) {
        SaveToOneNoteLogger.logCommonClipModifications(clipperState);
        SaveToOneNoteLogger.logClipModeSpecific(clipperState);
    };
    SaveToOneNoteLogger.logCommonClipModifications = function (clipperState) {
        var event = new Log.Event.BaseEvent(Log.Event.Label.ClipCommonOptions);
        event.setCustomProperty(Log.PropertyName.Custom.ClipMode, clipMode_1.ClipMode[clipperState.currentMode.get()]);
        event.setCustomProperty(Log.PropertyName.Custom.PageTitleModified, clipperState.pageInfo.contentTitle !== clipperState.previewGlobalInfo.previewTitleText);
        event.setCustomProperty(Log.PropertyName.Custom.AnnotationAdded, !!clipperState.previewGlobalInfo.annotation);
        frontEndGlobals_1.Clipper.logger.logEvent(event);
    };
    SaveToOneNoteLogger.logClipModeSpecific = function (clipperState) {
        switch (clipperState.currentMode.get()) {
            default:
            case clipMode_1.ClipMode.Pdf:
                SaveToOneNoteLogger.logPdfClip(clipperState);
                break;
            case clipMode_1.ClipMode.FullPage:
                // Nothing to log
                break;
            case clipMode_1.ClipMode.Region:
                SaveToOneNoteLogger.logRegionClip(clipperState);
                break;
            case clipMode_1.ClipMode.Augmentation:
                SaveToOneNoteLogger.logAugmentationClip(clipperState);
                break;
            case clipMode_1.ClipMode.Bookmark:
                // Nothing to log
                break;
            case clipMode_1.ClipMode.Selection:
                SaveToOneNoteLogger.logSelectionClip(clipperState);
                break;
        }
    };
    SaveToOneNoteLogger.logPdfClip = function (clipperState) {
        SaveToOneNoteLogger.logPdfClipOptions(clipperState);
        SaveToOneNoteLogger.logPdfByteMetadata(clipperState);
    };
    SaveToOneNoteLogger.logPdfClipOptions = function (clipperState) {
        var event = new Log.Event.BaseEvent(Log.Event.Label.ClipPdfOptions);
        var pdfInfo = clipperState.pdfPreviewInfo;
        event.setCustomProperty(Log.PropertyName.Custom.PdfAllPagesClipped, pdfInfo.allPages);
        event.setCustomProperty(Log.PropertyName.Custom.PdfAttachmentClipped, pdfInfo.shouldAttachPdf);
        event.setCustomProperty(Log.PropertyName.Custom.PdfIsLocalFile, clipperState.pageInfo.rawUrl.indexOf("file:///") === 0);
        event.setCustomProperty(Log.PropertyName.Custom.PdfIsBatched, pdfInfo.shouldDistributePages);
        var totalPageCount = clipperState.pdfResult.data.get().viewportDimensions.length;
        var selectedPageCount = pdfInfo.allPages ? totalPageCount : Math.min(totalPageCount, stringUtils_1.StringUtils.countPageRange(pdfInfo.selectedPageRange));
        event.setCustomProperty(Log.PropertyName.Custom.PdfFileSelectedPageCount, selectedPageCount);
        event.setCustomProperty(Log.PropertyName.Custom.PdfFileTotalPageCount, totalPageCount);
        frontEndGlobals_1.Clipper.logger.logEvent(event);
    };
    SaveToOneNoteLogger.logPdfByteMetadata = function (clipperState) {
        var event = new Log.Event.BaseEvent(Log.Event.Label.PdfByteMetadata);
        var byteLength = clipperState.pdfResult.data.get().byteLength;
        event.setCustomProperty(Log.PropertyName.Custom.ByteLength, byteLength);
        event.setCustomProperty(Log.PropertyName.Custom.BytesPerPdfPage, byteLength / clipperState.pdfResult.data.get().pdf.numPages());
        frontEndGlobals_1.Clipper.logger.logEvent(event);
    };
    SaveToOneNoteLogger.logRegionClip = function (clipperState) {
        var event = new Log.Event.BaseEvent(Log.Event.Label.ClipRegionOptions);
        event.setCustomProperty(Log.PropertyName.Custom.NumRegions, clipperState.regionResult.data.length);
        frontEndGlobals_1.Clipper.logger.logEvent(event);
    };
    SaveToOneNoteLogger.logAugmentationClip = function (clipperState) {
        var event = new Log.Event.BaseEvent(Log.Event.Label.ClipAugmentationOptions);
        SaveToOneNoteLogger.setEditOptions(event, clipperState.previewGlobalInfo, clipperState.augmentationPreviewInfo);
        event.setCustomProperty(Log.PropertyName.Custom.AugmentationModel, augmentationHelper_1.AugmentationModel[clipperState.augmentationResult.data.ContentModel]);
        frontEndGlobals_1.Clipper.logger.logEvent(event);
    };
    SaveToOneNoteLogger.logSelectionClip = function (clipperState) {
        var event = new Log.Event.BaseEvent(Log.Event.Label.ClipSelectionOptions);
        SaveToOneNoteLogger.setEditOptions(event, clipperState.previewGlobalInfo, clipperState.selectionPreviewInfo);
        frontEndGlobals_1.Clipper.logger.logEvent(event);
    };
    SaveToOneNoteLogger.setEditOptions = function (event, previewGlobalInfo, previewInfo) {
        event.setCustomProperty(Log.PropertyName.Custom.FontSize, previewGlobalInfo.fontSize);
        event.setCustomProperty(Log.PropertyName.Custom.IsSerif, previewGlobalInfo.serif);
        // Log if the user has performed a highlight
        var container = document.createElement("div");
        container.innerHTML = domUtils_1.DomUtils.cleanHtml(previewInfo.previewBodyHtml);
        var highlightedList = container.getElementsByClassName(constants_1.Constants.Classes.highlighted);
        event.setCustomProperty(Log.PropertyName.Custom.ContainsAtLeastOneHighlight, highlightedList && highlightedList.length > 0);
    };
    return SaveToOneNoteLogger;
}());
exports.SaveToOneNoteLogger = SaveToOneNoteLogger;
