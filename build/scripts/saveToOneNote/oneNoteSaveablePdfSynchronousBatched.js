"use strict";
var stringUtils_1 = require("../stringUtils");
var objectUtils_1 = require("../objectUtils");
var OneNoteSaveablePdfSynchronousBatched = (function () {
    function OneNoteSaveablePdfSynchronousBatched(page, pdf, pageIndexes, contentLocale, titleText) {
        this.page = page;
        this.pdf = pdf;
        this.pageIndexes = pageIndexes;
        this.contentLocale = contentLocale;
        this.titleText = titleText;
    }
    /**
     * @index starts at 0 and refers to the pages already in the saveable, NOT THE PAGE OF THE PDF ITSELF
     */
    OneNoteSaveablePdfSynchronousBatched.prototype.getPage = function (index) {
        var _this = this;
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(index) || index === 0) {
            // They are asking for the first page
            return Promise.resolve(this.page);
        }
        // Let's say a user wants to clip page range "2-4,7-9""
        // This gets converted to 2,3,4,7,8,9 and is then 0-indexed to 1,2,3,6,7,8
        // When a SynchronousBatchedPdf object is constructed, the array [1,2,3,6,7,8] gets converted
        // to: this.page: [1], this.pageIndexes: [2,3,6,7,8].
        // Therefore, getPage(0) must return this.page (INDEX 1), as done above
        // Then, getPage(1) must return [2], which corresponds to pageIndexes[1 - 1]  == pageIndexes[0]
        // We have to subtract 1 to account for the first page being removed from the pageIndexes array
        var pageNumber = this.pageIndexes[index - 1];
        return this.pdf.getPageAsDataUrl(pageNumber).then(function (dataUrl) {
            return _this.createPage(dataUrl, pageNumber);
        });
    };
    OneNoteSaveablePdfSynchronousBatched.prototype.createPage = function (dataUrl, pageNumber) {
        var title = stringUtils_1.StringUtils.getBatchedPageTitle(this.titleText, pageNumber);
        var page = new OneNoteApi.OneNotePage(title, "", this.contentLocale);
        page.addOnml("<p><img src=\"" + dataUrl + "\" /></p>&nbsp;");
        return page;
    };
    OneNoteSaveablePdfSynchronousBatched.prototype.getNumPages = function () {
        // Add 1 to account for the initial page
        return this.pageIndexes.length + 1;
    };
    OneNoteSaveablePdfSynchronousBatched.prototype.getNumPatches = function () {
        return 0;
    };
    OneNoteSaveablePdfSynchronousBatched.prototype.getPatch = function (index) {
        return Promise.resolve(undefined);
    };
    OneNoteSaveablePdfSynchronousBatched.prototype.getNumBatches = function () {
        return 0;
    };
    OneNoteSaveablePdfSynchronousBatched.prototype.getBatch = function (index) {
        return Promise.resolve(undefined);
    };
    return OneNoteSaveablePdfSynchronousBatched;
}());
exports.OneNoteSaveablePdfSynchronousBatched = OneNoteSaveablePdfSynchronousBatched;
