"use strict";
var constants_1 = require("../constants");
var promiseUtils_1 = require("../promiseUtils");
var settings_1 = require("../settings");
var frontEndGlobals_1 = require("../clipperUI/frontEndGlobals");
var clipperStorageKeys_1 = require("../storage/clipperStorageKeys");
var Log = require("../logging/log");
var oneNoteApiWithLogging_1 = require("./oneNoteApiWithLogging");
var oneNoteApiWithRetries_1 = require("./oneNoteApiWithRetries");
var _ = require("lodash");
/**
 * Solely responsible for saving the user's OneNote pages
 */
var SaveToOneNote = (function () {
    function SaveToOneNote(accessToken) {
        this.accessToken = accessToken;
    }
    /**
     * Saves a page (and if necessary, appends PATCHES) to OneNote
     */
    SaveToOneNote.prototype.save = function (options) {
        var _this = this;
        if (options.page.getNumPages() > 1) {
            return this.saveMultiplePagesSynchronously(options);
        }
        else {
            if (options.page.getNumPatches() > 0) {
                return this.rejectIfNoPatchPermissions(options.saveLocation).then(function () {
                    return _this.saveWithoutCheckingPatchPermissions(options);
                });
            }
            else {
                return this.saveWithoutCheckingPatchPermissions(options);
            }
        }
    };
    SaveToOneNote.prototype.saveMultiplePagesSynchronously = function (options) {
        var _this = this;
        var progressCallback = options.progressCallback ? options.progressCallback : function () { };
        progressCallback(0, options.page.getNumPages());
        return options.page.getPage().then(function (page) {
            return _this.getApi().createPage(page, options.saveLocation).then(function (responsePackage) {
                return _this.synchronouslyCreateMultiplePages(options, progressCallback).then(function () {
                    return Promise.resolve(responsePackage);
                });
            });
        });
    };
    SaveToOneNote.prototype.synchronouslyCreateMultiplePages = function (options, progressCallback) {
        var _this = this;
        if (progressCallback === void 0) { progressCallback = function () { }; }
        var saveable = options.page;
        var end = saveable.getNumPages();
        // We start the range at 1 since we have already included the first page
        return _.range(1, end).reduce(function (chainedPromise, i) {
            return chainedPromise = chainedPromise.then(function () {
                return new Promise(function (resolve, reject) {
                    // Parallelize the POST request intervals with the fetching of current dataUrl
                    var getPagePromise = _this.getPageWithLogging(saveable, i);
                    var timeoutPromise = promiseUtils_1.PromiseUtils.wait(SaveToOneNote.timeBetweenPostRequests);
                    Promise.all([getPagePromise, timeoutPromise]).then(function (values) {
                        var page = values[0];
                        _this.getApi().createPage(page, options.saveLocation).then(function () {
                            progressCallback(i, end);
                            resolve();
                        })["catch"](function (error) {
                            reject(error);
                        });
                    });
                });
            });
        }, Promise.resolve());
    };
    SaveToOneNote.prototype.rejectIfNoPatchPermissions = function (saveLocation) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            frontEndGlobals_1.Clipper.getStoredValue(clipperStorageKeys_1.ClipperStorageKeys.hasPatchPermissions, function (hasPermissions) {
                // We have checked their permissions successfully in the past, or the user signed in on this device (with the latest scope)
                if (hasPermissions) {
                    resolve();
                }
                else {
                    // As of v3.2.9, we have added a new scope for MSA to allow for PATCHing, however currently-logged-in users will not have
                    // this scope, so this call is a workaround to check for permissions, but is very unperformant. We need to investigate a
                    // quicker way of doing this ... perhaps exposing an endpoint that we can use for this sole purpose.
                    _this.getApi().getPages({ top: 1, sectionId: saveLocation }).then(function () {
                        frontEndGlobals_1.Clipper.storeValue(clipperStorageKeys_1.ClipperStorageKeys.hasPatchPermissions, "true");
                        resolve();
                    })["catch"](function (error) {
                        reject(error);
                    });
                }
            });
        });
    };
    SaveToOneNote.prototype.saveWithoutCheckingPatchPermissions = function (options) {
        var _this = this;
        // options.page is a misnomer, as its the saveable, not a specific page
        return options.page.getPage().then(function (page) {
            return _this.getApi().createPage(page, options.saveLocation).then(function (responsePackage) {
                if (options.page.getNumPatches() > 0) {
                    var pageId = responsePackage.parsedResponse.id;
                    return _this.patch(pageId, options.page).then(function () {
                        return Promise.resolve(responsePackage);
                    });
                }
                else if (options.page.getNumBatches() > 0) {
                    return _this.batch(options.page).then(function () {
                        return Promise.resolve(responsePackage);
                    });
                }
                else {
                    return Promise.resolve(responsePackage);
                }
            });
        });
    };
    SaveToOneNote.prototype.batch = function (saveable) {
        var _this = this;
        var timeBetweenBatchRequests = SaveToOneNote.timeBeforeFirstBatch;
        return _.range(saveable.getNumBatches()).reduce(function (chainedPromise, i) {
            return chainedPromise = chainedPromise.then(function () {
                return new Promise(function (resolve, reject) {
                    // Parallelize the BATCH request intervals with the fetching of the next set of dataUrls
                    var getRevisionsPromise = _this.getBatchWithLogging(saveable, i);
                    var timeoutPromise = promiseUtils_1.PromiseUtils.wait(timeBetweenBatchRequests);
                    Promise.all([getRevisionsPromise, timeoutPromise]).then(function (values) {
                        var batchRequest = values[0];
                        _this.getApi().sendBatchRequest(batchRequest).then(function () {
                            resolve();
                        })["catch"](function (error) {
                            reject(error);
                        });
                    });
                });
            });
        }, Promise.resolve());
    };
    SaveToOneNote.prototype.patch = function (pageId, saveable) {
        var _this = this;
        // As of 10/27/16, the page is not always ready when the 200 is returned, so we wait a bit, and then getPageContent with retries
        // When the getPageContent returns a 200, we start PATCHing the page.
        var timeBetweenPatchRequests = SaveToOneNote.timeBeforeFirstPatch;
        return Promise.all([
            _.range(saveable.getNumPatches()).reduce(function (chainedPromise, i) {
                return chainedPromise = chainedPromise.then(function () {
                    return new Promise(function (resolve, reject) {
                        // OneNote API returns 204 on a PATCH request when it receives it, but we have no way of telling when it actually
                        // completes processing, so we add an artificial timeout before the next PATCH to try and ensure that they get
                        // processed in the order that they were sent.
                        // Parallelize the PATCH request intervals with the fetching of the next set of dataUrls
                        var getRevisionsPromise = _this.getPatchWithLogging(saveable, i);
                        var timeoutPromise = promiseUtils_1.PromiseUtils.wait(timeBetweenPatchRequests);
                        Promise.all([getRevisionsPromise, timeoutPromise]).then(function (values) {
                            var revisions = values[0];
                            var thenCb = function () {
                                timeBetweenPatchRequests = SaveToOneNote.timeBetweenPatchRequests;
                                resolve();
                            };
                            var catchCb = function (error) {
                                if (error.statusCode === 401) {
                                    // The clip has taken a really long time and the user token has expired
                                    frontEndGlobals_1.Clipper.getExtensionCommunicator().callRemoteFunction(constants_1.Constants.FunctionKeys.ensureFreshUserBeforeClip, {
                                        callback: function (updatedUser) {
                                            if (updatedUser.user.accessToken) {
                                                // Try again with the new token
                                                _this.accessToken = updatedUser.user.accessToken;
                                                _this.updatePage(pageId, revisions, thenCb, catchCb);
                                            }
                                            else {
                                                reject(error);
                                            }
                                        }
                                    });
                                }
                                else {
                                    reject(error);
                                }
                            };
                            _this.updatePage(pageId, revisions, thenCb, catchCb);
                        });
                    });
                });
            }, this.getApi().getPageContent(pageId)) // Check if page exists with retries
        ]);
    };
    SaveToOneNote.prototype.updatePage = function (pageId, revisions, thenCb, catchCb) {
        this.getApi().updatePage(pageId, revisions)
            .then(thenCb)["catch"](catchCb);
    };
    // We try not and put logging logic in this class, but since we lazy-load images, this has to be an exception
    SaveToOneNote.prototype.getPatchWithLogging = function (saveable, index) {
        var event = new Log.Event.PromiseEvent(Log.Event.Label.ProcessPdfIntoDataUrls);
        return saveable.getPatch(index).then(function (revisions) {
            event.stopTimer();
            var numPages = revisions.length;
            event.setCustomProperty(Log.PropertyName.Custom.NumPages, numPages);
            if (revisions.length > 0) {
                // There's some html in the content itself, but it's negligible compared to the length of the actual dataUrls
                var lengthOfDataUrls = _.sumBy(revisions, function (revision) { return revision.content.length; });
                event.setCustomProperty(Log.PropertyName.Custom.ByteLength, lengthOfDataUrls);
                event.setCustomProperty(Log.PropertyName.Custom.BytesPerPdfPage, lengthOfDataUrls / numPages);
                event.setCustomProperty(Log.PropertyName.Custom.AverageProcessingDurationPerPage, event.getDuration() / numPages);
            }
            frontEndGlobals_1.Clipper.logger.logEvent(event);
            return Promise.resolve(revisions);
        });
    };
    SaveToOneNote.prototype.getPageWithLogging = function (saveable, index) {
        var event = new Log.Event.PromiseEvent(Log.Event.Label.ProcessPdfIntoDataUrls);
        return saveable.getPage(index).then(function (page) {
            event.stopTimer();
            var numPages = 1;
            event.setCustomProperty(Log.PropertyName.Custom.NumPages, numPages);
            var lengthOfDataUrls = page.getEntireOnml().length;
            event.setCustomProperty(Log.PropertyName.Custom.ByteLength, lengthOfDataUrls);
            event.setCustomProperty(Log.PropertyName.Custom.BytesPerPdfPage, lengthOfDataUrls / numPages);
            event.setCustomProperty(Log.PropertyName.Custom.AverageProcessingDurationPerPage, event.getDuration() / numPages);
            frontEndGlobals_1.Clipper.logger.logEvent(event);
            return Promise.resolve(page);
        });
    };
    SaveToOneNote.prototype.getBatchWithLogging = function (saveable, index) {
        var event = new Log.Event.PromiseEvent(Log.Event.Label.ProcessPdfIntoDataUrls);
        return saveable.getBatch(index).then(function (batchRequest) {
            event.stopTimer();
            var numPages = batchRequest.getNumOperations();
            event.setCustomProperty(Log.PropertyName.Custom.NumPages, numPages);
            if (numPages > 0) {
                // There's some html in the content itself, but it's negligible compared to the length of the actual dataUrls
                var batchRequestOperations = _.range(numPages).map(function (pageNumber) { return batchRequest.getOperation(pageNumber); });
                var lengthOfDataUrls = _.sumBy(batchRequestOperations, function (op) { return op.content.length; });
                event.setCustomProperty(Log.PropertyName.Custom.ByteLength, lengthOfDataUrls);
                event.setCustomProperty(Log.PropertyName.Custom.BytesPerPdfPage, lengthOfDataUrls / numPages);
                event.setCustomProperty(Log.PropertyName.Custom.AverageProcessingDurationPerPage, event.getDuration() / numPages);
            }
            frontEndGlobals_1.Clipper.logger.logEvent(event);
            return Promise.resolve(batchRequest);
        });
    };
    /**
     * We set the correlation id each time this gets called. When using this, make sure you are not reusing
     * the same object unless it's your intention to have their API calls use the same correlation id.
     */
    SaveToOneNote.prototype.getApi = function () {
        var headers = {};
        headers[constants_1.Constants.HeaderValues.appIdKey] = settings_1.Settings.getSetting("App_Id");
        headers[constants_1.Constants.HeaderValues.userSessionIdKey] = frontEndGlobals_1.Clipper.getUserSessionId();
        var api = new OneNoteApi.OneNoteApi(this.accessToken, undefined /* timeout */, headers);
        var apiWithRetries = new oneNoteApiWithRetries_1.OneNoteApiWithRetries(api);
        return new oneNoteApiWithLogging_1.OneNoteApiWithLogging(apiWithRetries);
    };
    return SaveToOneNote;
}());
SaveToOneNote.timeBeforeFirstPatch = 1000;
SaveToOneNote.timeBetweenPatchRequests = 7000;
SaveToOneNote.timeBeforeFirstBatch = 1000;
SaveToOneNote.timeBetweenBatchRequests = 7000;
SaveToOneNote.timeBeforeFirstPost = 1000;
SaveToOneNote.timeBetweenPostRequests = 0;
exports.SaveToOneNote = SaveToOneNote;
