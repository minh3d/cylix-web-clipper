"use strict";
var constants_1 = require("../constants");
var frontEndGlobals_1 = require("../clipperUI/frontEndGlobals");
var OneNoteApiUtils_1 = require("../clipperUI/OneNoteApiUtils");
var Log = require("../logging/log");
var OneNoteApiWithLogging = (function () {
    function OneNoteApiWithLogging(api) {
        this.api = api;
    }
    OneNoteApiWithLogging.prototype.createNotebook = function (name) {
        var _this = this;
        return this.executeWithLogging(function () {
            return _this.api.createNotebook(name);
        }, Log.Event.Label.CreateNotebook);
    };
    OneNoteApiWithLogging.prototype.createPage = function (page, sectionId) {
        var _this = this;
        return this.executeWithLogging(function () {
            return _this.api.createPage(page, sectionId);
        }, Log.Event.Label.CreatePage);
    };
    OneNoteApiWithLogging.prototype.sendBatchRequest = function (batchRequest) {
        var _this = this;
        return this.executeWithLogging(function () {
            return _this.api.sendBatchRequest(batchRequest);
        }, Log.Event.Label.SendBatchRequest);
    };
    OneNoteApiWithLogging.prototype.getPage = function (pageId) {
        var _this = this;
        return this.executeWithLogging(function () {
            return _this.api.getPage(pageId);
        }, Log.Event.Label.GetPage);
    };
    OneNoteApiWithLogging.prototype.getPageContent = function (pageId) {
        var _this = this;
        return this.executeWithLogging(function () {
            return _this.api.getPageContent(pageId);
        }, Log.Event.Label.GetPageContent);
    };
    OneNoteApiWithLogging.prototype.getPages = function (options) {
        var _this = this;
        return this.executeWithLogging(function () {
            return _this.api.getPages(options);
        }, Log.Event.Label.GetPages);
    };
    OneNoteApiWithLogging.prototype.updatePage = function (pageId, revisions) {
        var _this = this;
        return this.executeWithLogging(function () {
            return _this.api.updatePage(pageId, revisions);
        }, Log.Event.Label.UpdatePage);
    };
    OneNoteApiWithLogging.prototype.createSection = function (notebookId, name) {
        var _this = this;
        return this.executeWithLogging(function () {
            return _this.api.createSection(notebookId, name);
        }, Log.Event.Label.CreateSection);
    };
    OneNoteApiWithLogging.prototype.getNotebooks = function (excludeReadOnlyNotebooks) {
        var _this = this;
        return this.executeWithLogging(function () {
            return _this.api.getNotebooks(excludeReadOnlyNotebooks);
        }, Log.Event.Label.GetNotebooks);
    };
    OneNoteApiWithLogging.prototype.getNotebooksWithExpandedSections = function (expands, excludeReadOnlyNotebooks) {
        var _this = this;
        return this.executeWithLogging(function () {
            return _this.api.getNotebooksWithExpandedSections(expands, excludeReadOnlyNotebooks);
        }, Log.Event.Label.GetNotebooks /* re-use same label as getNotebooks */);
    };
    OneNoteApiWithLogging.prototype.getNotebookByName = function (name) {
        var _this = this;
        return this.executeWithLogging(function () {
            return _this.api.getNotebookByName(name);
        }, Log.Event.Label.GetNotebookByName);
    };
    OneNoteApiWithLogging.prototype.pagesSearch = function (query) {
        var _this = this;
        return this.executeWithLogging(function () {
            return _this.api.pagesSearch(query);
        }, Log.Event.Label.PagesSearch);
    };
    OneNoteApiWithLogging.prototype.executeWithLogging = function (func, eventLabel) {
        return new Promise(function (resolve, reject) {
            var event = new Log.Event.PromiseEvent(eventLabel);
            var correlationId;
            return func().then(function (response) {
                event.setCustomProperty(Log.PropertyName.Custom.CorrelationId, response.request.getResponseHeader(constants_1.Constants.HeaderValues.correlationId));
                resolve(response);
            }, function (error) {
                OneNoteApiUtils_1.OneNoteApiUtils.logOneNoteApiRequestError(event, error);
                reject(error);
            }).then(function () {
                frontEndGlobals_1.Clipper.logger.logEvent(event);
            });
        });
    };
    return OneNoteApiWithLogging;
}());
exports.OneNoteApiWithLogging = OneNoteApiWithLogging;
