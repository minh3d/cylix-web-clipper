"use strict";
var promiseUtils_1 = require("../promiseUtils");
var OneNoteApiWithRetries = (function () {
    function OneNoteApiWithRetries(api, maxRetries) {
        if (maxRetries === void 0) { maxRetries = 3; }
        this.api = api;
        this.maxRetries = maxRetries;
    }
    OneNoteApiWithRetries.prototype.createNotebook = function (name) {
        var _this = this;
        return promiseUtils_1.PromiseUtils.execWithRetry(function () {
            return _this.api.createNotebook(name);
        });
    };
    OneNoteApiWithRetries.prototype.createPage = function (page, sectionId) {
        var _this = this;
        return promiseUtils_1.PromiseUtils.execWithRetry(function () {
            return _this.api.createPage(page, sectionId);
        });
    };
    // TODO: call this sendBatch or somethin to differentiate it
    OneNoteApiWithRetries.prototype.sendBatchRequest = function (batchRequest) {
        var _this = this;
        return promiseUtils_1.PromiseUtils.execWithRetry(function () {
            return _this.api.sendBatchRequest(batchRequest);
        });
    };
    OneNoteApiWithRetries.prototype.getPage = function (pageId) {
        var _this = this;
        return promiseUtils_1.PromiseUtils.execWithRetry(function () {
            return _this.api.getPage(pageId);
        });
    };
    OneNoteApiWithRetries.prototype.getPageContent = function (pageId) {
        var _this = this;
        return promiseUtils_1.PromiseUtils.execWithRetry(function () {
            return _this.api.getPageContent(pageId);
        });
    };
    OneNoteApiWithRetries.prototype.getPages = function (options) {
        var _this = this;
        return promiseUtils_1.PromiseUtils.execWithRetry(function () {
            return _this.api.getPages(options);
        });
    };
    OneNoteApiWithRetries.prototype.updatePage = function (pageId, revisions) {
        var _this = this;
        return promiseUtils_1.PromiseUtils.execWithRetry(function () {
            return _this.api.updatePage(pageId, revisions);
        });
    };
    OneNoteApiWithRetries.prototype.createSection = function (notebookId, name) {
        var _this = this;
        return promiseUtils_1.PromiseUtils.execWithRetry(function () {
            return _this.api.createSection(notebookId, name);
        });
    };
    OneNoteApiWithRetries.prototype.getNotebooks = function (excludeReadOnlyNotebooks) {
        var _this = this;
        return promiseUtils_1.PromiseUtils.execWithRetry(function () {
            return _this.api.getNotebooks(excludeReadOnlyNotebooks);
        });
    };
    OneNoteApiWithRetries.prototype.getNotebooksWithExpandedSections = function (expands, excludeReadOnlyNotebooks) {
        var _this = this;
        return promiseUtils_1.PromiseUtils.execWithRetry(function () {
            return _this.api.getNotebooksWithExpandedSections(expands, excludeReadOnlyNotebooks);
        });
    };
    OneNoteApiWithRetries.prototype.getNotebookByName = function (name) {
        var _this = this;
        return promiseUtils_1.PromiseUtils.execWithRetry(function () {
            return _this.api.getNotebookByName(name);
        });
    };
    OneNoteApiWithRetries.prototype.pagesSearch = function (query) {
        var _this = this;
        return promiseUtils_1.PromiseUtils.execWithRetry(function () {
            return _this.api.pagesSearch(query);
        });
    };
    return OneNoteApiWithRetries;
}());
exports.OneNoteApiWithRetries = OneNoteApiWithRetries;
