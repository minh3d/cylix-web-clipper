"use strict";
var OneNoteSaveablePage = (function () {
    function OneNoteSaveablePage(page) {
        this.page = page;
    }
    OneNoteSaveablePage.prototype.getPage = function () {
        return Promise.resolve(this.page);
    };
    OneNoteSaveablePage.prototype.getNumPages = function () {
        return 1;
    };
    OneNoteSaveablePage.prototype.getPatch = function (index) {
        return Promise.resolve(undefined);
    };
    OneNoteSaveablePage.prototype.getNumPatches = function () {
        return 0;
    };
    OneNoteSaveablePage.prototype.getBatch = function (index) {
        return Promise.resolve(undefined);
    };
    OneNoteSaveablePage.prototype.getNumBatches = function () {
        return 0;
    };
    return OneNoteSaveablePage;
}());
exports.OneNoteSaveablePage = OneNoteSaveablePage;
