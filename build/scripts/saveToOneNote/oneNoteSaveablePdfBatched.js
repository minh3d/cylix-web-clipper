"use strict";
var arrayUtils_1 = require("../arrayUtils");
var stringUtils_1 = require("../stringUtils");
var OneNoteSaveablePdfBatched = (function () {
    function OneNoteSaveablePdfBatched(page, pdf, pageIndexes, contentLocale, saveLocation, titleText) {
        this.page = page;
        this.pdf = pdf;
        this.buckets = arrayUtils_1.ArrayUtils.partition(pageIndexes, OneNoteSaveablePdfBatched.maxImagesPerPatchRequest);
        this.contentLocale = contentLocale;
        this.saveLocation = saveLocation;
        this.titleText = titleText;
    }
    OneNoteSaveablePdfBatched.prototype.getPage = function () {
        return Promise.resolve(this.page);
    };
    OneNoteSaveablePdfBatched.prototype.getNumPages = function () {
        return 1;
    };
    OneNoteSaveablePdfBatched.prototype.getNumPatches = function () {
        return 0;
    };
    OneNoteSaveablePdfBatched.prototype.getPatch = function (index) {
        return Promise.resolve(undefined);
    };
    OneNoteSaveablePdfBatched.prototype.getNumBatches = function () {
        return this.buckets.length;
    };
    OneNoteSaveablePdfBatched.prototype.getBatch = function (index) {
        var _this = this;
        var bucket = this.buckets[index];
        return this.pdf.getPageListAsDataUrls(bucket).then(function (dataUrls) {
            return _this.createBatchRequest(bucket, dataUrls);
        });
    };
    OneNoteSaveablePdfBatched.prototype.createBatchRequest = function (pagesIndices, dataUrls) {
        var sectionId = this.saveLocation;
        var apiRoot = "/api/v1.0/me/notes";
        var sectionPath = sectionId ? "/sections/" + sectionId : "";
        var url = apiRoot + sectionPath + "/pages";
        var batchRequest = new OneNoteApi.BatchRequest();
        for (var i = 0; i < pagesIndices.length; i++) {
            var title = stringUtils_1.StringUtils.getBatchedPageTitle(this.titleText, pagesIndices[i]);
            var dataUrl = dataUrls[i];
            var page = new OneNoteApi.OneNotePage(title, "", this.contentLocale);
            page.addOnml("<p><img src=\"" + dataUrl + "\" /></p>&nbsp;");
            var batchRequestOperation = {
                httpMethod: "POST",
                uri: url,
                contentType: "text/html",
                content: page.getEntireOnml()
            };
            batchRequest.addOperation(batchRequestOperation);
        }
        return batchRequest;
    };
    return OneNoteSaveablePdfBatched;
}());
OneNoteSaveablePdfBatched.maxImagesPerPatchRequest = 15;
exports.OneNoteSaveablePdfBatched = OneNoteSaveablePdfBatched;
