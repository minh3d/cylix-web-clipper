"use strict";
var constants_1 = require("../constants");
var operationResult_1 = require("../operationResult");
var stringUtils_1 = require("../stringUtils");
var urlUtils_1 = require("../urlUtils");
var clipMode_1 = require("../clipperUI/clipMode");
var domUtils_1 = require("../domParsers/domUtils");
var localization_1 = require("../localization/localization");
var oneNoteSaveablePage_1 = require("./oneNoteSaveablePage");
var oneNoteSaveablePdf_1 = require("./oneNoteSaveablePdf");
var oneNoteSaveablePdfSynchronousBatched_1 = require("./oneNoteSaveablePdfSynchronousBatched");
var _ = require("lodash");
var OneNoteSaveableFactory = (function () {
    function OneNoteSaveableFactory(clipperState) {
        this.clipperState = clipperState;
    }
    OneNoteSaveableFactory.prototype.getSaveable = function () {
        var _this = this;
        var page = this.getInitialPage();
        this.addAnnotation(page);
        this.addClippedFromUrlToPage(page);
        return this.addPrimaryContent(page).then(function () {
            return _this.pageToSaveable(page);
        });
    };
    OneNoteSaveableFactory.prototype.getInitialPage = function () {
        var title = this.clipperState.previewGlobalInfo.previewTitleText;
        if (this.clipperState.currentMode.get() === clipMode_1.ClipMode.Pdf && this.clipperState.pdfPreviewInfo.shouldDistributePages) {
            var firstPageIndex = this.getPageIndicesToSendInPdfMode()[0];
            title = stringUtils_1.StringUtils.getBatchedPageTitle(title, firstPageIndex);
        }
        return new OneNoteApi.OneNotePage(title, "", this.clipperState.pageInfo.contentLocale, this.getMetaData());
    };
    OneNoteSaveableFactory.prototype.getMetaData = function () {
        if (this.clipperState.currentMode.get() === clipMode_1.ClipMode.Augmentation &&
            this.clipperState.augmentationResult.data &&
            this.clipperState.augmentationResult.data.PageMetadata) {
            return this.clipperState.augmentationResult.data.PageMetadata;
        }
        return undefined;
    };
    OneNoteSaveableFactory.prototype.addAnnotation = function (page) {
        var annotation = this.clipperState.previewGlobalInfo.annotation;
        if (annotation && annotation.length > 0) {
            var annotationWithQuotes = '"' + annotation + '"';
            var encodedAnnotation = page.escapeHtmlEntities(annotationWithQuotes);
            var formattedAnnotation = this.createPostProcessessedHtml("<div>" + encodedAnnotation + "</div>");
            page.addOnml(formattedAnnotation.outerHTML);
        }
    };
    OneNoteSaveableFactory.prototype.createPostProcessessedHtml = function (html) {
        // Wrap the preview in in-line styling to persist the styling through the OneNote API
        var newPreviewBody = document.createElement("div");
        newPreviewBody.innerHTML = domUtils_1.DomUtils.cleanHtml(html);
        var fontSize = this.clipperState.previewGlobalInfo.fontSize.toString() + "px";
        var fontFamilyString = this.clipperState.previewGlobalInfo.serif ? "WebClipper.FontFamily.Preview.SerifDefault" : "WebClipper.FontFamily.Preview.SansSerifDefault";
        var fontFamily = localization_1.Localization.getLocalizedString(fontFamilyString);
        var fontStyleString = "font-size: " + fontSize + "; font-family: " + fontFamily + ";";
        newPreviewBody.setAttribute("style", fontStyleString);
        // Tables don't inherit styles on the outer div in OneNote API
        var tables = newPreviewBody.getElementsByTagName("table");
        for (var i = 0; i < tables.length; i++) {
            var table = tables[i];
            table.setAttribute("style", fontStyleString);
        }
        this.stripUnwantedUIElements(newPreviewBody);
        return newPreviewBody;
    };
    OneNoteSaveableFactory.prototype.stripUnwantedUIElements = function (elem) {
        // Getting a list of buttons and deleting them one-by-one does not work due to a weird index/dereference issue, so we do it this way
        while (true) {
            var deleteHighlightButton = elem.querySelector("." + constants_1.Constants.Classes.deleteHighlightButton);
            if (!deleteHighlightButton) {
                break;
            }
            deleteHighlightButton.parentNode.removeChild(deleteHighlightButton);
        }
    };
    OneNoteSaveableFactory.prototype.addClippedFromUrlToPage = function (page) {
        if (this.clipperState.currentMode.get() !== clipMode_1.ClipMode.Bookmark) {
            var sourceUrlCitation = localization_1.Localization.getLocalizedString("WebClipper.FromCitation")
                .replace("{0}", '<a href="' + (this.clipperState.pageInfo.rawUrl) + '">' + this.clipperState.pageInfo.rawUrl + "</a>");
            var formattedCitation = this.createPostProcessessedHtml(sourceUrlCitation);
            page.addOnml(formattedCitation.outerHTML);
        }
    };
    OneNoteSaveableFactory.prototype.addPrimaryContent = function (page) {
        switch (this.clipperState.currentMode.get()) {
            default:
            case clipMode_1.ClipMode.Pdf:
                if (this.clipperState.pdfPreviewInfo.shouldAttachPdf && this.clipperState.pdfResult.data.get().byteLength < constants_1.Constants.Settings.maximumMimeSizeLimit) {
                    return this.addPdfAttachment(page);
                }
                break;
            case clipMode_1.ClipMode.FullPage:
                page.addHtml(this.clipperState.pageInfo.contentData);
                break;
            case clipMode_1.ClipMode.Region:
                for (var _i = 0, _a = this.clipperState.regionResult.data; _i < _a.length; _i++) {
                    var regionDataUrl = _a[_i];
                    // TODO: The API currently does not correctly space paragraphs. We need to remove "&nbsp;" when its fixed.
                    page.addOnml("<p><img src=\"" + regionDataUrl + "\" /></p>&nbsp;");
                }
                break;
            case clipMode_1.ClipMode.Augmentation:
                var processedAugmentedContent = this.createPostProcessessedHtml(this.clipperState.augmentationPreviewInfo.previewBodyHtml);
                page.addOnml(processedAugmentedContent.outerHTML);
                break;
            case clipMode_1.ClipMode.Bookmark:
                var processedBookmarkContent = this.createPostProcessessedHtml(this.clipperState.bookmarkPreviewInfo.previewBodyHtml);
                page.addOnml(processedBookmarkContent.outerHTML);
                break;
            case clipMode_1.ClipMode.Selection:
                var processedSelectedContent = this.createPostProcessessedHtml(this.clipperState.selectionPreviewInfo.previewBodyHtml);
                page.addOnml(processedSelectedContent.outerHTML);
                break;
        }
        return Promise.resolve();
    };
    OneNoteSaveableFactory.prototype.addPdfAttachment = function (page) {
        var _this = this;
        var pdf = this.clipperState.pdfResult.data.get().pdf;
        return pdf.getData().then(function (buffer) {
            if (buffer) {
                var attachmentName = urlUtils_1.UrlUtils.getFileNameFromUrl(_this.clipperState.pageInfo.rawUrl, "Original.pdf");
                page.addAttachment(buffer, attachmentName);
            }
            return Promise.resolve();
        });
    };
    OneNoteSaveableFactory.prototype.pageToSaveable = function (page) {
        var _this = this;
        if (this.clipperState.currentMode.get() === clipMode_1.ClipMode.Pdf) {
            var pdf_1 = this.clipperState.pdfResult.data.get().pdf;
            var pageIndexes_1 = this.getPageIndicesToSendInPdfMode();
            if (this.clipperState.pdfPreviewInfo.shouldDistributePages) {
                return pdf_1.getPageAsDataUrl(pageIndexes_1[0]).then(function (dataUrl) {
                    page.addOnml("<p><img src=\"" + dataUrl + "\" /></p>&nbsp;");
                    // We have added the first image to the createPage call, so we remove the first page from the indices we send in the BATCH
                    return new oneNoteSaveablePdfSynchronousBatched_1.OneNoteSaveablePdfSynchronousBatched(page, pdf_1, pageIndexes_1.slice(1), _this.clipperState.pageInfo.contentLocale, _this.clipperState.previewGlobalInfo.previewTitleText);
                });
            }
            else {
                return Promise.resolve(new oneNoteSaveablePdf_1.OneNoteSaveablePdf(page, pdf_1, pageIndexes_1));
            }
        }
        return Promise.resolve(new oneNoteSaveablePage_1.OneNoteSaveablePage(page));
    };
    OneNoteSaveableFactory.prototype.getPageIndicesToSendInPdfMode = function () {
        var pdf = this.clipperState.pdfResult.data.get().pdf;
        if (this.clipperState.pdfPreviewInfo.allPages) {
            return _.range(pdf.numPages());
        }
        var parsePageRangeOperation = stringUtils_1.StringUtils.parsePageRange(this.clipperState.pdfPreviewInfo.selectedPageRange, pdf.numPages());
        if (parsePageRangeOperation.status !== operationResult_1.OperationResult.Succeeded) {
            throw new Error("User is clipping an invalid page range");
        }
        var pageRange = parsePageRangeOperation.result;
        return pageRange.map(function (value) { return value - 1; });
    };
    return OneNoteSaveableFactory;
}());
OneNoteSaveableFactory.maxImagesPerPatchRequest = 15;
exports.OneNoteSaveableFactory = OneNoteSaveableFactory;
