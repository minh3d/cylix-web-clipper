"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var sessionLogger_1 = require("./sessionLogger");
/**
 * A session logger that no-ops on every call.
 */
var StubSessionLogger = (function (_super) {
    __extends(StubSessionLogger, _super);
    function StubSessionLogger() {
        return _super.call(this) || this;
    }
    StubSessionLogger.prototype.handleSetUserSessionId = function (sessionId) { return undefined; };
    StubSessionLogger.prototype.logEvent = function (event) { };
    StubSessionLogger.prototype.logFailure = function (label, failureType, failureInfo, id) { };
    StubSessionLogger.prototype.logUserFunnel = function (label) { };
    StubSessionLogger.prototype.logSessionStart = function () { };
    StubSessionLogger.prototype.logSessionEnd = function (endTrigger) { };
    StubSessionLogger.prototype.logTrace = function (label, level, message) { };
    StubSessionLogger.prototype.pushToStream = function (label, value) { };
    StubSessionLogger.prototype.logClickEvent = function (clickId) { };
    StubSessionLogger.prototype.setContextProperty = function (key, value) { };
    StubSessionLogger.prototype.handleClickEvent = function (clickId) { };
    StubSessionLogger.prototype.handleEvent = function (event) { };
    StubSessionLogger.prototype.handleEventPure = function (event) { };
    StubSessionLogger.prototype.handleSessionStart = function () { };
    StubSessionLogger.prototype.handleSessionEnd = function (endTrigger) { };
    StubSessionLogger.prototype.handleFailure = function (label, failureType, failureInfo, id) { };
    StubSessionLogger.prototype.handleUserFunnel = function (label) { };
    StubSessionLogger.prototype.handleTrace = function (label, level, message) { };
    StubSessionLogger.prototype.handleSetContext = function (key, value) { };
    return StubSessionLogger;
}(sessionLogger_1.SessionLogger));
exports.StubSessionLogger = StubSessionLogger;
