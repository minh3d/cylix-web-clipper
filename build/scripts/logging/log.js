"use strict";
var event_1 = require("./submodules/event");
var logMethods_1 = require("./submodules/logMethods");
exports.contextPropertyNameRegex = /^[a-zA-Z0-9](([a-zA-Z0-9|_]){0,98}[a-zA-Z0-9])?$/;
exports.enableConsoleLogging = "enable_console_logging";
exports.reportData = "ReportData";
exports.unknownValue = "unknown";
function parseAndLogDataPackage(data, logger) {
    switch (data.methodName) {
        case logMethods_1.LogMethods.LogEvent:
            var eventCategory = data.methodArgs[0];
            var eventData = data.methodArgs[1];
            logger.logEvent.apply(logger, [event_1.Event.createEvent(eventCategory, eventData)]);
            break;
        case logMethods_1.LogMethods.LogFailure:
            logger.logFailure.apply(logger, data.methodArgs);
            break;
        case logMethods_1.LogMethods.PushToStream:
            logger.pushToStream.apply(logger, data.methodArgs);
            break;
        case logMethods_1.LogMethods.LogFunnel:
            logger.logUserFunnel.apply(logger, data.methodArgs);
            break;
        case logMethods_1.LogMethods.LogSessionStart:
            logger.logSessionStart.apply(logger, data.methodArgs);
            break;
        case logMethods_1.LogMethods.LogSessionEnd:
            logger.logSessionEnd.apply(logger, data.methodArgs);
            break;
        case logMethods_1.LogMethods.LogClickEvent:
            logger.logClickEvent.apply(logger, data.methodArgs);
            break;
        case logMethods_1.LogMethods.SetContextProperty:
            logger.setContextProperty.apply(logger, data.methodArgs);
            break;
        case logMethods_1.LogMethods.LogTrace:
        /* falls through */
        default:
            logger.logTrace.apply(logger, data.methodArgs);
            break;
    }
}
exports.parseAndLogDataPackage = parseAndLogDataPackage;
var click_1 = require("./submodules/click");
exports.Click = click_1.Click;
var context_1 = require("./submodules/context");
exports.Context = context_1.Context;
var errorUtils_1 = require("./submodules/errorUtils");
exports.ErrorUtils = errorUtils_1.ErrorUtils;
var event_2 = require("./submodules/event");
exports.Event = event_2.Event;
var failure_1 = require("./submodules/failure");
exports.Failure = failure_1.Failure;
var funnel_1 = require("./submodules/funnel");
exports.Funnel = funnel_1.Funnel;
var logMethods_2 = require("./submodules/logMethods");
exports.LogMethods = logMethods_2.LogMethods;
var noop_1 = require("./submodules/noop");
exports.NoOp = noop_1.NoOp;
var propertyName_1 = require("./submodules/propertyName");
exports.PropertyName = propertyName_1.PropertyName;
var session_1 = require("./submodules/session");
exports.Session = session_1.Session;
var status_1 = require("./submodules/status");
exports.Status = status_1.Status;
var trace_1 = require("./submodules/trace");
exports.Trace = trace_1.Trace;
