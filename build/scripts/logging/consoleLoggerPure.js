"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Log = require("./log");
var consoleLoggerShell_1 = require("./consoleLoggerShell");
var Logger_1 = require("./Logger");
var logHelpers_1 = require("./logHelpers");
var ConsoleLoggerPure = (function (_super) {
    __extends(ConsoleLoggerPure, _super);
    function ConsoleLoggerPure(consoleOutput) {
        var _this = _super.call(this) || this;
        _this.consoleShell = new consoleLoggerShell_1.ConsoleLoggerShell(consoleOutput);
        return _this;
    }
    ConsoleLoggerPure.prototype.logEvent = function (event) {
        var logEvent = logHelpers_1.LogHelpers.createLogEventAsJson(event);
        this.consoleShell.logToConsole(logEvent);
    };
    ConsoleLoggerPure.prototype.logFailure = function (label, failureType, failureInfo, id) {
        var failureEvent = logHelpers_1.LogHelpers.createFailureEventAsJson(label, failureType, failureInfo, id);
        this.consoleShell.logToConsole(failureEvent);
    };
    ConsoleLoggerPure.prototype.logUserFunnel = function (label) {
        var funnelEvent = logHelpers_1.LogHelpers.createFunnelEventAsJson(label);
        this.consoleShell.logToConsole(funnelEvent);
    };
    ConsoleLoggerPure.prototype.logSessionStart = function () {
        var sessionEvent = logHelpers_1.LogHelpers.createSessionStartEventAsJson();
        this.consoleShell.logToConsole(sessionEvent);
    };
    ConsoleLoggerPure.prototype.logSessionEnd = function (endTrigger) {
        var sessionEvent = logHelpers_1.LogHelpers.createSessionEndEventAsJson(endTrigger);
        this.consoleShell.logToConsole(sessionEvent);
    };
    ConsoleLoggerPure.prototype.logTrace = function (label, level, message) {
        var traceEvent = logHelpers_1.LogHelpers.createTraceEventAsJson(label, level, message);
        this.consoleShell.logToConsole(traceEvent);
    };
    ConsoleLoggerPure.prototype.pushToStream = function (label, value) {
        // Deliberately no-op to reduce console noise
    };
    ConsoleLoggerPure.prototype.logClickEvent = function (clickId) {
        var clickEvent = logHelpers_1.LogHelpers.createClickEventAsJson(clickId);
        this.consoleShell.logToConsole(clickEvent);
    };
    ConsoleLoggerPure.prototype.setContextProperty = function (key, value) {
        this.consoleShell.setContextProperty(Log.Context.toString(key), value);
    };
    return ConsoleLoggerPure;
}(Logger_1.Logger));
exports.ConsoleLoggerPure = ConsoleLoggerPure;
