"use strict";
var Log = require("./log");
var LogHelpers;
(function (LogHelpers) {
    function createBaseEventAsJson(subCategory, label) {
        var baseEvent = {};
        baseEvent[Log.PropertyName.Reserved.EventType] = Log.reportData;
        baseEvent[Log.PropertyName.Reserved.Label] = label;
        var category = Log.PropertyName.Reserved.WebClipper + "." + subCategory;
        baseEvent[Log.PropertyName.Reserved.Category] = category;
        baseEvent[Log.PropertyName.Reserved.EventName] = category + "." + label;
        return baseEvent;
    }
    LogHelpers.createBaseEventAsJson = createBaseEventAsJson;
    /**
     * Creates and returns an event with category of Click and a label of `clickId`
     * @param clickId
     */
    function createClickEventAsJson(clickId) {
        if (!clickId) {
            throw new Error("Button clicked without an ID! Logged with ID " + JSON.stringify(clickId));
        }
        var clickEvent = LogHelpers.createBaseEventAsJson(Log.Click.category, clickId);
        return clickEvent;
    }
    LogHelpers.createClickEventAsJson = createClickEventAsJson;
    function createLogEventAsJson(event) {
        if (!event.timerWasStopped()) {
            event.stopTimer();
        }
        var eventCategory = event.getEventCategory();
        // Items that should exist on all event categories
        var logEvent = LogHelpers.createBaseEventAsJson(Log.Event.Category[eventCategory], event.getLabel());
        logEvent[Log.PropertyName.Reserved.Duration] = event.getDuration();
        addToLogEvent(logEvent, event.getCustomProperties());
        switch (eventCategory) {
            case Log.Event.Category.BaseEvent:
                break;
            case Log.Event.Category.PromiseEvent:
                addPromiseEventItems(logEvent, event);
                break;
            case Log.Event.Category.StreamEvent:
                addStreamEventItems(logEvent, event);
                break;
            default:
                throw new Error("createLogEvent does not specify a case for event category: " + Log.Event.Category[eventCategory]);
        }
        return logEvent;
    }
    LogHelpers.createLogEventAsJson = createLogEventAsJson;
    function addPromiseEventItems(logEvent, promiseEvent) {
        var status = promiseEvent.getStatus();
        logEvent[Log.PropertyName.Reserved.Status] = status;
        if (status === Log.Status[Log.Status.Failed]) {
            logEvent[Log.PropertyName.Reserved.FailureInfo] = promiseEvent.getFailureInfo();
            logEvent[Log.PropertyName.Reserved.FailureType] = promiseEvent.getFailureType();
        }
    }
    function addStreamEventItems(logEvent, streamEvent) {
        logEvent[Log.PropertyName.Reserved.Stream] = JSON.stringify(streamEvent.getEventData().Stream);
    }
    function createSetContextEventAsJson(key, value) {
        var baseEvent = new Log.Event.BaseEvent(Log.Event.Label.SetContextProperty);
        var event = LogHelpers.createBaseEventAsJson(Log.Event.Category[baseEvent.getEventCategory()], baseEvent.getLabel());
        var keyAsString = Log.Context.toString(key);
        event[Log.PropertyName.Custom[Log.PropertyName.Custom.Key]] = keyAsString;
        event[Log.PropertyName.Custom[Log.PropertyName.Custom.Value]] = value;
        return event;
    }
    LogHelpers.createSetContextEventAsJson = createSetContextEventAsJson;
    function createFailureEventAsJson(label, failureType, failureInfo, id) {
        var failureEvent = LogHelpers.createBaseEventAsJson(Log.Failure.category, Log.Failure.Label[label]);
        failureEvent[Log.PropertyName.Reserved.FailureType] = Log.Failure.Type[failureType];
        if (failureInfo) {
            failureEvent[Log.PropertyName.Reserved.FailureInfo] = Log.ErrorUtils.toString(failureInfo);
        }
        if (id) {
            failureEvent[Log.PropertyName.Reserved.Id] = id;
        }
        failureEvent[Log.PropertyName.Reserved.StackTrace] = Log.Failure.getStackTrace();
        return failureEvent;
    }
    LogHelpers.createFailureEventAsJson = createFailureEventAsJson;
    function createFunnelEventAsJson(label) {
        var funnelEvent = LogHelpers.createBaseEventAsJson(Log.Funnel.category, Log.Funnel.Label[label]);
        return funnelEvent;
    }
    LogHelpers.createFunnelEventAsJson = createFunnelEventAsJson;
    function createSessionStartEventAsJson() {
        var sessionEvent = LogHelpers.createBaseEventAsJson(Log.Session.category, Log.Session.State[Log.Session.State.Started]);
        return sessionEvent;
    }
    LogHelpers.createSessionStartEventAsJson = createSessionStartEventAsJson;
    function createSessionEndEventAsJson(endTrigger) {
        var sessionEvent = LogHelpers.createBaseEventAsJson(Log.Session.category, Log.Session.State[Log.Session.State.Ended]);
        sessionEvent[Log.PropertyName.Reserved.Trigger] = Log.Session.EndTrigger[endTrigger];
        return sessionEvent;
    }
    LogHelpers.createSessionEndEventAsJson = createSessionEndEventAsJson;
    function createTraceEventAsJson(label, level, message) {
        var traceEvent = LogHelpers.createBaseEventAsJson(Log.Trace.category, Log.Trace.Label[label]);
        if (message) {
            traceEvent[Log.PropertyName.Reserved.Message] = message;
        }
        traceEvent[Log.PropertyName.Reserved.Level] = Log.Trace.Level[level];
        switch (level) {
            case Log.Trace.Level.Warning:
                // Add stack trace to warnings
                traceEvent[Log.PropertyName.Reserved.StackTrace] = Log.Failure.getStackTrace();
                break;
            default:
                break;
        }
        return traceEvent;
    }
    LogHelpers.createTraceEventAsJson = createTraceEventAsJson;
    function addToLogEvent(logEvent, properties) {
        if (logEvent[Log.PropertyName.Reserved.Status] === Log.Status[Log.Status.Failed]) {
            logEvent[Log.PropertyName.Reserved.StackTrace] = Log.Failure.getStackTrace();
        }
        if (properties) {
            for (var name_1 in properties) {
                if (properties.hasOwnProperty(name_1)) {
                    var propValue = void 0;
                    if (typeof (properties[name_1]) === "object") {
                        propValue = JSON.stringify(properties[name_1]);
                    }
                    else {
                        propValue = properties[name_1];
                    }
                    logEvent[name_1] = propValue;
                }
            }
        }
    }
    LogHelpers.addToLogEvent = addToLogEvent;
    function isConsoleOutputEnabled() {
        try {
            if (localStorage.getItem(Log.enableConsoleLogging)) {
                return true;
            }
        }
        catch (e) { }
        ;
        return false;
    }
    LogHelpers.isConsoleOutputEnabled = isConsoleOutputEnabled;
})(LogHelpers = exports.LogHelpers || (exports.LogHelpers = {}));
