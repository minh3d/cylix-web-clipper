"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../constants");
var Log = require("./log");
var logger_1 = require("./logger");
var CommunicatorLoggerPure = (function (_super) {
    __extends(CommunicatorLoggerPure, _super);
    function CommunicatorLoggerPure(communicator) {
        var _this = _super.call(this) || this;
        _this.communicator = communicator;
        return _this;
    }
    CommunicatorLoggerPure.prototype.logEvent = function (event) {
        if (!event.timerWasStopped()) {
            event.stopTimer();
        }
        // We need to send the event category as well so that the other side knows which one it is
        this.sendDataPackage(Log.LogMethods.LogEvent, [event.getEventCategory(), event.getEventData()]);
    };
    CommunicatorLoggerPure.prototype.logFailure = function (label, failureType, failureInfo, id) {
        this.sendDataPackage(Log.LogMethods.LogFailure, arguments);
    };
    CommunicatorLoggerPure.prototype.logUserFunnel = function (label) {
        this.sendDataPackage(Log.LogMethods.LogFunnel, arguments);
    };
    CommunicatorLoggerPure.prototype.logSessionStart = function () {
        this.sendDataPackage(Log.LogMethods.LogSessionStart, arguments);
    };
    CommunicatorLoggerPure.prototype.logSessionEnd = function (endTrigger) {
        this.sendDataPackage(Log.LogMethods.LogSessionEnd, arguments);
    };
    CommunicatorLoggerPure.prototype.logTrace = function (label, level, message) {
        if (message) {
            this.sendDataPackage(Log.LogMethods.LogTrace, [label, level, message]);
        }
        else {
            this.sendDataPackage(Log.LogMethods.LogTrace, [label, level]);
        }
    };
    CommunicatorLoggerPure.prototype.pushToStream = function (label, value) {
        this.sendDataPackage(Log.LogMethods.PushToStream, arguments);
    };
    CommunicatorLoggerPure.prototype.logClickEvent = function (clickId) {
        this.sendDataPackage(Log.LogMethods.LogClickEvent, arguments);
    };
    CommunicatorLoggerPure.prototype.setContextProperty = function (key, value) {
        this.sendDataPackage(Log.LogMethods.SetContextProperty, arguments);
    };
    CommunicatorLoggerPure.prototype.sendDataPackage = function (methodName, args) {
        var data = {
            methodName: methodName,
            methodArgs: Object.keys(args).map(function (key) { return args[key]; })
        };
        this.communicator.callRemoteFunction(constants_1.Constants.FunctionKeys.telemetry, { param: data });
    };
    return CommunicatorLoggerPure;
}(logger_1.Logger));
exports.CommunicatorLoggerPure = CommunicatorLoggerPure;
