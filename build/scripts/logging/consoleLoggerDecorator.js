"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var consoleLoggerPure_1 = require("./consoleLoggerPure");
var loggerDecorator_1 = require("./loggerDecorator");
var ConsoleLoggerDecorator = (function (_super) {
    __extends(ConsoleLoggerDecorator, _super);
    function ConsoleLoggerDecorator(consoleOutput, options) {
        var _this = _super.call(this, options) || this;
        _this.pureConsoleLogger = new consoleLoggerPure_1.ConsoleLoggerPure(consoleOutput);
        return _this;
    }
    ConsoleLoggerDecorator.prototype.outputEvent = function (event) {
        this.pureConsoleLogger.logEvent(event);
    };
    ConsoleLoggerDecorator.prototype.outputFailure = function (label, failureType, failureInfo, id) {
        this.pureConsoleLogger.logFailure(label, failureType, failureInfo, id);
    };
    ConsoleLoggerDecorator.prototype.outputUserFunnel = function (label) {
        this.pureConsoleLogger.logUserFunnel(label);
    };
    ConsoleLoggerDecorator.prototype.outputSessionStart = function () {
        this.pureConsoleLogger.logSessionStart();
    };
    ConsoleLoggerDecorator.prototype.outputSessionEnd = function (endTrigger) {
        this.pureConsoleLogger.logSessionEnd(endTrigger);
    };
    ConsoleLoggerDecorator.prototype.outputTrace = function (label, level, message) {
        this.pureConsoleLogger.logTrace(label, level, message);
    };
    ConsoleLoggerDecorator.prototype.outputClickEvent = function (clickId) {
        this.pureConsoleLogger.logClickEvent(clickId);
    };
    ConsoleLoggerDecorator.prototype.outputSetContext = function (key, value) {
        this.pureConsoleLogger.setContextProperty(key, value);
    };
    return ConsoleLoggerDecorator;
}(loggerDecorator_1.LoggerDecorator));
exports.ConsoleLoggerDecorator = ConsoleLoggerDecorator;
