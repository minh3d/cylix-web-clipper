"use strict";
var communicatorLoggerDecorator_1 = require("../logging/communicatorLoggerDecorator");
var consoleLoggerDecorator_1 = require("../logging/consoleLoggerDecorator");
var logHelpers_1 = require("../logging/logHelpers");
var context_1 = require("../logging/context");
var stubSessionLogger_1 = require("../logging/stubSessionLogger");
var webConsole_1 = require("../logging/webConsole");
/**
 * Creates the logger responsible for centralized logging on the backend. If a
 * communicator parameter is specified, it is assumed that console logging is
 * enabled, and it will be set up in the logger object as well.
 */
function createExtLogger(sessionId, uiCommunicator) {
    if (uiCommunicator) {
        return createDebugLogger(uiCommunicator, sessionId);
    }
    return new stubSessionLogger_1.StubSessionLogger();
}
exports.createExtLogger = createExtLogger;
function createDebugLogger(uiCommunicator, sessionId) {
    var commLogger = uiCommunicator ? new communicatorLoggerDecorator_1.CommunicatorLoggerDecorator(uiCommunicator) : undefined;
    var consoleOutput = logHelpers_1.LogHelpers.isConsoleOutputEnabled() ? new webConsole_1.WebConsole() : undefined;
    return new consoleLoggerDecorator_1.ConsoleLoggerDecorator(consoleOutput, {
        contextStrategy: new context_1.ProductionRequirements(),
        component: commLogger,
        sessionId: sessionId
    });
}
/**
 * Sends an event to console with relevant data as query parameters
 */
function sendMiscLogRequest(data, keysToCamelCase) {
    console.warn(JSON.stringify({ label: data.label, category: data.category, properties: data.properties }));
}
exports.sendMiscLogRequest = sendMiscLogRequest;
