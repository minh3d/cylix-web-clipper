"use strict";
var WebConsole = (function () {
    function WebConsole() {
    }
    WebConsole.prototype.warn = function (message, object) {
        console.warn(message, object);
    };
    WebConsole.prototype.error = function (message, object) {
        console.error(message, object);
    };
    WebConsole.prototype.info = function (message, object) {
        console.info(message, object);
    };
    WebConsole.prototype.log = function (message, object) {
        console.log(message, object);
    };
    return WebConsole;
}());
exports.WebConsole = WebConsole;
