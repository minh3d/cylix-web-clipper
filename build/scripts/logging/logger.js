"use strict";
var Log = require("./log");
var Logger = (function () {
    function Logger() {
    }
    Logger.prototype.logJsonParseUnexpected = function (value) {
        this.logFailure(Log.Failure.Label.JsonParse, Log.Failure.Type.Unexpected, undefined /* failureInfo */, value);
    };
    return Logger;
}());
exports.Logger = Logger;
