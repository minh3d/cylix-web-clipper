"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var stringUtils_1 = require("../stringUtils");
var Log = require("./log");
var sessionLogger_1 = require("./sessionLogger");
var LoggerDecorator = (function (_super) {
    __extends(LoggerDecorator, _super);
    function LoggerDecorator(options) {
        var _this = this;
        var strategy = options && options.contextStrategy ? options.contextStrategy : undefined;
        var sessionId = options && options.sessionId ? options.sessionId : undefined;
        _this = _super.call(this, {
            contextStrategy: strategy,
            sessionId: sessionId
        }) || this;
        _this.component = options ? options.component : undefined;
        return _this;
    }
    // @Overrides SessionLogger
    LoggerDecorator.prototype.generateSessionId = function () {
        return undefined;
    };
    LoggerDecorator.prototype.handleSetUserSessionId = function (parentSessionId) {
        var sessionId;
        var existingId = this.sessionId.get();
        var generatedId = this.generateSessionId();
        if (existingId) {
            // Have we already initialized a sessionId?
            sessionId = existingId;
        }
        else if (parentSessionId) {
            // Is my parent telling me to set a sessionId?
            sessionId = parentSessionId;
        }
        else if (generatedId) {
            // Do I know how to generate a sessionId?
            sessionId = generatedId;
        }
        if (this.component && sessionId) {
            this.component.handleSetUserSessionId(sessionId);
        }
        else if (this.component && !sessionId) {
            sessionId = this.component.handleSetUserSessionId();
        }
        else if (!this.component && !sessionId) {
            // In the case where the session has Ended, we default to there being no session id as it wouldn't
            // make sense having one
            sessionId = this.currentSessionState === Log.Session.State.Started ?
                "cccccccc-" + stringUtils_1.StringUtils.generateGuid().substring(9) :
                undefined;
        }
        this.sessionId.set(sessionId);
        this.setContextPropertyPure(Log.Context.Custom.SessionId, sessionId);
        this.outputSetContext(Log.Context.Custom.SessionId, sessionId);
        return sessionId;
    };
    LoggerDecorator.prototype.handleClickEvent = function (clickId) {
        this.outputClickEvent(clickId);
        if (this.component) {
            this.component.executeClickEvent(clickId);
        }
    };
    LoggerDecorator.prototype.handleEvent = function (event) {
        this.handleEventPure(event);
        if (this.component) {
            this.component.logEvent(event);
        }
    };
    LoggerDecorator.prototype.handleEventPure = function (event) {
        this.outputEvent(event);
    };
    LoggerDecorator.prototype.handleSessionStart = function () {
        this.outputSessionStart();
        if (this.component) {
            this.component.executeSessionStart();
        }
    };
    LoggerDecorator.prototype.handleSessionEnd = function (endTrigger) {
        this.outputSessionEnd(endTrigger);
        if (this.component) {
            this.component.executeSessionEnd(endTrigger);
        }
    };
    LoggerDecorator.prototype.handleFailure = function (label, failureType, failureInfo, id) {
        this.outputFailure(label, failureType, failureInfo, id);
        if (this.component) {
            this.component.logFailure(label, failureType, failureInfo, id);
        }
    };
    LoggerDecorator.prototype.handleUserFunnel = function (label) {
        this.outputUserFunnel(label);
        if (this.component) {
            this.component.logUserFunnel(label);
        }
    };
    LoggerDecorator.prototype.handleTrace = function (label, level, message) {
        this.outputTrace(label, level, message);
        if (this.component) {
            this.component.logTrace(label, level, message);
        }
    };
    LoggerDecorator.prototype.handleSetContext = function (key, value) {
        this.outputSetContext(key, value);
        if (this.component) {
            this.component.setContextProperty(key, value);
        }
    };
    return LoggerDecorator;
}(sessionLogger_1.SessionLogger));
exports.LoggerDecorator = LoggerDecorator;
