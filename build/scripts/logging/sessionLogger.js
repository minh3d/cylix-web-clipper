"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var smartValue_1 = require("../communicator/smartValue");
var constants_1 = require("../constants");
var objectUtils_1 = require("../objectUtils");
var Log = require("./log");
var logger_1 = require("./logger");
var SessionLogger = (function (_super) {
    __extends(SessionLogger, _super);
    function SessionLogger(options) {
        var _this = _super.call(this) || this;
        // State
        _this.contextProperties = {};
        _this.currentSessionState = Log.Session.State.Ended;
        _this.logQueue = [];
        _this.streams = {};
        _this.userHasInteracted = false;
        _this.sessionId = undefined;
        _this.sessionId = options && options.sessionId ? options.sessionId : new smartValue_1.SmartValue();
        _this.contextStrategy = options ? options.contextStrategy : undefined;
        return _this;
    }
    SessionLogger.prototype.hasUserInteracted = function () {
        return this.userHasInteracted;
    };
    SessionLogger.prototype.getUserSessionId = function (sessionId) {
        return this.sessionId.get();
    };
    SessionLogger.prototype.sendFunnelInteractionEvent = function (clickId) {
        // These events do not count as "interactions" as they are tied to some other user intention
        var nonInteractionEvents = [constants_1.Constants.Ids.signInButtonMsa, constants_1.Constants.Ids.signInButtonOrgId,
            constants_1.Constants.Ids.signOutButton, constants_1.Constants.Ids.closeButton, constants_1.Constants.Ids.clipButton, constants_1.Constants.Ids.launchOneNoteButton,
            constants_1.Constants.Ids.checkOutWhatsNewButton, constants_1.Constants.Ids.proceedToWebClipperButton];
        if (this.hasUserInteracted() || nonInteractionEvents.indexOf(clickId) !== -1) {
            return;
        }
        this.userHasInteracted = true;
        this.logUserFunnel(Log.Funnel.Label.Interact);
    };
    SessionLogger.prototype.logEvent = function (event) {
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(event)) {
            this.logFailure(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected);
            return;
        }
        if (!this.areContextRequirementsMet()) {
            this.pushDataPackage(Log.LogMethods.LogEvent, arguments);
            return;
        }
        this.handleEvent(event);
    };
    SessionLogger.prototype.logFailure = function (label, failureType, failureInfo, id) {
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(label) || objectUtils_1.ObjectUtils.isNullOrUndefined(failureType)) {
            this.logFailure(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected);
            return;
        }
        if (!this.areContextRequirementsMet()) {
            this.pushDataPackage(Log.LogMethods.LogFailure, arguments);
            return;
        }
        this.handleFailure(label, failureType, failureInfo, id);
    };
    SessionLogger.prototype.logUserFunnel = function (label) {
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(label)) {
            this.logFailure(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected);
            return;
        }
        if (!this.areContextRequirementsMet()) {
            this.pushDataPackage(Log.LogMethods.LogFunnel, arguments);
            return;
        }
        this.handleUserFunnel(label);
    };
    SessionLogger.prototype.logSessionStart = function () {
        if (!this.areContextRequirementsMet()) {
            this.pushDataPackage(Log.LogMethods.LogSessionStart, arguments);
            return;
        }
        this.executeSessionStart();
        this.handleSetUserSessionId();
    };
    SessionLogger.prototype.executeSessionStart = function () {
        if (this.currentSessionState === Log.Session.State.Started) {
            var errorMessage = "Session already STARTED";
            this.logFailure(Log.Failure.Label.SessionAlreadySet, Log.Failure.Type.Unexpected, { error: errorMessage });
            return;
        }
        this.streams = {};
        this.currentSessionState = Log.Session.State.Started;
        this.handleSessionStart();
    };
    SessionLogger.prototype.logSessionEnd = function (endTrigger) {
        if (!this.areContextRequirementsMet()) {
            this.pushDataPackage(Log.LogMethods.LogSessionEnd, arguments);
            return;
        }
        this.executeSessionEnd(endTrigger);
        this.handleSetUserSessionId();
    };
    SessionLogger.prototype.executeSessionEnd = function (endTrigger) {
        if (this.currentSessionState === Log.Session.State.Ended) {
            var errorMessage = "Session already ENDED";
            if (!objectUtils_1.ObjectUtils.isNullOrUndefined(endTrigger)) {
                errorMessage += ". EndTrigger: " + Log.Session.EndTrigger[endTrigger];
            }
            this.logFailure(Log.Failure.Label.SessionAlreadySet, Log.Failure.Type.Unexpected, { error: errorMessage });
            return;
        }
        this.logAllStreams();
        this.sessionId.set(undefined);
        this.userHasInteracted = false;
        this.currentSessionState = Log.Session.State.Ended;
        this.handleSessionEnd(endTrigger);
    };
    SessionLogger.prototype.logTrace = function (label, level, message) {
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(label) || objectUtils_1.ObjectUtils.isNullOrUndefined(level)) {
            this.logFailure(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected);
            return;
        }
        if (!this.areContextRequirementsMet()) {
            this.pushDataPackage(Log.LogMethods.LogTrace, arguments);
            return;
        }
        this.handleTrace(label, level, message);
    };
    SessionLogger.prototype.pushToStream = function (label, value) {
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(label)) {
            this.logFailure(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected);
            return;
        }
        var labelAsString = Log.Event.Label[label];
        if (value) {
            if (!this.streams[labelAsString]) {
                this.streams[labelAsString] = [];
            }
            this.streams[labelAsString].push(value);
        }
    };
    SessionLogger.prototype.logClickEvent = function (clickId) {
        if (!this.areContextRequirementsMet()) {
            this.pushDataPackage(Log.LogMethods.LogClickEvent, arguments);
            return;
        }
        this.sendFunnelInteractionEvent(clickId);
        this.executeClickEvent(clickId);
    };
    SessionLogger.prototype.executeClickEvent = function (clickId) {
        if (!clickId) {
            this.logFailure(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected, { error: "Button clicked without an ID! Logged with ID " + JSON.stringify(clickId) });
            return;
        }
        this.pushToStream(Log.Event.Label.Click, clickId);
        this.handleClickEvent(clickId);
    };
    SessionLogger.prototype.setContextProperty = function (key, value) {
        this.setContextPropertyPure(key, value);
        this.handleSetContext(key, value);
        if (this.areContextRequirementsMet()) {
            this.flushEventQueue();
        }
    };
    SessionLogger.prototype.setContextPropertyPure = function (key, value) {
        var keyAsString = Log.Context.toString(key);
        this.contextProperties[keyAsString] = value;
    };
    SessionLogger.prototype.areContextRequirementsMet = function () {
        if (this.contextStrategy) {
            return this.contextStrategy.requirementsAreMet(this.contextProperties);
        }
        else {
            return true;
        }
    };
    SessionLogger.prototype.flushEventQueue = function () {
        var _this = this;
        if (this.isQueueFlushing || this.isQueueFlushed) {
            return;
        }
        this.isQueueFlushing = true;
        this.logQueue.forEach(function (data) {
            switch (data.methodName) {
                case Log.LogMethods.LogEvent:
                    _this.logEvent.apply(_this, data.methodArgs);
                    break;
                case Log.LogMethods.LogFailure:
                    _this.logFailure.apply(_this, data.methodArgs);
                    break;
                case Log.LogMethods.PushToStream:
                    _this.pushToStream.apply(_this, data.methodArgs);
                    break;
                case Log.LogMethods.LogFunnel:
                    _this.logUserFunnel.apply(_this, data.methodArgs);
                    break;
                case Log.LogMethods.LogSessionStart:
                    _this.logSessionStart.apply(_this, data.methodArgs);
                    break;
                case Log.LogMethods.LogSessionEnd:
                    _this.logSessionEnd.apply(_this, data.methodArgs);
                    break;
                case Log.LogMethods.LogClickEvent:
                    _this.logClickEvent.apply(_this, data.methodArgs);
                    break;
                case Log.LogMethods.SetContextProperty:
                    _this.setContextProperty.apply(_this, data.methodArgs);
                    break;
                case Log.LogMethods.LogTrace:
                /* falls through */
                default:
                    _this.logTrace.apply(_this, data.methodArgs);
                    break;
            }
        });
        this.isQueueFlushing = false;
        this.isQueueFlushed = true;
    };
    SessionLogger.prototype.logAllStreams = function () {
        for (var property in this.streams) {
            if (this.streams.hasOwnProperty(property)) {
                var streamEvent = new Log.Event.StreamEvent(Log.Event.Label[property]);
                var stream = this.streams[property];
                for (var i = 0; i < stream.length; i++) {
                    streamEvent.append(stream[i]);
                }
                this.handleEventPure(streamEvent);
            }
        }
    };
    SessionLogger.prototype.pushDataPackage = function (methodName, methodArgs) {
        this.logQueue.push({ methodName: methodName, methodArgs: methodArgs });
    };
    return SessionLogger;
}(logger_1.Logger));
exports.SessionLogger = SessionLogger;
