"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var communicatorLoggerPure_1 = require("./communicatorLoggerPure");
var loggerDecorator_1 = require("./loggerDecorator");
var CommunicatorLoggerDecorator = (function (_super) {
    __extends(CommunicatorLoggerDecorator, _super);
    function CommunicatorLoggerDecorator(communicator, logger) {
        var _this = _super.call(this, {
            component: logger
        }) || this;
        _this.pureLogger = new communicatorLoggerPure_1.CommunicatorLoggerPure(communicator);
        return _this;
    }
    CommunicatorLoggerDecorator.prototype.outputEvent = function (event) {
        this.pureLogger.logEvent(event);
    };
    CommunicatorLoggerDecorator.prototype.outputFailure = function (label, failureType, failureInfo, id) {
        this.pureLogger.logFailure(label, failureType, failureInfo, id);
    };
    CommunicatorLoggerDecorator.prototype.outputUserFunnel = function (label) {
        this.pureLogger.logUserFunnel(label);
    };
    CommunicatorLoggerDecorator.prototype.outputSessionStart = function () {
        this.pureLogger.logSessionStart();
    };
    CommunicatorLoggerDecorator.prototype.outputSessionEnd = function (endTrigger) {
        this.pureLogger.logSessionEnd(endTrigger);
    };
    CommunicatorLoggerDecorator.prototype.outputTrace = function (label, level, message) {
        this.pureLogger.logTrace(label, level, message);
    };
    CommunicatorLoggerDecorator.prototype.outputClickEvent = function (clickId) {
        this.pureLogger.logClickEvent(clickId);
    };
    CommunicatorLoggerDecorator.prototype.outputSetContext = function (key, value) {
        this.pureLogger.setContextProperty(key, value);
    };
    return CommunicatorLoggerDecorator;
}(loggerDecorator_1.LoggerDecorator));
exports.CommunicatorLoggerDecorator = CommunicatorLoggerDecorator;
