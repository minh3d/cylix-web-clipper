"use strict";
var Log = require("./log");
var ConsoleLoggerShell = (function () {
    function ConsoleLoggerShell(consoleOutput) {
        this.context = {};
        this.consoleOutput = consoleOutput;
    }
    ConsoleLoggerShell.prototype.logToConsole = function (event) {
        if (!event) {
            throw new Error("'event' argument to logToConsole was: " + event);
        }
        var level = event[Log.PropertyName.Reserved.Level];
        // if Status == Failed or SubCategory == Failure, log as an error
        if (event[Log.PropertyName.Reserved.Status] === Log.Status[Log.Status.Failed]
            || event[Log.PropertyName.Reserved.Category] === Log.PropertyName.Reserved.WebClipper + "." + Log.Failure.category) {
            level = Log.Trace.Level[Log.Trace.Level.Error];
        }
        var logger = this.consoleOutput;
        // Log all events as [EventName] [level?] [message?], eventToLog for ease of reading
        var messageToLog = "";
        messageToLog += "[" + event[Log.PropertyName.Reserved.EventName] + "]";
        messageToLog += level ? " [" + level + "]" : "";
        messageToLog += Log.PropertyName.Reserved.Message in event ? " " + event[Log.PropertyName.Reserved.Message] : "";
        var eventToLog = this.combineContextAndEvent(event);
        switch (Log.Trace.Level[level]) {
            case Log.Trace.Level.Warning:
                logger.warn(messageToLog, eventToLog);
                break;
            case Log.Trace.Level.Error:
                logger.error(messageToLog, eventToLog);
                break;
            case Log.Trace.Level.Verbose:
            case Log.Trace.Level.Information:
                logger.info(messageToLog, eventToLog);
                break;
            default:
                logger.log(messageToLog, eventToLog);
        }
    };
    ConsoleLoggerShell.prototype.setContextProperty = function (key, value) {
        this.context[key] = value;
    };
    ConsoleLoggerShell.prototype.combineContextAndEvent = function (event) {
        var contextAndEvent = {};
        for (var key in this.context) {
            if (this.context.hasOwnProperty(key)) {
                contextAndEvent[key] = this.context[key];
            }
        }
        for (var key in event) {
            if (event.hasOwnProperty(key)) {
                contextAndEvent[key] = event[key];
            }
        }
        return contextAndEvent;
    };
    return ConsoleLoggerShell;
}());
exports.ConsoleLoggerShell = ConsoleLoggerShell;
