"use strict";
var objectUtils_1 = require("../objectUtils");
var Log = require("./log");
var NoRequirements = (function () {
    function NoRequirements() {
    }
    NoRequirements.prototype.requirementsAreMet = function (requirements) {
        return true;
    };
    return NoRequirements;
}());
exports.NoRequirements = NoRequirements;
var ProductionRequirements = (function () {
    function ProductionRequirements(requiredProperties) {
        this.prodProperties = [
            Log.Context.toString(Log.Context.Custom.AppInfoId),
            Log.Context.toString(Log.Context.Custom.AppInfoVersion),
            Log.Context.toString(Log.Context.Custom.BrowserLanguage),
            Log.Context.toString(Log.Context.Custom.ExtensionLifecycleId),
            Log.Context.toString(Log.Context.Custom.ClipperType),
            Log.Context.toString(Log.Context.Custom.DeviceInfoId),
            Log.Context.toString(Log.Context.Custom.FlightInfo),
            Log.Context.toString(Log.Context.Custom.InPrivateBrowsing)
        ];
        this.requiredProperties = requiredProperties ? requiredProperties : this.prodProperties;
    }
    ProductionRequirements.prototype.requirementsAreMet = function (contextProps) {
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(contextProps)) {
            return false;
        }
        for (var i = 0; i < this.requiredProperties.length; ++i) {
            var prop = this.requiredProperties[i];
            if (!contextProps.hasOwnProperty(prop)) {
                return false;
            }
        }
        return true;
    };
    return ProductionRequirements;
}());
exports.ProductionRequirements = ProductionRequirements;
