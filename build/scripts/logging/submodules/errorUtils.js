"use strict";
var clientType_1 = require("../../clientType");
var constants_1 = require("../../constants");
var objectUtils_1 = require("../../objectUtils");
var localization_1 = require("../../localization/localization");
var log_1 = require("../log");
var ErrorUtils;
(function (ErrorUtils) {
    var ErrorPropertyName;
    (function (ErrorPropertyName) {
        ErrorPropertyName[ErrorPropertyName["Error"] = 0] = "Error";
        ErrorPropertyName[ErrorPropertyName["StatusCode"] = 1] = "StatusCode";
        ErrorPropertyName[ErrorPropertyName["Response"] = 2] = "Response";
        ErrorPropertyName[ErrorPropertyName["ResponseHeaders"] = 3] = "ResponseHeaders";
        ErrorPropertyName[ErrorPropertyName["Timeout"] = 4] = "Timeout";
    })(ErrorPropertyName || (ErrorPropertyName = {}));
    function toString(originalError) {
        if (!originalError) {
            return undefined;
        }
        var errorToObject = {};
        errorToObject[ErrorPropertyName[ErrorPropertyName.Error].toLowerCase()] = originalError.error;
        var tryCastError = originalError;
        if (tryCastError && tryCastError.statusCode !== undefined) {
            errorToObject[ErrorPropertyName[ErrorPropertyName.StatusCode].toLowerCase()] = tryCastError.statusCode;
            errorToObject[ErrorPropertyName[ErrorPropertyName.Response].toLowerCase()] = tryCastError.response;
            errorToObject[ErrorPropertyName[ErrorPropertyName.ResponseHeaders].toLowerCase()] = tryCastError.responseHeaders;
            if (tryCastError.timeout !== undefined) {
                errorToObject[ErrorPropertyName[ErrorPropertyName.Timeout].toLowerCase()] = tryCastError.timeout;
            }
        }
        return JSON.stringify(errorToObject);
    }
    ErrorUtils.toString = toString;
    function clone(originalError) {
        if (!originalError) {
            return undefined;
        }
        var tryCastError = originalError;
        if (tryCastError && tryCastError.statusCode !== undefined) {
            if (tryCastError.timeout !== undefined) {
                return { error: tryCastError.error, statusCode: tryCastError.statusCode, response: tryCastError.response, responseHeaders: tryCastError.responseHeaders, timeout: tryCastError.timeout };
            }
            else {
                return { error: tryCastError.error, statusCode: tryCastError.statusCode, response: tryCastError.response, responseHeaders: tryCastError.responseHeaders };
            }
        }
        else {
            return { error: originalError.error };
        }
    }
    ErrorUtils.clone = clone;
    /**
     * Sends a request to the misc logging endpoint with relevant failure data as query parameters
     */
    function sendFailureLogRequest(data) {
        var propsObject = {};
        propsObject[constants_1.Constants.Urls.QueryParams.failureType] = log_1.Failure.Type[data.properties.failureType];
        propsObject[constants_1.Constants.Urls.QueryParams.failureInfo] = ErrorUtils.toString(data.properties.failureInfo);
        propsObject[constants_1.Constants.Urls.QueryParams.stackTrace] = data.properties.stackTrace;
        if (!objectUtils_1.ObjectUtils.isNullOrUndefined(data.properties.failureId)) {
            propsObject[constants_1.Constants.Urls.QueryParams.failureId] = data.properties.failureId;
        }
        var clientInfo = data.clientInfo;
        addDelayedSetValuesOnNoOp(propsObject, clientInfo);
        LogManager.sendMiscLogRequest({
            label: log_1.Failure.Label[data.label],
            category: log_1.Failure.category,
            properties: propsObject
        }, true);
    }
    ErrorUtils.sendFailureLogRequest = sendFailureLogRequest;
    function handleCommunicatorError(channel, e, clientInfo, message) {
        var errorValue;
        if (message) {
            errorValue = JSON.stringify({ message: message, error: e.toString() });
        }
        else {
            errorValue = e.toString();
        }
        ErrorUtils.sendFailureLogRequest({
            label: log_1.Failure.Label.UnhandledExceptionThrown,
            properties: {
                failureType: log_1.Failure.Type.Unexpected,
                failureInfo: { error: errorValue },
                failureId: "Channel " + channel,
                stackTrace: log_1.Failure.getStackTrace(e)
            },
            clientInfo: clientInfo
        });
        throw e;
    }
    ErrorUtils.handleCommunicatorError = handleCommunicatorError;
    /*
    * Sends a request to the misc logging endpoint with noop-relevant data as query parameters
    *	and shows an alert if the relevant property is set.
    */
    function sendNoOpTrackerRequest(props, shouldShowAlert) {
        if (shouldShowAlert === void 0) { shouldShowAlert = false; }
        var propsObject = {};
        propsObject[constants_1.Constants.Urls.QueryParams.channel] = props.channel;
        propsObject[constants_1.Constants.Urls.QueryParams.url] = encodeURIComponent(props.url);
        propsObject[constants_1.Constants.Urls.QueryParams.timeoutInMs] = constants_1.Constants.Settings.noOpTrackerTimeoutDuration.toString();
        var clientInfo = props.clientInfo;
        addDelayedSetValuesOnNoOp(propsObject, clientInfo);
        LogManager.sendMiscLogRequest({
            label: log_1.NoOp.Label[props.label],
            category: log_1.NoOp.category,
            properties: propsObject
        }, true);
        if (shouldShowAlert && window) {
            window.alert(localization_1.Localization.getLocalizedString("WebClipper.Error.NoOpError"));
        }
    }
    ErrorUtils.sendNoOpTrackerRequest = sendNoOpTrackerRequest;
    /*
    * Returns a TimeOut that should be cleared, otherwise sends a request to onenote.com/count
    *	with relevant no-op tracking data
    */
    function setNoOpTrackerRequestTimeout(props, shouldShowAlert) {
        if (shouldShowAlert === void 0) { shouldShowAlert = false; }
        return setTimeout(function () {
            sendNoOpTrackerRequest(props, shouldShowAlert);
        }, constants_1.Constants.Settings.noOpTrackerTimeoutDuration);
    }
    ErrorUtils.setNoOpTrackerRequestTimeout = setNoOpTrackerRequestTimeout;
    /**
     * During a noop scenario, most properties are retrieved at the construction of the NoOpProperties
     * object for setNoOpTrackerRequestTimeout. But some properties could benefit from waiting
     * until after the noop timeout before we attempt to retrieve them (e.g., smart values).
     * This is a helper function for adding these values to the props object on delay.
     */
    function addDelayedSetValuesOnNoOp(props, clientInfo) {
        if (clientInfo) {
            props[constants_1.Constants.Urls.QueryParams.clientType] = objectUtils_1.ObjectUtils.isNullOrUndefined(clientInfo.get()) ? log_1.unknownValue : clientType_1.ClientType[clientInfo.get().clipperType];
            props[constants_1.Constants.Urls.QueryParams.clipperVersion] = objectUtils_1.ObjectUtils.isNullOrUndefined(clientInfo.get()) ? log_1.unknownValue : clientInfo.get().clipperVersion;
            props[constants_1.Constants.Urls.QueryParams.clipperId] = objectUtils_1.ObjectUtils.isNullOrUndefined(clientInfo.get()) ? log_1.unknownValue : clientInfo.get().clipperId;
        }
    }
})(ErrorUtils = exports.ErrorUtils || (exports.ErrorUtils = {}));
