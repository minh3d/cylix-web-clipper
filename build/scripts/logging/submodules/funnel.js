"use strict";
var Funnel;
(function (Funnel) {
    Funnel.category = "Funnel";
    var Label;
    (function (Label) {
        Label[Label["Invoke"] = 0] = "Invoke";
        Label[Label["AuthAlreadySignedIn"] = 1] = "AuthAlreadySignedIn";
        Label[Label["AuthAttempted"] = 2] = "AuthAttempted";
        Label[Label["AuthSignInCompleted"] = 3] = "AuthSignInCompleted";
        Label[Label["AuthSignInFailed"] = 4] = "AuthSignInFailed";
        Label[Label["ClipAttempted"] = 5] = "ClipAttempted";
        Label[Label["Interact"] = 6] = "Interact";
        Label[Label["ViewInWac"] = 7] = "ViewInWac";
        Label[Label["SignOut"] = 8] = "SignOut";
    })(Label = Funnel.Label || (Funnel.Label = {}));
})(Funnel = exports.Funnel || (exports.Funnel = {}));
