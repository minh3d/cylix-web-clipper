"use strict";
var Trace;
(function (Trace) {
    Trace.category = "Trace";
    var Label;
    (function (Label) {
        Label[Label["DefaultingToConsoleLogger"] = 0] = "DefaultingToConsoleLogger";
        Label[Label["DebugMode"] = 1] = "DebugMode";
        Label[Label["RequestForClipperInstalledPageUrl"] = 2] = "RequestForClipperInstalledPageUrl";
    })(Label = Trace.Label || (Trace.Label = {}));
    var Level;
    (function (Level) {
        Level[Level["None"] = 0] = "None";
        Level[Level["Error"] = 1] = "Error";
        Level[Level["Warning"] = 2] = "Warning";
        Level[Level["Information"] = 3] = "Information";
        Level[Level["Verbose"] = 4] = "Verbose";
    })(Level = Trace.Level || (Trace.Level = {}));
})(Trace = exports.Trace || (exports.Trace = {}));
