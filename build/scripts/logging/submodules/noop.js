"use strict";
var NoOp;
(function (NoOp) {
    NoOp.category = "NoOp";
    var Label;
    (function (Label) {
        Label[Label["InitializeCommunicator"] = 0] = "InitializeCommunicator";
        Label[Label["WebClipperUiFrameDidNotExist"] = 1] = "WebClipperUiFrameDidNotExist";
        Label[Label["WebClipperUiFrameIsNotVisible"] = 2] = "WebClipperUiFrameIsNotVisible";
    })(Label = NoOp.Label || (NoOp.Label = {}));
})(NoOp = exports.NoOp || (exports.NoOp = {}));
