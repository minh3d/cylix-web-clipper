"use strict";
var Status;
(function (Status) {
    Status[Status["Succeeded"] = 0] = "Succeeded";
    Status[Status["Failed"] = 1] = "Failed";
})(Status = exports.Status || (exports.Status = {}));
