"use strict";
var Session;
(function (Session) {
    Session.category = "Session";
    var EndTrigger;
    (function (EndTrigger) {
        EndTrigger[EndTrigger["SignOut"] = 0] = "SignOut";
        EndTrigger[EndTrigger["Unload"] = 1] = "Unload";
    })(EndTrigger = Session.EndTrigger || (Session.EndTrigger = {}));
    var State;
    (function (State) {
        State[State["Started"] = 0] = "Started";
        State[State["Ended"] = 1] = "Ended";
    })(State = Session.State || (Session.State = {}));
})(Session = exports.Session || (exports.Session = {}));
