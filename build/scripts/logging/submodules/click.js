"use strict";
var Click;
(function (Click) {
    Click.category = "Click";
    var Label;
    (function (Label) {
        Label.regionSelectionRemoveButton = "RegionSelectionRemoveButton";
        Label.sectionComponent = "SectionComponent";
        Label.sectionPickerLocationContainer = "SectionPickerLocationContainer";
    })(Label = Click.Label || (Click.Label = {}));
})(Click = exports.Click || (exports.Click = {}));
