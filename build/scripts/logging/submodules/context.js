"use strict";
var Context;
(function (Context) {
    var contextKeyToStringMap = {
        AppInfoId: "AppInfo.Id",
        AppInfoVersion: "AppInfo.Version",
        DeviceInfoId: "DeviceInfo.Id",
        ExtensionLifecycleId: "ExtensionLifecycle.Id",
        SessionId: "Session.Id",
        UserInfoId: "UserInfo.Id",
        UserInfoLanguage: "UserInfo.Language",
        AuthType: "AuthType",
        BrowserLanguage: "BrowserLanguage",
        ClipperType: "ClipperType",
        ContentType: "ContentType",
        FlightInfo: "FlightInfo",
        InPrivateBrowsing: "InPrivateBrowsing",
        InvokeHostname: "InvokeHostname",
        PageLanguage: "PageLanguage"
    };
    var Custom;
    (function (Custom) {
        Custom[Custom["AppInfoId"] = 0] = "AppInfoId";
        Custom[Custom["AppInfoVersion"] = 1] = "AppInfoVersion";
        Custom[Custom["ExtensionLifecycleId"] = 2] = "ExtensionLifecycleId";
        Custom[Custom["DeviceInfoId"] = 3] = "DeviceInfoId";
        Custom[Custom["SessionId"] = 4] = "SessionId";
        Custom[Custom["UserInfoId"] = 5] = "UserInfoId";
        Custom[Custom["UserInfoLanguage"] = 6] = "UserInfoLanguage";
        Custom[Custom["AuthType"] = 7] = "AuthType";
        Custom[Custom["BrowserLanguage"] = 8] = "BrowserLanguage";
        Custom[Custom["ClipperType"] = 9] = "ClipperType";
        Custom[Custom["ContentType"] = 10] = "ContentType";
        Custom[Custom["FlightInfo"] = 11] = "FlightInfo";
        Custom[Custom["InPrivateBrowsing"] = 12] = "InPrivateBrowsing";
        Custom[Custom["InvokeHostname"] = 13] = "InvokeHostname";
        Custom[Custom["PageLanguage"] = 14] = "PageLanguage";
    })(Custom = Context.Custom || (Context.Custom = {}));
    function toString(key) {
        return contextKeyToStringMap[Custom[key]];
    }
    Context.toString = toString;
})(Context = exports.Context || (exports.Context = {}));
