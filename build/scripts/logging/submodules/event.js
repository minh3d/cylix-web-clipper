"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var log_1 = require("../log");
var objectUtils_1 = require("../../objectUtils");
var Event;
(function (Event) {
    var Category;
    (function (Category) {
        Category[Category["BaseEvent"] = 0] = "BaseEvent";
        Category[Category["PromiseEvent"] = 1] = "PromiseEvent";
        Category[Category["StreamEvent"] = 2] = "StreamEvent";
    })(Category = Event.Category || (Event.Category = {}));
    var Label;
    (function (Label) {
        Label[Label["AddEmbeddedVideo"] = 0] = "AddEmbeddedVideo";
        Label[Label["AugmentationApiCall"] = 1] = "AugmentationApiCall";
        Label[Label["BookmarkPage"] = 2] = "BookmarkPage";
        Label[Label["CompressRegionSelection"] = 3] = "CompressRegionSelection";
        Label[Label["ClearNoOpTracker"] = 4] = "ClearNoOpTracker";
        Label[Label["Click"] = 5] = "Click";
        Label[Label["ClipAugmentationOptions"] = 6] = "ClipAugmentationOptions";
        Label[Label["ClipCommonOptions"] = 7] = "ClipCommonOptions";
        Label[Label["ClipPdfOptions"] = 8] = "ClipPdfOptions";
        Label[Label["ClipRegionOptions"] = 9] = "ClipRegionOptions";
        Label[Label["ClipSelectionOptions"] = 10] = "ClipSelectionOptions";
        Label[Label["ClipToOneNoteAction"] = 11] = "ClipToOneNoteAction";
        Label[Label["CloseClipper"] = 12] = "CloseClipper";
        Label[Label["ClosePageNavTooltip"] = 13] = "ClosePageNavTooltip";
        Label[Label["CreateNotebook"] = 14] = "CreateNotebook";
        Label[Label["CreatePage"] = 15] = "CreatePage";
        Label[Label["CreateSection"] = 16] = "CreateSection";
        Label[Label["DebugFeedback"] = 17] = "DebugFeedback";
        Label[Label["DeviceIdMap"] = 18] = "DeviceIdMap";
        Label[Label["FetchNonLocalData"] = 19] = "FetchNonLocalData";
        Label[Label["FullPageScreenshotCall"] = 20] = "FullPageScreenshotCall";
        Label[Label["GetBinaryRequest"] = 21] = "GetBinaryRequest";
        Label[Label["GetCleanDom"] = 22] = "GetCleanDom";
        Label[Label["GetExistingUserInformation"] = 23] = "GetExistingUserInformation";
        Label[Label["GetFlightingAssignments"] = 24] = "GetFlightingAssignments";
        Label[Label["GetLocale"] = 25] = "GetLocale";
        Label[Label["GetLocalizedStrings"] = 26] = "GetLocalizedStrings";
        Label[Label["GetNotebookByName"] = 27] = "GetNotebookByName";
        Label[Label["GetNotebooks"] = 28] = "GetNotebooks";
        Label[Label["GetPage"] = 29] = "GetPage";
        Label[Label["GetPageContent"] = 30] = "GetPageContent";
        Label[Label["GetPages"] = 31] = "GetPages";
        Label[Label["HandleSignInEvent"] = 32] = "HandleSignInEvent";
        Label[Label["HideClipperDueToSpaNavigate"] = 33] = "HideClipperDueToSpaNavigate";
        Label[Label["InvokeClipper"] = 34] = "InvokeClipper";
        Label[Label["InvokeTooltip"] = 35] = "InvokeTooltip";
        Label[Label["InvokeWhatsNew"] = 36] = "InvokeWhatsNew";
        Label[Label["LocalFilesNotAllowedPanelShown"] = 37] = "LocalFilesNotAllowedPanelShown";
        Label[Label["PagesSearch"] = 38] = "PagesSearch";
        Label[Label["PdfByteMetadata"] = 39] = "PdfByteMetadata";
        Label[Label["PdfDataUrlMetadata"] = 40] = "PdfDataUrlMetadata";
        Label[Label["ProcessPdfIntoDataUrls"] = 41] = "ProcessPdfIntoDataUrls";
        Label[Label["RegionSelectionCapturing"] = 42] = "RegionSelectionCapturing";
        Label[Label["RegionSelectionLoading"] = 43] = "RegionSelectionLoading";
        Label[Label["RegionSelectionProcessing"] = 44] = "RegionSelectionProcessing";
        Label[Label["RetrieveUserInformation"] = 45] = "RetrieveUserInformation";
        Label[Label["SendBatchRequest"] = 46] = "SendBatchRequest";
        Label[Label["SetContextProperty"] = 47] = "SetContextProperty";
        Label[Label["SetDoNotPromptRatings"] = 48] = "SetDoNotPromptRatings";
        Label[Label["ShouldShowRatingsPrompt"] = 49] = "ShouldShowRatingsPrompt";
        Label[Label["TooltipImpression"] = 50] = "TooltipImpression";
        Label[Label["UpdatePage"] = 51] = "UpdatePage";
        Label[Label["UserInfoUpdated"] = 52] = "UserInfoUpdated";
        Label[Label["WhatsNewImpression"] = 53] = "WhatsNewImpression";
    })(Label = Event.Label || (Event.Label = {}));
    var BaseEvent = (function () {
        function BaseEvent(labelOrData) {
            this._timerWasStopped = false;
            if (this.isEventData(labelOrData)) {
                var eventData = labelOrData;
                this._label = eventData.Label;
                this._duration = eventData.Duration;
                // TODO theoretically, this is a dangerous set
                // because we're not doing the checks found in .setCustomProperty
                this._properties = eventData.Properties ? JSON.parse(JSON.stringify(eventData.Properties)) : undefined;
            }
            else {
                var label = labelOrData;
                this._label = label;
                this.startTimer();
            }
        }
        BaseEvent.prototype.getDuration = function () {
            return this._duration;
        };
        /**
         * Returns the object's event category. Should be overriden by child classes.
         */
        BaseEvent.prototype.getEventCategory = function () {
            return Event.Category.BaseEvent;
        };
        /**
         * Returns a copy of this BaseEvent's internal data
         * (copy to prevent altering of class internals without setters)
         */
        BaseEvent.prototype.getEventData = function () {
            return {
                Label: this._label,
                Duration: this._duration,
                Properties: this.getCustomProperties()
            };
        };
        BaseEvent.prototype.getLabel = function () {
            return Event.Label[this._label];
        };
        /**
         * Returns a copy of this Event's Properties
         * (copy to prevent altering of class internals without .setCustomProperty())
         */
        BaseEvent.prototype.getCustomProperties = function () {
            return this._properties ? JSON.parse(JSON.stringify(this._properties)) : undefined;
        };
        BaseEvent.prototype.setCustomProperty = function (key, value) {
            if (this.isReservedPropertyName(key)) {
                throw new Error("Tried to overwrite key '" + log_1.PropertyName.Custom[key] + "' with value of " + JSON.stringify(value));
            }
            if (!this._properties) {
                this._properties = {};
            }
            this._properties[log_1.PropertyName.Custom[key]] = value;
        };
        /**
         * Calling this multiple times in a row will result in restart of the timer
         */
        BaseEvent.prototype.startTimer = function () {
            this._startTime = new Date().getTime();
        };
        /**
         * If called multiple times in a row, last call wins
         * If called before startTimer(), nothing happens
         */
        BaseEvent.prototype.stopTimer = function () {
            if (this._startTime) {
                this._duration = new Date().getTime() - this._startTime;
                this._timerWasStopped = true;
                return true;
            }
            return false;
        };
        BaseEvent.prototype.timerWasStopped = function () {
            return this._timerWasStopped;
        };
        BaseEvent.prototype.isEventData = function (labelOrData) {
            var tryCastAsEventData = labelOrData;
            if (tryCastAsEventData && !objectUtils_1.ObjectUtils.isNullOrUndefined(tryCastAsEventData.Label)) {
                return true;
            }
            return false;
        };
        BaseEvent.prototype.isReservedPropertyName = function (value) {
            for (var v in log_1.PropertyName.Reserved) {
                if (log_1.PropertyName.Custom[value].toLowerCase() === v.toLowerCase()) {
                    return true;
                }
            }
            return false;
        };
        return BaseEvent;
    }());
    Event.BaseEvent = BaseEvent;
    var PromiseEvent = (function (_super) {
        __extends(PromiseEvent, _super);
        function PromiseEvent(labelOrData) {
            var _this = _super.call(this, labelOrData) || this;
            _this._logStatus = log_1.Status.Succeeded;
            _this._failureType = log_1.Failure.Type.Unexpected;
            if (_this.isEventData(labelOrData)) {
                var eventData = labelOrData;
                _this._logStatus = eventData.LogStatus;
                _this._failureType = eventData.FailureType;
                _this._failureInfo = log_1.ErrorUtils.clone(eventData.FailureInfo);
            }
            return _this;
        }
        PromiseEvent.prototype.getEventCategory = function () {
            return Event.Category.PromiseEvent;
        };
        /**
         * Returns a copy of this PromiseEvent's internal data
         * (copy to prevent altering of class internals without setters)
         */
        PromiseEvent.prototype.getEventData = function () {
            return {
                Label: this._label,
                Duration: this._duration,
                Properties: this.getCustomProperties(),
                LogStatus: this._logStatus,
                FailureType: this._failureType,
                FailureInfo: log_1.ErrorUtils.clone(this._failureInfo)
            };
        };
        PromiseEvent.prototype.getStatus = function () {
            return log_1.Status[this._logStatus];
        };
        PromiseEvent.prototype.setStatus = function (status) {
            this._logStatus = status;
            if (!this._timerWasStopped) {
                this.stopTimer();
            }
        };
        PromiseEvent.prototype.getFailureInfo = function () {
            return log_1.ErrorUtils.toString(this._failureInfo);
        };
        /**
         * Set this PromiseEvent's FailureInfo to a copy of the GenericError passed in
         * (copy to prevent altering of class internals without this setter)
         */
        PromiseEvent.prototype.setFailureInfo = function (failureInfo) {
            this._failureInfo = log_1.ErrorUtils.clone(failureInfo);
        };
        PromiseEvent.prototype.getFailureType = function () {
            return log_1.Failure.Type[this._failureType];
        };
        PromiseEvent.prototype.setFailureType = function (type) {
            this._failureType = type;
        };
        return PromiseEvent;
    }(BaseEvent));
    Event.PromiseEvent = PromiseEvent;
    var StreamEvent = (function (_super) {
        __extends(StreamEvent, _super);
        function StreamEvent(labelOrData) {
            var _this = _super.call(this, labelOrData) || this;
            _this._stream = [];
            if (_this.isEventData(labelOrData)) {
                var eventData = labelOrData;
                _this._stream = eventData.Stream;
            }
            return _this;
        }
        StreamEvent.prototype.getEventCategory = function () {
            return Event.Category.StreamEvent;
        };
        /**
         * Returns a copy of this StreamEvent's internal data
         * (copy to prevent altering of class internals without setters)
         */
        StreamEvent.prototype.getEventData = function () {
            return {
                Label: this._label,
                Duration: this._duration,
                Properties: this.getCustomProperties(),
                Stream: this._stream
            };
        };
        StreamEvent.prototype.append = function (streamItem) {
            this._stream.push(streamItem);
        };
        return StreamEvent;
    }(BaseEvent));
    Event.StreamEvent = StreamEvent;
    function createEvent(eventCategory, eventData) {
        switch (eventCategory) {
            default:
            case Event.Category.BaseEvent:
                return new Event.BaseEvent(eventData);
            case Event.Category.PromiseEvent:
                return new Event.PromiseEvent(eventData);
            case Event.Category.StreamEvent:
                return new Event.StreamEvent(eventData);
        }
    }
    Event.createEvent = createEvent;
})(Event = exports.Event || (exports.Event = {}));
