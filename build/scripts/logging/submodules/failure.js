"use strict";
var Failure;
(function (Failure) {
    Failure.category = "Failure";
    var Type;
    (function (Type) {
        Type[Type["Unexpected"] = 0] = "Unexpected";
        Type[Type["Expected"] = 1] = "Expected"; /* ICE */
    })(Type = Failure.Type || (Failure.Type = {}));
    function getStackTrace(err) {
        if (!err) {
            err = new Error();
        }
        return err.stack;
    }
    Failure.getStackTrace = getStackTrace;
    var Label;
    (function (Label) {
        /* unexpected */
        Label[Label["ClickedButtonWithNoId"] = 0] = "ClickedButtonWithNoId";
        Label[Label["EndSessionWithoutTrigger"] = 1] = "EndSessionWithoutTrigger";
        Label[Label["GetChangeLog"] = 2] = "GetChangeLog";
        Label[Label["GetComputedStyle"] = 3] = "GetComputedStyle";
        Label[Label["GetLocalizedString"] = 4] = "GetLocalizedString";
        Label[Label["GetSetting"] = 5] = "GetSetting";
        Label[Label["IFrameMessageHandlerHasNoOtherWindow"] = 6] = "IFrameMessageHandlerHasNoOtherWindow";
        Label[Label["InvalidArgument"] = 7] = "InvalidArgument";
        Label[Label["IsFeatureEnabled"] = 8] = "IsFeatureEnabled";
        Label[Label["JsonParse"] = 9] = "JsonParse";
        Label[Label["NotImplemented"] = 10] = "NotImplemented";
        Label[Label["OnLaunchOneNoteButton"] = 11] = "OnLaunchOneNoteButton";
        Label[Label["OrphanedWebClippersDueToExtensionRefresh"] = 12] = "OrphanedWebClippersDueToExtensionRefresh";
        Label[Label["RegionSelectionProcessing"] = 13] = "RegionSelectionProcessing";
        Label[Label["RenderFailurePanel"] = 14] = "RenderFailurePanel";
        Label[Label["ReservedPropertyOverwriteAttempted"] = 15] = "ReservedPropertyOverwriteAttempted";
        Label[Label["SessionAlreadySet"] = 16] = "SessionAlreadySet";
        Label[Label["SetLoggerNoop"] = 17] = "SetLoggerNoop";
        Label[Label["SetUndefinedLocalizedStrings"] = 18] = "SetUndefinedLocalizedStrings";
        Label[Label["TraceLevelErrorWarningMessage"] = 19] = "TraceLevelErrorWarningMessage";
        Label[Label["UnhandledApiCode"] = 20] = "UnhandledApiCode";
        Label[Label["UnhandledExceptionThrown"] = 21] = "UnhandledExceptionThrown";
        Label[Label["UserSetWithInvalidExpiredData"] = 22] = "UserSetWithInvalidExpiredData";
        Label[Label["WebExtensionWindowCreate"] = 23] = "WebExtensionWindowCreate";
        /* expected */
        Label[Label["UnclippablePage"] = 24] = "UnclippablePage";
        Label[Label["UnsupportedBrowser"] = 25] = "UnsupportedBrowser";
    })(Label = Failure.Label || (Failure.Label = {}));
})(Failure = exports.Failure || (exports.Failure = {}));
