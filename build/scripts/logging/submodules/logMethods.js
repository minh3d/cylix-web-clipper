"use strict";
var LogMethods;
(function (LogMethods) {
    LogMethods[LogMethods["LogEvent"] = 0] = "LogEvent";
    LogMethods[LogMethods["LogFailure"] = 1] = "LogFailure";
    LogMethods[LogMethods["PushToStream"] = 2] = "PushToStream";
    LogMethods[LogMethods["LogFunnel"] = 3] = "LogFunnel";
    LogMethods[LogMethods["LogSession"] = 4] = "LogSession";
    LogMethods[LogMethods["LogSessionStart"] = 5] = "LogSessionStart";
    LogMethods[LogMethods["LogSessionEnd"] = 6] = "LogSessionEnd";
    LogMethods[LogMethods["LogTrace"] = 7] = "LogTrace";
    LogMethods[LogMethods["LogClickEvent"] = 8] = "LogClickEvent";
    LogMethods[LogMethods["SetContextProperty"] = 9] = "SetContextProperty";
})(LogMethods = exports.LogMethods || (exports.LogMethods = {}));
