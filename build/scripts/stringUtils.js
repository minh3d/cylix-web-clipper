"use strict";
var objectUtils_1 = require("./objectUtils");
var operationResult_1 = require("./operationResult");
var localization_1 = require("./localization/localization");
var _ = require("lodash");
var StringUtils;
(function (StringUtils) {
    /**
     * Takes a range of the form 1,3-6,7,8,13,1,3,4,a-b, etc. and then returns an array
     * corresponding to the numbers in that range. It ignores invalid input, sorts it, and removes duplicates
     */
    function parsePageRange(text, maxRange) {
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(text)) {
            return asFailedOperation("");
        }
        text = text.trim();
        if (text === "") {
            return asFailedOperation("");
        }
        var splitText = text.split(",");
        var range = [];
        for (var i = 0; i < splitText.length; ++i) {
            var valueToAppend = [], matches = void 0;
            var currentValue = splitText[i].trim();
            if (currentValue === "") {
                // We relax the restriction by allowing and ignoring whitespace between commas
                continue;
            }
            if (/^\d+$/.test(currentValue)) {
                var digit = parseInt(currentValue, 10 /* radix */);
                if (digit === 0 || !objectUtils_1.ObjectUtils.isNullOrUndefined(maxRange) && digit > maxRange) {
                    return asFailedOperation(currentValue);
                }
                valueToAppend = [digit];
            }
            else if (matches = /^(\d+)\s*-\s*(\d+)$/.exec(currentValue)) {
                var lhs = parseInt(matches[1], 10), rhs = parseInt(matches[2], 10);
                // Try and catch an invalid range as soon as possible, before we compute the range
                // We also define a maxRangeAllowed as 2^32, which is the max size of an array in JS
                var maxRangeSizeAllowed = 4294967295;
                if (lhs >= rhs || lhs === 0 || rhs === 0 || lhs >= maxRangeSizeAllowed || rhs >= maxRangeSizeAllowed ||
                    rhs - lhs + 1 > maxRangeSizeAllowed || (!objectUtils_1.ObjectUtils.isNullOrUndefined(maxRange) && rhs > maxRange)) {
                    return asFailedOperation(currentValue);
                }
                valueToAppend = _.range(lhs, rhs + 1);
            }
            else {
                // The currentValue is not a single digit or a valid range
                return asFailedOperation(currentValue);
            }
            range = range.concat(valueToAppend);
        }
        var parsedPageRange = _(range).sortBy().sortedUniq().value();
        if (parsedPageRange.length === 0) {
            return asFailedOperation(text);
        }
        var last = _.last(parsedPageRange);
        if (!objectUtils_1.ObjectUtils.isNullOrUndefined(maxRange) && last > maxRange) {
            return asFailedOperation(last.toString());
        }
        return asSucceededOperation(parsedPageRange);
    }
    StringUtils.parsePageRange = parsePageRange;
    function asSucceededOperation(obj) {
        return {
            status: operationResult_1.OperationResult.Succeeded,
            result: obj
        };
    }
    function asFailedOperation(obj) {
        return {
            status: operationResult_1.OperationResult.Failed,
            result: obj
        };
    }
    function countPageRange(text) {
        var operation = parsePageRange(text);
        if (operation.status !== operationResult_1.OperationResult.Succeeded) {
            return 0;
        }
        var pages = operation.result;
        return pages ? pages.length : 0;
    }
    StringUtils.countPageRange = countPageRange;
    function getBatchedPageTitle(titleOfDocument, pageIndex) {
        var firstPageNumberAsString = (pageIndex + 1).toString();
        return titleOfDocument + ": " + localization_1.Localization.getLocalizedString("WebClipper.Label.Page") + " " + firstPageNumberAsString;
    }
    StringUtils.getBatchedPageTitle = getBatchedPageTitle;
    function generateGuid() {
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c === "x" ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
    StringUtils.generateGuid = generateGuid;
})(StringUtils = exports.StringUtils || (exports.StringUtils = {}));
