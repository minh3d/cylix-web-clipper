"use strict";
var objectUtils_1 = require("./objectUtils");
var CookieUtils = (function () {
    function CookieUtils() {
    }
    CookieUtils.readCookie = function (cookieName, doc) {
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(doc)) {
            doc = document;
        }
        var cookieKVPairs = document.cookie.split(";").map(function (kvPair) { return kvPair.split("="); });
        for (var _i = 0, cookieKVPairs_1 = cookieKVPairs; _i < cookieKVPairs_1.length; _i++) {
            var cookie = cookieKVPairs_1[_i];
            if (cookie[0].trim() === cookieName) {
                return cookie[1].trim();
            }
        }
    };
    return CookieUtils;
}());
exports.CookieUtils = CookieUtils;
