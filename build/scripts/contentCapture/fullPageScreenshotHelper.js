"use strict";
var frontEndGlobals_1 = require("../clipperUI/frontEndGlobals");
var oneNoteApiUtils_1 = require("../clipperUI/oneNoteApiUtils");
var httpWithRetries_1 = require("../http/httpWithRetries");
var Log = require("../logging/log");
var constants_1 = require("../constants");
var settings_1 = require("../settings");
var stringUtils_1 = require("../stringUtils");
var FullPageScreenshotHelper = (function () {
    function FullPageScreenshotHelper() {
    }
    FullPageScreenshotHelper.getFullPageScreenshot = function (pageInfoContentData) {
        return new Promise(function (resolve, reject) {
            frontEndGlobals_1.Clipper.getUserSessionIdWhenDefined().then(function (sessionId) {
                var fullPageScreenshotEvent = new Log.Event.PromiseEvent(Log.Event.Label.FullPageScreenshotCall);
                var correlationId = stringUtils_1.StringUtils.generateGuid();
                fullPageScreenshotEvent.setCustomProperty(Log.PropertyName.Custom.CorrelationId, correlationId);
                var headers = {};
                headers[constants_1.Constants.HeaderValues.accept] = "application/json";
                headers[constants_1.Constants.HeaderValues.appIdKey] = settings_1.Settings.getSetting("App_Id");
                headers[constants_1.Constants.HeaderValues.noAuthKey] = "true";
                headers[constants_1.Constants.HeaderValues.correlationId] = correlationId;
                headers[constants_1.Constants.HeaderValues.userSessionIdKey] = sessionId;
                var errorCallback = function (error) {
                    fullPageScreenshotEvent.setCustomProperty(Log.PropertyName.Custom.CorrelationId, error.responseHeaders[constants_1.Constants.HeaderValues.correlationId]);
                    oneNoteApiUtils_1.OneNoteApiUtils.logOneNoteApiRequestError(fullPageScreenshotEvent, error);
                };
                httpWithRetries_1.HttpWithRetries.post(constants_1.Constants.Urls.fullPageScreenshotUrl, pageInfoContentData, headers, [200, 204], FullPageScreenshotHelper.timeout).then(function (request) {
                    if (request.status === 200) {
                        try {
                            resolve(JSON.parse(request.response));
                            fullPageScreenshotEvent.setCustomProperty(Log.PropertyName.Custom.FullPageScreenshotContentFound, true);
                        }
                        catch (e) {
                            errorCallback(OneNoteApi.ErrorUtils.createRequestErrorObject(request, OneNoteApi.RequestErrorType.UNABLE_TO_PARSE_RESPONSE));
                            reject();
                        }
                    }
                    else {
                        fullPageScreenshotEvent.setCustomProperty(Log.PropertyName.Custom.FullPageScreenshotContentFound, false);
                        reject();
                    }
                }, function (error) {
                    errorCallback(error);
                    reject();
                }).then(function () {
                    frontEndGlobals_1.Clipper.logger.logEvent(fullPageScreenshotEvent);
                });
            });
        });
    };
    return FullPageScreenshotHelper;
}());
FullPageScreenshotHelper.timeout = 50000;
exports.FullPageScreenshotHelper = FullPageScreenshotHelper;
