"use strict";
var arrayUtils_1 = require("../arrayUtils");
var _ = require("lodash");
var PdfJsDocument = (function () {
    function PdfJsDocument(pdf) {
        this.pdf = pdf;
    }
    PdfJsDocument.prototype.numPages = function () {
        return this.pdf.numPages;
    };
    PdfJsDocument.prototype.getByteLength = function () {
        return this.getData().then(function (buffer) {
            return Promise.resolve(buffer.length);
        });
    };
    PdfJsDocument.prototype.getData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.pdf.getData().then(function (buffer) {
                resolve(buffer);
            });
        });
    };
    PdfJsDocument.prototype.getPageListAsDataUrls = function (pageIndexes) {
        var _this = this;
        var dataUrls = new Array(pageIndexes.length);
        return new Promise(function (resolve) {
            var _loop_1 = function (i) {
                _this.getPageAsDataUrl(pageIndexes[i]).then(function (dataUrl) {
                    dataUrls[i] = dataUrl;
                    if (arrayUtils_1.ArrayUtils.isArrayComplete(dataUrls)) {
                        resolve(dataUrls);
                    }
                });
            };
            for (var i = 0; i < pageIndexes.length; i++) {
                _loop_1(i);
            }
        });
    };
    PdfJsDocument.prototype.getPageAsDataUrl = function (pageIndex) {
        var _this = this;
        return new Promise(function (resolve) {
            // In pdf.js, indexes start at 1
            _this.pdf.getPage(pageIndex + 1).then(function (page) {
                // Issue #369: When the scale is set to 1, we end up with poor quality images
                // on high resolution machines. The best explanation I found for this was in
                // this article: http://stackoverflow.com/questions/35400722/pdf-image-quality-is-bad-using-pdf-js
                // Note that we played around with setting the scale from 3-5, which looked better on high
                // resolution monitors - but did not look good at all at lower resolutions. scale=2 seems
                // to be the happy medium.
                var viewport = page.getViewport(2 /* scale */);
                var canvas = document.createElement("canvas");
                var context = canvas.getContext("2d");
                canvas.height = viewport.height;
                canvas.width = viewport.width;
                var renderContext = {
                    canvasContext: context,
                    viewport: viewport
                };
                page.render(renderContext).then(function () {
                    resolve(canvas.toDataURL());
                });
            });
        });
    };
    PdfJsDocument.prototype.getAllPageViewportDimensions = function () {
        var allPageIndexes = _.range(this.numPages());
        return this.getPageListViewportDimensions(allPageIndexes);
    };
    PdfJsDocument.prototype.getPageListViewportDimensions = function (pageIndexes) {
        var _this = this;
        var dimensions = new Array(pageIndexes.length);
        return new Promise(function (resolve) {
            var _loop_2 = function (i) {
                _this.getPageViewportDimensions(pageIndexes[i]).then(function (dimension) {
                    dimensions[i] = dimension;
                    if (arrayUtils_1.ArrayUtils.isArrayComplete(dimensions)) {
                        resolve(dimensions);
                    }
                });
            };
            for (var i = 0; i < pageIndexes.length; i++) {
                _loop_2(i);
            }
        });
    };
    PdfJsDocument.prototype.getPageViewportDimensions = function (pageIndex) {
        var _this = this;
        return new Promise(function (resolve) {
            // In pdf.js, indexes start at 1
            _this.pdf.getPage(pageIndex + 1).then(function (page) {
                var viewport = page.getViewport(1 /* scale */);
                resolve({
                    height: viewport.height,
                    width: viewport.width
                });
            });
        });
    };
    return PdfJsDocument;
}());
exports.PdfJsDocument = PdfJsDocument;
