"use strict";
var constants_1 = require("../constants");
var settings_1 = require("../settings");
var stringUtils_1 = require("../stringUtils");
var objectUtils_1 = require("../objectUtils");
var frontEndGlobals_1 = require("../clipperUI/frontEndGlobals");
var oneNoteApiUtils_1 = require("../clipperUI/oneNoteApiUtils");
var domUtils_1 = require("../domParsers/domUtils");
var HttpWithRetries_1 = require("../http/HttpWithRetries");
var Log = require("../logging/log");
var AugmentationModel;
(function (AugmentationModel) {
    AugmentationModel[AugmentationModel["None"] = 0] = "None";
    AugmentationModel[AugmentationModel["Article"] = 1] = "Article";
    AugmentationModel[AugmentationModel["BizCard"] = 2] = "BizCard";
    AugmentationModel[AugmentationModel["EntityKnowledge"] = 3] = "EntityKnowledge";
    AugmentationModel[AugmentationModel["Recipe"] = 4] = "Recipe";
    AugmentationModel[AugmentationModel["Product"] = 5] = "Product";
    AugmentationModel[AugmentationModel["Screenshot"] = 6] = "Screenshot";
    AugmentationModel[AugmentationModel["Wrapstar"] = 7] = "Wrapstar";
})(AugmentationModel = exports.AugmentationModel || (exports.AugmentationModel = {}));
var AugmentationHelper = (function () {
    function AugmentationHelper() {
    }
    AugmentationHelper.augmentPage = function (url, locale, pageContent) {
        return new Promise(function (resolve, reject) {
            var augmentationEvent = new Log.Event.PromiseEvent(Log.Event.Label.AugmentationApiCall);
            var correlationId = stringUtils_1.StringUtils.generateGuid();
            augmentationEvent.setCustomProperty(Log.PropertyName.Custom.CorrelationId, correlationId);
            AugmentationHelper.makeAugmentationRequest(url, locale, pageContent, correlationId).then(function (responsePackage) {
                var parsedResponse = responsePackage.parsedResponse;
                var result = { ContentModel: AugmentationModel.None, ContentObjects: [] };
                augmentationEvent.setCustomProperty(Log.PropertyName.Custom.CorrelationId, responsePackage.request.getResponseHeader(constants_1.Constants.HeaderValues.correlationId));
                if (parsedResponse && parsedResponse.length > 0 && parsedResponse[0].ContentInHtml) {
                    result = parsedResponse[0];
                    augmentationEvent.setCustomProperty(Log.PropertyName.Custom.AugmentationModel, AugmentationModel[result.ContentModel]);
                    // Remove tags that are unsupported by ONML before we display them in the preview
                    // Supported tags: https://msdn.microsoft.com/en-us/library/office/dn575442.aspx
                    var doc_1 = (new DOMParser()).parseFromString(result.ContentInHtml, "text/html");
                    var previewElement_1 = AugmentationHelper.getArticlePreviewElement(doc_1);
                    domUtils_1.DomUtils.toOnml(doc_1).then(function () {
                        domUtils_1.DomUtils.addPreviewContainerStyling(previewElement_1);
                        AugmentationHelper.addSupportedVideosToElement(previewElement_1, pageContent, url);
                        result.ContentInHtml = doc_1.body.innerHTML;
                        resolve(result);
                    });
                }
                else {
                    resolve(result);
                }
                augmentationEvent.setCustomProperty(Log.PropertyName.Custom.AugmentationModel, AugmentationModel[result.ContentModel]);
            })["catch"](function (failure) {
                oneNoteApiUtils_1.OneNoteApiUtils.logOneNoteApiRequestError(augmentationEvent, failure);
                reject();
            }).then(function () {
                frontEndGlobals_1.Clipper.logger.logEvent(augmentationEvent);
            });
        });
    };
    AugmentationHelper.getAugmentationType = function (state) {
        // Default type to Article mode
        var augmentationType = AugmentationModel[AugmentationModel.Article].toString();
        if (!state || !state.augmentationResult || !state.augmentationResult.data) {
            return augmentationType;
        }
        // TODO: There is a work-item to change the AugmentationApi to return ContentModel as a StringUtils
        // instead of an integer
        var contentModel = state.augmentationResult.data.ContentModel;
        if (AugmentationHelper.isSupportedAugmentationType(contentModel)) {
            augmentationType = AugmentationModel[contentModel].toString();
        }
        return augmentationType;
    };
    /*
     * Returns the augmented preview text.
     */
    AugmentationHelper.makeAugmentationRequest = function (url, locale, pageContent, requestCorrelationId) {
        return frontEndGlobals_1.Clipper.getUserSessionIdWhenDefined().then(function (sessionId) {
            var augmentationApiUrl = constants_1.Constants.Urls.augmentationApiUrl + "?renderMethod=extractAggressive&url=" + url + "&lang=" + locale;
            var headers = {};
            headers[constants_1.Constants.HeaderValues.appIdKey] = settings_1.Settings.getSetting("App_Id");
            headers[constants_1.Constants.HeaderValues.noAuthKey] = "true";
            headers[constants_1.Constants.HeaderValues.correlationId] = requestCorrelationId;
            headers[constants_1.Constants.HeaderValues.userSessionIdKey] = sessionId;
            return HttpWithRetries_1.HttpWithRetries.post(augmentationApiUrl, pageContent, headers).then(function (request) {
                var parsedResponse;
                try {
                    parsedResponse = JSON.parse(request.response);
                }
                catch (e) {
                    frontEndGlobals_1.Clipper.logger.logJsonParseUnexpected(request.response);
                    return Promise.reject(OneNoteApi.ErrorUtils.createRequestErrorObject(request, OneNoteApi.RequestErrorType.UNABLE_TO_PARSE_RESPONSE));
                }
                var responsePackage = {
                    parsedResponse: parsedResponse,
                    request: request
                };
                return Promise.resolve(responsePackage);
            });
        });
    };
    AugmentationHelper.getArticlePreviewElement = function (doc) {
        var mainContainers = doc.getElementsByClassName("MainArticleContainer");
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(mainContainers) || objectUtils_1.ObjectUtils.isNullOrUndefined(mainContainers[0])) {
            return doc.body;
        }
        return mainContainers[0];
    };
    AugmentationHelper.isSupportedAugmentationType = function (contentModel) {
        return contentModel === AugmentationModel.Article ||
            contentModel === AugmentationModel.Recipe ||
            contentModel === AugmentationModel.Product;
    };
    AugmentationHelper.addSupportedVideosToElement = function (previewElement, pageContent, url) {
        var addEmbeddedVideoEvent = new Log.Event.PromiseEvent(Log.Event.Label.AddEmbeddedVideo); // start event timer, just in case it gets logged
        addEmbeddedVideoEvent.setCustomProperty(Log.PropertyName.Custom.Url, url);
        domUtils_1.DomUtils.addEmbeddedVideosWhereSupported(previewElement, pageContent, url).then(function (videoSrcUrls) {
            // only log when supported video is found on page
            if (!objectUtils_1.ObjectUtils.isNullOrUndefined(videoSrcUrls)) {
                addEmbeddedVideoEvent.setCustomProperty(Log.PropertyName.Custom.VideoSrcUrl, JSON.stringify(videoSrcUrls.map(function (v) { return v.srcAttribute; })));
                addEmbeddedVideoEvent.setCustomProperty(Log.PropertyName.Custom.VideoDataOriginalSrcUrl, JSON.stringify(videoSrcUrls.map(function (v) { return v.dataOriginalSrcAttribute; })));
                frontEndGlobals_1.Clipper.logger.logEvent(addEmbeddedVideoEvent);
            }
        }, function (error) {
            addEmbeddedVideoEvent.setStatus(Log.Status.Failed);
            addEmbeddedVideoEvent.setFailureInfo(error);
            frontEndGlobals_1.Clipper.logger.logEvent(addEmbeddedVideoEvent);
        });
    };
    return AugmentationHelper;
}());
exports.AugmentationHelper = AugmentationHelper;
