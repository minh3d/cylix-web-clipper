"use strict";
var objectUtils_1 = require("../objectUtils");
var frontEndGlobals_1 = require("../clipperUI/frontEndGlobals");
var domUtils_1 = require("../domParsers/domUtils");
var Log = require("../logging/log");
var BookmarkHelper = (function () {
    function BookmarkHelper() {
    }
    /**
     * Grab useful metadata for a summary/bookmark view of a page from the provided DOM elements
     *
     * allowFallback: if true, we will attempt to infer bookmarking info from metadata
     * that does not adhere to the Open Graph protocol (http://ogp.me/)
     */
    BookmarkHelper.bookmarkPage = function (url, pageTitle, metadataElements, allowFallback, imageElements, textElements) {
        if (allowFallback === void 0) { allowFallback = false; }
        var bookmarkPageEvent = new Log.Event.PromiseEvent(Log.Event.Label.BookmarkPage);
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(url) || url === "") {
            var error = { error: "Page url is null, undefined, or empty", url: url };
            bookmarkPageEvent.setStatus(Log.Status.Failed);
            bookmarkPageEvent.setFailureInfo(error);
            frontEndGlobals_1.Clipper.logger.logEvent(bookmarkPageEvent);
            return Promise.reject(error);
        }
        var result = {
            url: url,
            title: pageTitle
        };
        var bookmarkLoggingInfo = {
            metadataElementsExist: true,
            pageTitleExists: true,
            descriptionMetadataUsed: "",
            thumbnailSrcMetadataUsed: "",
            thumbnailSrcToDataUrlFailure: undefined
        };
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(pageTitle) || pageTitle.length === 0) {
            bookmarkLoggingInfo.pageTitleExists = false;
        }
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(metadataElements) || metadataElements.length === 0) {
            bookmarkLoggingInfo.metadataElementsExist = false;
            bookmarkPageEvent.setCustomProperty(Log.PropertyName.Custom.BookmarkInfo, JSON.stringify(bookmarkLoggingInfo));
            frontEndGlobals_1.Clipper.logger.logEvent(bookmarkPageEvent);
            return Promise.resolve(result);
        }
        var descriptionResult = this.getPrimaryDescription(metadataElements);
        if (allowFallback && objectUtils_1.ObjectUtils.isNullOrUndefined(descriptionResult)) {
            descriptionResult = BookmarkHelper.getFallbackDescription(metadataElements);
            if (objectUtils_1.ObjectUtils.isNullOrUndefined(descriptionResult)) {
                // concatenate text on the page if all else fails
                descriptionResult = BookmarkHelper.getTextOnPage(textElements);
            }
        }
        var thumbnailSrcResult = this.getPrimaryThumbnailSrc(metadataElements);
        if (allowFallback && objectUtils_1.ObjectUtils.isNullOrUndefined(thumbnailSrcResult)) {
            thumbnailSrcResult = BookmarkHelper.getFallbackThumbnailSrc(metadataElements);
            if (objectUtils_1.ObjectUtils.isNullOrUndefined(thumbnailSrcResult)) {
                // get first image on the page as thumbnail if all else fails
                thumbnailSrcResult = BookmarkHelper.getFirstImageOnPage(imageElements);
            }
        }
        // populate final result object and log
        if (!objectUtils_1.ObjectUtils.isNullOrUndefined(descriptionResult)) {
            descriptionResult.description = BookmarkHelper.truncateString(descriptionResult.description);
            result.description = descriptionResult.description;
            bookmarkLoggingInfo.descriptionMetadataUsed = descriptionResult.metadataUsed.value;
        }
        if (!objectUtils_1.ObjectUtils.isNullOrUndefined(thumbnailSrcResult)) {
            bookmarkLoggingInfo.thumbnailSrcMetadataUsed = thumbnailSrcResult.metadataUsed.value;
            thumbnailSrcResult.thumbnailSrc = domUtils_1.DomUtils.toAbsoluteUrl(thumbnailSrcResult.thumbnailSrc, url);
            return domUtils_1.DomUtils.getImageDataUrl(thumbnailSrcResult.thumbnailSrc).then(function (thumbnailDataUrl) {
                if (!objectUtils_1.ObjectUtils.isNullOrUndefined(thumbnailDataUrl)) {
                    result.thumbnailSrc = thumbnailDataUrl;
                }
                else {
                    result.thumbnailSrc = thumbnailSrcResult.thumbnailSrc;
                    bookmarkLoggingInfo.thumbnailSrcToDataUrlFailure = "thumbnail conversion to data url returned undefined. falling back to non-data url as source: " + thumbnailSrcResult.thumbnailSrc;
                }
            })["catch"](function (error) {
                bookmarkLoggingInfo.thumbnailSrcToDataUrlFailure = error.error;
            }).then(function () {
                bookmarkPageEvent.setCustomProperty(Log.PropertyName.Custom.BookmarkInfo, JSON.stringify(bookmarkLoggingInfo));
                frontEndGlobals_1.Clipper.logger.logEvent(bookmarkPageEvent);
                return Promise.resolve(result);
            });
        }
        else {
            bookmarkPageEvent.setCustomProperty(Log.PropertyName.Custom.BookmarkInfo, JSON.stringify(bookmarkLoggingInfo));
            frontEndGlobals_1.Clipper.logger.logEvent(bookmarkPageEvent);
            return Promise.resolve(result);
        }
    };
    /**
     * Wrapper for native JS getElementsByTagName() which adds ability to provide multiple tag names
     */
    BookmarkHelper.getElementsByTagName = function (root, tagNames) {
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(root) || objectUtils_1.ObjectUtils.isNullOrUndefined(tagNames)) {
            return;
        }
        var elements = new Array();
        for (var _i = 0, tagNames_1 = tagNames; _i < tagNames_1.length; _i++) {
            var tag = tagNames_1[_i];
            elements = elements.concat(elements, Array.prototype.slice.call(root.getElementsByTagName(tag)));
        }
        return elements;
    };
    /**
     * Get non-whitespace text elements from the document
     *
     * cleanDoc: if true, THIS WILL MODIFY THE DOCUMENT PROVIDED -
     * we will attempt to make the document ONML-friendly before grabbing text elements
     */
    BookmarkHelper.getNonWhiteSpaceTextElements = function (doc, cleanDoc) {
        if (cleanDoc === void 0) { cleanDoc = false; }
        if (cleanDoc) {
            domUtils_1.DomUtils.removeElementsNotSupportedInOnml(doc);
        }
        return domUtils_1.DomUtils.textNodesNoWhitespaceUnder(doc);
    };
    BookmarkHelper.getPrimaryDescription = function (metadataElements) {
        var metadata = BookmarkHelper.primaryDescriptionKeyValuePair;
        var description = BookmarkHelper.getMetaContent(metadataElements, metadata);
        if (!objectUtils_1.ObjectUtils.isNullOrUndefined(description)) {
            return { metadataUsed: metadata, description: description };
        }
    };
    BookmarkHelper.getFallbackDescription = function (metadataElements) {
        for (var _i = 0, _a = BookmarkHelper.fallbackDescriptionKeyValuePairs; _i < _a.length; _i++) {
            var metadata = _a[_i];
            var description = BookmarkHelper.getMetaContent(metadataElements, metadata);
            if (!objectUtils_1.ObjectUtils.isNullOrUndefined(description)) {
                return { metadataUsed: metadata, description: description };
            }
        }
    };
    BookmarkHelper.getTextOnPage = function (textElements, numberOfWords) {
        if (numberOfWords === void 0) { numberOfWords = 50; }
        if (!objectUtils_1.ObjectUtils.isNullOrUndefined(textElements) && textElements.length > 0) {
            var metadata = BookmarkHelper.textOnPageKeyValuePair;
            var description = textElements.map(function (text) { return text.wholeText.trim(); }).join(" ");
            if (!objectUtils_1.ObjectUtils.isNullOrUndefined(description) && description.length > 0) {
                return { metadataUsed: metadata, description: description };
            }
        }
    };
    BookmarkHelper.truncateString = function (longStr, numChars) {
        if (numChars === void 0) { numChars = BookmarkHelper.maxNumCharsInDescription; }
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(longStr)) {
            return;
        }
        if (longStr.length === 0) {
            return "";
        }
        if (longStr.length <= numChars) {
            return longStr;
        }
        var ellipsisAppend = "...";
        var truncateRegEx = new RegExp("^(.{" + numChars + "}[^\\s]*)");
        var truncateMatch = longStr.replace(/\s+/g, " ").match(truncateRegEx);
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(truncateMatch)) {
            return;
        }
        return truncateMatch[1] + ellipsisAppend;
    };
    BookmarkHelper.getPrimaryThumbnailSrc = function (metaTags) {
        var metadata = BookmarkHelper.primaryThumbnailKeyValuePair;
        var imgSrc = BookmarkHelper.getMetaContent(metaTags, metadata);
        if (!objectUtils_1.ObjectUtils.isNullOrUndefined(imgSrc)) {
            return { metadataUsed: metadata, thumbnailSrc: imgSrc };
        }
    };
    BookmarkHelper.getFallbackThumbnailSrc = function (metaTags) {
        for (var _i = 0, _a = BookmarkHelper.fallbackThumbnailKeyValuePairs; _i < _a.length; _i++) {
            var metadata = _a[_i];
            var imgSrc = BookmarkHelper.getMetaContent(metaTags, metadata);
            if (!objectUtils_1.ObjectUtils.isNullOrUndefined(imgSrc)) {
                return { metadataUsed: metadata, thumbnailSrc: imgSrc };
            }
        }
    };
    BookmarkHelper.getFirstImageOnPage = function (imageElements) {
        if (!objectUtils_1.ObjectUtils.isNullOrUndefined(imageElements) && imageElements.length > 0) {
            var imgSrc = imageElements[0].getAttribute(BookmarkHelper.srcAttrName);
            var metadata = BookmarkHelper.firstImageOnPageKeyValuePair;
            if (!objectUtils_1.ObjectUtils.isNullOrUndefined(imgSrc) && imgSrc !== "") {
                return { metadataUsed: metadata, thumbnailSrc: imgSrc };
            }
        }
    };
    BookmarkHelper.getMetaContent = function (metaTags, metadata) {
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(metaTags) ||
            objectUtils_1.ObjectUtils.isNullOrUndefined(metadata) ||
            objectUtils_1.ObjectUtils.isNullOrUndefined(metadata.key) ||
            objectUtils_1.ObjectUtils.isNullOrUndefined(metadata.value)) {
            return;
        }
        for (var _i = 0, metaTags_1 = metaTags; _i < metaTags_1.length; _i++) {
            var tag = metaTags_1[_i];
            var attributeValue = tag.getAttribute(metadata.key);
            if (attributeValue &&
                attributeValue.toLowerCase().split(/\s/).indexOf(metadata.value.toLowerCase()) > -1) {
                var contentAttr = void 0;
                if (tag.nodeName === "LINK") {
                    contentAttr = "href";
                }
                if (tag.nodeName === "META") {
                    contentAttr = "content";
                }
                var content = tag.getAttribute(contentAttr);
                if (objectUtils_1.ObjectUtils.isNullOrUndefined(content) || content.length === 0) {
                    return;
                }
                return content;
            }
        }
    };
    return BookmarkHelper;
}());
BookmarkHelper.maxNumCharsInDescription = 140;
BookmarkHelper.metadataTagNames = [domUtils_1.DomUtils.tags.meta, domUtils_1.DomUtils.tags.link];
BookmarkHelper.nameAttrName = "name";
BookmarkHelper.propertyAttrName = "property";
BookmarkHelper.relAttrName = "rel";
BookmarkHelper.srcAttrName = "src";
BookmarkHelper.primaryDescriptionKeyValuePair = { key: BookmarkHelper.propertyAttrName, value: "og:description" };
BookmarkHelper.primaryThumbnailKeyValuePair = { key: BookmarkHelper.propertyAttrName, value: "og:image" };
BookmarkHelper.firstImageOnPageKeyValuePair = { key: "", value: "firstImageOnPage" };
BookmarkHelper.textOnPageKeyValuePair = { key: "", value: "textOnPage" };
// list is ordered by fallback priority
BookmarkHelper.fallbackDescriptionKeyValuePairs = [
    { key: BookmarkHelper.nameAttrName, value: "description" },
    { key: BookmarkHelper.nameAttrName, value: "twitter:description" },
    { key: BookmarkHelper.nameAttrName, value: "keywords" },
    { key: BookmarkHelper.propertyAttrName, value: "article:tag" }
];
// list is ordered by fallback priority
BookmarkHelper.fallbackThumbnailKeyValuePairs = [
    { key: BookmarkHelper.nameAttrName, value: "twitter:image:src" },
    { key: BookmarkHelper.nameAttrName, value: "twitter:image" },
    { key: BookmarkHelper.relAttrName, value: "image_src" },
    { key: BookmarkHelper.relAttrName, value: "icon" }
];
exports.BookmarkHelper = BookmarkHelper;
