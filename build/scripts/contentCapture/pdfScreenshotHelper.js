"use strict";
var frontEndGlobals_1 = require("../clipperUI/frontEndGlobals");
var Log = require("../logging/log");
var pdfJsDocument_1 = require("./pdfJsDocument");
var PdfScreenshotHelper = (function () {
    function PdfScreenshotHelper() {
    }
    PdfScreenshotHelper.getLocalPdfData = function (localUrl) {
        return PdfScreenshotHelper.getPdfScreenshotResult(localUrl);
    };
    PdfScreenshotHelper.getPdfData = function (url) {
        return new Promise(function (resolve, reject) {
            var getBinaryEvent = new Log.Event.PromiseEvent(Log.Event.Label.GetBinaryRequest);
            var request = new XMLHttpRequest();
            request.open("GET", url, true);
            request.responseType = "arraybuffer";
            var errorCallback = function (failureInfo) {
                getBinaryEvent.setStatus(Log.Status.Failed);
                getBinaryEvent.setFailureInfo(failureInfo);
                frontEndGlobals_1.Clipper.logger.logEvent(getBinaryEvent);
                reject();
            };
            request.onload = function () {
                if (request.status === 200 && request.response) {
                    var arrayBuffer_1 = request.response;
                    getBinaryEvent.setCustomProperty(Log.PropertyName.Custom.ByteLength, arrayBuffer_1.byteLength);
                    frontEndGlobals_1.Clipper.logger.logEvent(getBinaryEvent);
                    PdfScreenshotHelper.getPdfScreenshotResult(arrayBuffer_1).then(function (pdfScreenshotResult) {
                        pdfScreenshotResult.byteLength = arrayBuffer_1.byteLength;
                        resolve(pdfScreenshotResult);
                    });
                }
                else {
                    errorCallback(OneNoteApi.ErrorUtils.createRequestErrorObject(request, OneNoteApi.RequestErrorType.UNEXPECTED_RESPONSE_STATUS));
                }
            };
            request.ontimeout = function () {
                errorCallback(OneNoteApi.ErrorUtils.createRequestErrorObject(request, OneNoteApi.RequestErrorType.REQUEST_TIMED_OUT));
            };
            request.onerror = function () {
                errorCallback(OneNoteApi.ErrorUtils.createRequestErrorObject(request, OneNoteApi.RequestErrorType.NETWORK_ERROR));
            };
            request.send();
        });
    };
    /**
     * Source can be a buffer object, or a url (including local)
     */
    PdfScreenshotHelper.getPdfScreenshotResult = function (source) {
        // Never rejects, interesting
        return new Promise(function (resolve, reject) {
            PDFJS.getDocument(source).then(function (pdf) {
                var pdfDocument = new pdfJsDocument_1.PdfJsDocument(pdf);
                pdfDocument.getAllPageViewportDimensions().then(function (viewportDimensions) {
                    pdfDocument.getByteLength().then(function (byteLength) {
                        resolve({
                            pdf: pdfDocument,
                            viewportDimensions: viewportDimensions,
                            byteLength: byteLength
                        });
                    });
                });
            });
        });
    };
    return PdfScreenshotHelper;
}());
exports.PdfScreenshotHelper = PdfScreenshotHelper;
