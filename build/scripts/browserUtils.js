"use strict";
var BrowserUtils;
(function (BrowserUtils) {
    function appendHiddenIframeToDocument(url) {
        var iframe = document.createElement("iframe");
        iframe.hidden = true;
        iframe.style.display = "none";
        iframe.src = url;
        document.body.appendChild(iframe);
    }
    BrowserUtils.appendHiddenIframeToDocument = appendHiddenIframeToDocument;
    /**
     * Checks if the browser is unsupported by the Web Clipper, i.e., IE 9 and below.
     *
     * @return true if the the browser is supported by the Web Clipper; false otherwise
     */
    function browserNotSupported() {
        // Does not exist on IE8 and below
        if (!window.addEventListener) {
            return true;
        }
        // IE9
        if (navigator.appVersion.indexOf("MSIE 9") >= 0) {
            return true;
        }
        return false;
    }
    BrowserUtils.browserNotSupported = browserNotSupported;
    /**
     * Assuming the caller is from IE, returns the browser version
     */
    function ieVersion() {
        var ua = window.navigator.userAgent;
        var msieIndex = ua.indexOf("MSIE ");
        if (msieIndex >= 0) {
            // IE 10 or older
            return "IE" + ua.substring(msieIndex + 5, ua.indexOf(".", msieIndex));
        }
        if (ua.indexOf("Trident/") >= 0) {
            // IE 11
            var rvIndex = ua.indexOf("rv:");
            return "IE" + ua.substring(rvIndex + 3, ua.indexOf(".", rvIndex));
        }
        if (ua.indexOf("Edge/") >= 0) {
            // Edge (IE 12+) => return version number
            return "Edge";
        }
        return undefined;
    }
    BrowserUtils.ieVersion = ieVersion;
    function openPopupWindow(url, popupWidth, popupHeight) {
        if (popupWidth === void 0) { popupWidth = 1000; }
        if (popupHeight === void 0) { popupHeight = 700; }
        var leftPosition = (screen.width) ? (screen.width - popupWidth) / 2 : 0;
        var topPosition = (screen.height) ? (screen.height - popupHeight) / 2 : 0;
        var settings = "height=" + popupHeight +
            ",width=" + popupWidth +
            ",top=" + topPosition +
            ",left=" + leftPosition +
            ",scrollbars=yes,resizable=yes,location=no,menubar=no,status=yes,titlebar=no,toolbar=no";
        return window.open(url, "_blank", settings);
    }
    BrowserUtils.openPopupWindow = openPopupWindow;
})(BrowserUtils = exports.BrowserUtils || (exports.BrowserUtils = {}));
