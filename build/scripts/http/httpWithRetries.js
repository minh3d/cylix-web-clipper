"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var http_1 = require("./http");
var objectUtils_1 = require("../objectUtils");
var promiseUtils_1 = require("../promiseUtils");
/**
 * Helper class which extends the Http class in order to allow automatic retries.
 */
var HttpWithRetries = (function (_super) {
    __extends(HttpWithRetries, _super);
    function HttpWithRetries() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    HttpWithRetries.get = function (url, headers, timeout, expectedCodes, retryOptions) {
        var _this = this;
        if (timeout === void 0) { timeout = http_1.Http.defaultTimeout; }
        if (expectedCodes === void 0) { expectedCodes = [200]; }
        var func = function () {
            return _super.createAndSendRequest.call(_this, "GET", url, headers, expectedCodes, timeout);
        };
        return promiseUtils_1.PromiseUtils.execWithRetry(func, retryOptions);
    };
    HttpWithRetries.post = function (url, data, headers, expectedCodes, timeout, retryOptions) {
        var _this = this;
        if (expectedCodes === void 0) { expectedCodes = [200]; }
        if (timeout === void 0) { timeout = http_1.Http.defaultTimeout; }
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(data)) {
            throw new Error("data must be a non-undefined object, but was: " + data);
        }
        var func = function () {
            return _super.createAndSendRequest.call(_this, "POST", url, headers, expectedCodes, timeout, data);
        };
        return promiseUtils_1.PromiseUtils.execWithRetry(func, retryOptions);
    };
    return HttpWithRetries;
}(http_1.Http));
exports.HttpWithRetries = HttpWithRetries;
