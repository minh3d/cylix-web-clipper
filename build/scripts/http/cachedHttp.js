"use strict";
/**
 * Allows the creation of HTTP GET requests to retrieve data, as well as caching of the result.
 */
var CachedHttp = (function () {
    function CachedHttp(cache) {
        this.cache = cache;
    }
    /**
     * Given a key, checks the cache for a fresh copy of that value. If the cached value does
     * not exist, or is not fresh, fetches a fresh copy of the data from the specified endpoint.
     */
    CachedHttp.prototype.getFreshValue = function (key, getRemoteValue, updateInterval) {
        if (!key) {
            throw new Error("key must be a non-empty string, but was: " + key);
        }
        else if (!getRemoteValue) {
            throw new Error("getRemoteValue must be non-undefined");
        }
        else if (updateInterval < 0) {
            updateInterval = 0;
        }
        var value = this.cache.getValue(key);
        var keyIsPresent = !!value;
        if (keyIsPresent) {
            var valueAsJson = void 0;
            try {
                valueAsJson = JSON.parse(value);
            }
            catch (e) {
                return Promise.reject({ error: e });
            }
            var valueHasExpired = CachedHttp.valueHasExpired(valueAsJson, updateInterval);
            if (!valueHasExpired) {
                return Promise.resolve(valueAsJson);
            }
        }
        return this.getAndCacheRemoteValue(key, getRemoteValue);
    };
    CachedHttp.prototype.getAndCacheRemoteValue = function (key, getRemoteValue) {
        var _this = this;
        if (!key) {
            throw new Error("key must be a non-empty string, but was: " + key);
        }
        if (!getRemoteValue) {
            throw new Error("getRemoteValue must be non-undefined");
        }
        return getRemoteValue().then(function (responsePackage) {
            var setValue = _this.setTimeStampedValue(key, responsePackage.parsedResponse);
            if (!setValue) {
                // Fresh data from the remote was unavailable
                _this.cache.removeKey(key);
            }
            return Promise.resolve(setValue);
        })["catch"](function (error) {
            // TODO: Don't use OneNoteApi.RequestError
            _this.cache.removeKey(key);
            return Promise.reject(error);
        });
    };
    /**
     * Returns true if the timestamped data is older than the expiry time; false otherwise.
     */
    CachedHttp.valueHasExpired = function (value, expiryTime) {
        var lastUpdated = value && value.lastUpdated ? value.lastUpdated : 0;
        return (Date.now() - lastUpdated >= expiryTime);
    };
    /*
     * Helper function that stores a value together with a timestamp as a json string. If
     * the specified value is null, undefined, or unable to be jsonified, the value is not
     * stored, and undefined is returned.
     */
    CachedHttp.prototype.setTimeStampedValue = function (key, value) {
        if (!key) {
            throw new Error("key must be a non-empty string, but was: " + key);
        }
        if (!value) {
            return undefined;
        }
        var valueAsJson;
        try {
            valueAsJson = JSON.parse(value);
        }
        catch (e) {
            return undefined;
        }
        var timeStampedValue = { data: valueAsJson, lastUpdated: Date.now() };
        this.cache.setValue(key, JSON.stringify(timeStampedValue));
        return timeStampedValue;
    };
    return CachedHttp;
}());
exports.CachedHttp = CachedHttp;
