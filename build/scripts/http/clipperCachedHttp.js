"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var constants_1 = require("../constants");
var Log = require("../logging/log");
var cachedHttp_1 = require("./cachedHttp");
/**
 * Adds Clipper-specific logging functionality to the CachedHttp.
 */
var ClipperCachedHttp = (function (_super) {
    __extends(ClipperCachedHttp, _super);
    function ClipperCachedHttp(cache, logger) {
        var _this = _super.call(this, cache) || this;
        _this.logger = logger;
        return _this;
    }
    ClipperCachedHttp.prototype.setLogger = function (logger) {
        this.logger = logger;
    };
    ClipperCachedHttp.getDefaultExpiry = function () {
        return ClipperCachedHttp.defaultExpiry;
    };
    // Override
    ClipperCachedHttp.prototype.getFreshValue = function (key, getRemoteValue, updateInterval) {
        if (updateInterval === void 0) { updateInterval = ClipperCachedHttp.defaultExpiry; }
        if (!key) {
            this.logger.logFailure(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected, { error: "getFreshValue key parameter should be passed a non-empty string" }, "" + key);
            return Promise.resolve(undefined);
        }
        else if (!getRemoteValue) {
            this.logger.logFailure(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected, { error: "getFreshValue getRemoteValue parameter should be passed a non-undefined function" }, "" + getRemoteValue);
            return Promise.resolve(undefined);
        }
        else if (updateInterval < 0) {
            this.logger.logFailure(Log.Failure.Label.InvalidArgument, Log.Failure.Type.Unexpected, { error: "getFreshValue updateInterval parameter should be passed a number >= 0" }, "" + updateInterval);
            updateInterval = 0;
        }
        var loggedGetRemoteValue = this.addLoggingToGetResponseAsync(key, getRemoteValue, updateInterval);
        return _super.prototype.getFreshValue.call(this, key, loggedGetRemoteValue, updateInterval);
    };
    ClipperCachedHttp.prototype.addLoggingToGetResponseAsync = function (key, getResponseAsync, updateInterval) {
        var _this = this;
        return function () {
            // Need to download a fresh copy of the code.
            var fetchNonLocalDataEvent = new Log.Event.PromiseEvent(Log.Event.Label.FetchNonLocalData);
            fetchNonLocalDataEvent.setCustomProperty(Log.PropertyName.Custom.Key, key);
            fetchNonLocalDataEvent.setCustomProperty(Log.PropertyName.Custom.UpdateInterval, updateInterval);
            return new Promise(function (resolve, reject) {
                getResponseAsync().then(function (responsePackage) {
                    if (responsePackage.request) {
                        ClipperCachedHttp.addCorrelationIdToLogEvent(fetchNonLocalDataEvent, responsePackage.request);
                    }
                    resolve(responsePackage);
                }, function (error) {
                    fetchNonLocalDataEvent.setStatus(Log.Status.Failed);
                    fetchNonLocalDataEvent.setFailureInfo(error);
                    reject(error);
                }).then(function () {
                    _this.logger.logEvent(fetchNonLocalDataEvent);
                });
            });
        };
    };
    ClipperCachedHttp.addCorrelationIdToLogEvent = function (logEvent, request) {
        var correlationId = request.getResponseHeader(constants_1.Constants.HeaderValues.correlationId);
        if (correlationId) {
            logEvent.setCustomProperty(Log.PropertyName.Custom.CorrelationId, correlationId);
        }
    };
    return ClipperCachedHttp;
}(cachedHttp_1.CachedHttp));
ClipperCachedHttp.defaultExpiry = 12 * 60 * 60 * 1000; // 12 hours
exports.ClipperCachedHttp = ClipperCachedHttp;
