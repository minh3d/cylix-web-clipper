"use strict";
var objectUtils_1 = require("../objectUtils");
/**
 * Helper class for performing http requests. For each of the http methods, resolve(request) is only
 * called if the status code is an unexpected one, defined by the caller (defaulting to 200 only).
 *
 * TODO: Wean this off OneNoteApi.ErrorUtils once we move the general http logic into its own package.
 */
var Http = (function () {
    function Http() {
    }
    Http.get = function (url, headers, timeout, expectedCodes) {
        if (timeout === void 0) { timeout = Http.defaultTimeout; }
        if (expectedCodes === void 0) { expectedCodes = [200]; }
        return Http.createAndSendRequest("GET", url, headers, expectedCodes, timeout);
    };
    Http.post = function (url, data, headers, expectedCodes, timeout) {
        if (expectedCodes === void 0) { expectedCodes = [200]; }
        if (timeout === void 0) { timeout = Http.defaultTimeout; }
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(data)) {
            throw new Error("data must be a non-undefined object, but was: " + data);
        }
        return Http.createAndSendRequest("POST", url, headers, expectedCodes, timeout, data);
    };
    Http.createAndSendRequest = function (method, url, headers, expectedCodes, timeout, data) {
        if (expectedCodes === void 0) { expectedCodes = [200]; }
        if (timeout === void 0) { timeout = Http.defaultTimeout; }
        if (!url) {
            throw new Error("url must be a non-empty string, but was: " + url);
        }
        return new Promise(function (resolve, reject) {
            var request = new XMLHttpRequest();
            request.open(method, url);
            request.onload = function () {
                if (expectedCodes.indexOf(request.status) > -1) {
                    resolve(request);
                }
                else {
                    reject(OneNoteApi.ErrorUtils.createRequestErrorObject(request, OneNoteApi.RequestErrorType.UNEXPECTED_RESPONSE_STATUS));
                }
            };
            request.onerror = function () {
                reject(OneNoteApi.ErrorUtils.createRequestErrorObject(request, OneNoteApi.RequestErrorType.NETWORK_ERROR));
            };
            request.ontimeout = function () {
                reject(OneNoteApi.ErrorUtils.createRequestErrorObject(request, OneNoteApi.RequestErrorType.REQUEST_TIMED_OUT));
            };
            Http.setHeaders(request, headers);
            request.timeout = timeout;
            request.send(data);
        });
    };
    Http.setHeaders = function (request, headers) {
        if (headers) {
            for (var key in headers) {
                request.setRequestHeader(key, headers[key]);
            }
        }
    };
    return Http;
}());
Http.defaultTimeout = 30000;
exports.Http = Http;
