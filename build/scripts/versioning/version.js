"use strict";
var Version = (function () {
    function Version(version) {
        if (!version || !/^\d+\.\d+\.\d+$/.test(version)) {
            throw new Error("version must match 'int.int.int' pattern, but was: " + version);
        }
        var parts = version.split(".");
        this.major = parseInt(parts[0], 10);
        this.minor = parseInt(parts[1], 10);
        this.patch = parseInt(parts[2], 10);
        this.stringRepresentation = this.major + "." + this.minor + "." + this.patch;
    }
    Version.prototype.isEqualTo = function (other, ignorePatchUpdate) {
        return this.major === other.major && this.minor === other.minor && (ignorePatchUpdate || this.patch === other.patch);
    };
    Version.prototype.isGreaterThan = function (other, ignorePatchUpdate) {
        if (this.major !== other.major) {
            return this.major > other.major;
        }
        if (this.minor !== other.minor) {
            return this.minor > other.minor;
        }
        return ignorePatchUpdate ? false : this.patch > other.patch;
    };
    Version.prototype.isGreaterThanOrEqualTo = function (other, ignorePatchUpdate) {
        return this.isEqualTo(other, ignorePatchUpdate) || this.isGreaterThan(other, ignorePatchUpdate);
    };
    Version.prototype.isLesserThan = function (other, ignorePatchUpdate) {
        if (this.major !== other.major) {
            return this.major < other.major;
        }
        if (this.minor !== other.minor) {
            return this.minor < other.minor;
        }
        return ignorePatchUpdate ? false : this.patch < other.patch;
    };
    Version.prototype.isLesserThanOrEqualTo = function (other, ignorePatchUpdate) {
        return this.isEqualTo(other, ignorePatchUpdate) || this.isLesserThan(other, ignorePatchUpdate);
    };
    Version.prototype.toString = function () {
        return this.stringRepresentation;
    };
    return Version;
}());
exports.Version = Version;
