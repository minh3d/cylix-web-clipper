"use strict";
var version_1 = require("./version");
var ChangeLogHelper;
(function (ChangeLogHelper) {
    /**
     * Given a list of updates sorted in descending order, returns the updates that are more recent
     * than the specified verison. If the version is not specified (i.e., undefined), all updates
     * are returned.
     */
    function getUpdatesSinceVersion(updates, version) {
        if (!updates || updates.length === 0) {
            return [];
        }
        if (!version) {
            return updates;
        }
        for (var i = 0; i < updates.length; i++) {
            var updateVersion = new version_1.Version(updates[i].version);
            if (version.isGreaterThanOrEqualTo(updateVersion)) {
                return updates.slice(0, i);
            }
        }
        return updates;
    }
    ChangeLogHelper.getUpdatesSinceVersion = getUpdatesSinceVersion;
    /**
     * Given a list of updates sorted in descending order, returns the updates between a lower (exclusive)
     * and upper (inclusive) bound version. If the lower bound is not specified (i.e., undefined), all updates
     * before the upper bound are returned.
     */
    function getUpdatesBetweenVersions(updates, lowerVersion, higherVersion) {
        if (!updates || updates.length === 0) {
            return [];
        }
        var betweenUpdates = [];
        for (var i = 0; i < updates.length; i++) {
            var updateVersion = new version_1.Version(updates[i].version);
            if (lowerVersion && updateVersion.isLesserThanOrEqualTo(lowerVersion)) {
                break;
            }
            if (updateVersion.isLesserThanOrEqualTo(higherVersion)) {
                betweenUpdates.push(updates[i]);
            }
        }
        return betweenUpdates;
    }
    ChangeLogHelper.getUpdatesBetweenVersions = getUpdatesBetweenVersions;
    /**
     * Given a list of updates, returns the updates that contain at least one change that applies to the given
     * browser type (i.e., filtering out updates that do not apply to the specified browser).
     */
    function filterUpdatesThatDontApplyToBrowser(updates, browser) {
        if (!updates || !browser) {
            return [];
        }
        var filteredUpdates = [];
        for (var i = 0; i < updates.length; i++) {
            var update = updates[i];
            var filteredChanges = update.changes.filter(function (change) { return change.supportedBrowsers.indexOf(browser) !== -1; });
            // We are only interested in an update if it had at least one change relevant to the browser
            if (filteredChanges.length > 0) {
                // Do this to keep the function pure
                filteredUpdates.push({
                    version: update.version,
                    date: update.date,
                    changes: filteredChanges
                });
            }
        }
        return filteredUpdates;
    }
    ChangeLogHelper.filterUpdatesThatDontApplyToBrowser = filterUpdatesThatDontApplyToBrowser;
})(ChangeLogHelper = exports.ChangeLogHelper || (exports.ChangeLogHelper = {}));
