"use strict";
var PromiseUtils;
(function (PromiseUtils) {
    /**
     * Returns a promise that simply resolves after the specified time period
     */
    function wait(millieseconds) {
        return new Promise(function (resolve) {
            setTimeout(function () {
                resolve();
            }, millieseconds);
        });
    }
    PromiseUtils.wait = wait;
    /**
     * Executes the given function and retries on failure.
     */
    function execWithRetry(func, retryOptions) {
        if (retryOptions === void 0) { retryOptions = { retryCount: 3, retryWaitTimeInMs: 3000 }; }
        return func()["catch"](function (error1) {
            if (retryOptions.retryCount > 0) {
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        retryOptions.retryCount--;
                        execWithRetry(func, retryOptions).then(function (response) {
                            resolve(response);
                        })["catch"](function (error2) {
                            reject(error2);
                        });
                    }, retryOptions.retryWaitTimeInMs);
                });
            }
            else {
                return Promise.reject(error1);
            }
        });
    }
    PromiseUtils.execWithRetry = execWithRetry;
})(PromiseUtils = exports.PromiseUtils || (exports.PromiseUtils = {}));
