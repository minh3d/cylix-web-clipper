"use strict";
/**
 * Ensures that there's only one TextHighlighter in existence. Calls to
 * replaceWithInstance will tear down the previous instance, then constructs
 * a new TextHighlighter.
 */
var Highlighter = (function () {
    function Highlighter() {
    }
    /**
     * Tears down the old TextHighlighter instance and creates a new one
     */
    Highlighter.reconstructInstance = function (element, options) {
        Highlighter.tearDownCurrentInstance();
        Highlighter.instance = new TextHighlighter(element, options);
        return Highlighter.instance;
    };
    Highlighter.tearDownCurrentInstance = function () {
        if (Highlighter.instance) {
            Highlighter.instance.disable();
        }
    };
    return Highlighter;
}());
exports.Highlighter = Highlighter;
