"use strict";
var AuthType;
(function (AuthType) {
    AuthType[AuthType["Msa"] = 0] = "Msa";
    AuthType[AuthType["OrgId"] = 1] = "OrgId";
})(AuthType = exports.AuthType || (exports.AuthType = {}));
var UpdateReason;
(function (UpdateReason) {
    UpdateReason[UpdateReason["InitialRetrieval"] = 0] = "InitialRetrieval";
    UpdateReason[UpdateReason["SignInAttempt"] = 1] = "SignInAttempt";
    UpdateReason[UpdateReason["SignInCancel"] = 2] = "SignInCancel";
    UpdateReason[UpdateReason["SignOutAction"] = 3] = "SignOutAction";
    UpdateReason[UpdateReason["TokenRefreshForPendingClip"] = 4] = "TokenRefreshForPendingClip";
})(UpdateReason = exports.UpdateReason || (exports.UpdateReason = {}));
