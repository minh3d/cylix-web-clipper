"use strict";
// Load up the settings file
var settings = require("../settings.json");
var Settings;
(function (Settings) {
    function getSetting(name) {
        var localResult = settings[name];
        if (!localResult || !localResult.Value) {
            return undefined;
        }
        return localResult.Value;
    }
    Settings.getSetting = getSetting;
    function setSettingsJsonForTesting(jsonObject) {
        if (jsonObject) {
            settings = jsonObject;
        }
        else {
            // revert to default
            settings = require("../settings.json");
        }
    }
    Settings.setSettingsJsonForTesting = setSettingsJsonForTesting;
})(Settings = exports.Settings || (exports.Settings = {}));
