"use strict";
var objectUtils_1 = require("./objectUtils");
var settings_1 = require("./settings");
var tooltipType_1 = require("./clipperUI/tooltipType");
var UrlUtils;
(function (UrlUtils) {
    function checkIfUrlMatchesAContentType(url, tooltipTypes) {
        for (var i = 0; i < tooltipTypes.length; ++i) {
            var tooltipType = tooltipTypes[i];
            var contentTypeAsString = tooltipType_1.TooltipType[tooltipType];
            var contentTypeRegexes = settings_1.Settings.getSetting(contentTypeAsString + "Domains");
            var concatenatedRegExes = new RegExp(contentTypeRegexes.join("|"), "i");
            if (concatenatedRegExes.test(url)) {
                return tooltipType;
            }
        }
        return;
    }
    UrlUtils.checkIfUrlMatchesAContentType = checkIfUrlMatchesAContentType;
    function getFileNameFromUrl(url, fallbackResult) {
        if (!url) {
            return fallbackResult;
        }
        var regexResult = /\/(?=[^\/]+\.\w{3,4}$).+/g.exec(url);
        return regexResult && regexResult[0] ? regexResult[0].slice(1) : fallbackResult;
    }
    UrlUtils.getFileNameFromUrl = getFileNameFromUrl;
    function getHostname(url) {
        var l = document.createElement("a");
        l.href = url;
        return l.protocol + "//" + l.host + "/";
    }
    UrlUtils.getHostname = getHostname;
    function getPathname(url) {
        var l = document.createElement("a");
        l.href = url;
        var urlPathName = l.pathname;
        // We need to ensure the leading forward slash to make it consistant across all browsers.
        return ensureLeadingForwardSlash(urlPathName);
    }
    UrlUtils.getPathname = getPathname;
    function ensureLeadingForwardSlash(url) {
        url = objectUtils_1.ObjectUtils.isNullOrUndefined(url) ? "/" : url;
        return (url.length > 0 && url.charAt(0) === "/") ? url : "/" + url;
    }
    /**
     * Gets the query value of the given url and key.
     *
     * @param url The URL to get the query value from
     * @param key The query key in the URL to get the query value from
     * @return Undefined if the key does not exist; "" if the key exists but has no matching
     * value; otherwise the query value
     */
    function getQueryValue(url, key) {
        if (!url || !key) {
            return undefined;
        }
        var escapedKey = key.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + escapedKey + "(=([^&#]*)|&|#|$)", "i");
        var results = regex.exec(url);
        if (!results) {
            return undefined;
        }
        if (!results[2]) {
            return "";
        }
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
    UrlUtils.getQueryValue = getQueryValue;
    /**
     * Add a name/value pair to the query string of a URL.
     * If the name already exists, simply replace the value (there is no existing standard
     * for the usage of multiple identical names in the same URL, so we assume adding a
     * duplicate name is unintended).
     *
     * @param originalUrl The URL to add the name/value to
     * @param name New value name
     * @param value New value
     * @return Resulting URL
     */
    function addUrlQueryValue(originalUrl, key, value, keyToCamelCase) {
        if (keyToCamelCase === void 0) { keyToCamelCase = false; }
        if (!originalUrl || !key || !value) {
            return originalUrl;
        }
        if (keyToCamelCase) {
            key = key.charAt(0).toUpperCase() + key.slice(1);
        }
        // The fragment refers to the last part of the url consisting of "#" and everything after it
        var regexResult = originalUrl.match(/^([^#]*)(#.*)?$/);
        var beforeFragment = regexResult[1];
        var fragment = regexResult[2] ? regexResult[2] : "";
        var queryStartIndex = beforeFragment.indexOf("?");
        if (queryStartIndex === -1) {
            // There is no query string, so create a new one
            return beforeFragment + "?" + key + "=" + value + fragment;
        }
        else if (queryStartIndex === beforeFragment.length - 1) {
            // Sometimes for some reason there are no name/value pairs specified after the '?'
            return beforeFragment + key + "=" + value + fragment;
        }
        else {
            // Check that name does not already exist
            var pairs = beforeFragment.substring(queryStartIndex + 1).split("&");
            for (var i = 0; i < pairs.length; i++) {
                var splitPair = pairs[i].split("=");
                if (splitPair[0] === key) {
                    // Replace the value
                    pairs[i] = splitPair[0] + "=" + value;
                    return beforeFragment.substring(0, queryStartIndex + 1) + pairs.join("&") + fragment;
                }
            }
            // No existing name found
            return beforeFragment + "&" + key + "=" + value + fragment;
        }
    }
    UrlUtils.addUrlQueryValue = addUrlQueryValue;
    function onBlacklistedDomain(url) {
        return urlMatchesRegexInSettings(url, ["PageNav_BlacklistedDomains"]);
    }
    UrlUtils.onBlacklistedDomain = onBlacklistedDomain;
    function onWhitelistedDomain(url) {
        return urlMatchesRegexInSettings(url, ["AugmentationDefault_WhitelistedDomains", "ProductDomains", "RecipeDomains"]);
    }
    UrlUtils.onWhitelistedDomain = onWhitelistedDomain;
    function urlMatchesRegexInSettings(url, settingNames) {
        if (!url) {
            return false;
        }
        var domains = [];
        settingNames.forEach(function (settingName) {
            domains = domains.concat(settings_1.Settings.getSetting(settingName));
        });
        for (var _i = 0, domains_1 = domains; _i < domains_1.length; _i++) {
            var identifier = domains_1[_i];
            if (new RegExp(identifier).test(url)) {
                return true;
            }
        }
        return false;
    }
})(UrlUtils = exports.UrlUtils || (exports.UrlUtils = {}));
