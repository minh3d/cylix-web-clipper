"use strict";
var clipperStorageKeys_1 = require("./clipperStorageKeys");
var ClipperStorageGateStrategy = (function () {
    function ClipperStorageGateStrategy(storage) {
        this.keysThatRequireUserInfo = [
            clipperStorageKeys_1.ClipperStorageKeys.cachedNotebooks,
            clipperStorageKeys_1.ClipperStorageKeys.currentSelectedSection
        ];
        this.storage = storage;
    }
    ClipperStorageGateStrategy.prototype.shouldSet = function (key, value) {
        // An undefined value is the same thing as removing a key, so we allow it regardless
        if (value && this.keysThatRequireUserInfo.indexOf(key) > -1) {
            var userInfo = this.storage.getValue(clipperStorageKeys_1.ClipperStorageKeys.userInformation);
            return !!userInfo;
        }
        return true;
    };
    return ClipperStorageGateStrategy;
}());
exports.ClipperStorageGateStrategy = ClipperStorageGateStrategy;
