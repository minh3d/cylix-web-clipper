"use strict";
var constants_1 = require("../constants");
var RemoteStorage = (function () {
    function RemoteStorage(extCommunicator) {
        this.storageCache = {};
        this.extensionCommunicator = extCommunicator;
    }
    RemoteStorage.prototype.getCachedValue = function (key) {
        return this.storageCache[key];
    };
    RemoteStorage.prototype.getValue = function (key, callback, cacheValue) {
        var _this = this;
        this.extensionCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.getStorageValue, {
            param: key, callback: function (value) {
                if (cacheValue) {
                    _this.storageCache[key] = value;
                }
                callback(value);
            }
        });
    };
    RemoteStorage.prototype.getValues = function (keys, callback, cacheValue) {
        var _this = this;
        this.extensionCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.getMultipleStorageValues, { param: keys, callback: function (values) {
                if (cacheValue) {
                    for (var key in values) {
                        if (values.hasOwnProperty(key)) {
                            _this.storageCache[key] = values[key];
                        }
                    }
                }
                callback(values);
            } });
    };
    RemoteStorage.prototype.setValue = function (key, value, callback) {
        if (key in this.storageCache) {
            this.storageCache[key] = value;
        }
        this.extensionCommunicator.callRemoteFunction(constants_1.Constants.FunctionKeys.setStorageValue, {
            param: { key: key, value: value }, callback: function (retVal) {
                if (callback) {
                    callback(retVal);
                }
            }
        });
    };
    return RemoteStorage;
}());
exports.RemoteStorage = RemoteStorage;
