"use strict";
var LocalStorage = (function () {
    function LocalStorage() {
    }
    LocalStorage.prototype.getValue = function (key) {
        var result;
        if (window.localStorage) {
            result = window.localStorage.getItem(key);
            if (!result) {
                // Somehow we stored an invalid value. Destroy it!
                this.removeKey(key);
            }
        }
        return result;
    };
    LocalStorage.prototype.getValues = function (keys) {
        var values = {};
        if (window.localStorage && keys) {
            for (var i = 0; i < keys.length; i++) {
                var result = window.localStorage.getItem(keys[i]);
                if (!result) {
                    // Somehow we stored an invalid value. Destroy it!
                    this.removeKey(keys[i]);
                }
                else {
                    values[keys[i]] = result;
                }
            }
        }
        return values;
    };
    LocalStorage.prototype.setValue = function (key, value) {
        if (window.localStorage) {
            if (!value) {
                window.localStorage.removeItem(key);
            }
            else {
                window.localStorage.setItem(key, value);
            }
        }
    };
    LocalStorage.prototype.removeKey = function (key) {
        if (window.localStorage) {
            window.localStorage.removeItem(key);
        }
        return true;
    };
    return LocalStorage;
}());
exports.LocalStorage = LocalStorage;
