"use strict";
var clipperCachedHttp_1 = require("../http/clipperCachedHttp");
var clipperStorageGateStrategy_1 = require("./clipperStorageGateStrategy");
/**
 * The data-access-like object that handles the fetching and caching of data
 * from Clipper-specific endpoints (TODO: a factory class that is responsible for
 * providing getRemoteValue objects?). Also provides a gating mechanism as a
 * sanity-check for what information gets stored locally.
 */
var ClipperData = (function () {
    function ClipperData(storage, logger) {
        this.storage = storage;
        this.storageGateStrategy = new clipperStorageGateStrategy_1.ClipperStorageGateStrategy(storage);
        // We pass 'this' as the Storage object as it handles all the sanity-check gating in the setValue
        this.cachedHttp = new clipperCachedHttp_1.ClipperCachedHttp(this, logger);
    }
    ClipperData.prototype.setLogger = function (logger) {
        this.cachedHttp.setLogger(logger);
    };
    ClipperData.prototype.getFreshValue = function (key, getRemoteValue, updateInterval) {
        return this.cachedHttp.getFreshValue(key, getRemoteValue, updateInterval);
    };
    ClipperData.prototype.getValue = function (key) {
        return this.storage.getValue(key);
    };
    ClipperData.prototype.getValues = function (keys) {
        return this.storage.getValues(keys);
    };
    ClipperData.prototype.setValue = function (key, value) {
        if (this.storageGateStrategy.shouldSet(key, value)) {
            this.storage.setValue(key, value);
        }
    };
    ClipperData.prototype.removeKey = function (key) {
        return this.storage.removeKey(key);
    };
    return ClipperData;
}());
exports.ClipperData = ClipperData;
