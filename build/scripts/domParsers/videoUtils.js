"use strict";
var objectUtils_1 = require("../objectUtils");
var urlUtils_1 = require("../urlUtils");
var SupportedVideoDomains;
(function (SupportedVideoDomains) {
    SupportedVideoDomains[SupportedVideoDomains["YouTube"] = 0] = "YouTube";
    SupportedVideoDomains[SupportedVideoDomains["Vimeo"] = 1] = "Vimeo";
    SupportedVideoDomains[SupportedVideoDomains["KhanAcademy"] = 2] = "KhanAcademy";
})(SupportedVideoDomains = exports.SupportedVideoDomains || (exports.SupportedVideoDomains = {}));
var VideoUtils = (function () {
    function VideoUtils() {
        this.youTubeWatchVideoBaseUrl = "https://www.youtube.com/watch";
        this.youTubeVideoIdQueryKey = "v";
    }
    /**
     * Returns a string from the SupportedVideoDomains enum iff
     * the pageUrl's hostname contains the enum string
     */
    VideoUtils.videoDomainIfSupported = function (pageUrl) {
        if (!pageUrl) {
            return;
        }
        var pageUrlAsLowerCase = pageUrl.toLowerCase();
        if (!urlUtils_1.UrlUtils.onWhitelistedDomain(pageUrlAsLowerCase)) {
            return;
        }
        var hostname = urlUtils_1.UrlUtils.getHostname(pageUrlAsLowerCase).toLowerCase();
        for (var domainEnum in SupportedVideoDomains) {
            var domain = SupportedVideoDomains[domainEnum];
            if (typeof (domain) === "string" && hostname.indexOf(domain.toLowerCase() + ".") > -1) {
                return domain;
            }
        }
        return;
    };
    VideoUtils.matchRegexFromPageContent = function (pageContent, regexes) {
        if (objectUtils_1.ObjectUtils.isNullOrUndefined(pageContent)) {
            return;
        }
        var match;
        var matches = [];
        regexes.forEach(function (regex) {
            // Calling exec multiple times with the same parameter will continue finding matches until
            // there are no more
            while (match = regex.exec(pageContent)) {
                if (match[2]) {
                    matches.push(match[2]);
                }
                else if (match[3]) {
                    matches.push(match[3]);
                }
            }
        });
        if (matches.length === 0) {
            return;
        }
        // We used to return unique values, but since we now support videos in selections, we moved the
        // uniqueness logic to VideoExtractor::createEmbeddedVideosFromPage
        return matches;
    };
    return VideoUtils;
}());
exports.VideoUtils = VideoUtils;
