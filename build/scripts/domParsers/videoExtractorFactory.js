"use strict";
var khanAcademyVideoExtractor_1 = require("./khanAcademyVideoExtractor");
var videoUtils_1 = require("./videoUtils");
var vimeoVideoExtractor_1 = require("./vimeoVideoExtractor");
var YoutubeVideoExtractor_1 = require("./YoutubeVideoExtractor");
/**
 * Factory class to return a domain specific video extractor given a Domain
 * The video extractor examines pageInfo and returns data about the videos on the page
 * to be used in the preview and posting to OneNote
 */
var VideoExtractorFactory;
(function (VideoExtractorFactory) {
    function createVideoExtractor(domain) {
        // shorter typename
        var domains = videoUtils_1.SupportedVideoDomains;
        switch (domain) {
            case domains.KhanAcademy:
                return new khanAcademyVideoExtractor_1.KhanAcademyVideoExtractor();
            case domains.Vimeo:
                return new vimeoVideoExtractor_1.VimeoVideoExtractor();
            case domains.YouTube:
                return new YoutubeVideoExtractor_1.YoutubeVideoExtractor();
            default:
                return;
        }
    }
    VideoExtractorFactory.createVideoExtractor = createVideoExtractor;
})(VideoExtractorFactory = exports.VideoExtractorFactory || (exports.VideoExtractorFactory = {}));
