"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var domUtils_1 = require("./domUtils");
var videoExtractor_1 = require("./videoExtractor");
var urlUtils_1 = require("../urlUtils");
var YoutubeVideoExtractor = (function (_super) {
    __extends(YoutubeVideoExtractor, _super);
    function YoutubeVideoExtractor() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.youTubeWatchVideoBaseUrl = "https://www.youtube.com/watch";
        _this.youTubeVideoIdQueryKey = "v";
        _this.dataOriginalSrcAttribute = "data-original-src";
        return _this;
    }
    YoutubeVideoExtractor.prototype.createEmbeddedVideosFromHtml = function (html) {
        if (!html) {
            return [];
        }
        var divContainer = document.createElement("div");
        divContainer.innerHTML = html;
        var allIframes = divContainer.getElementsByTagName("iframe");
        var videoEmbeds = [];
        for (var i = 0; i < allIframes.length; i++) {
            if (this.isYoutubeUrl(allIframes[i].src)) {
                var videoEmbed = this.createEmbeddedVideoFromUrl(allIframes[i].src);
                if (videoEmbed) {
                    videoEmbeds.push(videoEmbed);
                }
            }
        }
        return videoEmbeds;
    };
    YoutubeVideoExtractor.prototype.isYoutubeUrl = function (url) {
        return /[^\w]youtube\.com\/watch(\?v=(\w+)|.*\&v=(\w+))/.test(url) || /[^\w]youtube\.com\/embed\/(\w+)/.test(url);
    };
    YoutubeVideoExtractor.prototype.createEmbeddedVideoFromUrl = function (url) {
        if (!url) {
            return undefined;
        }
        if (urlUtils_1.UrlUtils.getPathname(url).indexOf("/watch") === 0) {
            return this.createEmbeddedVideoFromId(urlUtils_1.UrlUtils.getQueryValue(url, this.youTubeVideoIdQueryKey));
        }
        if (urlUtils_1.UrlUtils.getPathname(url).indexOf("/embed") === 0) {
            var youTubeIdMatch = url.match(/youtube\.com\/embed\/([^?|\/?]+)/);
            return this.createEmbeddedVideoFromId(youTubeIdMatch[1]);
        }
        return undefined;
    };
    YoutubeVideoExtractor.prototype.createEmbeddedVideoFromId = function (id) {
        if (!id) {
            return undefined;
        }
        var videoEmbed = domUtils_1.DomUtils.createEmbedVideoIframe();
        var src = "https://www.youtube.com/embed/" + id;
        videoEmbed.src = src;
        var dataOriginalSrc = urlUtils_1.UrlUtils.addUrlQueryValue(this.youTubeWatchVideoBaseUrl, this.youTubeVideoIdQueryKey, id);
        videoEmbed.setAttribute(this.dataOriginalSrcAttribute, dataOriginalSrc);
        return videoEmbed;
    };
    return YoutubeVideoExtractor;
}(videoExtractor_1.VideoExtractor));
exports.YoutubeVideoExtractor = YoutubeVideoExtractor;
