"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var videoExtractor_1 = require("./videoExtractor");
var videoUtils_1 = require("./videoUtils");
var youtubeVideoExtractor_1 = require("./youtubeVideoExtractor");
var KhanAcademyVideoExtractor = (function (_super) {
    __extends(KhanAcademyVideoExtractor, _super);
    function KhanAcademyVideoExtractor() {
        var _this = _super.call(this) || this;
        // KhanAcademy hosts their videos on Youtube
        _this.youtubeExtractor = new youtubeVideoExtractor_1.YoutubeVideoExtractor();
        return _this;
    }
    KhanAcademyVideoExtractor.prototype.createEmbeddedVideosFromHtml = function (html) {
        var _this = this;
        if (!html) {
            return [];
        }
        // Matches strings of the form id="video_\S+" OR id='video_\S+'
        // with any amount of whitespace padding in between strings of interest
        var regex1 = /\sid\s*=\s*("\s*video_(\S+)\s*"|'\s*video_(\S+)\s*')/gi;
        // Matches strings of the form data-youtubeid="\S+" OR data-youtubeid='\S+'
        // with any amount of whitespace padding in between strings of interest
        var regex2 = /data-youtubeid\s*=\s*("\s*video_(\S+)\s*"|'\s*video_(\S+)\s*')/gi;
        var ids = videoUtils_1.VideoUtils.matchRegexFromPageContent(html, [regex1, regex2]);
        if (!ids) {
            return [];
        }
        return ids.map(function (id) { return _this.createEmbeddedVideoFromId(id); });
    };
    KhanAcademyVideoExtractor.prototype.createEmbeddedVideoFromUrl = function (url) {
        // KhanAcademy does not host their own videos. We can only derive videos
        // from their page's html.
        return undefined;
    };
    KhanAcademyVideoExtractor.prototype.createEmbeddedVideoFromId = function (id) {
        return this.youtubeExtractor.createEmbeddedVideoFromId(id);
    };
    return KhanAcademyVideoExtractor;
}(videoExtractor_1.VideoExtractor));
exports.KhanAcademyVideoExtractor = KhanAcademyVideoExtractor;
