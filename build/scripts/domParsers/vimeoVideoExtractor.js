"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var domUtils_1 = require("./domUtils");
var videoExtractor_1 = require("./videoExtractor");
var videoUtils_1 = require("./videoUtils");
var VimeoVideoExtractor = (function (_super) {
    __extends(VimeoVideoExtractor, _super);
    function VimeoVideoExtractor() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.dataOriginalSrcAttribute = "data-original-src";
        return _this;
    }
    VimeoVideoExtractor.prototype.createEmbeddedVideosFromHtml = function (html) {
        var _this = this;
        if (!html) {
            return [];
        }
        // Looking for all matches in pageContent of the general format: id="clip_###"
        // - where ### could be any number of digits
        // - ignore casing
        // - ignore possible whitespacing variations between characters
        // - accept the use of either double- or single-quotes around clip_###
        var regex1 = /id\s*=\s*("\s*clip_(\d+)\s*"|'\s*clip_(\d+)\s*')/gi;
        // Also account for embedded Vimeo videos
        var regex2 = /player\.vimeo\.com\/video\/((\d+))\d{0}/gi;
        var ids = videoUtils_1.VideoUtils.matchRegexFromPageContent(html, [regex1, regex2]);
        if (!ids) {
            return [];
        }
        return ids.map(function (id) { return _this.createEmbeddedVideoFromId(id); });
    };
    VimeoVideoExtractor.prototype.createEmbeddedVideoFromUrl = function (url) {
        if (!url) {
            return undefined;
        }
        var match = url.match(/^https?:\/\/vimeo\.com\/(\d+)\d{0}/);
        if (match) {
            return this.createEmbeddedVideoFromId(match[1]);
        }
        match = url.match(/^https?:\/\/player.vimeo.com\/video\/(\d+)\d{0}/);
        if (match) {
            return this.createEmbeddedVideoFromId(match[1]);
        }
        return undefined;
    };
    VimeoVideoExtractor.prototype.createEmbeddedVideoFromId = function (id) {
        if (!id) {
            return undefined;
        }
        var videoEmbed = domUtils_1.DomUtils.createEmbedVideoIframe();
        var src = "https://player.vimeo.com/video/" + id;
        videoEmbed.src = src;
        videoEmbed.setAttribute(this.dataOriginalSrcAttribute, src);
        return videoEmbed;
    };
    return VimeoVideoExtractor;
}(videoExtractor_1.VideoExtractor));
exports.VimeoVideoExtractor = VimeoVideoExtractor;
