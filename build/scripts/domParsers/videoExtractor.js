"use strict";
var _ = require("lodash");
var VideoExtractor = (function () {
    function VideoExtractor() {
    }
    /**
     * Given a page's url and its body html, return a unique list of
     * OneNoteApi-compliant video embeds. If there's a video associated
     * with the page url, it will be first in the list.
     */
    VideoExtractor.prototype.createEmbeddedVideosFromPage = function (url, html) {
        var allVideoEmbeds = [];
        var videoEmbedFromUrl = this.createEmbeddedVideoFromUrl(url);
        if (videoEmbedFromUrl) {
            allVideoEmbeds.push(videoEmbedFromUrl);
        }
        allVideoEmbeds = allVideoEmbeds.concat(this.createEmbeddedVideosFromHtml(html));
        return _.uniqWith(allVideoEmbeds, function (v1, v2) {
            return v1.src === v2.src;
        });
    };
    return VideoExtractor;
}());
exports.VideoExtractor = VideoExtractor;
;
