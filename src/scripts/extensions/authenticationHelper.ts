import {SmartValue} from "../communicator/smartValue";

import * as Log from "../logging/log";
import {Logger} from "../logging/logger";

import {CachedHttp, TimeStampedData} from "../http/cachedHttp";
import {HttpWithRetries} from "../http/HttpWithRetries";

import {ClipperData} from "../storage/clipperData";
import {ClipperStorageKeys} from "../storage/clipperStorageKeys";

import {AuthType, UserInfo, UpdateReason} from "../userInfo";
import {Constants} from "../constants";
import {ObjectUtils} from "../objectUtils";
import {ResponsePackage} from "../responsePackage";
import {StringUtils} from "../stringUtils";
import {UserInfoData} from "../userInfo";
import {UrlUtils} from "../urlUtils";

declare var browser;

export class AuthenticationHelper {
	public user: SmartValue<UserInfo>;
	private logger: Logger;
	private clipperData: ClipperData;

	constructor(clipperData: ClipperData, logger: Logger) {
		this.user = new SmartValue<UserInfo>();
		this.logger = logger;
		this.clipperData = clipperData;
	}

	/**
	 * Updates the current user's information.
	 */
	public updateUserInfoData(clipperId: string, updateReason: UpdateReason): Promise<UserInfo> {
		return new Promise<UserInfo>((resolve) => {
			let updateInterval = 0;

			let storedUserInformation = this.clipperData.getValue(ClipperStorageKeys.userInformation);
			if (storedUserInformation) {
				let currentInfo: any;
				try {
					currentInfo = JSON.parse(storedUserInformation);
				} catch (e) {
					this.logger.logJsonParseUnexpected(storedUserInformation);
				}

				if (currentInfo && currentInfo.data && ObjectUtils.isNumeric(currentInfo.data.accessTokenExpiration)) {
					// Expiration is in seconds, not milliseconds. Give additional leniency to account for response time.
					updateInterval = Math.max((currentInfo.data.accessTokenExpiration * 1000) - 180000, 0);
				}
			}

			let getUserInformationFunction = () => {
				return this.retrieveUserInformation(clipperId, undefined);
			};

			let getInfoEvent: Log.Event.PromiseEvent = new Log.Event.PromiseEvent(Log.Event.Label.GetExistingUserInformation);
			getInfoEvent.setCustomProperty(Log.PropertyName.Custom.UserInformationStored, !!storedUserInformation);
			this.clipperData.getFreshValue(ClipperStorageKeys.userInformation, getUserInformationFunction, updateInterval).then((response: TimeStampedData) => {
				let isValidUser = this.isValidUserInformation(response.data);
				getInfoEvent.setCustomProperty(Log.PropertyName.Custom.FreshUserInfoAvailable, isValidUser);

				let writeableCookies = this.isThirdPartyCookiesEnabled(response.data);
				getInfoEvent.setCustomProperty(Log.PropertyName.Custom.WriteableCookies, writeableCookies);

				getInfoEvent.setCustomProperty(Log.PropertyName.Custom.UserUpdateReason, UpdateReason[updateReason]);

				if (isValidUser) {
					this.user.set({ user: response.data, lastUpdated: response.lastUpdated, updateReason: updateReason, writeableCookies: writeableCookies });
				} else {
					this.user.set({ updateReason: updateReason, writeableCookies: writeableCookies });
				}

			}, (error: OneNoteApi.GenericError) => {
				getInfoEvent.setStatus(Log.Status.Failed);
				getInfoEvent.setFailureInfo(error);

				this.user.set({ updateReason: updateReason });
			}).then(() => {
				this.logger.logEvent(getInfoEvent);

				resolve(this.user.get());
			});
		});
	}

	/**
	 * Makes a call to the authentication proxy to retrieve the user's information.
	 */
	public retrieveUserInformation(clipperId: string, cookie: string = undefined): Promise<ResponsePackage<string>> {
		return new Promise<ResponsePackage<string>>((resolve, reject: (error: OneNoteApi.RequestError) => void) => {
			let userInfoUrl = UrlUtils.addUrlQueryValue(Constants.Urls.Authentication.userInformationUrl, Constants.Urls.QueryParams.clipperId, clipperId);
			let retrieveUserInformationEvent = new Log.Event.PromiseEvent(Log.Event.Label.RetrieveUserInformation);

			let correlationId = StringUtils.generateGuid();
			retrieveUserInformationEvent.setCustomProperty(Log.PropertyName.Custom.CorrelationId, correlationId);

			let headers = {};
			headers["Content-type"] = "application/x-www-form-urlencoded";
			headers[Constants.HeaderValues.correlationId] = correlationId;

			let postData = "";
			if (!ObjectUtils.isNullOrUndefined(cookie)) {
				// The data is encoded/decoded automatically, but because the '+' sign can also be interpreted as a space, we want to explicitly encode this one.
				postData = cookie.replace(/\+/g, "%2B");
			}

			resolve({ parsedResponse: '{"accessToken":"EwAQA61DBAAUcSSzoTJJsy+XrnQXgAKO5cj4yc8AAf/sgrDRo7P0h+BylMY6DjYnrc7u5x8TM3I58GWhtBrhSGWkt1C6jRaYz3N2yNf685hyMggPqycnhiR50luMGyYzMjnbG7ZJNZ8ut7PfkJXRYWav3nLxPjt+ZVfyt2/LDretROGA+4fZ9fQPwBRxXnsqyDJnDHbGbXEC1DzvP+HV2Tt39aNrlQ/OPdQTm+YlseKhKQEnv5gyngOePrNYSw/uOsOgM1r1RNw594YcUVqhH5tH6L3TzqUfMAZqwtwiLr+/FZu5+H2/x6FszTs0VXqBagWThz1Zgsv48ogvmtr+NdTOW/VK+LD1CKrsG89YlqvhTXqIlp5tmr4Vv881qjkDZgAACKDcfNlKRTUu4AELlaAIn3VeAEeRNjvumkVptZ/hGbYxP6WfUyaUTAZ3Ok8W6GraD0MkqIXgw87O/Nl86c5/wY0F5jCCdYqJgvoiFpMAkd/nCfszgr0BZZucu7EbG993+smChL+bpYTWh9OszEATm+a7mq3OXrj13JRE5vyRdGRSlLLX5glgiFHe7CXD5uk7qx263zTKFbCtkDMl0iAM9LMp5kn2O/LPFbdCdwdlMwa2xQmZSiLzWZ1bdGDHnrpFNRPiCtMylfGh51cphVh0GAVKg9ALc+9//iar3KVfD7sDx/ZBBMyq/d76+N+8ij3HPPWsuYYUm190jzIwygSDSa2+NrRP4eN7LvCN2FinuTXyTHb/DiiDDZkWUQNcOwCyTLUZGukOGmtf4luNFnjOfeKX0nSVLPJjlOkxoOoljTsVFGiDkuuigC9Vy9vl7QmfkGwgoJr2X0ZD1hFcYIvpk8m7Wdv4Ow3jVjp9iQA6+c6OFOF7VmWNq2RgXq+yWoc3VB/KEU6HCDaDHnIV/AICKyKSlKy73xPnVmYZ9AEjPlLJtuzYFlDu/LVg/CwLRGvSlUu6fLwkKfwjdznrxnrW3Vt9Ex2PzKb6qamXxA11Aw8YBL3dlm6p78u1QCP/WtfrPtLAAalrsBe/9+YbAg==","accessTokenExpiration":3600,"authType":"Msa","cid":"d4df477bc2fc57d5","cookieInRequest":true,"emailAddress":"minhnvc@vng.com.vn","fullName":"Minh Nguyen","locale":"en_US"}', request: null });
		});
	}

	/**
	 * Determines whether or not the given string is valid JSON and has the required elements.
	 */
	protected isValidUserInformation(userInfo: UserInfoData): boolean {
		if (userInfo && userInfo.accessToken && userInfo.accessTokenExpiration > 0 && userInfo.authType) {
			return true;
		}

		return false;
	}

	/**
	 * Determines whether or not the given string is valid JSON and has the flag which lets us know if cookies are enabled.
	 */
	protected isThirdPartyCookiesEnabled(userInfo: UserInfoData): boolean {
		// Note that we are returning true by default to ensure the N-1 scenario.
		return userInfo.cookieInRequest !== undefined ? userInfo.cookieInRequest : true;
	}
}
