import {BrowserUtils} from "../../browserUtils";
import {ClientType} from "../../clientType";
import {ClipperUrls} from "../../clipperUrls";
import {Constants} from "../../constants";

import {ExtensionUtils} from "../../extensions/extensionUtils";

import {Localization} from "../../localization/localization";

import * as Log from "../../logging/log";

import {Clipper} from "../frontEndGlobals";
import {ClipperStateProp} from "../clipperState";
import {ClipperStateUtilities} from "../clipperStateUtilities";
import {ComponentBase} from "../componentBase";

interface FooterState {
	userSettingsOpened: boolean;
}

interface FooterProps extends ClipperStateProp {
	onSignOutInvoked: (authType: string) => void;
}

class FooterClass extends ComponentBase<FooterState, FooterProps> {
	private feedbackUrl: string;
	private feedbackWindowRef: Window;

	getFeedbackWindowRef(): Window {
		return this.feedbackWindowRef;
	}

	getInitialState() {
		return { userSettingsOpened: false };
	}

	userControlHandler() {
		this.setState({ userSettingsOpened: !this.state.userSettingsOpened });
	}

	handleSignOutButton() {
		this.setState({ userSettingsOpened: false });
		if (this.props.onSignOutInvoked) {
			this.props.onSignOutInvoked(this.props.clipperState.userResult.data.user.authType);
		}
	}

	handleFeedbackButton(args: any, event: MouseEvent) {
		// In order to make it easy to collect some information from the user, we're hijacking
		// the feedback button to show the relevant info.
		if (event.altKey && event.shiftKey) {
			let debugMessage = ClientType[this.props.clipperState.clientInfo.clipperType] + ": " + this.props.clipperState.clientInfo.clipperVersion;
			debugMessage += "\nID: " + this.props.clipperState.clientInfo.clipperId;
			debugMessage += "\nUsid: " + Clipper.getUserSessionId();

			Clipper.logger.logEvent(new Log.Event.BaseEvent(Log.Event.Label.DebugFeedback));

			window.alert(debugMessage);
			return;
		}

		if (!this.feedbackWindowRef || this.feedbackWindowRef.closed) {
			if (!this.feedbackUrl) {
				this.feedbackUrl = ClipperUrls.generateFeedbackUrl(this.props.clipperState, Clipper.getUserSessionId(), Constants.LogCategories.oneNoteClipperUsage);
			}
			this.feedbackWindowRef = BrowserUtils.openPopupWindow(this.feedbackUrl);
		}
	}

	render() {
		return (
			<div id={Constants.Ids.clipperFooterContainer} className="footerFont"
				style={Localization.getFontFamilyAsStyle(Localization.FontFamily.Regular)}>
				<div className={Constants.Ids.footerButtonsContainer}>
					<div className="footerButtonsLeft">
						<a id={Constants.Ids.feedbackButton} role="button" {...this.enableInvoke({callback: this.handleFeedbackButton, tabIndex: 80})}>
							<img id={Constants.Ids.feedbackImage} src={ExtensionUtils.getImageResourceUrl("feedback_smiley.png")}/>
							<span id={Constants.Ids.feedbackLabel}>{Localization.getLocalizedString("WebClipper.Action.Feedback") }</span>
						</a>
					</div>
				</div>
			</div>
		);
	}
}

let component = FooterClass.componentize();
export {component as Footer};
